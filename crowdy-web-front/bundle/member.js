window.member = {}
member.join = require('../src/member/member-join.js').default;
member.login = require('../src/member/member-login.js').default;
member.findPassword = require('../src/member/member-find-password.js').default;
member.settingPassword = require('../src/member/member-setting-password.js').default;