window.common = {}
common.select = require('../src/common/select.js').default;
common.datePicker = require('../src/common/date-picker.js').default;
common.dropifyInput = require('../src/common/dropify-input.js').default;
common.textEditor = require('../src/common/text-editor.js').default;
common.fileInput = require('../src/common/file-input.js').default;
common.investModal = require('../src/common/invest-modal.js').default;
common.rejectedModal = require('../src/common/rejected-modal.js').default;