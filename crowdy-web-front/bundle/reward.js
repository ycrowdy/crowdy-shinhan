window.reward = {}
reward.list = require('../src/reward/reward-list-main.js').default;
reward.detail = require('../src/reward/reward-detail.js').default;
reward.best = require('../src/reward/reward-best-list.js').default;
reward.funding = {}
reward.funding.main = require('../src/reward/reward-funding-main.js').default;
reward.funding.support = require('../src/reward/reward-funding-support.js').default;
reward.funding.payment = require('../src/reward/reward-funding-payment.js').default;