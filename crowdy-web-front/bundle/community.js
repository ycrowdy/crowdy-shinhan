window.community = {}
community.make = require('../src/community/community-make.js').default;
community.banner = require('../src/community/community-banner.js').default;
community.list = require('../src/community/community-list.js').default;
community.main = require('../src/community/community-detail-main.js').default;
community.detail = require('../src/community/community-detail.js').default;
community.invest = require('../src/community/community-invest.js').default;
