window.crowdy = {}

crowdy.main = require('../src/crowdy/main.js').default;
// crowdy.banner = require('../src/crowdy/banner.js').default;
crowdy.companyBanner = require('../src/crowdy/company-banner.js').default;
crowdy.preOpenList = require('../src/crowdy/pre-open-list.js').default;
crowdy.preOpen = require('../src/crowdy/pre-open.js').default;
crowdy.term = require('../src/crowdy/term.js').default;
crowdy.nav = require('../src/crowdy/nav.js').default;
crowdy.news = require('../src/crowdy/main-news.js').default;
// crowdy.magazine = require('../src/crowdy/crowdy-magazine.js').default;
crowdy.help = require('../src/crowdy/help.js').default;
crowdy.search = require('../src/crowdy/search.js').default;