var webpack = require('webpack')
var WebpackChunkHash = require('webpack-chunk-hash');
// var path = require('path')

module.exports = {
  entry: {
    component : [__dirname +'/bundle/common.js',
        __dirname +'/bundle/reward.js', 
        __dirname +'/bundle/invest.js',
        __dirname +'/bundle/community.js',
        __dirname +'/bundle/make.js',
        __dirname +'/bundle/member.js',
        __dirname +'/bundle/news.js',
        __dirname +'/bundle/crowdy.js',
        __dirname + '/bundle/mypage.js',
        __dirname + '/bundle/dashboard.js',
        __dirname + '/bundle/simulation.js',
        __dirname + '/bundle/account.js']
  },
  output: {
    path: __dirname + '/dist/',
    //filename: '[name].[chunkhash].js',
     filename: '[name].js',
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /(node_modules)/,
      use: [{
        loader: 'babel-loader',
        options: {
          presets: [['es2015', {modules: false}]],
          plugins: ['syntax-dynamic-import']
        }
      }]
    }]
  },
  plugins: [
   new webpack.optimize.UglifyJsPlugin(),
   new WebpackChunkHash({algorithm: 'md5'})
  ]
};