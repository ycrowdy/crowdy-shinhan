class SearchInput {
    component() {
        return {
            template : `
                <input type="text" class="form-control" v-model="keyWord" v-on:keyup.enter="searchKeyWord" ref="input"/>
            `,
            data : function() {
                return {
                    keyWord : ''
                }
            },
            methods : {
                searchKeyWord : function() {
                    this.keyWord = this.keyWord.replace(/^\s+|\s+$/g,"")
					var encodeURL = encodeURI('/crowdy/search?title=' + this.keyWord);
					window.open(encodeURL, '_self')
				},
            }
        }
    }
}

export default new SearchInput()
