class NumberInput {
    component() {
        return {
            template : `
                <input :type="type" :value="num" @input="updateNum($event.target.value)" pattern="[0-9]*" ref="input"/>
            `,
            props : {
                num : {
                    default : ''
                },
                length : {
                    default : 0
                },
                type : {
                    default : 'text'
                }
            },
            methods : {
                updateNum : function(value) {
                    var data = value.replace(/^\s+|\s+$/g,""); //
                    this.$refs.input.value = data.replace(/[^0-9]/g, '')
                    this.$emit('input', data.replace(/[^0-9]/g, ''))
                }
            }
        }
    }
}

export default new NumberInput()
