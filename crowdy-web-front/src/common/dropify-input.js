class DropifyInput {
	component() {
		return {
			template : `
				<input type="file" class="dropify" v-on:change="read" :data-allowed-file-extensions="defaultFileExt" :data-max-file-size-preview="defaultFileMaxSize"/>
			`,
			props : {
				defaultMessage : {
					default : '이미지 추가하기'
				},
				defaultImg : {
					default : ''
				},
				defaultFileExt : {
					default : 'png jpg jpeg'
				},
				defaultFileMaxSize : {
					default : '5M'
				},
				defaultIndex : {
					default : 0
				}
			},
			data : function() {
				return {
					file : {
						fileData : "",
						fileName : this.defaultImg,
						state : true
					},
					tag : ''
				}
			},
			computed : {
				img : function() {
					return this.defaultImg != '' ? '//' + this.defaultImg : this.defaultImg;
				}
			},
			beforeDestroy : function(){
				this.tag.destroy()
			},
			mounted : function() {
				var self = this;
				var el = $(this.$el).dropify({
					defaultFile : self.img,
					messages: {
						default: self.defaultMessage,
						replace: '터치 하여 이미지를 교체 합니다',
						remove:  '삭제',
						error:   '오류가 발생하였습니다.'
					}
				});

				el.on('dropify.afterClear', function(event, element){
				    self.file.fileData = "";
				    self.file.fileName = "";
				    self.$emit('input', self.file);
					self.$emit('clear', self.defaultIndex);
				});

				this.tag = el.data('dropify')
			},
			methods : {
				read : function(e) {
					var self = this;
					var files = e.target.files || e.dataTransfer.files;
					if (!files.length) return;
					
					var reader = new FileReader();
					reader.onload = function(e) {
						if(files[0]) {
							self.file.fileData = e.target.result;
							self.file.fileName = files[0].name;
							self.$emit('input', self.file);
						}
					};
					reader.readAsDataURL(files[0]);
				}
			} 
		}
	}
}

export default new DropifyInput()