class Select {
	component() {
		return {
			template : `
				<select>
					<slot></slot>
				</select>
			`,
			props: ['options', 'value'],
			mounted : function() {
				var self = this;
				$(this.$el)
			      .select2({ data: this.options, minimumResultsForSearch: -1 })
			      .val(this.value)
			      .trigger('change')
			      .on('change', function () {
				        self.$emit('input', this.value)
				        self.$emit('change', this.value)
		      		})
			},
			watch : {
				value: function (value) {
			      $(this.$el).val(value).trigger('change');
			    }
			},
			destroyed: function () {
			    $(this.$el).off().select2('destroy')
		  	}
		}
	}
}

export default new Select()