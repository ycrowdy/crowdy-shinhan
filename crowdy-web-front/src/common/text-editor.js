class TextEditor {
	component() {
		return {
			template : `
				<textarea :id="id" :value="value"></textarea>
			`,
			props : {
				id: {
					type: String,
			        default: 'editor'
		      	},
		      	value: {
			        type: String,
			        default : ''
		      	}
			},
			mounted : function() {
				var self = this;
				tinymce.init({
					selector: "#" + self.id,
					height : "650" ,
					theme: 'modern',
					skin : 'white',
					menubar: false,
				 	plugins: [
				 		'autolink lists link image hr visualblocks media table textcolor'
					    // 'advlist autolink lists link image charmap print preview hr anchor pagebreak',
					    // 'searchreplace wordcount visualblocks visualchars code ',
					    // 'insertdatetime media nonbreaking save table directionality',
					    // 'emoticons template paste textcolor textpattern codesample image  ' 
					    // stickytoolbar autoresize fullscreen toc help colorpicker fullpage strikethrough contextmenu(우클릭)
			  		],
			  		toolbar1: 'undo redo | styleselect | bold italic custom_underline textcolor backgroundcolor | crowdy_hr crowdy_blockquote |  alignleft aligncenter |  crowdy_list | table link image media',
			  		// toolbar2: 'blockquote_center blockquote_left | hr1 hr2 | crowdy_blockquote',
				  	style_formats: [
					   	{
					    	title: '큰 제목', 
					    	block: 'h2'
					    },
					     {
					    	title: '중간 제목', 
					    	block: 'h3'
					    },
						{
					    	title: '작은 제목', 
					    	block: 'h4'
					    },
					    {
					    	title: '본문', 
					    	block: 'p'
					    },
					    {
					    	title: '사진주석', 
					    	block: 'h6'
					    }
				  	],
				  	formats: {
					  	backgroundcolor: { inline: 'span', 'classes': 'bg_color' },
					  	textcolor: { inline: 'span',  'classes': 'txt_color'},
					  	'custom_underline': { inline: 'span', 'classes': 'custom_underline'}
				  	},
			    	content_css: [
			    		// '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
				    	'/resources/css/custom-tinymce.css?20180719v01',	
						'/resources/js/tinymce/skins/white/skin.min.css' 
		  			],
				  	//  링크 기본적으로 새창으로 뜨도록  / 제목, 단계 없애기 
		 			default_link_target: "_blank", 
				 	link_title: false,
		 			target_list: false,

		 			// 테이블 생성 후 테이블 클릭하고 수정하는 버튼 들 : tableprops 제외
			  		table_toolbar: "tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol", 
					table_appearance_options: false,
					table_grid: true,
				  	table_advtab: false,
				  	table_cell_advtab: false,
				  	table_row_advtab: false,

				  	// block 보이기
				  	visualblocks_default_state: true,

				  	// 이미지 Drag & Drop 방지 
				  	paste_data_images: false,
				 	// 이미지 팝업창 : 이미지 설명 
				  	image_description : false,
				  	// 이미지 팝업창 : 이미지 사이즈 관련 텍스트  
				  	image_dimensions: false,
				  	image_advtab: false,

				  	media_advtab: false,
				  	media_live_embeds: true,
				  	media_poster: false,
				  	media_dimensions : false,
				  	media_alt_source: false,
				  	media_filter_html: false,
					
					// 복붙시 스타일 따라가지 않음 
					paste_as_text: true,

					force_p_newlines : true,

					// enter시 p태그가 생김
					forced_root_block : 'p',

					extended_valid_elements : "span[!class]",


				  	automatic_uploads: true,
				  	file_picker_types: 'image',
				  	file_picker_callback: function(cb, value, meta) {
				    	var input = document.createElement('input');
				    	input.setAttribute('type', 'file');
				    	input.setAttribute('accept', 'image/*');
				     
				    	input.onchange = function(e) {
							var files = e.target.files || e.dataTransfer.files;
							if (!files.length) return;

							var reader = new FileReader();
							reader.onload = function (e) {
								if(files[0]) {
									var file = {};
									file.fileData = e.target.result;
									file.fileName = files[0].name;
									self.imageSave(cb, file);
								}
				      		}; 
				      		reader.readAsDataURL(files[0]);
				    	};
				    
				    	input.click(); 
				  	},
				  	setup: function(editor) {
						editor.addButton('backgroundcolor', {
					      	icon: 'backcolor',
					      	tooltip: "글자 배경색 바꾸기",
					      	onclick: function(e) { 
					    	 	editor.execCommand('mceToggleFormat', false, 'backgroundcolor');
						  	},
						  	onPostRender: function() {
							    var _this = this;
				                editor.on('NodeChange', function(e) {
				                    var is_active = jQuery( editor.selection.getNode() ).hasClass('bg_color');
				                    _this.active( is_active );
				                })
							} 
					    });

					    editor.addButton('textcolor', {
				      		icon: 'forecolor',
					      	tooltip: "글자색 바꾸기",
					      	onclick: function(e) { 
					    	 	editor.execCommand('mceToggleFormat', false, 'textcolor');
						  	},
						  	onPostRender: function() {
							    var _this = this;
				                editor.on('NodeChange', function(e) {
				                    var is_active = jQuery( editor.selection.getNode() ).hasClass('txt_color');
				                    _this.active( is_active );
				                })
							}
					    });

					    
					    editor.addButton('custom_underline', {
				      		icon: 'underline',
					      	tooltip: "밑줄",
					      	onclick: function(e) { 
					    	 	editor.execCommand('mceToggleFormat', false, 'custom_underline');
						  	},
						  	onPostRender: function() {
							    var _this = this;
				                editor.on('NodeChange', function(e) {
				                    var is_active = jQuery( editor.selection.getNode() ).hasClass('custom_underline');
				                    _this.active( is_active );
				                })
							}	 
					    });

 						 editor.addButton('crowdy_list', {
						  type: 'menubutton',
				      		icon: 'bullist',
					      	tooltip: '리스트',
					      	menu: [{
						      	icon : 'bullist',
						        text: '점 리스트',
						        tooltip: '점 리스트',
						        onclick: function() {
            						editor.execCommand('InsertUnorderedList');          
						        }
						      }, {
						        icon : 'numlist',
						        text: '숫자 리스트',
					      		tooltip: '숫자 리스트',
						        onclick: function() {
						          	editor.execCommand('InsertOrderedList');          
							      }
						        }]
					    });


					    editor.addButton('crowdy_hr', {
						  type: 'menubutton',
					      image: '/resources/css/img/btn_icon_hr1.png',
					      tooltip: '구분선',
					      menu: [{
					      	icon : '/resources/css/img/btn_icon_hr1.png',
					      	image : '/resources/css/img/btn_icon_hr1.png',
					        text: '긴 구분선',
					        tooltip: '긴 구분선',
					        onclick: function() {
					          	editor.insertContent('<hr class="hr1"/><p></p>');
					        }
					      }, {
					        icon : '/resources/css/img/btn_icon_hr2.png',
					        image : '/resources/css/img/btn_icon_hr2.png',
					        text: '짧은 구분선',
				      		tooltip: '짧은 구분선',
					        onclick: function() {
					          	editor.insertContent('<hr class="hr2"/><p></p>');
						      }
					        }]
						});

					    editor.addButton('crowdy_blockquote', {
							type: 'menubutton',
						      image: '/resources/css/img/btn_icon_bq1.png',
						      tooltip: '인용구',
						      menu: [{
						      	icon : '/resources/css/img/btn_icon_bq1.png',
						      	image : '/resources/css/img/btn_icon_bq1.png',
						        text: '인용구1',
						        tooltip: '인용구1',
						        onclick: function() {
						          	var node = jQuery( editor.selection.getNode() )
						          	var is_active = node.hasClass('blockquote_center');
						          	var content = node.text();

						          	if (content == "" || content == " ") {
						          		content = '내용을 입력해주세요.';
						          	}
						          	
						          	editor.selection.getNode().parentNode.removeChild(editor.selection.getNode());

						          	if (!is_active) {
										editor.insertContent('<blockquote class="blockquote_center"> ' + content + ' </blockquote>');
						          	} else {
						          		editor.insertContent('<p> ' + content + '</p>');
						          	}
						        },
							  	onPostRender: function() {
								    var _this = this;
					                editor.on('NodeChange', function(e) {
					                    var is_active = jQuery( editor.selection.getNode() ).hasClass('blockquote_center');
					                    _this.active( is_active );
					                })
								}
						      }, {
						        icon : '/resources/css/img/btn_icon_bq2.png',
						        tooltip: '인용구2',
						        text: '인용구2',
					      		image : '/resources/css/img/btn_icon_bq2.png',
						        onclick: function() {
						          	var node = jQuery( editor.selection.getNode() )
						          	var is_active = node.hasClass('blockquote_left');
						          	var content = node.text();

						          	if (content == "" || content == " ") {
						          		content = '내용을 입력해주세요.';
						          	}
						          	// editor.selection.getNode().remove();
						          	editor.selection.getNode().parentNode.removeChild(editor.selection.getNode());

						          	if (!is_active) {
										editor.insertContent('<blockquote class="blockquote_left"> ' + content + ' </blockquote>');
						          	} else {
						          		editor.insertContent('<p> ' + content + '</p>');
						          	}
							    },
							  	onPostRender: function() {
								    var _this = this;
					                editor.on('NodeChange', function(e) {
					                    var is_active = jQuery( editor.selection.getNode() ).hasClass('blockquote_left');
					                    _this.active( is_active );
					                })
								}

						        }]
						    });
				  	},
				  	init_instance_callback: function(editor) {
				      	editor.on('KeyUp', (e) => {
				        	self.$emit('input', editor.getContent());
				      	});
				      	editor.on('Change', (e) => {
				        	self.$emit('input', editor.getContent());
				      	});
					      
				      	editor.setContent(self.value);

				      	
				    }
				});
				
			},
			destroyed : function() {
				tinymce.get(this.id).destroy();
			},
			methods : {
				imageSave : function(cb, file) {
					$('.page-loader-more').fadeIn('')
					 axios.post('/file/upload', {fileType : "aws" , file : file})
						.then(function (response) {

							$('.page-loader-more').fadeOut('');
					 		cb('//' + response.data, { title: file.fileName }); 
						})
				}
			}
		}
	}
}

export default new TextEditor()