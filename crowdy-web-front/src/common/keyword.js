class Keyword {
	component() {
		return {
			template : `
				<input type="text" class="form-control input-sm keyword" placeholder="최대 5개까지 등록 가능합니다. 키워드 입력 후 엔터를 눌러주세요."/>
			`,
			data : function() {
				return {
					value : []
				}
			},
			
			mounted : function() {
				var self = this;

				self.$nextTick(function() {
				    	$(this.$el)
					  	.on('tokenfield:createtoken', function (e) {
						    self.value.push(e.attrs.value)
						    self.$emit('input', self.value);
						  	})
					  	.on('tokenfield:removedtoken', function (e) {
							self.value = _.without(self.value, e.attrs.value)
							self.$emit('input', self.value);
						})
						.tokenfield();
			    	});

			},
			watch : {
				// value: function (value) {
			 //      $(this.$el).val(value).trigger('change');
			 //    }
			}
		}
	}
}
export default new Keyword()