class UrlInput {
    component() {
        return {
            template : `
                <input type="text" :value="url" @input="updateUrl($event.target.value)" ref="input"/>
            `,
            props : ['url'],
            methods : {
                updateUrl : function(value) {
                    var data = value.replace(/^\s+|\s+$/g,"");
                    this.$refs.input.value = data.replace(/[ㄱ-ㅎ|ㅏ-ㅣ|가-힣|A-Z|~!?@\#$%<>^&*\()=+\’\\,.:;\'\"`/]/g, '')
                    this.$emit('input', data.replace(/[ㄱ-ㅎ|ㅏ-ㅣ|가-힣|A-Z|~!?@\#$%<>^&*\()=+\’\\,.:;\'\"`/]/g, ''))
                }
            }
        }
    }
}

export default new UrlInput()