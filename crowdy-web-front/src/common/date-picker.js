class DatePicker {
	component() {
		return {
			template : `
				<input class="form-control datetimepicker" style="border-radius: 4px !important;"/>
			`,
			props: { 
				dateFormat : {
					default : 'yy-mm-dd'
				},  
				date : {
					type: String,
					required: true
				},
				max : {
					type: String,
					required: false,
					default : null	
				},
				min : {
					type: String,
					required: false,
					default : null	
				}
			},
			watch : {
				date : function() {
					$(this.$el).datepicker('setDate', this.date);	
				}
			},
			mounted : function() {
				var self = this;
				$(this.$el).datepicker({
					dateFormat: self.dateFormat,
					prevText: '이전 달',
				    nextText: '다음 달',
				    monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				    monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				    dayNames: ['일', '월', '화', '수', '목', '금', '토'],
				    dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
				    dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
				    showMonthAfterYear: true,
				    yearSuffix: '년',
					onSelect: function(date) {
						self.$emit('input', date);
			    	},
			    	beforeShow:function(input) {
				        $(input).css({
				            // "position": "relative",
				            "z-index": 999999
				        });
				    },
					maxDate: self.max,
					minDate: self.min
			 	 }).datepicker("setDate", self.date);
			},
			beforeDestroy: function() {
			    $(this.$el).datepicker('hide').datepicker('destroy');
		  	}
		}
	}
}


export default new DatePicker()