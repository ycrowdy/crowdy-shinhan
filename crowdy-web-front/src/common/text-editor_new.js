class TextEditor {
	component() {
		return {
			template : `
				<textarea :id="id" :value="value"></textarea>
			`,
			props : {
				id: {
					type: String,
			        default: 'editor'
		      	},
		      	value: {
			        type: String,
			        default : ''
		      	}
			},
			mounted : function() {
				var self = this;
				tinymce.init({
					selector: "#" + self.id,
					height : "650" ,
					theme: 'modern',
					menubar: false,
				 	plugins: [
					    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
					    'searchreplace wordcount visualblocks visualchars code ',
					    'insertdatetime media nonbreaking save table directionality',
					    'emoticons template paste textcolor textpattern codesample image stickytoolbar ' //autoresize fullscreen toc help colorpicker fullpage strikethrough contextmenu(우클릭)
			  		],
			  		toolbar1: 'undo redo | styleselect | bold italic custom_underline textcolor backgroundcolor | alignleft aligncenter |  bullist numlist | link image media table',
			  		toolbar2: 'blockquote_center blockquote_left | hr1 hr2 hr3 ',
				  	style_formats: [
					   	{
					    	title: '제목1', 
					    	block: 'h2'
					    	// inline : 'h2'
					    },
					     {
					    	title: '제목2', 
					    	block: 'h3'
					    },
						{
					    	title: '제목3', 
					    	block: 'h4'
					    },
					    // {
					    // 	title: '본문1', 
					    // 	block: 'h5'
					    // },
					    {
					    	title: '본문', 
					    	block: 'p'
					    },
					    {
					    	title: '주석', 
					    	block: 'h6'
					    }
				  	],
				  	formats: {
					  	backgroundcolor: { inline: 'span', 'classes': 'bg_color' },
					  	textcolor: { inline: 'span',  'classes': 'txt_color'},
					  	'custom_underline': { inline: 'span', 'classes': 'custom_underline'}
				  	},
				  	content_css: [
					    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
					    // '//www.tinymce.com/css/codepen.min.css',
					    '/resources/css/custom-tinymce.css?20180702v02',
					    '/resources/js/tinymce/skins/lightgray/skin.min.css'

				  	],
				  	//  링크 기본적으로 새창으로 뜨도록  / 제목, 단계 없애기 
		 			default_link_target: "_blank", 
				 	link_title: false,
		 			target_list: false,

		 			// 테이블 생성 후 테이블 클릭하고 수정하는 버튼 들 : tableprops 제외
			  		table_toolbar: "tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol", 
					table_appearance_options: false,
					table_grid: true,
				  	table_advtab: false,
				  	table_cell_advtab: false,
				  	table_row_advtab: false,

				  	// block 보이기
				  	visualblocks_default_state: true,

				  	// 이미지 Drag & Drop 방지 
				  	paste_data_images: false,
				 	// 이미지 팝업창 : 이미지 설명 
				  	image_description : false,
				  	// 이미지 팝업창 : 이미지 사이즈 관련 텍스트  
				  	image_dimensions: false,
				  	image_advtab: false,

				  	media_advtab: false,
				  	media_live_embeds: true,
				  	media_poster: false,
				  	media_dimensions : false,
				  	media_alt_source: false,
				  	media_filter_html: false,
					
					// 복붙시 스타일 따라가지 않음 
					paste_as_text: true,

				  	automatic_uploads: true,
				  	file_picker_types: 'image',
				  	file_picker_callback: function(cb, value, meta) {
				    	var input = document.createElement('input');
				    	input.setAttribute('type', 'file');
				    	input.setAttribute('accept', 'image/*');
				     
				    	input.onchange = function(e) {
							var files = e.target.files || e.dataTransfer.files;
							if (!files.length) return;

							var reader = new FileReader();
							reader.onload = function (e) {
								if(files[0]) {
									var file = {};
									file.fileData = e.target.result;
									file.fileName = files[0].name;
									self.imageSave(cb, file);
								}
				      		}; 
				      		reader.readAsDataURL(files[0]);
				    	};
				    
				    	input.click(); 
				  	},
				  	setup: function(editor) {
						editor.addButton('backgroundcolor', {
					      	icon: 'backcolor',
					      	tooltip: "글자 배경색 바꾸기",
					      	onclick: function(e) { 
					    	 	editor.execCommand('mceToggleFormat', false, 'backgroundcolor');
						  	} 
					    });

					    editor.addButton('textcolor', {
				      		icon: 'forecolor',
					      	tooltip: "글자색 바꾸기",
					      	onclick: function(e) { 
					    	 	editor.execCommand('mceToggleFormat', false, 'textcolor');
						  	} 
					    });

					    
					    editor.addButton('custom_underline', {
				      		icon: 'underline',
					      	tooltip: "밑줄",
					      	onclick: function(e) { 
					    	 	editor.execCommand('mceToggleFormat', false, 'custom_underline');
						  	} 
					    });

					    editor.addButton('blockquote_center', {
					      tooltip: '인용구1',
					      // icon: false,
					      image : '/resources/css/img/btn_icon_bq1.png',
					      onclick: function (e) {

					      	var content = editor.selection.getContent();

					      	if (content == "") {
					      		editor.insertContent('<blockquote class="blockquote_center"> 내용을 입력해주세요. </blockquote><p></p>');
					      	} else {
					      		editor.insertContent('<blockquote class="blockquote_center"> ' + content + ' </blockquote><p></p>');
					      	}

					        
					      }
					    });

					    editor.addButton('blockquote_left', {
					      tooltip: '인용구2',
					      // icon: false,
					      image : '/resources/css/img/btn_icon_bq2.png',
					      onclick: function () {

					      	var content = editor.selection.getContent();

					      	if (content == "") {
					      		editor.insertContent('<blockquote class="blockquote_left"> 내용을 입력해주세요. </blockquote><p></p>');
					      	} else {
					      		editor.insertContent('<blockquote class="blockquote_left"> ' + content + ' </blockquote><p></p>');
					      	}
					      }
					    });

  						editor.addButton('hr1', {
					      tooltip: '구분선1',
					      // icon: false,
					      image : '/resources/css/img/btn_icon_hr1.png',
					      onclick: function () {
					        editor.insertContent('<hr class="hr1" />');
					      }
					    });

					    editor.addButton('hr2', {
					      tooltip: '구분선2',
					      image : '/resources/css/img/btn_icon_hr2.png',
					      // icon: false,
					      onclick: function () {
					        editor.insertContent('<hr class="hr2" />');
					      }
					    });

					    editor.addButton('hr3', {
					      tooltip: '구분선3',
					      image : '/resources/css/img/btn_icon_hr3.png',
					      // icon: false,
					      onclick: function () {
					        editor.insertContent('<hr class="hr3" />');
					      }
					    });

				  	},
				  	init_instance_callback: function(editor) {
				      	editor.on('KeyUp', (e) => {
				        	self.$emit('input', editor.getContent());
				      	});
				      	editor.on('Change', (e) => {
				        	self.$emit('input', editor.getContent());
				      	});
					      
				      	editor.setContent(self.value);
				    }
				});

				
			},
			destroyed : function() {
				tinymce.get(this.id).destroy();
			},
			methods : {
				imageSave : function(cb, file) {
					$('.page-loader-more').fadeIn('')
					 axios.post('/file/upload', {fileType : "aws" , file : file})
						.then(function (response) {

							$('.page-loader-more').fadeOut('');
					 		cb('//' + response.data, { title: file.fileName }); 
						})
				}
			}
		}
	}
}

export default new TextEditor()