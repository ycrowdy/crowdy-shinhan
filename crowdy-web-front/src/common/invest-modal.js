class InvestModal {
	component() {
		return {
			template : `
                <!-- 투자자별 자격 요건 안내 -->
                <div id="investModal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body">
                            	<div v-show="mode == 1">
	                                <ul>
	                                    <li>
	                                        <strong class="grey-800">1. 일반투자자</strong>
	                                        <ul>
	                                            <li>
	                                                적격투자자, 전문투자자에 해당하지 않는 모든 투자자
	                                            </li>
	                                        </ul>
	                                    </li>
	                                    <li>
	                                        <div class="mt15"><strong class="grey-800">2. 적격투자자</strong></div>
	                                        <ul>
	                                            <li>
	                                            	1) 근로소득금액이 1억원을 초과하는 경우
	                                            </li>
												<li>
													2) 사업소득금액이 1억원을 초과하는 경우
												</li>
												<li>
													3) 근로소득금액과 사업소득금액의 합계가 1억원을 초과하는 경우
												</li>
												<li>
													4) 금융소득종합과세 대상자인 경우 (이자소득+배당소득이 2천만원을 초과하는 경우)
												</li>
												<li>
													5) 금융전문자격시험에 합격하고, 금융투자전문인력으로 협회에 3년이상 등록되어 있는 경우
												</li>

	                                        </ul>
	                                    </li>
	                                    <li>
	                                        <div class="mt15"><strong class="grey-800">3. 전문투자자</strong></div>
	                                        <ul>
	                                        	<li>
	                                        		1) 자본시장법상 지정전문투자자: 금융투자협회에 등록신청하여 지정전문투자 확인
	                                    		</li>
												<li>
													2) 벤처기업육성에 관한 특별조치법상의 전문엔젤투자자
												</li>
												<li>
													3) 비상장기업에 대한 투자실적 (최근 2년간 1건, 5,000만원 2건 이상 합계 2,000만원)을 보유한 자
												</li>
												<li>
													4) 전문가 그룹: 발행인에게 회계, 자문 등의 용역을 제공하는 공인회계사, 감정인, 변호사, 변리사, 세무사
												</li>
	                                        </ul>
	                                    </li>
	                                </ul>
                                </div>

                                <div v-show="mode == 2">
	                                <ul>
	                                    <li>
	                                        <strong class="grey-800">1. 일반투자자</strong>
	                                        <ul>
	                                            <li>
	                                                적격투자자, 전문투자자에 해당하지 않는 모든 법인
	                                            </li>
	                                        </ul>
	                                    </li>
	                                    <li>
	                                        <div class="mt15"><strong class="grey-800">2. 적격투자자</strong></div>
	                                        <ul>
	                                            <li>
	                                            	직전연도 자기자본 10억 이상의 법인
	                                            </li>
	                                        </ul>
	                                    </li>
	                                    <li>
	                                        <div class="mt15"><strong class="grey-800">3. 전문투자자</strong></div>
	                                        <ul>
	                                        	<li>
	                                        		1) 자본시장법 당연전문투자자
	                                        		<ul>
                                    			         <li>a. 주권상장법인</li>
												         <li>b. 은행, 금융투자업자</li>
												         <li>c. 집합투자기구 (개별법 펀드 제외)</li>
												         <li>d.  해외주권상장법인, 국제기구 등</li>
	                                        		</ul>
	                                    		</li>
												<li>
													2) 자본시장법상 지정전문투자자 (금융투자협회 등록)
													<ul>
                                    			         <li>금융투자잔고 100억원 이상의 국내법인, 단체 <br/>(외감법 대상인 경우 50억원)</li>
	                                        		</ul>
												</li>
												<li>
													3) 전문성, 위험감수능력을 갖춘 자
													<ul>
											            <li>a. 중소기업창업투자회사</li>
											            <li>b. 개별법(창지법, 여전법, 벤특법 등)에 따른 집합투자기구 <br/>(창업투자조합, 신기술사업투자조합, 벤처투자조합 등)</li>
											            <li>c. 벤특법상 개인투자조합</li>
													</ul>
												</li>
	                                        </ul>
	                                    </li>
	                                </ul>
                                </div>

                                <table class="table table-condensed">
                                    <thead>
                                        <tr>
                                            <th>투자자 구분</th>
                                            <th>1개 기업당 투자한도</th>
                                            <th>연간 총 투자한도</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-center">일반투자자</td>
                                            <td class="text-center">500만원</td>
                                            <td class="text-center">1,000만원</td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">적격투자자</td>
                                            <td class="text-center">1,000만원</td>
                                            <td class="text-center">2,000만원</td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">전문투자자</td>
                                            <td class="text-center">없음</td>
                                            <td class="text-center">없음</td>
                                        </tr>
                                    </tbody>
                                </table>

                                <div class="modal-footer text-center">
                                    <div class="row row-mobile-n">
                                        <div class="col-xs-6 col-xs-offset-3 col-sm-6 col-sm-offset-3">
                                            <a href="https://image-se.ycrowdy.com/file/%E1%84%8F%E1%85%B3%E1%84%85%E1%85%A1%E1%84%8B%E1%85%AE%E1%84%83%E1%85%B5_%E1%84%8C%E1%85%A5%E1%86%A8%E1%84%80%E1%85%A7%E1%86%A8_%E1%84%8C%E1%85%A5%E1%86%AB%E1%84%86%E1%85%AE%E1%86%AB%E1%84%90%E1%85%AE%E1%84%8C%E1%85%A1%E1%84%8C%E1%85%A1_%E1%84%80%E1%85%AE%E1%84%87%E1%85%B5%E1%84%89%E1%85%A5%E1%84%85%E1%85%B2_ver180905.pdf" target="_blank" class="btn btn-block btn-primary-outline">구비서류 자세히 보기</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- 투자자별 자격 요건 안내 -->
			`,
			props : {
				mode : ""	
			}
		}
	}
}


export default new InvestModal()