class FileInput {
	component() {
		return {
			template : `
				<input type="file" v-on:change="read"/>
			`,
			props : {
				size : {
					default : 10
				}
			},
			data : function() {
				return {
					file : {
						fileData : "",
						fileName : "",
						state : true
					}
				}
			},
			methods: {
				read : function(e) {
					var self = this;
					var files = e.target.files || e.dataTransfer.files;
					if (!files.length) return;
					
					var reader = new FileReader();
					reader.onload = function(e) {
						if((files[0].size / 1024000) > self.size) {
							self.$emit('error', files[0].size);
							return;
						}
						self.file.fileData = e.target.result;
						self.file.fileName = files[0].name;
						self.$emit('input', self.file);
					};
					reader.readAsDataURL(files[0]);
				}
			}
		}
	}
}

export default new FileInput()