class RejectedModal {
	component() {
		return {
			template : `
                <!-- 반려사유 -->
				<div class="modal fade" id="aRejectedModal" role="dialog" tabindex="-1" data-backdrop="static" data-keyboard="false">
					<div class="modal-dialog modal-sm">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							</div>
							<div class="modal-body modal-order">
								<p>
									승인이 반려되었습니다.
									<div class="mt15 red-800">
										반려사유 : {{rejectedReason}}
									</div>
								</p>

								<div class="modal-footer text-center">
									<div class="row not-space">
										<div class="col-xs-4 col-xs-offset-4">
											<button type="button" class="btn btn-block btn-primary-outline" data-dismiss="modal">확인</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- //반려사유 -->
			`,
			props : {
				rejectedReason : ""	
			}
		}
	}
}


export default new RejectedModal()