class LineChart {
	component() {
		return {
			props: ['data', 'options'],
  			extends: VueChartJs.Line,
			mounted : function() {
				this.renderChart(this.data, this.options);
			}
		}
	}
}
export default new LineChart()