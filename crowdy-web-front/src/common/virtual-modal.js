class VirtualModal {
	component() {
		return {
			template : `
                <!-- 투자자별 자격 요건 안내 -->
                <div id="virtualModal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body">
                            	<div v-show="mode == 1">
	                                <ul>
	                                    <li>
	                                        <strong class="grey-800"></strong>
	                                        <ul>
	                                            <li>
	                                               	회원은 본 약관에 동의함으로써, 크라우디 투자 프로젝트에 참여할 수 있는 투자예치금 계좌를 발급 받게 됩니다. 투자예치금 계좌는 회원의 전용 가상계좌이며, 투자 프로젝트에 참여 하기 전, 청약을 원하는 금액을 해당 계좌에 입금해야 합니다.
	                                            </li>
	                                            <li>
	                                               회원이 투자 프로젝트에 청약을 완료하면, 청약증거금 상당의 금액이 투자예치금 계좌에서 공제되며, 해당 금액의 출금이 불가능합니다. 해당 금액은 청약한 투자프로젝트가 모집금액의 80% 달성에 실패할 경우, 프로젝트는 성공하였으나 배정 순위에서 탈락할 경우, 청약 기간 내에 청약 취소를 한 경우에는 회원의 투자예치금 계좌에 해당 금액이 복원되며, 본인 명의의 계좌로 출금이 가능합니다.
	                                            </li>
	                                            <li>
	                                               투자예치금의 출금을 위해서는 '출금계좌등록'의 프로세스를 통해 본인 명의의 계좌를 등록하여야 합니다. 출금계좌등록 전에는 투자예치금 계좌에 잔액이 남아있다고 하더라도 출금이 불가능합니다. 출금계좌등록 프로세스에는 입력한 계좌의 명의확인, 1원 이체 방식을 통한 소유확인 절차가 포함됩니다.
	                                            </li>
	                                            <li>
	                                               회원이 발급받는 투자예치금은 계좌는 (주)크라우디를 예금주로 하는 신한은행 계좌를 모계좌로 하며, 금융투자업 규정 제4-114조에 따라 투자자재산으로 부기 되어있습니다. 해당 계좌는 금융투자업 규정과 크라우디와 신한은행과의 추가약정을 통해 안전하게 관리됩니다.
	                                            </li>
	                                            <li>
	                                               투자예치금 계좌 발급 및 관리를 위해 크라우디가 신한은행에 제공하는 정보는 아래와 같습니다. 
	                                            </li>
	                                        </ul>
	                                    </li>
	                                    <li>
	                                        <div class="mt15"><strong class="grey-800">[개인회원]</strong></div>
	                                        <ul>
	                                            <li>
	                                            	고객명, 생년월일, 휴대폰번호, 출금통장 계좌번호, 가상계좌번호, CI (본인확인기관에서 부여하는 개인식별정보)
	                                            </li>
	                                        </ul>
	                                    </li>
	                                    <li>
	                                        <div class="mt15"><strong class="grey-800">[법인/조합회원]</strong></div>
	                                        <ul>
	                                        	<li>
	                                        		법인/조합명, 사업자번호(조합번호), 대표이사명, 담당자 생년월일, 담당자 CI, 담당자 휴대폰번호, 출금통장 계좌번호, 가상계좌번호 
	                                    		</li>
	                                        </ul>
	                                    </li>
	                                </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- 투자자별 자격 요건 안내 -->
			`,
			props : {
				mode : ""	
			}
		}
	}
}


export default new VirtualModal()