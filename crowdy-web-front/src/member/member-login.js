class MemberLogin {
    component() {
        return {
            template : `
                <div class="member_frame">
                    <div class="login_lpopup">
                        <a href="/">
                            <div class="login_logo">
                                CROWDY
                            </div>
                        </a>

                        <form class="form-horizontal">
                            <div class="form-group row-mobile-n">
                                <div class="col-xs-12">
                                    <!-- <input type="text" class="form-control" :class="{'error' : errors.has('memEmail')}" name="memEmail" v-validate="'required'" v-model="request.memEmail" placeholder="이메일" autofocus  v-on:keyup.enter="emailLogin"/>
                                    <label class="error" v-if="errors.has('memEmail')" v-text="errors.first('memEmail')"></label> -->

                                    <input type="text" class="form-control" :class="{'error' : emailError}" name="memEmail" v-model="request.memEmail" placeholder="이메일" autofocus v-on:keyup="write('email')" v-on:keyup.enter="emailLogin"/>
                                    <label class="error" v-if="emailError" v-text="errorText"></label>

                                </div>
                            </div>

                            <div class="form-group row-mobile-n">
                                <div class="col-xs-12">
                                    <!-- <input type="password" class="form-control" :class="{'error' : errors.has('memPwd')}" name="memPwd" v-validate="'required'" v-model="request.memPwd" placeholder="비밀번호"  v-on:keyup.enter="emailLogin"/>
                                    <label class="error" v-if="errors.has('memPwd')" v-text="errors.first('memPwd')"></label> -->

                                    <input type="password" class="form-control" :class="{'error' : pwdError}" name="memPwd" v-model="request.memPwd" placeholder="비밀번호" v-on:keyup="write('pwd')" v-on:keyup.enter="emailLogin"/>
                                    <label class="error" v-if="pwdError" v-text="errorText"></label>

                                </div>
                            </div>

                            <a class="btn btn-block btn-lg btn-lg-big btn-primary mt15" v-on:click="emailLogin">로그인</a>
                            <label class="error" v-show="error != ''" v-text="error"></label>
                            <hr class="big" />

                            <div class="sns_member_wrap mt30">
                                <div class="sns_login_info">소셜 계정으로 로그인 하기</div>
                                <div class="sns_login_btn mb45 xs-mb30">
                                    <ul>
                                        <li class="nth-child1">
                                            <a v-on:click="snsLogin('facebook')">페이스북</a>
                                        </li>
                                        <!-- <li class="nth-child2">
                                            <a href="javascript:void(0)">카카오톡</a>
                                        </li> -->
                                        <li class="nth-child3">
                                            <a v-on:click="snsLogin('naver')">네이버</a>
                                        </li>
                                        <!-- <li class="nth-child4">
                                            <a href="javascript:void(0)">구글</a>
                                        </li> -->
                                    </ul>
                                </div>
                            </div>

                            <div class="login_etc">
                                <a href="/user/find-password">비밀번호 찾기</a>
                                <a href="/user/join">회원가입하기</a>
                            </div>
                        </form>
                    </div>
                </div>
            `,
            data: function() {
                return {
                    request : {
                        snsLogin : "N",
                        memEmail : "",
                        memPwd : "",
                        memSnsToken : "",
                        snsId : "",
                        snsType : ""
                    },
                    snsInfo : {},
                    error : "",
                    notReturn : [
                        "/user/login",
                        "/user/join"
                    ],
                    errorText : '필수 입력란입니다.',
                    isEmailStart : true,
                    isPwdStart : true
                }
            },
            created: function() {

                // SNS 로그인을 위한 정보 Get
                this.getSnsInfo();
                // NAVER-2 : NAVER OAUTH 확인 (redirectUrl 접속 시)
                this.checkNaverOauth();

                var returnType = this.getParameterByName('returnType');
                if (returnType == '97') {
                    localStorage.setItem('return', '/sc/gipf');
                } else if (returnType == 'foodshow') {
                    localStorage.setItem('return', '/sc/foodshow');
                } else if (returnType == 'bixpo2018') {
                    localStorage.setItem('return', '/sc/bixpo2018');
                }
            },
            computed : {
                emailError : function() {
                    // return (this.request.memEmail != '' ) || this.isStart ? false : true;
                    return this.isEmailStart ? false : (this.request.memEmail != '' ? false : true);
                },
                pwdError : function() {
                    // return this.request.memPwd != '' ? false : true;
                    return this.isPwdStart ? false : (this.request.memPwd != '' ? false : true);
                }
            },
            methods : {
                // SNS 로그인을 위한 정보 Get
                getSnsInfo : function() {
                    var self = this;
                    axios.post('/member/getSnsInfo')
                        .then(function(response){
                            self.snsInfo = response.data;
                        })
                },
                emailLogin : function(){
                    var self = this;
                    self.request.snsLogin = 'N';
                    // this.$validator.validateAll()
                    // .then(function(success) {
                    //     if(!success) return;
                    //     // 로그인 API 
                    //     self.login();
                    // })

                    if (!this.emailError && !this.pwdError) {
                        self.login();
                    } else {
                        this.error = '아이디와 비밀번호를 입력해주세요.';    
                    }
                }, 
                // SNS 로그인 버튼 클릭
                snsLogin : function(snsType) {
                    var self = this;
                    this.error = '';
                    
                    // 페이스북
                    if (snsType == 'facebook') {
                        // var FB_SCOPE = 'email, publish_actions, user_friends';
                        var FB_SCOPE = 'email, public_profile';
                        // FACEBOOK-1 : FACEBOOK LOGIN
                        FB.login(function(response) {
                            var status = response && response.status;
                            if (status === 'connected') {
                                // FACEBOOK-2 : GET USER INFO
                                self.getFacebookUserInfo(response);
                            } else if (status === 'not_authorized') {
                                noti.open('페이스북 로그인을 취소하셨거나 실패하였습니다.');
                            } else {
                                noti.open('페이스북 로그인을 실패하였습니다.');
                            }
                        }, { scope: FB_SCOPE }); 
                    
                    // 네이버
                    } else if (snsType == 'naver') {
                        var state = naverLogin.getUniqState();
                        naverLogin.setDomain(self.snsInfo.naverAppDomain);
                        naverLogin.setState(state);
                        naverLogin.redirect_uri = self.snsInfo.naverAppLoginRedirectUrl;
                        // NAVER-1 : NAVER LOGIN
                        window.open(naverLogin.getNaverIdLoginLink(), '_self');
                        /* snsPopup = window.open(
                                        naverLogin.getNaverIdLoginLink(),
                                        'snsPopup',
                                        'width=800, height=800, top=100, left=100, fullscreen=no, menubar=no, status=no, toolbar=no, titlebar=yes, location=no, scrollbar=no'); */           
                    }
                },
                // FACEBOOK-2 : GET USER INFO
                getFacebookUserInfo : function(status) {
                    var self = this;
                    FB.api('/me', { fields: 'name, email, picture' }, function(response) {
                        self.request.snsId = status.authResponse.userID;
                        self.request.snsLogin = 'Y';
                        self.request.memSnsToken = status.authResponse.accessToken;
                        self.request.memEmail = response.email;
                        self.request.snsType = '1';
                        self.login();
                    }); 
                },
                // 로그인
                login : function() {
                    var self = this;
                    this.error = '';
                    axios.post('/member/login', this.request)
                        .then(function(response){
                            if(response.data.rCode == "0000") {
                                localStorage.setItem('user', JSON.stringify(response.data.rData));

                                // if (self.getParameterByName('returnType') == '97') {
                                //     localStorage.setItem('return', '/sc/gipf');
                                // } else if (self.getParameterByName('returnType') == 'foodshow') {
                                //     localStorage.setItem('return', '/sc/foodshow');
                                // } else if (self.getParameterByName('returnType') == 'bixpo2018') {
                                //     localStorage.setItem('return', 'https://bixpo2018.ycrowdy.com');
                                // }

                                var returnUrl = localStorage.getItem('return');
                                if (returnUrl && self.notReturn.indexOf(returnUrl) < 0) {
                                    window.open(returnUrl, '_self');
                                } else {
                                    window.open('/', '_self');
                                }
                            } else {
                                self.error = response.data.rMsg
                                self.resetRequest();
                            }
                        })
                },
                // 로그인 실패 시 RESET
                resetRequest : function() {
                    var self = this;
                    self.request.snsLogin = 'N';
                    self.request.memPwd = "";
                    self.request.memSnsToken = "";
                    self.request.snsId = "";
                },
                // NAVER-2 : NAVER OAUTH 확인
                checkNaverOauth : function() {
                    var self = this;
                    window.loginInfo = self;
                    if (naverLogin.oauthParams.access_token != undefined) {
                       naverLogin.get_naver_userprofile("loginInfo.naverSignInCallback()");
                    }
                },
                // NAVER-3 : GET USER INFO
                naverSignInCallback : function() {
                    var self = this;
                    self.request.snsId =  naverLogin.getProfileData('id');
                    self.request.memEmail =  naverLogin.getProfileData('email');
                    self.request.snsLogin = 'Y';
                    self.request.memSnsToken = naverLogin.oauthParams.access_token;
                    self.request.snsType = '5';
                    self.login();
                },
                write : function(type) {
                    if (type == 'email') {
                        this.isEmailStart = false;
                    } else {
                        this.isPwdStart = false;
                    }
                },
                getParameterByName : function(name, url) {
                    if (!url) url = window.location.href;
                    name = name.replace(/[\[\]]/g, '\\$&');
                    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
                        results = regex.exec(url);
                    if (!results) return null;
                    if (!results[2]) return '';
                    return decodeURIComponent(results[2].replace(/\+/g, ' '));
                }

            }//method 끝
        }
    }
}

export default new MemberLogin()