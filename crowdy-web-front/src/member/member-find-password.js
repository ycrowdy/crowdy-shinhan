class MemberFindPassword {
	component() {
		return {
			template : `
				<div class="list_wrap none_style">
					<div class="container">
						<div class="list_title webfont2">
							비밀번호 찾기
						</div>
						<div class="list_summury">
							CROWDY에 가입하실 때 사용하신 이메일을 입력하여 주시면, <br class="hidden-xs" />
							해당 이메일로 비밀번호를 새로 만들 수 있는 링크를 보내드립니다.
						</div>

						<form class="form-horizontal">
							<div class="form-group row-mobile-n">
								<label for="email" class="col-sm-1 control-label hidden-xs">
									<div class="text-left">이메일</div>
								</label>
								<div class="col-sm-4 col-md-3">
									<input type="email" name="memEmail" v-validate="'required|email'" v-model.trim="request.memEmail" class="form-control" :class="{'error' : errors.has('memEmail')}" placeholder="이메일" autofocus />
									<label class="error" v-if="errors.has('memEmail')" v-text="errors.first('memEmail')"></label>
								</div>
							</div>
						</form>
						<hr class="hidden-xs" />

						<button class="btn btn-lg m-btn-block btn-primary" v-on:click="find">임시 비밀번호 받기</button>

						<hr class="hidden-sm hidden-md hidden-lg" />

						<div class="form-info">
							<span>임시비밀번호</span>로 로그인 하신 후, 계정 설정을 통해 <span>비밀번호</span>를 변경하여 주시기 바랍니다. <br class="hidden-xs" />
							<div v-if="afterSendLink">
							<span>링크를 발송</span>했습니다. <br class="hidden-sm hidden-md hidden-lg" />
							이메일을 확인하시고 <span>비밀번호를 재설정 해주세요.</span>
							</div>
						</div>
					</div>
				</div>
			`,
			data : function() {
				return {
					request : {
						memEmail : ''
					},
					afterSendLink : false
				}
			},
			methods : {
				findPassword : function() {
					var self = this;
            		axios.post('/set/member/password-init', this.request)
            			.then(function(response){
                			var result = response.data;
	                        if(result.rCode == "0000") {
	                            noti.open("입력하신 이메일로<br/>비밀번호를 재 설정할수있는 링크를 발송해드렸습니다.");
	                            self.afterSendLink = true;
	                        } else {
	                            noti.open(result.rMsg);
	                            self.afterSendLink = false;
	                        }
        				})
				},
				find : function() {
					var self = this;
					this.$validator.validateAll()
                        .then(function(success) {
                            if(!success) return;
                            self.findPassword();
                        })
				}
			}

		}
	}
}

export default new MemberFindPassword()