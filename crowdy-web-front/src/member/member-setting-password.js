class MemberSettingPassword {
	component() {
		return {
			template : `
				<div class="container">
					<div class="list_title webfont2">
						비밀번호 재설정
					</div>
					<div class="list_summury">
						새로 사용하기 희망하는 비밀번호를 <br />
						(아래 재입력까지) 입력해 주세요.
					</div>

					<form class="form-horizontal">
						<div class="row not-space">
							<div class="col-sm-8">
								<div class="form-group row-mobile-n">
									<div class="col-sm-6 col-md-4">
										<input type="password" name="memPwd" v-validate="'required|min:8'" v-model="request.memPwd" class="form-control" :class="{'error' : errors.has('memPwd')}" placeholder="비밀번호" />
										<label class="error" v-if="errors.has('memPwd')" v-text="errors.first('memPwd')"></label>
									</div>
								</div>
							</div>
						</div>

						<div class="row not-space">
							<div class="col-sm-8">
								<div class="form-group row-mobile-n mb0">
									<div class="col-sm-6 col-md-4">
										<input type="password" name="memPwdConfirm" v-validate="'required|confirmed:memPwd'" v-model="request.memPwdConfirm" class="form-control" :class="{'error' : errors.has('memPwdConfirm')}" placeholder="비밀번호 확인" />
                                        <label class="error" v-if="errors.has('memPwdConfirm')" v-text="errors.first('memPwdConfirm')"></label>
									</div>
								</div>
							</div>
						</div>
					</form>

					<hr class="hidden-xs" />

					<a class="btn btn-lg m-btn-block btn-primary" v-on:click="change">변경</a>
				</div>
			`,
			props : ['tokenData'],
			data : function() {
				return {
					request : {
						token : this.tokenData,
						memPwd : ""
					}
				}
			},
			methods : {
				change : function() {
					var self = this;
					this.$validator.validateAll()
						.then(function(sucess) {
							if(!sucess) return;

							if(!/^.*(?=.{8,20})(?=.*[0-9])(?=.*[a-zA-Z]).*$/.test(self.request.memPwd)){
		                        noti.open('숫자와 영문자 조합으로 8자리 이상을 사용해야 합니다.');
		                        return false;
		                    }

							$('.page-loader-more').fadeIn('')
							axios.post('/set/member/password-init-update', self.request)
								.then(function(response) {
									$('.page-loader-more').fadeOut('')
	                    			var result = response.data;

	                    			if(result.rCode == "0000") {
			                    		noti.open("변경되었습니다. <br/> 다시 로그인해주세요.", function() {window.open("/", '_self');})
			                    	} else {
			                    		noti.open(result.rMsg)
			                    	}
								})
						})
				}
			}
		}
	}
}

export default new MemberSettingPassword()