class MemberJoin {
    component() {
        return {
            template : `
                <div class="member_frame">
                    <div class="mcreate_lpopup" :style="{width:popupSize + 'px'}">

                        <div class="list_title_member webfont2">
                            회원가입<br class="hidden-xs" />
                            기본정보 입력
                        </div>

                        <ul class="nav nav-tabs nav-justified" role="tablist">
                            <li role="presentation" class="active"><a href="javascript:void(0)" aria-controls="individual" role="tab" data-toggle="tab" v-on:click="typeChange(1)">개인회원</a></li>
                            <li role="presentation"><a href="javascript:void(0)" aria-controls="corporation" role="tab" data-toggle="tab" v-on:click="typeChange(2)">법인회원</a></li>
                            <li role="presentation"><a href="javascript:void(0)" aria-controls="combination" role="tab" data-toggle="tab" v-on:click="typeChange(3)">조합회원</a></li>
                        </ul>

                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active">
                                <form class="form-horizontal">

                                    <div>
                                        <div style="height:25px;margin-top:8px;margin-bottom:8px">
                                            <label class="chk_container">전체동의
                                              <input type="checkbox" v-model="computedCheckAllTerms">
                                              <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        
                                        <div style="height:25px;margin-top:8px;margin-bottom:8px">
                                            <label class="chk_container col-xs-10 col-sm-10">회원가입 기본약관(필수)
                                              <input type="checkbox" v-model="checkTerms1">
                                              <span class="checkmark"></span>
                                            </label>

                                            <div class="col-xs-2 col-sm-2 show_member_join_terms" v-on:click="showTerms(1)">
                                                <span class="show_member_join_terms_caret"></span>
                                            </div>
                                        </div>

                                        <div class="member_join_terms" v-if="showTerms1" v-html="readTerm('1')"></div>

    
                                        <div style="height:25px;margin-top:8px;margin-bottom:8px">
                                            <label class="chk_container col-xs-10 col-sm-10">CROWDY 이용약관(필수)
                                              <input type="checkbox" v-model="checkTerms2">
                                              <span class="checkmark"></span>
                                            </label>

                                            <div class="col-xs-2 col-sm-2 show_member_join_terms" v-on:click="showTerms(2)">
                                                <span class="show_member_join_terms_caret"></span>
                                            </div>
                                        </div>

                                        <div class="member_join_terms" v-if="showTerms2" v-html="readTerm('2')"></div>

                                        <div style="height:25px;margin-top:8px;margin-bottom:8px">
                                            <label class="chk_container col-xs-10 col-sm-10">개인정보 취급방침(필수)
                                              <input type="checkbox" v-model="checkTerms3">
                                              <span class="checkmark"></span>
                                            </label>

                                            <div class="col-xs-2 col-sm-2 show_member_join_terms" v-on:click="showTerms(3)">
                                                <span class="show_member_join_terms_caret"></span>
                                            </div>
                                        </div>

                                        <div class="member_join_terms" v-if="showTerms3" v-html="readTerm('4')"></div>
    
                                        <div style="height:25px;margin-top:8px;margin-bottom:8px">
                                            <label class="chk_container col-sm-12">크라우디의 소식과 다양한 안내(선택)
                                              <input type="checkbox" v-model="checkTerms4" v-on:click="checkTerms(4)" checked="checked">
                                              <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                
                                    <!-- 개인 이름 -->
                                    <div class="form-group mt15" v-if="request.memType == 1">
                                        <div class="col-xs-12">
                                            <input type="text" class="form-control" name="memName" v-validate="'required'" v-model="request.memName" :class="{'error' : errors.has('memName')}" placeholder="이름" />
                                            <label class="error" v-if="errors.has('memName')" v-text="errors.first('memName')"></label>
                                        </div>
                                    </div>
                                    <!-- //개인 이름 -->
                                    
                                    <!-- 이메일 -->
                                    <div class="form-group row-mobile-n">
                                        <div class="col-xs-12">
                                            <input type="email" name="memEmail" v-validate="'required|email'" v-model="request.memEmail" class="form-control" :class="{'error' : errors.has('memEmail')}" placeholder="이메일(ID)" />
                                            <label class="error" v-if="errors.has('memEmail')" v-text="errors.first('memEmail')"></label>
                                        </div>
                                    </div>
                                    <!-- 이메일 -->
                                    
                                    <!-- 비밀번호 -->
                                    <div class="form-group row-mobile-n">
                                        <div class="col-xs-12">
                                            <input type="password" name="memPwd" v-validate="'required|min:8'" v-model="request.memPwd" class="form-control" :class="{'error' : errors.has('memPwd')}" placeholder="비밀번호(8자 이상의 글자+숫자 조합)" />
                                            <label class="error" v-if="errors.has('memPwd')" v-text="errors.first('memPwd')"></label>
                                        </div>
                                    </div>
                                    <!-- //비밀번호 -->

                                    <!-- 비밀번호 확인 -->
                                    <div class="form-group row-mobile-n">
                                        <div class="col-xs-12">
                                            <input type="password" name="memPwdConfirm" v-validate="'required|confirmed:memPwd'" v-model="request.memPwdConfirm" class="form-control" :class="{'error' : errors.has('memPwdConfirm')}" placeholder="비밀번호 확인" />
                                            <label class="error" v-if="errors.has('memPwdConfirm')" v-text="errors.first('memPwdConfirm')"></label>
                                        </div>
                                    </div>
                                    <!--// 비밀번호 확인-->
                                    
                                    <!-- 법인/조합 회원 -->
                                    <template v-if="request.memType != 1">
                                    <div class="form-group row-mobile-n">
                                        <div class="col-xs-12">
                                            <input type="text" class="form-control" :class="{'error' : errors.has('memName')}" name="memName" v-validate="'required'" v-model="request.memName" v-bind:placeholder="placeCompanyName" />
                                            <label class="error" v-if="errors.has('memName')" v-text="errors.first('memName')"></label>
                                        </div>
                                    </div>

                                    <div class="form-group row-mobile-n">
                                        <div class="col-xs-12">
                                            <input type="tel" class="form-control" :class="{'error' : errors.has('bizNum')}" name="bizNum" v-validate="'required'" v-model="request.bizNum" v-bind:placeholder="placeCompanyNum+'(숫자만 입력)'" />
                                            <label class="error" v-if="errors.has('bizNum')" v-text="errors.first('bizNum')"></label>
                                        </div>
                                    </div>

                                    <div class="form-group row-mobile-n">
                                        <div class="col-xs-12">
                                            <input type="text" class="form-control" :class="{'error' : errors.has('resName')}" name="resName" v-validate="'required'" v-model="request.resName" placeholder="담당자 명" />
                                            <label class="error" v-if="errors.has('resName')" v-text="errors.first('resName')"></label>
                                        </div>
                                    </div>
                                    </template>
                                    <!-- //법인/조합 회원 -->

                                    <div class="form-group mt30 mb0">
                                        <div class="col-xs-12">
                                            <button type="button" class="btn btn-lg btn-block btn-primary" v-on:click="emailJoin">회원가입</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <hr class="big" />

                        <div class="sns_member_wrap" v-show="request.memType == 1">
                            <div class="sns_login_info">SNS 계정으로 회원가입</div>
                            <div class="sns_login_btn">
                                <ul>
                                    <li class="nth-child1">
                                        <a v-on:click="snsJoin('facebook')">페이스북</a>
                                    </li>
                                    <!-- <li class="nth-child2">
                                        <a href="javascript:void(0)">카카오톡</a>
                                    </li> -->
                                    <li class="nth-child3">
                                        <a v-on:click="snsJoin('naver')">네이버</a>
                                    </li>
                                    <!-- <li class="nth-child4">
                                        <a href="javascript:void(0)">구글</a>
                                    </li> -->
                                </ul>
                            </div>
                        </div>

                    </div>

                    <div id="email-add-modal" class="modal fade" tabindex="-1" role="dialog" style="z-index: 9999;" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog modal-sm" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" aria-label="Close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                                </div>
                                <div class="modal-body modal-order">
                                    <p>휴대폰 번호로 가입한 SNS계정은 <br /> 이메일을 추가적으로 입력해주셔야합니다.</p>
                                    <div class="form-group row-mobile-n">
                                        <div class="col-xs-12 mb20">
                                            <input type="email" name="memEmail" v-validate="'required|email'" v-model="request.memEmail" class="form-control" :class="{'error' : errors.has('memEmail')}" placeholder="이메일(ID)" />
                                            <label class="error" v-if="errors.has('memEmail')" v-text="errors.first('memEmail')"></label>
                                        </div>
                                    </div>
                                    <div class="modal-footer text-center">
                                        <div class="row not-space">
                                            <div class="col-xs-4 col-xs-offset-1">
                                                <button type="button" class="btn btn-block btn-primary-outline" v-on:click="emailPopupJoin">확인</button>
                                            </div>
                                            <div class="col-xs-4 col-xs-offset-2">
                                                <button type="button" class="btn btn-block btn-primary-outline" data-dismiss="modal">취소</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            `,
            data: function() {
                return {
                    placeCompanyName: '법인명',
                    placeCompanyNum : '사업자등록번호',
                    request : {
                        memName : '',
                        memType : 1,
                        memEmail : '',
                        memPwd : '',
                        memPwdConfirm : '',
                        sns : 'N',
                        snsType : '',
                        snsId : '',
                        memShotImg : '',
                        snsLogin : 'N',
                        memSnsToken : '',
                        bizNum : "",
                        resName : "",
                        marketingAgree : 'Y'
                    }, 
                    showTerms1 : false,
                    showTerms2 : false,
                    showTerms3 : false,
                    checkAllTerms : false,
                    checkTerms1 : false,
                    checkTerms2 : false,
                    checkTerms3 : false,
                    checkTerms4 : true,
                    snsInfo : {},
                    error : "",
                    terms : [],
                    notReturn : [
                        "/user/login",
                        "/user/join"
                    ]
                }
            },
            created: function() {
                // SNS 회원가입을 위한 정보 Get
                this.getSnsInfo();
                // NAVER-2 : NAVER OAUTH 확인 (redirectUrl 접속 시)
                this.checkNaverOauth();
                this.loadTerms();
            },
            computed : {
                computedCheckAllTerms : {
                    get : function() {
                        if(this.checkTerms1 == true && this.checkTerms2 == true && this.checkTerms3 == true && this.checkTerms4 == true) {
                            return true;
                        } else {
                            return false;
                        }
                    },
                    set : function(value) {
                        this.checkAllTerms = value;
                        if(this.checkAllTerms) {
                            this.checkTerms1 = true;
                            this.checkTerms2 = true;
                            this.checkTerms3 = true;
                            this.checkTerms4 = true;
                            this.request.marketingAgree = 'Y';
                        } else {
                            this.checkTerms1 = false;
                            this.checkTerms2 = false;
                            this.checkTerms3 = false;
                            this.checkTerms4 = false;
                            this.request.marketingAgree = 'N';
                        }    
                    }
                },
                popupSize : function() {
                    return window.innerWidth;
                }

            },
            methods : {
                loadTerms : function() {
                    var self = this;
                    axios.post('/data/crowdy/term', {})
                        .then(function(response) {
                            var result = response.data;
                            self.terms = result.rData;
                        })
                },
                // 개인/법인/조합 선택
                typeChange : function(val) {
                    this.request.memType = val;
                    if(this.request.memType == 2) { 
                        this.placeCompanyName = '법인명'
                        this.placeCompanyNum = '사업자등록번호'
                    } else if(this.request.memType == 3) {
                        this.placeCompanyName = '조합명'
                        this.placeCompanyNum = '고유번호'
                    }
                },
                // SNS 회원가입을 위한 정보 Get
                getSnsInfo : function() {
                    var self = this;
                    axios.post('/member/getSnsInfo', this.request)
                    .then(function(response){
                        if(response.data.rCode == "0000") {
                            self.snsInfo = response.data.rData;
                        } else {
                            self.error = response.data.rMsg
                        }
                    })
                },
                // 회원가입 버튼 클릭
                emailJoin : function() {
                    var self = this;
                    self.request.sns = 'N';
                    self.request.snsType = '0';

                    if(this.checkTerms1 == false || this.checkTerms2 == false || this.checkTerms3 == false) {
                        noti.open("회원가입을 위해 필수 이용약관 동의가 필요합니다.")
                        return;
                    }

                    if(!/^.*(?=.{8,20})(?=.*[0-9])(?=.*[a-zA-Z]).*$/.test(this.request.memPwd)){
                        noti.open('비밀번호는 숫자와 영문자 조합으로 8자리 이상을 사용해야 합니다.');
                        return false;
                    }

                    if(!/^[0-9]*$/.test(this.request.bizNum)) {
                        noti.open(this.placeCompanyNum + '는 숫자만 입력해주세요.');
                        return;
                    }

                    this.$validator.validateAll()
                        .then(function(success) {
                            if(!success) return;

                            if (self.validateEmail(self.request.memEmail)) {
                                // 회원가입 API 
                                self.join('email');
                            } else {
                                noti.open('잘못된 이메일 형식입니다.');
                            }
                        })
                },
                validateEmail : function(email) {
                    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    return re.test(email);
                },
                // SNS 회원가입 버튼 클릭
                snsJoin : function(snsType) {
                    var self = this;
                    this.error = '';
                    
                    if(this.checkTerms1 == false || this.checkTerms2 == false || this.checkTerms3 == false) {
                        noti.open("회원가입을 위해 필수 이용약관 동의가 필요합니다.")
                        return;
                    }
                    
                    // 페이스북
                    if (snsType == 'facebook') {
                        // var FB_SCOPE = 'email, publish_actions, user_friends';
                        var FB_SCOPE = 'email, public_profile';
                        // FACEBOOK-1 : FACEBOOK LOGIN
                        FB.login(function(response) {
                            var status = response && response.status;
                            if (status === 'connected') {
                                // FACEBOOK-2 : GET USER INFO
                                self.getFacebookUserInfo(response);
                            } else if (status === 'not_authorized') {
                                noti.open('페이스북 가입을 취소하셨거나 실패하였습니다.');
                            } else {
                                noti.open('페이스북 가입을 실패하였습니다.');
                            }
                        }, { scope: FB_SCOPE });

                    // 네이버
                    } else if (snsType == 'naver') {
                        var state = naverLogin.getUniqState();
                        naverLogin.setDomain(self.snsInfo.naverAppDomain);
                        naverLogin.setState(state);
                        naverLogin.redirect_uri = "https://ycrowdy.com/user/join"; //self.snsInfo.naverAppJoinRedirectUrl;
                        // NAVER-1 : NAVER LOGIN
                        window.open(naverLogin.getNaverIdLoginLink(), '_self');
                    }
                },
                // FACEBOOK-2 : GET USER INFO
                getFacebookUserInfo : function(status) {
                    var self = this;
                    FB.api('/me', { fields: 'name, email, picture' }, function(response) {

                        self.request = {
                            memName : response.name,
                            memType : 1,
                            memEmail : response.email,
                            memPwd : '',
                            memPwdConfirm : '',
                            sns : 'Y',
                            snsType : '1',
                            snsId : response.id,
                            memShotImg : 'graph.facebook.com/' + response.id + '/picture?type=large',
                            snsLogin : 'Y',
                            memSnsToken : status.authResponse.accessToken
                        }

                        if (response.email == '' || typeof(response.email) == "undefined") {
                            $('#email-add-modal').modal('show');
                            return;
                        }

                        self.join('facebook');
                    }); 
                },
                emailPopupJoin : function() {
                    var self = this;
                    if (this.request.memEmail != '' && typeof(this.request.memEmail) != "undefined") {
                        self.join('facebook');
                    }
                },
                // 회원가입 API 
                join : function(snsType) {
                    var self = this;
                    this.error = '';
                    
                    axios.post('/set/member/insert', this.request)
                        .then(function(response){
                            var result = response.data;
                            if(result.rCode == "0000") {

                                // gtm 회원 가입 이벤트 발생
                                window.dataLayer.push({
                                    'event': 'RegistrationComplete',
                                    'sns_type': snsType
                                });

                                // 회원가입 성공 시 로그인
                                self.login();
                            } else {
                                noti.open(result.rMsg);
                                self.resetRequest(self.request.memType);
                            }
                    }) 
                },
                // 회원가입 성공 시 로그인
                login : function() {
                    var self = this;
                    this.error = '';
                    axios.post('/member/login', this.request)
                        .then(function(response){
                            if(response.data.rCode == "0000") {
                                localStorage.setItem('user', JSON.stringify(response.data.rData));
                                var returnUrl = localStorage.getItem('return');
                                if (returnUrl && self.notReturn.indexOf(returnUrl) < 0) {
                                    window.open(returnUrl, '_self');
                                } else {
                                    window.open('/', '_self');
                                }
                            } else {
                                self.error = response.data.rMsg
                                self.resetRequest(self.request.memType);
                            }
                            
                        })
                },
                // 회원가입 실패 시 RESET
                resetRequest : function(val) {
                    var self = this;

                    self.request = {
                        memName : '',
                        memType : val,
                        memEmail : '',
                        memPwd : '',
                        memPwdConfirm : '',
                        sns : 'N',
                        snsType : '',
                        snsId : '',
                        memShotImg : '',
                        snsLogin : 'N',
                        memSnsToken : '',
                        bizNum : "",
                        resName : "",
                        marketingAgree : 'Y'
                    }
                },
                // NAVER-2 : NAVER OAUTH 확인
                checkNaverOauth : function() {
                    var self = this;
                    window.joinInfo = self;
                    if (naverLogin.oauthParams.access_token != undefined) {
                        naverLogin.get_naver_userprofile("joinInfo.naverSignInCallback()");
                    }
                },
                // NAVER-3 : GET USER INFO
                naverSignInCallback : function() {
                    var self = this;
                    self.request = {
                            memName : naverLogin.getProfileData('name'),
                            memType : 1,
                            memEmail : naverLogin.getProfileData('email'),
                            memPwd : '',
                            memPwdConfirm : '',
                            sns : 'Y',
                            snsType : '5',
                            snsId : naverLogin.getProfileData('id'),
                            memShotImg : naverLogin.getProfileData('profile_image'),
                            snsLogin : 'Y',
                            memSnsToken : naverLogin.oauthParams.access_token
                        }
                    self.join('naver');
                },
                checkTerms : function(mode) {
                    if(mode == 4) {
                        if(this.request.marketingAgree == 'Y') {
                            this.request.marketingAgree = 'N';
                        } else {
                            this.request.marketingAgree = 'Y';
                        }
                    }
                },
                showTerms : function(mode) {
                    if(mode == 1) {
                        if(this.showTerms1 == false) {
                            this.showTerms1 = true;
                        } else {
                            this.showTerms1 = false;
                        }
                    } else if(mode == 2) {
                        if(this.showTerms2 == false) {
                            this.showTerms2 = true;
                        } else {
                            this.showTerms2 = false;
                        }
                    } else if(mode == 3) {
                        if(this.showTerms3 == false) {
                            this.showTerms3 = true;
                        } else {
                            this.showTerms3 = false;
                        }
                    }
                },
                readTerm : function(type) {
                    var data = _.find(this.terms, {'type' : type})
                    return data ? data.content : ''
                }
            }//method 끝
        }
    }
}

export default new MemberJoin()