class RewardComment {
    component() {
        return {
            template : `
            <div>
                <div id="comment-collapse">
                    <div class="mp-body">
                    <!-- 댓글작성 & 댓글목록 -->
                    <!-- 댓글작성 -->
                    <form class="form-horizontal pt15 xs-pt10 xs-pl5 xs-pr5">
                        <div class="form-group row-mobile-n mb10">
                            <div class="col-sm-2 hidden-xs">
                                <div class="comment-object"><img v-if="userImg != ''" :src="'//' + userImg + '/ycrowdy/resize/!160x!160'" /></div>
                            </div>
                            <div class="col-sm-10">
                                <textarea rows="3" cols="25" v-model="commentSaveData.crpyContent" name="crpyContent" class="textarea-form-control form-control" v-validate="'required|max:500'" v-on:click="commentClick" :placeholder="name + '님! 프로젝트 성공하시길 바래요!'"></textarea>
                                <label class="error" v-if="errors.has('crpyContent')" v-text="errors.first('crpyContent')"></label>
                            </div>
                        </div>

                        <div class="form-group row-mobile-n mb35 xs-mb10 xs-pb3">
                            <div class="col-sm-2"></div>
                            <div class="col-xs-4 col-sm-2">
                                <span class="textarea_text_leng xs-mt5"><span v-text="commentSaveData.crpyContent.length"></span> / 500</span>
                            </div>
                            <div class="col-xs-8 text-right">
                                <a href="javascript:void(0)" class="btn btn-primary" id="complete_pay" v-on:click="commentRegit('c')">댓글등록하기</a>
                            </div>
                        </div>
                    </form>
                    <!-- //댓글작성 -->
                    </div>
                </div>

                <!-- 댓글목록 -->
                <div class="comments mb30">
                    <!-- Loop -->
                    <div class="comment media" v-if="commentData.length > 0" v-for="(item, index) in commentData">
                        <div class="media-left pr20 m-pr10">
                            <div class="reply-object" href="javascript:void(0)"><img v-if="item.memShotImg != ''" :src="'//' + item.memShotImg + '/ycrowdy/resize/!160x!160'" /></div>
                        </div>
                        <div class="comment-body media-body">
                            <a href="javascript:void(0)" class="comment-author">{{ item.memName }}</a>
                            <template v-if="item.crpyStatus == 1">
                                <div class="comment-content" v-html="$options.filters.commentFilter(item.crpyContent)"></div>
                            </template>
                            <template v-if="item.crpyStatus == 2">
                                <div class="comment-content">해당 댓글은 삭제되었습니다.</div>
                            </template>
                            <div class="comment-meta">
                                {{ item.wdate }}
                            </div>
                            <a class="blue-800" :href="'#'+item.crpyIdx" role="button" data-toggle="collapse" aria-expanded="false" :aria-controls="item.crpyIdx">답글달기</a>
                            <!-- 답글 작성 -->
                            <form :id="item.crpyIdx" class="collapse comment-reply">
                                <div class="mb10">
                                    <textarea class="textarea-form-control form-control" v-model="commentSaveData.crpyContentRe" v-on:click="replyClick(item.crpyIdx)" maxlength="300" rows="3" cols="25" placeholder="모든 댓글은 크라우드펀딩법에 따라 수정 및 삭제가 불가능하므로 신중히 써주시기 바랍니다."></textarea>
                                </div>
                                <div class="text-right">
                                    <a class="btn btn-sm btn-default-outline" :href="'#'+item.crpyIdx" role="button" data-toggle="collapse" aria-expanded="false" :aria-controls="item.crpyIdx">답글취소</a>
                                    <a href="javascript:void(0)" role="button" data-toggle="collapse" aria-expanded="false" v-on:click="commentRegit('r')" class="btn btn-sm btn-primary-outline">답글등록</a>
                                </div>
                            </form>
                            <!-- //답글 작성 -->
                            <reply :param-reply = "replyData" :param-comment-idx = "commentData[index].crpyIdx" :reply-change="replyChange"></reply>
                        </div>
                    </div>
                </div>   
                
                <div class="text-center mb40">
                    <a href="javascript:void(0)" id="comment_list_more" v-show="moreShow" v-on:click="more()" class="btn btn-primary-outline">더보기</a>
                </div>
            </div>
            `,
            props: {
                cpCode : {
                    default : '',
                },
                crpyPidx : {
                    default : '',
                },
                paramSearchCount : {
                    default : "10"
                },
                training : {
                    default : false
                },
                name : {
                    default : '',
                },
            },
            data: function() {
                return {
                    commentData : [],
                    replyData : [],
                    commentParam : {
                        cpCode: this.cpCode,
                        //crpyPidx: this.crpyPidx,
                        paging : {
                            page : "1",
                            count : this.paramSearchCount
                        }
                    },
                    commentSaveData : {
                        cpCode : this.cpCode,
                        memCode : userInfo.memCode,
                        crpyPidx : 0,
                        crpyDeps : 0,
                        crpyContent : '',
                        crpyContentRe : '',
                    },
                    moreShow : true,
                    refresh : true,
                    replyChange : 1,
                }
            },
            components : {
                reply : require('./reward-reply.js').default.component(),
            },
            computed : {
                userImg : function() {
                    return userInfo.image;
                }
            },
            created: function() { 
                this.comment('regit');
            },
            filters : {
                commentFilter : function(val) {
                    return val.replace( /[\n]/g, "<br/>")
                }
            },
            methods: {
                //댓글 로드
                comment : function(data) {
                    var self = this;

                    if(_.isEqual('regit', data)) {
                        self.commentParam.paging.page = "1";
                        self.commentParam.paging.count = 10;
                    }
                    axios.post('/data/view/reward/comment-list', self.commentParam)
                    .then(function (response) {
                        if(_.isEqual('regit', data)) {
                            self.commentData = response.data.rData.comment;
                            self.replyData = response.data.rData.reply;
                        } else {
                            self.commentData = _.concat(self.commentData, response.data.rData.comment);
                            self.replyData = _.concat(self.replyData, response.data.rData.reply);
                        }
                        if(!self.moreEnd() || response.data.rData.comment.length == 0) {
                            self.moreShow = false;
                        }
                    })
                },
                //댓글, 답글등록
                commentRegit : function(type) {
                    if(!userInfo.loginConfirm()) return;
                        var self = this;
                        //답글 등록할 때
                        if(type == 'r') {
                            self.commentSaveData.crpyContent = self.commentSaveData.crpyContentRe;
                        }
                        this.$validator.validateAll()
                        .then(function(success) {
                            if(!success) return;
                            //등록
                            axios.post('/set/save/reward/set-comment', self.commentSaveData)
                            .then(function (response) {
                                var result = response.data;
                                if(result.rCode == "0000") {
                                    noti.open('등록되었습니다.');
                                    self.commentSaveData.crpyContentRe = '';
                                    self.commentSaveData.crpyContent = '';
                                    self.commentData = [];
                                    self.replyData = [];
                                    self.comment('regit');
                                } else {
                                    noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
                                }                                  
                            })
                        })
                },
                //답글등록시 셋팅
                replyClick : function(data) {
                    var self = this;
                    if(!userInfo.loginConfirm()) return;
                    self.commentSaveData.crpyPidx = data;
                    self.commentSaveData.crpyDeps = 1;
                },
                //더보기
                more : function() {
                    var self = this;
                    this.commentParam.paging.page  = _.toString(_.add(_.toNumber(this.commentParam.paging.page), 1));
                    this.comment();
                    self.replyChange++;
                },
                moreEnd : function() {
                    return (this.commentData.length % this.commentParam.paging.count == 0) ? true : false;
                },
                commentClick : function() {
                    this.$nextTick(function() {
                        this.commentSaveData.crpyPidx = 0;
                    });
                },
            }
        }
    }
}

export default new RewardComment()