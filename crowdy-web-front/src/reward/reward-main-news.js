class RewardMainNews {
	component() {
		return {
			template : `
				<div>
					<div class="row row-mobile">
						<div class="col-xs-6 col-sm-3 list_card_pre_open_height_parent" v-for="(item, index) in data">
							<a :href="'/r/' + item.cpAliasUrl" class="bbs-caption over-box list_card_pre_open_height">
								<div class="bbs-img">
									<img :src="'//' + item.cpCardImg + '/ycrowdy/resize/!370x!246'" class="img-responsive" />
								</div>
								<div class="bbs-subject" v-text="item.cpTitle"></div>
								<div class="bbs-summury" v-text="item.cbbsTitle"></div>
								<div class="bbs-date" v-text="item.cbbsWdate"></div>
							</a>
						</div>
					</div>

					<div  v-show="moreShow">
                        <a href="javascript:void(0)" v-on:click="more()" class="btn btn-primary-outline col-sm-8 col-md-6 col-xs-12 col-sm-offset-2 col-md-offset-3 btn-more text-center mt50 mb20 md-mt65 md-mb0 m-mt50 m-mb20 xs-mt20 xs-mb10">새소식 더보기</a>
                    </div>
				</div>
			`,
			props : {
	            paramSearchCount : {
	                default : "4"
	            }
	        },
			data : function() {
				return {
					data : [],
					search : {
						paging : {
	                        page : "1",
	                        count : this.paramSearchCount
                    	}
					},
					moreShow : true
				}
			},
			created : function() {
				this.load()
			},
			methods : {
				load : function() {
					var self = this;
	                axios.post('/data/view/reward/main/news', self.search)
	                    .then(function(response){
							self.data = _.concat(self.data, response.data.rData);
							if(response.data.rData.length != parseInt(self.search.paging.count)) {
                            	self.moreShow = false;
                        	}
	                    })
				},
				more : function() {
                    this.search.paging.page  = _.toString(_.add(_.toNumber(this.search.paging.page), 1));
                    this.load();
                },
			}
		}
	}
}

export default new RewardMainNews()