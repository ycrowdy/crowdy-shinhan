class RewardDetailStory {
    component() {
        return {
            template : `
            <div>
                <div class="detail_wrap">
                    <div class="container">
                        <div class="row row-mobile-n">
                            <div class="col-sm-8">
                                <!-- 동영상, 이미지 미리보기 -->
                                <ul id="lightSlider" class="xs-mt10" v-if="mediaConfirm">
                                    <template v-for="(item, index) in mediaData">
                                        <li v-if="item.cmscDiv == '1'" :data-thumb="item.src">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <iframe class="embed-responsive-item" :src="item.msrc" height="538" allowfullscreen></iframe>
                                            </div>
                                        </li>
                                        <li v-else="item.cmscDiv == '2'" :data-thumb="item.src">
                                            <img :src="item.msrc" class="img-responsive" />
                                        </li>
                                    </template>
                                </ul>
                                <!-- //동영상, 이미지 미리보기 -->
                                <!-- 모바일 -->
                                <div class="detail_order_info_mobile hidden-sm hidden-md hidden-lg">
                                    <div class="media">
                                        <div class="media-left media-middle">
                                            <div class="imgava" style="cursor: pointer;" v-on:click="detailShow"><img v-if="rewardDetailData.memShotImg != ''" :src="'//' + rewardDetailData.memShotImg" class="media-object" /></div>
                                        </div>
                                        <div class="media-body media-middle">
                                            <div class="row row-mobile-n">
                                                <div class="col-xs-6">
                                                    <a style="cursor: pointer;" v-on:click="detailShow"><strong>{{ rewardDetailData.memName }}</strong></a>
                                                </div>
                                                <div class="col-xs-6 text-right">
                                                    <!--<a href="javascript:void(0)" data-toggle="modal" data-target="#qaEmail">문의하기</a>-->
                                                    <!--<a class="ml10" style="cursor: pointer;" v-on:click="detailShow">자세히보기</a>-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- //모바일 -->
                            </div>
                            <info :reward-detail-data="rewardDetailData" :funding-url="fundingUrl" :traning="true" v-on:funding="funding" v-on:facebook-share="facebookShare"></info>
                        </div>
                        
                        <div class="clearfix"></div>
                        <hr class="big big_grey hidden-xs"/>
                        <div class="row row-mobile-n mt30 m-mt20">
                            <div class="col-sm-8">
                                <div class="mp-group">
                                    <!-- 새소식 추가 & 스토리 & 댓글 등 -->
                                    <div class="news_plus_btn xs-text-center">
                                        <a href="javascript:void(0)" v-if="rewardDetailData.memCode == memCode" class="hidden-xs" v-on:click="newsClick(rewardDetailData.cpCode, memCode)">새소식 추가하기 <i></i></a>
                                    </div>
                                    <!-- //새소식 추가 & 스토리 & 댓글 등 -->
                                    <!-- 새소식(신규) -->
                                    <div class="mp-panel">
                                        <div class="mp-heading" role="tab">
                                            <div class="mp-title" data-toggle="collapse" data-target="#rexplus_collapse" aria-expanded="false" aria-controls="rexplus_collapse">
                                                <strong>새소식</strong> <span>({{ rewardDetailData.cbCount }})</span>
                                            </div>
                                        </div>
                                        <div id="rexplus_collapse" class="mp-collapse in">
                                            <!-- Loop -->
                                            <news :cp-code="cpCode" :news-change="newsChange" :cb-count="rewardDetailData.cbCount"></news>
                                            <!-- //Loop -->
                                            <hr class="big hidden-xs">
                                        </div>
                                    </div>
                                    <!-- //새소식(신규) -->
                                    <!-- 스토리 콘텐츠 -->
                                    <div class="mp-panel">
                                        <div class="mp-heading" role="tab">
                                            <div class="mp-title" data-toggle="collapse" data-target="#story_collapse" aria-expanded="false" aria-controls="story_collapse">
                                                <strong>스토리</strong>
                                            </div>
                                        </div>
                                        <div id="story_collapse" class="mp-collapse in">
                                            <div class="mp-body">
                                                <div class="detail_contents mce-content-body" v-html ="rewardDetailData.cpStory">
                                                     
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="big hidden-xs" />
                                    <!-- //스토리 콘텐츠 -->
                                    <!-- 환불 및 교환 정책 기본사항 -->
                                    <div class="mp-panel" v-if="!traning">
                                        <div class="mp-heading" role="tab">
                                            <div class="mp-title collapsed" data-toggle="collapse" data-target="#rex_collapse" aria-expanded="false" aria-controls="rex_collapse">
                                                <strong>환불 및 교환 정책 기본사항</strong>
                                            </div>
                                        </div>
                                        <div id="rex_collapse" class="mp-collapse">
                                            <div class="mp-body">
                                                <div class="project_per">
                                                    <strong>본 프로젝트는</strong>
                                                    <!-- 목표달성형 -->
                                                    <ul v-if="rewardDetailData.cpFundType == '1'">
                                                        <li>100% 이상 모이면 리워드가 제공되며, 목표금액이 초과하여도 계속해서 펀딩이 가능합니다.</li>
                                                        <li>프로젝트 성공 시 결제가 이루어지며, 그 전에는 결제가 진행되지 않은 결제 예약 상태입니다. </li>
                                                        <li>펀딩에 참여했을지라도, 프로젝트가 목표미달성시 결제는 진행되지 않습니다.</li>
                                                    </ul>
                                                    <!-- //목표달성형 -->
                                                    <!-- 자율형 -->
                                                    <ul v-else>
                                                        <li>목표금액과 상관없이 펀딩에 참여한 모든분들께 리워드가 제공됩니다.</li>
                                                        <li>프로젝트 종료 시 결제가 이루어지며, 그 전에는 결제가 진행되지 않는 결제 예약 상태입니다.</li>
                                                    </ul>
                                                    <!-- //자율형 -->
                                                </div>
                                                <div class="detail_rex mt25 mb30 xs-mb30" v-if="rewardDetailData.cpRefundPolicy != '' && rewardDetailData.cpRefundPolicy != null">
                                                    <strong class="hidden-xs">진행자 환불 및 교환 정책</strong>
                                                    <div v-html="$options.filters.commentFilter(rewardDetailData.cpRefundPolicy)"></div>
                                                </div>
                                                <div class="detail_rex xs-mt10" style="margin-top: 20px;">
                                                    <strong class="hidden-xs">크라우디 환불 및 교환 정책</strong>
                                                    <ol>
                                                        <li>1. 프로젝트 기간 중에는 자유롭게 <a v-on:click="mypage" style="cursor: pointer;">마이 페이지</a>에서 펀딩 취소가 가능합니다.</li>
                                                        <li>2. 펀딩을 받아야만 생산을 시작할 수 있는 크라우드 펀딩 특성상, 프로젝트 종료 이후에는 단순 변심으로 인한 교환이나 환불이 불가하니 이점 양해 부탁드립니다.</li>
                                                        <li>3. 리워드 배송일이 예상보다 지연되는 경우, 새소식과 후원자 분들의 이메일을 통해 안내해드리겠습니다.<br />이에 관한 문의는 <a :href="'mailto:' + rewardDetailData.cpInquiryEmail"><b>이메일 "{{ rewardDetailData.cpInquiryEmail}}</b></a>" <a :href="'tel:' + phoneNumber" style="cursor: pointer;"><b>{{ phoneNumber }}</b></a> 로 연락바랍니다.</li>
                                                    </ol>
                                                </div>
                                                <div class="detail_rex_p">
                                                    * 프로젝트 종료일 이후에 리워드와 관련된 환불 및 교환은 프로젝트 제작자가 약속하는 것에 따르며 크라우디는 이에 책임지지 않습니다.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="big hidden-xs" />
                                    <!-- //환불 및 교환 정책 기본사항 -->
                                </div>
                                <!-- 주목 받는 프로젝트 -->
                                <div class="mt50 mb80 xs-mt30 xs-mb30 hidden-xs">
                                    <div class="notable-project-title" style="text-align: center;">
                                        지금 주목받는 프로젝트
                                    </div>
                                    <best :param-order-type="1"></best>
                                </div>
                                <!-- //주목 받는 프로젝트 -->
                            </div>
                            <div class="col-sm-4">
                                <div class="row not-space">
                                    <div class="col-md-1"></div>
                                    <div class="col-md-11">
                                        
                                        <!-- 판매중 리워드 -->
                                        <div class="clearfix"></div>
                                        <div class="st-items-wrap mt50">
                                            <div class="clearfix"></div>
                                            <!-- 커뮤니티 파트너일 경우 -->
                                            <template v-if="rewardDetailData.communityName != ''">
                                                <div class="comm_partners webfont2">
                                                    해당 프로젝트는<br />
                                                    <span class="blue-800">{{rewardDetailData.communityName}}</span> 과 함께합니다.
                                                </div>
                                            </template>
                                            <div class="row row-mobile">
                                                <!-- Loop -->
                                                <div class="st-items-title webfont2" v-if="rewardDetailData.cpEndStatus != 0">선택가능한 리워드</div>
                                                <div id="st-items1" class="col-xs-12" v-for="(item, index) in cardBenefitData" v-if="(item.cbfRemQty != 0 && item.cbfStatus == 1) || (rewardDetailData.cpEndStatus == 0)">
                                                    <div class="st-items st-items-loop">
                                                        <a v-on:click="funding(rewardDetailData.cpEndStatus)" class="st-link"></a>
                                                        <div class="row not-space">
                                                            <div class="col-xs-6 col-sm-12">
                                                                <div class="st-items-su">
                                                                    <strong><span class="webfont2">{{ parseInt(item.cbfAmount).toLocaleString() }}</span>원 펀딩</strong>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-6 col-sm-12 xs-text-right">
                                                                <div class="st-items-btn">
                                                                    <span class="btn btn-sm btn-danger" v-if="item.cbfRemQty > 89999">무제한</span>
                                                                    <span class="btn btn-sm btn-danger" v-if="item.cbfRemQty < 90000">{{ item.cbfRemQty }}개 남음</span>
                                                                    <span class="btn btn-sm btn-danger-outline">{{ parseInt(item.cpBenefitSpsQty).toLocaleString() }}개 펀딩</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <dl>
                                                                    <dt>리워드 제목</dt>
                                                                    <dd><strong>{{ item.cbfTitle }}</strong></dd>
                                                                </dl>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <p>{{ item.cbfInfo }}</p>
                                                            </div>
                                                            <div class="col-xs-6 col-sm-12" v-if="!traning">
                                                                <dl>
                                                                    <dt>예상 배송일</dt>
                                                                    <dd><strong>{{ item.cbfResDate }}</strong></dd>
                                                                </dl>
                                                            </div>
                                                        </div>
                                                        <!-- 옵션 -->
                                                        <div v-for="(items, index) in item.cpBenefitOptions">
                                                            <div class="st-items-option" v-if="item.cpBenefitOptions.length > 0">
                                                                <dl>
                                                                    <dt>옵션</dt>
                                                                    <dd>
                                                                    {{ items.cpBenefitOptionTitle }}
                                                                    </dd>
                                                                </dl>
                                                            </div>
                                                        </div>
                                                        <!-- //옵션 -->
                                                    </div>
                                                </div>
                                                <!-- //Loop -->
                                            </div>
                                        </div>
                                        <!-- //판매중 리워드 -->
                                        
                                        <!-- 제공중지 리워드 -->
                                        <div class="st-items-wrap" v-if="cnt > 0">
                                            <div class="st-items-title webfont2" v-if="rewardDetailData.cpEndStatus != 0">펀딩완료 리워드</div>
                                            <div id="st-items-wrap" class="row row-mobile">
                                                <!-- Loop -->
                                                <div id="st-items-a" class="col-xs-12" v-for="(item, index) in cardBenefitData" v-if="item.cbfRemQty == 0">
                                                    <div class="st-items st-items-sc">
                                                        <div class="st-items-mask" v-if="item.cbfStatus == 1 || item.cbfRemQty == 0"><span>리워드 재고가<br /> 없어요</span></div>
                                                        <div class="row not-space">
                                                            <div class="col-xs-6 col-sm-12">
                                                                <div class="st-items-su">
                                                                    <strong><span class="webfont2">{{ parseInt(item.cbfAmount).toLocaleString() }}</span>원 펀딩</strong>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-6 col-sm-12 xs-text-right">
                                                                <div class="st-items-btn">
                                                                    <!--<span class="btn btn-sm btn-danger" v-if="item.cbfStatus != 2">24개 남음</span>-->
                                                                    <span class="btn btn-sm btn-danger-outline">{{ parseInt(item.cpBenefitSpsQty).toLocaleString() }}개 펀딩</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <dl>
                                                                    <dt>리워드 제목</dt>
                                                                    <dd><strong>{{ item.cbfTitle }}</strong></dd>
                                                                </dl>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <p>{{ item.cbfInfo }}</p>
                                                            </div>
                                                            <div class="col-xs-6 col-sm-12" v-if="!traning">
                                                                <dl>
                                                                    <dt>예상 배송일</dt>
                                                                    <dd><strong>{{ item.cbfResDate }}</strong></dd>
                                                                </dl>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- //Loop -->
                                            </div>
                                        </div>
                                        <!-- //판매완료 리워드 -->
                                        <!-- 제공 중지된 리워드 -->
                                        <div class="st-items-wrap" v-if="scnt > 0">
                                            <div class="st-items-title webfont2" v-if="rewardDetailData.cpEndStatus != 0">제공 중지된 리워드</div>
                                            <div id="st-items-wrap" class="row row-mobile">
                                                <!-- Loop -->
                                                <div id="st-items-a" class="col-xs-12" v-for="(item, index) in cardBenefitData" v-if="item.cbfStatus == 2">
                                                    <div class="st-items st-items-sc">
                                                        <div class="st-items-mask" v-if="item.cbfStatus == 2 && item.cbfRemQty > 0"><span>리워드 제공이<br /> 중지됐어요</span></div>
                                                        <div class="row not-space">
                                                            <div class="col-xs-6 col-sm-12">
                                                                <div class="st-items-su">
                                                                    <strong><span class="webfont2">{{ parseInt(item.cbfAmount).toLocaleString() }}</span>원 펀딩</strong>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-6 col-sm-12 xs-text-right">
                                                                <div class="st-items-btn">
                                                                    <!--<span class="btn btn-sm btn-danger" v-if="item.cbfStatus != 2">24개 남음</span>-->
                                                                    <span class="btn btn-sm btn-danger-outline">{{ parseInt(item.cpBenefitSpsQty).toLocaleString() }}개 펀딩</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <dl>
                                                                    <dt>리워드 제목</dt>
                                                                    <dd><strong>{{ item.cbfTitle }}</strong></dd>
                                                                </dl>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <p>{{ item.cbfInfo }}</p>
                                                            </div>
                                                            <div class="col-xs-6 col-sm-12" v-if="!traning">
                                                                <dl>
                                                                    <dt>예상 배송일</dt>
                                                                    <dd><strong>{{ item.cbfResDate }}</strong></dd>
                                                                </dl>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- //Loop -->
                                            </div>
                                        </div>
                                        <!-- //제공 중지된 리워드 -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="detail_order_btn_mobile hidden-sm hidden-md hidden-lg">
                    <div class="detail_order_btn_mobile_frame">
                        <div class="detail_order_btn_mobile_left">
                            <div class="mdsns_btn_wrap">
                                <!--<a class="mdsns_btn" v-on:click="share">공유하기</a>-->
                                <label for="partner_facebook" class="icon_sns icon_facebook pointer" v-on:click="facebookShare"></label>
                                <ul class="mdsns_btn_navi" :class="{'open' : shareCheck}">
                                    <li class="mdsns_heart"><a v-on:click="facebookShare">페이스북 공유</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="detail_order_btn_mobile_right">
                            <div class="detail_order_btn">
                                <button class="btn btn-lg btn-block btn-primary" :disabled="rewardDetailData.cpEndStatus != 1" v-on:click="funding">펀딩하기</button>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <!--새소식-->
                <div id="news" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
                    <div class="modal-dialog modal-dialog-lg" role="document">
                        <form class="form-horizontal modal-content">
                            <div class="modal-header modal-header-black">
                                <a class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                                <div class="modal-title">새소식 등록하기 <br /><h5>등록한 새소식은 수정할 수 없으니 신중하게 입력해주세요.</h5></div>
                            </div>
                            
                            <div class="modal-body">
                                <div class="form-group row-mobile-n mt30 xs-mt10">
                                    <label for="newsSubject" class="col-xs-12 control-label">
                                        <div class="text-left">새소식 제목</div>
                                    </label>
                                    <div class="col-xs-12">
                                        <input type="text" class="form-control" :class="{'error' : errors.has('cbbsTitle')}" data-vv-name="cbbsTitle" v-model="save.cbbsTitle" v-validate="'required'"/>
                                        <label class="error" v-if="errors.has('cbbsTitle')" v-text="errors.first('cbbsTitle')"></label>
                                    </div>
                                </div>
                                
                                <div class="form-group row-mobile-n">
                                    <label for="newsMemo" class="col-xs-12 control-label">
                                        <div class="text-left">새소식 상세내용</div>
                                    </label>
                                    <div class="col-xs-12">
                                        <div class="panel-body panel-edit">
                                            <div class="mb15">
                                                Enter(↵) : 문단 나눔, Shift + Enter : 줄바꿈입니다!  <br />
                                                적절한 문단/줄 바꿈만으로 멋진 프로젝트를 완성 할 수 있습니다!
                                            </div> 
                                            <news-area v-if="dataConfirm" :id="'editor1'" :value="save.cbbsContent" :class="{'error' : errors.has('cbbsContent')}" data-vv-name="cbbsContent" v-model="save.cbbsContent" v-validate="'required'"></news-area>
                                            <label class="error" v-if="errors.has('cbbsContent')" v-text="errors.first('cbbsContent')"></label>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="modal-footer text-center">
                                    <div class="row row-mobile-n">
                                        <div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3">
                                            <div class="row row-mobile-n">
                                                <div class="col-xs-6">
                                                    <button type="button" class="btn btn-block btn-default-outline" v-on:click="newsDismiss">취소</button>
                                                </div>
                                                <div class="col-xs-6">
                                                    <a class="btn btn-block btn-primary" v-on:click="confirm">등록</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!--//새소식-->
                <!-- 전화연결은 모바일에서 가능합니다. -->
                <div id="telnotlModal" class="modal fade" tabindex="-1" role="dialog" style="z-index:1100;">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body modal-order">
                                전화연결은 모바일에서 가능합니다.
                                
                                <div class="modal-footer text-center">
                                    <div class="row row-mobile-n">
                                        <div class="col-xs-4 col-xs-offset-4">
                                            <button type="button" class="btn btn-block btn-primary-outline" data-dismiss="modal">확인</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //전화연결은 모바일에서 가능합니다. -->
                <!-- 프로필정보 -->
                <div id="profileSnsModal" class="modal fade" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <a class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                            </div>
                            <div class="modal-body pt15" v-if="snsConfirm">
                                <div class="widget">
                                    <div class="widget-body">
                                        <div class="media">
                                            <div class="media-left media-middle">
                                                <div class="media-object avatar avatar-lg">
                                                <img v-if="rewardDetailData.memShotImg != ''" :src="'//' + rewardDetailData.memShotImg" class="media-object" />
                                                </div>
                                            </div>
                                            <div class="media-body media-middle" style="font-size: 16px;">
                                                <strong class="grey-800">{{ rewardDetailData.memName }}</strong> <a href="mailto:rewardDetailData.memEmail" class="ml5 grey-700">({{rewardDetailData.memEmail}})</a>
                                            </div>
                                            <div class="sns-group">
                                                <dl v-if="snsInfo.websiteUrl != '' && snsInfo.websiteUrl != null">
                                                    <dt><label for="partner_website" class="icon_sns icon_website pointer"></label></dt><dt>
                                                    </dt><dd><strong class="form-control" style="display: block" v-text="snsInfo.websiteUrl"></strong></dd><dd>
                                                </dd></dl>

                                                <dl v-if="snsInfo.facebookUrl != '' && snsInfo.facebookUrl != null">
                                                    <dt><label for="partner_facebook" class="icon_sns icon_facebook pointer"></label></dt><dt>
                                                    </dt><dd><a type="text" class="form-control" v-text="snsInfo.facebookUrl"></a></dd><dd>
                                                </dd></dl>
                                    
                                                <dl v-if="snsInfo.instagramUrl != '' && snsInfo.instagramUrl != null">
                                                    <dt><label for="partner_instagram" class="icon_sns icon_instagram pointer"></label></dt><dt>
                                                    </dt><dd><a type="text" class="form-control" v-text="snsInfo.instagramUrl"></a></dd><dd>
                                                </dd></dl>
                                        
                                                <dl v-if="snsInfo.blogUrl != '' && snsInfo.blogUrl != null">
                                                    <dt><label for="partner_blog" class="icon_sns icon_blog pointer"></label></dt><dt>
                                                    </dt><dd><a type="text" class="form-control" v-text="snsInfo.blogUrl"></a></dd><dd>
                                                </dd></dl>
                                        
                                                <dl v-if="snsInfo.twitterUrl != '' && snsInfo.twitterUrl != null">
                                                    <dt><label for="partner_twitter" class="icon_sns icon_twitter pointer"></label></dt><dt>
                                                    </dt><dd><a type="text" class="form-control" v-text="snsInfo.twitterUrl"></a></dd><dd>
                                                </dd></dl>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                <div class="modal-footer text-center">
                                    <div class="row row-mobile-n">
                                        <div class="col-xs-4 col-xs-offset-4">
                                            <a class="btn btn-block btn-primary-outline" data-dismiss="modal">확인</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //프로필정보 -->
                </div>
            </div>
            `,
            props : {
                rewardDetailData : {
                    type : Object
                },
                cpCode : {
                    type : String
                },
                traning : {
                    default : false
                },
                fundingUrl : {
                    default : '/reward/funding/'
                }
            },
            data : function() {
                return {
                    dataConfirm : false,
                    mediaConfirm : false,
                    snsConfirm : false,
                    memCode : userInfo.memCode,
                    save : {
                        cpCode : '',
                        memCode : '',
                        cbbsTitle : '',
                        cbbsSummary : '',
                        cbbsContent : '',
                    },
                    snsInfo : {
						websiteUrl : "",
						facebookUrl : "",
						blogUrl : "",
						instagramUrl : "",
						twitterUrl : ""
                    },
                    cardBenefitData : [],
                    cardBenefitOptionsData : [],
                    mediaData : [],
                    youtubeUrl : {},
                    saveBackup : {},
                    newsChange : 1,
                    commentCheck : true,
                    vimeoConfirm : false,
                    lightSlideObj : {},
                    cnt : 0,
                    scnt : 0,
                    shareCheck : false,
                }
            },
            components : {
                comment : require('./reward-comment.js').default.component(),
                info : require('./reward-detail-info.js').default.component(),
                best : require('./reward-best-list.js').default.component(),
                news : require('./reward-news-list.js').default.component(),
                newsArea : require('../common/text-editor.js').default.component(),
            },
            created : function() {
                var self = this;
                self.saveBackup = _.clone(self.save);
                this.load();

                $(window.document).on("contextmenu", function(){
                 if(event.target.nodeName == "IMG"){
                        return false;
                    }
                });

                $( ".mce-content-body img" ).css( "-webkit-user-select", "none !important" );
                $( ".mce-content-body img" ).css( "-webkit-touch-callout", "none !important" );
                $( ".mce-content-body img" ).css( "-moz-user-select", "none !important" );
                $( ".mce-content-body img" ).css( "-ms-user-select", "none !important" );
                $( ".mce-content-body img" ).css( "-o-user-select", "none !important" );
                $( ".mce-content-body img" ).css( "user-select", "none !important" );
                $( ".mce-content-body img" ).css( "pointer-events", "none !important" );
                $( ".mce-content-body img" ).attr( "draggable", "false" );

                $('body').on('click', '.mce-btn', function() { 
                    $( ".mce-menu" ).addClass( "popup_mce_menu" );  
                    $( ".mce-tooltip" ).addClass( "popup_mce_tooltip" );  
                });
            },
            filters : {
                commentFilter : function(val) {
                    return val.replace( /[\n]/g, "<br/>")
                }
            },
            computed : {
                phoneNumber : function() {
                    var self = this;
                    if(this.rewardDetailData.refundTellViewStatus == "1") {
                        if(self.rewardDetailData.mobileno != null && self.rewardDetailData.mobileno != "") {
                            return '연락처 "' +  self.rewardDetailData.mobileno + '"';
                        } else {
                            return "";
                        }
                    } else {
                        return "";
                    }
                },
            },
            methods : {
                load : function() {
                    var self = this;
                    axios.post('/data/view/reward/benefit-list', {cpCode : this.rewardDetailData.cpCode})
                        .then(function (response) {
                            var result = response.data.rData;
                            self.cardBenefitData = result;
                            if(result == null) {
                                noti.open('잘못된 주소입니다.', function() {window.open("/", '_self');});
                                return;
                            }
                            for(var i=0; i < result.length; i++) {
                                if(result[i].cbfRemQty == 0) {
                                    self.cnt++;
                                } else if(self.rewardDetailData.cpRecStatus == '2') {
                                    if(result[i].cbfStatus != 1) {
                                        self.scnt++;
                                    } 
                                }
                            }
                            self.media();
                        })
                },
                //동영상, 이미지
                media : function() {
                    var self = this;
                    axios.post('/data/view/reward/media', { cpCode : this.rewardDetailData.cpCode })
                        .then(function (response) {
                            var result = response.data.rData;
                            if(result == null) {
                                noti.open('잘못된 주소입니다.', function() {window.open("/", '_self');});
                                return;
                            }
                            var result =  response.data.rData;
                            self.mediaConfirm = true;
                            var regExp = /(?:youtube(?:-nocookie)?\.com\/(?:[^\/\n\s]+\/\S+\/|(?:v|e(?:mbed)?)\/|\S*?[?&]v=)|youtu\.be\/)([a-zA-Z0-9_-]{11})/;

                            for(var i=0; i < result.length; i++) {
                                var data = result[i];
                                var mediaObject = {
                                    cmscDiv : '',
                                    msrc : '',
                                    src : '',                                
                                };
                                
                                mediaObject.src = data.cmscUrl;
                                //동영상
                                if(data.cmscDiv == '1') {
                                    mediaObject.cmscDiv = data.cmscDiv;
                                    //youtube
                                    var url = data.cmscUrl.match(regExp);
                                    if(url != null) {
                                        
                                        mediaObject.msrc = 'https://www.youtube.com/embed/' + url[1];

                                        mediaObject.src = '//img.youtube.com/vi/' + url[1] + '/default.jpg';
                                        // if (self.rewardDetailData.cpCode == '1000003441' && i == 0) {
                                        //     // 자동재생 & 반복을 위한 링크 수정
                                        //     mediaObject.msrc = 'https://www.youtube.com/embed/' + url[1] + '?autoplay=1&mute=1&enablejsapi=1&loop=1&playlist=' + url[1] + '&amp;showinfo=0';
                                        // }                                         
                                       
                                    } else { //vimeo
                                        var vimeoRegExp = /(https?:\/\/)?(www.)?(player.)?vimeo.com\/([a-z]*\/)*([0-9]{6,11})[?]?.*/;
                                        mediaObject.msrc = 'https://player.vimeo.com/video/' + data.cmscUrl.match(vimeoRegExp)[5];
                                        mediaObject.src = '//image-se.ycrowdy.com/logo/project-default.png/ycrowdy/resize/!200x!150'
                                        self.getVimeoUrl(data.cmscUrl, i);
                                    }
                                } else {
                                    mediaObject.cmscDiv = data.cmscDiv;
                                    mediaObject.src = '//' + data.cmscUrl + '/ycrowdy/resize/!200x!150';
                                    mediaObject.msrc = '//' + data.cmscUrl + '/ycrowdy/resize/!740x!417';
                                } 
                                self.mediaData.push(mediaObject);
                            }
                            self.$nextTick(function() {
                                self.lightSlide();
						    });
                        })
                },
                getVimeoUrl : function(url, index) {
                    var self = this;
                    var regExp = /(https?:\/\/)?(www.)?(player.)?vimeo.com\/([a-z]*\/)*([0-9]{6,11})[?]?.*/;
                    var vUrl = url.match(regExp);
                    axios.get('/vimeo/get/' + vUrl[5])
                       .then(function (response) {
                           var vUrl = response.data.rData.data.medium;
                           self.mediaData[index].src = vUrl
                           self.mediaData[index].confirm = true;
                           self.lightSlideObj.destroy();
                           self.lightSlide();
                    })
                },
                newsClick : function(code, memcode) {
                    var self = this;
                    self.save.cpCode = code;
                    self.save.memCode = memcode;
                    self.dataConfirm = true;
                    $('#news').modal('show');
                    
                },
                //펀딩하기
                funding : function(val) {
                    this.$emit('funding');
                },
                //새소식 등록
                confirm : function() {
                    var self = this;

                    if (self.save.cbbsTitle.length < 1) {
                        noti.open('새소식 제목을 입력해주세요.');                 
                        return;
                    }

                    noti.open('새소식을 등록하시면 수정이 불가합니다. <br /> 새소식을 등록하시겠습니까?', self.regitConfirm, true);                 
                },
                //새소식등록 확인
                regitConfirm : function() {
                    var self = this;
                    self.$validator.validateAll()
                        .then(function(success) {
                            if(!success) return;

                            axios.post('/set/save/reward/set-news', self.save)
                            .then(function (response) {
                                var result = response.data;
                                if(result.rCode == "0000") {
                                    $('#news').modal('hide');
                                    self.dataConfirm = false;
                                    self.save = _.clone(self.saveBackup);
                                    self.rewardDetailData.cbCount++;
                                    self.newsChange++;
                                } else {
                                    noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
                                }
                            })
                        })  
                },
                //페이스북 공유
                facebookShare : function() {
                    this.$emit('facebook-share');
                },
                //모바일에서 공유하기 버튼 클릭
                share : function() {
                    this.shareCheck = !this.shareCheck;
                },
                funding : function(val) {
                    this.$emit('funding');
                },
                newsDismiss : function() {
                    this.dataConfirm = false;
                    $('#news').modal('hide');
                },
                commentChange : function(val) {
                },
                mypage : function() {
                    window.open("/mypage/main", '_self');
                },
                //자세히보기
                detailShow : function() {
                    this.getSnsInfo();
                    $('#profileSnsModal').modal('show');
                },
                getSnsInfo : function() {
                    var self = this;
					axios.post('/data/save/reward/info/fund', {cpCode : this.rewardDetailData.cpCode})
	                    .then(function(response){
                            var result = response.data;
	                     	if(result.rCode == "0000") {
		                    	for (var i = 0; i < result.rData.snsList.length; i++) {
		                    		var snsType = result.rData.snsList[i].snsType;
		                    		var snsUrl = result.rData.snsList[i].snsUrl;
		                    		var miscIdx = result.rData.snsList[i].miscIdx;

		                    		if (snsType == 0) {
		                    			self.snsInfo.websiteUrl = snsUrl;
		                    		} else if (snsType == 1) {
										self.snsInfo.facebookUrl = snsUrl;
		                    		} else if (snsType == 2) {
		                    			self.snsInfo.blogUrl = snsUrl;
		                    		} else if (snsType == 3) {
		                    			self.snsInfo.instagramUrl = snsUrl;
		                    		} else if (snsType == 4) {
		                    			self.snsInfo.twitterUrl = snsUrl;
		                    		}
                                }
		                    	self.snsConfirm = true;
	                    	} else {
	                    		noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
                            }
	                })
                },
                lightSlide : function() {
                    this.lightSlideObj = $('#lightSlider').lightSlider({
                        item:1,
                        loop:true,
                        speed:500,
                        keyPress:true,
                        gallery:true,
                        thumbItem:6,
                        thumbMargin: 10,
                        slideMargin:0,
                        responsive : [
                            {
                                breakpoint:800,
                                settings: {
                                    thumbItem:6,
                                    thumbMargin: 5
                                  }
                            },
                            {
                                breakpoint:480,
                                settings: {
                                    thumbItem:6,
                                    thumbMargin: 5
                                  }
                            }
                        ],
                        enableTouch:true,
                        enableDrag:false,
                        adaptiveHeight: true,
                        currentPagerPosition: 'middle',
                        slideEndAnimation: true,
                        onSliderLoad: function() {
                            $('#lightSlider').removeClass('cS-hidden');
                        }
                    });
                }
            }
        }
    }
}        
        
export default new RewardDetailStory()








