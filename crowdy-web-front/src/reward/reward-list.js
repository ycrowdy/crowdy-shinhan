class RewardList {
    component() {
        return {
            template : `
                <div>
                    <div class="rewards-list">
                        <div class="row row-mobile">
                            <div v-for="(item, index) in reward"
                                class="col-sm-4 col-md-3 col-xs-6" 
                                >
                                <figure>
                                    <a :href="'/r/' + item.cpAliasUrl" class="items over-box">
                                        <div class="items_img">
                                            <div class="badge_icon background-pink" v-if="(!badgeCheck(item.cpStartDate) && item.cpEventType == '1') || ((item.cpEndStatus == '1') && (parseInt(item.cpCurrentAmount) == 0 || (parseInt(item.cpCurrentAmount) > 0 && badgeCheck(item.cpStartDate))))">GIFT</div>
                                            <div class="badge_icon" v-if="badgeCheck(item.cpStartDate)">NEW</div>
                                            <div class="badge_icon badge_icon_event" v-if="item.cpEndStatus =='2'">성공</div>
                                            <div class="badge_icon badge_icon_event" v-if="item.cpEndStatus =='3'">종료</div>
                                            <img :src="'//' + item.cpCardImg + '/ycrowdy/resize/!340x!226'" class="img-responsive" alt="..." />
                                        </div>
                                        <figcaption class="rewards-caption">
                                            <div class="rewards-subject">
                                                <div class="rewards-summury">{{item.memName}}</div>
                                                <div class="rewards-card-title">{{item.cpTitle}}</div>
                                            </div>

                                            <!-- <template v-if="(item.cpEndStatus == '1' && parseInt(item.cpCurrentAmount) > 0 && !badgeCheck(item.cpStartDate)) || item.cpEndStatus != '1'"> -->
                                            <template v-if="(item.cpEndStatus == '1' && parseInt(item.cpCurrentAmount) > 0) || item.cpEndStatus != '1'">

                                                <span class="rewards-price"> {{ parseInt(item.cpCurrentAmount).toLocaleString() }}원</span>
                                                <span class="rewards-percent rewards-percent-right mt4">{{item.cpRate}}%</span>

                                                <div class="progress">
                                                    <div class="progress-bar" role="progressbar" :aria-valuenow="item.cpRate" aria-valuemin="0" aria-valuemax="100" :style="{width: item.cpRate + '%'}"><span class="sr-only">{{item.cpRate}}% 완료</span></div>
                                                </div>

                                                <div class="row not-space">
                                                    <div class="col-xs-6">
                                                        <div class="rewards-support">{{item.cpSponsorCount}}명 참여</div>
                                                    </div>
                                                    <div class="col-xs-6 text-right">
                                                    <template v-if="item.cpEndStatus == '1'">
                                                            <span class="rewards-day" v-if="item.cpDday > 0">D-{{item.cpDday}}</span>
                                                            <!-- <span class="rewards-day rewards-day-end" v-else-if="item.cpDday == 0">{{ endTime }}</span> -->
                                                            <span class="rewards-day rewards-day-end" v-if="item.cpDday == 0">오늘 종료</span>
                                                    </template>
                                                    <template v-else>
                                                        <span class="rewards-day rewards-day-end">종료</span>
                                                    </template>
                                                    </div>
                                                </div>
                                            </template>
                                            <!-- <template v-if="(item.cpEndStatus == '1') && (parseInt(item.cpCurrentAmount) == 0 || (parseInt(item.cpCurrentAmount) > 0 && badgeCheck(item.cpStartDate)))"> -->
                                             <template v-if="(item.cpEndStatus == '1') && parseInt(item.cpCurrentAmount) == 0">

                                                <span class="rewards-price visible-hidden"><span class="webfont2">원</span></span>
                                                <span class="rewards-percent rewards-percent-right mt4 visible-hidden">%</span>

                                                <div class="progress visible-hidden">
                                                    <div class="progress-bar" role="progressbar" :aria-valuenow="item.cpRate" aria-valuemin="0" aria-valuemax="100"><span class="sr-only">완료</span></div>
                                                </div> 

                                                <div class="row not-space">
                                                    <div class="col-xs-11">
                                                        <div class="rewards-support font-pink">선착순 1명 기프티콘 증정!</div>
                                                    </div>
                                                    <div class="col-xs-1 text-right visible-hidden">
                                                        <span class="rewards-day rewards-day-end">&nbsp;</span>
                                                    </div>
                                                </div>
                                            </template>
                                        </figcaption>
                                    </a>
                                </figure>
                            </div>
                        </div>
                    </div>
                    
                    <template v-if="main">
                        <div v-show="moreShow">
                            <a href="javascript:void(0)" v-on:click="more()" class="btn btn-primary-outline col-sm-8 col-md-6 col-xs-12 col-sm-offset-2 col-md-offset-3 btn-more text-center mt50 mb20 md-mt65 md-mb0 m-mt50 m-mb20 xs-mt20 xs-mb10">주목받는 리워드 더보기</a>
                        </div>
                    </template>
                    <template v-if="!main">
                        <!-- <div v-show="moreShow" class="text-center mt75 mb20 md-mt65 md-mb0 m-mt50 m-mb20 xs-mt30 xs-mb0">
                            <a href="javascript:void(0)" v-on:click="more()" class="btn btn-primary-outline col-sm-8 col-md-6 col-xs-12 col-sm-offset-2 col-md-offset-3 btn-more">프로젝트 더보기</a>
                        </div> -->

                        <div v-show="moreShow">
                            <a href="javascript:void(0)" v-on:click="more()" class="btn btn-primary-outline col-sm-8 col-md-6 col-xs-12 col-sm-offset-2 col-md-offset-3 btn-more text-center mt50 mb20 md-mt65 md-mb0 m-mt50 m-mb20 xs-mt20 xs-mb10">프로젝트 더보기</a>
                        </div>

                    </template>
                    
                </div>
            `,
            props : {
                main : {
                    default : true
                },
                paramOrderType : {
                    default : "1"
                },
                paramEndType : {
                    default : "1"
                },
                paramSearchCount : {
                    default : "12"
                }
            },
            data : function() {
                return {
                    reward : [],
                    search : {
                        orderType : this.paramOrderType,
                        endType: this.paramEndType,
                        cpSimulationConfirm : "",
                        paging : {
                            page : "1",
                            count : this.paramSearchCount
                        }
                    },
                    moreShow : true
                }
            },
            created : function() {
                this.load();
            },
            watch : {
                paramOrderType : function(value) {
                    this.search.orderType = value;
                    this.search.paging.page = "1";
                    this.reward = [];
                    this.load();
                },
                paramEndType : function(value) {
                    this.search.endType = value;
                    this.search.paging.page = "1";
                    this.reward = [];
                    this.load();
                }
            },
            computed : {
                endTime : function() {
                    moment.updateLocale('en', {
                        relativeTime : {
                            hh : "%d 시간 남음",
                            mm : "%d 분 남음",
                            d : "오늘 까지",
                            h : "1 시간 남음"
                        }
                    })
                    return moment().endOf('day').fromNow(true)
                }
            },
            methods : {
                load : function() {
                    var self = this;
                    axios.post('/data/view/reward/list', this.search)
                        .then(function(response){
                            self.reward = _.concat(self.reward, response.data.rData);
                            if(response.data.rData.length != parseInt(self.search.paging.count)) {
                                self.moreShow = false;
                            }
                        })
                },
                more : function() {
                    this.search.paging.page  = _.toString(_.add(_.toNumber(this.search.paging.page), 1));
                    this.load();
                },
                badgeCheck : function(startDate) {
                    var now = moment(new Date());
                    var start = moment(startDate);
                    var duration = now.diff(start, 'days');
                    var newCheck = duration < 7 ? true : false

                    return newCheck;
                }
            }
        }
    }
}

export default new RewardList()