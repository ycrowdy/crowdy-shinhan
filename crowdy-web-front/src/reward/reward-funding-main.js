class RewardFundingMain {
    component() {
        return {
            template : `
                <div>
                    <!-- 프로젝트 펀딩하기 - 펀딩하기 -->
                    <div id="list_wrap" class="common_support">
                        <div class="common_sub_vi bg_gray">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-9 m-text-center">
                                        <div class="common_vi_title webfont2">{{ rewardDetailData.cpTitle }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
        
                        <div class="common_sub_layout">
                            <div class="container">
                                <div class="row not-space">
                                    <div class="col-md-2">
                                        <div class="st-tab">
                                            <ul>
                                                <li :class="{'active' : step == 1}" style="cursor: pointer;"><a class="st-tab-a" v-on:click="rewardChoose"><i></i>리워드선택</a><span></span></li>
                                                <li :class="{'active' : step == 2}" style="cursor: pointer;"><a class="st-tab-a"><i></i>결제예약</a></li>
                                                <li :class="{'active' : step == 3}" style="cursor: pointer;"><a class="st-tab-a"><i></i>예약확인</a><span></span></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <support v-if="step == 1 && dataConfirm" :code="cpCode" :id="id" v-on:set-id="setId" v-on:step-change="stepChange"></support>
                                    <payment v-if="step == 2 && dataConfirm" :code="cpCode" :id="id" :url="cpAliasUrl" :title="title" v-on:step-change="stepChange" v-on:set-idx="setIdx"></payment>
                                    <confirm v-if="step == 3 && dataConfirm" :code="cpCode" :id="id" :idx="idx" v-on:step-change="stepChange" v-on:facebook-share="facebookShare"></confirm>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- //프로젝트 펀딩하기 - 펀딩하기 -->
                </div>           
            `,
            props : {
                cpAliasUrl : {
                    require : true
                },
                paramCpTraining : {
                    default : 'N'
                }
            },
            data : function() {
                return {
                    step : 1,
                    rewardDetailData : [],
                    cpCode : '',
                    param: {
                    	cpAliasUrl: this.cpAliasUrl,
                        cpTraining : this.paramCpTraining
                    },
                    dataConfirm : false,
                    id : '',
                    idx : '',
                    title : ''
                }
            },
            components : {
                support : require('./reward-funding-support.js').default.component(),
                payment : require('./reward-funding-payment.js').default.component(),
                confirm : require('./reward-funding-confirm.js').default.component()
            },
            created : function() {
                this.load();
            },
            methods : {
                load : function() {
                    var self = this;
                    axios.post('/data/view/reward/detail', this.param)
                        .then(function (response) {
                            var result = response.data.rData;
                            if(result == null) {
                                noti.open('잘못된 주소입니다.', function() {window.open("/", '_self');});
                                return;
                            }
                            self.rewardDetailData = response.data.rData;
                            self.cpCode = response.data.rData.cpCode;
                            self.title = response.data.rData.cpTitle;
                            self.dataConfirm = !self.dataConfirm;
                        })
                },
                stepChange : function(step) {
                    this.step = step;
                },
                setIdx : function(idx) {
                    this.idx = idx;
                },
                setId : function(id) {
                    this.id = id;
                },
                rewardChoose : function() {
                    if(this.step == '2') {
                        this.step = '1';
                    }
                },
                facebookShare : function() {
                    var self = this;
                    FB.ui({
                        method: 'share',
                        display: 'popup',
                        href: document.location.origin + "/r/" + this.cpAliasUrl + '?utm_source=facebook&utm_medium=share&utm_campaign=' + this.cpAliasUrl,
                    }, function(response){
                        // 공유를 다 안하고 팝업을 끄면 undefined 반환,
                        // 공유 성공 시 post_id를 반환함
                        if (typeof(response) != "undefined" || response.post_id != '') {
                            noti.open("공유되었습니다.");
                        }
                    });
                },
            }
        }
    }
}
export default new RewardFundingMain()