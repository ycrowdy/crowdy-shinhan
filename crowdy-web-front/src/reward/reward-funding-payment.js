class RewardFundingPayment {
    component() {
        return {
            template : `
                <div>
                    <div class="col-md-10 col-line">
                        <div class="row not-space">
                            <div class="col-md-1"></div>
                            <div class="col-md-11">
                                <form class="form-horizontal pay_info_wrap_not" action="#">
                                    <!-- 내가 선택한 리워드 -->
                                    <div class="pay_info_wrap">
                                        <div class="step-title mt0">
                                            내가 선택한 리워드
                                        </div>

                                        <div class="xs-mt0 m-mb0 my_dash_table my_dash_table_center">
                                            <table class="table table-condensed-big table-vertical-align-top table-thead-blue table-fixed mb20">
                                                <colgroup>
                                                    <col style="width:18%;">
                                                    <col style="width:45%;">
                                                    <col style="width:26%;" class="hidden-xs">
                                                    <col style="width:11%;">
                                                </colgroup>
                                                <thead>
                                                    <tr>
                                                        <th>펀딩금액</th>
                                                        <th>리워드명</th>
                                                        <th class="hidden-xs">리워드옵션</th>
                                                        <th>수량</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <!-- Loop -->
                                                    <tr v-if="dataConfirm" v-for="(item, index) in funding.list">
                                                        <td><strong>{{calcAmount(item.sbfUnitPrice, item.qty)}} 원 <br class="hidden-sm hidden-md hidden-lg" />펀딩</strong></td>
                                                        <td class="text-left">
                                                            <strong>{{item.cbfTitle}}</strong>
                                                            <p class="text-left">{{item.cbfInfo}}</p>
                                                        </td>
                                                        <td class="hidden-xs"><strong>{{item.benefit}}</strong></td>
                                                        <td><strong>{{item.qty}}개</strong></td>
                                                    </tr>
                                                    <!-- //Loop -->
                                                </tbody>
                                            </table>

                                            <div class="total_price text-right" v-if="dataConfirm && parseInt(funding.spsAmount) > 0">
                                                추가펀딩 금액 <span>{{parseInt(funding.spsAmount).toLocaleString()}}원</span>
                                            </div>
                                            <div class="total_price text-right blue-800">
                                                총 결제예상 금액 <span>{{ parseInt(saveData.sponsorData.spsTotAmount).toLocaleString() }}원</span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- //내가 선택한 리워드 -->
                                    <!-- 결제정보입력 -->
                                    <div class="pay_info_wrap">
                                        <div class="step-title">
                                            <span class="webfont2">1</span>결제정보 입력
                                        </div>
                                        <!-- 카드정보 -->
                                        <div class="form-group row-mobile-n">
                                            <div class="col-sm-6">
                                                <div class="row row-mobile-n">
                                                    <label for="pay_card_num" class="col-xs-12 control-label">
                                                        <div class="text-left">
                                                            카드번호
                                                        </div>
                                                    </label>
                                                    <div class="col-xs-3">
                                                        <number-input class="form-control" :type="'tel'" :num="cardNo[0]" v-model="cardNo[0]" maxlength="4"></number-input>
                                                    </div>
                                                    <div class="col-xs-3">
                                                        <number-input class="form-control" :type="'tel'" :num="cardNo[1]" v-model="cardNo[1]" maxlength="4"></number-input>
                                                    </div>
                                                    <div class="col-xs-3">
                                                        <number-input class="form-control" :type="'tel'" :num="cardNo[2]" v-model="cardNo[2]" maxlength="4"></number-input>
                                                    </div>
                                                    <div class="col-xs-3">
                                                        <number-input class="form-control" :type="'tel'" :num="cardNo[3]" v-model="cardNo[3]" maxlength="4"></number-input>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row-mobile-n">
                                            <div class="col-sm-6">
                                                <div class="row row-mobile-n">
                                                    <label for="pay_card_pass" class="col-xs-12 control-label">
                                                        <div class="text-left">
                                                            카드 비밀번호
                                                        </div>
                                                    </label>
                                                    <div class="col-xs-4 col-sm-3">
                                                        <input type="password" class="form-control" v-model="saveData.cardData.cardPw" placeholder="앞 두자리" maxlength="2"/> 
                                                         <!-- v-on:keydown="isNumber"/> -->
                                                    </div>
                                                    <div class="col-xs-3 col-sm-3">
                                                        <p class="form-control-static form-control-static-big"> · ·</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row-mobile-n">
                                            <div class="col-sm-6">
                                                <div class="row row-mobile-n">
                                                    <label for="pay_card_month" class="col-xs-12 control-label">
                                                        <div class="text-left">
                                                            유효기간
                                                        </div>
                                                    </label>
                                                    <div class="col-xs-6 col-sm-3">
                                                        <number-input class="form-control" :type="'tel'" :num="saveData.cardData.expMonth" v-model="saveData.cardData.expMonth" placeholder="MM" maxlength="2"></number-input>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-3">
                                                        <number-input class="form-control" :type="'tel'" :num="saveData.cardData.expYear" v-model="saveData.cardData.expYear" placeholder="YY" maxlength="2"></number-input>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- //카드정보 -->

                                        <!-- 생년월일(앞 6자리) -->
                                        <div class="form-group row-mobile-n">
                                            <label for="pay_happy" class="col-xs-12 control-label">
                                                <div class="text-left">
                                                    생년월일 <span style="display:inline;">(앞 6자리)</span>
                                                </div>
                                            </label>
                                            <div class="col-sm-6">
                                                <number-input class="form-control" :type="'tel'" :num="saveData.cardData.idNo" v-model="saveData.cardData.idNo" placeholder="법인카드의 경우 사업자등록번호 10자리 입력" maxlength="10"></number-input>
                                            </div>
                                        </div>
                                        <!-- //생년월일(앞 6자리) -->

                                        <!-- 정보동의 -->
                                        <div class="form-group row-mobile-n mb5">
                                            <label for="pay_consent1" class="col-xs-12 control-label">
                                                <div class="text-left">
                                                    <strong>정보동의</strong>
                                                </div>
                                            </label>
                                            <div class="col-xs-12">
                                                <label for="pay_consent2" class="checkbox-inline">
                                                    <input type="checkbox" name="pay_consent2" id="pay_consent2" v-model="saveData.cardData.agree" /><span class="label"></span><span class="label-text grey-800">결제사 정보 제공을 동의합니다.</span>
                                                </label>
                                                <div class="ml25"><a href="javascript:void(0)" class="btn-link grey-500" v-on:click="policy">결제사 정보 제공 약관 보기 <i class="fa fa-angle-right ml5" aria-hidden="true"></i></a></div>
                                            </div>
                                        </div>
                                        <!-- //정보동의 -->
                                    </div>
                                    <hr />
                                    <!-- //결제정보입력 -->

                                    <!-- 배송정보 입력 -->
                                    <div class="pay_info_wrap pb0">
                                        <div class="step-title">
                                            <span class="webfont2">2</span>배송정보 입력
                                        </div>

                                        <!-- 받는 사람 휴대폰 번호 -->
                                        <div class="form-group row-mobile-n">
                                            <label for="delivery_person_check" class="checkbox-inline" v-if="deliveryCheck">
                                                <input type="checkbox" name="delivery_person_check" id="delivery_person_check" v-model="saveData.sponsorData.memberInfo" v-on:click="sameInfoInput" /><span class="label"></span><span class="label-text" v-on:click="sameInfoText">회원 정보와 동일</span>
                                            </label>
                                            <label for="delivery_mobile" class="col-xs-12 control-label">
                                                <div class="text-left">
                                                    결제자 휴대폰 번호
                                                </div>
                                            </label>
                                            <div class="col-sm-6">
                                                <div class="row row-mobile-n">
                                                    <div class="col-xs-10 col-sm-9">
                                                        <number-input  :type="'tel'" class="form-control" :num="saveData.sponsorData.spsMbMobileno" v-model="saveData.sponsorData.spsMbMobileno" minlength="10" maxlength="11"></number-input>
                                                    </div>
                                                </div>    
                                            </div>
                                        </div>
                                        <!-- //받는 사람 휴대폰 번호 -->

                                        <!-- 받는 사람 -->
                                        <div class="form-group row-mobile-n" v-if="deliveryCheck">
                                            <label for="delivery_person" class="col-xs-12 control-label">
                                                <div class="text-left">
                                                    받는 사람
                                                </div>
                                            </label>
                                            <div class="col-sm-6">
                                                <div class="row row-mobile-n">
                                                    <div class="col-xs-10 col-sm-9">
                                                        <input type="text" class="form-control" v-model="saveData.sponsorData.spsRsName" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- //받는 사람 -->

                                        <!-- 우편번호 -->
                                        <div class="form-group row-mobile-n" v-if="deliveryCheck">
                                            <label for="zip_code" class="col-xs-12 control-label">
                                                <div class="text-left">
                                                    우편번호
                                                </div>
                                            </label>
                                            <div class="col-sm-6">
                                                <div class="row row-mobile-n">
                                                    <div class="col-sm-9">
                                                        <div class="input-group input-group-file">
                                                            <input type="tel" class="form-control" v-on:click="postOpen" v-model="postNum" name="zip_code" id="zip_code" placeholder="" readonly="readonly" />
                                                            <span class="input-group-btn" v-on:click="postOpen">
                                                                <span class="btn btn-outline btn-file">
                                                                    <i class="fa fa-upload" aria-hidden="true"></i>
                                                                </span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- //우편번호 -->

                                        <!-- 주소 -->
                                        <div class="form-group row-mobile-n" v-if="deliveryCheck">
                                            <div class="col-md-10">
                                                <div class="row row-mobile-n">
                                                    <label for="address1" class="col-xs-12 control-label">
                                                        <div class="text-left">
                                                            주소
                                                        </div>
                                                    </label>
                                                    <div class="col-xs-12 mb10">
                                                        <input type="text" class="form-control" v-on:click="postOpen" name="address1" id="address1" v-model="address1" placeholder="주소를 검색해주세요." readonly="readonly" />
                                                    </div>
                                                    <div class="col-xs-12 mb0">
                                                        <input type="text" class="form-control" v-model="saveData.sponsorData.spsAddr2" placeholder="상세 주소를 입력해주세요." />
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label for="delivery_post_check" class="checkbox-inline">
                                                            <input type="checkbox" name="delivery_post_check" id="delivery_post_check" v-model="saveData.sponsorData.myAddressSave" /><span class="label"></span><span class="label-text">내 배송지 저장</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- //주소 -->
                                        <hr class="big_m" />

                                        <!-- 진행자에게 요청사항 -->
                                        <div class="form-group row-mobile-n mb0" v-if="deliveryCheck">
                                            <label for="delivery_person" class="col-xs-12 control-label">
                                                <div class="text-left">
                                                    진행자에게 요청사항(선택사항)
                                                </div>
                                            </label>
                                            <div class="col-md-10">
                                                <textarea rows="3" cols="50" class="form-control" v-model="saveData.sponsorData.spsMiscinfo"></textarea>
                                            </div>
                                        </div>
                                        <!-- //진행자에게 요청사항 -->
                                    </div>
                                    <!-- //배송정보 입력 -->
                                    <div class="row st-submit">
                                        <div class="col-sm-4 col-md-3">
                                            <a class="btn btn-lg btn-block btn-primary" v-on:click="payReserveFinish">결제 예약 완료</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <!-- 결제사 정보 제공 약관 -->
                    <div class="modal fade modal-primary" id="privacyCardPopup" role="dialog" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <a class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                                </div>
                                <div class="modal-body">
                                    <h4 class="blue-800 text-center mb20">결제사 정보 제공 약관 보기</h4>
                                    <ol>
                                        <li>1. 귀하가 신청하신 신용카드 정기과금 결제는 나이스정보통신(주)에서 제공하는 서비스로, 귀하의 신용카드 결제내역에는 이용가맹점이 NICE로 표기됩니다. 또한, 나이스정보통신㈜는 정기과금 결제대행만 제공하므로, 정기과금 결제신청 및 해지 등 모든 업무는 해당 인터넷 상점을 통해 직접 요청하셔야 합니다.</li>
                                        <li>2. 나이스정보통신㈜는 귀하의 본 신청과 관련한 거래내역을 e-mail로 통보 드리며, 당사 홈페이지 (<a href="https://www.nicepay.co.kr" target="_blank">https://home.nicepay.co.kr</a>)에서도 조회서비스를 제공합니다.</li>
                                        <li>3. 나이스정보통신㈜는 조회 등의 기본 서비스제공을 위해 필요한 최소 정보(성명, 이메일)만을 보관하고 있습니다.</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- //결제사 정보 제공 약관 -->
                </div>           
            `,
            props : ['code', 'id', 'title', 'url'],
            data : function() {
                return {
                    funding : {
                        list : []
                    },
                    cardNo : [],
                    saveData : {
                        sponsorData : {
                            cpCode : this.code,
                            memCode : userInfo.memCode,
                            spsIdx : '',
                            //받는사람 이름
                            spsRsName : '',
                            //우편번호
                            spsPostnum : '',
                            //주소1
                            spsAddr1 : '',
                            //상세주소
                            spsAddr2 : '',
                            //주문비고내용
                            spsMiscinfo : '',
                            //추가 후원금
                            spsAmount : '0',
                            //전체후원금
                            spsTotAmount : 0,
                            //후원상태
                            spsStatus : '0',
                            //후원공개여부
                            spsViewStatus : '0',
                            //계좌이체 인증
                            spsCmsYn : 'N',
                            //배송상태
                            spsDeliveryStatus : '0',
                            //결제구분(1: 카드)
                            payDiv : '1',
                            //결제번호(billkey)
                            payNum : '',
                            //은행, 카드 코드
                            bankCode : '',
                            //생년월일사업자번호
                            payInfoNum : '',
                            //결제자 구분(계좌)
                            payMemType : '0',
                            //결제자 휴대전화
                            spsMbMobileno : '',
                            //접속타입(0:PC, 1:모바일)
                            conType : '0', 
                            //결제동의, 회원정보동의, 제3자회원정보 동의
                            payAgreeYn : 'Y',
                            //결제비고
                            payEtc : 'N',
                            memberInfo : false, //(/reward/info쪽 )
                            myAddressSave : false,
                            benefitList : [],
                        },
                        cardData : {
                            cardNo : [],
                            cardPw : '',
                            expMonth : '',
                            expYear : '',
                            idNo : '',
                            agree : false,
                        },
                    },
                    address : {
						postNum : '',
						address1 : ''
                    },
                    deliveryCheck : false,
                    dataConfirm : false,
                }
            },
            components : {
				numberInput : require('../common/number-input.js').default.component(),
			},
            created : function() {
                this.load();
            },
            computed : {
                postNum : function() {
                    this.saveData.sponsorData.spsPostnum = this.address.postNum
                    return this.address.postNum
                },
                address1 : function() {
                    this.saveData.sponsorData.spsAddr1 = this.address.address1
                    return this.address.address1
                }
            },
            methods : {
                load : function() {
                    var self = this;
                    $(window).scrollTop(0);
                    axios.post('/reward/funding/get/benefit/' + this.id)
                        .then(function (response) {
                            var result = response.data.rData.data;
                            self.funding = result;
                            self.saveData.sponsorData.benefitList = result.list;
                            self.saveData.sponsorData.spsAmount = self.funding.spsAmount;
                            //self.saveData.sponsorData.cpCode = result.list[0].cpCode;
                            if(result.list.length > 0) {
                                if(result.list[0].cpBenefitDeliveryConfirm == 'Y') {
                                    self.saveData.sponsorData.spsDeliveryStatus = '1';
                                }
                                for(var i=0; i < result.list.length; i++) {
                                    self.saveData.sponsorData.spsTotAmount += (parseInt(result.list[i].sbfUnitPrice) * parseInt(result.list[i].qty));
                                }
                                self.saveData.sponsorData.spsTotAmount = parseInt(self.saveData.sponsorData.spsTotAmount) + parseInt(self.funding.spsAmount);
                            } else {
                                self.saveData.sponsorData.spsTotAmount = parseInt(self.funding.spsAmount);
                            }

                            _.forEach(self.funding.list, function(value) {
                                if(value.cbfDevrAddrYn == "Y") {
                                    self.deliveryCheck = true;
                                }
                            });
                            self.dataConfirm = true;

                            self.setPixelAddToCart();
                            // 우리은행 전산시스템 중지 팝업 
                            //noti.open("5월 5일 00시 ~ 5월 7일 24시까지 우리은행 전산시스템이 중지됨에 따라 우리은행 체크카드로는 결제를 하실 수 없어요. 우리은행 신용카드, 타 은행카드를 이용해주세요.");
                        });  
                },
                policy : function() {
                    $('#privacyCardPopup').modal('show');
                },
                postOpen : function() {
					post.open(this);
                },
                calcAmount : function(amount, qty) {
                    return (parseInt(amount) * parseInt(qty)).toLocaleString();
                },
                //결제예약완료
                payReserveFinish : function() {            
                    var self = this;

                    // if(_.size(this.cardNo[0]) != 4 || _.size(this.cardNo[1]) != 4 || _.size(this.cardNo[2]) != 4 || _.size(this.cardNo[3]) != 4) {
                    //     noti.open("카드 번호를 자리수에 맞게 입력해 주세요.");
                    //     return;
                    // }

                    if(_.size(this.saveData.cardData.cardPw) != 2) {
                        noti.open("카드 비밀번호 2자리를 입력해 주세요.");
                        return;
                    }
                    if(_.size(this.saveData.cardData.expMonth) != 2 || _.size(this.saveData.cardData.expYear) != 2) {
                        noti.open("카드 유효기간 2자리를 입력해 주세요.");
                        return;
                    }
                    if(_.size(this.saveData.cardData.idNo) < 1 ) {
                        noti.open("생년월일 또는 사업자등록번호를 입력해 주세요.");
                        return;
                    }
                    if(_.size(this.saveData.sponsorData.spsMbMobileno) < 1 ) {
                        noti.open("결제자 휴대폰 번호를 입력해 주세요.");
                        return;
                    }
                    if(this.deliveryCheck) {
                        if(_.size(this.saveData.sponsorData.spsRsName) < 1 ) {
                            noti.open("받는 사람을 입력해 주세요.");
                            return;
                        }
                        if(_.size(this.saveData.sponsorData.spsAddr2) < 1 ) {
                            noti.open("상세 주소를 입력해 주세요.");
                            return;
                        }
                    }

                    // 치펜데일 프로젝트 - 성인확인
                    //if(_.size(this.saveData.cardData.idNo) == 6 && (this.code == '1000003638' || this.code == '1000003619')) {
                    
                        // 러부마블 - 성인확인
                    if(_.size(this.saveData.cardData.idNo) == 6 && (this.code == '1000004165')) {

                        var birth = this.saveData.cardData.idNo;
                        var year = birth.slice(0, 2) * 1;
                        var date = birth.slice(-4);

                        if (year < 18 || ( year == 99 && date > moment().format("MMDD"))) {
                            noti.open("이 프로젝트는 만 19세 이상의 성인만 참여하실 수 있습니다.");
                            return;    
                        } 
                     }

                    this.saveData.cardData.cardNo = this.cardNo.join('');

                    //결제 정보 확인
                    if(this.saveData.cardData.agree) { 
                        $('.page-loader-more').fadeIn('')
                        axios.post('/data/reward/funding/bill/issue', this.saveData.cardData)
                            .then(function (response) {
                                var result = response.data.rData;
                                if(result.resultCode != "F100") {
                                    $('.page-loader-more').fadeOut('')
                                    var error = result.resultMsg;
                                    noti.open("결제정보를 확인해 주세요.<br />" + error);
                                    window.scrollTo(0,400);
                                    return;
                                } else {
                                    self.saveData.sponsorData.bankCode = result.cardCode;
                                    self.saveData.sponsorData.payNum = result.bid;
                                    self.saveData.sponsorData.payInfoNum = self.saveData.cardData.idNo;
                                    self.saveData.sponsorData.spsIdx = result.spsIdx;
                                    self.save();
                                    //내 배송지 저장
                                    if(self.saveData.sponsorData.myAddressSave) {
                                        self.saveMyAddress();
                                    }
                                }
                            });  
                    } else {
                        noti.open("결제사 정보 제공 동의 해야 결제예약이 진행 가능합니다.")
                        return;
                    }
                },
                save : function() {
                    var self = this;                    
                    axios.post('/set/reward/funding/confirm', self.saveData.sponsorData)
                        .then(function (response) {
                            var result = response.data;
                            $('.page-loader-more').fadeOut('')
                            if(result.rCode == "0000") {
                                // GTM - 향상된 전자상거래 데이터 삽입 & 다음 페이지로 이동
                                self.setGtmPurchases(result);
                            } else {
                                noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
                            }                                  
                        })
                },
                setPixelAddToCart : function() {
                     var self = this;    
                    var affiliation = this.title; // 파트너 또는 상점
                    var revenue = this.saveData.sponsorData.spsTotAmount; // 거래의 총 가치 
                    var campaignId = this.code; // 캠페인 아이디 
                    var campaignUrl = this.url;

                    var products = []; // 제품 데이터 
                    var contents = []; // 제품 데이터 

                    var addedPrice = this.funding.spsAmount * 1;
                    var totalBenefitPrice = (this.saveData.sponsorData.spsTotAmount * 1) - (this.funding.spsAmount * 1);

                    for (var i = 0 ; i < this.funding.list.length; i++) {

                        var info = this.funding.list[i];
                        var product = {
                            'id': info.cbfIdx, //제품 SKU
                            'name': info.cbfInfo, // 제품명
                            'price': info.sbfUnitPrice * 1, // 제품 개당 금액 
                            'quantity': info.qty * 1 // 제품 개수 
                        };
                        products.push(product); 
                    }
                    
                    // 추가후원금액 계산
                    if (addedPrice > 0) {
                        var product = {
                            'id': '추가후원',
                            'name': '추가후원',
                            'price': addedPrice,
                            'quantity': 1
                        };
                        products.push(product);
                    } 
              
                    var content = {
                        'id': campaignUrl, //제품 SKU
                        'name': affiliation, // 제품명
                        'item_price': revenue * 1, // 제품 개당 금액 
                        'quantity': 1// 제품 개수 
                    };
                    contents.push(content);

                    // 거래 데이터 
                    var gtmDataLayer = {
                            'ecommerce': {
                                "currencyCode": 'KRW',
                                'add': {
                                    'products': products
                                 }
                            },
                            'event' : 'AddToCart'
                        };

                    window.pixelCartData = window.pixelCartData || {};
                    window.pixelCartData = {
                        'contents' : contents,
                        'content_name' : affiliation,
                        'content_ids' : [ campaignUrl ] ,
                        'content_type' : 'product',
                        'value' : revenue * 1,
                        'currency' : 'KRW'
                    };

                    dataLayer.push(gtmDataLayer);

                },
                // GTM - 향상된 전자상거래 데이터 삽입 & 다음 페이지로 이동
                setGtmPurchases : function(result) {
                    var self = this;    

                    var id = result.rData.spsIdx; // 고유 거래 식별자 
                    var affiliation = this.title; // 파트너 또는 상점
                    var revenue = this.saveData.sponsorData.spsTotAmount; // 거래의 총 가치 
                    var campaignId = this.code; // 캠페인 아이디 
                    var campaignUrl = this.url;

                    var products = []; // 제품 데이터 
                    var contents = []; // 제품 데이터 

                    var addedPrice = this.funding.spsAmount * 1;
                    var totalBenefitPrice = (this.saveData.sponsorData.spsTotAmount * 1) - (this.funding.spsAmount * 1);

                    for (var i = 0 ; i < this.funding.list.length; i++) {

                        var info = this.funding.list[i];
                        var product = {
                            'id': info.cbfIdx, //제품 SKU
                            'name': info.cbfInfo, // 제품명
                            'price': info.sbfUnitPrice * 1, // 제품 개당 금액 
                            'quantity': info.qty * 1 // 제품 개수 
                        };
                        products.push(product); 

                       // var content = {
                       //      'id': info.cbfIdx, //제품 SKU
                       //      'name': info.cbfInfo, // 제품명
                       //      'item_price': info.sbfUnitPrice * 1, // 제품 개당 금액 
                       //      'quantity': info.qty * 1// 제품 개수 
                       //  };
                       //  contents.push(content);
                    }

                     var content = {
                            'id': campaignUrl, //제품 SKU
                            'name': affiliation, // 제품명
                            'item_price': revenue * 1, // 제품 개당 금액 
                            'quantity': 1// 제품 개수 
                        };
                        contents.push(content);
                    
                    // 추가후원금액 계산
                    if (addedPrice > 0) {
                        var product = {
                            'id': '추가후원',
                            'name': '추가후원',
                            'price': addedPrice,
                            'quantity': 1
                        };
                        products.push(product);

                        // var content = {
                        //     'id': '추가후원',
                        //     'name': '추가후원',
                        //     'item_price': addedPrice,
                        //     'quantity': 1
                        // };
                        // contents.push(content);
                    } 
                    
                    // 거래 데이터 
                    var gtmDataLayer = {
                            'ecommerce': {
                                'purchase': {
                                    'actionField': {
                                            'id' : id,
                                            'affiliation': affiliation,
                                            'revenue': revenue * 1
                                             },
                                    'products': products
                                 }
                            },
                            'event' : 'Purchase'
                        };
                    
                    window.pixelData = window.pixelData || {};
                    window.pixelData = {
                        'contents' : contents,
                        'content_name' : affiliation,
                        'content_ids' : [ campaignUrl ],
                        'content_type' : 'product',
                        'value' : revenue * 1,
                        'currency' : 'KRW',
                        'id' : id
                    };

                    dataLayer.push(gtmDataLayer);

                    self.$emit('step-change', 3);
                    self.$emit('set-idx', result.rData.spsIdx);
                },
                //회원정보와 동일
                sameInfoInput : function() {
                    this.sameInfo();
                },
                sameInfoText : function() {
                    this.sameInfo();
                },
                sameInfo : function() {
                    var self = this;
                    this.saveData.sponsorData.memberInfo = !this.saveData.sponsorData.memberInfo

                    if(this.saveData.sponsorData.memberInfo) {
                        axios.post('/data/member/info', { memCode : userInfo.memCode })
                            .then(function (response) {
                                var result = response.data.rData;
                                self.saveData.sponsorData.spsMbMobileno = result.mobileNo == "NULL" ? "" : result.mobileNo;
                                self.saveData.sponsorData.spsRsName = userInfo.name;
                                self.address.postNum = result.memSpsPostNum == "NULL" ? "" : result.memSpsPostNum;
                                self.address.address1 = result.memSpsAddr1 == "NULL" ? "" : result.memSpsAddr1;
                                self.saveData.sponsorData.spsAddr2 = result.memSpsAddr2 == "NULL" ? "" : result.memSpsAddr2;
                            })
                    } else {
                        self.saveData.sponsorData.spsMbMobileno = "";
                        self.saveData.sponsorData.spsRsName = "";
                        self.address.postNum = "";
                        self.address.address1 = "";
                        self.saveData.sponsorData.spsAddr2 = "";
                    }
                },
                //내 배송지 저장
                saveMyAddress : function() {
                    var self = this;                    
                    axios.post('/set/reward/funding/myaddress', self.saveData.sponsorData)
                        .then(function (response) {
                            var result = response.data;
                            if(result.rCode == "0000") {
                                $('.page-loader-more').fadeOut('')
                            }                               
                        })
                },
                isNumber : function(evt) {
                    evt = (evt) ? evt : window.event;
                    var charCode = (evt.which) ? evt.which : evt.keyCode;
                    if ((charCode > 31 && (charCode < 48 || charCode > 57)) && charCode !== 46) {
                      evt.preventDefault();
                    }
                    else {
                      return true;
                    }
                }
            }
        }
    }
}
export default new RewardFundingPayment