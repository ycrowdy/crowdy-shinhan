class RewardSponsor {
    component() {
        return {
            template : `
            <div>
                <!-- 후원자 -->
                <div class="col-md-8">
                    <div class="row not-space rds_wrap">
                        <div class="col-xs-6">
                            <div class="rds_frame">
                                <strong class="webfont2">{{ sponsorCount }}</strong>
                                <div class="stitle mt5">펀딩한 사람</div>
                                <p>실제 결제예약을 <br class="hidden-sm hidden-md hidden-lg" />한 사람들</p>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="rds_frame">
                                <strong class="webfont2">187</strong>
                                <div class="stitle mt5">공유한 사람</div>
                                <p>페이스북으로 <br class="hidden-sm hidden-md hidden-lg" />공유한 사람들</p>
                            </div>
                        </div>
                    </div>

                    <div class="rds_group mb50 xs-mb30">
                        <div class="rds_group_title webfont2">
                            이 프로젝트에 참여한 사람들
                        </div>

                        <div id="rds_group" class="row row-mobile-n">
                            <!-- Loop -->
                            <div class="col-xs-6 col-sm-3" v-for="(item, index) in sponsorData">
                                <div class="user_item">
                                    <div class="imgava"></div>
                                    <strong>{{ item.memName }}</strong>
                                    <p>{{ item.spsTotAmount }}원 펀딩</p>
                                    <div class="user_item_ul">
                                        <ul>
                                            <li>{{ item.projectSponCount }}개 프로젝트 펀딩</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- //Loop -->
                        </div>

                        <!-- 페이징 -->
                        <div class="text-center mb40">
                            <a href="javascript:void(0)" id="comment_list_more" v-show="moreShow" v-on:click="more()" class="btn btn-primary-outline">더보기</a>
                        </div>
                        <!-- //페이징 -->
                    </div>
                </div>                            
                <!-- //후원자 -->
            </div>
            `,
            props : {
                cpCode : {
                    default : '',
                },
                paramSearchCount : {
                    default : "12"
                }
            },
            data : function() {
                return {
                    sponsorData: [],
                    sponsorParam: {
                        cpCode : this.cpCode,
                        paging : {
                            page : "1",
                            count : this.paramSearchCount
                        }
                    },
                    sponsorCount : '',
                    moreShow : true,
                }
            },
            created : function() { 
                this.sponsor();
            },
            methods : {
                sponsor : function() {
                    var self = this;                    
                    axios.post('/data/view/reward/sponsor-list', self.sponsorParam)
                    .then(function (response) {
                        self.sponsorData = _.concat(self.sponsorData ,response.data.rData.sponsor);
                        self.sponsorCount = response.data.rData.sponsorCount;

                        if(!self.moreEnd() || response.data.rData.length == 0) {
                            self.moreShow = false;
                        }
                    })
                },
                more : function() {
                    this.sponsorParam.paging.page  = _.toString(_.add(_.toNumber(this.sponsorParam.paging.page), 1));
                    this.sponsor();
                },
                moreEnd : function() {
                    return (this.sponsorParam.length % this.sponsorParam.paging.count == 0) ? true : false;
                },
            },
        }
    }
}

export default new RewardSponsor()