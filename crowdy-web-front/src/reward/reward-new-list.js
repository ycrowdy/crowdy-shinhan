class RewardList {
    component() {
        return {
            template : `
                <div>
                    <div class="main-box-wrap" v-if="dataConfirm">
                        <div class="container">
                            <div class="main-box">
                                <div class="title">
                                    <div class="title-block">
                                        <span class="main-title">새로운 리워드</span>
                                        <a href="/reward/list" class="title-line"><span class="main-title-description">새로 시작한 프로젝트들을 만나보세요</span><i class="fa fa-angle-right hidden-md hidden-lg" aria-hidden="true"></i></a>
                                    </div>
                                </div>

                                <div class="rewards-list">
                                    <div class="row row-mobile">
                                        <div v-for="(item, index) in reward"
                                            class="col-sm-4 col-md-3 col-xs-6 list_card_pre_open_height_parent">
                                            <figure>
                                                <a :href="'/r/' + item.cpAliasUrl" class="items over-box list_card_pre_open_height">
                                                    <div class="items_img">
                                                        <div class="badge_icon full-badge">NEW!</div>
                                                        <div class="badge_icon badge_icon_event" v-if="item.cpEndStatus =='2'">성공</div>
                                                        <div class="badge_icon badge_icon_event" v-if="item.cpEndStatus =='3'">종료</div>
                                                        <img :src="'//' + item.cpCardImg + '/ycrowdy/resize/!340x!226'" class="img-responsive" alt="..." />
                                                    </div>
                                                    <figcaption class="rewards-caption">
                                                        <div class="rewards-subject new-subject">
                                                            <div class="rewards-summury">{{item.memName}}</div>
                                                            <div class="rewards-card-title">{{item.cpTitle}}</div>
                                                        </div>
                                                        <div class="progress visible-hidden hidden-lg">
                                                            <div class="progress-bar" role="progressbar" :aria-valuenow="item.cpRate" aria-valuemin="0" aria-valuemax="100" :style="{width: item.cpRate + '%'}"><span class="sr-only">{{item.cpRate}}% 완료</span></div>
                                                        </div>

                                                        <div class="row not-space">
                                                            <div class="col-xs-12">
                                                                <template v-if="parseInt(item.cpCurrentAmount) > 0">
                                                                    <div class="rewards-support font-blue pb2">지금 바로 펀딩하세요!</div>
                                                                </template>
                                                                <template v-if="parseInt(item.cpCurrentAmount) == 0">
                                                                    <div class="rewards-support font-pink pb2">선착순 1명 기프티콘 증정!</div>
                                                                </template>
                                                            </div>
                                                        </div>
                                                    </figcaption>
                                                </a>
                                            </figure>
                                        </div>
                                    </div>
                                </div>

                                
                                <template v-if="dataConfirm">
                                    <div v-show="moreShow" class="text-center mt50 mb20 md-mt65 md-mb0 m-mt50 m-mb20 xs-mt30 xs-mb0">
                                       <a href="javascript:void(10)" v-on:click="more()" class="btn btn-primary-outline col-sm-8 col-md-6 col-xs-12 col-sm-offset-2 col-md-offset-3 btn-more text-center mt50 mb20 md-mt65 md-mb0 m-mt50 m-mb20 xs-mt20 xs-mb10">새로운 리워드 더보기</a>
                                    </div>
                                </template>
                            </div>
                        </div>
                    </div>
                    
                    
                </div>
            `,
            props : {
                paramSearchCount : {
                    default : "4"
                }
            },
            data : function() {
                return {
                    reward : [],
                    search : {
                        paging : {
                            page : "1",
                            count : this.paramSearchCount
                        }
                    },
                    moreShow : true,
                    dataConfirm : false
                }
            },
            created : function() {
                this.load();
            },
            methods : {
                load : function() {
                    var self = this;
                    axios.post('/data/view/reward/new-list', this.search)
                        .then(function(response){

                            if (response.data.rData.cardCount > 0) {
                                self.reward = _.concat(self.reward, response.data.rData.cardList);
                                if(response.data.rData.cardCount < 5 || self.reward.length == 8) {
                                    self.moreShow = false;
                                }
                                self.dataConfirm = true;
                            }
                            
                        })
                },
                more : function() {
                    this.search.paging.page = 2;
                    this.load();
                }
            }
        }
    }
}

export default new RewardList()