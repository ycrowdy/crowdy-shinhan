class RewardFundingConfirm {
    component() {
        return {
            template : `
                <div class="col-md-10 col-line">
                    <div class="row not-space">
                        <div class="col-md-1"></div>
                        <div class="col-md-11">
                            <!-- 후원하여 주셔서 감사합니다! -->
                            <div class="fp-items-info">
                                <div class="row not-space">
                                    
                                    <div class="col-sm-7">
                                        <div class="fp-items-detail-c">
                                            <div class="container_mobile">
                                                <div class="yment">
                                                    <h5 class="blue-800 xs-mt20">펀딩 참여 완료</h5>
                                                    <strong>후원하여 주셔서 감사합니다!</strong>
                                                    <span>{{ (parseInt(sponsorData.sponsorCount)).toLocaleString() }}번째 후원자</span>가 되셨습니다. <em>{{ sponsorData.wdate }}</em>
                                                </div>
                                            </div>

                                            <hr class="big hidden-sm hidden-md hidden-lg" />

                                            <div class="container_mobile xs-mt5">
                                                <div class="yment_s">결제 관련 정보</div>
                                                <dl>
                                                    <dt>결제수단</dt>
                                                    <dd><span>{{sponsorData.payDiv | payType}}</span></dd>
                                                </dl>
                                                <dl>
                                                    <dt>총금액</dt>
                                                    <dd><strong>{{ parseInt(sponsorData.spsTotAmount).toLocaleString() }}원</strong></dd>
                                                </dl>
                                                <dl>
                                                    <dt>결제 예정일</dt>
                                                    <dd>{{ sponsorData.paymentData }}</dd>
                                                </dl>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <hr class="big_m" />
                            <!-- //후원하여 주셔서 감사합니다! -->

                            <form class="form-horizontal">
                                <div class="pay_info_wrap pt0 pb3">
                                    <div class="step-title step-title2 mt0 xs-mb10">
                                        좋은 아이디어는 <br class="hidden-lg hidden-md hidden-lg" />많은 사람들에게 알려져야합니다
                                    </div>
                                    <div class="row row-mobile mb20">
                                        <div class="col-sm-12 col-md-5">
                                            <div class="row row-mobile-n">
                                                <div class="col-xs-6">
                                                    <a class="btn btn-block btn-primary-outline" v-on:click="facebookShare"><i class="fa fa-facebook mr5" aria-hidden="true"></i> 페이스북 공유</a>
                                                </div>
                                                <div class="col-xs-6">
                                                    <a class="btn btn-block btn-primary-outline" href="/"><i class="fa mr5" aria-hidden="true"></i>메인으로 돌아가기</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <hr class="big_m" />

                                </div>
                            </form>
                            <div class="container_mobile">
                                <div class="row not-space">
                                    <div class="col-md-2">
                                        <div class="notable-project-title">
                                            지금 주목받는 <br class="hidden-xs hidden-sm" />
                                            프로젝트
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div id="notable-project" class="pb40 m-pb0 xs-pb5">
                                            <best :param-order-type="1"></best>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            `,
            props : ['code', 'idx'],
            data : function() {
                return {
                    sponsorData : [],
                    search : {
                        cpCode : this.code,
                        memCode : userInfo.memCode,
                        spsIdx : this.idx,
                    }
                }
            },
            components : {
                best : require('./reward-best-list.js').default.component(),
            },
            created : function() {
                this.load();
            },
            methods : {
                load : function() {
                    $(window).scrollTop(0);
                        var self = this;
                        axios.post('/data/view/reward/funding/result', this.search)
                            .then(function (response) {
                                var result = response.data.rData;
                                self.sponsorData = result;
                            })
                },
                facebookShare : function() {
                    this.$emit('facebook-share');
                }
            },
            filters : {
                payType : function(val) {
                    //결제구분(카드:1, 계좌이체:2, 쿠폰:3)
                    var result = "";
                    if(val == "1"){
                        result = "카드";
                    } else if(val == "2") {
                        result = "계좌이체";
                    } else if(val == "3") {
                        result = "쿠폰";
                    }
                    return result;
                }
            }
        }
    }
}
export default new RewardFundingConfirm