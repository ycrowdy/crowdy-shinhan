class RewardReply {
    component() {
        return {
            template : `
            <div>
                <div class="comments" v-for="(item, index) in replyFilter">
                    <div class="comment media">
                        <div class="media-left pr20 m-pr10">
                            <div class="reply-object" href="javascript:void(0)"><img v-if="item.memShotImg != ''" :src="'//' + item.memShotImg + '/ycrowdy/resize/!160x!160'" /></div>
                        </div>
                        <div class="comment-body media-body">
                            <a href="javascript:void(0)" class="comment-author">{{ item.memName }}</a>
                            <template v-if="item.crpyStatus == 1">
                                <div class="comment-content">{{ item.crpyContent }}</div>
                            </template>
                            <template v-if="item.crpyStatus == 2">
                                <div class="comment-content">해당 댓글은 삭제되었습니다.</div>
                            </template>
                            <div class="comment-meta">
                                <span class="date">{{ item.wdate }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            `,
            props: [
                "paramReply", "paramCommentIdx", "replyChange"
            ],
            data : function() {
                return {
                    comment : this.paramCommentIdx,
                    reply : _.orderBy(this.paramReply, ['wdate', 'asc'])
                }
            },
            watch : {
                replyChange : function() {
                    var self = this;
                    return this.reply.filter(function(reply) {
                        return reply.crpyPidx == self.comment;
                    })
                }
            },
            computed : {
                replyFilter : function() {
                    var self = this;
                    return this.reply.filter(function(reply) {
                        return reply.crpyPidx == self.comment;
                    })
                }
            }
        }
    }
}

export default new RewardReply()