class RewardListMain {
	component() {
		return {
			template : `
			<div>
				<div id="list-category">
                    <div class="list_category">
                        <div class="container">
                            <div class="row row-mobile">
                                <div class="col-xs-7 col-md-3">
                                    <order-select :options="orderTypeOptions" v-model="orderType"></order-select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container">
                    <reward :param-order-type="orderType" :param-end-type="2" :main="false"></reward>
                </div>
			</div>
			`,
			data : function() {
				return {
                    orderType : "1",
                    orderTypeOptions : [
                        { id : "1", text : "펀딩금액순"},
                        { id : "2", text : "마감임박순"},
                        { id : "3", text : "최신순"},
                        { id : "4", text : "참여자 많은순"},
                    ]
				}
			},
			mounted : function() {
                var scroll = $(this).scrollTop();
                var gnb = $(".navbar-venture");
                var category = $(".list_category");
                var cstopMenu = $("#list-category .common_sub_top_menu");
                var dobm = $(".detail_order_btn_mobile");
                
                var listWrap = $("#main").position();
                var listCategory = $("#list-category").position();
                
                if (scroll > listWrap.top) {
                    gnb.addClass('navbar-fixed-up');
                } else {
                    gnb.removeClass('navbar-fixed-up');
                }

                if (scroll > listCategory.top) {
                    $("body").addClass('fixed');
                    category.addClass('category_fixed');
                    cstopMenu.addClass('stm_fixed');
                    dobm.addClass('fixed');
                } else {
                    $("body").removeClass('fixed');
                    category.removeClass('category_fixed');
                    cstopMenu.removeClass('stm_fixed');
                    dobm.removeClass('fixed');
                }

			},
            components : {
                reward : require('./reward-list.js').default.component(),
                orderSelect : require('../common/select.js').default.component(),
            }
		}
	}
}

export default new RewardListMain()