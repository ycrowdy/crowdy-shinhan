class rewardBestList {
	component() {
		return {
			template : `
				<div class="pb40 xs-pb5" id="notable-project">
					<figure v-for="item in reward">
						<a :href="'/r/' + item.cpAliasUrl" class="mt0 bbs-caption over-box">
							<div class="bbs-img">
								<img :src="'//' + item.cpCardImg + '/ycrowdy/resize/!340x!226'" class="img-responsive" />
							</div>
							<div class="bbs-subject bbs-subject2">{{ item.cpTitle }}</div>
						</a>
					</figure>
				</div>
			`,
			props : {
	            paramOrderType : {
	                default : "1"
	            },
	            paramEndType : {
	                default : "0"
	            },
	            paramSearchCount : {
	                default : "6"
	            },
	            type : {
	            	default : true
	            }
	        },
	        data : function() {
	        	return {
	        		reward : [],
	        		search : {
	                    orderType : this.paramOrderType,
	                    endType: this.paramEndType,
	                    cpSimulationConfirm : "",
	                    paging : {
	                        page : "1",
	                        count : this.paramSearchCount
	                    }
	                },
	        	}
	        },
	        created : function() {
	            this.load();
	        },
	        methods : {
	        	load : function() {
	                var self = this;
	                axios.post('/data/view/reward/list', self.search)
	                    .then(function(response){
							self.reward = response.data.rData;
							self.$nextTick(function() {
								self.slickCall();
							});
	                    })
				},
				slickCall : function() {
					$(this.$el).slick({
						arrows: false,
						dots: true,
						swipe: false,
						swipeToSlide: false,
						touchMove: false,
						infinite: true,
						autoplay: true,
						autoplaySpeed: 3000,
						slidesToShow: 3,
						slidesToScroll: 3,
						responsive: [
							{
							  breakpoint: 1050,
							  settings: {
								dots: true,
								slidesToShow: 3,
								slidesToScroll: 3
							  }
							},
							{
							  breakpoint: 767,
							  settings: {
								dots: true,
								slidesToShow: 3,
								slidesToScroll: 3
							  }
							},
							{
							  breakpoint: 477,
							  settings: {
								dots: true,
								slidesToShow: 2,
								slidesToScroll: 2
							  }
							}
						]
					});
				}
	        }
		}
	}
}

export default new rewardBestList()
