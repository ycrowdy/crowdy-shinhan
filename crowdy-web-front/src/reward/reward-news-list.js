class RewardNewsList {
    component() {
        return {
            template : `
                <div>
                <div class="mp-body" v-for="(item, index) in newsData">
                    <div v-html="item.cbbsContent" class="mce-content-body"></div>
                    <div style="text-align: right;">{{item.wdate}}</div>
                    <hr v-if="cbCount != (index + 1)" class="big"/>
                </div>
                </div>
            `,
            props : ['cpCode', 'newsChange', 'cbCount'],
            data : function() {
                return {
                    newsData : [],
                    newsCount : "",
                    newsParam : {
                        cpCode : this.cpCode,
                    }
                }
            },
            created : function() { 
                this.news();
            },
            watch : {
                newsChange : function() {
                    this.news()
                }
            },
            methods : {
                news : function() {
                    var self = this;                    
                    axios.post('/data/view/reward/news-list', self.newsParam)
                    .then(function (response) {
                        self.newsData = response.data.rData;

                    })
                }
            },
        }
    }
}

export default new RewardNewsList()