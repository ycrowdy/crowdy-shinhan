class RewardDetail {
    component() {
        return {
            template : `
                <div>
                    <div class="common_vi_wrap common_vi_wrap8">
                        <div class="common_vi_frame webfont2">
                            {{ rewardDetailData.cpTitle }}
                            <template v-if="rewardDetailData.communityName != ''">
                            <h4>{{ rewardDetailData.communityName }}</h4>
                            </template>
                        </div>
                    </div>
                
                    <div class="common_sub_top_menu common_stm_detail hidden-sm hidden-md hidden-lg" style="border-top: 1px solid #ddd;">
                        <div class="container common_stm_detail_mobile common_stm_detail_mobile_story">
                            <a class="hash-link" :class="{'active' : step == 1}" style="cursor: pointer;" v-on:click="stepChange(1)"><span class="webfont2">스토리</span></a>
                            <a class="hash-link new_icon" :class="{'active' : step == 2}" style="cursor: pointer;" v-on:click="stepChange(2)"><span class="webfont2">댓글 <em v-if="cpAliasUrl != '90kjh'">{{ rewardDetailData.replyCnt }}</em></span></a>
                        </div>
                    </div>
                
                    <div id="list_category" class="hidden-xs" style="border-top: 1px solid #ddd;">
                        <div class="common_sub_top_menu common_stm_detail">
                            <div class="container common_stm_detail_hide">
                                <a class="hash-link" :class="{'active' : step == 1}" style="cursor: pointer;" v-on:click="stepChange(1)"><span class="webfont2">스토리</span></a>
                                <a class="hash-link new_icon" :class="{'active' : step == 2}" style="cursor: pointer;" v-on:click="stepChange(2)"><span class="webfont2">댓글 <em v-if="cpAliasUrl != '90kjh'">{{ rewardDetailData.replyCnt }}</em></span></a>
                            </div>
                            <div class="container common_stm_detail_show">
                                <div class="row row-mobile-n">
                                    <div class="col-sm-8">
                                        <a class="hash-link first-child" :class="{'active' : step == 1}" style="cursor: pointer;" v-on:click="stepChange(1)"><span class="webfont2">스토리</span></a>
                                        <a class="hash-link new_icon" :class="{'active' : step == 2}" style="cursor: pointer;" v-on:click="stepChange(2)"><span class="webfont2">댓글 <em v-if="cpAliasUrl != '90kjh'">{{ rewardDetailData.replyCnt }}</em></span></a>
                                        <!--<div class="dropdown">
                                            <a href="javascript:void(0)" id="dLabel" class="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="webfont2">공유하기</span> <span class="caret"></span></a>
                                            <ul class="dropdown-menu" aria-labelledby="dLabel">
                                                <li><a href="javascript:void(0)" v-on:click="facebookShare">페이스북 공유</a></li>
                                            </ul>
                                        </div>-->
                                    </div>
                                    <div class="col-sm-4" v-if="dataConfirm">
                                        <div class="row not-space">
                                            <div class="col-md-1"></div>
                                            <div class="col-md-11">
                                                <div class="detail_order_btn">
                                                    <button class="btn btn-block btn-lg btn-primary" :disabled="rewardDetailData.cpEndStatus != 1" v-on:click="funding">펀딩하기</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                
                    <story :reward-detail-data="rewardDetailData" :cp-code="cpCode" v-if="step == 1 && dataConfirm" v-on:funding="funding" v-on:facebook-share="facebookShare"></story>
                
                    <div v-if="step == 2  && dataConfirm">
                        <!-- 댓글 -->
                        <div class="detail_wrap">
                            <div class="container">
                                <div class="row row-mobile-n">
                                    <div class="col-md-8">
                                        <template v-if="cpAliasUrl != '90kjh'">
                                            <div class="list_title list_title_none webfont2 xs-mt25">
                                                여러분의 한마디가<br />
                                                진행자에게 큰 힘이 됩니다
                                            </div>
                                            <div class="list_summury list_summury_big webfont2">
                                                <span class="blue-800">{{ rewardDetailData.replyCnt }}개</span>의 댓글이 달려있습니다
                                            </div>
                                            <!-- 댓글작성 & 댓글목록 -->
                                            <comment :cp-code="cpCode" :crpy-idxs="0" :param-search-count="10" :training="training" :name="rewardDetailData.memName"></comment>
                                            <!-- //댓글작성 & 댓글목록 -->
                                        </template>
                                        <template v-if="cpAliasUrl == '90kjh'">
                                            <div id="list_wrap">
                                                <div class="enot_wrap enot_wrap4">
                                                    <div class="row">
                                                        <div class="col-sm-2">
                                                        </div>
                                                        <div class="col-md-12 col-sm-4">
                                                            <div class="enot_logo4"></div>
                                                        </div>
                                                        <div class="col-sm-6 col-md-12">
                                                            <div class="enot_text4">
                                                                <!-- 현재 프로젝트 댓글창은 프로젝트 진행자의 <br/> 요청으로 비활성화 되었습니다. -->
                                                                안녕하세요. 크라우디입니다.<br/>

                                                                [성평등 프로젝트]의 승인과 진행에 관한<br/>
                                                                크라우디의 입장을 전달해 드립니다.<br/>
                                                                <br/>
                                                                 

                                                                크라우디는 정치적 견해, 종교적 신념, 사회적 이슈에 대한 의견 표출에 대하여 다양한 의견이<br/>
                                                                공론화 되어야 한다고 믿습니다.<br/>
                                                                <br/>

                                                                크라우드펀딩 플랫폼은 중립적인 입장에서 다양한 주장의 표출 및 프로젝트의 시도가 <br/>
                                                                이뤄져야 한다고 생각합니다.<br/>
                                                                <br/>

                                                                크라우디가 진행하는 프로젝트는 크라우디의 개별적 구성원들의<br/>
                                                                의견이나 신념과 일치하지 않을 수 있습니다.<br/>
                                                                <br/>

                                                                다수 의견과 소수 의견 모두 우리 사회를 구성하는<br/>
                                                                여러분들의 소중한 의견이라 생각합니다.<br/>
                                                                <br/>

                                                                크라우디는, 진행하는 다양한 프로젝트들이 사회적 갈등보다는 의견의 표출과 경청 및 다양한 구성원들의<br/>
                                                                의견 수렴이라는 긍정적인 작용으로<br/>
                                                                영향을 미치길 바랍니다.<br/>
                                                                <br/>

                                                                항상 크라우디를 이용해주셔서 감사합니다.<br/>

                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </template>
                                    </div>
                                    <info :reward-detail-data="rewardDetailData" :training="training" v-on:funding="funding" v-on:facebook-share="facebookShare" :step="step"></info>
                                </div>
                            </div>
                        </div>
                        <!-- //댓글 -->
                    </div>
                    <!-- //댓글 -->
                </div>
            `,
            props: ['cpAliasUrl'],        
            data: function() {
                return {
                    step : 1,
                    dataConfirm : false,
                    rewardDetailData: {
                        cpCardImg : "",
                        cpCOde : "",
                    },
                    commentData: [],
                    replyDate: [],
                    param: {
                    	cpAliasUrl : this.cpAliasUrl,
                        cpTraining : 'N',
                        cpCode : '',
                    },
                    cpCode : '',
                    fundingUrl : '/reward/funding/',
                    training : false,
                }
            },
            components : {
                story : require('./reward-detail-story.js').default.component(),
                comment : require('./reward-comment.js').default.component(),
                info : require('./reward-detail-info.js').default.component(),
            },
            created: function() {
                $('meta[name=url]').attr('content', encodeURIComponent(document.location.href));
                
                $(document).scroll(function () {
                    var scroll = $(this).scrollTop();
                    var gnb = $(".navbar-venture");
                    var cpl = $("#ch-plugin-launcher");

                    var listWrap = $("#reward-detail").position();

                    if (scroll > listWrap.top) {
                        cpl.addClass('affix');
                    } else {
                        cpl.removeClass('affix');
                    }
                });

                this.load();  
            },
            computed : {
                computeCommentCount: function() {
                    return this.commentCountValue;
                }
            },
            methods: {
                load : function() {
                    var self = this;
                    axios.post('/data/view/reward/detail', this.param)
                        .then(function (response) {
                            var result = response.data.rData;
                            if(result == null || result.cpCode == null) {
                                noti.open('잘못된 주소입니다.', function() {window.open("/", '_self');});
                                return;
                            }
                            if (result.cpStatus == '0') {
                                noti.open('비공개 처리된 프로젝트입니다.', function() {window.open("/", '_self');});
                                return;   
                            }
                            self.rewardDetailData = response.data.rData;

                            if (self.rewardDetailData.cpInquiryEmail == null || self.rewardDetailData.cpInquiryEmail == '') {
                                self.rewardDetailData.cpInquiryEmail = self.rewardDetailData.memEmail;
                            }

                            self.cpCode = result.cpCode;

                            //window.dataLayer.push({'dynx_itemid': self.cpAliasUrl, 'dynx_pagetype': 'offerdetail'});  

                            window.dataLayer.push({
                                'event': 'fireRemarketingTag',
                                'google_tag_params': {
                                  'dynx_itemid': self.cpAliasUrl,
                                  'dynx_pagetype': 'offerdetail'
                                }
                               });

                             window.pixelViewData = window.pixelViewData || {};
                            window.pixelViewData = {
                                'content_name' : self.rewardDetailData.cpTitle,
                                'content_ids' : [ self.cpAliasUrl ],
                                'content_type' : 'product',
                                'currency' : 'KRW'
                            };

                            // 거래 데이터 
                            var gtmDataLayer = {
                                    'event' : 'RewardViewContent'
                                };

                            dataLayer.push(gtmDataLayer);

                            self.dataConfirm = !self.dataConfirm;
                        })
                },
                stepChange : function(step) {
                    this.step = step;
                },
                funding : function() {
                    if(!userInfo.loginConfirm()) return;
                    if(this.rewardDetailData.cpEndStatus != 1) return;
                    window.open(this.fundingUrl + this.rewardDetailData.cpAliasUrl, '_self')
                },
                //페이스북 공유
                facebookShare : function() {
                    var self = this;
                    if(!userInfo.loginConfirm()) return;
                    FB.ui({
                        method: 'share',
                        display: 'popup',
                        href: encodeURI(document.location.href) + '?utm_source=facebook&utm_medium=share&utm_campaign=' + self.cpAliasUrl,
                    }, function(response){
                        // 공유를 다 안하고 팝업을 끄면 undefined 반환,
                        // 공유 성공 시 post_id를 반환함
                        if (typeof(response) != "undefined" || response.post_id != '') {
                            noti.open("공유되었습니다.");
                        }
                    });
                },
            },
        }
    }
}

export default new RewardDetail()