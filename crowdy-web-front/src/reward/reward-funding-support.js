class RewardFundingSupport {
    component() {
        return {
            template : `
                <div class="col-md-10 col-line xs-pt15">
                    <!-- 리워드 추가 선택하기 -->
                    <div class="row not-space">
                        <div class="col-md-1"></div>
                        <div class="col-md-11">
                            <div class="label-title">
                                <div class="frame">리워드 추가 선택하기</div>
                                <p class="red-800">(선택사항)</p>
                            </div>
                            <div class="row not-space">
                                <div class="col-xs-12">
                                    <div class="row row-mobile" v-if="dataConfirm">
                                        <!-- Loop %2 == 0 -->
                                        <div class="col-xs-6">
                                            <div class="col-xs-12 hidden-xs" v-for="(item, index) in cardBenefitFilter" v-if="item.cbfRemQty > 0 && (index % 2 == 0)"> 
                                                <div class="st-items st-items-loop" v-on:click="cardChoose(item, 1, $event)" :class="{'active' : item.isActive}">
                                                    <a href="javascript:void(0)" class="st-link">바로가기</a>
                                                    <div class="row not-space">
                                                        <div class="col-xs-6 col-sm-12">
                                                            <div class="st-items-su">
                                                                <strong v-bind:style="[item.isActive ? {'color': '#ff0071'} : {'color': 'rgb(70, 70, 70)'}]"><span class="webfont2">{{ parseInt(item.cbfAmount).toLocaleString() }}</span>원 펀딩</strong>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-6 col-sm-12 xs-text-right">
                                                            <div class="st-items-btn">
                                                                <span class="btn btn-sm btn-danger" v-if="item.cbfRemQty >= 89999">무제한</span>
                                                                <span class="btn btn-sm btn-danger" v-if="item.cbfRemQty < 90000">{{ item.cbfRemQty }}개 남음</span>
                                                                <span class="btn btn-sm btn-danger-outline">{{ parseInt(item.cpBenefitSpsQty).toLocaleString() }}개 펀딩</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <dl>
                                                                <dt>리워드명</dt>
                                                                <dd><strong>{{ item.cbfTitle }}</strong></dd>
                                                            </dl>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <p>{{ item.cbfInfo }}</p>
                                                        </div>
                                                        <div class="col-xs-6 col-sm-12" v-if="item.cbfDevrAddrYn == 'Y'">
                                                            <dl>
                                                                <dt>예상 배송일</dt>
                                                                <dd><strong>{{ item.cbfResDate }}</strong></dd>
                                                            </dl>
                                                        </div>
                                                    </div>

                                                    <!-- 옵션 -->
                                                    <div class="st-items-option">
                                                        <dl>
                                                            <dt>수량</dt>
                                                            <dd><number-input class="form-control" :num="item.qty" v-model="item.qty" maxlength="5" v-on:click.native="cardChoose(item, 2, $event)" placeholder="수량"></number-input></dd>
                                                            <!--<dd><input type="tel" class="form-control" v-on:click="cardChoose(item, 2, $event)" v-model="item.qty" placeholder="수량" maxlength="5" v-on:keypress="isNumber"></dd>-->
                                                        </dl>

                                                        <dl v-for="(items, indexs) in item.cpBenefitOptions">
                                                            <dt>{{ items.cpBenefitOptionTitle }}</dt>
                                                            <dd><input type="text" v-model="item.benefit[indexs]" v-on:click="cardChoose(item, 2, $event)" class="form-control" placeholder="옵션"></dd>
                                                        </dl>
                                                    </div>
                                                    <!-- //옵션 -->
                                                </div>
                                            </div>
                                        </div>
                                        <!-- //Loop -->
                                        <!-- Loop %2 == 1-->
                                        <div class="col-xs-6">
                                            <div class="col-xs-12 hidden-xs" v-for="(item, index) in cardBenefitFilter" v-if="item.cbfRemQty > 0 && (index % 2 == 1)"> 
                                                <div class="st-items st-items-loop" v-on:click="cardChoose(item, 1, $event)" :class="{'active' : item.isActive}">
                                                    <a href="javascript:void(0)" class="st-link">바로가기</a>
                                                    <div class="row not-space">
                                                        <div class="col-xs-6 col-sm-12">
                                                            <div class="st-items-su">
                                                                <strong v-bind:style="[item.isActive ? {'color': '#ff0071'} : {'color': 'rgb(70, 70, 70)'}]"><span class="webfont2">{{ parseInt(item.cbfAmount).toLocaleString() }}</span>원 펀딩</strong>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-6 col-sm-12 xs-text-right">
                                                            <div class="st-items-btn">
                                                                <span class="btn btn-sm btn-danger" v-if="item.cbfRemQty >= 89999">무제한</span>
                                                                <span class="btn btn-sm btn-danger" v-if="item.cbfRemQty < 90000">{{ item.cbfRemQty }}개 남음</span>
                                                                <span class="btn btn-sm btn-danger-outline">{{ parseInt(item.cpBenefitSpsQty).toLocaleString() }}개 펀딩</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <dl>
                                                                <dt>리워드명</dt>
                                                                <dd><strong>{{ item.cbfTitle }}</strong></dd>
                                                            </dl>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <p>{{ item.cbfInfo }}</p>
                                                        </div>
                                                        <div class="col-xs-6 col-sm-12" v-if="item.cbfDevrAddrYn == 'Y'">
                                                            <dl>
                                                                <dt>예상 배송일</dt>
                                                                <dd><strong>{{ item.cbfResDate }}</strong></dd>
                                                            </dl>
                                                        </div>
                                                    </div>

                                                    <!-- 옵션 -->
                                                    <div class="st-items-option">
                                                        <dl>
                                                            <dt>수량</dt>
                                                            <dd><number-input class="form-control" :num="item.qty" v-model="item.qty" maxlength="5" v-on:click.native="cardChoose(item, 2, $event)" placeholder="수량"></number-input></dd>
                                                            <!--<dd><input type="tel" class="form-control" v-on:click="cardChoose(item, 2, $event)" v-model="item.qty" placeholder="수량" maxlength="5" v-on:keypress="isNumber"></dd>-->
                                                        </dl>

                                                        <dl v-for="(items, indexs) in item.cpBenefitOptions">
                                                            <dt>{{ items.cpBenefitOptionTitle }}</dt>
                                                            <dd><input type="text" v-model="item.benefit[indexs]" v-on:click="cardChoose(item, 2, $event)" class="form-control" placeholder="옵션"></dd>
                                                        </dl>
                                                    </div>
                                                    <!-- //옵션 -->
                                                </div>
                                            </div>
                                        </div>
                                        <!-- //Loop -->
                                        <!-- Loop 모바일 -->
                                        <div class="col-sm-6 hidden-lg hidden-md hidden-sm" v-for="(item, index) in cardBenefitData" v-if="item.cbfRemQty > 0"> 
                                            <div class="st-items st-items-loop" v-on:click="cardChoose(item, 1, $event)" :class="{'active' : item.isActive}">
                                                <a href="javascript:void(0)" class="st-link">바로가기</a>
                                                <div class="row not-space">
                                                    <div class="col-xs-6 col-sm-12">
                                                        <div class="st-items-su">
                                                            <strong v-bind:style="[item.isActive ? {'color': '#ff0071'} : {'color': 'rgb(70, 70, 70)'}]"><span class="webfont2">{{ parseInt(item.cbfAmount).toLocaleString() }}</span>원 펀딩</strong>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-12 xs-text-right">
                                                        <div class="st-items-btn">
                                                            <span class="btn btn-sm btn-danger" v-if="item.cbfRemQty >= 89999">무제한</span>
                                                            <span class="btn btn-sm btn-danger" v-if="item.cbfRemQty < 90000">{{ item.cbfRemQty }}개 남음</span>
                                                            <span class="btn btn-sm btn-danger-outline">{{ parseInt(item.cpBenefitSpsQty).toLocaleString() }}개 펀딩</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <dl>
                                                            <dt>리워드명</dt>
                                                            <dd><strong>{{ item.cbfTitle }}</strong></dd>
                                                        </dl>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <p>{{ item.cbfInfo }}</p>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-12" v-if="item.cbfDevrAddrYn == 'Y'">
                                                        <dl>
                                                            <dt>예상 배송일</dt>
                                                            <dd><strong>{{ item.cbfResDate }}</strong></dd>
                                                        </dl>
                                                    </div>
                                                </div>

                                                <!-- 옵션 -->
                                                <div class="st-items-option">
                                                    <dl>
                                                        <dt>수량</dt>
                                                        <dd><number-input class="form-control" :num="item.qty" v-model="item.qty" maxlength="5" v-on:click.native="cardChoose(item, 2, $event)" placeholder="수량"></number-input></dd>
                                                        <!--<dd><input type="tel" class="form-control" v-on:click="cardChoose(item, 2, $event)" v-model="item.qty" placeholder="수량" maxlength="5" v-on:keypress="isNumber"></dd>-->
                                                    </dl>

                                                    <dl v-for="(items, indexs) in item.cpBenefitOptions">
                                                        <dt>{{ items.cpBenefitOptionTitle }}</dt>
                                                        <dd><input type="text" v-model="item.benefit[indexs]" v-on:click="cardChoose(item, 2, $event)" class="form-control" placeholder="옵션"></dd>
                                                    </dl>
                                                </div>
                                                <!-- //옵션 -->
                                            </div>
                                        </div>
                                        <!-- //Loop -->
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                                                    
                            <div class="label-title" v-if="finishBenefit > 0">
                                <div class="frame">펀딩완료 리워드</div>
                            </div>
                            <div class="row not-space">
                                <div class="col-xs-12">
                                    <div class="row row-mobile" v-if="dataConfirm">
                                        <!-- Loop %2 == 0 -->
                                        <div class="col-xs-6">
                                            <div class="col-xs-12 hidden-xs" v-for="(item, index) in cardBenefitFinishFilter" v-if="item.cbfRemQty == 0 && (index % 2 == 0)"> 
                                                <div class="st-items st-items-loop">
                                                    <a href="javascript:void(0)" class="st-link">바로가기</a>
                                                    <div class="row not-space">
                                                        <div class="col-xs-6 col-sm-12">
                                                            <div class="st-items-su">
                                                                <strong v-bind:style="[item.isActive ? {'color': '#ff0071'} : {'color': 'rgb(70, 70, 70)'}]"><span class="webfont2">{{ parseInt(item.cbfAmount).toLocaleString() }}</span>원 펀딩</strong>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-6 col-sm-12 xs-text-right">
                                                            <div class="st-items-btn">
                                                                <span class="btn btn-sm btn-danger" v-if="item.cbfRemQty >= 89999">무제한</span>
                                                                <span class="btn btn-sm btn-danger" v-if="item.cbfRemQty < 90000">{{ item.cbfRemQty }}개 남음</span>
                                                                <span class="btn btn-sm btn-danger-outline">{{ parseInt(item.cpBenefitSpsQty).toLocaleString() }}개 펀딩</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <dl>
                                                                <dt>리워드명</dt>
                                                                <dd><strong>{{ item.cbfTitle }}</strong></dd>
                                                            </dl>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <p>{{ item.cbfInfo }}</p>
                                                        </div>
                                                        <div class="col-xs-6 col-sm-12" v-if="item.cbfDevrAddrYn == 'Y'">
                                                            <dl>
                                                                <dt>예상 배송일</dt>
                                                                <dd><strong>{{ item.cbfResDate }}</strong></dd>
                                                            </dl>
                                                        </div>
                                                    </div>

                                                    <!-- 옵션 -->
                                                    <!--<div class="st-items-option">
                                                        <dl>
                                                            <dt>수량</dt>
                                                            <dd><number-input class="form-control" :num="item.qty" v-model="item.qty" maxlength="5" v-on:click.native="cardChoose(item, 2, $event)" placeholder="수량"></number-input></dd>
                                                        </dl>

                                                        <dl v-for="(items, indexs) in item.cpBenefitOptions">
                                                            <dt>{{ items.cpBenefitOptionTitle }}</dt>
                                                            <dd><input type="text" v-model="item.benefit[indexs]" v-on:click="cardChoose(item, 2, $event)" class="form-control" placeholder="옵션"></dd>
                                                        </dl>
                                                    </div>-->
                                                    <!-- //옵션 -->
                                                </div>
                                            </div>
                                        </div>
                                        <!-- //Loop -->
                                        <!-- Loop %2 == 1-->
                                        <div class="col-xs-6">
                                            <div class="col-xs-12 hidden-xs" v-for="(item, index) in cardBenefitFinishFilter" v-if="item.cbfRemQty == 0 && (index % 2 == 1)"> 
                                                <div class="st-items st-items-loop">
                                                    <a href="javascript:void(0)" class="st-link">바로가기</a>
                                                    <div class="row not-space">
                                                        <div class="col-xs-6 col-sm-12">
                                                            <div class="st-items-su">
                                                                <strong v-bind:style="[item.isActive ? {'color': '#ff0071'} : {'color': 'rgb(70, 70, 70)'}]"><span class="webfont2">{{ parseInt(item.cbfAmount).toLocaleString() }}</span>원 펀딩</strong>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-6 col-sm-12 xs-text-right">
                                                            <div class="st-items-btn">
                                                                <span class="btn btn-sm btn-danger" v-if="item.cbfRemQty >= 89999">무제한</span>
                                                                <span class="btn btn-sm btn-danger" v-if="item.cbfRemQty < 90000">{{ item.cbfRemQty }}개 남음</span>
                                                                <span class="btn btn-sm btn-danger-outline">{{ parseInt(item.cpBenefitSpsQty).toLocaleString() }}개 펀딩</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <dl>
                                                                <dt>리워드명</dt>
                                                                <dd><strong>{{ item.cbfTitle }}</strong></dd>
                                                            </dl>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <p>{{ item.cbfInfo }}</p>
                                                        </div>
                                                        <div class="col-xs-6 col-sm-12" v-if="item.cbfDevrAddrYn == 'Y'">
                                                            <dl>
                                                                <dt>예상 배송일</dt>
                                                                <dd><strong>{{ item.cbfResDate }}</strong></dd>
                                                            </dl>
                                                        </div>
                                                    </div>

                                                    <!-- 옵션 -->
                                                    <!--<div class="st-items-option">
                                                        <dl>
                                                            <dt>수량</dt>
                                                            <dd><number-input class="form-control" :num="item.qty" v-model="item.qty" maxlength="5" v-on:click.native="cardChoose(item, 2, $event)" placeholder="수량"></number-input></dd>
                                                        </dl>

                                                        <dl v-for="(items, indexs) in item.cpBenefitOptions">
                                                            <dt>{{ items.cpBenefitOptionTitle }}</dt>
                                                            <dd><input type="text" v-model="item.benefit[indexs]" v-on:click="cardChoose(item, 2, $event)" class="form-control" placeholder="옵션"></dd>
                                                        </dl>
                                                    </div>-->
                                                    <!-- //옵션 -->
                                                </div>
                                            </div>
                                        </div>
                                        <!-- //Loop -->
                                        <!-- Loop 모바일 -->
                                        <div class="col-sm-6 hidden-lg hidden-md hidden-sm" v-for="(item, index) in cardBenefitFinishFilter" v-if="item.cbfRemQty == 0"> 
                                            <div class="st-items st-items-loop">
                                                <a href="javascript:void(0)" class="st-link">바로가기</a>
                                                <div class="row not-space">
                                                    <div class="col-xs-6 col-sm-12">
                                                        <div class="st-items-su">
                                                            <strong v-bind:style="[item.isActive ? {'color': '#ff0071'} : {'color': 'rgb(70, 70, 70)'}]"><span class="webfont2">{{ parseInt(item.cbfAmount).toLocaleString() }}</span>원 펀딩</strong>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-12 xs-text-right">
                                                        <div class="st-items-btn">
                                                            <span class="btn btn-sm btn-danger" v-if="item.cbfRemQty >= 89999">무제한</span>
                                                            <span class="btn btn-sm btn-danger" v-if="item.cbfRemQty < 90000">{{ item.cbfRemQty }}개 남음</span>
                                                            <span class="btn btn-sm btn-danger-outline">{{ parseInt(item.cpBenefitSpsQty).toLocaleString() }}개 펀딩</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <dl>
                                                            <dt>리워드명</dt>
                                                            <dd><strong>{{ item.cbfTitle }}</strong></dd>
                                                        </dl>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <p>{{ item.cbfInfo }}</p>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-12" v-if="item.cbfDevrAddrYn == 'Y'">
                                                        <dl>
                                                            <dt>예상 배송일</dt>
                                                            <dd><strong>{{ item.cbfResDate }}</strong></dd>
                                                        </dl>
                                                    </div>
                                                </div>

                                                <!-- 옵션 -->
                                                <!--<div class="st-items-option">
                                                    <dl>
                                                        <dt>수량</dt>
                                                        <dd><number-input class="form-control" :num="item.qty" v-model="item.qty" maxlength="5" v-on:click.native="cardChoose(item, 2, $event)" placeholder="수량"></number-input></dd>
                                                    </dl>

                                                    <dl v-for="(items, indexs) in item.cpBenefitOptions">
                                                        <dt>{{ items.cpBenefitOptionTitle }}</dt>
                                                        <dd><input type="text" v-model="item.benefit[indexs]" v-on:click="cardChoose(item, 2, $event)" class="form-control" placeholder="옵션"></dd>
                                                    </dl>
                                                </div>-->
                                                <!-- //옵션 -->
                                            </div>
                                        </div>
                                        <!-- //Loop -->
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            <hr class="big" />
                        </div>
                    </div>
                    <!-- //리워드 추가 선택하기 -->

                    <div class="row not-space">
                        <div class="col-md-1"></div>
                        <div class="col-md-11">
                            <form class="form-horizontal">
                                <!-- 추가 펀딩하기 -->
                                <div class="form-group not-space">
                                    <label for="plus_funding" class="col-xs-12 control-label control-label-big xs-pt0 xs-pb0">
                                        <div class="text-left">
                                            추가 펀딩하기
                                            <p class="red-800">(선택사항)</p>
                                        </div>
                                    </label>
                                    <div class="col-xs-7 col-sm-4">
                                        <number-input class="form-control" :num="funding.spsAmount" v-model="funding.spsAmount" maxlength="15"></number-input>
                                        <p class="form-control-static mb5">후원금을 더하여 펀딩 할 수 있습니다.</p>
                                    </div>
                                    <div class="col-xs-5">
                                        <div class="mt5 ml10">원을 추가 펀딩</div>
                                    </div>
                                </div>
                                <hr class="big" />
                                <!-- //추가 펀딩하기 -->
                                <div class="row st-submit">
                                    <div class="col-sm-3">
                                        <a class="btn btn-lg btn-block btn-primary" v-on:click="nextStep">다음 단계</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            `,
            props : ['code', 'id'],
            data : function() {
                return {
                    cardBenefitData : [],
                    memShotImg : "",
                    param : {
                        cpCode : this.code,
                        cbfStatus : '1',
                    },
                    dataConfirm : false,
                    cardClickSeparation : '',
                    funding : {
                        list : [],
                        spsAmount : 0,
                        memCode : userInfo.memCode,
                        cpCode : this.code,
                        id : userInfo.memCode + this.code + Math.random().toString(36).substr(2, 5),
                    },
                    url : '',
                    finishBenefit : 0,
                    selectedTotalAmount : 0
                }
            },
            components : {
				numberInput : require('../common/number-input.js').default.component(),
			},
            created : function() {
                this.load();
            },
            computed : {
                cardBenefitFilter : function() {
                    return this.cardBenefitData.filter(function(cardBenefitData) {
                        return cardBenefitData.cbfRemQty > 0;
                    })
                },
                cardBenefitFinishFilter : function() {
                    var self = this;
                    return this.cardBenefitData.filter(function(cardBenefitData) {
                        if(cardBenefitData.cbfRemQty == 0) {
                            self.finishBenefit++;
                        }
                        return cardBenefitData.cbfRemQty == 0;
                    })
                },
            },
            methods : {
                load : function() {
                    //this.memShotImg = JSON.parse(localStorage.getItem('user')).memShotImg;
                    var self = this;
                    axios.post('/data/view/reward/benefit-list', this.param)
                    .then(function (response) {
                        var result = response.data.rData;
                        self.cardBenefitData = result;
                        _.forEach(self.cardBenefitData, function(value) {
                            self.$set(value, 'isActive', false)
                            self.$set(value, 'benefit', [])
                            self.$set(value, 'qty', 1)
                        });
                        self.dataConfirm = true;
                    })
                },
                isNumber : function(evt) {
                    evt = (evt) ? evt : window.event;
                    var charCode = (evt.which) ? evt.which : evt.keyCode;
                    if ((charCode > 31 && (charCode < 48 || charCode > 57)) && charCode !== 46) {
                      evt.preventDefault();
                    } else {
                      return true;
                    }
                },
                cardChoose : function(item, val, e) {
                    this.cardClickSeparation = val;                    
                    if(this.cardClickSeparation == 1) {
                        item.isActive = !item.isActive;
                    } else {
                        if(item.isActive == true) {
                            e.stopPropagation();
                        }
                    }                    
                },
                nextStep : function() {
                    var self = this;
                    var check = false;
                    _.forEach(this.cardBenefitData, function(value) {
                        if(value.isActive) {
                            var data = {
                                cbfIdx : value.cbfIdx,
                                cpCode : self.param.cpCode,
                                cbfTitle : value.cbfTitle,
                                sbfUnitPrice : value.cbfAmount,
                                cbfInfo : value.cbfInfo,
                                cbfDevrAddrYn : value.cbfDevrAddrYn,
                                benefit : value.benefit.toString(),
                                qty : value.qty,
                                cbfRemQty : value.cbfRemQty,
                                cpBenefitOptions : value.cpBenefitOptions
                            }
                            self.funding.list.push(data)
                            check = true;
                        }
                    });

                    if(check) {
                        var selfCheck = false;
                        _.forEach(this.funding.list, function(value) {
                            if(value.qty == 0 || (value.qty == "")) {
                                noti.open("선택하신 리워드의 수량을 정확히 입력해주세요.");
                                self.funding.list = [];
                                selfCheck = true;
                                return;
                            }

                            var qty = parseInt(value.qty);
                            var cpBenefitRemQty = parseInt(value.cbfRemQty);
                            
                            if((cpBenefitRemQty < 999999) && (qty > cpBenefitRemQty)) {
                                noti.open("선택하신 리워드의 수량이 부족합니다. <br />다시 한 번 수량을 확인해주세요.");
                                selfCheck = true;
                                self.funding.list = [];
                                return;
                            }
                            if(value.cpBenefitOptions.length > 0 && value.benefit == '') {
                                noti.open("선택하신 리워드에 옵션을 적어주세요!")
                                selfCheck = true;
                                self.funding.list = [];
                                return;
                            }
                        });  
                        if(selfCheck) {
                            return;
                        }
                    } else {
                        if(self.funding.spsAmount == 0) {
                            noti.open("리워드 카드를 선택하지 않으셨습니다. <br />리워드 카드를 선택해주세요.")
                            this.funding.list = [];
                            return;
                        } else {
                            noti.open("리워드 카드를 선택하지 않고 추가 펀딩만 하셨습니다. <br />이럴 경우, 리워드는 제공되지 않습니다. <br />이대로 진행하시겠습니까?", self.save, true);
                            return;
                        }
                    }
                    this.save();
                },
                save : function() {
                    var self = this;

                    // TODO : 프로젝트 1인당 펀딩금액 제한
                    if(this.code == '1000003999') {

                        var limitPoint = 5000000;

                         self.selectedTotalAmount = 0;
                        for (var i = 0; i < self.funding.list.length; i++) {
                            var card = self.funding.list[i]
                            self.selectedTotalAmount +=  parseInt(card.sbfUnitPrice) * parseInt(card.qty);
                        }
                        self.selectedTotalAmount += parseInt(self.funding.spsAmount);

                        if (parseInt(self.selectedTotalAmount) > parseInt(limitPoint)) {
                            noti.open('현재 진행 중인 프로젝트는 <br /> 1인당 ' + parseInt(limitPoint).toLocaleString() + '원까지 펀딩가능합니다.');
                            return;
                        }

                        axios.post('/data/reward/funding/info/amount', { memCode : userInfo.memCode, cpCode : this.param.cpCode })
                            .then(function (response) {
                                var result = response.data.rData;

                                if(parseInt(self.selectedTotalAmount) + parseInt(result.spsTotAmount) > limitPoint) {

                                    if (result.spsTotAmount > 0) {
                                        noti.open('현재 진행 중인 프로젝트는 <br /> 1인당 ' + parseInt(limitPoint).toLocaleString() + '원까지 펀딩가능합니다. <br/> 현재 프로젝트에 이미 ' + parseInt(result.spsTotAmount).toLocaleString() +'원 펀딩하셨습니다.')
                                    } else {
                                        noti.open('현재 진행 중인 프로젝트는 <br /> 1인당 ' + parseInt(limitPoint).toLocaleString() + '원까지 펀딩가능합니다.')
                                    }
                                    // self.funding.list = [];
                                    // self.spsTotAmount = 0;
                                    return;
                                } else {
                                    self.saveBenefit();
                                }
                            })
                    } else {
                       self.saveBenefit();
                    }

                    // axios.post('/reward/funding/set/benefit', this.funding)
                    // .then(function (response) {
                    //     self.$emit('set-id', self.funding.id);
                    //     self.$emit('step-change', 2);
                    // });  
                },
                saveBenefit : function() {
                    var self = this;
                    axios.post('/reward/funding/set/benefit', this.funding)
                    .then(function (response) {
                        self.$emit('set-id', self.funding.id);
                        self.$emit('step-change', 2);
                    });
                }

            }
        }
    }
}
export default new RewardFundingSupport