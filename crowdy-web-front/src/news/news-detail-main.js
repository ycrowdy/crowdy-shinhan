class NewsDetailMain {
	component() {
		return {
			template : `
				<div class="list_wrap">
                    <div class="container">
                        <div class="row not-space mt20">
                            <div class="col-md-3">
                                <div class="list_title list_title_mb xs-mb0 webfont2">
                                    크라우디<br />
                                    새소식
                                </div>
                                <div class="list_summury list_summury_big hidden-xs">
                                    크라우디에서 새로운 <br />
                                    유익한 소식들을 <br class="hidden-sm" />
                                    받아보세요!!
                                </div>
                            </div>
                            
                            <news-detail :search="search"></news-detail>
                            

                        </div>
                    </div>
                </div>
			`,
			props : ['newsIdx'],
			data : function() {
				return {
					search : {
                        newsIdx : this.newsIdx,
                    }
				}
			},
			components : {
				newsDetail : require('./news-detail.js').default.component()
            }
		}
	}
}

export default new NewsDetailMain()
