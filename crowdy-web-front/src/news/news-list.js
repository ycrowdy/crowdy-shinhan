class NewsList {
    component() {
        return {
            template : `
            <div>
                <div class="col-md-9">
                    <div class="row row-mobile">
                        <!-- Loop -->
                        <div class="col-xs-6 col-sm-4" v-for="item in newsList">
                            <a :href="'/news/detail/' + item.newsIdx" class="thumbnail over-box">
                                <img :src="'//' + item.newsImg + '/ycrowdy/resize/!740x!492'" class="img-responsive" alt="" />
                                <div class="caption-partner">
                                    <div class="caption-line-news">
                                        <span class="caption-subject">{{ item.newsTitle }}</span>
                                        <div class="caption-date">{{ item.wdate }}</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <!-- //Loop -->
                    </div>

                    <!-- 페이징 -->
                    <nav class="text-center mt30 mb20" >
                        <paginate
                            :page-count="pageCount"
                            :class="'pagination'"
                            :click-handler="nextPage"
                            :force-page="forcePage"
                            >
                        </paginate>
                    </nav>
                    <!-- //페이징 -->
                </div>
            </div>
            `,
            data : function() {
                return {
                    newsList : [],
                    pageCount : 0,
                    forcePage : 0,
                    search : {
                        paging : {
			                page : "1",
			                count : "12"
			            }
                    }
                }
            },
            created : function() {
                this.load();
            },
            components : {
                paginate : VuejsPaginate
            },
            methods : {
                load : function() {
                    var self = this;
                    axios.post('/data/crowdy/news/list', self.search)
                        .then(function(response){
                            var remainder = parseInt(response.data.rData.count % self.search.paging.count);
                            var remainderCount = 0;
                            if(remainder > 0) {
                                remainderCount++;
                            }
                            self.pageCount = (parseInt(response.data.rData.count / self.search.paging.count)) + remainderCount;
                            self.newsList = response.data.rData.data;
                        })
                },
                nextPage : function(page) {
                    this.forcePage = page - 1;
                    this.search.paging.page = page;
                    this.load();
                },
                init :  function() {
                    this.search.paging.page = "1";
                    this.pageCount = 0;
                    this.forcePage = 0;
                    this.newsList = [];
                    this.load();
                }   
            }
        }
    }
}        
        
export default new NewsList()