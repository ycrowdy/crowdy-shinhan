class News {
	component() {
		return {
			template : `
				<div class="col-md-9">
					<!-- View -->
					<div class="panel panel-news">
						<div class="panel-frame">
							<div class="panel-heading">
								<h3 class="panel-title" v-text="news.newsTitle"></h3>
							</div>
							<div class="list-group list-group-bordered list-group-m pl0 pr0">
								<div class="list-group-item pl0 pr0">
									<div class="row">
										<div class="col-xs-7" v-text="news.wdate">
										</div>
									</div>
								</div>
							</div>
							<div class="panel-body mce-content-body" v-html="news.newsContent">
								
							</div>
							<div class="list-group list-group-bordered list-group-ne">
								<a class="list-group-item text-truncate" v-if="news.nextNewsIdx != ''" :href="'/news/detail/' + news.nextNewsIdx">
									<i class="fa fa-angle-up mr15" aria-hidden="true"></i>{{ news.nextNewsTitle }}
								</a>
								<a class="list-group-item text-truncate" v-if="news.preNewsIdx != ''" :href="'/news/detail/' + news.preNewsIdx">
									<i class="fa fa-angle-down mr15" aria-hidden="true"></i>{{ news.preNewsTitle }}
								</a>
							</div>
						</div>
					</div>

					<div class="text-center mb20">
						<a href="/news/main" class="btn btn-primary-outline">목록보기</a>
					</div>
					<!-- //View -->
				</div>
			`,
			props : ['search'],
			data : function() {
				return {
					news : {}
				}
			},
			created : function() {
				this.load();
			},
			methods : {
				load : function() {
					var self = this;
					axios.post('/data/crowdy/news/detail', this.search)
                        .then(function(response){
                            var result = response.data.rData;
                            if(result == null) {
                                noti.open('잘못된 주소입니다.', function() {window.open("/news/main", '_self');});
                                return;
                            }
                            self.news = result;
                        })
				}
			}
		}
	}
}

export default new News()