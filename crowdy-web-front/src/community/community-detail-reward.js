class CommunityDetailReward {
	component() {
		return {
			template : `
			<div>
				<div class="rewards-list">
					<div class="row row-mobile">
						<div class="col-sm-4 col-md-3" v-for="(item, index) in community" v-if="(item.communityConfirmStatus != 0) || (item.communityConfirmStatus == 0 && userMemCode == memCode)" v-on:click="detail(item, $event)">
							<figure>
								<a href="javascript:void(0)" class="items over-box">
									<div v-if="item.communityConfirmStatus == 0" class="items_img pointer">
										<div class="coming-type webfont2">
											<div>
												<span>승인 대기중</span>
											</div>										
										</div>
										<img :src="'//' + item.cpCardImg + '/ycrowdy/resize/!340x!226'" class="img-responsive" alt="..." />
									</div>			
									<div v-else-if="item.communityConfirmStatus == 1" class="items_img">
										<img :src="'//' + item.cpCardImg + '/ycrowdy/resize/!340x!226'" class="img-responsive" alt="..." />
									</div>
									<figcaption class="rewards-caption">
		                                <div class="rewards-subject">
		                                    <strong>{{item.cpTitle}}</strong>
		                                    <div class="rewards-summury" v-if="item.communityName != ''">{{item.communityName}}</div>
		                                </div>

										<div v-if="item.communityConfirmStatus == 0">
											<div class="coming-soon-btn">
												<div class="row row-mobile-n">
													<div class="col-xs-6">
														<a class="btn btn-sm btn-block btn-primary-outline" v-on:click="showModal($event, 0, item.cpCode, item.cpTitle)">승인</a>
													</div>
													<div class="col-xs-6">
														<a class="btn btn-sm btn-block btn-default-outline" v-on:click="showModal($event, 1, item.cpCode, item.cpTitle)">반려</a>
													</div>
												</div>
											</div>
										</div>
										<div v-else-if="item.communityConfirmStatus == 1">
			                                <span class="rewards-price"><span class="webfont2">₩</span> {{ parseInt(item.cpCurrentAmount).toLocaleString() }} </span>
			                                <span class="rewards-percent">{{item.cpRate}}%</span>

			                                <div class="progress">
			                                    <div class="progress-bar" role="progressbar" :aria-valuenow="item.cpRate" aria-valuemin="0" aria-valuemax="100" :style="{width: item.cpRate + '%'}"><span class="sr-only">{{item.cpRate}}% 완료</span></div>
			                                </div>

			                                <div class="row not-space">
			                                    <div class="col-xs-6">
			                                        <div class="rewards-support">{{item.cpSponsorCount}}명 후원</div>
			                                    </div>
			                                    <div class="col-xs-6 text-right">
			                                        <span class="rewards-day" v-if="item.cpDday > 0">D - {{item.cpDday}}</span>
			                                        <span class="rewards-day rewards-day-end" v-else-if="item.cpDday == 0">{{ endTime }}</span>
			                                        <span class="rewards-day rewards-day-end" v-else="item.cpDday < 0">종료</span>
			                                    </div>
			                                </div>
		                                </div>
		                            </figcaption>
								</a>
							</figure>
						</div>
					</div>
				</div>

				<div class="text-center mt75 mb20 md-mt65 md-mb0 m-mt50 m-mb20 xs-mt30 xs-mb0">
		            <a href="javascript:void(10)" v-show="moreShow" v-on:click="more()" class="btn btn-primary-outline">더보기</a>
		        </div>
		        <approval-modal :cp-code="itemCode" :cp-title="itemTitle" :community-idx="communityIdx" v-on:load="load"></approval-modal>
		    </div>
			`,
			props : {
				communityIdx : {
					required: true
				},
				memCode : {
					required: true
				},
				paramEndType : {
                    default : "3"
                },
				paramOrderType : {
					default : "1"
				},
				paramSearchCount : {
                    default : "12"
                }
			},
            data: function() {
            	return {
			  		request : {
					    orderType: this.paramOrderType,
					    endType: this.paramEndType,
					    communityIdx: this.communityIdx,
			            paging : {
			                page : "1",
			                count : this.paramSearchCount
			            }
			        },
			        itemCode: "",
			        itemTitle: "",
			  		community: [],
				    moreShow : true,
                    //리워드 상세, 버튼클릭 구분
                    check : false,
				}
		  	},
		  	created : function() {
                this.load();
            },
            computed : {
            	userMemCode : function() {
            		return userInfo.memCode
            	},
            	endTime : function() {
                    moment.updateLocale('en', {
                        relativeTime : {
                            hh : "%d"
                        }
                    })
                    return moment().endOf('day').fromNow(true) + " 시간 남음"
                }
            },
            watch : {
            	paramOrderType : function(value) {
            		this.request.orderType = value;
                    this.request.paging.page = "1";
                    this.community = [];
                    this.load();
            	}
            },
		  	methods : {
		  		showModal : function(e, mode, cpCode, cpTitle) {
                    this.check = !this.check;

                    if(this.check) {
                        this.itemCode = cpCode;
			  			this.itemTitle = cpTitle;
			  			
                        e.stopPropagation();
			  			if(mode === 0) {
			  				$('#approvalModal').modal('show');	
			  			} else {
			  				$('#denyModal').modal('show');	
			  			}
                    }
		  		},
	            load : function() {
	                var self = this;

	                this.request.communityIdx = this.communityIdx;

	                axios.post('/data/view/reward/list', this.request)
	                    .then(function(response){
	                    	self.community = _.concat(self.community, response.data.rData);		
	                    	if(!self.moreEnd() || response.data.rData.length == 0) {
                                self.moreShow = false;
                            }
	                    })
	            },
	            more : function() {
                    this.request.paging.page  = _.toString(_.add(_.toNumber(this.request.paging.page), 1));
                    this.load();
                },
                moreEnd : function() {
                    return (this.community.length % this.request.paging.count == 0) ? true : false;
                },
                //프로젝트 상세로 이동
                detail : function(data) {
                    this.check = false;
                    window.open('/r/' + data.cpAliasUrl, '_self')
                },
            },
            components : {
            	approvalModal : require('./community-project-approval-deny.js').default.component()
            }

		}
	}
}

export default new CommunityDetailReward()