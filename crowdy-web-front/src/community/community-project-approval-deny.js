class CommunityProjectApprovalDeny {
	component() {
		return {
			template:`
				<div id="modalVue">
					<!-- 승인 -->
					<div id="approvalModal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
						<div class="modal-dialog modal-sm" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								</div>
								<div class="modal-body modal-order">
									<p>
										{{cpTitle}} <br />
										참여를 승인하시겠습니까?
									</p>

									<div class="modal-footer text-center">
										<div class="row row-mobile-n">
											<div class="col-xs-8 col-xs-offset-2">
												<div class="row row-mobile-n">
													<div class="col-xs-6">
														<a v-on:click="approveCampaign()" class="btn btn-block btn-primary-outline" data-dismiss="modal">예</a>
													</div>
													<div class="col-xs-6">
														<button type="button" class="btn btn-block btn-default-outline" data-dismiss="modal">아니오</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- //승인 -->

					<!-- 반려 -->
					<div id="denyModal" class="modal fade" role="dialog" tabindex="-1" data-backdrop="static" data-keyboard="false">
						<div class="modal-dialog modal-sm">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								</div>
								<div class="modal-body modal-order">
									<p>
										{{cpTitle}} <br />
										참여를 반려하시겠습니까?
									</p>

									<div class="modal-footer text-center">
										<div class="row row-mobile-n">
											<div class="col-xs-8 col-xs-offset-2">
												<div class="row row-mobile-n">
													<div class="col-xs-6">
														<a class="btn btn-block btn-primary-outline" data-dismiss="modal" data-toggle="modal" data-target="#denyWriteModal">예</a>
													</div>
													<div class="col-xs-6">
														<a class="btn btn-block btn-default-outline" data-dismiss="modal">아니오</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- //반려 -->

					<!-- 반려사유 작성 -->
					<div id="denyWriteModal" class="modal fade" aria-hidden="true" aria-labelledby="denyWriteModal" role="dialog" tabindex="-1" data-backdrop="static" data-keyboard="false">
						<div class="modal-dialog modal-sm">
							<form class="modal-content form-horizontal" action="#">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								</div>
								<div class="modal-body modal-order">
									<p>
										반려 사유를 작성해주세요. <br />
										프로젝트 제작자에게 메지지로 전달됩니다.
									</p>

									<div class="form-group row-mobile-n">
										<div class="col-xs-12">
											<textarea rows="3" cols="40" class="form-control" v-model="request.communityProjectReturnReason" placeholder="내용을 입력하세요"></textarea>
										</div>
									</div>

									<div class="modal-footer text-center">
										<div class="row not-space">
											<div class="col-xs-4 col-xs-offset-4">
												<a v-on:click="rejectCampaign()" class="btn btn-block btn-primary-outline" data-dismiss="modal">보내기</a>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- //반려사유 작성 -->
				</div>
				`,
			props: ['cpCode', 'cpTitle', 'communityIdx'],
			data: function(){
            	return {
			  		request : {
			  			cpCode: this.cpCode,
			  			communityIdx: this.communityIdx,
			  			communityProjectReturnReason: ""
			  		}
			  	}
		  	},
	        methods: {
	        	approveCampaign : function() {
		            var self = this;
		            this.request.communityProjectReturnReason = "캠페인 승인 요청";
		            this.request.cpCode = this.cpCode;
		            axios.post('/data/save/community/project/apply', this.request)
		                .then(function(response){
		                	if (response.data.rCode == 0) {
		                		noti.open("해당 프로젝트의 승인이 완료되었습니다.")
		                    	self.$emit('load');
		                	} else {
		                		noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
		                	}
		                })
		        },
		        rejectCampaign : function() {
		            var self = this;
		            this.request.cpCode = this.cpCode;
		            axios.post('/data/save/community/project/trans', this.request)
		                .then(function(response){
		                	if (response.data.rCode == 0) {
		                		noti.open("반려되었습니다.")
		                		self.$emit('load');
		                	} else {
		                		noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
		                	}
		                })
		        }
	        }
		}
	}
}

export default new CommunityProjectApprovalDeny()