class CommunityList {
	component() {
		return {
			template : `
				<div>
					<div id="list-category">
						<div class="list_category">
							<div class="container">
								<div class="row row-mobile">
									<!-- <div class="col-xs-6 col-sm-4 col-md-3"> -->
										<!-- <order-select :options="searchTypeOptions" v-model="searchType"></order-select> -->
									<!-- </div> -->
									<div class="col-xs-6 col-sm-4 col-md-3">
										<order-select :options="orderTypeOptions" v-model="orderType"></order-select>
									</div>
									<template v-if="name != ''">
										<div class="col-xs-18 col-sm-8 col-md-9 text-right xs-text-center s-text hidden-xs">
											<a :href="'/community/make?index=' + status.communityIdx" v-if="status.communityStatus == '2'" class="btn btn-primary-outline ml10 xs-mt10">파트너 신청하기</a>
											<a :href="'/c/' + status.communityUrl" v-else-if="status.communityStatus == '1'" class="btn btn-primary-outline ml10 xs-mt10">내 커뮤니티 파트너 바로가기</a>
											<a href="javascript:void(0)" v-else-if="status.communityStatus == '0'" class="btn btn-primary-outline ml10 xs-mt10" disabled>신청중</a>
											<a href="/community/make" v-if="status.communityIdx == ''" class="btn btn-primary-outline ml10 xs-mt10">파트너 신청하기</a>
										</div>
									</template>
								</div>
							</div>
						</div>
					</div>

					<div class="container">
						<div class="rewards-list mc-list">
							<div id="mc_list_load" class="row row-mobile">
								<div class="col-sm-4 col-md-3" v-for="(item, index) in community">
									<figure>
										<a :href="'/c/' + item.communityUrl" class="items over-box">
											<div class="items_img">
												<img :src="'//' + item.communityImg + '/ycrowdy/resize/!350x!350'" class="img-responsive" />
											</div>
											<figcaption class="rewards-caption">
												<div class="rewards-subject">
													<strong>{{ item.communityName }}</strong>
													<div class="rewards-summury rewards-summury-nowrap grey-800">{{item.communityInfo}}</div>
												</div>

												<div class="row row-mobile-n">
													<div class="col-xs-12 mc-tlabel">커뮤니티 파트너</div>
													<div class="col-xs-4 mc-label">총 모금액</div>
													<div class="col-xs-8 text-right mc-price">
														<span class="webfont2">₩</span>{{parseInt(item.communityCurrentAmount).toLocaleString()}}
													</div>
													<div class="col-xs-6 mc-label">총 프로젝트</div>
													<div class="col-xs-6 text-right mc-count">{{item.communityCpCount}} 개</div>
												</div>

												<hr class="thin hidden-xs" />

												<div class="row row-mobile-n xs-mt15">
													<div class="col-xs-12 mc-tlabel">모의펀딩</div>
													<div class="col-xs-4 mc-label">총 모금액</div>
													<div class="col-xs-8 text-right mc-price">
														<span class="webfont2">₩</span>{{parseInt(item.communitySimulationCurrentAmount).toLocaleString()}}
													</div>
													<div class="col-xs-6 mc-label">총 프로젝트</div>
													<div class="col-xs-6 text-right mc-count">{{item.communitySimulationCount}} 개</div>
												</div>
											</figcaption>
										</a>
									</figure>
								</div>
							</div>
						</div>

						<div class="text-center mt80 mb10 md-mt60 md-mb10 m-mt50 m-mb20 xs-mt15 xs-mb20">	
							<a href="javascript:void(10)" v-show="moreShow" v-on:click="more()" class="btn btn-primary-outline">더보기</a>
						</div>
					</div>
				</div>
			`,
            data : function() {
                return {
                	searchType: 0,
				    searchTypeOptions: [
				      { id: 0, text: '전체보기' },
				      { id: 1, text: '기업/단체' },
				      { id: 2, text: '학교' },
				      { id: 3, text: '개인' }
				    ],
				    orderType: 0,
				    orderTypeOptions: [
				      { id: 0, text: '후원금액순' },
				      { id: 1, text: '프로젝트 개설순' },
				      { id: 2, text: '최신순' }
				    ],					
                    community : [],
			        search : {
					    searchType: 0,
					    orderType: 0,
					    memCode : "",
			            paging : {
			                page : "1",
			                count : "12"
			            }
			        },
			        moreShow : true,
			        status : {
			        	communityStatus : "",
			        	communityReturnReason : "",
			        	communityIdx : ""
			        }
                }
            },
            created : function() {
                this.load();
                this.getStatus();
            },
            computed : {
            	name : function() {
            		return userInfo.name;
            	}
            },
            watch : {
            	searchType : function(value) {
                    this.search.searchType = value;
                    this.search.paging.page = "1";
                    this.community = [];
                    this.load();
                },
                orderType : function(value) {
                    this.search.orderType = value;
                    this.search.paging.page = "1";
                    this.community = [];
                    this.load();
                }      
            },
            methods : {
                load : function() {
	                var self = this;

	                axios.post('/data/view/community/list', this.search)
	                    .then(function(response){
	                    	self.community = _.concat(self.community, response.data.rData);	
	                        if(!self.moreEnd() || response.data.rData.length == 0) {
	                            self.moreShow = false;
	                        }
	                    })
	            },
	            getStatus : function() {
                	var self = this;

	            	if(userInfo.memCode) {
	            		self.search.memCode = userInfo.memCode;
	            		  axios.post('/data/view/community/status', this.search)
		                     .then(function(response){
		                     	if(response.data.rCode == "0000") {
									if (response.data.rData) {
				      					self.status = response.data.rData;
									}
	                            } else {
	                                self.error = response.data.rMsg;
	                            }
		                     })
	            	}
	            },
	            more : function() {
	                this.search.paging.page  = _.toString(_.add(_.toNumber(this.search.paging.page), 1));
	                this.load();
	            },
	            moreEnd : function() {
	                return (this.community.length % this.search.paging.count == 0) ? true : false;
	            }
            },
	        components : {  
	        	orderSelect : require('../common/select.js').default.component()
			}
		}
	}
}

export default new CommunityList()