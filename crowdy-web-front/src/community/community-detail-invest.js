class CommunityDetailReward {
	component() {
		return {
			template : `
			<div>
				<div class="rewards-list">
					<div class="row row-mobile">
						<div class="col-sm-4 col-md-3" v-for="(item, index) in community" v-on:click="detail(item, $event)">
							<figure>
								<a href="javascript:void(0)" class="items over-box">	
									<div class="items_img">
										<img :src="'//' + item.pjCardImg + '/ycrowdy/resize/!340x!226'" class="img-responsive" alt="..." />
									</div>
									<figcaption class="rewards-caption">
		                                <div class="rewards-subject">
		                                    <strong>{{item.pjTitle}}</strong>
		                                    <div class="rewards-summury" v-if="item.communityName != ''">{{item.communityName}}</div>
		                                </div>
										<div>
			                                <span class="rewards-price"><span class="webfont2">₩</span> {{ parseInt(item.pjCurrentAmount).toLocaleString() }} </span>
			                                <span class="rewards-percent">{{item.pjRate}}%</span>

			                                <div class="progress">
			                                    <div class="progress-bar" role="progressbar" :aria-valuenow="item.pjRate" aria-valuemin="0" aria-valuemax="100" :style="{width: item.pjRate + '%'}"><span class="sr-only">{{item.pjRate}}% 완료</span></div>
			                                </div>

			                                <div class="row not-space">
			                                    <div class="col-xs-6">
			                                        <div class="rewards-support">{{item.pjSponsorCount}}명 투자</div>
			                                    </div>
			                                    <div class="col-xs-6 text-right">
			                                        <!-- <span class="rewards-day" v-if="item.cpDday > 0">D - {{item.cpDday}}</span>
			                                        <span class="rewards-day rewards-day-end" v-else-if="item.cpDday == 0">{{ endTime }}</span>
			                                        <span class="rewards-day rewards-day-end" v-else="item.cpDday < 0">종료</span> -->
			                                        <span class="rewards-day rewards-day-end">종료</span>
			                                    </div>
			                                </div>
		                                </div>
		                            </figcaption>
								</a>
							</figure>
						</div>
					</div>
				</div>
		    </div>
			`,
			props : {
				communityIdx : {
					required: true
				},
				memCode : {
					required: true
				},
				paramEndType : {
                    default : "3"
                },
				paramOrderType : {
					default : "1"
				},
				paramSearchCount : {
                    default : "12"
                }
			},
            data: function() {
            	return {
			  		request : {
					    orderType: this.paramOrderType,
					    endType: this.paramEndType,
					    communityIdx: this.communityIdx,
			            paging : {
			                page : "1",
			                count : this.paramSearchCount
			            }
			        },
			        itemCode: "",
			        itemTitle: "",
			  		community: [],
				    moreShow : true,
                    //리워드 상세, 버튼클릭 구분
                    check : false,
				}
		  	},
		  	created : function() {
                this.load();
            },
            computed : {
            	userMemCode : function() {
            		return userInfo.memCode
            	},
            	endTime : function() {
                    moment.updateLocale('en', {
                        relativeTime : {
                            hh : "%d"
                        }
                    })
                    return moment().endOf('day').fromNow(true) + " 시간 남음"
                }
            },
		  	methods : {
	            load : function() {
	                var self = this;

	                this.request.communityIdx = this.communityIdx;
	                self.community = [
						{
							pjCardImg : 'image-se.ycrowdy.com/20180614/CROWDY_201806141124460825_2yvkz.png',
							pjTitle : '국내 최초 화덕피자 패스트푸드',
							communityName : '농식품 크라우드펀딩 전용관',
							pjCurrentAmount : '16750000',
							pjRate : '17',
							pjSponsorCount : '10',
							pjAliasUrl : 'gopizza'
						}
	                ];
	                // axios.post('/data/view/reward/list', this.request)
	                //     .then(function(response){
	                //     	self.community = _.concat(self.community, response.data.rData);		
	                //     	if(!self.moreEnd() || response.data.rData.length == 0) {
                 //                self.moreShow = false;
                 //            }
	                //     })
	            },
                //프로젝트 상세로 이동
                detail : function(data) {
                    this.check = false;
                    window.open('/i/' + data.pjAliasUrl, '_self')
                },
            }

		}
	}
}

export default new CommunityDetailReward()