class CommunityDetail {
	component() {
		return {
			template : `
				<div class="list_wrap pt0">
					<div class="community_vi_wrap">
						<div v-if="dataConfirm" class="community_vi_frame" :style="{'background-image' : 'url(//'+ communityInfoData.communityBackgroundImg +'/ycrowdy/resize/!1920x!400)'}">
							<div class="community_vi_zindex">
								<span class="webfont2">{{communityInfoData.communityName}}</span>
								<div class="row not-space">
									<div class="col-xs-6 text-right">
										<div class="mcd-items">
											<i></i>리워드 프로젝트
											<strong class="webfont2">{{communityInfoData.communityCpCount}}개</strong>
										</div>
									</div>
									<div class="col-xs-6 text-left">
										<div class="mcd-items">
											<i></i>투자 프로젝트
											<strong class="webfont2">1개</strong>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div id="list-wrap" class="mcd_info">
						<div class="container">
							<div class="mcd_info_line">
								<div class="media">
									<div class="media-left media-middle">
										<img v-if="dataConfirm" :src="'//' + communityInfoData.communityImg + '/ycrowdy/resize/!350x!350'" class="media-object" alt="..." />
									</div>
									<div class="media-body media-middle">
										<div class="mt5" v-html="$options.filters.infoFilter(communityInfoData.communityInfo)">{{communityInfoData.communityInfo | infoFilter}}</div>
										<div class="mcd_sns">
											<a v-if="communityInfoData.communityEmail != ''" :href="'mailto:' + communityInfoData.communityEmail" class="nth-child2"><span>email</span></a>
											<a v-if="communityInfoData.communityFaceBook != ''" :href="linkUrl(communityInfoData.communityFaceBook)" target="_blank" class="nth-child3"><span>facebook</span></a>
											<a v-if="communityInfoData.communityInstagram != ''" :href="linkUrl(communityInfoData.communityInstagram)" target="_blank" class="nth-child4"><span>instagram</span></a>
											<a v-if="communityInfoData.communityBlog != ''" :href="linkUrl(communityInfoData.communityBlog)" target="_blank" class="nth-child5"><span>blog</span></a>
											<a v-if="communityInfoData.communityWebSite != ''" :href="linkUrl(communityInfoData.communityWebSite)" target="_blank" class="nth-child6"><span>global</span></a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div id="list-category">
						<div class="list_category">
							<div class="container">
								<div class="row row-mobile">
									<!-- <div class="col-xs-6 col-sm-4 col-md-3">
										<order-select :options="searchTypeOptions" v-model="searchType"></order-select>
									</div>
									<div class="col-xs-6 col-sm-4 col-md-3">
										<order-select :options="orderTypeOptions" v-model="orderType"></order-select>
									</div> -->

									<div class="col-xs-12 col-sm-4 col-md-6 text-right xs-text-center s-text hidden-xs" v-if="memCode == communityInfoData.communityMemCode">
										<span class="hidden-sm" v-if="communityInfoData.communityConfirmCount != 0"><span class="red-800">{{communityInfoData.communityConfirmCount}}개의 프로젝트가 승인</span>대기중입니다.</span>
										<a :href="'/community/make?index=' + communityInfoData.communityIdx" class="btn btn-primary-outline ml10 xs-mt10">파트너 정보수정</a>
									</div>
									
								</div>
							</div>
						</div>
					</div>
					
					<div class="container">
						<h3 class="webfont2 jcorgFilterTextParent" style="margin-top:  0px;">투자 프로젝트</h3>
						<detail-invest v-if="dataConfirm" :community-idx="communityInfoData.communityIdx" :mem-code="communityInfoData.communityMemCode" :param-order-type="orderType"></detail-invest>
						<hr class="big big_grey">
						<h3 class="webfont2 jcorgFilterTextParent" style="padding-top:  40px;">리워드 프로젝트</h3>
						<detail-reward v-if="dataConfirm" :community-idx="communityInfoData.communityIdx" :mem-code="communityInfoData.communityMemCode" :param-order-type="orderType"></detail-reward>
					</div>
				</div>
			`,
			props : ['url'],
            data : function(){
            	return {
			  		dataConfirm : false,
			  		memCode : userInfo.memCode,
			  		searchType: 0,
				    searchTypeOptions: [
				      { id: 0, text: '리워드' },
				      { id: 1, text: '모의 펀딩' }
				    ],
				    orderType: 1,
				    orderTypeOptions: [
				      { id: 1, text: '펀딩금액순' },
				      { id: 2, text: '마감 임박순' },
				      { id: 3, text: '최신순' },
				      { id: 4, text: '후원자순' }
				      // { id: 5, text: '소개많은순' }
				    ],
				    communityInfoData : {
			  			communityIdx : "",
						communityType : "",
						communityName : "",
						communityImg : "",
						communityInfo : "",
						communityBackgroundImg : "",
						communityCurrentAmount : "",
						communityEmail : "",
						communityFaceBook : "",
						communityInstagram: "",
						communityMemCode: "",
						communityCpCount : "",
						communityConfirmCount : "",
						communitySimulationCount : "",
						communityWebSite: "",
						communityBlog : "",
						communityReturnReason: "",
						communityStatus: ""
			  		}
			  	}
            },
            created : function() {
        		this.load();
            },
            components : {
				detailReward : require('./community-detail-reward.js').default.component(),
				detailInvest : require('./community-detail-invest.js').default.component(),
	        	orderSelect : require('../common/select.js').default.component()
            },
		  	methods : {
                load : function() {
	                var self = this;
	                axios.post('/data/view/community/view', {communityUrl : this.url})
	                    .then(function(response){
	                    	var result = response.data.rData;
	                    	if(result.communityIdx == null) {
	                    		noti.open('잘못된 주소입니다.', function() {window.open("/", '_self');});
                                return;
	                    	}
	                    	self.communityInfoData = response.data.rData;
	                    	self.dataConfirm = !self.dataConfirm;
	                    })
	            },
                linkUrl : function(value) {
					var urls = value;
					var returnUrl = '';
                    if(urls.indexOf("http://") >= 0 || urls.indexOf("https://") >= 0) {
                    	returnUrl = urls;
                    } else {
                    	returnUrl = 'http://' + urls;
                    }
                    return returnUrl;
                }
            },
            filters : {
                infoFilter : function(val) {
                    return val.replace( /[\n]/g, "<br/>")
                }
            }
		}
	}
}

export default new CommunityDetail()