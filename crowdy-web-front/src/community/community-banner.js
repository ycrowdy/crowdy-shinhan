class communityBanner {
	component() {
		return {
			template : `
            <div>
				<div class="tab-content partner-tab-content xs-mt5">
                    <div role="tabpanel" class="tab-pane active">
                        <div id="community-list" class="xs-pb0">
                            <figure v-for="(item, index) in community">
                                <a :href="'/c/' + item.communityUrl" class="thumbnail over-box">
                                    <img :src="'//' + item.communityImg + '/ycrowdy/resize/!320x!240'" class="img-responsive" alt="" />
                                    <div class="caption-partner">
                                        <div class="caption-line">{{item.communityName}}</div>
                                    </div>
                                </a>
                            </figure>
                        </div>
                    </div>
                </div>

                <div class="text-center mt50 mb20 md-mt65 md-mb0 m-mt50 m-mb20 xs-mt30 xs-mb0 hidden-xs">
                    <a href="/community/main" class="btn btn-primary-outline col-sm-8 col-md-6 col-xs-8 col-sm-offset-2 col-md-offset-3 col-xs-offset-2 btn-more mb80">파트너 전체보기</a>
                </div>
            </div>
			`,
			data : function() {
				return  {
					community : []
				}
			},
			created : function() {
                    this.load();
            },
            updated : function() {
                
            },
            methods : {
            	load : function() {
                    var self = this;
                    axios.post('/data/view/community/banner-list', {})
                        .then(function(response) {
                            self.community = _.concat(self.community, response.data.rData);
                            self.$nextTick(function() {
                                $("#community-list").slick({
                                    dots: false,
                                    infinite: true,
                                    autoplay: true,
                                    autoplaySpeed: 3000,
                                    slidesToShow: 4,
                                    slidesToScroll: 4,
                                    responsive: [
                                        {
                                          breakpoint: 1050,
                                          settings: {
                                            dots: true,
                                            slidesToShow: 4,
                                            slidesToScroll: 4
                                          }
                                        },
                                        {
                                          breakpoint: 767,
                                          settings: {
                                            dots: true,
                                            slidesToShow: 3,
                                            slidesToScroll: 3
                                          }
                                        },
                                        {
                                          breakpoint: 477,
                                          settings: {
                                            dots: true,
                                            slidesToShow: 2,
                                            slidesToScroll: 2
                                          }
                                        }
                                    ]
                                });
                            });
                        })
                }
            }
		}
	}
}


export default new communityBanner()