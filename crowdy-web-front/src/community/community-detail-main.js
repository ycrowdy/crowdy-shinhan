class CommunityDetailMain {
	component() {
		return {
			template : `
				<div>
					<template v-if="url == 'apfs'">
						<invest :url="url"></invest>
					</template>
					<template v-if="url != 'apfs'">
						<detail :url="url"></detail>
					</template>
				</div>
			`,
			props : ['url'],
            components : {
				detail : require('./community-detail.js').default.component(),
				invest : require('./community-invest.js').default.component()
            }
		}
	}
}

export default new CommunityDetailMain()