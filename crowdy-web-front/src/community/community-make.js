class CommunityMake {
	component() {
		return {
			template : `
			<div>
				<div class="col-md-9">
					<form class="form-horizontal pl5 pr5 xs-pl0 xs-pr0">
						<div class="pl5 pr5 xs-pl0 xs-pr0">
							<!-- 반려사유 -->
							<div class="form-group row-mobile-n" v-if="request.communityStatus == '2'">
								<div class="col-xs-12 control-label">
									<div class="text-left red-800" v-text="'반려사유'"></div>
								</div>
								<div class="col-xs-12">
									<div class="ifm-wrap-box-form">
										{{ request.communityReturnReason | infoFilter}}
									</div>
								</div>
							</div>
							<!-- //반려사유 -->

							<div class="form-group row-mobile-n">
								<label for="partner_name" class="col-xs-12 control-label">
									<div class="text-left mb10">파트너 이름 <a class="form-tip webuiPopover" href="javascript:void(0)" data-plugin="webuiPopover" data-content="꿈과 아이디어를 가진 개설자와 이를 증폭시켜주는 참여자가 만나 지역 ∙ 관심별 커뮤니티 안에서 상호작용하는 크라우드펀딩 플랫폼의 새로운 지표입니다." data-animation="pop"><i class="fa fa-question" aria-hidden="true"></i></a></div>
								</label>
								<div class="col-xs-12 col-sm-6">
									<input type="text" name="communityName" class="form-control" :class="{'error' : errors.has('communityName')}" v-model="request.communityName" v-validate="'required|max:15'" :disabled="disable"/>
									<label class="error" v-if="errors.has('communityName')" v-text="errors.first('communityName')"></label>
								</div>
							</div>

							<div class="form-group row-mobile-n">
								<label for="partner_introduce" class="col-xs-12 control-label">
									<div class="text-left mb10">파트너 소개 <a class="form-tip webuiPopover" href="javascript:void(0)" data-plugin="webuiPopover" data-content="꿈과 아이디어를 가진 개설자와 이를 증폭시켜주는 참여자가 만나 지역 ∙ 관심별 커뮤니티 안에서 상호작용하는 크라우드펀딩 플랫폼의 새로운 지표입니다." data-animation="pop"><i class="fa fa-question" aria-hidden="true"></i></a></div>
								</label>
								<div class="col-xs-12 col-sm-10">
									<textarea rows="10" cols="50" class="form-control" :class="{'error' : errors.has('communityInfo')}" name="communityInfo" v-model="request.communityInfo" v-validate="'required|max:180'"></textarea>
									<label class="error" v-if="errors.has('communityInfo')" v-text="errors.first('communityInfo')"></label>
									<div class="text-right mt10">
										<span class="textarea_text_leng webfont2"><span v-text="request.communityInfo.length">0</span> / 180</span>
									</div>
								</div>
							</div>

							<div class="form-group row-mobile-n">
								<label for="project_weburl" class="col-xs-12 control-label control-label-big">
									<div class="text-left">파트너 웹주소 설정 <a class="form-tip webuiPopover" href="javascript:void(0)" data-plugin="webuiPopover" data-content="프로젝트의 랜딩 페이지 url 주소를 직접 지정해주세요. 프로젝트의 내용과 관련된 키워드가 포함된 짧은 영어로 설정 해주시면 됩니다. 띄어쓰기 없이 입력하셔야 하고, 특수문자 사용은 불가합니다." data-animation="pop"><i class="fa fa-question" aria-hidden="true"></i></a></div>
								</label>
								<div class="col-xs-4 col-sm-3">
									<p class="form-control-static">https://www.ycrowdy.com/c/</p>
								</div>
								<div class="col-xs-8 col-sm-3">
									<div style="margin-left:15px;">
										<url-input :url="url" v-model="url" class="form-control" name="communityUrl" :class="{'error' : !urlConfirm || errors.has('communityUrl')}" maxlength="30" placeholder="커뮤니티 주소를 입력해주세요" :disabled="disable"></url-input>
									</div>
									<label style="margin-left:15px;" class="error" v-if="errors.has('communityUrl')" v-text="errors.first('communityUrl')"></label>
									<label style="margin-left:15px;" class="error" v-if="!urlConfirm" v-text="'사용중인 주소입니다.'"></label>
								</div>
							</div>

							<div class="form-group row-mobile-n">
								<label for="partner_profile" class="col-xs-12 control-label">
									<div class="text-left mb10">파트너 프로필 이미지 <a class="form-tip webuiPopover" href="javascript:void(0)" data-plugin="webuiPopover" data-content="꿈과 아이디어를 가진 개설자와 이를 증폭시켜주는 참여자가 만나 지역 ∙ 관심별 커뮤니티 안에서 상호작용하는 크라우드펀딩 플랫폼의 새로운 지표입니다." data-animation="pop"><i class="fa fa-question" aria-hidden="true"></i></a></div>
								</label>
								<div class="col-xs-12 dropify-wrapper-170">
									<dropify-input accept="image/*" class="dropify-gallery" v-if="dataConfirm" v-model="request.communityImgFile" default-message="최적 사이즈 340 X 340" :default-img="request.communityImg"></dropify-input>
								</div>
							</div>

							<div class="form-group row-mobile-n">
								<label for="partner_top_img" class="col-xs-12 control-label">
									<div class="text-left mb10">파트너 상단배경 이미지 <a class="form-tip webuiPopover" href="javascript:void(0)" data-plugin="webuiPopover" data-content="꿈과 아이디어를 가진 개설자와 이를 증폭시켜주는 참여자가 만나 지역 ∙ 관심별 커뮤니티 안에서 상호작용하는 크라우드펀딩 플랫폼의 새로운 지표입니다." data-animation="pop"><i class="fa fa-question" aria-hidden="true"></i></a></div>
								</label>
								<div class="col-xs-12 col-md-11 dropify-wrapper-1920-400">
									<div>
										<dropify-input accept="image/*" class="dropify-gallery" v-if="dataConfirm" v-model="request.communityBackgroundImgFile" default-message="최적 사이즈 1920 X 400" :default-img="request.communityBackgroundImg"></dropify-input>
									</div>
								</div>
							</div>

							<!-- <div class="form-group row-mobile-n">
								<label for="partner_top_img" class="col-xs-12 control-label">
									<div class="text-left mb10">파트너 종류<a class="form-tip webuiPopover" href="javascript:void(0)" data-plugin="webuiPopover" data-content="꿈과 아이디어를 가진 개설자와 이를 증폭시켜주는 참여자가 만나 지역 ∙ 관심별 커뮤니티 안에서 상호작용하는 크라우드펀딩 플랫폼의 새로운 지표입니다." data-animation="pop"><i class="fa fa-question" aria-hidden="true"></i></a><p>(선택사항)</p></div>
								</label>
								<div class="col-xs-12 col-md-11 dropify-wrapper-1920-400">
									<div>
										<type-select :options="typeOptions" v-model="request.communityType"></type-select>
									</div>
								</div>
							</div> -->
						</div>

						<hr class="big" />

						<div class="form-group row-mobile-n mb10">
							<div class="col-xs-2 col-sm-1">
								<label for="partner_website" class="icon_sns icon_website pointer"></label>
							</div>
							<div class="col-xs-10 col-sm-6">
								<input type="text" class="form-control" placeholder="웹사이트 URL을 입력해주세요" v-model="request.communityWebSite"/>
							</div>
						</div>

						<div class="form-group row-mobile-n mb10">
							<div class="col-xs-2 col-sm-1">
								<label for="partner_facebook" class="icon_sns icon_facebook pointer"></label>
							</div>
							<div class="col-xs-10 col-sm-6">
								<input type="text" class="form-control" placeholder="페이스북 URL을 입력해주세요" v-model="request.communityFaceBook"/>
							</div>
						</div>

						<div class="form-group row-mobile-n mb10">
							<div class="col-xs-2 col-sm-1">
								<label for="partner_instagram" class="icon_sns icon_instagram pointer"></label>
							</div>
							<div class="col-xs-10 col-sm-6">
								<input type="text" class="form-control" placeholder="그외의 URL을 입력해주세요" v-model="request.communityBlog"/>
							</div>
						</div>						

						<hr class="big" />

						<div class="form-group row-mobile-n">
							<div class="col-xs-6 col-sm-3">
								<button type="button" class="btn btn-block btn-primary" v-text="saveText" v-on:click="saveConfirm" :disabled="request.communityStatus == 0 || !dataConfirm"></button>
							</div>
							<div class="col-xs-6 col-sm-3">
								<a href="/community/main" class="btn btn-block btn-primary-outline">취소</a>
							</div>
						</div>
					</form>
				</div>

				<div id="community-confirm-modal" class="modal fade" tabindex="-1" role="dialog" style="z-index: 9999;" data-backdrop="static" data-keyboard="false">
				    <div class="modal-dialog modal-sm" role="document">
				        <div class="modal-content">
				            <div class="modal-header">
				                <button type="button" class="close" aria-label="Close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
				            </div>
				            <div class="modal-body modal-order">
				                <p>커뮤니티 파트너를 신청하시겠습니까?</p>
				                <div class="modal-footer text-center">
				                    <div class="row not-space">
			                            <div class="col-xs-4 col-xs-offset-1">
			                                <button type="button" class="btn btn-block btn-primary-outline" v-on:click="save">확인</button>
			                            </div>
			                            <div class="col-xs-4 col-xs-offset-2">
			                                <button type="button" class="btn btn-block btn-primary-outline" data-dismiss="modal">취소</button>
			                            </div>
				                    </div>
				                </div>
				            </div>
				        </div>
				    </div>
				</div>
			</div>
			`,
			props : ['communityIdx'],
			data : function() {
				return {
					dataConfirm : false,
			  		urlConfirm : true,
			  		request : {
				  			communityIdx : this.communityIdx,
				  			communityName : "",
				  			communityInfo : "",
				  			communityUrl : "",
				  			communityImg : "",
				  			communityImgFile : {},
				  			communityBackgroundImg : "",
				  			communityBackgroundImgFile : {},
				  			communityEmail : "",
				  			communityFaceBook : "",
				  			communityWebSite : "",
				  			communityEmail : "",
				  			communityInstagram : "",
				  			communityBlog : "",
				  			communityStatus : "3",
				  			communityReturnReason : "",
				  			communityType : "1"

			  		},
			  		typeOptions : [
			  			{
        					id : "1",
        					text : "기업/단체"
        				},
        				{
        					id : "2",
        					text : "학교"
        				},
        				{
        					id : "3",
        					text : "개인"
        				}
			  		]
				}
			},
			created : function() {
		  		var self = this;
		  		if(this.communityIdx == '') {
		  			this.dataConfirm = !this.dataConfirm
		  			return;
		  		}
		  		window.communityMake = this;
		  		this.load()

		  	},
		  	computed : {
		  		saveText : function() {
		  			return this.request.communityStatus == 1 ? "수정하기" : "신청하기"
		  		},
		  		disable : function() {
		  			return this.request.communityStatus == 0 || this.request.communityStatus == 1 ? true : false;
		  		},
		  		url : {
					get : function() {
						return this.request.communityUrl;
					},
					set : function(value) {
						
						if(value != '') {
							var self = this;
							axios.post('/data/community/url', { url : value })
		                        .then(function(response){
		                            self.urlConfirm = response.data
		                        })
						}

						this.request.communityUrl = value;
					}
				}
		  	},
		  	components : {
		  		dropifyInput : require('../common/dropify-input.js').default.component(),
		  		typeSelect : require('../common/select.js').default.component(),
		  		urlInput : require('../common/url-input.js').default.component(),
		  	},
		  	methods : {
		  		load : function() {
		  			var self = this;
		  			axios.post('/data/view/community/view', {communityIdx : this.request.communityIdx})
	                    .then(function(response) {
	                    	var result = response.data.rData;
            				var data = self.request;
            				if(userInfo.memCode != result.communityMemCode) {
            					noti.open("잘못된 접근입니다.", function() {window.open("/", '_self');})
    							return;
            				}

	                    	data.communityIdx = result.communityIdx
							data.communityName = result.communityName
							data.communityInfo = result.communityInfo
							data.communityUrl = result.communityUrl
							data.communityImg = result.communityImg
							data.communityBackgroundImg = result.communityBackgroundImg
							data.communityEmail = result.communityEmail
							data.communityFaceBook = result.communityFaceBook
							data.communityWebSite = result.communityWebSite
							data.communityEmail = result.communityEmail
							data.communityInstagram = result.communityInstagram
							data.communityBlog = result.communityBlog
							data.communityStatus  = result.communityStatus 
							data.communityReturnReason = result.communityReturnReason
							data.communityType = result.communityType

	                    	self.dataConfirm = !self.dataConfirm
	                    })
		  		},
		  		saveConfirm : function() {
		  			$('#community-confirm-modal').modal('show');
		  		},
		  		save : function() {
		  			var self = this;
		  			this.$validator.validateAll()
		  				.then(function(sucess) {
		  					$('#community-confirm-modal').modal('hide');
		  					if(!sucess || !self.urlConfirm) return;
		  					
		  					self.request.communityMemCode = userInfo.memCode;

		  					$('.page-loader-more').fadeIn('')
							axios.post('/set/community/apply', self.request)
			                    .then(function(response){
			                    	$('.page-loader-more').fadeOut('')
			                    	var result = response.data;
			                    	if(result.rCode == "0000") {
			                    		noti.open("'커뮤니티 파트너 신청이 완료되었습니다. <br /> 영업일 기준 7일 이내에 이메일을 통해 승인 여부를 알려드립니다", function() {window.open('/community/main', '_self')});
			                    	} else {
			                    		noti.open(result.rMsg);
			                    	}
			                    })
		  				})

		  		},
		  		urlCheck : function() {
		  			var self = this;
		  			axios.post('/data/community/url', { url : this.request.communityUrl })
			                    .then(function(response){
			                    	self.urlConfirm = response.data
			                    })
		  		}
		  	},
		  	filters : {
                infoFilter : function(val) {
                    return val.replace( /[\n]/g, "<br/>")
                }
            }


		}
	}
}

export default new CommunityMake()