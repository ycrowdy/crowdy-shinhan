class SimulationDetail {
    component() {
        return {
            template : `
                <div>
                    <div class="detail_vi_wrap">
                        <div v-if="dataConfirm" class="detail_vi_frame" :style="{'background-image' : 'url(//'+ rewardDetailData.cpCardImg +'/ycrowdy/resize/!1920x!400)'}">
                            <div class="detail_vi_zindex">
                                <h2 class="webfont2">{{ rewardDetailData.cpTitle }}</h2>
                                <template v-if="rewardDetailData.communityName != ''">
                                <h4 v-text="rewardDetailData.communityName"></h4>
                                </template>
                            </div>
                        </div>
                    </div>
                
                    <div class="common_sub_top_menu common_stm_detail hidden-sm hidden-md hidden-lg">
                        <div class="container common_stm_detail_mobile common_stm_detail_mobile_story">
                            <a class="hash-link" :class="{'active' : step == 1}" style="cursor: pointer;" v-on:click="stepChange(1)"><span class="webfont2">스토리</span></a>
                            <a class="hash-link new_icon" :class="{'active' : step == 2}" style="cursor: pointer;" v-on:click="stepChange(2)"><span class="webfont2">댓글 <em>{{ rewardDetailData.replyCnt }}</em></span></a>
                        </div>
                    </div>
                
                    <div id="list_category" class="hidden-xs">
                        <div class="common_sub_top_menu common_stm_detail">
                            <div class="container common_stm_detail_hide">
                                <a class="hash-link" :class="{'active' : step == 1}" style="cursor: pointer;" v-on:click="stepChange(1)"><span class="webfont2">스토리</span></a>
                                <a class="hash-link new_icon" :class="{'active' : step == 2}" style="cursor: pointer;" v-on:click="stepChange(2)"><span class="webfont2">댓글 <em>{{ rewardDetailData.replyCnt }}</em></span></a>
                            </div>
                            <div class="container common_stm_detail_show">
                                <div class="row row-mobile-n">
                                    <div class="col-sm-8">
                                        <a href="" class="hash-link first-child" :class="{'active' : step == 1}" style="cursor: pointer;" v-on:click="stepChange(1)"><span class="webfont2">스토리</span></a>
                                        <a href="" class="hash-link new_icon" :class="{'active' : step == 2}" style="cursor: pointer;" v-on:click="stepChange(2)"><span class="webfont2">댓글 <em>{{ rewardDetailData.replyCnt }}</em></span></a>
                                        <div class="dropdown">
                                            <a href="javascript:void(0)" id="dLabel" class="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="webfont2">공유하기</span> <span class="caret"></span></a>
                                            <ul class="dropdown-menu" aria-labelledby="dLabel">
                                                <li><a href="javascript:void(0)" v-on:click="facebookShare">페이스북 공유</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="row not-space">
                                            <div class="col-md-1"></div>
                                            <div class="col-md-11">
                                                <div class="detail_order_btn">
                                                    <button type="button" class="btn btn-block btn-lg btn-primary" :disabled="rewardDetailData.cpEndStatus != 1" v-on:click="funding">펀딩하기</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <story :reward-detail-data="rewardDetailData" :cp-code="cpCode" :traning="true" v-if="step == 1 && dataConfirm" v-on:funding="funding" v-on:facebook-share="facebookShare"></story>
                    
                    <div v-if="step == 2  && dataConfirm">
                        <!-- 댓글 -->
                        <div class="detail_wrap">
                            <div class="container">
                                <div class="row row-mobile-n">
                                    <div class="col-md-8">
                                        <div class="list_title list_title_none webfont2 xs-mt25">
                                            여러분의 한마디가<br />
                                            창작자에게 큰 힘이 됩니다
                                        </div>
                                        <div class="list_summury list_summury_big webfont2">
                                            <span class="blue-800">{{ rewardDetailData.replyCnt }}개</span>의 댓글이 달려있습니다
                                        </div>
                                        <!-- 댓글작성 & 댓글목록 -->
                                        <comment :cp-code="cpCode" :crpy-idxs="0" :param-search-count="4"></comment>
                                        <!-- //댓글작성 & 댓글목록 -->
                                    </div>
                                    <info :reward-detail-data="rewardDetailData" :training="true"></info>
                                </div>
                            </div>
                        </div>
                        <!-- //댓글 -->
                    </div>
                    <!-- //댓글 -->

                    <simulation-registration v-on:join="join" :mem-code="memCode" :simulation-idx="contest.simulationIdx"></simulation-registration>
                </div>
            
            `,
            props: ['cpAliasUrl'],
            data: function() {
                return {
                    step : 1,
                    dataConfirm : false,
                    rewardDetailData: {
                        cpCardImg : "",
                        cpCode : ""
                    },
                    commentData: [],
                    replyDate: [],
                    param: {
                    	cpAliasUrl : this.cpAliasUrl,
                        cpTraining : 'Y'
                    },
                    registration : {},
                    contest : {},
                    cpCode : '',
                    fundingUrl : '/simulation/funding/',
                    memCode : userInfo.memCode
                }
            },
            components : {
                story : require('../reward/reward-detail-story.js').default.component(),
                comment : require('../reward/reward-comment.js').default.component(),
                info : require('../reward/reward-detail-info.js').default.component(),
                simulationRegistration : require('./simulation-registration.js').default.component(),
            },
            created: function() {
                this.load();  
            },
            computed : {
                computeCommentCount: function() {
                    return this.commentCountValue;
                }
            },
            methods: {
                load : function() {
                    var self = this;
                    axios.post('/data/view/reward/detail', this.param)
                        .then(function (response) {
                            var result = response.data.rData;
                            if(result == null || result.cpCode == null) {
                                noti.open('잘못된 주소입니다.', function() {window.open("/", '_self');});
                                return;
                            }
                            if (result.cpStatus == '0') {
                                noti.open('비공개 처리된 프로젝트입니다.', function() {window.open("/", '_self');});
                                return;   
                            }
                            self.rewardDetailData = response.data.rData;
                            self.cpCode = result.cpCode;
                            self.contestLoad()
                            // self.dataConfirm = !self.dataConfirm;
                        })
                },
                contestLoad : function() {
                    var self = this;
                    axios.post('/data/view/simulation/contest/detail', {simulationIdx : this.rewardDetailData.trnIdx})
                        .then(function(response){
                            self.contest = response.data.rData;
                            if(self.contest.simulationStatus == 9) {
                                noti.open('중지된 모의펀딩입니다.', function() {window.open("/", '_self');});
                                return
                            }
                            self.joinCheck();
                        })
                },
                joinCheck : function() {
                    var self = this;
                    axios.post('/data/reward/simulation/funding/info/registration-status', {simulationIdx : this.rewardDetailData.trnIdx, memCode : this.memCode})
                        .then(function(response){
                            self.registration = response.data.rData;
                            self.dataConfirm = true;
                        })
                },
                stepChange : function(step) {
                    this.step = step;
                },
                funding : function() {
                    var self = this;
                    if(!userInfo.loginConfirm()) return;
                    if(this.registration.simulationRegistrationStatus == 'N') {
                        noti.open('가상 금액을 받고 모의펀딩에 참여해보세요.', function() {window.open("/sc/" + self.contest.simulationUrl, '_self');})
                         // noti.open('가상 금액을 받고 모의펀딩에 참여해보세요.', self.joinOpen, true);       
                        return;
                    } else {
                        window.open(this.fundingUrl + this.cpAliasUrl, '_self');    
                    }
                    
                },
                joinOpen : function() {
                    if(!userInfo.loginConfirm()) return;
                    $('#authorizationModal').modal({backdrop: 'static', keyboard: false});
                },
                join : function(mobile) {
                    var self = this;
                    var request = {
                        memCode : self.user.memCode,
                        simulationIdx : self.contest.simulationIdx,
                        mobileNo : mobile,
                        simulationPoint : self.contest.simulationPoint,
                    }

                    axios.post('/data/reward/simulation/funding/registration', request)
                        .then(function(response){
                            var result = response.data;
                            if(result.rCode != '0000') {
                                noti.open(result.rMsg)
                                return
                            } else {
                                self.joinCheck();
                            }
                        })
                },
                facebookShare : function() {
                    var self = this;
                    if(!userInfo.loginConfirm()) return;
                    
                    FB.ui({
                        method: 'share',
                        display: 'popup',
                        href: document.location.href + '?utm_source=facebook&utm_medium=share&utm_campaign=' + this.cpAliasUrl,
                    }, function(response){
                        // 공유를 다 안하고 팝업을 끄면 undefined 반환,
                        // 공유 성공 시 post_id를 반환함
                        if (typeof(response) != "undefined" || response.post_id != '') {
                            noti.open("공유되었습니다.");
                        }
                    });
                },
            },
        }
    }
}

export default new SimulationDetail()