class SimulationRegistration {
	component() {
		return {
			template : `
				<div id="authorizationModal" class="modal fade" tabindex="-1" role="dialog">
					<div class="modal-dialog modal-sm" role="document">
						<form class="modal-content form-horizontal" action="#">
							<div class="modal-header">
								<button type="button" class="close" v-on:click="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							</div>
							<div class="modal-body pb30 md-pb20 xs-pb15">
								<div class="rewards-modal-head rewards-modal-head-big text-center">
									<h4 class="blue-800 pt20 pb10">해당 프로젝트는 본인인증을 해야 <br />참여가 가능합니다.</h4>
								</div>

								<hr />

								<!-- 휴대전화 번호 -->
								<div class="form-group row-mobile-n m-mb0 mt20">
									<label for="member_mobile" class="col-xs-12 control-label">
										<div class="text-left">
											휴대전화 번호
										</div>
									</label>
									<div class="col-xs-8">
										<div class="row row-mobile-n">
											<input type="tel" class="form-control" v-model="mobileRequest.mobileNo"/>
										</div>
									</div>
									<div class="col-xs-4">
										<button type="button" class="btn btn-block btn-primary-outline" v-on:click="mobileNoCheck">인증번호 발송</button>
									</div>
								</div>
								<!-- //휴대전화 번호 -->

								<!-- 인증번호 입력 -->
								<div class="form-group row-mobile-n mb10" v-show="timerStart">
									<label for="member_certi" class="col-xs-12 control-label pt0">
										<div class="text-left">
											인증번호 입력
										</div>
									</label>
									<div class="col-xs-6">
										<input type="tel" class="form-control" v-model="mobileRequest.authNo" />
									</div>
									<div class="col-xs-2">
										<span>{{ time }}</span>  
									</div>
									<div class="col-xs-4">
										<button type="button" class="btn btn-block btn-primary-outline" v-on:click="mobileAuth">본인 인증하기</button>
									</div>
								</div>
								<span>{{ resultText }}</span>
								<!-- //인증번호 입력 -->
							</div>
						</form>
					</div>
				</div>
			`,
			props: ['memCode', 'simulationIdx'],
			data : function() {
				return {
					mobileRequest: {
                        mobileNo : '',
                        reqSeq : '',
                        authNo : ''
                    },
                    time : "",
					timer : {},
					timerStart : false,
					timeOut : true,
					resultText : "",
					authChange : false
				}
			},
			methods : {
				mobileNoCheck : function() {
					if(this.mobileRequest.mobileNo == '') return;

					if(this.authChange) {
						clearTimeout(this.timer);
					}

					var self = this;

					var request = {
						memCode : self.memCode,
						simulationIdx : self.simulationIdx,
						mobileNo : self.mobileRequest.mobileNo,
					}

					axios.post('/data/reward/simulation/funding/mobileNoCheck', request)
                        .then(function(response){
                        	var result = response.data;
                        	if(result.rCode != '0000') {
                        		noti.open(result.rMsg)
                        		return;
                        	} else {
                        		self.mobileAuthCall();
                        	}
                        });

				},
				mobileAuthCall : function() {
					var self = this;
                    axios.post('/auth/mobile', this.mobileRequest)
                        .then(function(response){
                            self.mobileRequest.reqSeq = response.data;
                            self.timerStart = true;
                            self.authChange = true;
                            self.countdown(self.countdownTimeOut);
                        });
				},
				mobileAuth : function() {
                    var self = this;
                    axios.post('/auth/mobile/result', this.mobileRequest)
                        .then(function(response){
                            clearTimeout(self.timer);
                            self.timerStart = false;
                            if(response.data) {
                                self.$emit('join', self.mobileRequest.mobileNo);
                                self.close();
                            } else {
                            	self.authChange = false;
                            	clearTimeout(this.timer);
                                self.resultText = "인증번호가 유효하지 않습니다."
                            }
                        });
                },
                countdown : function(callback) {
                    var endTime, hours, mins, msLeft, time;
                    var minutes = 2;
                    var seconds = 59;
                    var self = this;

                    function twoDigits(n) {
                        return (n <= 9 ? "0" + n : n);
                    }

                    function updateTimer() {
                        if(self.isCntdownRestart){
                            self.time = "";
                            self.isCntdownRestart = false;
                            clearTimeout(self.timer);
                            minutes = 2;
                            seconds = 59;
                            endTime = (+new Date) + 1000 * (60 * minutes + seconds) + 500;
                        }

                        msLeft = endTime - (+new Date);
                        
                        if ( msLeft < 1000 ) {
                            callback();
                        } else {
                            time = new Date( msLeft );
                            hours = time.getUTCHours();
                            mins = time.getUTCMinutes();
                            self.time = (hours ? hours + ':' + twoDigits(mins) : mins) + ':' + twoDigits(time.getUTCSeconds())
                            self.timer = setTimeout(updateTimer, time.getUTCMilliseconds() + 500);
                        }
                    }
                    endTime = (+new Date) + 1000 * (60* minutes + seconds) + 500;
                    updateTimer();

                },
                countdownTimeOut : function() {
                    this.timeOut = true;
                    this.timerStart = false;
                },
                close : function() {
                	this.mobileRequest.mobileNo = '';
                	this.mobileRequest.reqSeq = '';
                	this.mobileRequest.authNo = '';
                    this.time = "";
					this.timer = {};
					this.timerStart = false;
					this.timeOut = true;
					this.resultText = "";
					self.authChange = false;
                	$('#authorizationModal').modal('hide')
                }
			}
		}
	}
}

export default new SimulationRegistration()