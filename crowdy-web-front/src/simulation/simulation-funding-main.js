class SimulationFundingMain {
    component() {
        return {
            template : `
                <!-- 프로젝트 펀딩하기 - 펀딩하기 -->
                <div id="list_wrap" class="common_support">
                    <div class="common_sub_vi bg_gray">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-9 m-text-center">
                                    <div class="common_vi_title webfont2">{{ rewardDetailData.cpTitle }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
    
                    <div class="common_sub_layout">
                        <div class="container">
                            <div class="row not-space">
                                <div class="col-md-2">
                                    <div class="st-tab">
                                        <ul>
                                            <li :class="{'active' : step == 1}"><a href="javascript:void(0)" class="st-tab-a"><i></i>리워드선택</a><span></span></li>
                                            <li :class="{'active' : step == 2}"><a href="javascript:void(0)" class="st-tab-a"><i></i>결제예약</a></li>
                                            <li :class="{'active' : step == 3}"><a href="javascript:void(0)" class="st-tab-a"><i></i>예약확인</a><span></span></li>
                                        </ul>
                                    </div>

                                    <div class="st-price" v-if="dataConfirm">
                                        <span>남은 가상 포인트</span>
                                        <strong>{{ userPoint }} 포인트</strong>
                                    </div>

                                </div>
                                
                                <support v-if="step == 1 && dataConfirm" :code="cpCode" :simulation-point="parseInt(this.contest.simulationPoint - this.registration.simulationUsePoint)" :limit-point="contest.simulationLimitPoint" v-on:set-id="setId" v-on:step-change="stepChange"></support>
                                <payment v-if="step == 2 && dataConfirm" :code="cpCode" :id="id" :simulation-idx="rewardDetailData.trnIdx" v-on:step-change="stepChange" v-on:set-idx="setIdx"></payment>
                                <confirm v-if="step == 3 && dataConfirm" :code="cpCode" :id="id" :idx="idx" :simulation-url="contest.simulationUrl" v-on:step-change="stepChange" v-on:facebook-share="facebookShare" v-on:update-point="updatePoint"></confirm>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //프로젝트 펀딩하기 - 펀딩하기 -->
            `,
            props : {
                cpAliasUrl : {
                    require : true
                },
                paramCpTraining : {
                    default : 'Y'
                }
            },
            data : function() {
                return {
                    step : 1,
                    rewardDetailData : {},
                    registration : {},
                    contest : {},
                    cpCode : '',
                    param: {
                    	cpAliasUrl: this.cpAliasUrl,
                        cpTraining : this.paramCpTraining
                    },
                    dataConfirm : false,
                    id : '',
                    idx : '',
                    memCode : userInfo.memCode,
                    usedPoint : 0
                }
            },
            components : {
               support : require('./simulation-funding-support.js').default.component(),
               payment : require('./simulation-funding-payment.js').default.component(),
               confirm : require('./simulation-funding-confirm.js').default.component()
            },
            computed : {
                userPoint : function() {
                    return parseInt(this.contest.simulationPoint - this.registration.simulationUsePoint - this.usedPoint).toLocaleString()
                }
            },
            created : function() {
                this.load();
            },
            methods : {
                load : function() {
                    var self = this;
                    axios.post('/data/view/reward/detail', this.param)
                        .then(function (response) {
                            var result = response.data.rData;
                            if(result == null) {
                                noti.open('잘못된 주소입니다.', function() {window.open("/", '_self');});
                                return;
                            }
                            self.rewardDetailData = response.data.rData;
                            self.cpCode = response.data.rData.cpCode;
                            self.contestLoad();
                        })
                },
                contestLoad : function() {
                    var self = this;
                    axios.post('/data/view/simulation/contest/detail', {simulationIdx : this.rewardDetailData.trnIdx})
                        .then(function(response){
                            self.contest = response.data.rData;
                            if(self.contest.simulationStatus == 9) {
                                noti.open('중지된 모의펀딩 대회입니다.', function() {window.open("/", '_self');});
                                return
                            }
                            self.joinCheck();

                            // self.dataConfirm = true;
                        })
                },
                joinCheck : function() {
                    var self = this;
                    axios.post('/data/reward/simulation/funding/info/registration-status', {simulationIdx : this.rewardDetailData.trnIdx, memCode : this.memCode})
                        .then(function(response){
                            self.registration = response.data.rData;
                            if(self.registration.simulationRegistrationStatus == 'N') {
                                noti.open('모의 펀딩에 먼저 참여하세요.', function() {window.open("/sc/" + self.contest.simulationUrl , '_self');})
                                return;
                            }
                            self.dataConfirm = true;
                        })
                },
                stepChange : function(step) {
                    this.step = step;
                },
                setIdx : function(idx) {
                    this.idx = idx;
                },
                setId : function(id) {
                    this.id = id;
                },
                facebookShare : function() {
                    var self = this;
                    FB.ui({
                        method: 'share',
                        display: 'popup',
                        href: document.location.origin + "/sr/" + this.cpAliasUrl + '?utm_source=facebook&utm_medium=share&utm_campaign=' + this.cpAliasUrl,
                    }, function(response){
                        // 공유를 다 안하고 팝업을 끄면 undefined 반환,
                        // 공유 성공 시 post_id를 반환함
                        if (typeof(response) != "undefined" || response.post_id != '') {
                            noti.open("공유되었습니다.");
                        }
                    });
                },
                updatePoint : function(nowPoint) {
                    this.usedPoint = nowPoint;
                }
            }
        }
    }
}
export default new SimulationFundingMain()