class SimulationRewardList {
	component() {
		return {
			template : `
                <div class="container">
    				<div class="rewards-list">
                        <div class="row row-mobile">
                            <div class="col-sm-4 col-md-3" v-if="paramaStatus == 3">
                                <figure>
                                    <a href="javascript:void(0)" class="items items_plus over-box" v-on:click="add">
                                        <div class="items_plus_table">
                                            <div class="items_plus_td">
                                                <i></i>
                                                <span>
                                                    모의펀딩<br />
                                                    프로젝트 등록하기
                                                </span>
                                            </div>
                                        </div>
                                    </a>
                                </figure>
                            </div>
                            <div v-for="(item, index) in reward" class="col-sm-4 col-md-3">
                                <figure>
                                    <a :href="'/sr/' + item.cpAliasUrl" class="items over-box">
                                        <div class="items_img">
                                            <img :src="'//' + item.cpCardImg + '/ycrowdy/resize/!340x!226'" class="img-responsive" alt="..." />
                                        </div>
                                        <figcaption class="rewards-caption">
                                            <div class="rewards-subject">
                                                <div class="rewards-summury">{{item.memName}}</div>
                                                <strong>{{item.cpTitle}}</strong>
                                            </div>

                                            <template v-if="memCode == '1000046908' || paramSimulationIdx != '99'">
                                            <span class="rewards-price"><span class="webfont2">₩</span> {{ parseInt(item.cpCurrentAmount).toLocaleString() }} </span>
                                            <span class="rewards-percent">{{item.cpRate}}%</span>
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" :aria-valuenow="item.cpRate" aria-valuemin="0" aria-valuemax="100" :style="{width: item.cpRate + '%'}"><span class="sr-only">{{item.cpRate}}% 완료</span></div>
                                                </div> 

                                            <div class="row not-space">
                                                <div class="col-xs-6">
                                                    <div class="rewards-support">{{item.cpSponsorCount}}명 후원</div>
                                                </div>
                                                <div class="col-xs-6 text-right">
                                                    <span class="rewards-day" v-if="item.cpDday > 0">D - {{item.cpDday}}</span>
                                                    <span class="rewards-day rewards-day-end" v-else-if="item.cpDday == 0">{{ endTime }}</span>
                                                    <span class="rewards-day rewards-day-end" v-else>종료</span>
                                                </div>
                                            </div>
                                            </template>
                                            <template v-if="memCode != '1000046908' && paramSimulationIdx == '99'">
                                                <span class="rewards-price"><span class="webfont2"></span> &nbsp; </span>
                                                <span class="rewards-percent">&nbsp;</span>
                                                <div class="progress">
                                                    <div class="progress-bar" role="progressbar" :aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" :style="{width:'0%'}"><span class="sr-only"> &nbsp; </span></div>
                                                </div>
                                                 <div class="row not-space">
                                                <div class="col-xs-6">
                                                    <div class="rewards-support">&nbsp;</div>
                                                </div>
                                                <div class="col-xs-6 text-right">
                                                    <span class="rewards-day" v-if="item.cpDday > 0">D - {{item.cpDday}}</span>
                                                    <span class="rewards-day rewards-day-end" v-else-if="item.cpDday == 0">{{ endTime }}</span>
                                                    <span class="rewards-day rewards-day-end" v-else>종료</span>
                                                </div>
                                            </div>
                                            </template>
                                           
                                        </figcaption>
                                    </a>
                                </figure>
                            </div>
                        </div>
                    </div>
                    
                    <div class="text-center mt75 mb20 md-mt65 md-mb0 m-mt50 m-mb20 xs-mt30 xs-mb0">
                        <a href="javascript:void(10)" v-show="moreShow" v-on:click="more()" class="btn btn-primary-outline">더보기</a>
                    </div>
                </div>
			`,
			props : {
                paramOrderType : {
                    default : "1"
                },
                paramEndType : {
                    default : "3"
                },
                paramSearchCount : {
                    default : "12"
                },
                paramaStatus : {
                    required: true
                },
                memCode : {
                    required: true
                },
                paramSimulationIdx : {
                    required : true
                }

            },
            data : function() {
                return {
                    reward : [],
                    search : {
                        orderType : this.paramOrderType,
                        endType: this.paramEndType,
                        cpSimulationConfirm : "Y",
                        communityIdx : "",
                        simulationIdx : this.paramSimulationIdx,
                        paging : {
                            page : "1",
                            count : this.paramSearchCount
                        }
                    },
                    moreShow : true,
                    addRequest : {
                        memCode : userInfo.memCode,
                        simulationIdx : this.paramSimulationIdx,
                        simulationConfirm : 'Y'
                    }
                }
            },
            created : function() {
                this.load();
            },
            watch : {
                paramOrderType : function(value) {
                    this.search.orderType = value;
                    this.search.paging.page = "1";
                    this.reward = [];
                    this.load();
                }
            },
            computed : {
                endTime : function() {
                    moment.updateLocale('en', {
                        relativeTime : {
                            hh : "%d"
                        }
                    })
                    return moment().endOf('day').fromNow(true) + " 시간 남음"
                }
            },
            methods : {
                load : function() {
                    var self = this;
                    axios.post('/data/view/reward/list', this.search)
                        .then(function(response){
                            self.reward = _.concat(self.reward, response.data.rData);
                            if(!self.moreEnd() || response.data.rData.length == 0) {
                                self.moreShow = false;
                            }
                        })
                },
                more : function() {
                    this.search.paging.page  = _.toString(_.add(_.toNumber(this.search.paging.page), 1));
                    this.load();
                },
                moreEnd : function() {
                    return (this.reward.length % this.search.paging.count == 0) ? true : false;
                },
                add : function() {
                    if(!userInfo.loginConfirm()) return;

                    // 모의 펀딩용 리워드 프로젝트 만들고 만들기 페이지로 이동
                    axios.post('/data/save/reward/create', this.addRequest)
                        .then(function(response) {
                            var result = response.data;
                            if(result.rCode == "0000") {
                                window.open("/make/simulation/main/" + result.rData.cpCode, '_self')
                            } else {
                                noti.open('시스템 오류입니다. 다시 시도해주세요.');
                            }
                        });
                }
            }
		}
	}
}

export default new SimulationRewardList()
