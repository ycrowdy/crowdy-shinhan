class SimulationMain {
	component() {
		return {
			template : `
			<div>
				<div id="list-category">
					<div class="list_category">
						<div class="container">
							<div class="row row-mobile">
								<div class="col-xs-6 col-sm-4 col-md-3">
									<order-select :options="searchTypeOptions" v-model="searchType"></order-select>
								</div>
								<div class="col-xs-6 col-sm-4 col-md-3">
									<order-select :options="orderTypeOptions" v-model="orderType"></order-select>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="container">
                    <detail-simulation  :paramEndType="searchType" :paramOrderType="orderType" :paramSearchCount="5"></detail-simulation>
				</div>
			</div>
			`,
			data : function() {
				return {
					searchType: 6,
				    searchTypeOptions: [
				      { id: 6, text: '전체' },
				      { id: 1, text: '진행중' },
				      { id: 2, text: '진행예정' },
				      { id: 5, text: '종료' }
				    ],
				    orderType: 3,
				    orderTypeOptions: [
				      { id: 3, text: '최신순' },
				      { id: 2, text: '마감임박순' },
				      { id: 1, text: '펀딩금액순' },
				      { id: 4, text: '후원자순' }
				    ]
				}
			},
			components : {  
	        	orderSelect : require('../common/select.js').default.component(),
	        	detailSimulation : require('./simulation-list.js').default.component(),
			}
		}
	}
}

export default new SimulationMain()