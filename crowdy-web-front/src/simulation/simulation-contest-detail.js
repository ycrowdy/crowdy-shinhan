class SimulationContestDetail {
	component() {
		return {
			template : `
				<div id="list-wrap" v-if="dataConfirm">
					<div class="common_vi_wrap common_vi_wrap8">
						<div class="common_vi_frame webfont2">
							{{contest.simulationTitle}}
						</div>
					</div>

					<div class="container">
						<div class="mcf_d_info">
							<ul>
								<li v-if="contest.communityName != ''">
									<strong class="webfont2">주최</strong> : <span class="webfont2">{{contest.communityName}}</span> <a :href="'/c/' + contest.communityUrl" class="btn btn-sm btn-primary ml5">파트너 페이지</a>
								</li>
								<li>
									<strong class="webfont2">진행기간</strong> : <span class="webfont2 mr5">{{contest.simulationStartDate}} ~ {{contest.simulationEndDate}}</span>
									<br class="hidden-sm hidden-md hidden-lg" />
									<span class="btn_cursor btn btn-sm btn-primary-outline mr5">{{statusName}}</span>
								</li>
								<li>
									<div class="mcf_d_info_frame">
										<div class="mcf_d_info_left">
											<strong class="webfont2"> 공지사항 </strong> :
										</div>
										<div class="mcf_d_info_memo">
											{{contest.simulationGuideInfo}}
										</div>
									</div>
									<div class="clearfix"></div>
								</li>
							</ul>
						</div>
						<div class="clearfix"></div>

						<hr class="big" />
						<div class="mcf_winfo text-center">
							<template v-if="contest.simulationStatus == 1">
								<div class="avatar avatar-90 avatar_bnone" v-if="user.image != ''">
									<img :src="'//' + user.image" alt="..." />
								</div>
								<div class="clearfix"></div>
								<p class="mcf_winfo_big">
									<template v-if="user.memCode == '' || registration.simulationRegistrationStatus == 'N'">
										가상 금액을 받고 모의펀딩에 참여해보세요.<br/>
										<a href="javascript:void(0)" class="btn btn-sm btn-primary mt25" v-on:click="joinOpen">가상 금액 받기</a>
									</template>
									<template v-else-if="registration.simulationRegistrationStatus == 'Y'">
										총 <strong class="blue-800">{{ registration.simulationFundingCount }}개</strong>의 프로젝트에 <strong class="blue-800">{{ parseInt(registration.simulationUsePoint).toLocaleString() }}원</strong>을 펀딩하였습니다.<br />
										<strong class="blue-800">{{ user.name }}</strong>님의 남은 모의펀딩 가상 금액 : <strong class="blue-800">{{ parseInt(contest.simulationPoint - registration.simulationUsePoint).toLocaleString() }}원</strong><br />
									</template>
								</p>
							</template>
							<template v-else-if="contest.simulationStatus == 5">
								<i></i>
								<div class="clearfix"></div>
								<h5 class="webfont2">종료된 모의펀딩입니다.</h5>
								<p>
									모의펀딩 기간 : {{contest.simulationStartDate}} ~ {{contest.simulationEndDate}}
								</p>
							</template>
							<template v-else>
								<i></i>
								<div class="clearfix"></div>
								<h5 class="webfont2">해당 모의펀딩은 아직 시작하지 않았습니다.</h5>

								<p>
									<template v-if="contest.simulationStatus == 3">
									모의펀딩 프로젝트 등록은 <br class="hidden-sm hidden-md hidden-lg" />아래 프로젝트 등록하기를 통해 접수 해주세요.<br />
									</template>
									등록기간 : {{contest.simulationRegistrationStartDate}} ~ {{contest.simulationRegistrationEndDate}}
								</p>
							</template>
						</div>
						<div class="clearfix"></div>

						<hr class="big" />
					</div>
					
					<div id="list-category">
						<div class="list_category">
							<div class="container">
								<div class="row row-mobile">
									<div class="col-xs-6 col-sm-3">
										<order-select :options="orderTypeOptions" v-model="orderType"></order-select>
									</div>
									<div class="col-xs-6 col-sm-3">

									</div>
									<div class="col-xs-12 col-sm-6 text-right m-text-center xs-text-center hidden-xs">
										<div class="s-text2 mt5 xs-mt10">
											<span class="red-800">[ ! ] 모의펀딩 리워드는 <br class="hidden-sm hidden-md hidden-lg" />실제로 배송 혹은 제공되지 않습니다.</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<simulation-reward-list v-if="dataConfirm" :param-order-type="orderType" :parama-status="contest.simulationStatus" :mem-code="user.memCode" :param-simulation-idx="contest.simulationIdx"></simulation-reward-list>
					
					<simulation-registration v-on:join="join" :mem-code="user.memCode" :simulation-idx="contest.simulationIdx"></simulation-registration>
				</div>
			`,
			props: ['contestUrl'],
			data : function() {
				return {
					dataConfirm : false,
					user : {
						memCode : userInfo.memCode,
						image : userInfo.image,
						name : userInfo.name
					},
					registration : {
						simulationRegistrationStatus : "N",
						simulationFundingCount : "0",
						simulationUsePoint : "0"
					},
					contest : {},
					orderType : "5",
                    orderTypeOptions : [
                        { id : "1", text : "펀딩금액순"},
                        { id : "4", text : "후원자 많은순"},
                        { id : "5", text : "랜덤"}
                    ]
				}
			},
			created : function() {
				this.load();
			},
			methods : {
				load : function() {
					var self = this;
					axios.post('/data/view/simulation/contest/detail', {simulationUrl : this.contestUrl})
                        .then(function(response){
                            if(response.data.rData == null) {
                                noti.open('잘못된 주소입니다.', function() {window.open("/", '_self');});
                                return;
                            }
                            self.contest = response.data.rData;
                            if(self.contest.simulationStatus == 9) {
                            	noti.open('중지된 모의펀딩입니다.', function() {window.open("/", '_self');});
                            	return
                            }
                            self.dataConfirm = true;
                            self.joinCheck(false);
                        })
				},
				joinCheck : function(moveCheck) {
					if(this.user.memCode == '') return;
					var self = this;
					axios.post('/data/reward/simulation/funding/info/registration-status', {simulationIdx : this.contest.simulationIdx, memCode : this.user.memCode})
                        .then(function(response){
                            self.registration = response.data.rData;

                            // 로그인했는데 아직 포인트가 없다면 강제로 문자인증 창 띄우기
                            if (self.registration.simulationRegistrationStatus == 'N') {
                            	self.joinOpen();
                            } else {
                            	if(moveCheck) {
                            		if (self.contestUrl == 'bixpo2018') {
                            			window.open("https://bixpo2018.ycrowdy.com", '_self');
                            		}
                            	}
                            }
                            
                        })
				},
				joinOpen : function() {
					if(!userInfo.loginConfirm()) return;
					$('#authorizationModal').modal({backdrop: 'static', keyboard: false});
				},
				join : function(mobile) {
					var self = this;
					var request = {
						memCode : self.user.memCode,
						simulationIdx : self.contest.simulationIdx,
						mobileNo : mobile,
						simulationPoint : self.contest.simulationPoint,
					}

					axios.post('/data/reward/simulation/funding/registration', request)
                        .then(function(response){
                        	var result = response.data;
                        	if(result.rCode != '0000') {
                        		noti.open(result.rMsg)
                        		return
                        	} else {
                        		self.joinCheck(true);
                        	}
                        })
				}
 			},
 			components : {  
	        	orderSelect : require('../common/select.js').default.component(),
	        	simulationRewardList : require('./simulation-reward-list.js').default.component(),
	        	simulationRegistration : require('./simulation-registration.js').default.component(),
			},
 			computed : {
 				statusName : function() {
 					var name = '';
 					var status = this.contest.simulationStatus
 					if(status == 1) {
 						name = '진행중'
 					} else if(status == 2 || status == 3) {
 						name = '진행예정'
 					} else if(status == 5) {
 						name = '종료'
 					}
 					return name;
 				},

 			},

		}
	}
}

export default new SimulationContestDetail()