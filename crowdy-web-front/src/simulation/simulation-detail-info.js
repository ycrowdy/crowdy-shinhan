class RewardDetailInfo {
    component() {
        return {
            template : `
                <div class="col-sm-4" :class="{'col-md-4 hidden-xs hidden-sm' : step == 2}">
                    <div class="row not-space">
                        <div class="col-md-1"></div>
                        <div class="col-md-11">
                            <!-- 펀딩하기 외 -->
                            <div class="clearfix"></div>
                            <div class="detail_order_wrap">
                                <!--  식품대전 때문에 잠시 가림 
                                <div class="detail_order_price webfont2"><strong>{{ parseInt(rewardDetailData.cpSponsorAmount).toLocaleString() }} 원</strong></div>  
                                
                                <div class="detail_order_goal">목표금액 {{ parseInt(rewardDetailData.tgAmount).toLocaleString() }} 원</div>
                                
                                <div class="detail_order_progress">
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="max-width: 100%;" :style="{ width: rewardDetailData.cpAchievement + '%'}"><span>{{ rewardDetailData.cpAchievement}}%</span></div>
                                    </div>
                                </div> -->

                                 <!-- 식품대전 때문에 잠시 수정 -->
                                <div class="detail_order_price webfont2"><strong> &nbsp; </strong></div>  
                                                
                                                <div class="detail_order_goal">목표금액 {{ parseInt(rewardDetailData.tgAmount).toLocaleString() }} 원</div>
                                                
                                                  <div class="detail_order_progress">
                                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="max-width: 100%;" :style="{ width: '0%'}"><span>&nbsp; </span></div>
                                                    </div>
                                                </div>
                                
                                <div class="detail_order_rp">
                                <template v-if="rewardDetailData.cpEndStatus == '0' || rewardDetailData.cpEndStatus == '1'">
                                    <span v-if="rewardDetailData.cpDday > 0">남은기간 <em><strong>{{ rewardDetailData.cpDday }}</strong>일</em></span>
                                    <span v-else-if="rewardDetailData.cpDday == 0"><em><strong><font color="#ff0071">{{ endTime }}</font></strong></em></span>
                                </template>
                                <template v-else>
                                    <span><em><strong><font color="#ff0071">종료</font></strong></em></span>
                                </template>
                                    <!-- <span>참여자 <em><strong>{{ rewardDetailData.sponCnt }}</strong>명</em></span> -->
                                    <span>&nbsp; <em><strong>&nbsp;</strong>&nbsp;</em></span>
                                </div>
                                
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group row-mobile-n">
                                            <div class="col-sm-10">
                                                <button class="btn btn-lg btn-block btn-primary" :disabled="rewardDetailData.cpEndStatus != 1" v-on:click="funding(rewardDetailData.cpEndStatus)">펀딩하기</button>
                                            </div>
                                            <div class="col-sm-2 hidden-xs hidden-sm">
                                                <a class="btn btn-lg btn-block icon_sns_fund icon_facebook_fund pointer icon_facebook_reward" v-on:click="facebookShare"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <!-- 프로필 PC -->
                                <div class="detail_order_info hidden-xs">
                                    <div class="detail_order_info_head">
                                        <div class="media">
                                            <div class="media-left media-middle">
                                                <div class="imgava" style="cursor: pointer;" v-on:click="detailShow"><img v-if="rewardDetailData.memShotImg != ''" :src="'//' + rewardDetailData.memShotImg" class="media-object" /></div>
                                            </div>
                                            <div class="media-body media-middle">
                                                <a style="cursor: pointer;" v-on:click="detailShow"><strong>{{ rewardDetailData.memName }}</strong></a>
                                            </div>
                                        </div>
                                    </div>
                                    <!--<div class="detail_order_info_btn">
                                        <div class="row not-space">
                                            <a class="first-child" style="cursor: pointer;" v-on:click="detailShow">자세히보기</a>
                                        </div>
                                    </div>-->
                                </div>
                                <!-- //프로필 PC -->
                                
                                <!-- 시작, 종료, 결제예정일 Mobile -->
                                <div class="clearfix"></div>
                                <div class="sded_wrap" style="margin-top: 10px;" v-if="!training">
                                    <div class="row not-space">
                                        <div class="col-xs-4 loop-dot child1">
                                            <div class="loop-dot-group">
                                                <em><i></i></em>
                                                <strong>펀딩종료일</strong>
                                                <p>{{ rewardDetailData.cpEndDate }}</p>
                                            </div>
                                        </div>
                                        <div class="col-xs-4 loop-dot child4">
                                            <div class="loop-dot-group">
                                                <em><i></i></em>
                                                <strong>결제예정일</strong>
                                                <p>{{ rewardDetailData.paymentData }}</p>
                                            </div>
                                        </div>
                                        <div class="col-xs-4 loop-dot child3_2">
                                            <div class="loop-dot-group">
                                                <em><i></i></em>
                                                <strong>예상배송일</strong>
                                                <p>{{ rewardDetailData.cpDeliveryDate }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- //시작, 종료, 결제예정일 Mobile -->
                            </div>
                            <!-- //펀딩하기 외 -->
                        </div>
                        <!-- 개설자에게 이메일 문의하기 -->
                        <div id="inquire" class="modal fade" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <form class="modal-content form-horizontal" action="#">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="rewards-modal-head rewards-modal-head-big">
                                            <p class="big-blue webfont2 mt10 mb0">문의하기</p>
                                            <hr />
                                        </div>
                        
                                        <div class="row row-mobile-n">
                                            <div class="col-sm-10 col-sm-offset-1">
                                                <div class="form-group row-mobile-n">
                                                    <label for="qaEmail_name" class="col-xs-12 control-label">
                                                        <div class="text-left">이름</div>
                                                    </label>
                                                    <div class="col-xs-12">
                                                        <input type="text" class="form-control input-md {validate:{required:false}}" :class="{'error' : errors.has('name')}" v-model="save.name" maxlength="10" name="name" placeholder="이름을 입력하세요" v-validate="'required'"/>
                                                        <label class="error" v-if="errors.has('name')" v-text="errors.first('name')"></label>
                                                    </div>
                                                </div>
                        
                                                <div class="form-group row-mobile-n">
                                                    <label for="qaEmail_email" class="col-xs-12 control-label">
                                                        <div class="text-left">이메일 주소</div>
                                                    </label>
                                                    <div class="col-xs-12">
                                                        <input type="email" class="form-control input-md {validate:{required:true}}" :class="{'error' : errors.has('memEmail')}" v-model="save.memEmail" maxlength="60" name="memEmail" title="이메일 주소를 입력하세요" placeholder="이메일 주소를 입력하세요" v-validate="'required'"/>
                                                        <label class="error" v-if="errors.has('memEmail')" v-text="errors.first('memEmail')"></label>
                                                    </div>
                                                </div>
                        
                                                <div class="form-group row-mobile-n">
                                                    <label for="qaEmail_memo" class="col-xs-12 control-label">
                                                        <div class="text-left">어떤 것이 궁금하신가요?</div>
                                                    </label>
                                                    <div class="col-xs-12">
                                                        <textarea rows="5" cols="40" id="qaEmail_memo" class="form-control  {validate:{required:true}}" :class="{'error' : errors.has('inqContent')}" v-model="save.inqContent" name="inqContent" placeholder="내용을 입력하세요" v-validate="'required'"></textarea>
                                                        <label class="error" v-if="errors.has('inqContent')" v-text="errors.first('inqContent')"></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                        
                                        <hr />
                        
                                        <div class="modal-footer modal-footer-noline text-center pt0">
                                            <div class="row row-mobile-n">
                                                <div class="col-xs-6 col-xs-offset-3 col-sm-4 col-sm-offset-4">
                                                    <a class="btn btn-block btn-primary-outline" data-dismiss="modal" v-on:click="confirm">작성완료</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- //개설자에게 이메일 문의하기 -->

                        <!-- 프로필정보 -->
                        <div id="profileModal" class="modal fade" tabindex="-1" role="dialog">
                            <div class="modal-dialog modal-sm" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <a class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                                    </div>
                                    <div class="modal-body pt15">
                                        <div class="widget">
                                            <div class="widget-body">
                                                <div class="media">
                                                    <div class="media-left media-middle">
                                                        <div class="media-object avatar avatar-lg">
                                                        <img v-if="rewardDetailData.memShotImg != ''" :src="'//' + rewardDetailData.memShotImg" class="media-object" />
                                                        </div>
                                                    </div>
                                                    <div class="media-body media-middle" style="font-size: 16px;">
                                                        <strong class="grey-800">{{ rewardDetailData.memName }}</strong> <a href="mailto:rewardDetailData.memEmail" class="ml5 grey-700">({{rewardDetailData.memEmail}})</a>
                                                    </div>
                                                    <div class="sns-group">
                                                        <dl v-if="snsInfo.websiteUrl != '' && snsInfo.websiteUrl != null">
															<dt><label for="partner_website" class="icon_sns icon_website pointer"></label></dt><dt>
															</dt><dd><strong class="form-control" style="display: block" v-text="snsInfo.websiteUrl"></strong></dd><dd>
                                                        </dd></dl>

                                                        <dl v-if="snsInfo.facebookUrl != '' && snsInfo.facebookUrl != null">
                                                            <dt><label for="partner_facebook" class="icon_sns icon_facebook pointer"></label></dt><dt>
                                                            </dt><dd><a type="text" class="form-control" v-text="snsInfo.facebookUrl"></a></dd><dd>
                                                        </dd></dl>
                                            
                                                        <dl v-if="snsInfo.instagramUrl != '' && snsInfo.instagramUrl != null">
                                                            <dt><label for="partner_instagram" class="icon_sns icon_instagram pointer"></label></dt><dt>
                                                            </dt><dd><a type="text" class="form-control" v-text="snsInfo.instagramUrl"></a></dd><dd>
                                                        </dd></dl>
                                                
                                                        <dl v-if="snsInfo.blogUrl != '' && snsInfo.blogUrl != null">
                                                            <dt><label for="partner_blog" class="icon_sns icon_blog pointer"></label></dt><dt>
                                                            </dt><dd><a type="text" class="form-control" v-text="snsInfo.blogUrl"></a></dd><dd>
                                                        </dd></dl>
                                                
                                                        <dl v-if="snsInfo.twitterUrl != '' && snsInfo.twitterUrl != null">
                                                            <dt><label for="partner_twitter" class="icon_sns icon_twitter pointer"></label></dt><dt>
                                                            </dt><dd><a type="text" class="form-control" v-text="snsInfo.twitterUrl"></a></dd><dd>
                                                        </dd></dl>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                        <div class="modal-footer text-center">
                                            <div class="row row-mobile-n">
                                                <div class="col-xs-4 col-xs-offset-4">
                                                    <a class="btn btn-block btn-primary-outline" data-dismiss="modal">확인</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- //프로필정보 -->
                    </div>
                </div>
            `,
            props : {
                rewardDetailData : {
                    type : Object
                }, 
                fundingUrl : {
                    default : '/reward/funding/'
                },
                training : {
                    default : false
                },
                step : {
                    default : 1,
                },
            },
            data : function() {
                return {
                    save : { 
                        inqCode : '',
                        inqType : 0,
                        memCode : userInfo.memCode,
                        name : userInfo.name,
                        memEmail : userInfo.email,
                        rcvMemEmail : '',
                        inqContent : '',
                    },
                    snsInfo : {
						websiteUrl : "",
						facebookUrl : "",
						blogUrl : "",
						instagramUrl : "",
						twitterUrl : ""
                    },
                    dataConfirm : false,
                    cpCode : this.rewardDetailData.cpCode,
                }
            },
            created : function() {
                this.getSnsInfo();
            },
            computed : {
                endTime : function() {
                    moment.updateLocale('en', {
                        relativeTime : {
                            hh : "%d 시간 남음",
                            mm : "%d 분 남음",
                            d : "오늘 까지",
                            h : "1 시간 남음"
                        }
                    })
                    return moment().endOf('day').fromNow(true)
                }
            },
            methods : {
                confirm : function() {
                    var self = this;
                    self.$validator.validateAll();
                    
                    axios.post('/set/save/reward/set-inquire', self.save)
                    .then(function (response) {
                        var result = response.data;
                        if(result.rCode == "0000") {
                            noti.open('등록되었습니다.');
                            $('#inquire').modal('hide');
                            self.save.content = '';
                            self.save.inqContent = '';
                        } else {
                            noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
                        }
                    })
                },
                //자세히보기
                detailShow : function() {
                    $('#profileModal').modal('show');
                    this.getSnsInfo();
                },
                getSnsInfo : function() {
                    var self = this;
                    if(this.rewardDetailData.snsList == null) return;
                    for (var i = 0; i < this.rewardDetailData.snsList.length; i++) {
                            var snsType = this.rewardDetailData.snsList[i].snsType;
                            var snsUrl = this.rewardDetailData.snsList[i].snsUrl;
                            var miscIdx = this.rewardDetailData.snsList[i].miscIdx;

                            if (snsType == 0) {
                                self.snsInfo.websiteUrl = snsUrl;
                            } else if (snsType == 1) {
                                self.snsInfo.facebookUrl = snsUrl;
                            } else if (snsType == 2) {
                                self.snsInfo.blogUrl = snsUrl;
                            } else if (snsType == 3) {
                                self.snsInfo.instagramUrl = snsUrl;
                            } else if (snsType == 4) {
                                self.snsInfo.twitterUrl = snsUrl;
                            }
                    }
                },
                //페이스북 공유
                facebookShare : function() {
                    this.$emit('facebook-share');
                },
                funding : function(val) {
                    this.$emit('funding');
                }
            }
        }
    }
}

export default new RewardDetailInfo()