class SimulationFundingPayment {
	component() {
		return {
			template : `
				<div class="col-md-10 col-line">
				    <div class="row not-space">
				        <div class="col-md-1"></div>
				        <div class="col-md-11">
				            <form class="form-horizontal pay_info_wrap_not">
				                <!-- 내가 선택한 리워드 -->
				                <div class="pay_info_wrap">
				                	<div class="s-text2 mt5 xs-mt10">
		                                <span class="red-800">[ ! ] 모의 펀딩은 가상의 포인트로만 결제하실 수 있으며, 실제로는 결제 되지 않습니다. <br  />또한 선택하신 리워드도 실제로 배송 혹은 제공되지 않습니다.</span>
		                            </div>
		                            <hr class="big" />
				                    <div class="step-title mt0">
				                        내가 선택한 리워드
				                    </div>

				                    <div class="xs-mt0 m-mb0 my_dash_table my_dash_table_center">
				                        <table class="table table-condensed-big table-vertical-align-top table-thead-blue table-fixed mb20">
				                            <colgroup>
				                                <col style="width:18%;">
				                                <col style="width:45%;">
				                                <col style="width:26%;" class="hidden-xs">
				                                <col style="width:11%;">
				                            </colgroup>
				                            <thead>
				                                <tr>
				                                    <th>펀딩포인트</th>
				                                    <th>리워드명</th>
				                                    <th class="hidden-xs">리워드옵션</th>
				                                    <th>수량</th>
				                                </tr>
				                            </thead>
				                            <tbody>
				                                <!-- Loop -->
				                                <tr v-if="dataConfirm" v-for="(item, index) in funding.list">
				                                    <td><strong>{{calcAmount(item.sbfUnitPrice, item.qty)}} 포인트 <br class="hidden-sm hidden-md hidden-lg" />펀딩</strong></td>
				                                    <td class="text-left">
				                                        <strong>{{item.cbfTitle}}</strong>
				                                        <p class="text-left">{{item.cbfInfo}}</p>
				                                    </td>
				                                    <td class="hidden-xs"><strong>{{item.benefit}}</strong></td>
				                                    <td><strong>{{item.qty}}개</strong></td>
				                                </tr>
				                                <!-- //Loop -->
				                            </tbody>
				                        </table>

				                        <div class="total_price text-right" v-if="dataConfirm && parseInt(funding.spsAmount) > 0">
				                            추가펀딩 포인트 <span>{{parseInt(funding.spsAmount).toLocaleString()}} 포인트</span>
				                        </div>
				                        <div class="total_price text-right blue-800">
				                            총 결제 포인트 <span>{{ parseInt(saveData.sponsorData.spsTotAmount).toLocaleString() }} 포인트</span>
				                        </div>
				                    </div>
				                </div>
				                <!-- //내가 선택한 리워드 -->
				                <!-- 결제정보입력 -->
					            <div class="pay_info_wrap">
									<div class="form-group row-mobile-n mb0">
                                        <label for="delivery_person" class="col-xs-12 control-label">
                                            <div class="text-left">
                                                진행자에게 요청사항
                                            </div>
                                        </label>
                                        <div class="col-md-10">
                                            <textarea rows="3" cols="50" class="form-control" v-model="saveData.sponsorData.spsMiscinfo"></textarea>
                                        </div>
                                    </div>
					            </div>
								<!-- //결제정보입력 -->
				                <div class="row st-submit">
				                    <div class="col-sm-4 col-md-3">
				                        <a class="btn btn-lg btn-block btn-primary" v-on:click="save">다음단계</a>
				                    </div>
				                </div>
				            </form>
				        </div>
				    </div>
				</div>
			`,
			props : ['code', 'id', 'simulationIdx'],
			data : function() {
				return {
					dataConfirm : false,
					funding : {
                        list : []
                    },
                    saveData : {
                        sponsorData : {
                            cpCode : this.code,
                            memCode : userInfo.memCode,
                            spsIdx : '',
                            //받는사람 휴대전화
                            spsRsMobileno : '',
                            //받는사람 이름
                            spsRsName : '',
                            //우편번호
                            spsPostnum : '',
                            //주소1
                            spsAddr1 : '',
                            //상세주소
                            spsAddr2 : '',
                            //주문비고내용
                            spsMiscinfo : '',
                            //추가 후원금
                            spsAmount : '0',
                            //전체후원금
                            spsTotAmount : 0,
                            //후원상태
                            spsStatus : '0',
                            //후원공개여부
                            spsViewStatus : '0',
                            //계좌이체 인증
                            spsCmsYn : 'N',
                            //배송상태
                            spsDeliveryStatus : '0',
                            //결제구분(1: 카드)
                            payDiv : '3',
                            //결제번호(billkey)
                            payNum : '',
                            //은행, 카드 코드
                            bankCode : '',
                            //생년월일사업자번호
                            payInfoNum : '',
                            //결제자 구분(계좌)
                            payMemType : '0',
                            //결제자 휴대전화
                            spsMbMobileno : '',
                            //접속타입(0:PC, 1:모바일)
                            conType : '0', 
                            //결제동의, 회원정보동의, 제3자회원정보 동의
                            payAgreeYn : 'Y',
                            //결제비고
                            payEtc : 'N',
                            simulationIdx : this.simulationIdx,
                            memberInfoSame : false, //(/reward/info쪽 )
                            myAddressSave : false,
                            benefitList : [],
                        }
                    }
                }
			},
            created : function() {
                this.load();
            },
            methods : {
            	load : function() {
                    var self = this;
                    axios.post('/reward/funding/get/benefit/' + this.id)
	                    .then(function (response) {
	                        var result = response.data.rData.data;
	                        self.funding = result;
	                        self.saveData.sponsorData.benefitList = result.list;
	                        self.saveData.sponsorData.spsAmount = self.funding.spsAmount;
	                        //self.saveData.sponsorData.cpCode = result.list[0].cpCode;
	                        if(result.list.length > 0) {
	                            if(result.list[0].cpBenefitDeliveryConfirm == 'Y') {
	                                self.saveData.sponsorData.spsDeliveryStatus = '1';
	                            }
	                            for(var i=0; i < result.list.length; i++) {
	                                self.saveData.sponsorData.spsTotAmount += (parseInt(result.list[i].sbfUnitPrice) * parseInt(result.list[i].qty));
	                            }
	                            self.saveData.sponsorData.spsTotAmount = self.saveData.sponsorData.spsTotAmount + parseInt(self.funding.spsAmount);
	                        } else {
	                        	self.saveData.sponsorData.spsTotAmount = parseInt(self.funding.spsAmount);
	                        }
	                        self.dataConfirm = true;
	                    });  
                },
                calcAmount : function(amount, qty) {
                    return (parseInt(amount) * parseInt(qty)).toLocaleString();
                },
                save : function() {
                	var self = this;
                	$('.page-loader-more').fadeIn('')                  
                    axios.post('/set/reward/simulation/funding/confirm', self.saveData.sponsorData)
                        .then(function (response) {
                            var result = response.data;
                            $('.page-loader-more').fadeOut('')
                            if(result.rCode == "0000") {
                                self.$emit('step-change', 3);
                                self.$emit('set-idx', response.data.rData.spsIdx);
                            } else {
                                noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
                            }                                  
                        })
                }
            }
		}
	}
}

export default new SimulationFundingPayment()