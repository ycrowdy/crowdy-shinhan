class SimulationList {
	component() {
		return {
			template : `
			<div>
				<div class="rewards-list">
					<div class="row row-mobile">
						<div class="col-xs-12" v-for="(item, index) in simulation">
							<a :href="'/sc/' + item.simulationUrl" class="mcf_loop_m">
								<div class="row not-space">
									<div class="col-sm-4 col-md-3">
										<div class="items_img">
											
											<span v-if="item.simulationStatus == 1" class="mcf-icon hidden-sm hidden-md hidden-lg">진행중</span>
											<span v-if="item.simulationStatus == 2 || item.simulationStatus == 3" class="mcf-icon mcf-icon-end hidden-sm hidden-md hidden-lg">진행예정</span>
											<span v-if="item.simulationStatus == 5" class="mcf-icon mcf-icon-end hidden-sm hidden-md hidden-lg">대회종료</span>
											<img :src="'//' + item.simulationCardImg + '/ycrowdy/resize/!340x!226'" class="img-responsive" alt="..." />
										</div>
									</div>
									<div class="col-sm-6 col-md-7">
										<div class="frame">
											<h5>{{item.simulationTitle}}</h5>
											<div class="mcf_summury">{{item.simulationLimitInfo}}</div>
											<ul class="mcf_info" v-if="item.simulationStatus == 1 || item.simulationStatus == 5">
												<li>
													기간 <span>{{item.simulationStartDate}} ~ {{item.simulationEndDate}}</span>
												</li>
												<li>
													총 펀딩금액 <span>{{ parseInt(item.simulationTotalAmount).toLocaleString() }}원</span>
												</li>
												<li>
													총 프로젝트 수 <span>{{item.simulationCpCount}}개</span>
												</li>
												<li>
													총 참여자 수 <span>{{item.simulationSponsorCount}}명</span>
												</li>
											</ul>
											<div class="mcf_notice" v-else>
												<strong>공지사항</strong>
												<div v-html="convert(item.simulationGuideInfo)"></div>
											</div>
										</div>
									</div>

									<div class="col-sm-2 text-right hidden-xs">
										<span v-if="item.simulationStatus == 1" class="mcf-icon">진행중</span>
										<span v-if="item.simulationStatus == 2 || item.simulationStatus == 3" class="mcf-icon mcf-icon-end">진행예정</span>
										<span v-if="item.simulationStatus == 5" class="mcf-icon mcf-icon-end">대회종료</span>
									</div>
								</div>
							</a>
						</div>
						
						<div class="text-center mt75 mb20 md-mt65 md-mb0 m-mt50 m-mb20 xs-mt30 xs-mb0">
				            <a href="javascript:void(10)" v-show="moreShow" v-on:click="more()" class="btn btn-primary-outline">더보기</a>
				        </div>
					</div>
				</div>
			</div>
			`,
			props : {
				communityIdx : {
					default : ""
				},
				memCode : {
					default : ""
				},
				paramEndType : {
                    default : ""
                },
				paramOrderType : {
					default : "1"
				},
				paramSearchCount : {
                    default : "12"
                }
			},
            data: function() {
            	return {
			  		request : {
					    orderType: this.paramOrderType,
					    endType: this.paramEndType,
					    partnerIdx: this.communityIdx,
			            paging : {
			                page : "1",
			                count : this.paramSearchCount
			            }
			        },
			        itemCode: "",
			        itemTitle: "",
			  		simulation: [],
				    moreShow : true
			    }
		  	},
		  	created : function() {
                this.load();
            },
            computed : {
            	userMemCode : function() {
            		return userInfo.memCode
            	}
            },
            watch : {
            	paramOrderType : function(value) {
            		this.request.orderType = value;
                    this.request.paging.page = "1";
                    this.simulation = [];
                    this.load();
            	},
            	paramEndType : function(value) {
            		this.request.endType = value;
                    this.request.paging.page = "1";
                    this.simulation = [];
                    this.load();
            	}
            },
		  	methods : {
		  		showModal : function(mode,cpCode,cpTitle) {
		  			this.itemCode = cpCode;
		  			this.itemTitle = cpTitle;
		  			
		  			if(mode === 0) {
		  				$('#approvalModal').modal('show');	
		  			} else {
		  				$('#denyModal').modal('show');	
		  			}
		  		},
	            load : function() {
	                var self = this;
	                if(this.request.endType == 6) {
	                	this.request.endType = '';
	                }
	                axios.post('/data/view/simulation/contest/list', this.request)
	                    .then(function(response){
	                    	self.simulation = _.concat(self.simulation, response.data.rData);
	                    	if(!self.moreEnd() || response.data.rData.length == 0) {
                                self.moreShow = false;
                            }
	                    })
	            },
	            more : function() {
                    this.request.paging.page  = _.toString(_.add(_.toNumber(this.request.paging.page), 1));
                    this.load();
                },
                moreEnd : function() {
                    return (this.simulation.length % this.request.paging.count == 0) ? true : false;
                },
                convert : function(data) {
					return data.replace( /[\n]/g, "<br/>")
				}
            }
		}
	}
}

export default new SimulationList()