class AccountMain {
    component() {
        return {
            template : `
            <div>
                <div class="common-menu-bottom font15 fontBold">
                    <div class="common-menu-back">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true" style="margin-right:120px;"></span>출금계좌 변경
                    </div>
                </div>
                
                <div class="account-main-layout">
                    <div class="displayFlex" style="width:100%;">
                        <div class="mypage-step-box-blue" style="width:45%" >1 step</div>
                        <div class="mypage-step-box" style="width:45%" :class="{'mypage-step-box-blue' : accountStep == 2 || accountStep == 3}">2 step</div>
                        <div class="mypage-step-box" style="width:10%" :class="{'mypage-step-box-blue' : accountStep == 3 && accountError == 'N', 'red-800' : accountStep == 3 && accountError == 'Y',}">Finish</div>
                    </div>

                    <!--출금계좌 프로세스 1step-->
                    <template v-if="accountStep == 1">
                        <div class="mypage-title-big">출금계좌 변경</div>
                        <div class="font14 mt15 mb30">
                            출금계좌로 사용할 본인 명의의 계좌를 입력해주세요.<br/>
                            <span class="blue-800">크라우디에서 1원을 송금</span>하여 본인 여부를 확인합니다.
                        </div>

                        <bank-select v-if="dataConfirm" :options="bankCodeOptions" v-model="request.bankCode"></bank-select>
                        <number-input type="tel" class="form-control mt15" placeholder="‘-’ 없이 숫자만 입력해주세요"  :num="request.bankNo" v-model="request.bankNo"/></number-input>
                        <div class="mypage-setting-btn mt20" v-on:click="transfer(2)">다음 단계</div>
                    </template>
                    <!--//출금계좌 프로세스 1step-->


                    <!--출금계좌 프로세스 2step-->
                    <template v-if="accountStep == 2">
                        <div class="mypage-title-big">계좌 인증</div>
                        <div class="font14 mt15">
                            입력하신 계좌로 1원을 보내드렸습니다. 입금자 명의 <span class="blue-800">크라우디 - 000에서 000의 숫자를 입력</span>해주세요.
                        </div>

                        <number-input type="tel" class="form-control mt30" placeholder="입금자명 중 숫자만 입력해주세요" :num="bankMemo" v-model="bankMemo"/></number-input>
                        <div class="mypage-setting-btn mt20" v-on:click="transferCheck(3)">다음 단계</div>
                        
                        <div class="common-flex-between mt30">
                            <div class="font13">입력하신 출금 계좌</div>
                            <div class="font12 blue-800" v-on:click="changeStep(1)">다시 입력하기</div>
                        </div>
                        <div class="mypage-setting-block-box2 mt5"></div>
                        <div class="mypage-accoun-box-margin">
                            <div class="font14">{{bankName}}</div>
                            <div class="font16 fontBold">{{request.bankNo}} {{userName}}</div>
                        </div>
                    </template>
                    <!--//출금계좌 프로세스 2step-->


                    <!--출금계좌 프로세스 3step 성공시 -->
                    <template v-if="accountStep == 3 && accountError=='N'">
                        <div class="mypage-title-big">출금계좌 변경완료</div>
                        <div class="mt15">아래의 계좌로 출금계좌가 변경 되었습니다</div>
                        <div class="mt30 font13">입력하신 출금 계좌</div>
                        <div class="mypage-setting-block-box2 mt5"></div>
                        <div class="mypage-accoun-box-margin">
                            <div class="font14">{{bankName}}</div>
                            <div class="font16 fontBold">{{request.bankNo}} {{userName}}</div>
                        </div>

                        <div class="mypage-setting-btn mt40" v-on:click="returnFirst">확인</div>
                    </template>

                    <!--출금계좌 프로세스 3step 실패시 -->
                    <template v-if="accountStep == 3 && accountError=='Y'">
                        <div class="mypage-title-big">출금계좌 변경실패</div>
                        <div class="mt15">잘못된 숫자를 입력하셨습니다. <br/>보안을 위해 처음부터 다시 진행해주세요.</div>

                        <div class="mypage-setting-btn mt20" v-on:click="refresh">확인</div>
                    </template>
                    <!--//출금계좌 프로세스 3step-->

                    </div>
                    <!-- //컨텐츠 영역 -->
                </div>
            </div>
            `,
            data : function() {
                return {
                    accountStep : "1",
                    accountError : "N",
                    confirm : {
                        bank : false
                    },
                    bankCodeOptions : [],
                    request : {
                        bankCode : '088',    
                        bankNo : '',
                        memName : this.userName,
                        memCode : this.userCode
                    },
                    checkRequest : {
                        bankMemo : '',
                        memCode : this.userCode
                    },
                    bankMemo : ''
                }
            },
            created : function() {
                this.getBankCodeList();// 은행 목록
            },
            components : { 
            },
            computed : {
                dataConfirm : function() {
                    return this.confirm.bank;
                },
                bankName : function() {
                    for (var i = 0; i < this.bankCodeOptions.length; i++) {
                       if (this.bankCodeOptions[i].id == this.request.bankCode) {
                            return this.bankCodeOptions[i].text;
                       } 
                    }
                    return "";
                },
                userName : function() {
                    return JSON.parse(localStorage.getItem('user')).memName;
                },
                userCode : function() {
                    return JSON.parse(localStorage.getItem('user')).memCode;
                }
            },
            methods : {
                transfer : function(ch) {
                    var self = this;
                    
                    if (this.request.bankNo == "") {
                        noti.open("계좌번호를 입력해주세요.");
                        return;
                    }

                    this.request.memName = this.userName;
                    this.request.memCode = this.userCode;

                    $('.page-loader-more').fadeIn('');
                    axios.post('/data/invest/virtual/transfer', this.request)
                        .then(function(response){
                            $('.page-loader-more').fadeOut('');
                            var result = response.data;
                            if(result.rCode == "0000") {
                                self.changeStep(ch);
                            } else {
                                noti.open(result.rMsg);
                            }
                            // } else if(result.rCode == "5000") {
                            //     noti.open(result.rMsg);
                            // } else {
                            //     noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.");
                            // }
                    })
                },
                transferCheck : function(ch) {
                    var self = this;
                    this.checkRequest.bankMemo = "크라우디" + this.bankMemo;
                    this.checkRequest.memCode = this.userCode;

                    $('.page-loader-more').fadeIn('');
                    axios.post('/data/invest/virtual/transfer-check', this.checkRequest)
                        .then(function(response){
                            $('.page-loader-more').fadeOut('');
                            var result = response.data;
                            if(result.rCode == "0000") {
                                self.accountError = 'N';
                                self.changeStep(ch);
                            } else if(result.rCode == "5001") {
                                self.accountError = 'Y';
                                self.changeStep(ch);
                            } else {
                                noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.");
                            }
                    })
                },
                changeStep : function(ch){
                    this.accountStep = ch;
                },
                returnFirst : function() {
                    var returnUrl = localStorage.getItem('return');
                    if (returnUrl) {
                        window.open(returnUrl, '_self');
                    } else {
                        window.open('/mypage/main?menu=5&sub-menu=2', '_self');
                    }
                },
                refresh : function() {
                    window.open("/account/main", '_self');
                },
                getBankCodeList : function() {
                    var self = this;
                    
                    axios.post('/data/crowdy/code/invest', {gcode : "IRMI_STOCK_BANK"})
                        .then(function(response){
                            var result = response.data;
                            if(result.rCode == "0000") {
                                for (var i = 0; i < result.rData.length; i++) {
                                    var option = {
                                            id : result.rData[i].commonCode,
                                            text : result.rData[i].commonInfo
                                        };
                                    self.bankCodeOptions.push(option);
                                }
                                self.confirm.bank = true;
                            } else {
                                noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.");
                            }
                    })
                },
            },
            components :{
                bankSelect : require('../common/select.js').default.component(),
                numberInput : require('../common/number-input.js').default.component()
            }
        }
    }
}

export default new AccountMain()