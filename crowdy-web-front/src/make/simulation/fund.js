class Fund {
	component() {
		return {
			template : `
			<div class="col-md-10 col-line reward-make-fund">
				<div class="row not-space">
					<div class="col-lg-1"></div>
					<div class="col-lg-11">
						<div class="md-ml30">
							<form class="form-horizontal">

								<div class="form-group row-mobile-n">
									<label for="project_vod" class="col-xs-12 control-label control-label-big">
										<div class="text-left mb10">계좌 정보를 입력해주세요 </div>
										<p class="form-control-static mb10 mt0">실제 리워드 프로젝트를 진행할 경우에 필요한 정보입니다.</p>
									</label>
									<div class="col-xs-12">
										<div class="row row-mobile-n mb20">
											<label for="account_bank" class="col-xs-12 control-label">
												<div class="text-left mb10">거래 은행을 선택해주세요 </div>
											</label>
											<div class="col-sm-3">
												<bank-select v-if="dataConfirm" :options="bankCodeOptions" v-model="request.cpFundBankCode" disabled></bank-select>
											</div>
										</div>

										<div class="row row-mobile-n mb20">
											<label for="account_num" class="col-xs-12 control-label">
												<div class="text-left mb10">계좌번호를 적어주세요</div>
											</label>
											<div class="col-sm-6">
												<input type="tel" v-if="dataConfirm" class="form-control" v-model="request.cpFundBankAccountNo" maxlength="20" disabled/>
											</div>
										</div>

										<div class="row row-mobile-n mb20">
											<label for="account_name" class="col-xs-12 control-label">
												<div class="text-left mb10">예금주명을 적어주세요</div>
											</label>
											<div class="col-sm-6">
												<input type="text" v-if="dataConfirm" class="form-control" id="account_name" maxlength="10" v-model="request.cpFundBankAccountName" placeholder="계좌에 등록된 예금주명과 일치해야 합니다." disabled/>
											</div>
										</div>

										<div class="row row-mobile-n mb20">
											<label for="bankbook_img" class="col-xs-12 control-label">
												<div class="text-left mb10">통장 사본 이미지를 올려주세요</div>
											</label>
											<div class="col-sm-6">
												<div class="input-group input-group-file dropify-wrapper-340">
													<dropify-input v-if="dataConfirm" v-model="request.cpFundBankAccountFile" :default-img="request.cpFundBankAccountImg" disabled></dropify-input>
												</div>
											</div> 
										</div>
									</div>
								</div>

								<div class="form-group row-mobile-n">
									<label for="project_vod" class="col-xs-12 control-label control-label-big">
										<div class="text-left mb10">세금계산서 정보를 입력해주세요<span class="red-800">(선택사항)</span></div>
										<p class="form-control-static mb10 mt0">실제 리워드 프로젝트를 진행할 경우에 필요한 정보입니다.</p>
									</label>
									<div class="col-xs-12">
										<div role="tabpanel" class="tab-pane active" id="tex_tab1">
											<div class="row row-mobile-n mb20">
												<label for="tex1_company_email" class="col-xs-12 control-label">
													<div class="text-left mb10">전자 세금 계산서 이메일을 적어주세요 </div>
												</label>
												<div class="col-sm-6">
													<input type="email" v-if="dataConfirm" class="form-control" maxlength="40" v-model="request.cpFundTaxEmail" disabled/>
												</div>
											</div>

											<div class="row row-mobile-n mb20">
												<label for="tex1_company_img" class="col-xs-12 control-label">
													<div class="text-left mb10">사업자등록증 사본 이미지를 올려주세요 </div>
												</label>
												<div class="col-sm-6">
													<div class="input-group input-group-file dropify-wrapper-340">
														<dropify-input v-if="dataConfirm" v-model="request.cpBizFile" :default-img="request.cpBizImg"  disabled></dropify-input>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="form-group row-mobile-n">
									<label for="partner_facebook" class="col-xs-12 control-label control-label-big">
										<div class="text-left mb10">
											홈페이지와 SNS 주소를 적어주세요<span class="red-800">(선택사항)</span>
										</div>
										<p class="form-control-static mb10 mt0">현재 운영 중인 곳의 주소를 적어주세요. 진행자 정보에 노출됩니다. </p>
									</label>
									<div class="col-xs-7 sns-group" v-if="dataConfirm" >
										<dl>
											<dt><label for="partner_website" class="icon_sns icon_website pointer"></label></dt>
											<dd><input type="text" class="form-control" id="partner_website" v-model="snsInfo.websiteUrl" placeholder="웹사이트 URL을 입력해주세요" :disabled="cpStatus == 1"/></dd>
										</dl>

										<dl>
											<dt><label for="partner_facebook" class="icon_sns icon_facebook pointer"></label></dt>
											<dd><input type="text" class="form-control" id="partner_facebook" v-model="snsInfo.facebookUrl" placeholder="페이스북 페이지 주소를 입력해주세요" :disabled="cpStatus == 1"/></dd>
										</dl>

										<dl>
											<dt><label for="partner_instagram" class="icon_sns icon_instagram pointer"></label></dt>
											<dd><input type="text" class="form-control" id="partner_instagram" v-model="snsInfo.instagramUrl" placeholder="인스타그램 URL을 입력해주세요" :disabled="cpStatus == 1"/></dd>
										</dl>

										<dl>
											<dt><label for="partner_blog" class="icon_sns icon_blog pointer"></label></dt>
											<dd><input type="text" class="form-control" id="partner_blog" v-model="snsInfo.blogUrl" placeholder="블로그 URL을 입력해주세요." :disabled="cpStatus == 1"/></dd>
										</dl>

										<dl>
											<dt><label for="partner_twitter" class="icon_sns icon_twitter pointer"></label></dt>
											<dd><input type="text" class="form-control" id="partner_twitter" v-model="snsInfo.twitterUrl" placeholder="트위터 URL을 입력해주세요" :disabled="cpStatus == 1"/></dd>
										</dl>
									</div>
								</div>

								<div class="form-group row-mobile-n">
									<label for="mc_partner" class="col-xs-12 control-label control-label-big">
										<div class="text-left mb10">
											커뮤니티 파트너를 선택해주세요
											<span class="red-800">(선택사항) 프로젝트가 시작해야 선택할 수 있습니다. </span>
										</div>
										<p class="form-control-static mb10 mt0">실제 리워드 프로젝트를 진행할 경우에 필요한 정보입니다. </p>
									</label>
									<div class="col-xs-7">
										<div class="row row-mobile-n">
											<div class="col-xs-7">
												<community-select v-if="dataConfirm" :options="communityList" v-model="request.communityIdx" disabled/></community-select>
											</div>
											<div class="col-xs-5">
												<span class="red-800" v-if="partnerStatus == 2"> 승인 대기 중입니다. </span>
												<span class="red-800" v-if="partnerStatus == 1"> 승인 완료되었습니다. </span>
												<button type="button" v-if="partnerStatus == 3" class="btn btn-block btn-primary-outline" :disabled="cpStatus != 2" v-on:click="partnerApprovalRequest">{{ partnerName }}</button>
											</div>
										</div>
									</div>
								</div>

								<div class="form-group row-mobile-n mt90">
									<div class="col-sm-11">
										<div class="form-group row-mobile-n">
											<div class="col-xs-6 col-sm-4 col-md-3">
												<button type="button" class="btn btn-block btn-primary" v-on:click="save">저장하기</button>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			`,
			props : ['cpCode', 'cpStatus', 'endStatus'],
			data : function() {
				return {
					file : {
						fileData : "",
						fileName : "",
						state : true
					},
					request : {
						cpCode : "",
						memCode : userInfo.memCode,
						cpFundBankCode : "02",
						cpFundBankAccountNo : "",
						cpFundBankAccountName : "",
						cpFundBankAccountImg : "",
						cpFundBankAccountFile : {},
						cpBizImg : "",
						cpBizFile : {},
						cpFundTaxEmail : userInfo.email,
						//파트너 승인상태(승인대기:0 승인:1 반려:2 취소:9)
						communityApproveStatus : "",
						communityIdx : "",
						communityReturnReason : "",
						snsList : []
					},
					bankCodeOptions : [],
					communityList : [],
					confirm : {
						bank : false,
						community : false,
						fund : false
					},
					snsInfo : {
						websiteUrl : "",
						facebookUrl : "",
						blogUrl : "",
						instagramUrl : "",
						twitterUrl : ""
					},
					partnerStatus : 3
				}
			},
			computed : {
				dataConfirm : function() {
					return this.confirm.bank && this.confirm.fund && this.confirm.community; 
				},
				partnerName : function() {
					if(this.request.communityApproveStatus == 2 && this.endStatus == 1) {
						return '파트너 재승인 요청'
					} else {
						return '파트너 승인 요청'
					}
				}
			},
			components : {
				bankSelect : require('../../common/select.js').default.component(),
				dropifyInput : require('../../common/dropify-input.js').default.component(),
				communitySelect : require('../../common/select.js').default.component()
			},
			created : function() {
                $(window).scrollTop(0);
				// 은행 목록
				this.getBankCodeList();
				
				// 커뮤니티는 프로젝트가 시작한 후 선택할 수 있음
				if (this.cpStatus != 0) {
					// 커뮤니티 목록
					this.getCommunityList();	
				} else {
					this.confirm.community = true;
				}			
				// 제작자 계좌정보를 가져옴
				this.getFundInfo();
			},
			methods : {
				setPartnerStatus : function() {
					// 파트너 승인완료 
					if (this.request.communityApproveStatus == 1) {
						this.partnerStatus = 1;
					} else {
						// 승인 대기 중 또는 반려되었을 떄
						if (this.request.communityIdx > 0 && this.request.communityApproveStatus == 0) {
							this.partnerStatus = 2;
						//그 외
						} else {
							this.partnerStatus = 3;
						}
					}
				},
				getBankCodeList : function() {
					var self = this;
					
					axios.post('/data/crowdy/code/reward', {gcode : "CFND_BANK_CODE"})
	                    .then(function(response){
	                    	var result = response.data;
	                     	if(result.rCode == "0000") {
		                    	for (var i = 0; i < result.rData.length; i++) {
		                    		var option = {
		                    				id : result.rData[i].commonCode,
		                    				text : result.rData[i].commonInfo
		                    			};
		                    		self.bankCodeOptions.push(option);
		                    	}
		                    	self.confirm.bank = true;
	                    	} else {
	                    		noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
	                    	}
	                })
				},
				getCommunityList : function() {
					var self = this;

					axios.post('/data/save/reward/info/community', {cpCode : this.cpCode})
	                    .then(function(response){
	                    	var result = response.data;
	                     	if(result.rCode == "0000") {
		                    	for (var i = 0; i < result.rData.length; i++) {
		                    		var option = {
		                    				id : result.rData[i].communityIdx,
		                    				text : result.rData[i].communityName
		                    			};
		                    		self.communityList.push(option);
		                    		self.setPartnerStatus();
		                    	}
		                    	self.confirm.community = true;
	                    	} else {
	                    		noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
	                    	}
	                })
				},
				getFundInfo : function() {
					var self = this;
					axios.post('/data/save/reward/info/fund', {cpCode : this.cpCode})
	                    .then(function(response){
	                    	var result = response.data;
	                     	if(result.rCode == "0000") {
	                     		var data = self.request;

	                     		if (result.rData.cpFundBankCode == '' || result.rData.cpFundBankCode == null) {
	                     			data.cpFundBankCode = "02";
	                     		} else {
	                     			data.cpFundBankCode = result.rData.cpFundBankCode;	
	                     		}
		                    	
		                    	data.cpFundBankAccountNo = result.rData.cpFundBankAccountNo;
		                    	data.cpFundBankAccountName = result.rData.cpFundBankAccountName;
		                    	data.cpFundBankAccountImg = result.rData.cpFundBankAccountImg;
		                    	data.cpBizImg = result.rData.cpBizImg;
		                    	data.cpFundTaxEmail = result.rData.cpFundTaxEmail;

		                    	if (result.rData.cpFundTaxEmail == "") {
									data.cpFundTaxEmail = userInfo.email;		                    		
		                    	}

		                    	data.communityApproveStatus = result.rData.communityApproveStatus;
		                    	data.communityIdx = result.rData.communityIdx;
		                    	data.communityReturnReason = result.rData.communityReturnReason;

		                    	for (var i = 0; i < result.rData.snsList.length; i++) {
		                    		var snsType = result.rData.snsList[i].snsType;
		                    		var snsUrl = result.rData.snsList[i].snsUrl;
		                    		var miscIdx = result.rData.snsList[i].miscIdx;

		                    		if (snsType == 0) {
		                    			self.snsInfo.websiteUrl = snsUrl;
		                    		} else if (snsType == 1) {
										self.snsInfo.facebookUrl = snsUrl;
		                    		} else if (snsType == 2) {
		                    			self.snsInfo.blogUrl = snsUrl;
		                    		} else if (snsType == 3) {
		                    			self.snsInfo.instagramUrl = snsUrl;
		                    		} else if (snsType == 4) {
		                    			self.snsInfo.twitterUrl = snsUrl;
		                    		}
		                    	}

								self.setPartnerStatus();
		                    	self.confirm.fund = true;
		                    	
	                    	} else {
	                    		noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
	                    	}
	                })
				},
				save : function() {
					var self = this;

					this.request.snsList = [];
					this.request.snsList.push({snsType : 0, snsUrl : this.snsInfo.websiteUrl});
					this.request.snsList.push({snsType : 1, snsUrl : this.snsInfo.facebookUrl});
					this.request.snsList.push({snsType : 2, snsUrl : this.snsInfo.blogUrl});
					this.request.snsList.push({snsType : 3, snsUrl : this.snsInfo.instagramUrl});
					this.request.snsList.push({snsType : 4, snsUrl : this.snsInfo.twitterUrl});

					this.request.cpCode = this.cpCode;

					$('.page-loader-more').fadeIn('')
					axios.post('/set/reward/fund', this.request)
						.then(function(response) {
							$('.page-loader-more').fadeOut('');
							var result = response.data;
							if(result.rCode == "0000") {
		                    	noti.open("저장되었습니다.");
		                    	self.$emit('check', false);
	                    	} else {
	                    		noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
	                    	}
						})
				},
				partnerApprovalRequest : function() {
					var self = this;

					$('.page-loader-more').fadeIn('')
					axios.post('/set/save/community/project/approvalRequest', {cpCode : this.cpCode, communityIdx : this.request.communityIdx})
						.then(function(response) {
							$('.page-loader-more').fadeOut('');
							var result = response.data;
							if(result.rCode == "0000") {
		                    	noti.open("파트너 승인 요청이 완료되었습니다.");
		                    	self.getFundInfo();
	                    	} else {
	                    		noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
	                    	}
						})
				}
			}
		}
	}
}

export default new Fund()