class ProjectStart {
	component() {
		return {
			template : `
				<div>
					<div class="spstart_vi_wrap spstart_vi_wrap1">
						<div class="spstart_vi_frame">
							<div class="container container_2">
								<div class="row not-space">
									<div class="col-sm-3 col-md-5 col-lg-6"></div>
									<div class="col-sm-9 col-md-7 col-lg-6">
										<div class="md-ml20 m-ml15 xs-ml0">
											<div class="spstart_vi_title">
												<strong class="webfont2">리워드형</strong>
												<span>크라우드펀딩</span>
												<p>제품/서비스를 리워드로 제공하여 수익을 얻습니다.</p>
											</div>
											<div class="spstart_vi_btn">
												<div class="row row-mobile-n hidden-sm hidden-md hidden-lg">
													<!-- <div class="col-xs-6">
														<a href="https://docs.google.com/forms/d/e/1FAIpQLSdzhOOCoTTj0OFbvM0UCYLymArrilwdkND67W27t5w7X3SH6Q/viewform?usp=sf_link" class="btn btn-block btn-sps btn-shadow btn-default-outline" target="_blank">진행가능여부 확인</a>
													</div> -->
													<div class="col-xs-12">
														<a href="https://image-se.ycrowdy.com/file/%E1%84%8F%E1%85%B3%E1%84%85%E1%85%A1%E1%84%8B%E1%85%AE%E1%84%83%E1%85%B5_%E1%84%85%E1%85%B5%E1%84%8B%E1%85%AF%E1%84%83%E1%85%B3%E1%84%91%E1%85%B3%E1%84%85%E1%85%A9%E1%84%8C%E1%85%A6%E1%86%A8%E1%84%90%E1%85%B3_%E1%84%8C%E1%85%AE%E1%86%AB%E1%84%87%E1%85%B5%E1%84%80%E1%85%AD%E1%84%80%E1%85%AA%E1%84%89%E1%85%A5_ver4.pdf" target="_blank" class="btn btn-block btn-sps btn-shadow btn-default-outline">리워드 가이드북<span class="hidden-xs"> 다운로드</span></a>
													</div>
													<div class="col-xs-12 mt10">
														<div class="mt15 grey-800 big-hb hidden-sm hidden-md hidden-lg text-center">리워드 프로젝트 바로시작은 PC에서 가능합니다.</div>
													</div>
												</div>
												<div class="hidden-xs">
													<!-- <a href="https://docs.google.com/forms/d/e/1FAIpQLSdzhOOCoTTj0OFbvM0UCYLymArrilwdkND67W27t5w7X3SH6Q/viewform?usp=sf_link" class="btn btn-sps btn-shadow btn-default-outline" target="_blank">진행가능여부 확인</a> -->
													<a href="https://image-se.ycrowdy.com/file/%E1%84%8F%E1%85%B3%E1%84%85%E1%85%A1%E1%84%8B%E1%85%AE%E1%84%83%E1%85%B5_%E1%84%85%E1%85%B5%E1%84%8B%E1%85%AF%E1%84%83%E1%85%B3%E1%84%91%E1%85%B3%E1%84%85%E1%85%A9%E1%84%8C%E1%85%A6%E1%86%A8%E1%84%90%E1%85%B3_%E1%84%8C%E1%85%AE%E1%86%AB%E1%84%87%E1%85%B5%E1%84%80%E1%85%AD%E1%84%80%E1%85%AA%E1%84%89%E1%85%A5_ver4.pdf" target="_blank" class="btn btn-sps btn-shadow btn-default-outline">리워드 가이드북<span class="hidden-xs"> 다운로드</span></a>
													<div v-on:click="createRewardProject" class="btn btn-sps btn-shadow btn-primary">리워드 프로젝트 만들기</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="spstart_vi_wrap spstart_vi_wrap2">
						<div class="spstart_vi_frame">
							<div class="container container_2">
								<div class="row not-space">
									<div class="col-sm-3 col-md-5 col-lg-6"></div>
									<div class="col-sm-9 col-md-7 col-lg-6">
										<div class="md-ml20 m-ml15 xs-ml0">
											<div class="spstart_vi_title">
												<strong class="webfont2">투자형</strong>
												<span>크라우드펀딩</span>
												<p>스타트업의 주식/채권을 발행하여 자금을 조달합니다.</p>
											</div>
											<div class="spstart_vi_btn">
												<div class="row row-mobile-n hidden-sm hidden-md hidden-lg">
													<div class="col-xs-6">
														<a href="/crowdy/help#help_5_96" target="_blank" class="btn btn-block btn-sps btn-shadow btn-default-outline">자격요건 알아보기</a>
													</div>
													<div class="col-xs-6">
														<a href="https://image-se.ycrowdy.com/file/%E1%84%80%E1%85%A1%E1%84%8B%E1%85%B5%E1%84%83%E1%85%B3%E1%84%87%E1%85%AE%E1%86%A8_ver2018.pdf" target="_blank" class="btn btn-block btn-sps btn-shadow btn-default-outline">투자 상세가이드 다운</a>
													</div>
													<div class="col-xs-12 mt10">
														<div class="mt15 grey-800 big-hb hidden-sm hidden-md hidden-lg text-center">투자 프로젝트 의뢰신청은 PC에서 가능합니다.</div>
													</div>
												</div>
												<div class="hidden-xs">
													<a href="/crowdy/help#help_5_46" target="_blank" class="btn btn-sps btn-shadow btn-default-outline">자격요건 알아보기</a>
													<a href="https://image-se.ycrowdy.com/file/%E1%84%80%E1%85%A1%E1%84%8B%E1%85%B5%E1%84%83%E1%85%B3%E1%84%87%E1%85%AE%E1%86%A8_ver2018.pdf" target="_blank" class="btn btn-sps btn-shadow btn-default-outline">투자 가이드북 다운로드</a>
													<a href="javascript:void(0)" v-on:click="requestInvestProject" class="btn btn-sps btn-shadow btn-primary">투자신청 의뢰하기</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="common_vi_wrap common_vi_wrap5">
						<div class="common_vi_frame">
							<div class="container container_2">
								<span class="webfont2">모의펀딩 시작하기</span>
								<div class="row not-space">
									<div class="col-sm-8 col-sm-offset-2 col-lg-4 col-lg-offset-4">
										<div class="md-pl30 md-pr30 xs-pl0 xs-pr0">
											<div class="row row-mobile-n mt20">
												<div class="col-sm-12">
													<a href="javascript:void(0)" class="btn btn-block btn-sps btn-shadow btn-default-outline hover">모의펀딩 가이드</a>
												</div>
												<!-- <div class="col-sm-6">
													<a href="javascript:void(0)" class="btn btn-block btn-sps btn-shadow btn-primary-outline hover hidden-xs">모의크라우드펀딩 시작하기</a>
													<div class="mt15 grey-800 big-hb hidden-sm hidden-md hidden-lg text-center">모의크라우드펀딩 시작하기는 PC에서 가능합니다.</div>
												</div> -->
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="spstrat_wrap">
						<div class="container container_2">
							<div class="spstrat_top">
								CROWDY와 함께하는 프로젝트
							</div>

							
						</div>

						<div class="container container_2">
							<div class="pl15 pr15 xs-pl0 xs-pr0">
								<div class="row">
									<div class="col-sm-6">
										<div class="spstrat_frame">
											<strong>CROWDY 도움말</strong>
											<p>
												프로젝트를 시작하시는데 어려움을 겪고 계신가요? <br />
												도움말을 참고해보세요!
											</p>
											<div class="row row-mobile-n">
												<div class="col-xs-6 col-xs-offset-3">
													<a href="/crowdy/help" target="_blank" class="btn btn-sps m-btn-block btn-primary-outline">자세히 보기</a>
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="spstrat_frame">
											<strong>서비스 운영정책</strong>
											<p>
												서비스, 이용약관 및 서비스 운영정책에 <br />
												대해 안내해 드립니다.
											</p>
											<div class="row row-mobile-n">
												<div class="col-xs-6 col-xs-offset-3">
													<a href="/crowdy/term?menu=2" class="btn btn-sps m-btn-block btn-primary-outline">자세히 보기</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			`,
			methods : {
				createRewardProject : function() {
					// 로그인했는지 확인 
					if(!userInfo.loginConfirm()) return;
					
					// 리워드 프로젝트 만들고 만들기 페이지로 이동
					axios.post('/data/save/reward/create', {memCode : userInfo.memCode})
						.then(function(response) {
						 	var result = response.data;
						 	if(result.rCode == "0000") {
						 		window.open("/make/reward/main/" + result.rData.cpCode, '_self')
						 	} else {
						 		noti.open('시스템 오류입니다. 다시 시도해주세요.');
						 	}
						});
				},
				requestInvestProject : function() {
					if(!userInfo.loginConfirm()) return;
					if(userInfo.type != '2') {
						noti.open('투자형 크라우드펀딩은<br /> 법인만 가능합니다.')
						return;
					}
					var self = this;
                    axios.post('/data/member/investor/state', {memCode : userInfo.memCode})
                        .then(function(response) {
                            var result = response.data.rData;
                            if(result.investor != 'Y' || (result.investor == 'Y' && result.memIvsState == 'MIC001')) {
                            	noti.open('투자회원으로 등록해야 프로젝트를 시작할 수 있습니다.<br /> 설정 페이지로 이동하시겠습니까?', function() {window.open('/mypage/main?menu=5&sub-menu=2', '_self')}, true);
								return;
                            }

                            window.open("/make/invest/request", '_self')
                        })
				}
			}
		}
	}
}

export default new ProjectStart()