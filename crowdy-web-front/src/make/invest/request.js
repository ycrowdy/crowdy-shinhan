class InvestRequest {
	component() {
		return {
			template : `
				<form class="form-horizontal">
					<div class="container">
						<div class="row row-mobile-n">
							<div class="col-md-2">
								<div class="sr-subtitle-s">
									<strong>투자 의뢰<br />기초자료</strong>
								</div>
							</div>
							<div class="col-md-10">
								<div class="form-group row-mobile-n">
									<label for="attraction_fund" class="col-xs-12 control-label">
										<div class="text-left">투자 유치자금 규모</div>
									</label>
									<div class="col-xs-12 col-sm-4">
										<vue-numeric class="form-control" separator="," currency="₩" v-model="request.pjAmount" data-vv-name="pjAmount" v-validate="'required|min_value:1000'" :class="{'error' : errors.has('pjAmount')}"></vue-numeric>
										<label class="error" v-if="errors.has('pjAmount')" v-text="errors.first('pjAmount')"></label>
									</div>
								</div>

								<div class="form-group row-mobile-n">
									<label for="business_info" class="col-xs-12 control-label">
										<div class="text-left">사업내용</div>
									</label>
									<div class="col-xs-12">
										<textarea rows="4" cols="50" class="form-control" :class="{'error' : errors.has('pjContent')}" data-vv-name="pjContent" v-model="request.pjContent" v-validate="'required'"></textarea>
										<label class="error" v-if="errors.has('pjContent')" v-text="errors.first('pjContent')"></label>
									</div>
								</div>
								
								<div class="form-group row-mobile-n">
									<label for="company_profile" class="col-xs-12 control-label">
										<div class="text-left">회사소개서 <span class="red-800">(선택사항)</span></div>
									</label>
									<div class="col-xs-12 col-sm-7">
										<div class="input-group input-group-file">
											<input type="text" class="form-control" :class="{'error' : errors.has('file')}" readonly="readonly" v-model="request.file.fileName" />
											<span class="input-group-btn">
												<span class="btn btn-outline btn-file">
													<i class="fa fa-upload" aria-hidden="true"></i>
													<input type="file" v-on:change="fileUpload" data-vv-name="file" v-validate="'size:10240'"  />
												</span>
											</span>
										</div>
									</div>
									<label for="company_profile" class="col-xs-12 control-label" v-if="errors.has('file')">
										<div class="text-left"><span class="red-800" v-text="errors.first('file')"></span></div>
									</label>
								</div>
								
								<div class="form-group row-mobile-n">
									<div class="col-xs-12">
										<div class="form-control-static form-control-static-grey">
											신청 후 5일 이내에 투자팀의 심사를 거쳐 연락을 드립니다.<br />
											크라우디와 함께 성장하고 싶은 스타트업들의 많은 신청 부탁드립니다.
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>

					<hr class="wline" />
				
					<div class="container">
						<div class="text-center form-control-static form-control-static-grey">
							<span class="red-800">승인후 프로세스 안내</span>
							<div class="mt10">
								투자팀 PM이 의뢰가 들어온 투자 프로젝트를 승인완료 하면,<br />
								의뢰한 회원은 마이페이지 <i class="fa fa-angle-right" aria-hidden="true"></i> 나의 프로젝트 <i class="fa fa-angle-right" aria-hidden="true"></i> 투자프로젝트 <i class="fa fa-angle-right" aria-hidden="true"></i> 시작하기에서 온라인 IR 작성이 가능합니다.
							</div>
						</div>

						<div class="form-group row-mobile mt40 pb30">
							<div class="col-xs-4 col-xs-offset-4 col-sm-4 col-sm-offset-4">
								<div class="row not-space">
									<div class="col-sm-10 col-sm-offset-1">
										<a href="javascript:void(0)" class="btn btn-block btn-lg btn-primary" v-on:click="completed">신청하기</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			`,
			data : function() {
				return {
					request : {
						memCode : userInfo.memCode,
						pjAmount : '',
						pjContent : '',
						file : {
							fileData : "",
							fileName : ""
						}
					}
				}
			},
			components : {
				vueNumeric : VueNumeric.default
			},
			methods : {
				fileUpload : function(e) {
					var self = this;
					var files = e.target.files || e.dataTransfer.files;
					if (!files.length) return;
					
					var reader = new FileReader();
					reader.onload = function(e) {
						self.request.file.fileData = e.target.result;
						self.request.file.fileName = files[0].name;
					};
					reader.readAsDataURL(files[0]);
				},
				completed : function() {
					var self = this;
					this.$validator.validateAll()
						.then(function(sucess) {
							if(!sucess) return;

							$('.page-loader-more').fadeIn('')
							axios.post('/set/invest/request', self.request)
			                    .then(function(response){
			                    	$('.page-loader-more').fadeOut('')
			                    	var result = response.data;
			                    	if(result.rCode == "0000") {
			                    		$('#completedModal').modal('show');
			                    	} else {
			                    		noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
			                    	}
			                    })
						})
				}
			}
		}
	}
}


export default new InvestRequest()