class CompanyBasic {
	component() {
		return {
			template : `
				<div class="col-md-10 col-line">
					<div class="row not-space">
						<div class="col-lg-1"></div>
						<div class="col-lg-11">
							<div class="md-ml30 mt5 md-mt0">
								<form class="form-horizontal">

									<div class="form-group row-mobile-n">
										<label for="com_sectors" class="col-xs-5 control-label control-label-big">
											<div class="text-left">업종</div>
										</label>
										<label for="com_ceo" class="col-xs-5 control-label control-label-big">
											<div class="text-left">대표이사</div>
										</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" v-model.trim="request.companyType" :disabled="pjStatus == 1"/>
										</div>
										<div class="col-sm-1"></div>
										<div class="col-sm-4">
											<input type="text" class="form-control" v-model.trim="request.companyCeo" :disabled="pjStatus == 1"/>
										</div>
									</div>
									
									<div class="form-group row-mobile-n">
										<label for="com_establishment" class="col-xs-5 control-label control-label-big">
											<div class="text-left">설립일</div>
										</label>
										<label for="com_employees" class="col-xs-5 control-label control-label-big">
											<div class="text-left">임직원 수</div>
										</label>
										<div class="col-sm-4">
											<date-picker v-model="request.companyIncoDate" :date="request.companyIncoDate" v-if="dataConfirm" :disabled="pjStatus == 1"></date-picker>
										</div>
										<div class="col-sm-1"></div>
										<div class="col-sm-4">
											<vue-numeric class="form-control" separator="," currency="" v-model.trim="request.companyStaffCount" :disabled="pjStatus == 1"></vue-numeric>
										</div>
										<div class="col-sm-1">
											<span class="textarea_text_leng webfont2 mt10">명</span>
										</div>
									</div>
									
									<div class="form-group row-mobile-n">
										<label for="zip_code" class="col-xs-12 control-label control-label-big">
											<div class="text-left">
												우편번호
											</div>
										</label>
										<div class="col-sm-6">
											<div class="row row-mobile-n">
												<div class="col-sm-9">
													<div class="input-group input-group-file">
														<input type="tel" class="form-control" v-on:click="postOpen" v-model="address.postNum" readonly="readonly" style="position: relative; z-index: 1;"/>
														<span class="input-group-btn" v-on:click="postOpen">
															<span class="btn btn-outline btn-file">
																<i class="fa fa-upload" aria-hidden="true"></i>
															</span>
														</span>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="form-group row-mobile-n">
										<div class="col-md-10">
											<div class="row row-mobile-n">
												<label for="address1" class="col-xs-12 control-label control-label-big">
													<div class="text-left">
														주소
													</div>
												</label>
												<div class="col-xs-12 mb10">
													<input type="text" class="form-control" v-on:click="postOpen" v-model="address1" placeholder="주소를 검색해주세요." readonly="readonly" />
												</div>
												<div class="col-xs-12">
													<input type="text" class="form-control" v-model="request.companyAddress2" placeholder="상세 주소를 입력해주세요." :disabled="pjStatus == 1"/>
												</div>
											</div>
										</div>
									</div>
									
									<div class="form-group row-mobile-n">
										<label for="com_website" class="col-xs-12 control-label control-label-big">
											<div class="text-left">
												웹사이트
												<div><span class="red-800">(선택사항)</span></div>
											</div>
										</label>
										<div class="col-xs-12 col-sm-7">
											<input type="text" class="form-control" v-model="request.companyWebSite" :disabled="pjStatus == 1" />
										</div>
									</div>
									
									<div class="form-group row-mobile-n">
										<label for="com_email" class="col-xs-12 control-label control-label-big">
											<div class="text-left">대표 이메일</div>
										</label>
										<div class="col-xs-12 col-sm-7">
											<input type="text" class="form-control" v-model="request.companyEmail" :disabled="pjStatus == 1" />
										</div>
									</div>
									
									<div class="form-group row-mobile-n">
										<label for="crash_history" class="col-xs-12 control-label control-label-big">
											<div class="text-left">
												범죄이력
											</div>
										</label>
										<div class="col-xs-12 col-sm-7">
											<div class="option_group option_group_none">
												<label for="crash1" class="radio-inline">
													<input type="radio" name="crash_history" id="crash1" value="N" v-model="request.companyLaw" :disabled="pjStatus == 1" /><span class="webfont">없음</span>
												</label>
												<label for="crash2" class="radio-inline">
													<input type="radio" name="crash_history" id="crash2" value="Y" v-model="request.companyLaw" :disabled="pjStatus == 1" /><span class="webfont">있음</span>
												</label>
											</div>
										</div>
									</div>
									
									<div class="form-group row-mobile-n mt40">
				                        <div class="col-sm-11">
				                            <div class="form-group row-mobile-n">
				                                <div class="col-xs-6 col-sm-4 col-md-3">
				                                    <a class="btn btn-block btn-primary-outline" v-on:click="save">저장</a>
				                                </div>
				                                <div class="col-xs-6 col-sm-4 col-md-3">
				                                    <a class="btn btn-block btn-primary" v-on:click="save(true)">저장 후  다음단계</a>
				                                </div>
				                            </div>
				                        </div>
				                    </div>

								</form>
							</div>
						</div>
					</div>
				</div>
			`,
			props : ['pjCode', 'pjStatus'],
			data : function() {
				return {
					dataConfirm : false,
					request : {
							companyType : '',
							companyCeo : '',
							companyStaffCount : 0,
							companyIncoDate : '',
							companyPostNum : '',
							companyAddress1 : '',
							companyAddress2 : '',
							companyWebSite : '',
							companyEmail : '',
							companyLaw : 'N'
						},
					address : {
						postNum : '',
						address1 : ''
					}
				}
			},
			computed : {
				postNum : function() {
					this.request.companyPostNum = this.address.postNum
					return this.address.postNum
				},
				address1 : function() {
					this.request.companyAddress1 = this.address.address1
					return this.address.address1
				}
			},
			components : {
				datePicker : require('../../common/date-picker.js').default.component(),
				vueNumeric : VueNumeric.default
			},		
			created :  function() {
				this.load();
			},		
			methods : {
				load : function() {
					var self = this;
					axios.post('/data/save/invest/info/company-basic', {pjCode : this.pjCode})
	                    .then(function(response){
	                    	var result = response.data.rData;
                			var data = self.request;

                			data.companyType = result.companyType;
                			data.companyCeo = result.companyCeo;
                			data.companyStaffCount = result.companyStaffCount;
                			data.companyIncoDate = result.companyIncoDate;
                			data.companyPostNum = result.companyPostNum;
                			data.companyAddress1 = result.companyAddress1;
                			data.companyAddress2 = result.companyAddress2;
                			data.companyWebSite = result.companyWebSite;
                			data.companyEmail = result.companyEmail;
                			data.companyLaw = result.companyLaw;

                			self.address.postNum = result.companyPostNum;
                			self.address.address1 = result.companyAddress1;
                			self.dataConfirm = !self.dataConfirm
                			self.$nextTick(function() {
						    	$( ".option_group .radio-inline input" ).checkboxradio();
								$( ".crash_history" ).controlgroup();
						    });
	                    })
				},
				postOpen : function() {
					if(this.pjStatus == 1) return;
					post.open(this);
				},
				save : function(next) {
					if(this.pjStatus == 1) {
						noti.open('승인대기 중 인 프로젝트는 수정이 불가능합니다.')
						return;
					}

					var self = this;
					$('.page-loader-more').fadeIn('')
					this.request.pjCode = this.pjCode;
					this.request.companyPostNum = this.address.postNum
					this.request.companyAddress1 = this.address.address1
					axios.post('/set/save/invest/company-basic', this.request)
						.then(function(response) {
							$('.page-loader-more').fadeOut('');
							var result = response.data;
							if(result.rCode == "0000") {
								
		                    	if(next == true) {
		                    		self.$emit('step-change', 3, true);
		                    	} else {
		                    		noti.open("저장되었습니다.")
		                    		self.$emit('step-change', 3, false);
		                    	}
	                    	} else {
	                    		noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
	                    	}
						})
				}
			}
		}
	}
}

export default new CompanyBasic()