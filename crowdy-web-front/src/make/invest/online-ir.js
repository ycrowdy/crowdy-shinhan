class OnlineIr {
	component() {
		return {
			template : `
				<div class="col-md-10 col-line">
					<div class="row not-space">
						<div class="col-lg-1"></div>
						<div class="col-lg-11">
							<div class="md-ml30 mt10">
								<p class="form-control-static form-control-static-grey2 mt10 mb70">
									온라인IR에 필요한 정보들을 선택하여 작성해 주세요
								</p>
								<form class="form-horizontal">
									<div class="panel-group faq invest">

										<div class="panel panel-default">
											<div class="panel-heading">
												<div class="panel-title" :class="{'checked' : checkInput.content}">
													<label for="core-inve0" class="checkbox-inline" data-toggle="collapse" data-target="#collapse1000" aria-expanded="false" aria-controls="collapse1000">
														<input type="checkbox" id="core-inve0" v-model="checkInput.content" /><span class="label"></span><span class="label-text">프로젝트 소개</span>
													</label>
												</div>
											</div>
											<div id="collapse1000" class="panel-collapse collapse" :class="{'in' : checkInput.content}">
												<div class="panel-body panel-edit">
													<ir-area :id="'editor0'" v-if="dataConfirm" :value="request.companyContent" v-model="request.companyContent" :disabled="pjStatus != 0"></ir-area>
												</div>
											</div>
										</div>
										
										<div class="panel panel-default">
											<div class="panel-heading">
												<div class="panel-title" :class="{'checked' : checkInput.problem}">
													<label for="core-inve1" class="checkbox-inline" data-toggle="collapse" data-target="#collapse1001" aria-expanded="false" aria-controls="collapse1001">
														<input type="checkbox" id="core-inve1" v-model="checkInput.problem" /><span class="label"></span><span class="label-text">문제점 / 해결방안</span>
													</label>
												</div>
											</div>
											<div id="collapse1001" class="panel-collapse collapse" :class="{'in' : checkInput.problem}">
												<div class="panel-body panel-edit">
													<ir-area :id="'editor1'" v-if="dataConfirm" :value="request.companyProblem" v-model="request.companyProblem" :disabled="pjStatus != 0"></ir-area>
												</div>
											</div>
										</div>

										<div class="panel panel-default">
											<div class="panel-heading">
												<div class="panel-title" :class="{'checked' : checkInput.service}">
													<label for="core-inve2" class="checkbox-inline" data-toggle="collapse" data-target="#collapse1002" aria-expanded="false" aria-controls="collapse1002">
														<input type="checkbox" id="core-inve2" v-model="checkInput.service" /><span class="label"></span><span class="label-text">제품 / 서비스 소개</span>
													</label>
												</div>
											</div>
											<div id="collapse1002" class="panel-collapse collapse" :class="{'in' : checkInput.service}">
												<div class="panel-body panel-edit">
													<ir-area :id="'editor2'" v-if="dataConfirm" :value="request.companyService" v-model="request.companyService" :disabled="pjStatus != 0"></ir-area>
												</div>
											</div>
										</div>

										<div class="panel panel-default">
											<div class="panel-heading">
												<div class="panel-title" :class="{'checked' : checkInput.trend}">
													<label for="core-inve3" class="checkbox-inline" data-toggle="collapse" data-target="#collapse1003" aria-expanded="false" aria-controls="collapse1003">
														<input type="checkbox" id="core-inve3" v-model="checkInput.trend" /><span class="label"></span><span class="label-text">시장분석</span>
													</label>
												</div>
											</div>
											<div id="collapse1003" class="panel-collapse collapse" :class="{'in' : checkInput.trend}">
												<div class="panel-body panel-edit">
													<ir-area :id="'editor3'" v-if="dataConfirm" :value="request.companyTrend" v-model="request.companyTrend" :disabled="pjStatus != 0"></ir-area>
												</div>
											</div>
										</div>
										
										<div class="panel panel-default">
											<div class="panel-heading">
												<div class="panel-title" :class="{'checked' : checkInput.validate}">
													<label for="core-inve4" class="checkbox-inline" data-toggle="collapse" data-target="#collapse1004" aria-expanded="false" aria-controls="collapse1004">
														<input type="checkbox" id="core-inve4" v-model="checkInput.validate" /><span class="label"></span><span class="label-text">시장성 검증</span>
													</label>
												</div>
											</div>
											<div id="collapse1004" class="panel-collapse collapse" :class="{'in' : checkInput.validate}">
												<div class="panel-body panel-edit">
													<ir-area :id="'editor4'" v-if="dataConfirm" :value="request.companyValidate" v-model="request.companyValidate" :disabled="pjStatus != 0"></ir-area>
												</div>
											</div>
										</div>

										<div class="panel panel-default">
											<div class="panel-heading">
												<div class="panel-title" :class="{'checked' : checkInput.biz}">
													<label for="core-inve5" class="checkbox-inline" data-toggle="collapse" data-target="#collapse1005" aria-expanded="false" aria-controls="collapse1005">
														<input type="checkbox" id="core-inve5" v-model="checkInput.biz" /><span class="label"></span><span class="label-text">비즈니스 모델</span>
													</label>
												</div>
											</div>
											<div id="collapse1005" class="panel-collapse collapse" :class="{'in' : checkInput.biz}">
												<div class="panel-body panel-edit">
													<ir-area :id="'editor5'" v-if="dataConfirm" :value="request.companyBiz" v-model="request.companyBiz" :disabled="pjStatus != 0"></ir-area>
												</div>
											</div>
										</div>

										<div class="panel panel-default">
											<div class="panel-heading">
												<div class="panel-title" :class="{'checked' : checkInput.compare}">
													<label for="core-inve6" class="checkbox-inline" data-toggle="collapse" data-target="#collapse1006" aria-expanded="false" aria-controls="collapse1006">
														<input type="checkbox" id="core-inve6" v-model="checkInput.compare" /><span class="label"></span><span class="label-text">경쟁자비교 / 경쟁우위</span>
													</label>
												</div>
											</div>
											<div id="collapse1006" class="panel-collapse collapse" :class="{'in' : checkInput.compare}">
												<div class="panel-body panel-edit">
													<ir-area :id="'editor6'" v-if="dataConfirm" :value="request.companyCompare" v-model="request.companyCompare" :disabled="pjStatus != 0"></ir-area>
												</div>
											</div>
										</div>

										<div class="panel panel-default">
											<div class="panel-heading">
												<div class="panel-title" :class="{'checked' : checkInput.atPlan}">
													<label for="core-inve7" class="checkbox-inline" data-toggle="collapse" data-target="#collapse1007" aria-expanded="false" aria-controls="collapse1007">
														<input type="checkbox" id="core-inve7" v-model="checkInput.atPlan" /><span class="label"></span><span class="label-text">고객획득 전략 / 실행전략</span>
													</label>
												</div>
											</div>
											<div id="collapse1007" class="panel-collapse collapse" :class="{'in' : checkInput.atPlan}">
												<div class="panel-body panel-edit">
													<ir-area :id="'editor7'" v-if="dataConfirm" :value="request.companyAtPlan" v-model="request.companyAtPlan" :disabled="pjStatus != 0"></ir-area>
												</div>
											</div>
										</div>
										
										<div class="panel panel-default">
											<div class="panel-heading">
												<div class="panel-title" :class="{'checked' : checkInput.riExplan}">
													<label for="core-inve8" class="checkbox-inline" data-toggle="collapse" data-target="#collapse1008" aria-expanded="false" aria-controls="collapse1008">
														<input type="checkbox" id="core-inve8" v-model="checkInput.riExplan" /><span class="label"></span><span class="label-text">리스크요인 / 대응전략</span>
													</label>
												</div>
											</div>
											<div id="collapse1008" class="panel-collapse collapse" :class="{'in' : checkInput.riExplan}">
												<div class="panel-body panel-edit">
													<ir-area :id="'editor8'" v-if="dataConfirm" :value="request.companyRiExplan" v-model="request.companyRiExplan" :disabled="pjStatus != 0"></ir-area>
												</div>
											</div>
										</div>

										<div class="panel panel-default">
											<div class="panel-heading">
												<div class="panel-title" :class="{'checked' : checkInput.nowState}">
													<label for="core-inve9" class="checkbox-inline" data-toggle="collapse" data-target="#collapse1009" aria-expanded="false" aria-controls="collapse1009">
														<input type="checkbox" id="core-inve9" v-model="checkInput.nowState" /><span class="label"></span><span class="label-text">기존재무현황</span>
													</label>
												</div>
											</div>
											<div id="collapse1009" class="panel-collapse collapse" :class="{'in' : checkInput.nowState}">
												<div class="panel-body panel-edit">
													<ir-area :id="'editor9'" v-if="dataConfirm" :value="request.companyNowState" v-model="request.companyNowState" :disabled="pjStatus != 0"></ir-area>
												</div>
											</div>
										</div>

										<div class="panel panel-default">
											<div class="panel-heading">
												<div class="panel-title" :class="{'checked' : checkInput.pinState}">
													<label for="core-inve10" class="checkbox-inline" data-toggle="collapse" data-target="#collapse10010" aria-expanded="false" aria-controls="collapse10010">
														<input type="checkbox" id="core-inve10" v-model="checkInput.pinState" /><span class="label"></span><span class="label-text">추정재무제표</span>
													</label>
												</div>
											</div>
											<div id="collapse10010" class="panel-collapse collapse" :class="{'in' : checkInput.pinState}">
												<div class="panel-body panel-edit">
													<ir-area :id="'editor10'" v-if="dataConfirm" :value="request.companyPinState" v-model="request.companyPinState" :disabled="pjStatus != 0"></ir-area>
												</div>
											</div>
										</div>

										<div class="panel panel-default">
											<div class="panel-heading">
												<div class="panel-title" :class="{'checked' : checkInput.value}">
													<label for="core-inve11" class="checkbox-inline" data-toggle="collapse" data-target="#collapse10011" aria-expanded="false" aria-controls="collapse10011">
														<input type="checkbox" id="core-inve11" v-model="checkInput.value" /><span class="label"></span><span class="label-text">기업가치 및 펀딩제안</span>
													</label>
												</div>
											</div>
											<div id="collapse10011" class="panel-collapse collapse" :class="{'in' : checkInput.value}">
												<div class="panel-body panel-edit">
													<ir-area :id="'editor11'" v-if="dataConfirm" :value="request.companyValue" v-model="request.companyValue" :disabled="pjStatus != 0"></ir-area>
												</div>
											</div>
										</div>

										<div class="panel panel-default">
											<div class="panel-heading">
												<div class="panel-title" :class="{'checked' : checkInput.plan}">
													<label for="core-inve12" class="checkbox-inline" data-toggle="collapse" data-target="#collapse10012" aria-expanded="false" aria-controls="collapse10012">
														<input type="checkbox" id="core-inve12" v-model="checkInput.plan" /><span class="label"></span><span class="label-text">자금사용계획</span>
													</label>
												</div>
											</div>
											<div id="collapse10012" class="panel-collapse collapse" :class="{'in' : checkInput.plan}">
												<div class="panel-body panel-edit">
													<ir-area :id="'editor12'" v-if="dataConfirm" :value="request.companyPlan" v-model="request.companyPlan" :disabled="pjStatus != 0"></ir-area>
												</div>
											</div>
										</div>

										<div class="panel panel-default">
											<div class="panel-heading">
												<div class="panel-title" :class="{'checked' : checkInput.staff}">
													<label for="core-inve13" class="checkbox-inline" data-toggle="collapse" data-target="#collapse10013" aria-expanded="false" aria-controls="collapse10013">
														<input type="checkbox" id="core-inve13" v-model="checkInput.staff" /><span class="label"></span><span class="label-text">팀소개/주주구성</span>
													</label>
												</div>
											</div>
											<div id="collapse10013" class="panel-collapse collapse" :class="{'in' : checkInput.staff}">
												<div class="panel-body panel-edit">
													<ir-area :id="'editor13'" v-if="dataConfirm" :value="request.companyStaff" v-model="request.companyStaff" :disabled="pjStatus != 0"></ir-area>
												</div>
											</div>
										</div>

										<div class="panel panel-default">
											<div class="panel-heading">
												<div class="panel-title" :class="{'checked' : checkInput.patent}">
													<label for="core-inve14" class="checkbox-inline" data-toggle="collapse" data-target="#collapse10014" aria-expanded="false" aria-controls="collapse10014">
														<input type="checkbox" id="core-inve14" v-model="checkInput.patent" /><span class="label"></span><span class="label-text">특허/언론보도/수상내용</span>
													</label>
												</div>
											</div>
											<div id="collapse10014" class="panel-collapse collapse" :class="{'in' : checkInput.patent}">
												<div class="panel-body panel-edit">
													<ir-area :id="'editor14'" v-if="dataConfirm" :value="request.companyPatent" v-model="request.companyPatent" :disabled="pjStatus != 0"></ir-area>
												</div>
											</div>
										</div>

									</div>

									<div class="form-group row-mobile-n mt40">
				                        <div class="col-sm-11">
				                            <div class="form-group row-mobile-n">
				                                <div class="col-xs-6 col-sm-4 col-md-3">
				                                    <a class="btn btn-block btn-primary-outline" v-on:click="save">저장</a>
				                                </div>
				                                <div class="col-xs-6 col-sm-4 col-md-3">
				                                    <a class="btn btn-block btn-primary" v-on:click="save(true)">저장 후  다음단계</a>
				                                </div>
				                            </div>
				                        </div>
				                    </div>

								</form>
							</div>
						</div>
					</div>
				</div>
			`,
			props : ['pjCode', 'pjStatus'],
			data : function() {
				return {
					dataConfirm : false,
					request : {
						companyContent : '',
						companyProblem : '',
						companyRiExplan : '',
						companyService : '',
						companyTrend : '',
						companyValidate : '',
						companyBiz : '',
						companyCompare : '',
						companyAtPlan : '',
						companyNowState : '',
						companyPinState : '',
						companyValue : '',
						companyPlan : '',
						companyStaff : '',
						companyPatent : ''
					},
					checkInput : {
						content : false,
						problem : false,
						riExplan : false,
						service : false,
						trend : false,
						validate : false,
						biz : false,
						compare : false,
						atPlan : false,
						nowState : false,
						pinState : false,
						value : false,
						plan : false,
						staff : false,
						patent : false,
					}
				}
			},
			computed : {
				content : function() {
					return this.request.companyContent != '' ? true : false;
				},
				problem : function() {
					return this.request.companyProblem != '' ? true : false;
				},
				riExplan : function() {
					return this.request.companyRiExplan != '' ? true : false;
				},
				service : function() {
					return this.request.companyService != '' ? true : false;
				},
				trend : function() {
					return this.request.companyTrend != '' ? true : false;
				},
				validate : function() {
					return this.request.companyValidate != '' ? true : false;
				},
				biz : function() {
					return this.request.companyBiz != '' ? true : false;
				},
				compare : function() {
					return this.request.companyCompare != '' ? true : false;
				},
				atPlan : function() {
					return this.request.companyAtPlan != '' ? true : false;
				},
				nowState : function() {
					return this.request.companyNowState != '' ? true : false;
				},
				pinState : function() {
					return this.request.companyPinState != '' ? true : false;
				},
				value : function() {
					return this.request.companyValue != '' ? true : false;
				},
				plan : function() {
					return this.request.companyPlan != '' ? true : false;
				},
				staff : function() {
					return this.request.companyStaff != '' ? true : false;
				},
				patent : function() {
					return this.request.companyPatent != '' ? true : false;
				}
			},
			created : function() {
				this.load();
			},
			components : {
				irArea : require('../../common/text-editor.js').default.component()
			},
			methods : {
				load : function() {
					var self = this;
					axios.post('/data/save/invest/info/online-ir', {pjCode : this.pjCode})
	                    .then(function(response){
	                    	var result = response.data.rData;
	                    	var data = self.request;
	                    	
	                    	data.companyContent = result.companyContent
							data.companyProblem = result.companyProblem
							data.companyRiExplan = result.companyRiExplan
							data.companyService = result.companyService
							data.companyTrend = result.companyTrend
							data.companyValidate = result.companyValidate
							data.companyBiz = result.companyBiz
							data.companyCompare = result.companyCompare
							data.companyAtPlan = result.companyAtPlan
							data.companyNowState = result.companyNowState
							data.companyPinState = result.companyPinState
							data.companyValue = result.companyValue
							data.companyPlan = result.companyPlan
							data.companyStaff = result.companyStaff
							data.companyPatent = result.companyPatent

	                    	self.checkInput.content = self.content
							self.checkInput.problem = self.problem
							self.checkInput.riExplan = self.riExplan
							self.checkInput.service = self.service
							self.checkInput.trend = self.trend
							self.checkInput.validate = self.validate
							self.checkInput.biz = self.biz
							self.checkInput.compare = self.compare
							self.checkInput.atPlan = self.atPlan
							self.checkInput.nowState = self.nowState
							self.checkInput.pinState = self.pinState
							self.checkInput.value = self.value
							self.checkInput.plan = self.plan
							self.checkInput.staff = self.staff
							self.checkInput.patent = self.patent

							self.dataConfirm = !self.dataConfirm
	                    })
				},
				save : function(next) {
					if(this.pjStatus != 0) {
						noti.open('승인대기 중 이거나 진행중인 프로젝트는 수정이 불가능합니다.')
						return;
					}

					var self = this;
					$('.page-loader-more').fadeIn('')
					this.request.pjCode = this.pjCode;

					axios.post('/set/save/invest/online-ir', this.request)
						.then(function(response) {
							$('.page-loader-more').fadeOut('');
							var result = response.data;
							if(result.rCode == "0000") {
								
		                    	if(next == true) {
		                    		self.$emit('step-change', 6, true);
		                    	} else {
		                    		noti.open("저장되었습니다.")
		                    		self.$emit('step-change', 6, false);
		                    	}
	                    	} else {
	                    		noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
	                    	}
						})
				}
			}

		}
	}
}

export default new OnlineIr()