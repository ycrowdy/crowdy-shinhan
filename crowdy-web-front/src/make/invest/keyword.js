class KeyWord {
	component() {
		return {
			template : `
				<div class="col-md-10 col-line">
					<div class="row not-space">
						<div class="col-lg-1"></div>
						<div class="col-lg-11">
							<div class="md-ml30">
								<form class="form-horizontal">
									<div class="form-group row-mobile-n">
										<label for="mc_keyword" class="col-xs-12 control-label control-label-big">
											<div class="text-left">
												프로젝트 키워드 등록 <a class="form-tip webuiPopover" href="javascript:void(0)" data-plugin="webuiPopover" data-content="키워드는 최대 다섯개까지 등록할 수 있습니다. 키워드를 등록 해두는 경우, &lt;br / &gt;프로젝트 제목 외에도 해당 키워드 검색 시 검색 결과에 프로젝트가 포함되게 됩니다." data-animation="pop"><i class="fa fa-question" aria-hidden="true"></i></a>
												<br /><span class="red-800">(선택사항)</span>
											</div>
										</label>
										<div class="col-xs-7">
											<input type="text" data-limit="5" v-model.trim="keyWords" id="keyword" placeholder="최대 5개까지 등록 가능합니다." :disabled="pjStatus != 0"/>
										</div>
									</div>

									<div class="form-group row-mobile-n mt40">
				                        <div class="col-sm-11">
				                            <div class="form-group row-mobile-n">
				                                <div class="col-xs-6 col-sm-4 col-md-3">
				                                    <button type="button" class="btn btn-block btn-primary-outline" v-on:click="save" :disabled="pjStatus != 0">저장</button>
				                                </div>
				                                <div class="col-xs-6 col-sm-4 col-md-3">
				                                    <button type="button" class="btn btn-block btn-primary" v-on:click="approve" :disabled="!approveCheck || pjStatus != 0">승인요청하기</button>
				                                </div>
				                            </div>
				                        </div>
				                    </div>
								</form>
							</div>
						</div>
					</div>
				</div>
			`,
			props : ['pjCode', 'approveCheck', 'pjStatus'],
			data : function() {
				return {
					request : {
						keyWords : []
					},
					keyWords : []
				}
			},
			created : function() {
				this.load();
			},
			methods : {
				load : function() {
					var self = this;
					axios.post('/data/save/invest/info/keyword', {pjCode : this.pjCode})
	                    .then(function(response){
	                    	var result = response.data.rData;
	            			var data = self.request;
	            			self.keyWords = result.keyWords;

	            			self.$nextTick(function() {
						    	$('#keyword')
							  	.on('tokenfield:createtoken', function (e) {
								    self.request.keyWords.push(e.attrs.value)
							  	})
								.on('tokenfield:removedtoken', function (e) {
									self.request.keyWords = _.without(self.request.keyWords, e.attrs.value)
								})
								.tokenfield();
					    	});
                		})
				},
				save : function() {
					if(this.pjStatus != 0) return;

					var self = this;
					$('.page-loader-more').fadeIn('')
					this.request.pjCode = this.pjCode;

					axios.post('/set/save/invest/keyword', this.request)
						.then(function(response) {
							$('.page-loader-more').fadeOut('');
							var result = response.data;
							if(result.rCode == "0000") {
	                    		noti.open("저장되었습니다.")
	                    	} else {
	                    		noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
	                    	}
						})
				},
				approve : function() {
					if(this.pjStatus != 0) return;

					this.$emit('approve');
				}
			}
		}
	}
}

export default new KeyWord()