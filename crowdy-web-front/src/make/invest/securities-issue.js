class SecuritiesIssue {
	component() {
		return {
			template : `
				<div class="col-md-10 col-line">
					<div class="row not-space">
						<div class="col-lg-1"></div>
						<div class="col-lg-11">
							<div class="md-ml30 mt15">
								<div class="fp-items-method mb60">
									<div class="fp-items-method-tab">
										<label for="method_set1" class="radio-btn webfont2">
											주식형 <input type="radio" name="method_set" id="method_set1" value="1" v-model="request.pjStockType" :disabled="pjStatus != 0"/>
										</label>
										<label for="method_set2" class="radio-btn webfont2">
											채권형 <input type="radio" name="method_set" id="method_set2" value="2" v-model="request.pjStockType" :disabled="pjStatus != 0"/>
										</label>
									</div>
								</div>
								
								<div class="ptab_contents">
									<form class="form-horizontal">
										<div class="form-group row-mobile-n mb10">
											<label for="stock_pu_type1" class="col-xs-3 control-label control-label-big">
												<div class="text-left">공개발행 여부</div>
											</label>
											<div class="col-xs-9">
												<div class="option_group option_group_none">
													<div class="stock_pu_type">
														<label for="stock_pu_type1" class="radio-inline">
															<input type="radio" name="stock_pu_type" id="stock_pu_type1" value="1" v-model="request.pjType" :disabled="pjStatus != 0"/><span class="webfont">공모</span>
														</label>
														<label for="stock_pu_type2" class="radio-inline">
															<input type="radio" name="stock_pu_type" id="stock_pu_type2" value="2" v-model="request.pjType" :disabled="pjStatus != 0"/><span class="webfont">사모</span>
														</label>
													</div>
												</div>
											</div>
										</div>

										<div class="form-group row-mobile-n mb10">
											<label for="stock_Income_type1" class="col-xs-3 control-label control-label-big">
												<div class="text-left">소득공제 가능여부</div>
											</label>
											<div class="col-xs-9">
												<div class="option_group option_group_none">
													<div class="stock_income_type">
														<label for="stock_income_type1" class="radio-inline">
															<input type="radio" name="stock_income_type" id="stock_income_type1" value="N" v-model="request.pjDeductionConfirm" :disabled="pjStatus != 0"/><span class="webfont">불가능</span>
														</label>
														<label for="stock_income_type2" class="radio-inline">
															<input type="radio" name="stock_income_type" id="stock_income_type2" value="Y" v-model="request.pjDeductionConfirm" :disabled="pjStatus != 0"/><span class="webfont">가능</span>
														</label>
													</div>
												</div>
											</div>
										</div>

										<div class="form-group row-mobile-n">
											<label for="stock_type_0" class="col-xs-3 control-label control-label-big">
												<div class="text-left">증권 구분</div>
											</label>
											
											<template v-if="request.pjStockType == 1">
											<div class="col-xs-4">
												<order-select :options="securCodeOptions" v-model="request.pjSecurCode" :disabled="pjStatus != 0"></order-select>
											</div>
											<div class="col-xs-4">
												<order-select :options="stockCodeOptions" v-model="request.pjStockCode" :disabled="pjStatus != 0"></order-select>
											</div>
											</template>

											<template v-else>
											<div class="col-xs-4">
												<bond-select :options="securBondOptions" v-model="request.pjSecurBondCode" :disabled="pjStatus != 0"></bond-select>
											</div>
											<div class="col-xs-4">
												<input type="text" class="form-control" v-model="request.pjBondType" placeholder="채권종류를 입력하세요." :disabled="pjStatus != 0"/>
											</div>
											</template>
											
										</div>
										
										<hr class="big_m2" />

										<div class="form-group row-mobile-n">
											<label for="stock_ta" class="col-xs-12 control-label control-label-big">
												<div class="text-left">목표금액</div>
											</label>
											<div class="col-sm-7">
												<vue-numeric class="form-control" separator="," currency="" v-model="request.pjTgAmount" :disabled="pjStatus != 0"></vue-numeric>
											</div>
											<div class="col-sm-1">
												<span class="textarea_text_leng webfont2 mt10">원</span>
											</div>
										</div>

										<div class="form-group row-mobile-n">
											<label for="stock_project_start" class="col-xs-12 control-label control-label-big">
												<div class="text-left">프로젝트 기간</div>
											</label>
											<div class="col-xs-12 col-sm-7">
												<div class="row row-mobile-n">
													<div class="col-xs-5">
														<date-picker v-model="request.pjStartDate" :date="request.pjStartDate" v-if="dataConfirm" :disabled="pjStatus != 0"></date-picker>
													</div>
													<div class="col-xs-1 text-center form-mline webfont2"></div>
													<div class="col-xs-5">
														<date-picker v-model="request.pjEndDate" :date="request.pjEndDate" v-if="dataConfirm" :disabled="pjStatus != 0"></date-picker>
													</div>
												</div>
											</div>
										</div>
										
										<div class="form-group row-mobile-n">
											<label for="bond_apr" class="col-xs-12 control-label control-label-big">
												<div class="text-left">청약배정순위</div>
											</label>
											<div class="col-sm-4">
												<order-select :options="assignOptions" v-model="request.pjAssignPl" :disabled="pjStatus != 0"></order-select>
											</div>
										</div>
										
										<hr class="big_m2" />

										<div class="form-group row-mobile-n">
											<label for="bond_pd" class="col-xs-5 control-label control-label-big">
												<div class="text-left">배정확정/환불일</div>
											</label>
											<label for="bond_rd" class="col-xs-5 control-label control-label-big">
												<div class="text-left">납입일</div>
											</label>
											<div class="col-sm-4">
												<date-picker v-model="request.pjFixedDate" :date="request.pjFixedDate" v-if="dataConfirm" :disabled="pjStatus != 0"></date-picker>
											</div>
											<div class="col-sm-1"></div>
											<div class="col-sm-4">
												<date-picker v-model="request.pjPmtDate" :date="request.pjPmtDate" v-if="dataConfirm" :disabled="pjStatus != 0"></date-picker>
											</div>
										</div>

										<div class="form-group row-mobile-n">
											<label for="bond_rd" class="col-xs-5 control-label control-label-big">
												<div class="text-left">발행예정일</div>
											</label>
											<label for="bond_pd" class="col-xs-5 control-label control-label-big">
												<div class="text-left">증권입고일</div>
											</label>
											<div class="col-sm-4">
												<date-picker v-model="request.pjPublishDate" :date="request.pjPublishDate" v-if="dataConfirm" :disabled="pjStatus != 0"></date-picker>
											</div>
											<div class="col-sm-1"></div>
											<div class="col-sm-4">
												<date-picker v-model="request.pjWearingDate" :date="request.pjWearingDate" v-if="dataConfirm" :disabled="pjStatus != 0"></date-picker>
											</div>
										</div>

										<div class="form-group row-mobile-n" v-if="request.pjStockType == 2">
											<label for="bond_rd" class="col-xs-12 control-label control-label-big">
												<div class="text-left">채권만기일</div>
											</label>
											<div class="col-sm-4">
												<date-picker v-model="request.pjBondDueDate" :date="request.pjBondDueDate" v-if="dataConfirm" :disabled="pjStatus != 0"></date-picker>
											</div>
										</div>
										
										<hr class="big_m2" />

										<template v-if="request.pjStockType == 1">
											<div class="form-group row-mobile-n">
												<label for="bond_cs" class="col-xs-5 control-label control-label-big">
													<div class="text-left">현재 기업가치</div>
												</label>
												<label for="bond_pd" class="col-xs-5 control-label control-label-big">
													<div class="text-left" v-text="pubStockName"></div>
												</label>

												<div class="col-sm-4">
													<vue-numeric class="form-control" separator="," currency="" v-model="request.pjCurrValue" placeholder="기업가치를 입력하세요." :disabled="pjStatus != 0"></vue-numeric>
												</div>
												<div class="col-sm-1">
													<span class="textarea_text_leng webfont2 mt10">원</span>
												</div>
												<div class="col-sm-4">
													<vue-numeric class="form-control" separator="," currency="" v-model="request.pjPublishCnt" :placeholder="pubStockName + '를 입력하세요.'" :disabled="pjStatus != 0"></vue-numeric>
												</div>
												<div class="col-sm-1">
													<span class="textarea_text_leng webfont2 mt10">주</span>
												</div>
											</div>

											<div class="form-group row-mobile-n">
												<label for="stock_cs" class="col-xs-5 control-label control-label-big">
													<div class="text-left">현재 주식수</div>
												</label>
												<label for="stock_aes" class="col-xs-5 control-label control-label-big">
													<div class="text-left">발행 후 배정지분율</div>
												</label>
												
												<div class="col-sm-4">
													<vue-numeric class="form-control" separator="," currency="" v-model="request.pjCurrStock" placeholder="현재 주식수를 입력하세요." :disabled="pjStatus != 0"></vue-numeric>
												</div>
												<div class="col-sm-1">
													<span class="textarea_text_leng webfont2 mt10">주</span>
												</div>
												<div class="col-sm-4">
													<vue-numeric class="form-control" separator="," currency="" :precision="6" v-model="request.pjAtAssign" placeholder="발행 후 배정지분율를 입력하세요." :disabled="pjStatus != 0"></vue-numeric>
												</div>
												<div class="col-sm-3">
													<span class="textarea_text_leng webfont2 mt10">%</span>
												</div>
											</div>
										</template>

										<div class="form-group row-mobile-n">
												<label for="stock_ms" class="col-xs-5 control-label control-label-big">
													<div class="text-left">투자가능 최대{{stockType}}수</div>
												</label>
												<label for="stock_ms" class="col-xs-5 control-label control-label-big">
													<div class="text-left">투자가능 최소{{stockType}}수</div>
												</label>
												
												<div class="col-sm-4">
													<vue-numeric class="form-control" separator="," currency="" v-model.trim="request.pjInvestMaxStock" :placeholder="'투자가능 최대' + stockType + '수'" :disabled="pjStatus != 0"></vue-numeric>
												</div>
												<div class="col-sm-1">
													<span class="textarea_text_leng webfont2 mt10">{{stockTypeText}}</span>
												</div>

												<div class="col-sm-4">
													<vue-numeric class="form-control" separator="," currency="" v-model.trim="request.pjInvestMinStock" :placeholder="'투자가능 최소' + stockType + '수'" :disabled="pjStatus != 0"></vue-numeric>
												</div>
												<div class="col-sm-1">
													<span class="textarea_text_leng webfont2 mt10">{{stockTypeText}}</span>
												</div>
											</div>


										<template v-if="request.pjStockType == 2">
											<div class="form-group row-mobile-n">
												<label for="bond_cs" class="col-xs-5 control-label control-label-big">
													<div class="text-left">연이자율</div>
												</label>
												<label for="bond_pd" class="col-xs-5 control-label control-label-big">
													<div class="text-left" v-text="pubStockName"></div>
												</label>
												<div class="col-sm-4">
													<vue-numeric class="form-control" separator="," currency="" :precision="6"  v-model="request.pjPaymentRate" placeholder="연이자율를 입력하세요." :disabled="pjStatus != 0"></vue-numeric>
												</div>
												<div class="col-sm-1">
													<span class="textarea_text_leng webfont2 mt10">%</span>
												</div>
												<div class="col-sm-4">
													<vue-numeric class="form-control" separator="," currency="" v-model="request.pjPublishCnt" :placeholder="pubStockName + '를 입력하세요.'" :disabled="pjStatus != 0"></vue-numeric>
												</div>
												<div class="col-sm-1">
													<span class="textarea_text_leng webfont2 mt10">구좌</span>
												</div>
											</div>

											<div class="form-group row-mobile-n">
												<label for="bond_aes" class="col-xs-12 control-label control-label-big">
													<div class="text-left">이자지급 주기</div>
												</label>
												<div class="col-sm-4">
													<input type="text" class="form-control" v-model="request.pjPaymentCycle" :disabled="pjStatus != 0">
												</div>
											</div>

										</template>

										<hr class="big_m2" />
										
										<div class="form-group row-mobile-n">
											<label for="bond_isin" class="col-xs-5 control-label control-label-big">
												<div class="text-left">종목코드(ISIN)</div>
											</label>
											<label for="bond_ppw" class="col-xs-5 control-label control-label-big">
												<div class="text-left" v-text="contStockName"></div>
											</label>
											<div class="col-sm-4">
												<input type="text" class="form-control" v-model="request.pjIsin" placeholder="종목코드를 입력하세요." :disabled="pjStatus != 0"/>
											</div>
											<div class="col-sm-1"></div>
											<div class="col-sm-4">
												<vue-numeric class="form-control" separator="," currency="" v-model="request.pjContStock" :placeholder="contStockName + '을 입력하세요.'" :disabled="pjStatus != 0"></vue-numeric>
											</div>
											<div class="col-sm-3">
												<span class="textarea_text_leng webfont2 mt10">원</span>
											</div>
										</div>
										
										<div class="form-group row-mobile-n">
											<label for="stock_isin" class="col-xs-12 control-label control-label-big">
												<div class="text-left" v-text="faceName"></div>
											</label>
											<div class="col-xs-4">
												<vue-numeric class="form-control" separator="," currency="" v-model="request.pjFaceValue" :placeholder="faceName + '를 입력하세요.'" :disabled="pjStatus != 0"></vue-numeric>
											</div>
											<div class="col-xs-1">
												<span class="textarea_text_leng webfont2 mt10">원</span>
											</div>
										</div>
											
										<hr class="big_m2" />

										<div class="form-group row-mobile-n">
											<label for="bond_uniq" class="col-xs-3 control-label control-label-big">
												<div class="text-left">발행증권의 특이사항</div>
											</label>
											<div class="col-xs-9">
												<textarea rows="5" cols="50" class="form-control mt10 md-mt0 m-mt0 xs-mt0" v-model="request.pjSignReport" :disabled="pjStatus != 0"></textarea>
											</div>
										</div>
										
										<div class="form-group row-mobile-n mt40">
					                        <div class="col-sm-11">
					                            <div class="form-group row-mobile-n">
					                                <div class="col-xs-6 col-sm-4 col-md-3">
					                                    <a class="btn btn-block btn-primary-outline" v-on:click="save">저장</a>
					                                </div>
					                                <div class="col-xs-6 col-sm-4 col-md-3">
					                                    <a class="btn btn-block btn-primary" v-on:click="save(true)">저장 후  다음단계</a>
					                                </div>
					                            </div>
					                        </div>
					                    </div>

									</form>
								</div>

							</div>
						</div>
					</div>
				</div>
			`,
			props : ['pjCode', 'pjStatus'],
			data : function() {
				return {
					dataConfirm : false,
					request : {
						pjType : "1",
						pjStockType : "1",
						pjDeductionConfirm : "N",
						pjSecurBondCode : "SEC002",
						pjSecurCode : "SEC001",
						pjStockCode : "STC001",
						pjBondType : "",
						pjTgAmount : 0,
						pjStartDate : '',
						pjEndDate : '',
						pjAssignPl : "ASS001",
						pjFixedDate : '',
						pjPmtDate : '',
						pjPublishDate : '',
						pjWearingDate : "",
						pjBondDueDate : "",
						pjCurrValue : 0,
						pjPublishCnt : 0,
						pjCurrStock : 0,
						pjAtAssign : 0,
						pjInvestMinStock : 0,
						pjInvestMaxStock : 0,
						pjInvestMaxAmt : 0,
						pjPaymentRate : 0,
						pjPaymentCycle : "",
						pjIsin : "",
						pjContStock : 0,
						pjFaceValue : 0,
						pjSignReport : ""
					},
					securCodeOptions : [
						{
							id : "SEC001",
							text : "지분증권-주식형"
						},
					],
					securBondOptions : [
						{
							id : "SEC002",
							text : "채무증권-채권형"
						},
						{
							id : "SEC003",
							text : "채무증권-CB(전환사채)형"
						},
						{
							id : "SEC004",
							text : "채무증권-BW(신주인수권부사채)형"
						},
						{
							id : "SEC006",
							text : "채무증권-등록발행"
						},
						{
							id : "SEC007",
							text : "채무증권-비등록발행"
						},
					],
					stockCodeOptions : [
						{
							id : "STC001",
							text : "보통주"
						},
						{
							id : "STC002",
							text : "우선주"
						}
					],
					assignOptions : [
						{
							id : "ASS001",
							text : "선착순"
						},
						{
							id : "ASS002",
							text : "금액순"
						},
						{
							id : "ASS003",
							text : "기타배정"
						}
					]
				}
			},
			computed : {
				pubStockName : function() {
					return this.request.pjStockType == 1 ? "발행예정 주식수" : "발행예정 구좌수"
				},
				contStockName : function() {
					return this.request.pjStockType == 1 ? "주당 가격" : "구좌당 가격"
				},
				faceName : function() {
					return this.request.pjStockType == 1 ? "액면가" : "권면가"
				},
				stockTypeText : function() {
					return this.request.pjStockType == 1 ? '주' : '구좌'	
				},
				stockType : function() {
					return this.request.pjStockType == 1 ? '주식' : '구좌'	
				}
			},
			components : {
				orderSelect : require('../../common/select.js').default.component(),
				bondSelect : require('../../common/select.js').default.component(),
				vueNumeric : VueNumeric.default,
				datePicker : require('../../common/date-picker.js').default.component(),
			},
			created : function() {
				this.load();
			},
			methods : {
				load : function() {
					var self = this;
					axios.post('/data/save/invest/info/securities-issue', {pjCode : this.pjCode})
	                    .then(function(response){

	                    	var result = response.data.rData;
                			var data = self.request;

                			var pjStockType = result.pjStockType;
                			data.pjType = result.pjType
                			data.pjStockType = pjStockType;

                			if(pjStockType == 2) {
                				data.pjSecurBondCode = result.pjSecurCode;
                			} else {
                				data.pjSecurCode = result.pjSecurCode;
                				data.pjStockCode = result.pjStockCode
                			}

                			data.pjDeductionConfirm = result.pjDeductionConfirm;
                			data.pjBondType = result.pjBondType;
                			data.pjTgAmount = result.pjTgAmount
							data.pjStartDate = result.pjStartDate
							data.pjEndDate = result.pjEndDate
							data.pjAssignPl = result.pjAssignPl
							data.pjFixedDate = result.pjFixedDate
							data.pjPmtDate = result.pjPmtDate
							data.pjPublishDate = result.pjPublishDate
							data.pjWearingDate = result.pjWearingDate
							data.pjBondDueDate = result.pjBondDueDate
							data.pjCurrValue = result.pjCurrValue
							data.pjPublishCnt = result.pjPublishCnt
							data.pjCurrStock = result.pjCurrStock
							data.pjAtAssign = result.pjAtAssign
							data.pjInvestMinStock = result.pjInvestMinStock

							data.pjInvestMaxAmt = result.pjInvestMaxAmt
							data.pjInvestMaxStock = result.pjInvestMaxAmt / result.pjContStock

							data.pjPaymentRate = result.pjPaymentRate
							data.pjPaymentCycle = result.pjPaymentCycle
							data.pjIsin = result.pjIsin
							data.pjContStock = result.pjContStock
							data.pjFaceValue = result.pjFaceValue
							data.pjSignReport = result.pjSignReport

                			
                			self.dataConfirm = !self.dataConfirm
                			self.$nextTick(function() {
								$( ".option_group .radio-inline input" ).checkboxradio();
								$( ".stock_pu_type, .stock_income_type, .bond_pu_type, .bond_income_type" ).controlgroup();

								$( ".fp-items-method-tab .radio-inline input" ).checkboxradio();
								$( ".fp-items-method-tab" ).controlgroup();
						    });
	                    })
				},
				save : function(next) {
					if(this.pjStatus != 0) {
						noti.open('승인대기 중 이거나 진행중인 프로젝트는 수정이 불가능합니다.')
						return;
					}

					var self = this;
					$('.page-loader-more').fadeIn('')
					this.request.pjCode = this.pjCode;

					var pjStockType = this.request.pjStockType;

                	if(pjStockType == 1) {
                		this.request.pjBondType = "";
                		this.request.pjBondDueDate = ""
                		this.request.pjInvestMaxAmt = this.request.pjInvestMaxStock * this.request.pjContStock;
                	} else {
                		this.request.pjSecurCode = this.request.pjSecurBondCode;
                		this.request.pjStockCode = "";
                		this.request.pjInvestMaxAmt = 0;
                	}

					axios.post('/set/save/invest/securities-issue', this.request)
						.then(function(response) {
							$('.page-loader-more').fadeOut('');
							var result = response.data;
							if(result.rCode == "0000") {
								
		                    	if(next == true) {
		                    		self.$emit('step-change', 4, true);
		                    	} else {
		                    		noti.open("저장되었습니다.")
		                    		self.$emit('step-change', 4, false);
		                    	}
	                    	} else {
	                    		noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
	                    	}
						})
				}		
			}

		}
	}
}

export default new SecuritiesIssue()