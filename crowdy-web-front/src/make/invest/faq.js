class InvestFaq {
	component() {
		return {
			template : `
				<div>
					<div class="col-md-10 col-line">
						<div class="row not-space">
							<div class="col-lg-1"></div>
							<div class="col-lg-11">
								<div class="md-ml30">
									<form class="form-horizontal">
										<p class="form-control-static form-control-static-grey2 mt10 mb70">
											투자자들이 자주 문의할 것으로 예상되는 질문과 대답을 등록해 주세요!
										</p>

										<div class="form-group row-mobile-n">
											<label for="faq_subject" class="col-xs-1 control-label">
												<div class="text-left">질문</div>
											</label>
											<div class="col-xs-11">
												<input type="text" class="form-control" v-model="pjFaq.pjFaqTitle" :class="{'error' : errors.has('pjFaqTitle')}" name="pjFaqTitle" v-validate="'required'" :disabled="pjStatus == 1"/>
												<label class="error" v-if="errors.has('pjFaqTitle')" v-text="errors.first('pjFaqTitle')"></label>
											</div>
										</div>

										<div class="form-group row-mobile-n">
											<label for="faq_memo" class="col-xs-1 control-label">
												<div class="text-left">답변</div>
											</label>
											<div class="col-xs-11">
												<textarea rows="3" cols="25" class="textarea-form-control form-control" v-model="pjFaq.pjFaqContent" :class="{'error' : errors.has('pjFaqContent')}" name="pjFaqContent" v-validate="'required'" :disabled="pjStatus == 1"></textarea>
												<label class="error" v-if="errors.has('pjFaqContent')" v-text="errors.first('pjFaqContent')"></label>
											</div>
										</div>

										<div class="form-group row-mobile-n">
											<div class="col-sm-12 text-right">
												<button type="button" class="btn btn-primary-outline" v-on:click="faqSave" :disabled="pjStatus == 1">등록하기</button>
											</div>
										</div>

										<hr class="big_m" />

										<p class="form-control-static form-control-static-grey2 mb30">
											<strong>등록된 질문들</strong>
										</p>

										<div class="faq_sip_group">

											<div class="faq_sip_loop" v-for="(item, index) in faqList">
												<h5 v-text="item.pjFaqTitle"></h5>
												<div class="faq_sip_memo" v-html="convert(item.pjFaqContent)">
												</div>
												<div class="faq_sip_btn text-right">
													<a href="javascript:void(0);" class="btn_none_icon btn_modify red-800" v-on:click="updateOpen(item, index)">수정하기</a>
													<a href="javascript:void(0);" class="btn_none_icon btn_delete btn_delete_grey grey-500 ml20" v-on:click="removeConfirm(item.pjFaqIdx)">삭제하기</a>
												</div>
											</div>

										</div>
										
										<div class="form-group row-mobile-n mt40">
					                        <div class="col-sm-11">
					                            <div class="form-group row-mobile-n">
					                                <div class="col-xs-6 col-sm-4 col-md-3">
					                                    <a class="btn btn-block btn-primary-outline" v-on:click="save">저장</a>
					                                </div>
					                                <div class="col-xs-6 col-sm-4 col-md-3">
					                                    <a class="btn btn-block btn-primary" v-on:click="save(true)">저장 후  다음단계</a>
					                                </div>
					                            </div>
					                        </div>
					                    </div>

									</form>
								</div>
							</div>
						</div>
					</div>

					<!-- 수정하기 -->
					<div id="faqmModal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
						<div class="modal-dialog" role="document">
							<form class="form-horizontal modal-content">
								<div class="modal-header modal-header-black">
									<button type="button" class="close" v-on:click="updateClose" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<div class="modal-title">수정하기</div>
								</div>

								<div class="modal-body">
									<div class="form-group row-mobile-n mt30 xs-mt10">
										<label for="faq_subject_pop" class="col-xs-12 control-label">
											<div class="text-left">질문</div>
										</label>
										<div class="col-xs-12">
											<input type="text" class="form-control" v-model="updateFaq.pjFaqTitle" />
										</div>
									</div>

									<div class="form-group row-mobile-n">
										<label for="faq_memo_pop" class="col-xs-12 control-label">
											<div class="text-left">답변</div>
										</label>
										<div class="col-xs-12">
											<textarea rows="8" cols="40" class="form-control" v-model="updateFaq.pjFaqContent"></textarea>
										</div>
									</div>

									<div class="modal-footer text-center">
										<div class="row row-mobile-n">
											<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3">
												<div class="row row-mobile-n">
													<div class="col-xs-6">
														<a class="btn btn-block btn-default-outline" v-on:click="updateClose">취소</a>
													</div>
													<div class="col-xs-6">
														<a class="btn btn-block btn-primary" v-on:click="update">수정</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- //수정하기 -->

					<!-- FAQ 삭제 -->
	                <div id="removeModal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
	                    <div class="modal-dialog modal-sm" role="document">
	                        <div class="modal-content">
	                            <div class="modal-header">
	                                <a class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
	                            </div>
	                            <div class="modal-body modal-order">
	                                <div class="mt15 red-800">정말 삭제하시겠습니까?</div>
	                                <div class="modal-footer text-center">
	                                    <div class="row not-space">
	                                        <div class="col-xs-4 col-xs-offset-1">
	                                            <a class="btn btn-block btn-danger-outline" v-on:click="remove">삭제</a>
	                                        </div>
	                                        <div class="col-xs-4 col-xs-offset-2">
	                                            <a class="btn btn-block btn-primary-outline" data-dismiss="modal">취소</a>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <!-- //파일삭제확인 -->
				</div>
			`,
			props : ['pjCode', 'pjStatus'],
			data : function() {
				return {
					dataConfirm : false,
					faqList : [],
					pjFaq : {
						pjFaqTitle : '',
						pjFaqContent : ''
					},
					updateFaq : {
						pjFaqIdx : '',
						pjFaqTitle : '',
						pjFaqContent : '',
						index : '',
					},
					deleteCode : ""
				}
			},
			created : function() {
				this.load();
			},
			methods : {
				load : function() {
					var self = this;
					axios.post('/data/save/invest/info/faq', {pjCode : this.pjCode})
	                    .then(function(response){
                			self.faqList = _.concat(self.faqList, response.data.rData);
	                    })
				},
				convert : function(data) {
					return data.replace( /[\n]/g, "<br/>")
				},
				faqSave : function() {
					if(this.pjStatus == 1) return;

					var self = this;
					this.$validator.validateAll()
						.then(function(sucess) {
							if(!sucess) return;

							$('.page-loader-more').fadeIn('')
							self.pjFaq.pjCode = self.pjCode;
							self.pjFaq.memCode = userInfo.memCode;
							axios.post('/set/save/invest/faq', self.pjFaq)
								.then(function(response) {
									$('.page-loader-more').fadeOut('');
									var result = response.data;
									if(result.rCode == "0000") {
										self.pjFaq.pjFaqIdx = result.rData.pjFaqIdx
										self.faqList.push(_.clone(self.pjFaq));

										self.pjFaq.pjFaqTitle = '';
										self.pjFaq.pjFaqContent = '';

			                    		noti.open("저장되었습니다.")
			                    		self.$emit('step-change', 7, false);
			                    	} else {
			                    		noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
			                    	}
								})
						})
				},
				update : function() {
					var self = this;
					$('.page-loader-more').fadeIn('')

					this.updateFaq.pjCode = this.pjCode;
					this.updateFaq.memCode = userInfo.memCode;

					axios.post('/set/save/invest/faq', this.updateFaq)
						.then(function(response) {
							$('.page-loader-more').fadeOut('');
							var result = response.data;
							if(result.rCode == "0000") {
								var data = self.faqList[self.updateFaq.index];

								data.pjFaqTitle = self.updateFaq.pjFaqTitle
								data.pjFaqContent = self.updateFaq.pjFaqContent

	                    		self.updateClose();
	                    	} else {
	                    		noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
	                    	}
						})
				},
				updateOpen : function(data, index) {
					if(this.pjStatus == 1) {
						noti.open('승인대기 중 인 프로젝트는 수정이 불가능합니다.')
						return;
					}

					this.updateFaq.pjFaqIdx = data.pjFaqIdx
					this.updateFaq.pjFaqTitle = data.pjFaqTitle
					this.updateFaq.pjFaqContent = data.pjFaqContent
					this.updateFaq.index = index
					

					$('#faqmModal').modal('show')
				},
				updateClose : function() {

					this.updateFaq.index = ""
					this.updateFaq.pjFaqIdx = ""
					this.updateFaq.pjFaqTitle = ""
					this.updateFaq.pjFaqContent = ""
					$('#faqmModal').modal('hide');
				},
				removeConfirm : function(code) {
					if(this.pjStatus == 1) {
						noti.open('승인대기 중 인 프로젝트는 삭제가 불가능합니다.')
						return;
					}

					this.deleteCode = code;
					$('#removeModal').modal('show');
				},
				remove : function(index) {
					var self = this;
					$('#removeModal').modal('hide');
					$('.page-loader-more').fadeIn('')
					axios.post('/set/save/invest/faq/delete', {pjCode : this.pjCode, pjFaqIdx :  this.deleteCode})
						.then(function(response) {
							$('.page-loader-more').fadeOut('');
							var result = response.data;

							if(result.rCode == "0000") {

								self.faqList = _.filter(self.faqList, function(item) {
										return item.pjFaqIdx != index
								});
		                    	
	                    		noti.open("삭제되었습니다.")
	                    	} else {
	                    		noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
	                    	}
						})
				},
				save : function(next) {
					var self = this;
					
					if(next == true) {
						self.$emit('step-change', 7, true);
					}
					
				}
			}

		}
	}
}

export default new InvestFaq()