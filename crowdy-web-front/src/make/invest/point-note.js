class PointNote {
	component() {
		return {
			template : `
				<div class="col-md-10 col-line">
					<div class="row not-space">
						<div class="col-lg-1"></div>
						<div class="col-lg-11">
							<div class="md-ml30 mt10">
								<div class="mb15">
									선택 후 작성된 항목들만 노출됩니다. 신중하게 선택 후 작성해 주세요.
								</div>
								<form class="form-horizontal">
									<div class="panel-group faq invest">

										<div class="panel panel-default">
											<div class="panel-heading">
												<div class="panel-title" :class="{'checked' : checkInput.summary}">
													<label for="core-inve1" class="checkbox-inline" data-toggle="collapse" data-target="#collapse1001" aria-expanded="false" aria-controls="collapse1001">
														<input type="checkbox" id="core-inve1" v-model="checkInput.summary" :disabled="pjStatus == 1"/><span class="label"></span><span class="label-text">회사 3줄 요약</span>
													</label>
												</div>
											</div>
											<div id="collapse1001" class="panel-collapse collapse" :class="{'in' : checkInput.summary}">
												<div class="panel-body panel-edit">
													<note-area :id="'editor1'" v-if="dataConfirm" :value="request.pjSummary" v-model="request.pjSummary" :disabled="pjStatus == 1"></note-area>
												</div>
											</div>
										</div>

										<div class="panel panel-default">
											<div class="panel-heading">
												<div class="panel-title" :class="{'checked' : checkInput.point}">
													<label for="core-inve2" class="checkbox-inline" data-toggle="collapse" data-target="#collapse1002" aria-expanded="false" aria-controls="collapse1002">
														<input type="checkbox" id="core-inve2" v-model="checkInput.point" :disabled="pjStatus == 1"/><span class="label"></span><span class="label-text">투자포인트</span>
													</label>
												</div>
											</div>
											<div id="collapse1002" class="panel-collapse collapse" :class="{'in' : checkInput.point}">
												<div class="panel-body panel-edit">
													<note-area :id="'editor2'" v-if="dataConfirm" :value="request.pjPoint" v-model="request.pjPoint" :disabled="pjStatus == 1"></note-area>
												</div>
											</div>
										</div>

										<div class="panel panel-default">
											<div class="panel-heading">
												<div class="panel-title" :class="{'checked' : checkInput.problem}">
													<label for="core-inve3" class="checkbox-inline" data-toggle="collapse" data-target="#collapse1003" aria-expanded="false" aria-controls="collapse1003">
														<input type="checkbox" id="core-inve3" v-model="checkInput.problem" :disabled="pjStatus == 1"/><span class="label"></span><span class="label-text">문제점 / 해결방안</span>
													</label>
												</div>
											</div>
											<div id="collapse1003" class="panel-collapse collapse" :class="{'in' : checkInput.problem}">
												<div class="panel-body panel-edit">
													<note-area :id="'editor3'" v-if="dataConfirm" :value="request.pjProblem" v-model="request.pjProblem" :disabled="pjStatus == 1"></note-area>
												</div>
											</div>
										</div>
										
										<div class="panel panel-default">
											<div class="panel-heading">
												<div class="panel-title" :class="{'checked' : checkInput.bizModel}">
													<label for="core-inve4" class="checkbox-inline" data-toggle="collapse" data-target="#collapse1004" aria-expanded="false" aria-controls="collapse1004">
														<input type="checkbox" id="core-inve4" v-model="checkInput.bizModel" :disabled="pjStatus == 1"/><span class="label"></span><span class="label-text">사업모델</span>
													</label>
												</div>
											</div>
											<div id="collapse1004" class="panel-collapse collapse" :class="{'in' : checkInput.bizModel}">
												<div class="panel-body panel-edit">
													<note-area :id="'editor4'" v-if="dataConfirm" :value="request.pjBizModel" v-model="request.pjBizModel" :disabled="pjStatus == 1"></note-area>
												</div>
											</div>
										</div>
										
										<div class="panel panel-default">
											<div class="panel-heading">
												<div class="panel-title" :class="{'checked' : checkInput.marketAz}">
													<label for="core-inve5" class="checkbox-inline" data-toggle="collapse" data-target="#collapse1005" aria-expanded="false" aria-controls="collapse1005">
														<input type="checkbox" id="core-inve5" v-model="checkInput.marketAz" :disabled="pjStatus == 1" /><span class="label"></span><span class="label-text">시장분석</span>
													</label>
												</div>
											</div>
											<div id="collapse1005" class="panel-collapse collapse" :class="{'in' : checkInput.marketAz}">
												<div class="panel-body panel-edit">
													<note-area :id="'editor5'" v-if="dataConfirm" :value="request.pjMarketAz" v-model="request.pjMarketAz":disabled="pjStatus == 1" ></note-area>
												</div>
											</div>
										</div>

										<div class="panel panel-default">
											<div class="panel-heading">
												<div class="panel-title" :class="{'checked' : checkInput.cometition}">
													<label for="core-inve6" class="checkbox-inline" data-toggle="collapse" data-target="#collapse1006" aria-expanded="false" aria-controls="collapse1006">
														<input type="checkbox" id="core-inve6" v-model="checkInput.cometition" /><span class="label"></span><span class="label-text">경쟁</span>
													</label>
												</div>
											</div>
											<div id="collapse1006" class="panel-collapse collapse" :class="{'in' : checkInput.cometition}">
												<div class="panel-body panel-edit">
													<note-area :id="'editor6'" v-if="dataConfirm" :value="request.pjCometition" v-model="request.pjCometition"></note-area>
												</div>
											</div>
										</div>

									</div>

									<div class="form-group row-mobile-n mt40">
				                        <div class="col-sm-11">
				                            <div class="form-group row-mobile-n">
				                                <div class="col-xs-6 col-sm-4 col-md-3">
				                                    <a class="btn btn-block btn-primary-outline" v-on:click="save">저장</a>
				                                </div>
				                                <div class="col-xs-6 col-sm-4 col-md-3">
				                                    <a class="btn btn-block btn-primary" v-on:click="save(true)">저장 후  다음단계</a>
				                                </div>
				                            </div>
				                        </div>
				                    </div>

								</form>
							</div>
						</div>
					</div>
				</div>
			`,
			props : ['pjCode', 'pjStatus'],
			data : function() {
				return {
					dataConfirm : false,
					request : {
						pjSummary : '',
						pjPoint : '',
						pjProblem : '',
						pjBizModel : '',
						pjMarketAz : '',
						pjCometition : ''
					},
					checkInput : {
						summary : false,
						point : false,
						problem : false,
						bizModel : false,
						marketAz : false,
						cometition : false
					}
				}
			},
			computed : {
				summary : function() {
					return this.request.pjSummary != '' ? true : false;
				},
				point : function() {
					return this.request.pjPoint != '' ? true : false;
				},
				problem : function () {
					return this.request.pjProblem != '' ? true : false;
				},
				bizModel : function() {
					return this.request.pjBizModel != '' ? true : false;
				},
				marketAz : function() {
					return this.request.pjMarketAz != '' ? true : false;
				},
				cometition :  function() {
					return this.request.pjCometition != '' ? true : false;
				}
			},
			components : {
				noteArea : require('../../common/text-editor.js').default.component()
			},
			created : function() {
				this.load();
			},
			methods : {
				load : function() {
					var self = this;
					axios.post('/data/save/invest/info/point', {pjCode : this.pjCode})
	                    .then(function(response){
	                    	var result = response.data.rData;
	                    	var data = self.request;

	                    	data.pjSummary = result.pjSummary;
	                    	data.pjPoint = result.pjPoint;
	                    	data.pjProblem = result.pjProblem;
	                    	data.pjBizModel = result.pjBizModel;
	                    	data.pjMarketAz = result.pjMarketAz;
	                    	data.pjCometition = result.pjCometition

	                    	self.checkInput.summary = self.summary
							self.checkInput.point = self.point
							self.checkInput.problem = self.problem
							self.checkInput.bizModel = self.bizModel
							self.checkInput.marketAz = self.marketAz
							self.checkInput.cometition = self.cometition

	                    	self.dataConfirm = !self.dataConfirm
	                    })
				},
				save : function(next) {
					if(this.pjStatus == 1) {
						noti.open('승인대기 중 인 프로젝트는 수정이 불가능합니다.')
						return;
					}
					var self = this;
					this.request.pjCode = this.pjCode;
					$('.page-loader-more').fadeIn('')

					axios.post('/set/save/invest/point', this.request)
						.then(function(response) {
							$('.page-loader-more').fadeOut('');
							var result = response.data;
							if(result.rCode == "0000") {
								
		                    	if(next == true) {
		                    		self.$emit('step-change', 5, true);
		                    	} else {
		                    		noti.open("저장되었습니다.")
		                    		self.$emit('step-change', 5, false);
		                    	}
	                    	} else {
	                    		noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
	                    	}
						})
				}
			}

		}
	}
}

export default new PointNote()