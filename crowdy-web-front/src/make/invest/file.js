class InvestFile {
	component() {
		return {
			template : `
			<div>
				<div class="col-md-10 col-line">
					<div class="row not-space">
						<div class="col-lg-1"></div>
						<div class="col-lg-11">
							<div class="md-ml30">
								<form class="form-horizontal">
									<p class="form-control-static form-control-static-grey2 mt10 mb70">
										투자자에게 게시할 파일들을 등록해 주세요.
									</p>

									<div class="form-group row-mobile-n">
										<label for="sip_filename" class="col-xs-1 control-label">
											<div class="text-left">제목</div>
										</label>
										<div class="col-xs-11">
											<input type="text" class="form-control" v-model="request.fileTitle" :class="{'error' : errors.has('fileTitle')}" data-vv-name="fileTitle" v-validate="'required'" :disabled="pjStatus == 1"/>
											<label class="error" v-if="errors.has('fileTitle')" v-text="errors.first('fileTitle')"></label>
										</div>
									</div>
									
									<div class="form-group row-mobile-n">
										<label for="sip_file" class="col-xs-1 control-label">
											<div class="text-left">첨부</div>
										</label>
										<div class="col-xs-11">
											<div class="input-group input-group-file">
												<input type="text" class="form-control" :class="{'error' : errors.has('file')}" v-model="request.file.fileName" readonly="readonly" />
												<span class="input-group-btn">
													<span class="btn btn-outline btn-file">
														<i class="fa fa-upload" aria-hidden="true"></i>
														<file-input v-model="request.file" data-vv-name="file" v-validate="'required'" :disabled="pjStatus == 1" v-on:error="error"></file-input>
													</span>
												</span>
												<label class="error" v-if="errors.has('file')" v-text="errors.first('file')"></label>
											</div>
										</div>
									</div>
									
									<div class="form-group row-mobile-n">
										<div class="col-sm-12 text-right">
											<button type="button" class="btn btn-primary-outline" v-on:click="add" :disabled="pjStatus != 0">등록하기</button>
										</div>
									</div>
									
									<hr class="big_m" />
									
									<p class="form-control-static form-control-static-grey2 mb30">
										<strong>첨부된 파일들</strong>
									</p>
									
									<table class="table table-stext table-fixed">
										<colgroup>
											<col style="width:24%;">
											<col style="width:29%;" class="hidden-xs">
											<col style="width:16%;">
											<col style="width:15%;">
										</colgroup>
										<thead>
											<tr>
												<th><span>문서제목</span></th>
												<th class="hidden-xs"><span>첨부파일명</span></th>
												<th><span>용량</span></th>
												<th></th>
											</tr>
										</thead>
										<tbody>
											<tr class="text-center" v-for="(item, index) in files">
												<td v-text="item.fileInfo"></td>
												<td class="hidden-xs" v-text="item.fileName"></td>
												<td v-text="size(item.fileSize)"></td>
												<td><button type="button" class="btn btn-sm btn-link grey-500" v-on:click="removeConfirm(item.fileCode)" :disabled="pjStatus != 0"><i class="btn-link-delete"></i> 삭제하기</button></td>
											</tr>
										</tbody>

									</table>

									<div class="form-group row-mobile-n mt40">
				                        <div class="col-sm-11">
				                            <div class="form-group row-mobile-n">
				                                <div class="col-xs-6 col-sm-4 col-md-3">
				                                    <a class="btn btn-block btn-primary-outline" v-on:click="save">저장</a>
				                                </div>
				                                <div class="col-xs-6 col-sm-4 col-md-3">
				                                    <a class="btn btn-block btn-primary" v-on:click="save(true)">저장 후  다음단계</a>
				                                </div>
				                            </div>
				                        </div>
				                    </div>

								</form>
							</div>
						</div>
					</div>
				</div>

				<!-- 파일삭제확인 -->
                <div id="removeModal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <a class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                            </div>
                            <div class="modal-body modal-order">
                                <div class="mt15 red-800">정말 삭제하시겠습니까?</div>
                                <div class="modal-footer text-center">
                                    <div class="row not-space">
                                        <div class="col-xs-4 col-xs-offset-1">
                                            <a class="btn btn-block btn-danger-outline" v-on:click="remove">삭제</a>
                                        </div>
                                        <div class="col-xs-4 col-xs-offset-2">
                                            <a class="btn btn-block btn-primary-outline" data-dismiss="modal">취소</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //파일삭제확인 -->

			</div>
			`,
			props : ['pjCode', 'pjStatus'],
			data : function() {
				return {
					request : {
						fileTitle : "",
						fileCodeGroup : "",
						file : {
							fileData : "",
							fileName : "",
						}
					},
					files : [],
					file : {
						fileData : "",
						fileName : "",
					},
					deleteCode : ""
				}
			},
			components : {
				fileInput : require('../../common/file-input.js').default.component()
			},
			created : function() {
				this.load();
			},
			methods : {
				load : function() {
					var self = this;
					axios.post('/data/save/invest/info/file', {pjCode : this.pjCode})
	                    .then(function(response){
	                    	var result = response.data.rData;
    						var data = self.request;

    						data.fileCodeGroup = result.fileCodeGroup
    						self.files = result.files;

	                    })
				},
				add : function() {
					if(this.pjStatus != 0) return;
					var self = this;
					this.$validator.validateAll()
						.then(function(sucess) {
							if(!sucess) return;

							self.request.pjCode = self.pjCode;
							$('.page-loader-more').fadeIn('')
							axios.post('/set/invest/file', self.request)
			                    .then(function(response){
			                    	$('.page-loader-more').fadeOut('');
			                    	var result = response.data.rData;
		    						var data = self.request;

		    						self.request.fileCodeGroup = result.data;
		    						data.fileTitle = "";
		    						data.file = self.file;
		    						self.load();
		    						self.$emit('step-change', 8, false);
			                    })
						})
					
					

				},
				removeConfirm : function(code) {
					this.deleteCode = code;
					$('#removeModal').modal('show');
				},
				remove : function(code) {
					var self = this;
					axios.post('/file/delete/' + this.deleteCode)
	                    .then(function(response){
	                    	$('#removeModal').modal('hide');
	                    	self.load();
	                    	self.$emit('step-change', 8, false);
	                    })
				},
				size : function formatFileSize(size) {
					var units = ['Byte', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
					var threshold = 1024;
				  	const i = size === 0 ? 0 : Math.floor(Math.log(size) / Math.log(threshold));
				  	return ((size / Math.pow(threshold, i)).toFixed(2) * 1) + " " + units[i];
				},
				error : function(size) {
					noti.open('업로드되는 파일 사이즈는 10MB보다 작아야합니다.')
					this.request.fileTitle = "";
					this.request.file = this.file;
				},
				save : function(next) {
					var self = this;
					
					if(next == true) {
						self.$emit('step-change', 8, true);
					}
					
				}
			}
		}
	}
}

export default new InvestFile()