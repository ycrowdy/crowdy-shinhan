class StockPayment {
	component() {
		return {
			template : `
				<div class="col-md-10 col-line">
					<div class="row not-space">
						<div class="col-lg-1"></div>
						<div class="col-lg-11">
							<div class="md-ml30">
								<form class="form-horizontal">
									<p class="form-control-static form-control-static-grey2 mt10 mb70">
										주금 납입을 위해 주금을 받을 계좌 정보를 입력하여 주세요.
									</p>

									<div class="form-group row-mobile-n">
										<label for="project_vod" class="col-xs-12 control-label control-label-big">
											<div class="text-left mb10">계좌정보 </div>
										</label>
										<div class="col-xs-12">

											<div class="row row-mobile-n mb20">
												<label for="account_bank" class="col-xs-12 control-label">
													<div class="text-left mb10">증권발행대금 은행</div>
												</label>
												<div class="col-sm-3">
													<bank-select v-if="dataConfirm" :options="bankCodeOptions" v-model="request.irmiStockBank" :disabled="pjStatus != 0"></bank-select>
												</div>
											</div>
											
											<div class="row row-mobile-n mb20">
												<label for="account_num" class="col-xs-12 control-label">
													<div class="text-left mb10">증권발행대금 납부계좌</div>
												</label>
												<div class="col-sm-6">
													<input type="tel" v-if="dataConfirm" class="form-control" v-model="request.irmiStockAcc" :disabled="pjStatus != 0"/>
												</div>
											</div>
											
											<div class="row row-mobile-n mb20">
												<label for="account_num" class="col-xs-12 control-label">
													<div class="text-left mb10">증권발행대금 예금주명</div>
												</label>
												<div class="col-sm-6">
													<input type="text" v-if="dataConfirm" class="form-control" v-model="request.irmiStockAccNm" :disabled="pjStatus != 0"/>
												</div>
											</div>
											
											<div class="form-group row-mobile-n">
												<label for="project_vod" class="col-xs-12 control-label control-label-big">
													<div class="text-left mb10">세금계산서 정보</div>
												</label>
												<div class="col-xs-12">
													<div role="tabpanel" class="tab-pane active" id="tex_tab1">
														<div class="row row-mobile-n mb20">
															<label for="tex1_company_email" class="col-xs-12 control-label">
																<div class="text-left mb10">전자세금 계산서 이메일</div>
															</label>
															<div class="col-sm-6">
																<input type="email" v-if="dataConfirm" class="form-control" v-model="request.irmiTaxEmail" :disabled="pjStatus != 0"/>
															</div>
														</div>
													</div>
												</div>
											</div>

										</div>
									</div>

									<div class="form-group row-mobile-n mt40">
				                        <div class="col-sm-11">
				                            <div class="form-group row-mobile-n">
				                                <div class="col-xs-6 col-sm-4 col-md-3">
				                                    <a class="btn btn-block btn-primary-outline" v-on:click="save">저장</a>
				                                </div>
				                                <div class="col-xs-6 col-sm-4 col-md-3">
				                                    <a class="btn btn-block btn-primary" v-on:click="save(true)">저장 후  다음단계</a>
				                                </div>
				                            </div>
				                        </div>
				                    </div>
								</form>
							</div>
						</div>
					</div>
				</div>
			`,
			props : ['pjCode', 'pjStatus'],
			data : function() {
				return {
					bankCodeOptions : [],
					request : {
						irmiStockAcc : '',
					   	irmiStockAccNm : '',
					   	irmiStockBank : '',
					   	irmiTaxEmail : '',
					},
					confirm : {
						bank : false,
						stock : false
					}
				}
			},
			created : function() {
				this.load();
				this.bankCodeList();
			},
			computed : {
				dataConfirm : function() {
					return this.confirm.bank && this.confirm.stock; 
				}
			},
			components : {
				bankSelect : require('../../common/select.js').default.component(),
			},
			methods : {
				load : function() {
					var self = this;
					axios.post('/data/save/invest/info/stock-payment', {pjCode : this.pjCode})
	                    .then(function(response){
	                    	var result = response.data.rData;
	                    	self.request.irmiStockAcc = result.irmiStockAcc;
	                    	self.request.irmiStockAccNm = result.irmiStockAccNm;
	                    	var irmiStockBank = result.irmiStockBank;
	                    	if(irmiStockBank == '') {
	                    		irmiStockBank = '001';
	                    	}
	                    	self.request.irmiStockBank = irmiStockBank;
	                    	self.request.irmiTaxEmail = result.irmiTaxEmail;
	                    	self.confirm.stock = true;
	                     	
	                })
				},
				bankCodeList : function() {
					var self = this;
					axios.post('/data/crowdy/code/invest', {gcode : "IRMI_STOCK_BANK"})
	                    .then(function(response){
	                    	var result = response.data;
	                    	for (var i = 0; i < result.rData.length; i++) {
	                    		var option = {
	                    				id : result.rData[i].commonCode,
	                    				text : result.rData[i].commonInfo
                    			};
	                    		self.bankCodeOptions.push(option);
	                    	}
	                    	self.confirm.bank = true;
	                    	
	                })
				},
				save : function(next) {
					if(this.pjStatus != 0) {
						noti.open('승인대기 중 이거나 진행중인 프로젝트는 수정이 불가능합니다.')
						return;
					}

					var self = this;
					$('.page-loader-more').fadeIn('')
					this.request.pjCode = this.pjCode;
					axios.post('/set/save/invest/stock-payment', this.request)
						.then(function(response) {
							$('.page-loader-more').fadeOut('');
							var result = response.data;
							if(result.rCode == "0000") {
								if(next == true) {
		                    		self.$emit('step-change', 9, true);
		                    	} else {
		                    		noti.open("저장되었습니다.")
		                    		self.$emit('step-change', 9, false);
		                    	}
	                    		
	                    	
	                    	} else {
	                    		noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
	                    	}
						})
				}
			}
		}
	}
}

export default new StockPayment()