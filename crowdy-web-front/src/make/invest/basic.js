class InvestBasic {
	component() {
		return {
			template : `
				<div class="col-md-10 col-line">
				    <div class="row not-space">
				        <div class="col-lg-1"></div>
				        <div class="col-lg-11">
				            <div class="md-ml30 mt5 md-mt0">
				                <form class="form-horizontal">
				                    <div class="control-label control-label-big-lg webfont2">
				                        <div class="text-left mb15">시작하기 전에</div>
				                    </div>
				                    <p class="form-control-static mb40 md-mb40">
				                        프로젝트 진행을 쉽게 하기위해 ‘프로젝트 진행하기 가이드’를 준비하였습니다.
				                        <br /> 상세한 안내를 원하시면 가이드를 다운 받으세요.
				                    </p>
				                    <div class="form-group row-mobile-n">
				                    	<div class="col-md-6 mb40"><a href="https://image-se.ycrowdy.com/file/%E1%84%80%E1%85%A1%E1%84%8B%E1%85%B5%E1%84%83%E1%85%B3%E1%84%87%E1%85%AE%E1%86%A8_ver2018.pdf" target="_blank" class="btn btn-block btn-primary-outline">투자 가이드 다운받기</a></div> 
				                    </div>
				                    <div class="form-group row-mobile-n">
				                        <label for="project_subject" class="col-xs-12 control-label control-label-big">
				                            <div class="text-left">프로젝트 제목</div>
				                        </label>
				                        <div class="col-xs-12 col-sm-8">
				                            <input type="text" class="form-control" v-model.trim="request.pjTitle" maxlength="50" :disabled="pjStatus != 0" />
				                        </div>
				                        <div class="col-xs-12 col-sm-2">
				                            <span class="textarea_text_leng webfont2 mt10"><span v-text="request.pjTitle.length"></span> / 50</span>
				                        </div>
				                    </div>
				                    <div class="form-group row-mobile-n">
				                        <label for="project_summary" class="col-xs-12 control-label control-label-big">
				                            <div class="text-left">프로젝트 요약</div>
				                        </label>
				                        <div class="col-xs-12 col-sm-8">
				                        	<textarea rows="4" cols="50" class="form-control" v-model="request.pjSummary" :disabled="pjStatus == 1"></textarea>
				                        </div>
				                    </div>
				                    <div class="form-group row-mobile-n">
				                        <label for="project_weburl" class="col-xs-12 control-label control-label-big">
				                            <div class="text-left">프로젝트 웹주소 설정</div>
				                        </label>
				                        <div class="col-xs-4 col-sm-3">
				                            <p class="form-control-static">https://www.ycrowdy.com/i/</p>
				                        </div>
				                        <div class="col-xs-8 col-sm-3">
				                        	<url-input :url="url" v-model="url" class="form-control" :class="{'error' : !urlConfirm}" maxlength="30" placeholder="프로젝트 명" :disabled="pjStatus != 0"></url-input>
				                        </div>
				                        <div class="col-xs-12" v-if="!urlConfirm">
											<label style="margin-left:15px;" class="error" v-text="'이미 같은 주소가 있습니다. 새로운 주소를 입력해주세요'"></label>
			                        	</div>
				                    </div>
				                    <div class="form-group row-mobile-n">
				                        <div class="col-xs-12 col-sm-6">
				                            <div class="row row-mobile-n">
				                                <label for="project_img" class="col-xs-12 control-label control-label-big">
				                                    <div class="text-left">프로젝트 이미지</div>
				                                </label>
				                                <div class="col-xs-12 dropify-wrapper-340">
				                                    <dropify-input v-if="dataConfirm" v-model="request.pjCardFile" default-message="최적 사이즈 740 X 492" :default-img="request.pjCardImg"></dropify-input>
				                                </div>
				                            </div>
				                        </div>
				                        <div class="col-xs-12 col-sm-6">
				                            <div class="row row-mobile-n">
				                                <label for="company_ci" class="col-xs-12 control-label control-label-big">
				                                    <div class="text-left">회사 로고(CI)</div>
				                                </label>
				                                <div class="col-xs-12 dropify-wrapper-350">
				                                    <dropify-input v-if="dataConfirm" v-model="request.pjCiFile" default-message="최적 사이즈 350 X 350" :default-img="request.companyCiImg"></dropify-input>
				                                </div>
				                            </div>
				                        </div>
				                    </div>
				                    <!-- <div class="form-group row-mobile-n">
				                        <label for="project_top_img" class="col-xs-12 control-label control-label-big">
				                            <div class="text-left">프로젝트 상단배경 이미지
				                                <p class="red-800">(선택사항)</p>
				                            </div>
				                        </label>
				                        <div class="col-xs-12 col-md-11 dropify-wrapper-1920-400">
				                            <div>
				                                <dropify-input v-if="dataConfirm" v-model="request.pjBannerfile" default-message="최적 사이즈 1920 X 400" :default-img="request.pjBannerImg"></dropify-input>
				                            </div>
				                            <p class="form-control-static">
				                                이미지를 선택하지 않으시면, 기본 이미지가 제공됩니다.
				                            </p>
				                        </div>
				                    </div> -->
				                    <div class="form-group row-mobile-n">
				                        <label for="project_vod" class="col-xs-12 control-label control-label-big">
				                            <div class="text-left">메인 동영상 또는 이미지</div>
				                        </label>
				                        <div class="col-xs-12">
				                            <p class="form-control-static mt0 mb25">프로젝트 페이지의 최상단에 노출될 동영상 또는 이미지 입니다. 영상과 이미지를 함께 등록할 경우 영상이 먼저 보여집니다.</p>
				                            <div class="row row-mobile-n">
				                                <label for="vod_gallery" class="col-xs-12 control-label">
				                                    <div class="text-left">동영상의 주소를 적어주세요</div>
				                                </label>
				                                <div class="vod-holder input-append" v-for="(item, index) in request.pjVideos" :class="{'mt10' : index != 0}">
				                                    <div class="row row-mobile-n">
				                                        <div class="col-xs-10 col-sm-7">
				                                            <input type="text" class="form-control" v-model.trim="request.pjVideos[index]" placeholder="Youtube 또는 Vimeo에 등록된 영상의 URL을 입력해주세요." />
				                                        </div>
				                                        <div class="col-xs-2 col-sm-1">
				                                            <span class="files-add vod-add" v-on:click="videoAdd" v-if="index == 0">+</span>
				                                            <span class="files-add img-delete vod-delete" v-on:click="videoDelete" v-else>-</span>
				                                        </div>
				                                    </div>
				                                </div>
				                            </div>
				                        </div>
				                        <div class="col-xs-12 mt25">
				                            <label for="eligible[]" class="control-label">
				                                <div class="text-left">이미지를 등록해주세요</div>
				                            </label>
				                            <div class="row">
				                                <div class="eligible-holder input-append dropify-wrapper-197 col-xs-6 col-sm-3 mt10" v-for="(item, index) in request.pjImage" v-if="request.pjImage[index].state">
				                                    <dropify-input class="dropify-gallery" :default-index="index" v-model="request.pjImage[index]" v-on:clear="imageDelete" :default-img="item.fileName"></dropify-input>
				                                </div>
				                                <div class="col-xs-6 col-sm-3 dropify-wrapper-197 mt10">
				                                    <div class="dropify-wrapper dropify-wrapper-plus" v-on:click="imageAdd">+</div>
				                                </div>
				                            </div>
				                        </div>
				                    </div>
				                    <div class="form-group row-mobile-n mt40">
				                        <div class="col-sm-11">
				                            <div class="form-group row-mobile-n">
				                                <div class="col-xs-6 col-sm-4 col-md-3">
				                                    <a class="btn btn-block btn-primary-outline" v-on:click="save">저장</a>
				                                </div>
				                                <div class="col-xs-6 col-sm-4 col-md-3">
				                                    <a class="btn btn-block btn-primary" v-on:click="save(true)">저장 후  다음단계</a>
				                                </div>
				                            </div>
				                        </div>
				                    </div>
				                </form>
				            </div>
				        </div>
				    </div>
				</div>
			`,
			props : ['pjCode', 'pjStatus'],
			data : function() {
				return {
					file : {
						fileData : "",
						fileName : "",
						state : true
					},
					dataConfirm : false,
					request : {
						pjTitle : '',
						pjAliasUrl : '',
						pjSummary : '',
						pjCardFile : {},
						pjCiFile : {},
						pjBannerfile : {},
						pjCardImg : "",
						companyCiImg : "",
						pjBannerImg : "",
						pjVideos : [],
						pjImage : []
					},
					urlConfirm : true
				}
			},
			components : {
				dropifyInput : require('../../common/dropify-input.js').default.component(),
				urlInput : require('../../common/url-input.js').default.component()
			},
			created : function() {
				this.load();
			},
			computed : {
				url : {
					get : function() {
						return this.request.pjAliasUrl;
					},
					set : function(value) {
						
						if(value != '') {
							var self = this;
							axios.post('/data/invest/url', { url : value })
			                    .then(function(response){
			                    	self.urlConfirm = response.data
			                    })
						}

						this.request.pjAliasUrl = value;
					}
				}
			},
			methods : {
				load : function() {
					var self = this;
					axios.post('/data/save/invest/info/basic', {pjCode : this.pjCode})
	                    .then(function(response){
	                    	var result = response.data.rData;
	                    	var data = self.request;
	                    	data.pjTitle = result.pjTitle;
	                    	data.pjAliasUrl = result.pjAliasUrl;
	                    	data.pjSummary = result.pjSummary;
	                    	data.pjCardImg = result.pjCardImg;
	                    	data.pjBannerImg = result.pjBannerImg;
	                    	data.companyCiImg = result.companyCiImg

	                    	if(result.pjImages.length == 0) {
	                    		self.request.pjImage.push(self.file)
	                    	} else {
	                    		_.forEach(result.pjImages, function(item) {
		                    		self.request.pjImage.push({
		                    			fileName : item,
		                    			fileData : "",
		                    			state : true
		                    		})
	                    		})
	                    	}

	                    	if(result.pjVideos.length == 0) {
	                    		self.request.pjVideos.push("")
	                    	} else {
		                    	_.forEach(result.pjVideos, function(item) {
		                    		self.request.pjVideos.push(item)
		                    	})
	                    	}

	                    	self.dataConfirm = !self.dataConfirm
	                    })
				},
				videoAdd : function() {
					if(this.pjStatus == 1) return;
					
					if(!_.last(this.request.pjVideos)) return
					this.request.pjVideos.push("");
				},
				videoDelete : function() {
					if(this.pjStatus == 1) return;
					this.request.pjVideos.pop();
				},
				imageAdd : function() {
					if(this.pjStatus == 1) return;
					var check = _.last(this.request.pjImage);
					if(check.state && check.fileName == '') return
					this.request.pjImage.push(this.file)
				},
				imageDelete : function(index) {
					if(this.pjStatus == 1) return;

					this.request.pjImage[index].state = false;

					var searchArray = _.filter(this.request.pjImage, function(item) {
						return item.state == true
					});

					if(searchArray.length == 0) {
						this.request.pjImage.push(this.file)
					}
				},
				save : function(next) {
					if(this.pjStatus == 1) {
						noti.open('승인대기 중 인 프로젝트는 수정이 불가능합니다.')
						return;
					}

					if(!this.urlConfirm) {
						noti.open("이미 같은 주소가 있습니다. 새로운 주소를 입력해주세요")
						return false;
					}

					var self = this;
					this.request.pjCode = this.pjCode;
					this.request.pjImage = _.filter(this.request.pjImage, function(item) {
						return (item.state == true && item.fileName != '')
					});

					this.request.pjVideos = _.filter(this.request.pjVideos, function(item) {
						return item != ""
					});

					$('.page-loader-more').fadeIn('')
					axios.post('/set/invest/basic', this.request)
						.then(function(response) {
							$('.page-loader-more').fadeOut('');
							var result = response.data;
							if(result.rCode == "0000") {
							
								if(self.request.pjImage.length == 0) {
		                    		self.request.pjImage.push(self.file)
		                    	}
		                    	
		                    	if(self.request.pjVideos.length == 0) {
		                    		self.request.pjVideos.push("")
		                    	}

		                    	if(next == true) {
		                    		self.$emit('step-change', 2, true);
		                    	} else {
		                    		noti.open("저장되었습니다.")
		                    		self.$emit('step-change', 2, false);
		                    	}
	                    		
	                    	
	                    	} else {
	                    		noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
	                    	}
						})
				},
				urlCheck : function() {
		  			var self = this;
		  			if(this.request.pjAliasUrl.length == 0 || this.request.pjAliasUrl == '') {
		  				this.urlConfirm = true;
		  				return;
		  			}

		  			axios.post('/data/invest/url', { url : this.request.pjAliasUrl })
	                    .then(function(response){
	                    	self.urlConfirm = response.data
	                    })
		  		}
			}
		}
	}
}

export default new InvestBasic()