class Main {
	component() {
		return {
			template : `
				<div class="common_support hidden-xs hidden-sm">
					<div class="common_sub_vi bg_gray">
						<div class="container">
							<div class="row not-space">
								<div class="col-md-3">
									<div class="common_vi_title webfont2" v-text="title"></div>
								</div>
								<div class="col-md-4 text-right">
								</div>
								<div class="col-md-5">
									<div class="row">
										<!-- <div class="col-md-6">
											<a href="javascript:void(0)" class="btn btn-block btn-primary-outline">투자 가이드 다운받기</a>
										</div> -->
										<div class="col-md-6">
											<button type="button" class="btn btn-block btn-danger-outline" v-on:click="approve" :disabled="changeStatus != 0">승인요청하기</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="common_sub_layout">
						<div class="container">
								<div class="row not-space">
									<div class="col-md-2">
										<div class="col-line-lm">
											<div class="user-media">
												<div class="media">
													<div class="media-left media-middle">
														<img class="media-object" :src="'//' + userImage"  />
													</div>
													<div class="media-body media-middle" v-html="userName"></div>
												</div>
											</div>
											<div class="st-navi-device-wrap">
												<span>프로젝트 미리보기</span>
												<div class="st-navi-device">
													<a v-on:click="pagePreview('A1522')">모바일</a>
													<a v-on:click="pagePreview('A1474')">태블릿</a>
													<a v-on:click="pagePreview('PC05')">PC</a>
												</div>
											</div>
											<ul class="st-navi st-navi-p">
												<li class="active">
													<a href="javascript:void(0)">프로젝트 만들기</a>
													<div class="list_link_tab">
														<strong>STEP 1</strong>
														<ul>
															<li :class="{'active' : step == 1, 'vpass' : confirm.step1}"><a href="javascript:void(0)" v-on:click="stepChange(1, true)">1. 프로젝트 기본정보</a></li>
															<li :class="{'active' : step == 2, 'vpass' : confirm.step2}"><a href="javascript:void(0)" v-on:click="stepChange(2, true)">2. 회사 기본정보</a></li>
															<li :class="{'active' : step == 3, 'vpass' : confirm.step3}"><a href="javascript:void(0)" v-on:click="stepChange(3, true)">3. 증권발행조건</a></li>
														</ul>
													</div>

													<div class="list_link_tab">
														<strong>STEP 2</strong>
														<ul>
															<li :class="{'active' : step == 4, 'vpass' : confirm.step4}"><a href="javascript:void(0)" v-on:click="stepChange(4, true)">1. 핵심투자노트</a></li>
															<li :class="{'active' : step == 5, 'vpass' : confirm.step5}"><a href="javascript:void(0)" v-on:click="stepChange(5, true)">2. 온라인 IR</a></li>
															<li :class="{'active' : step == 6, 'vpass' : confirm.step6}"><a href="javascript:void(0)" v-on:click="stepChange(6, true)">3. 자주하는 질문</a></li>
														</ul>
													</div>

													<div class="list_link_tab">
														<strong>STEP 3</strong>
														<ul>
															<li :class="{'active' : step == 7, 'vpass' : confirm.step7}"><a href="javascript:void(0)" v-on:click="stepChange(7, true)">1. 첨부파일</a></li>
															<li :class="{'active' : step == 8, 'vpass' : confirm.step8}"><a href="javascript:void(0)" v-on:click="stepChange(8, true)">2. 주금납입정보</a></li>
															<li :class="{'active' : step == 9}"><a href="javascript:void(0)" v-on:click="stepChange(9, true)">3. 부가정보</a></li>
														</ul>
													</div>
												</li>
											</ul>
										</div>
									</div>
									
									<template v-if="dataConfirm">
										<invest-basic :pj-code="pjCode" :pj-status="changeStatus" v-if="step == 1" v-on:step-change="stepChange"></invest-basic>
										<company-basic :pj-code="pjCode" :pj-status="changeStatus" v-if="step == 2" v-on:step-change="stepChange"></company-basic>
										<securities-issue :pj-code="pjCode" :pj-status="changeStatus" v-if="step == 3" v-on:step-change="stepChange"></securities-issue>
										<point-note :pj-code="pjCode" :pj-status="changeStatus" v-if="step == 4" v-on:step-change="stepChange"></point-note>
										<online-ir :pj-code="pjCode" :pj-status="changeStatus" v-if="step == 5" v-on:step-change="stepChange"></online-ir>
										<faq :pj-code="pjCode" :pj-status="changeStatus" v-if="step == 6" v-on:step-change="stepChange"></faq>
										<file :pj-code="pjCode" :pj-status="changeStatus" v-if="step == 7" v-on:step-change="stepChange"></file>
										<stock :pj-code="pjCode" :pj-status="changeStatus" v-if="step == 8" v-on:step-change="stepChange"></stock>
										<keyword :pj-code="pjCode" :pj-status="changeStatus" :approve-check="approveCheck" v-if="step == 9" v-on:step-change="stepChange" v-on:approve="approve"></keyword>	
									</template>
								</div>
						</div>

					</div>
				</div>
			`,
			props : ['pjCode'],
			data : function() {
				return {
					dataConfirm : false,
					step : 1,
					confirm : {
						step1 : false,
						step2 : false,
						step3 : false,
						step4 : false,
						step5 : false,
						step6 : false,
						step7 : false,
						step8 : false
					},
					titleName : {
						1 : "기본정보",
						2 : "회사 기본정보",
						3 : "증권발행조건",
						4 : "핵심투자노트",
						5 : "온라인 IR",
						6 : "자주하는질문",
						7 : "첨부파일",
						8 : "주금납입정보",
						9 : "부가정보"
					},
					url : '',
					endStatus : '',
					recStatus : ''
				}
			},
			computed : {
				userName : function() {
					return userInfo.name + "<br /> 님의 프로젝트"
				},
				userImage : function() {
					return userInfo.image
				},
				title : function() {
					return this.titleName[this.step]
				},
				approveCheck : function() {
					return this.confirm.step1 && this.confirm.step2 && this.confirm.step3 && this.confirm.step4 && this.confirm.step7 && this.confirm.step8;
				},
				projectUrl : function() {
					return (this.url != '' && this.url) ? encodeURI(this.url) : ''
				},
				changeStatus : function() {
					if(this.recStatus == '1') {
						return 1; //승인대기, 전부 수정 불가 
					}

					if(this.recStatus == '2' && this.endStatus != '0') {
						return 2; //진행중 or 종료, 부분 수정 가능
					}

					return 0; //수정가능
				}
			},
			components : {
				investBasic : require('./basic.js').default.component(),
				companyBasic : require('./company-basic.js').default.component(),
				securitiesIssue : require('./securities-issue.js').default.component(),
				pointNote : require('./point-note.js').default.component(),
				onlineIr : require('./online-ir.js').default.component(),
				faq : require('./faq.js').default.component(),
				file : require('./file.js').default.component(),
				stock : require('./stock-payment.js').default.component(),
				keyword : require('./keyword.js').default.component()
			},
			created : function() {
				this.check(true);
			},
			methods : {
				stepChange : function(step, check) {
					if(check){
						this.step = step;
					}
					this.check(false)
				},
				check : function(init) {
					var self = this;
					axios.post('/data/save/invest/info/confirm', {pjCode : this.pjCode, memCode : userInfo.memCode})
						.then(function(response) {
							var result = response.data.rData;
        					if(!result.check) {
        						noti.open("잘못된 접근입니다.", function() {window.open('/', '_self')});
        						return;
        					}
        					if(init) {
        						self.dataConfirm = true;
        					}
        					self.url = result.pjAliasUrl;
        					self.endStatus = result.pjEndStatus;
							self.recStatus = result.pjRecStatus;
        					self.confirm.step1 = result.investBasic;
        					self.confirm.step2 = result.companyBasic;
        					self.confirm.step3 = result.securitiesIssue;
        					self.confirm.step4 = result.pointNote;
        					self.confirm.step5 = result.onlineIr;
        					self.confirm.step6 = result.faq;
        					self.confirm.step7 = result.file;
        					self.confirm.step8 = result.stockPayment;
						})
				},
				approve : function() {
					if(this.changeStatus != 0) return;

					if (!this.approveCheck) {
						noti.open("모든 단계를 완료해야 승인요청을 할 수 있습니다.");
						return false;
					}

					$('.page-loader-more').fadeIn('')
					axios.post('/data/save/invest/approve', {pjCode : this.pjCode})
						.then(function(response) {
							$('.page-loader-more').fadeOut('');
							var result = response.data;
							if(result.rCode == "0000") {
	                    		noti.open("승인요청이 완료되었습니다")
	                    	} else {
	                    		noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
	                    	}
						})
				},
				pagePreview : function(type) {
					if(this.projectUrl == '') {
						noti.open('프로젝트 웹주소를 먼저 설정해주세요.');
						return;
					}

					window.open('http://troy.labs.daum.net/?url=' + this.projectUrl + '&device=' + type, '_blank')
				}
			}
		}
	}
}

export default new Main()