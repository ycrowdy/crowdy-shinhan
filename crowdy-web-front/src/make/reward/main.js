class Main {
	component() {
		return {
			template : `
			<div id="list_wrap" class="common_support hidden-xs hidden-sm reward-make-main">
				<div class="common_sub_vi bg_gray">
					<div class="container">
						<div class="row not-space">
							<div class="col-md-3">
								<div class="common_vi_title webfont2" v-text="title"></div>
							</div>
							<div class="col-md-4 text-right">
								<!-- <div class="mr20">
									<a href="javascript:void(0)" class="btn btn-link mr15 webfont2">미리보기</a>
									<a href="javascript:void(0)" class="btn btn-link webfont2">저장 후 다음단계</a>
								</div> -->
							</div>
							<div class="col-md-5">
								<div class="row">
									<div class="col-md-6">
										<a href="https://image-se.ycrowdy.com/reward_guide/%E1%84%8F%E1%85%B3%E1%84%85%E1%85%A1%E1%84%8B%E1%85%AE%E1%84%83%E1%85%B5%E1%84%80%E1%85%A1%E1%84%8B%E1%85%B5%E1%84%83%E1%85%B3%E1%84%87%E1%85%AE%E1%86%A8(20180308).pdf" class="btn btn-block btn-primary-outline">가이드북 다운로드</a>
									</div>
									<div class="col-md-6">
										<button type="button" href="javascript:void(0)" class="btn btn-block btn-danger-outline" :disabled="changeStatus != 0" v-on:click="approveChecking">승인요청하기</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="common_sub_layout">
					<div class="container">
						<div class="row not-space">
							<div class="col-md-2">
								<div class="col-line-lm">
									<!-- <div class="user-media">
										<div class="media">
											<div class="media-left media-middle">
												<img class="media-object" :src="'//' + userImage"  />
											</div>
											<div class="media-body media-middle" v-html="userName"></div>
										</div>
									</div> -->
									<div class="st-navi-device-wrap">
										<span>프로젝트 미리보기</span>
										<div class="st-navi-device">
											<a v-on:click="pagePreview('A1522')">모바일</a>
											<a v-on:click="pagePreview('A1474')">태블릿</a>
											<a v-on:click="pagePreview('PC05')">PC</a>
										</div>
									</div>
									<ul class="st-navi st-navi-p">
										<li class="active">
											<a href="javascript:void(0)">프로젝트 만들기</a>
											<div class="list_link_tab">
												<ul>
													<li :class="{'active' : step == 1, 'vpass' : confirm.step1}"><a href="javascript:void(0)" v-on:click="stepChange(1, true)">1. 기본정보</a></li>
													<li :class="{'active' : step == 2, 'vpass' : confirm.step2}"><a href="javascript:void(0)" v-on:click="stepChange(2, true)">2. 스토리</a></li>
													<li :class="{'active' : step == 3, 'vpass' : confirm.step3}"><a href="javascript:void(0)" v-on:click="stepChange(3, true)">3. 리워드</a></li>
													<li :class="{'active' : step == 4, 'vpass' : confirm.step4}"><a href="javascript:void(0)" v-on:click="stepChange(4, true)">4. 제작자/부가 정보</a></li>
												</ul>
											</div>
										</li>
									</ul>
								</div>
							</div>
							
							<template v-if="dataConfirm">
								<reward-basic :cp-code="cpCode" :cp-status="changeStatus" v-if="step == 1" v-on:set-url="setUrl" v-on:step-change="stepChange" v-on:check="check"></reward-basic>
								<story :cp-code="cpCode" :cp-status="changeStatus" v-if="step == 2"  v-on:step-change="stepChange" v-on:check="check"></story>
								<benefit :cp-code="cpCode" :cp-status="changeStatus" v-if="step == 3"  v-on:step-change="stepChange" v-on:check="check"></benefit>
								<fund :cp-code="cpCode" :cp-status="changeStatus" :end-status="endStatus" v-if="step == 4"  v-on:step-change="stepChange" v-on:check="check"></fund>
							</template>

						</div>
					</div>
				</div>
			</div>
			`,
			props : ['cpCode'],
			data : function() {
				return {
					// cpEndStatus : (승인대기:0 진행중:1 성공:2 실패:3)
					// cpEndStatus : 0,
					dataConfirm : false,
					step : 1,
					confirm : {
						step1 : false,
						step2 : false,
						step3 : false,
						step4 : false
					},
					titleName : {
						1 : "기본정보",
						2 : "스토리",
						3 : "리워드",
						4 : "제작자/부가 정보"
					},
					url : '',
					endStatus : '',
					recStatus : ''
				}
			},
			computed : {
				userName : function() {
					return userInfo.name + "<br /> 님의 프로젝트"
				},
				userImage : function() {
					return userInfo.image
				},
				title : function() {
					return this.titleName[this.step]
				},
				approveCheck : function() {
					return this.confirm.step1 && this.confirm.step2 && this.confirm.step3 && this.confirm.step4;
				},
				projectUrl : function() {
					return (this.url != '' && this.url) ? encodeURI(this.url) : ''
				},
				changeStatus : function() {
					if(this.recStatus == '1' || (this.recStatus == '2' && this.endStatus != '1')) {
						return 1; //승인대기 || 종료, 전부 수정 불가 
					}

					if(this.recStatus == '2' && this.endStatus == '1') {
						return 2; //진행중, 부분 수정 가능
					}

					return 0; //수정가능
				}
			},
			components : {
				rewardBasic : require('./basic.js').default.component(),
				story : require('./story.js').default.component(),
				benefit : require('./benefit.js').default.component(),
				fund : require('./fund.js').default.component()
			},
			created : function() {
				this.check(true)
			},
			methods : {
				stepChange : function(step, check) {
					if(check){
						this.step = step;
					}
					this.check(false)
				},
				check : function(init) {
					var self = this;
					axios.post('/data/save/reward/info/confirm', {cpCode : this.cpCode, memCode : userInfo.memCode})
						.then(function(response) {
							var result = response.data.rData;
        					if(!result.check) {
        						noti.open("잘못된 접근입니다.", function() {window.open('/', '_self')});
        						return;
        					}
        					if(init) {
        						self.dataConfirm = true;
        					}
        					self.confirm.step1 = result.rewardBasic;
        					self.confirm.step2 = result.story;
        					self.confirm.step3 = result.benefit;
        					self.confirm.step4 = result.fund;
        					self.cpEndStatus = result.cpEndStatus;
        					self.url = result.cpAliasUrl;
        					self.endStatus = result.cpEndStatus;
							self.recStatus = result.cpRecStatus;
						})
				},
				approveChecking : function() {
					var self = this;

					if(this.changeStatus != 0) return;

					$('.page-loader-more').fadeIn('')
					axios.post('/data/member/info', { memCode : userInfo.memCode })
						.then(function(response) {
							$('.page-loader-more').fadeOut('');
							var result = response.data;
							if(result.rCode == "0000") {
								if(result.rData.memNameConfirm == 'Y') {

									if (!self.approveCheck) {
										var moveStep = 1;
										if (!self.confirm.step1) {
											moveStep = 1; 
										} else if (!self.confirm.step2) {
											moveStep = 2; 
										} else if (!self.confirm.step3) {
											moveStep = 3; 
										} else {
											moveStep = 4; 
										}

										noti.open("< " + self.titleName[moveStep] + " > 단계의 정보를 입력하지 않으셨습니다. <br/> < " + self.titleName[moveStep] + " > 단계의 모든 요소를 입력 후 다시 승인요청을 해주세요.", self.stepChange(moveStep, true));
										return false;
									}

									noti.open("승인 요청하시겠습니까?", self.approve, true);
								} else {
									// 설정 - 계정정보로 이동
									noti.open('본인인증을 완료해야 승인요청 할 수 있습니다. 설정 페이지로 이동하시겠습니까?', function() {window.open('/mypage/main?menu=5&sub-menu=1', '_self')}, true);
								}
	                    	} else {
	                    		noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
	                    	}
						})

				},
				approve : function() {
					$('.page-loader-more').fadeIn('')
					axios.post('/data/save/reward/approval', {cpCode : this.cpCode})
						.then(function(response) {
							$('.page-loader-more').fadeOut('');
							var result = response.data;
							if(result.rCode == "0000") {
								// 마이페이지 - 제작한 프로젝트 - 리워드로 이동
	                    		noti.open("승인요청이 완료되었습니다.", function() {window.open('/mypage/main?menu=2&sub-menu=1', '_self')});
	                    	} else {
	                    		noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
	                    	}
						})
				},
				pagePreview : function(type) {
					if(this.projectUrl == '') {
						noti.open('프로젝트 고유 주소를 먼저 저장해주세요.');
						return;
					}

					window.open('http://troy.labs.daum.net/?url=' + this.projectUrl + '&device=' + type, '_blank')
				},
				setUrl : function(url){
					this.url = (url != '' && url) ? encodeURI(window.location.origin + '/r/' + url) : '';
				}
			}
		}
	}
}

export default new Main()