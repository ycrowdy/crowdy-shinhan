class RewardBasic {
	component() {
		return {
			template : `
			<div class="col-md-10 col-line">
				<div class="row not-space">
					<div class="col-lg-1"></div>
					<div class="col-lg-11">
						<div class="md-ml30">
							<form class="form-horizontal" id="projectForm" method="get" action="#">

								<div class="form-group row-mobile-n mb30">
									<label for="project_type1" class="col-xs-12 control-label control-label-big">
										<div class="text-left mb10">프로젝트의 성공 조건을 정해주세요</div>
										<p class="form-control-static mb10 mt0">100% 이상을 선택할 경우 <a class="red-800">기본 수수료는 5%</a>, 조건 없음을 선택할 경우 <a class="red-800">기본 수수료는 10%</a>입니다. (VAT 별도)</p>
									</label>
									<div class="col-xs-12">
										<div class="option_group option_group_none mt3">

											<div class="project_type">
												<label for="project_type1" class="radio-inline">
													<input type="radio" name="project_type" id="project_type1" value="1" v-model="request.cpFundType" :disabled="cpStatus != 0"/><span class="webfont">100% 이상</span>
												</label>
												<label for="project_type2" class="radio-inline">
													<input type="radio" name="project_type" id="project_type2" value="2" v-model="request.cpFundType" :disabled="cpStatus != 0"/><span class="webfont">조건 없음</span>
												</label>
											</div>

										</div>
									</div>
								</div>

								<div class="form-group row-mobile-n mb30">
									<label for="project_subject" class="col-xs-12 control-label control-label-big">
										<div class="text-left mb10">프로젝트의 제목을 적어주세요</div>
										<p class="form-control-static mb10 mt0">프로젝트의 핵심 내용을 담을 수 있고 간결한 제목을 정해주세요.</p>
									</label>
									<div class="col-xs-12 col-sm-8">
										<input type="text" class="form-control" id="project_subject" maxlength="40" v-model.trim="request.cpTitle" :disabled="cpStatus != 0" />
									</div>
									<div class="col-xs-12 col-sm-2">
										<span class="textarea_text_leng webfont2 mt10"><span id="charNum" v-text="request.cpTitle.length"></span> / 40</span>
									</div>
								</div>

								<div class="form-group row-mobile-n mb30">
									<label for="project_price" class="col-xs-12 control-label control-label-big">
										<div class="text-left mb10">목표 금액을 적어주세요</div>
										<p class="form-control-static mb10 mt0"><a class="red-800">최소 100,000원 이상</a>이어야 합니다.</p>
									</label>
									<div class="col-xs-12 col-sm-8" v-if="dataConfirm">									
										<vue-numeric  :disabled="cpStatus != 0" class="form-control" separator="," currency="" v-model="request.cpTargetAmount" data-vv-name="cpTargetAmount"></vue-numeric>
										
										<!-- <vue-numeric  :disabled="cpStatus != 0" class="form-control" separator="," currency="" v-model="request.cpTargetAmount" data-vv-name="cpTargetAmount" v-validate="'target_amount'" :class="{'error' : errors.has('cpTargetAmount')}"></vue-numeric>
										<label class="error" v-if="errors.has('cpTargetAmount')" v-text="errors.first('cpTargetAmount')"></label> -->
									</div>
									<div class="col-xs-12 col-sm-2">
										<div class="textarea_text_leng mt10">원</div>
									</div>
									<div class="col-xs-12 col-sm-10">
										<div class="fees_info">
											<div class="fees_info_frame">
												<div class="row not-space">
													<div class="col-xs-8">
														펀딩 결제 수수료(카드 수수료 + 계좌이체 수수료)
													</div>
													<div class="col-xs-4 text-right">
														<span id="pg_fees">0</span>원
													</div>
												</div>

												<div class="row not-space">
													<div class="col-xs-8">
														크라우디 플랫폼 이용 수수료
													</div>
													<div class="col-xs-4 text-right">
														<span id="platform_fees">0</span>원
													</div>
												</div>

												<div class="row not-space">
													<div class="col-xs-8">
														프로젝트를 성공했을 경우 예상 정산 금액
													</div>
													<div class="col-xs-4 text-right text-big">
														<span id="totalSum" class="red-800">0</span>원
													</div>
												</div>

												<ul>
													<li>1. 위 금액은 예상이므로, 실제 정산 금액은 조금 다를 수 있습니다.</li>
													<li>2. 목표금액을 초과달성하여도 수수료는 동일한 비율로 적용됩니다.</li>
													<li>
														3. 수수료의 비율은 다음과 같습니다.
														<ul>
															<li> - 크라우디 플랫폼 이용 수수료 (결제승인금액의 5% + 부가가치세 10%)</li>
															<li> - PG 수수료 (카드결제승인금액의 3-4% + 부가가치세 10%)</li>
														</ul>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>

								<div class="form-group row-mobile-n mb30">
									<label for="project_start" class="col-xs-12 control-label control-label-big">
										<div class="text-left mb10">프로젝트의 진행 기간을 적어주세요</div>
										<p class="form-control-static mb10 mt0">최소 7일부터 최대 60일까지 가능합니다.</p>
									</label>
									<div class="col-xs-12 col-sm-6">
										<div class="row row-mobile-n">
											<div class="col-xs-5">
												<input type="text" class="form-control" max="60" v-model.trim="cpDateTerm" placeholder="" v-if="dataConfirm" disabled/>
											</div>
											<div class="col-xs-2 col-sm-2">
												<div class="textarea_text_leng mt10">일 남음</div>
											</div>
											<div class="col-xs-5">
												<date-picker v-model="cpEndDate" :date="cpEndDate" v-if="dataConfirm" :max="maxDate" :min="minDate" :disabled="cpStatus != 0"></date-picker>
											</div>
										</div>
									</div>
								</div>
								
								<div class="form-group row-mobile-n">
			                        <label for="project_weburl" class="col-xs-12 control-label control-label-big">
			                            <div class="text-left">프로젝트의 고유 주소를 적어주세요</div>
			                            <p class="form-control-static mb10 mt0">프로젝트와 관련된 키워드를 짧은 영어로 적어주세요. 특수문자와 띄어쓰기는 불가능합니다.</p>
			                        </label>
			                        <div class="col-xs-4 col-sm-3">
			                            <p class="form-control-static">https://www.ycrowdy.com/r/</p>
			                        </div>
			                        <div class="col-xs-8 col-sm-3">
			                            <url-input :url="url" v-model="url" class="form-control" :class="{'error' : !urlConfirm}" maxlength="30" placeholder="프로젝트 명" :disabled="cpStatus != 0"></url-input>
			                        </div>
			                        <div class="col-xs-12" v-if="!urlConfirm">
										<label style="margin-left:15px;" class="error" v-text="'이미 같은 주소가 있습니다. 새로운 주소를 입력해주세요'"></label>
			                        </div>
			                    </div>

								<div class="form-group row-mobile-n mb30">
									<label for="project_img" class="col-xs-12 control-label control-label-big">
										<div class="text-left mb10">프로젝트 대표 이미지를 등록해주세요</div>
										<p class="form-control-static mb10 mt0">홈페이지와 외부 공유를 했을 때 보여집니다. 프로젝트를 한 눈에 나타낼 수 있는 이미지를 등록해주세요.</p>
									</label>
									<div class="col-xs-12 dropify-wrapper-340">
										<dropify-input v-if="dataConfirm" v-model="request.cpCardFile" default-message="최적 사이즈 740*492px" :default-img="request.cpCardImg" :disabled="cpStatus == 1"></dropify-input>
									</div>
								</div>

								<div class="form-group row-mobile-n mb30">
									<label for="project_top_img" class="col-xs-12 control-label control-label-big">
										<div class="text-left mb10">프로젝트 키워드를 적어주세요<span class="red-800">(선택사항)</span></div>
										<p class="form-control-static mb10 mt0">제목 외에도 키워드 검색 시 검색 결과에 프로젝트가 나타납니다.</p>
									</label>
									<div class="col-xs-12">
										<input type="text" data-limit="5" v-model.trim="keyWords" id="keyword" placeholder="최대 5개까지 등록 가능합니다. 키워드 입력 후 엔터를 눌러주세요" :disabled="cpStatus == 1"/>
									</div>
								</div>

								<div class="form-group row-mobile-n mt90">
									<div class="col-sm-11">
										<div class="form-group row-mobile-n">
											<div class="col-xs-6 col-sm-4 col-md-3">
												<div class="btn btn-block btn-primary-outline" v-on:click="save">저장하기</div>
											</div>
											<div class="col-xs-6 col-sm-4 col-md-3">
												<button type="button" class="btn btn-block btn-primary" v-on:click="save(true)">다음단계</button>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			`,
			props : ['cpCode', 'cpStatus'],
			data : function() {
				return {
					dataConfirm : false,
					now : moment().format("YYYY-MM-DD"),
					/* base */
					file : {
						fileData : "",
						fileName : "",
						state : true
					},
					request : {
						cpTitle : '',
						cpFundType : 1,
						cpAliasUrl : '',
						cpTargetAmount : 0,
						cpEndDate : moment(this.now).add(14, 'days').format("YYYY-MM-DD"),
						cpCardFile : {},
						cpCardImg : '',
						cpKeyWord : '',
						cpKeyWords : []
					},
					keyWords : [],
					cpStartDate :  this.now,
					cpEndDate : moment(this.now).add(14, 'days').format("YYYY-MM-DD"),
					cpDateTerm : 1,
					minDate : moment(this.now).add(7, 'days').format("YYYY-MM-DD"),
					maxDate : moment(this.now).add(60, 'days').format("YYYY-MM-DD"),
					urlConfirm : true
				}
			},
			components : {
				dropifyInput : require('../../common/dropify-input.js').default.component(),
				vueNumeric : VueNumeric.default,
				datePicker : require('../../common/date-picker.js').default.component(),
				urlInput : require('../../common/url-input.js').default.component(),
			},
			created : function() {

                $(window).scrollTop(0);
                
				var self = this;
				 axios.post('/data/save/reward/info/basic', {cpCode : this.cpCode})
                    .then(function(response){

                    	var result = response.data;
                     	if(result.rCode == "0000") {
                     		var data = self.request;

                     		if (result.rData.simulationConfirm == 'Y') {
                     			noti.open("잘못된 URL입니다.", function() {window.open('/', '_self')});
                     		}

	                    	data.cpTitle = result.rData.cpTitle;
	                    	data.cpFundType = result.rData.cpFundType;
	                    	data.cpAliasUrl = result.rData.cpAliasUrl;
	                    	data.cpTargetAmount = result.rData.cpTargetAmount;
	                    	
	                    	self.cpEndDate = result.rData.cpEndDate;
							self.cpDateTerm = moment(self.cpEndDate).diff(self.cpStartDate, 'days') + 1;

	                    	data.cpCardImg = result.rData.cpCardImg;

	                    	self.keyWords = result.rData.cpKeyWord;
	                    	self.dataConfirm = !self.dataConfirm;

	                    	//cpFundType 체크
	                    	self.$nextTick(function() {
								$( ".option_group .radio-inline input" ).checkboxradio();
								$( ".project_type" ).controlgroup();

								$('#keyword')
							  	.on('tokenfield:createtoken', function (e) {
								    self.request.cpKeyWords.push(e.attrs.value)
							  	})
								.on('tokenfield:removedtoken', function (e) {
									self.request.cpKeyWords = _.without(self.request.cpKeyWords, e.attrs.value)
								})
								.tokenfield();
						    });

                    	} else {
                    		noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
                    	}
                })
			},
			watch : {
				// 캠페인 기간 변경
				cpEndDate : function(value) {
					var diff = moment(value).diff(this.cpStartDate, 'days');
					this.cpDateTerm = diff + 1;
				}
				// cpDateTerm : function() {
				// 	this.cpEndDate = moment(this.cpStartDate, "YYYY-MM-DD").add(this.cpDateTerm - 1, 'days');
				// }
			},
			computed : {
				url : {
					get : function() {
						return this.request.cpAliasUrl;
					},
					set : function(value) {
						
						if(value != '') {
							var self = this;
							axios.post('/data/reward/url', { url : value })
		                        .then(function(response){
		                            self.urlConfirm = response.data
		                        })
						}

						this.request.cpAliasUrl = value;
					}
				}
			},
			methods : {
				save : function(next) {
					var self = this;
					
	            	this.request.cpEndDate = this.cpEndDate;
					this.request.cpCode = this.cpCode;

					if(this.cpDateTerm < 7 && this.cpStatus == 0) {
						noti.open("캠페인 기간은 7일 이상이어야합니다.")
						return false;
					}	

					if(!this.urlConfirm) {
						noti.open("이미 같은 주소가 있습니다. 새로운 주소를 입력해주세요")
						return false;
					}

					this.request.cpKeyWord = _.uniq(_.compact(this.request.cpKeyWords)).join();

					this.$validator.validateAll()
						.then(function(success) {
							if(!success) return;

							$('.page-loader-more').fadeIn('')
							axios.post('/set/reward/basic', self.request)
								.then(function(response) {
									$('.page-loader-more').fadeOut('');
									var result = response.data;
									if(result.rCode == "0000") {
										self.$emit('set-url', self.request.cpAliasUrl);
										if(next == true) {
					                		self.$emit('step-change', 2, true);
					                	} else {
					                		noti.open("저장되었습니다.");
					                		self.$emit('step-change', 2, false);
					                	}
										
			                    	} else {
			                    		noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
			                    	}
								})
					})
				}
			}
		}
	}
}

export default new RewardBasic()