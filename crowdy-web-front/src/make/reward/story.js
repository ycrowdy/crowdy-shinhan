class Story {
	component() {
		return {
			template : `
			<div class="col-md-10 col-line">
				<div class="row not-space">
					<div class="col-lg-1"></div>
					<div class="col-lg-11">
						<div class="md-ml30">
							<form class="form-horizontal" id="combinationForm" method="get" action="#">

								<div class="form-group row-mobile-n">
			                        <label for="project_vod" class="col-xs-12 control-label control-label-big">
			                            <div class="text-left mb10">프로젝트 소개 영상과 이미지를 등록해주세요</div>
			                            <p class="form-control-static mb10 mt0">영상과 이미지를 함께 등록할 경우, 영상이 먼저 보여집니다.</p>
			                        </label>
			                        <div class="col-xs-12">
			                            <div class="row row-mobile-n">
			                                <label for="vod_gallery" class="col-xs-12 control-label">
			                                    <div class="text-left">동영상 주소를 적어주세요</div>
			                                </label>
			                                <div class="vod-holder input-append" v-for="(item, index) in request.cpVideos" :class="{'mt10' : index != 0}" :disabled="cpStatus == 1">
			                                    <div class="row row-mobile-n">
			                                        <div class="col-xs-10 col-sm-7">
			                                            <input type="text" class="form-control" v-model.trim="request.cpVideos[index]" placeholder="YouTube, Vimeo  URL을 입력해주세요" />
			                                        </div>
			                                        <div class="col-xs-2 col-sm-1" style="display: -webkit-box;">
			                                            <span class="files-add vod-add" v-on:click="videoAdd" v-if="index + 1 == request.cpVideos.length" style="margin-right: 10px;">+</span>
			                                            <span class="files-add img-delete vod-delete" v-on:click="videoDelete(index)" v-if="request.cpVideos.length != 1">-</span>
			                                        </div>
			                                    </div>
			                                </div>
			                            </div>
			                        </div>
			                        <div class="col-xs-12 mt25">
			                            <label for="eligible[]" class="control-label">
			                                <div class="text-left">이미지를 등록해주세요</div>
			                            </label>
			                            <div class="row">
			                                <div class="eligible-holder input-append dropify-wrapper-197 col-xs-6 col-sm-3 mt10" v-for="(item, index) in request.cpImage" v-if="request.cpImage[index].state" :disabled="cpStatus == 1">
			                                    <dropify-input class="dropify-gallery" :default-index="index" v-model="request.cpImage[index]" v-on:clear="imageDelete" :default-message="'최적 사이즈 740×417px'" :default-img="item.fileName"></dropify-input>
			                                </div>
			                                <div class="col-xs-6 col-sm-3 dropify-wrapper-197 mt10">
			                                    <div class="dropify-wrapper dropify-wrapper-plus" v-on:click="imageAdd" :disabled="cpStatus == 1">+</div>
			                                </div>
			                            </div>
			                        </div>
			                    </div>

								<div class="form-group row-mobile-n">
									<label for="project_story" class="col-xs-12 control-label control-label-big">
										<div class="text-left mb10">프로젝트 스토리를 적어주세요</div>
										<p class="form-control-static mb10 mt0"><a class="red-800">프로젝트를 시작하기 위해 필요한 내용이 없다면 승인이 되지 않습니다. 프로젝트 가이드북</a>을 읽어보시고 매력적인 스토리를 적어주세요.</p>
									</label>
									<div class="col-xs-12">
										<div class="ps-wrap">
											<div class="mb15">
												Enter(↵) : 문단 나눔, Shift + Enter : 줄바꿈입니다!  <br />
												적절한 문단/줄 바꿈만으로 멋진 프로젝트를 완성 할 수 있습니다!
											</div> 
											<story-area :id="'editor0'" v-if="dataConfirm" :value="request.cpStory" v-model="request.cpStory" :disabled="cpStatus == 1"></story-area>
										</div>
									</div>
								</div>

								<div class="form-group row-mobile-n">
									<label for="project_story" class="col-xs-12 control-label control-label-big">
										<div class="text-left mb10">환불 및 교환 정책을 적어주세요</div>
										<p class="form-control-static mb10 mt0">스토리 맨 아래에 노출됩니다.</p>
									</label>
									<div class="col-xs-12">
										<div class="ps-wrap">

											<div class="row row-mobile-n mb15">
												<label for="project_policy" class="col-xs-12 control-label">
													<div class="text-left mb10">진행자의 환불 및 교환 정책<span class="red-800">(선택사항)</span></div>
												</label>
												<div class="col-xs-12">
													<textarea rows="5" id="project_policy" class="form-control" title="" placeholder="크라우디 환불 및 교환 정책 외에 추가사항이 있으시면 적어주세요." v-model="request.cpRefundPolicy" :disabled="cpStatus != 0"></textarea>
												</div>
											</div>


											<div class="row row-mobile-n mb35">
												<label for="project_mobile1" class="col-xs-12 control-label">
													<div class="text-left mb10">문의 가능한 번호</div>
													<p class="form-control-static mb10 mt0">번호 공개 여부를 선택해주세요.</p>
												</label>
												<div class="col-sm-6">
													<input type="text" class="form-control telbox" maxlength="200" placeholder="번호를 적어주세요 (예. 01012345678)" :value="request.cpRefundTell" v-model="request.cpRefundTell" :disabled="cpStatus == 1"/>
												</div>
												<div class="col-sm-6">
													<label for="tel_open" class="checkbox-inline" style="position: relative;top:-3px;">
														<input type="checkbox" name="tel_open" id="tel_open" :checked="request.cpRefundTellViewStatus == '1'" v-on:click="changeRefundTellViewStatus()" :disabled="cpStatus == 1"/><span class="label"></span><span class="label-text">번호공개</span>
													</label>
												</div>
											</div>

											<div class="row row-mobile-n mb35">
												<label for="project_email" class="col-xs-12 control-label">
													<div class="text-left mb10">문의 이메일</div>
													<p class="form-control-static mb10 mt0">반드시 적어주세요.</p>
												</label>
												<div class="col-sm-6">
													<input type="text" class="form-control" id="project_email" maxlength="200" title="" placeholder="info@ycrowdy.com" :value="request.cpInquiryEmail" v-model="request.cpInquiryEmail" :disabled="cpStatus == 1"/>
												</div>
											</div>

											<div class="textarea-style">
												<strong>크라우디의 환불 및 교환정책 기본사항</strong>
												<ul>
													<li>1. 프로젝트 기간 중에는 자유롭게 <a href="/mypage/main?menu=1&sub-menu=1" target="_blank">마이 페이지</a>에서 펀딩 취소가 가능합니다.</li>
													<li>2. 펀딩을 받아야만 생산을 시작할 수 있는 크라우드 펀딩 특성상, 프로젝트 종료 이후에는 단순 변심으로 인한 교환이나 환불이 불가하니 이점 양해 부탁드립니다.</li>
													<li>3. 리워드 배송일이 예상보다 지연되는 경우, 새소식과 후원자 분들의 이메일을 통해 안내해드리겠습니다.<br />이에 관한 문의는 이메일 "<a :href="'mailto:' + request.cpInquiryEmail" target="_blank" class="project_email red-800">{{request.cpInquiryEmail}}</a>" <template v-if="request.cpRefundTellViewStatus == '1'"> , 연락처 "<a class="red-800" :href="'tel:' + request.cpRefundTell">{{request.cpRefundTell}}</a>" 로 연락바랍니다. </template>  <template v-if="request.cpRefundTellViewStatus == '0'"> 로 문의바랍니다.</template> </li>
												</ul>
											</div>
											<ul class="con-icon blue-800 mb25">
												<li>프로젝트 종료일 이후에 리워드와 관련된 환불 및 교환은 프로젝트 제작자가 약속하는 것에 따르며 크라우디는 이에 책임지지 않습니다.</li>
											</ul>
										</div>
									</div>
								</div>

								<div class="form-group row-mobile-n mt90">
									<div class="col-sm-11">
										<div class="form-group row-mobile-n">
											<div class="col-xs-6 col-sm-4 col-md-3">
												<button type="button" class="btn btn-block btn-primary-outline" v-on:click="save(false)">저장하기</button>
											</div>
											<div class="col-xs-6 col-sm-4 col-md-3">
												<button type="button" class="btn btn-block btn-primary" v-on:click="save(true)">다음단계</button>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			`,
			props : ['cpCode', 'cpStatus'],
			data : function() {
				return {
					dataConfirm : false,
					file : {
						fileData : "",
						fileName : "",
						state : true
					},
					request : {
						cpCode : "",
						memCode : "",
						cpStory : "",
						cpRefundTell : "",
						// 0 : 비공개, 1: 공개
						cpRefundTellViewStatus : "1",
						cpInquiryEmail : "",
						cpRefundPolicy : "",
						cpVideos : [],
						cpImage : []
					}
				}
			},
			components : {
				dropifyInput : require('../../common/dropify-input.js').default.component(),
				storyArea : require('../../common/text-editor.js').default.component()
			},
			created : function() {

                $(window).scrollTop(0);

				var self = this;
				 axios.post('/data/save/reward/info/story', {cpCode : this.cpCode})
                    .then(function(response){
                    	var result = response.data;
                     	if(result.rCode == "0000") {
                     		var data = self.request;
	                    	data.cpStory = result.rData.cpStory;
	                    	data.cpRefundTell = result.rData.cpRefundTell;
	                    	data.cpInquiryEmail = result.rData.cpInquiryEmail;
	                    	data.cpRefundTellViewStatus = result.rData.cpRefundTellViewStatus;
	                    	data.cpRefundPolicy = result.rData.cpRefundPolicy;

							if(result.rData.cpImages.length == 0) {
	                    		data.cpImage.push(self.file)
	                    	} else {
	                    		_.forEach(result.rData.cpImages, function(item) {
		                    		data.cpImage.push({
		                    			fileName : item,
		                    			fileData : "",
		                    			state : true
		                    		})
	                    		})
	                    	}

	                    	if(result.rData.cpVideos.length == 0) {
	                    		data.cpVideos.push("");
	                    	} else {
		                    	_.forEach(result.rData.cpVideos, function(item) {
		                    		data.cpVideos.push(item)
		                    	})
	                    	}
	                    	
	                    	self.dataConfirm = !self.dataConfirm;

                    	} else {
                    		noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
                    	}
                })
			},
			methods : {
				videoAdd : function() {
					//if(!_.last(this.request.cpVideos)) return
					if(this.cpStatus == 1) return;
					this.request.cpVideos.push("");
				},
				videoDelete : function(index) {
					if(this.cpStatus == 1) return;
					this.request.cpVideos.splice(index, 1);
				},
				imageAdd : function() {
					if(this.cpStatus == 1) return;
					var check = _.last(this.request.cpImage);
					if(check.state && check.fileName == '') return
					this.request.cpImage.push(this.file)
				},
				imageDelete : function(index) {
					if(this.cpStatus == 1) return;
					this.request.cpImage[index].state = false;

					var searchArray = _.filter(this.request.cpImage, function(item) {
						return item.state == true
					});

					if(searchArray.length == 0) {
						this.request.cpImage.push(this.file)
					}
				},
				changeRefundTellViewStatus : function() {
					var self = this;

					if (self.request.cpRefundTellViewStatus == '1') {
						self.request.cpRefundTellViewStatus = '0'
						$('input.telbox').attr("disabled", true);
					} else {
						self.request.cpRefundTellViewStatus = '1'
						$('input.telbox').attr("disabled", false);
					}
				},
				save : function(next) {
					var self = this;

					if(this.cpStatus == 1) {
						noti.open('수정이 불가능한 상태입니다.')
						return;
					}
					
            		this.request.cpCode = this.cpCode;
					this.request.memCode = userInfo.memCode;

					this.request.cpImage = _.filter(this.request.cpImage, function(item) {
						return (item.state == true && item.fileName != '')
					});

					this.request.cpVideos = _.filter(this.request.cpVideos, function(item) {
						return item != ""
					});


					$('.page-loader-more').fadeIn('')
					axios.post('/set/reward/story', this.request)
						.then(function(response) {
							$('.page-loader-more').fadeOut('');
							var result = response.data;
							if(result.rCode == "0000") {

								if(self.request.cpImage.length == 0) {
		                    		self.request.cpImage.push(self.file)
		                    	}
		                    	
		                    	if(self.request.cpVideos.length == 0) {
		                    		self.request.cpVideos.push("")
		                    	}
		                    	
		                    	if(next == true) {
			                		self.$emit('step-change', 3, true);
			                	} else {
			                		noti.open("저장되었습니다.")
			                		self.$emit('step-change', 3, false);
			                	}
	                    		
	                    	} else {
	                    		noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
	                    	}
						})

				}
			}
		}
	}
}

export default new Story()