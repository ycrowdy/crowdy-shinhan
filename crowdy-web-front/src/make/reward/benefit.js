class Benefit {
	component() {
		return {
			template : `
			<div class="col-md-10 col-line">
				<div class="row not-space">
					<div class="col-lg-1"></div>
					<div class="col-lg-11">
						<div class="md-ml30">
							<form class="form-horizontal" id="combinationForm" method="get" action="#">
								<div class="form-group row-mobile-n">
									<div class="col-xs-12 control-label control-label-big">
										<div class="text-left mb10">
											프로젝트 리워드를 구성해주세요 
										</div>
										<p class="form-control-static mb10 mt0">프로젝트 시작을 위해서는 <a class="red-800">최소 1개 이상의 리워드가 있어야 합니다.</a> 배송이 필요한 리워드는 배송비가 포함된 가격을 적어주세요.</p>
									</div> 
								</div>

								<div class="ps-wrap mb80">
									<div class="row row-mobile-n mb25">
										<label for="rewards_price" class="col-xs-2 control-label">
											<div class="text-left">리워드 금액</div>
										</label>
										<div class="col-xs-8">
											<vue-numeric class="form-control" separator="," maxlength="9" currency="" v-model="request.cpBenefitAmount" placeholder="1,000원 이상 입력해주세요."></vue-numeric>
										</div>
										<div class="col-xs-2">
											<div class="mt10 textarea_text_leng webfont2">원</div>
										</div>
									</div>

									<div class="row row-mobile-n mb25">
										<label for="rewards_ea" class="col-xs-2 control-label">
											<div class="text-left">리워드 제공 <br />가능 수</div>
										</label>

										<div class="col-xs-5">
											<div class="option_group option_group_none mt3">
												<div class="qty_radio">
													<label for="qty_radio1" class="radio-inline">
														<input type="radio" name="qty_radio" id="qty_radio1" value="N" v-model="request.cpBenefitQtyLimit"/><span class="webfont">무제한</span>
													</label>
													<label for="qty_radio2" class="radio-inline">
														<input type="radio" name="qty_radio" id="qty_radio2" value="Y" v-model="request.cpBenefitQtyLimit"/><span class="webfont">제한</span>
													</label>
												</div>
											</div>
										</div>

										<template v-if="request.cpBenefitQtyLimit == 'Y'">
											<div class="col-xs-2">
												<input type="tel" class="form-control" v-model="request.cpBenefitQty" maxlength="100" placeholder="제공 가능 수량" />
											</div>

											<div class="col-xs-1">
												<div class="mt10 textarea_text_leng webfont2">개</div>
											</div>
										</template>
										
									</div>

									<div class="row row-mobile-n mb25">
										<label for="rewards_name" class="col-xs-2 control-label">
											<div class="text-left">리워드 제목</div>
										</label>
										<div class="col-xs-8">
											<input type="text" class="form-control " maxlength="30" v-model="request.cpBenefitTitle" />
										</div>
										<div class="col-xs-2">
											<div class="mt10 textarea_text_leng webfont2"><span id="charNum1" v-text="request.cpBenefitTitle.length"></span> / 30</div>
										</div>
									</div>

									<div class="row row-mobile-n mb25">
										<label for="rewards_contents" class="col-xs-2 control-label">
											<div class="text-left mb10">리워드 내용</div>
										</label>
										<div class="col-xs-8">
											<textarea rows="5" id="rewards_contents" class="form-control" v-model="request.cpBenefitInfo" maxlength="70" placeholder="준비된 리워드와 설명을 적어주세요"></textarea>
										</div>
										<div class="col-xs-2">
											<div class="mt10 textarea_text_leng webfont2"><span id="charNum2" v-text="request.cpBenefitInfo.length"></span> / 70</div>
										</div>
									</div>

									<div class="row row-mobile-n mb25">
										<label for="rewards_date" class="col-xs-2 control-label">
											<div class="text-left">예상 배송일</div>
										</label>
										<div class="col-xs-3">
											<date-picker v-model="request.cpBenefitDeliveryDate" :date="request.cpBenefitDeliveryDate"></date-picker>
										</div>
									</div>

									<div class="row row-mobile-n mb30">
										<div class="col-xs-2 control-label">
											<div class="text-left mb10">리워드 옵션</div>
										</div>
										<div class="col-xs-9">
											<!-- 옵션이 없을때 기본 버튼 -->										
											<template v-if="request.cpBenefitOptions.length == 0">
												<button type="button" class="btn btn-block btn-primary-outline" v-on:click="addOption" :disabled="cpStatus == 1">리워드 옵션 추가하기</button>
											</template>

											<!-- 옵션선택 Loop -->
											<!-- <div class="option_group" v-for="(item, index) in request.cpBenefitOptions" :disabled="cpStatus == 1">-->
											<div v-for="(item, index) in request.cpBenefitOptions" :disabled="cpStatus == 1">
												<!--<h6 class="small-hb grey-600 mb10">옵션선택 {{index + 1}}</h6>-->
												<!-- <div class="row row-mobile-n option_select">
													<div class="col-xs-6" :class="'option_radio_' + index">
														<label :for="'option_radio_' + index + '_1'" class="radio-inline">
															<input type="radio" :name="'option_radio_' + index" :id="'option_radio_' + index + '_1'" value="1" v-model="item.cpBenefitOptionType"/><span class="webfont">선택형</span>
														</label>
														<label :for="'option_radio_' + index + '_2'" class="radio-inline">
															<input type="radio" :name="'option_radio_' + index" :id="'option_radio_' + index + '_2'" value="2" v-model="item.cpBenefitOptionType"/><span class="webfont">단답형</span>
														</label>
													</div> 
													<div class="col-xs-6 text-right">
													<div class="col-xs-12 text-right">
														<div class="mt5">
															<a href="javascript:void(0);" class="btn_none_icon btn_plus blue-800" v-if="index == request.cpBenefitOptions.length - 1" v-on:click="addOption">추가하기</a>
															<a href="javascript:void(0);" class="btn_none_icon btn_delete red-800" v-on:click="deleteOption(index)">삭제하기</a>
														</div>
													</div>
												</div> --> 
												<!-- 선택형 -->
												<!-- <div id="option1_contents_radio1" class="row row-mobile-n otab1_contents mt10" v-if="item.cpBenefitOptionType == 1">
													<div class="col-xs-4">
														<input type="text" class="form-control " v-model="item.cpBenefitOptionTitle" maxlength="30" placeholder="ex) 옷의 사이즈를 적어주세요" />
													</div>
													<div class="col-xs-8">
														<keyword v-model.trim="item.cpBenefitOption" :value="item.cpBenefitOption"></keyword>
													</div>
												</div> -->
												<!-- //선택형 -->

												<!-- 단답형 -->
												<div id="option1_contents_radio2" class="row row-mobile-n otab1_contents mt10"  v-if="item.cpBenefitOptionType == 2">
													<div class="col-xs-8">
														<input type="text" class="form-control " v-model="item.cpBenefitOptionTitle" maxlength="20" placeholder="ex) 옷의 사이즈를 적어주세요" />
													</div>
													<div class="col-xs-2">
														<div class="mt10 textarea_text_leng webfont2"><span id="charNum3" v-text="item.cpBenefitOptionTitle.length"></span> / 20</div>
													</div>
												</div>
												<!-- //단답형 -->

												<div class="row row-mobile-n option_select">
													<div class="col-xs-12">
														<div class="mt5">
															<a href="javascript:void(0);" class="btn_none_icon btn_delete red-800" v-on:click="deleteOption(index)">삭제하기</a>
														</div>
													</div>
												</div>
											</div>
											<!-- //옵션선택 Loop -->
										</div>
									</div>

									<div class="row row-mobile-n mb25">
										<label for="rewards_date" class="col-xs-2 control-label">
											<div class="text-left">배송지 필요여부</div>
										</label>
										<div class="col-xs-6">
											<div class="option_group option_group_none mt3">
												<div class="delivery_radio">
													<label for="delivery_radio1" class="radio-inline" style="z-index: 0;">
														<input type="radio" name="delivery_radio" id="delivery_radio1" value="Y" v-model="request.cpBenefitDeliveryConfirm"/><span class="webfont">배송지 필요</span>
													</label>
													<label for="delivery_radio2" class="radio-inline" style="z-index: 0;">
														<input type="radio" name="delivery_radio" id="delivery_radio2" value="N" v-model="request.cpBenefitDeliveryConfirm"/><span class="webfont">배송지 필요없음</span>
													</label>
												</div>
											</div>
										</div>
									</div>

									<hr />

									<div class="row row-mobile-n pt15 mb30">
										<div class="col-xs-4"></div>
										<div class="col-xs-2">
											<button type="button" class="btn btn-block btn-primary" v-on:click="insertBenefit()">
												<template v-if="request.cpBenefitCode == ''">
													등록
												</template>
												<template v-if="request.cpBenefitCode != ''">
													수정
												</template>
											</button>
										</div>
										<div class="col-xs-2">
											<button type="button" class="btn btn-block btn-primary-outline" v-on:click="resetBenefit()">
												<template v-if="request.cpBenefitCode == ''">
													초기화
												</template>
												<template v-if="request.cpBenefitCode != ''">
													취소
												</template>
											</button>
										</div>
										<div class="col-xs-4"></div>
									</div>
								</div>

								<div class="form-group row-mobile-n">
									<div class="col-xs-2 control-label control-label-big">
										<div class="text-left mb10">
											등록된 리워드<br />미리보기 <!--a class="form-tip webuiPopover" href="javascript:void(0);" data-plugin="webuiPopover" data-content="운영하고 계신 웹사이트 또는 SNS가 있으시다면 등록 해주세요. 프로젝트 상세보기 페이지의 &lt;br / &gt;제작자 정보 ‘자세히 보기’에서 아이콘 클릭을 통해 해당 웹사이트로 바로 이동할 수 있게 됩니다. &lt;br / &gt;링크 등록을 통해 크라우드펀딩을 진행하는 제품/서비스 외에 제작자(팀)의 활동을 알려보세요." data-animation="pop"><i class="fa fa-question" aria-hidden="true"></i></a-->
										</div>
									</div>
									<div class="col-xs-10">
										<div id="st-items-wrap" class="row row-mobile-n">
											<p class="form-control-static mb10 mt0" v-if="!dataConfirm"> 리워드가 없습니다. 리워드를 추가해주세요.</p>
											
											<div class="col-xs-6">
												<div class="col-xs-12" v-for="(item, index) in benefitList" v-if="dataConfirm && (index % 2 == 0)">
                                                    <div class="st-items st-items-loop">
                                                        <a href="javascript:void(0)" class="st-link">바로가기</a>
                                                        <div class="row not-space">
                                                            <div class="col-xs-6 col-sm-12">
                                                                <div class="st-items-su">
                                                                    <strong><span class="webfont2" v-text="item.cpBenefitAmount"></span>원 펀딩</strong>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-6 col-sm-12 xs-text-right">
                                                                <div class="st-items-btn">
	                                                                <template v-if="item.cpBenefitQty < 999999">
	                                                                	<span class="btn btn-sm btn-danger">{{item.cpBenefitRemQty}}개 남음</span>
	                                                                </template>
	                                                                <template v-if="item.cpBenefitQty >= 999999">
	                                                                	<span class="btn btn-sm btn-danger">무제한</span>
	                                                                </template>
                                                                    <span class="btn btn-sm btn-danger-outline">{{item.cpBenefitSpsQty}}개 펀딩</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <dl>
                                                                    <dt>리워드명</dt>
                                                                    <dd><strong v-text="item.cpBenefitTitle"></strong></dd>
                                                                </dl>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <p v-html="item.cpBenefitInfo"></p>
                                                            </div>
                                                            <div class="col-xs-6 col-sm-12">
                                                                <dl>
                                                                    <dt>예상 배송일</dt>
                                                                    <dd><strong v-text="item.cpBenefitDeliveryDate"></strong></dd>
                                                                </dl>
                                                            </div>
                                                        </div>
                                                        <!-- 옵션 -->
                                                        <div class="st-items-option">
                                                            <dl v-for="(option, optionIndex) in item.cpBenefitOptions" v-if="dataConfirm">
                                                                <dt>{{option.cpBenefitOptionTitle}}</dt>
                                                                <dd>
                                                                	<template v-if="option.cpBenefitOptionType == 1">
																		<strong>{{option.cpBenefitOption}} 중 택 1</strong>
																	</template>
																	<template v-if="option.cpBenefitOptionType == 2">
																		<input type="text" value="직접 입력하세요" class="form-control" maxlength="50" disabled/>
																	</template>
                                                                </dd>
                                                            </dl>
                                                        </div>
                                                        <!-- //옵션 -->

                                                        <div class="st-items-option-control">
                                                        	<a v-if="item.cpBenefitStatus == 1 && cpStatus != 1" href="javascript:void(0);" class="btn_none_icon btn_stop blue-800" v-on:click="changeBenefitStatus(item, '2')">리워드 중단</a>
                                                        	<a v-if="item.cpBenefitStatus == 2 && cpStatus != 1" href="javascript:void(0);" class="btn_none_icon btn_stop blue-800" v-on:click="changeBenefitStatus(item, '1')">리워드 제공</a>
															<a v-if="cpStatus != 1 && item.cpBenefitSpsQty == 0" href="javascript:void(0);" class="btn_none_icon btn_modify red-800" v-on:click="updateBenefit(index)">수정</a>
															<a v-if="cpStatus != 1 && item.cpBenefitSpsQty == 0" href="javascript:void(0);" class="btn_none_icon btn_delete red-800" v-on:click="deleteBenefitCheck(item)">삭제</a>
														</div>
                                                    </div>
                                            	</div>
											</div> 
											<div class="col-xs-6">
												<div class="col-xs-12" v-for="(item, index) in benefitList" v-if="dataConfirm && (index % 2 == 1)">
                                                    <div class="st-items st-items-loop">
                                                        <a href="javascript:void(0)" class="st-link">바로가기</a>
                                                        <div class="row not-space">
                                                            <div class="col-xs-6 col-sm-12">
                                                                <div class="st-items-su">
                                                                    <strong><span class="webfont2" v-text="item.cpBenefitAmount"></span>원 펀딩</strong>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-6 col-sm-12 xs-text-right">
                                                                <div class="st-items-btn">
	                                                                <template v-if="item.cpBenefitQty < 999999">
	                                                                	<span class="btn btn-sm btn-danger">{{item.cpBenefitRemQty}}개 남음</span>
	                                                                </template>
	                                                                <template v-if="item.cpBenefitQty >= 999999">
	                                                                	<span class="btn btn-sm btn-danger">무제한</span>
	                                                                </template>
                                                                    <span class="btn btn-sm btn-danger-outline">{{item.cpBenefitSpsQty}}개 펀딩</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <dl>
                                                                    <dt>리워드명</dt>
                                                                    <dd><strong v-text="item.cpBenefitTitle"></strong></dd>
                                                                </dl>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <p v-html="item.cpBenefitInfo"></p>
                                                            </div>
                                                            <div class="col-xs-6 col-sm-12">
                                                                <dl>
                                                                    <dt>예상 배송일</dt>
                                                                    <dd><strong v-text="item.cpBenefitDeliveryDate"></strong></dd>
                                                                </dl>
                                                            </div>
                                                        </div>
                                                        <!-- 옵션 -->
                                                        <div class="st-items-option">
                                                            <dl v-for="(option, optionIndex) in item.cpBenefitOptions" v-if="dataConfirm">
                                                                <dt>{{option.cpBenefitOptionTitle}}</dt>
                                                                <dd>
                                                                	<template v-if="option.cpBenefitOptionType == 1">
																		<strong>{{option.cpBenefitOption}} 중 택 1</strong>
																	</template>
																	<template v-if="option.cpBenefitOptionType == 2">
																		<input type="text" value="직접 입력하세요" class="form-control" maxlength="50" disabled/>
																	</template>
                                                                </dd>
                                                            </dl>
                                                        </div>
                                                        <!-- //옵션 -->

                                                        <div class="st-items-option-control">
                                                        	<a v-if="item.cpBenefitStatus == 1 && cpStatus != 1" href="javascript:void(0);" class="btn_none_icon btn_stop blue-800" v-on:click="changeBenefitStatus(item, '2')">리워드 중단</a>
                                                        	<a v-if="item.cpBenefitStatus == 2 && cpStatus != 1" href="javascript:void(0);" class="btn_none_icon btn_stop blue-800" v-on:click="changeBenefitStatus(item, '1')">리워드 제공</a>
															<a v-if="cpStatus != 1 && item.cpBenefitSpsQty == 0" href="javascript:void(0);" class="btn_none_icon btn_modify red-800" v-on:click="updateBenefit(index)">수정</a>
															<a v-if="cpStatus != 1 && item.cpBenefitSpsQty == 0" href="javascript:void(0);" class="btn_none_icon btn_delete red-800" v-on:click="deleteBenefitCheck(item)">삭제</a>
														</div>
                                                    </div>
                                            	</div>
											</div>
											
										</div>
									</div>
								</div>

								<div class="form-group row-mobile-n mt90">
									<div class="col-sm-11">
										<div class="form-group row-mobile-n">
											<div class="col-xs-6 col-sm-4 col-md-3">
												<button type="button" class="btn btn-block btn-primary" v-on:click="save(true)">다음단계</button>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			`,
			props : ['cpCode', 'cpStatus'],
			data : function() {
				return {
					request : {
						cpCode : this.cpCode,
						memCode : userInfo.memCode,
						cpBenefitCode : "",
			            cpBenefitTitle : "",
			            cpBenefitAmount : 0,
			            cpBenefitInfo : "",
			            cpBenefitQty : 0,
			            cpBenefitRemQty : 0,
			            cpBenefitDeliveryDate : moment().format("YYYY-MM-DD"),
			            cpBenefitDeliveryConfirm : "Y",
			            //1:제공, 2:중지 3: 대기 (3인적은 없음.)
			            cpBenefitStatus : 1,
			            cpBenefitSpsQty : 0,
			            cpBenefitOptions : [],
			            cpBenefitQtyLimit : "N"
					},
					benefitList : [],
					selectedBenefit : {}
				}
			},
			computed : {
				dataConfirm : function() {
					return this.benefitList.length > 0; 
				}
			},
			components : {
				dropifyInput : require('../../common/dropify-input.js').default.component(),
				datePicker : require('../../common/date-picker.js').default.component(),
				//keyword : require('../../common/keyword.js').default.component(),
				vueNumeric : VueNumeric.default,
			},
			created : function() {
                $(window).scrollTop(0);
				// 리워드 혜택 카드 목록
				this.getBenefitList();
			},
			methods : {
				// 리워드 목록
				getBenefitList : function(refreshCheck) {
					var self = this;
					axios.post('/data/save/reward/info/benefit', {cpCode : this.cpCode})
	                    .then(function(response){
	                    	var result = response.data;
	                     	if(result.rCode == "0000") {
	                     		self.resetBenefit();
		                    	self.benefitList = result.rData;
		                    	self.setRadio(refreshCheck);
	                    	} else {
	                    		noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
	                    	}
	                })
				},
				setRadio : function(refreshCheck) {
					var self = this;
					// 라디오 체크

					if (refreshCheck == 'refresh') {
						self.$nextTick(function() {
							$( ".option_group .radio-inline input" ).checkboxradio(refreshCheck);
							$( ".delivery_radio, .qty_radio" ).controlgroup(refreshCheck);
					    });
					} else {
						self.$nextTick(function() {
							$( ".option_group .radio-inline input" ).checkboxradio();
							$( ".delivery_radio, .qty_radio" ).controlgroup();
					    });
					}
                	
				},
				// 리워드 옵션 추가
				addOption : function() {
					var self = this;
					var basicOption = {	
				            	cpBenefitOptionType : 2,
				            	cpBenefitOption : [],
				            	cpBenefitOptionTitle : "",
				            	cpBenefitCode : ""
				            };
					self.request.cpBenefitOptions.push(basicOption);
				//	self.setRadio();
				},
				// 리워드 옵션 삭제
				deleteOption : function(index) {
					var self = this;
					self.request.cpBenefitOptions.splice(index, 1);
				},
				// 리워드 등록/저장
				insertBenefit : function(){
					var self = this;
					for (var i = 0; i < self.request.cpBenefitOptions.length; i++) {
						var optionString = _.uniq(self.request.cpBenefitOptions[i].cpBenefitOption).join();
						self.request.cpBenefitOptions[i].cpBenefitOption = optionString;
						self.request.cpBenefitOptions[i].cpBenefitCode = self.request.cpBenefitCode;
					}
					self.request.memCode = userInfo.memCode;

		            if (self.request.cpBenefitQtyLimit == 'N') {
		            	self.request.cpBenefitRemQty = 999999;
		            	self.request.cpBenefitQty = 999999;
					} else {
						var a = self.request.cpBenefitQty;
		            	self.request.cpBenefitRemQty = a;
		            }
					
		            if (self.request.cpBenefitAmount < 1000) {
						noti.open("리워드 금액은 1000원 이상 입력해야합니다.");
		            	return;
		            }

		            if (self.request.cpBenefitQtyLimit == 'Y' && self.request.cpBenefitQty < 1) {
		            	noti.open("리워드 제공가능 수는 0개 이상이어야 합니다.");
		            	return;
		            }

		            if (self.request.cpBenefitTitle == '') {
		            	noti.open("리워드 제목을 입력해주세요.");
		            	return;
		            }

		            if ( self.request.cpBenefitInfo == '') {
						noti.open("리워드 내용을 입력해주세요.");
		            	return;
		            }

					axios.post('/data/save/reward/benefit/set', this.request)
	                    .then(function(response){
	                    	var result = response.data;
	                     	if(result.rCode == "0000") {
	                     		noti.open("등록/저장되었습니다.");
								self.getBenefitList('refresh');
								self.$emit('check', false);
	                    	} else {
	                    		noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.");
	                    	}
	                })
				},
				// 리워드 삭제
				deleteBenefitCheck : function(item){
					var self = this;
					this.selectedBenefit = item;
					noti.open("정말 리워드를 삭제 하시겠습니까?", self.deleteBenefit, true);
				},
				// 리워드 삭제
				deleteBenefit : function(){
					var self = this;

					axios.post('/data/save/reward/benefit/delete', {cpCode : this.cpCode, cpBenefitCode : this.selectedBenefit.cpBenefitCode})
	                    .then(function(response){
	                    	var result = response.data;
	                     	if(result.rCode == "0000") {
								self.getBenefitList('refresh');
								self.$emit('check', false);
	                    	} else {
	                    		noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
	                    	}
	                })
				},
				// 리워드 상태 변경 (중지, ... )
				changeBenefitStatus : function(item, status){
					var self = this;
					
					axios.post('/data/save/reward/benefit/status', {cpCode : this.cpCode, cpBenefitCode : item.cpBenefitCode, cpBenefitStatus : status})
	                    .then(function(response){
	                    	var result = response.data;
	                     	if(result.rCode == "0000") {

	                     		if (status =='2') {
	                     			noti.open("리워드의 제공이 중단되었습니다.");
	                     		} else {
	                     			noti.open("리워드가 다시 제공됩니다.");
	                     		}
		                    	
		                    	self.getBenefitList();
	                    	} else {
	                    		noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
	                    	}
	                })
				},
				// 리워드 수정하기 
				updateBenefit : function(index){
					var self = this;
					self.resetBenefit();

					self.request.cpCode = this.cpCode;
					self.request.memCode = userInfo.memCode;
					self.request.cpBenefitCode = self.benefitList[index].cpBenefitCode;
		            self.request.cpBenefitTitle = self.benefitList[index].cpBenefitTitle;
		            self.request.cpBenefitAmount = self.benefitList[index].cpBenefitAmount;
		            self.request.cpBenefitInfo = self.benefitList[index].cpBenefitInfo;
		            self.request.cpBenefitQty = self.benefitList[index].cpBenefitQty;
		            self.request.cpBenefitRemQty = self.benefitList[index].cpBenefitRemQty;
		            self.request.cpBenefitDeliveryDate = self.benefitList[index].cpBenefitDeliveryDate;
		            self.request.cpBenefitDeliveryConfirm = self.benefitList[index].cpBenefitDeliveryConfirm;
		            self.request.cpBenefitStatus = self.benefitList[index].cpBenefitStatus;
		            self.request.cpBenefitSpsQty = self.benefitList[index].cpBenefitSpsQty;

		            if (self.request.cpBenefitQty < 999999) {
		            	self.request.cpBenefitQtyLimit = 'Y';
		            } else {
		            	self.request.cpBenefitQtyLimit = 'N';
		            }

		            self.request.cpBenefitOptions = [];

		            var options = self.benefitList[index].cpBenefitOptions;
	            	for (var i = 0; i < options.length; i++) {

			            var optionList = options[i].cpBenefitOption.split(",");
						var option = 
				            {	
				            	cpBenefitOptionType : options[i].cpBenefitOptionType,
				            	cpBenefitOptionTitle : options[i].cpBenefitOptionTitle,
				            	cpBenefitCode : options[i].cpBenefitCode,
				            	cpBenefitOption : []
				            };
				        self.request.cpBenefitOptions.push(option);

				        for (var j = 0; j < optionList.length; j++) {
				        	self.request.cpBenefitOptions[i].cpBenefitOption.push(optionList[j]);
				        }
					}
					self.setRadio('refresh');
				},
				// 초기화
				resetBenefit : function() {
					this.request = {
						memCode : userInfo.memCode,
						cpCode : this.cpCode,
						cpBenefitCode : "",
			            cpBenefitTitle : "",
			            cpBenefitAmount : 0,
			            cpBenefitInfo : "",
			            cpBenefitQty : 0,
			            cpBenefitRemQty : 0,
			            cpBenefitDeliveryDate : moment().format("YYYY-MM-DD"),
			            cpBenefitDeliveryConfirm : "Y",
			            cpBenefitStatus : 1,
			            cpBenefitSpsQty : 0,
			            cpBenefitOptions : [],
			            cpBenefitQtyLimit : "N"
				    }

				    this.selectedBenefit = {};
				},
				// 다음 단계로
				save : function(next) {
					var self = this;
							
                	if(next == true) {
                		self.$emit('step-change', 4, true);
                	}
				}
			}
		}
	}
}

export default new Benefit()