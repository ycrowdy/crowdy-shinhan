class InvestDetailComment {
    component() {
        return {
            template : `
              <div>
                <div id="comment-collapse">
                    <div class="mp-body">
                    <!-- 댓글작성 & 댓글목록 -->
                    <!-- 댓글작성 -->
                    <form class="form-horizontal pt15 xs-pt10 xs-pl5 xs-pr5">
                        <div class="form-group row-mobile-n mb10">
                            <div class="col-sm-2 hidden-xs">
                                <div class="comment-object"><img v-if="userImg != ''" :src="'//' + userImg + '/ycrowdy/resize/!160x!160'" /></div>
                            </div>
                            <div class="col-sm-10">
                                <textarea rows="3" cols="25" v-model="commentSaveData.feedbackContent" name="feedbackContent" class="textarea-form-control form-control" v-validate="'required|max:500'" placeholder="모든 댓글은 크라우드펀딩 관련법에 따라 수정 및 삭제할 수 없습니다."></textarea>
                                <label class="error" v-if="errors.has('feedbackContent')" v-text="errors.first('feedbackContent')"></label>
                            </div>
                        </div>

                        <div class="form-group row-mobile-n mb35 xs-mb10 xs-pb3">
                            <div class="col-sm-2"></div>
                            <div class="col-xs-4 col-sm-2">
                                <span class="textarea_text_leng xs-mt5"><span v-text="commentSaveData.feedbackContent.length"></span> / 500</span>
                            </div>
                            <div class="col-xs-8 text-right">
                                <a href="javascript:void(0)" class="btn btn-primary" id="complete_pay" v-on:click="commentRegit('c')">댓글등록하기</a>
                            </div>
                        </div>
                    </form>
                    <!-- //댓글작성 -->
                    </div>
                </div>

                <!-- 댓글목록 -->
                <div class="comments mb30">
                    <!-- Loop -->
                    <div class="comment media" v-if="commentData.length > 0" v-for="(item, index) in commentData">
                        <div class="media-left pr20 m-pr10">
                            <div class="reply-object" href="javascript:void(0)"><img v-if="item.memShotImg != ''" :src="'//' + item.memShotImg + '/ycrowdy/resize/!160x!160'" /></div>
                        </div>
                        <div class="comment-body media-body">
                            <a href="javascript:void(0)" class="comment-author">{{ item.memName }}</a>
                            <div class="comment-content" v-html="$options.filters.commnetFilter(item.feedbackContent)">
                            </div>
                            <div class="comment-meta">
                                {{ item.wdate }}
                            </div>
                            <a class="blue-800" :href="'#'+item.feedbackIdx" role="button" data-toggle="collapse" aria-expanded="false" :aria-controls="item.feedbackIdx">답글달기</a>
                            <!-- 답글 작성 -->
                            <form :id="item.feedbackIdx" class="collapse comment-reply">
                                <div class="mb10">
                                    <textarea class="textarea-form-control form-control" v-model="commentSaveData.feedbackContentRe" v-on:click="replyClick(item.feedbackIdx)" maxlength="300" rows="3" cols="25" placeholder="모든 댓글은 크라우드펀딩 관련법에 따라 수정 및 삭제할 수 없습니다."></textarea>
                                </div>
                                <div class="text-right">
                                    <a class="btn btn-sm btn-default-outline" :href="'#'+item.feedbackIdx" role="button" data-toggle="collapse" aria-expanded="false" :aria-controls="item.feedbackIdx" v-on:click="commentReset">답글취소</a>
                                    <a href="javascript:void(0)" role="button" data-toggle="collapse" aria-expanded="false" v-on:click="commentRegit('r')" class="btn btn-sm btn-primary-outline">답글등록</a>
                                </div>
                            </form>
                            <!-- //답글 작성 -->
                            <reply :param-reply = "replyData" :param-comment-idx = "commentData[index].feedbackIdx" :reply-change="replyChange" v-if="replyRefresh"></reply>
                        </div>
                    </div>
                </div>   
                
                <div class="text-center mb40">
                    <a href="javascript:void(0)" id="comment_list_more" v-show="moreShow" v-on:click="more()" class="btn btn-primary-outline">더보기</a>
                </div>
            </div>
            `,
            props: {
                pjCode : {
                    default : '',
                },
                paramSearchCount : {
                    default : "4"
                }
            },
            data: function() {
                return {
                    commentData : [],
                    replyData : [],
                    commentParam : {
                        pjCode: this.pjCode,
                        paging : {
                            page : "1",
                            count : this.paramSearchCount
                        }
                    },
                    commentSaveData : {
                        pjCode : this.pjCode,
                        memCode : userInfo.memCode,
                        feedbackIdx : '',
                        feedbackContent : '',
                        feedbackContentRe : '',
                    },
                    moreShow : true,
                    refresh : true,
                    replyChange : 1,
                    replyRefresh : false
                }
            },
            components : {
                reply : require('./invest-reply.js').default.component(),
            },
            computed : {
                userImg : function() {
                    return userInfo.image;
                }
            },
            created: function() { 
                this.comment('regit');
            },
            methods: {
                //댓글 로드
                comment : function(data) {
                    var self = this;
                    if(_.isEqual('regit', data)) {
                        self.commentParam.paging.page = "1";
                        self.commentParam.paging.count = 4;
                    }
                    
                    axios.post('/data/view/invest/comment-list', self.commentParam)
                    .then(function (response) {

                        if(_.isEqual('regit', data)) {
                            self.commentData = response.data.rData.comment;
                            self.replyData = response.data.rData.reply;
                        } else {
                            self.commentData = _.concat(self.commentData, response.data.rData.comment);
                            self.replyData = _.concat(self.replyData, response.data.rData.reply);
                        }

                        self.replyRefresh = true;

                        if(!self.moreEnd() || response.data.rData.comment.length == 0) {
                            self.moreShow = false;
                        }
                    })
                },
                //댓글, 답글등록
                commentRegit : function(type) {
                    if(!userInfo.loginConfirm()) return;
                    var self = this;
                    
                    //답글 등록할 때
                    if(type == 'r') {
                        self.commentSaveData.feedbackContent = self.commentSaveData.feedbackContentRe;
                    } else {
                        self.commentSaveData.feedbackIdx = '';    
                    }
                    
                    $('.page-loader-more').fadeIn('')
                    axios.post('/data/member/investor/state', {memCode : userInfo.memCode})
                        .then(function (response) {
                            $('.page-loader-more').fadeOut('')
                             var data = response.data.rData;

                             //사용자 투자회원 유무, Y - 투자회원, N - 투자회원 아님
                            if(data.investor == 'N') {
                                noti.open('투자회원이어야 댓글을 등록할 수 있습니다. <br /> 설정 페이지로 이동하시겠습니까?', function() {window.open('/mypage/main?menu=5&sub-menu=2', '_self')}, true);
                                return;
                            }

                            self.$validator.validateAll()
                            .then(function(success) {
                                if(!success) return;
                                //등록
                                 axios.post('/set/save/invest/set-comment', self.commentSaveData)
                                .then(function (response) {
                                    var result = response.data;
                                    if(result.rCode == "0000") {
                                        self.replyRefresh = false;

                                        noti.open('등록되었습니다.');
                                        self.commentSaveData.feedbackContentRe = '';
                                        self.commentSaveData.feedbackContent = '';
                                    
                                        self.comment('regit');
                                    } else {
                                        noti.open(result.rMsg)
                                    }                                  
                                })
                            })
                        })
                    
                },
                //답글등록시 셋팅
                replyClick : function(data) {
                    var self = this;
                    if(!userInfo.loginConfirm()) return;
                    self.commentSaveData.feedbackIdx = data;
                },
                more : function() {
                    var self = this;
                    this.commentParam.paging.page  = _.toString(_.add(_.toNumber(this.commentParam.paging.page), 1));
                    this.comment();

                    self.replyChange++;
                },
                moreEnd : function() {
                    return (this.commentData.length % this.commentParam.paging.count == 0) ? true : false;
                },
                commentReset : function() {
                    this.commentSaveData.feedbackContentRe = '';
                }
            },
            filters : {
                commnetFilter : function(val) {
                    return val.replace( /[\n]/g, "<br/>")
                }
            }
        }
    }
}

export default new InvestDetailComment()