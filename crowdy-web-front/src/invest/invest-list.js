class InvestList {
	component() {
		return {
			template : `
			<div>
				<div class="list_wrap">
					<div class="container">
                        <div class="list_title webfont2">
                            창업 할 수 없다면<br />
                            투자하라
                        </div>
                    </div>

					<div id="list-category">
	                    <div class="list_category">
	                        <div class="container">
	                            <div class="row row-mobile">
	                                <div class="col-xs-6 col-md-3">
	                                    <order-select :options="orderTypeOptions" v-model="orderType"></order-select>
	                                </div>
	                                <div class="col-xs-6 col-md-3">
	                                    <order-select :options="searchTypeOptions" v-model="searchType"></order-select>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>

					<div class="container">
						<div class="rewards-list invest-list">
							<div class="row row-mobile">
								<div class="col-sm-6 col-md-4" v-for="(item, index) in invest">
									<figure>
										<a :href="'/i/' + item.pjAliasUrl" class="items over-box">
											<div class="items_img">
												<div class="coming-type webfont2" v-if="item.pjEndStatus == 0"><div><span>COMING SOON</span></div></div>
												<div class="badge_icon" v-if="item.pjEndStatus == 1">{{item.pjAssignPlType | assign }}</div>
												<div class="badge_icon" :class="{badge_icon_event : item.pjEndStatus == 2}" v-else-if="item.pjEndStatus == 2">성공</div>
												<img :src="'//' + item.pjCardImg + '/ycrowdy/resize/!740x!492'" class="img-responsive" alt="..." />
											</div>
											<figcaption class="rewards-caption">
												<div class="row not-space">
													<div class="col-xs-3">
														<div class="company_logo"><img :src="'//' + item.companyCiImg + '/ycrowdy/resize/!370x!246'" alt="" /></div>
													</div>
													<div class="col-xs-9">
														<div class="rewards-subject">
															<strong>{{item.pjTitle}}</strong>
															<div class="rewards-summury">{{item.companyName}}</div>
														 	<span class="btn btn-xs btn-default-outline" v-if="item.pjSecurName != null">{{item.pjSecurName}}</span>
			                                                <span class="btn btn-xs btn-default-outline" v-if="item.pjSecurName != '채권형' && item.pjStockName != null">{{item.pjStockName}}</span>
		                                                	<span class="btn btn-xs btn-primary-outline" v-if="item.pjSecurName != '채권형' && item.pjDeductionConfirm == 'Y'">소득공제</span>
														</div>
													</div>
												</div>
												<div class="row not-space">
													<div class="col-xs-9 col-sm-8">
														<span class="rewards-price"><span class="webfont2">₩</span>{{ parseInt(item.pjCurrentAmount).toLocaleString() }}</span> <span class="rewards-percent">{{item.pjRate}}%</span>
													</div>
													<div class="col-xs-3 col-sm-4 text-right">
														<span class="rewards-day" v-if="item.pjEndStatus == 1">{{endDay(item.pjStartDate, item.pjEndDate, item.pjEndStatus)}}</span>
														<span class="rewards-day" v-else>종료</span>
													</div>
												</div>

												<div class="progress">
													<div class="progress-bar" role="progressbar" :aria-valuenow="item.pjRate" aria-valuemin="0" aria-valuemax="100" :style="{width: item.pjRate + '%'}">
	                                                	<span class="sr-only">{{item.pjRate}}% 완료</span>
	                                            	</div>
												</div>

												<div class="row not-space">
													<div class="col-xs-5">
														<div class="invest-support">{{item.pjInvestorCount}}명 투자</div>
													</div>
													<div class="col-xs-7 text-right">
														<!-- <span class="invest-share"></span> -->
														<span class="invest-comment">{{ parseInt(item.pjCommentCount).toLocaleString() }}</span>
													</div>
												</div>
											</figcaption>
										</a>
									</figure>
								</div>
							</div>
						</div>

						<div v-show="moreShow" class="text-center mt75 mb10 md-mt65 md-mb10 m-mt50 m-mb20 xs-mt20 xs-mb0">
							<a href="javascript:void(0)" v-on:click="more()" class="btn btn-primary-outline col-sm-8 col-md-6 col-xs-12 col-sm-offset-2 col-md-offset-3 btn-more">프로젝트 더보기</a>
						</div>
					</div>
				</div>

				<hr class="mt0 mb0" />

                <div class="list_wrap">
                    <invest-sucess></invest-sucess>
                </div>

                <div class="list_wrap bg_gray">
                    <div class="container">
                        <div class="list_title webfont2">
                            투자 프로젝트<br />
                            사전공개
                        </div>
                        <div class="list_summury">
                            곧 여러분께 선보일 투자 프로젝트들을 미리 보여드립니다.
                        </div>

                        <pre-open :main="false" :param-pre-open-type="2"></pre-open>

                    </div>
                </div>

                <div class="list_wrap">
                    <div class="container">
                        <div class="list_title webfont2">
                            투자 관련<br />
                            최근 소식보기
                        </div>
                        <div class="list_summury">
                            투자형 크라우드펀딩의 새로운 소식을 알려드립니다!
                        </div>

                        <invest-news></invest-news>

                    </div>
                </div>

                <div class="mowhy-wrap mowhy-wrap2">
                    <div class="row not-space">
                        <div class="col-xs-12 banner-area">
                            <a href="javascript:void(0)" data-target="#irnModal" data-toggle="modal" data-dismiss="modal">
                                <div class="mowhy-title webfont2">투자위험고지</div>
                                <div class="mowhy-contents">
                                    비상장 기업에 대한 투자는 높은 기대수익만큼 손실가능성을 가지고 있습니다. <br class="hidden-xs" />
                                    투자 전에 투자위험에 대한 내용을 반드시 확인 해주세요. <br class="hidden-sm hidden-md hidden-lg" />
                                    <span class="ml5 red-800">자세히보기 <i class="fa fa-angle-right" aria-hidden="true"></i></span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
			</div>
			`,
			props : {
                paramSearchCount : {
                    default : "6"
                }
			},
			data : function() {
				return {
					invest : [],
					search : {
                        orderType : "1",
                        endType: '3',
                        searchType : "3",
                        paging : {
                            page : "1",
                            count : this.paramSearchCount
                        }
                    },
                    moreShow : true,
                    orderTypeOptions : [
	                    { id : "1", text : "펀딩금액순" },
	                    { id : "2", text : "마감임박순" },
	                    { id : "3", text : "최신순" },
	                    { id : "4", text : "목표달성률순" }
                    ],
                    searchTypeOptions : [
                        { id : "3", text : "전체" },
                        { id : "1", text : "주식형" },
                        { id : "2", text : "채권형" }
                    ]
				}
			},
			created : function() {
				this.load()
			},
			mounted : function() {
                this.scrollEvent();
            },
            components : {
                orderSelect : require('../common/select.js').default.component(),
                investSucess : require('./invest-sucess-list.js').default.component(),
                preOpen : require('../crowdy/pre-open-list.js').default.component(),
                investNews : require('./invest-main-news.js').default.component(),
            },
            computed : {
            	orderType : {
            		get : function() {
            			return this.search.orderType;
            		},
            		set : function(value) {
            			this.search.orderType = value;
            			this.search.paging.page = "1";
	                    this.invest = [];
                        this.load();
	                    
            		}
            	},
            	searchType : {
            		get : function() {
            			return this.search.searchType;
            		},
            		set : function(value) {
            			this.search.searchType = value;
            			this.search.paging.page = "1";
	                    this.invest = [];
                        this.load();
            		}
            	}
            },
			methods : {
				load : function() {
					var self = this;
					axios.post('/data/view/invest/list', this.search)
                        .then(function(response){
                            self.invest = _.concat(self.invest, response.data.rData);
                            if(response.data.rData.length != parseInt(self.search.paging.count)) {
                                self.moreShow = false;
                            }
                        })
				},
                more : function() {
                    this.search.paging.page  = _.toString(_.add(_.toNumber(this.search.paging.page), 1));
                    this.load();
                },
                endDay : function(startDate, endDate, status) {
                    var start = moment(startDate)
                    var end = moment(endDate)
					var now = moment().format("YYYY-MM-DD");
					
                    var duration = end.diff(now, 'days');
                    var nowDuraion = start.diff(now, 'days');
                    
                    if(status == 0) {
                    	return "펀딩시작 " + nowDuraion + "일전"
                    }

                    return duration > 0 ? "D - " + duration : this.endTime()
                },
                endTime : function() {
                    moment.updateLocale('en', {
                        relativeTime : {
                            hh : "%d 시간 남음",
                            mm : "%d 분 남음",
                            d : "오늘 까지",
                            h : "1 시간 남음"
                        }
                    })
                    return moment().endOf('day').add(-1, 'hours').fromNow(true)
                },
                scrollEvent : function() {
                    var scroll = $(this).scrollTop();
                    var gnb = $(".navbar-venture");
                    var category = $(".list_category");
                    var cstopMenu = $("#list-category .common_sub_top_menu");
                    var dobm = $(".detail_order_btn_mobile");

                    var listWrap = $("#list").position();
                    var listCategory = $("#list-category").position();

                    if (scroll > listWrap.top) {
                        gnb.addClass('navbar-fixed-up');
                    } else {
                        gnb.removeClass('navbar-fixed-up');
                    }

                    if (scroll > listCategory.top) {
                        $("body").addClass('fixed');
                        category.addClass('category_fixed');
                        cstopMenu.addClass('stm_fixed');
                        dobm.addClass('fixed');
                    } else {
                        $("body").removeClass('fixed');
                        category.removeClass('category_fixed');
                        cstopMenu.removeClass('stm_fixed');
                        dobm.removeClass('fixed');
                    }
                }
			},
			filters : {
				assign : function(val) {
					if(val == 'ASS001') {
						return '선착순배정'
					} else if(val == 'ASS002') {
						return '금액순배정'
					} else {
						return '기타배정'
					}
				}
			}

		}
	}
}


export default new InvestList()