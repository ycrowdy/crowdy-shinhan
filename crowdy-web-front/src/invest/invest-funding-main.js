class InvestFundingMain {
    component() {
        return {
            template : `
                <div>
                    <!-- 프로젝트 펀딩하기 - 결제정보 -->
                    <div id="list_wrap" class="common_support">
                        
                        <div class="common_sub_vi common_sub_vi_custom">
                            <div class="container_custom">
                                <div class="common_sub_title_small webfont2">{{investInfoData.pjCompanyName}}</div>
                                <div class="common_sub_title_big webfont2 mt5">{{investInfoData.pjTitle}}</div>
                            </div>
                        </div>

                        <div class="common_sub_layout">
                            <div class="container_custom">
                                <!--메뉴 -->
                                <div class="common_circle_menu">
                                    <div class="common-flex-between">
                                        <div class="common_circle_layout">
                                            <div class="common_circle_invest bgBlue">1</div>
                                            <div class="ivs-danger-step-title colorBlue1">투자위험고지</div>
                                        </div>
                                        <div class="common_circle_layout">
                                            <div class="common_circle_invest" :class="{'bgBlue' : step==2 || step==3 }">2</div>
                                            <div class="ivs-danger-step-title" :class="{'colorBlue1' : step==2 || step==3}">청약정보입력</div>
                                        </div>
                                        <div class="common_circle_layout">
                                            <div :class="{'common_circle_success' : step==1||step==2, 'common_circle_success_on' : step==3}"></div>
                                            <div class="ivs-danger-step-title" :class="{'colorBlue1' : step==3 }">청약완료</div>
                                        </div>
                                    </div>
                                    <hr class="common_hr_custom">
                                </div>
                                <!--//메뉴 -->

                                <danger v-if="step == 1 && dataConfirm" :invest-info-data="investInfoData" :code="pjCode" v-on:step-change="stepChange"></danger>
                                <limit v-if="step == 2 && dataConfirm" :invest-info-data="investInfoData" :code="pjCode" v-on:step-change="stepChange" v-on:set-idx="setIdx"></limit>
                                <!--<pay v-if="step == 3 && dataConfirm" :investor-idx="idx" :pj-url="pjAliasUrl" v-on:step-change="stepChange" v-on:set-pay-info="setPayInfo"></pay>-->
                                <confirm v-if="step == 3 && dataConfirm" :invest-info-data="investInfoData" :investor-idx="idx" v-on:move-project-detail="moveProjectDetail"></confirm>

                            </div>
                        </div>
                    </div>
                    <!-- //프로젝트 펀딩하기 - 결제정보 -->
                </div>           
            `,
            props : ['pjAliasUrl', 'investorIdx', 'moveStep', 'memberData'],
            data : function() {
                return {
                   //step : 1,
                    investInfoData : [],
                    pjCode : '',
                    param: {
                    	pjAliasUrl: this.pjAliasUrl,
                    },
                    dataConfirm : false,
                    // 투자자 인덱스
                   // idx : '',
                    // 주식 수
                    qty : 0,
                    // 증권계좌번호
                    no : 0,
                    // 증권사 이름
                    name : ''
                }
            },
            components : {
                danger : require('./invest-funding-danger.js').default.component(),
                limit : require('./invest-funding-limit.js').default.component(),
                pay : require('./invest-funding-pay.js').default.component(),
                confirm : require('./invest-funding-confirm.js').default.component()
            },
            computed : {
                step : function(){
                    //return 4;
                    return this.moveStep != '' ? this.moveStep : 1;
                },
                idx : function(){
                    return this.investorIdx != '' ? this.investorIdx : '';
                }
            },
            created : function() {
                if (this.pjAliasUrl == null) {
                    noti.open('투자 중에는 새로고침 할 수 없습니다.', function() {window.open("/invest/list", '_self');});
                }

                this.load();
            },
            methods : {
                load : function() {
                    var self = this;

                    axios.post('/data/view/invest/info', this.param)
                        .then(function (response) {
                            var result = response.data.rData;
                            if(result == null) {
                                noti.open('잘못된 주소입니다.', function() {window.open("/", '_self');});
                                return;
                            }

                            self.investInfoData = response.data.rData;

                            self.investInfoData.pjMaxStock = 0;
                            if (self.investInfoData.pjMaxAmount > 0) {
                                self.investInfoData.pjMaxStock = self.investInfoData.pjMaxAmount / self.investInfoData.pjContStock;  
                            }

                            self.pjCode = response.data.rData.pjCode;
                            self.dataConfirm = !self.dataConfirm;
                        })
                },
                stepChange : function(step) {
                    this.moveStep = step;
                },
                moveProjectDetail : function(){
                    window.open("/i/" +  this.pjAliasUrl, '_self');
                },
                setPayInfo : function(investorIdx) {
                    this.investorIdx = investorIdx;
                },
                setIdx : function(idx) {
                    this.investorIdx = idx;
                }
            }
        }
    }
}
export default new InvestFundingMain