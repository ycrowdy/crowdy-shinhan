class InvestMainList {
	component() {
		return {
			template : `
				<div>
					<div class="ing-invest-wrap">
                        <template  v-for="(item, index) in invest">
                            <a :href="'/i/' + item.pjAliasUrl" class="ing-invest-loop over-box">
                                <div class="row not-space">
                                    <div class="col-xs-4 col-sm-3 col-md-2">
                                        <div class="items_img">
                                            <div class="items_logo hidden-xs">
                                                <img :src="'//' + item.companyCiImg" alt="" />
                                            </div>
                                            <img :src="'//' + item.pjCardImg + '/ycrowdy/resize/!370x!246'" class="img-responsive" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-xs-8 col-sm-9 col-md-10">
                                        <div class="ing-invest-loop-con">
                                            <div class="row not-space">
                                                <div class="col-sm-12 col-lg-8">
                                                    <strong class="invest-subject">{{item.pjTitle}}</strong>
                                                    <div class="invest-company">
                                                        {{item.companyName}}
                                                        <span class="invest-subject-sort">{{item.pjAssignPlType | assign }}</span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-4 text-right md-text-left m-text-left xs-text-left m-mt5 hidden-xs">
                                                    <span class="btn btn-xs btn-default-outline" v-if="item.pjSecurName != null && item.pjSecurName != ''">{{item.pjSecurName}}</span>
                                                    <span class="btn btn-xs btn-default-outline" v-if="item.pjSecurName != '채권형' && item.pjStockName != null && item.pjStockName != ''">{{item.pjStockName}}</span>
                                                    <span class="btn btn-xs btn-primary-outline" v-if="item.pjSecurName != '채권형' && item.pjDeductionConfirm == 'Y'">소득공제</span>
                                                </div>
                                            </div>
                                            
                                            <div class="row not-space hidden-xs mt15 md-mt10 m-mt10">
                                                <div class="col-xs-5">
                                                    <span class="invest-price"><span class="webfont2">₩</span>{{ parseInt(item.pjCurrentAmount).toLocaleString() }}</span>
                                                    <span class="invest-percent">{{item.pjRate}}%</span>
                                                </div>
                                                <div class="col-xs-7 text-right">
                                                     <span class="invest-day">{{endDay(item.pjStartDate, item.pjEndDate)}}</span>
                                                </div>
                                            </div>
                                                
                                            <div class="progress hidden-xs">
                                                <div class="progress-bar" role="progressbar" :aria-valuenow="item.pjRate" aria-valuemin="0" aria-valuemax="100" :style="{width: item.pjRate + '%'}">
                                                    <span class="sr-only">{{item.pjRate}}% 완료</span>
                                                </div>
                                            </div>
                                                
                                            <div class="row not-space hidden-xs">
                                                <div class="col-xs-5">
                                                    <div class="invest-support">{{item.pjInvestorCount}}명 투자</div>
                                                </div>
                                                <div class="col-xs-7 text-right">
                                                    <!-- <span class="invest-share">254</span> -->
                                                    <span class="invest-comment">{{ parseInt(item.pjCommentCount).toLocaleString() }}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 hidden-sm hidden-md hidden-lg mt5">
                                        <div class="invest-price">
                                            <span class="webfont2">₩</span><span>{{ parseInt(item.pjCurrentAmount).toLocaleString() }}</span> 
                                            <span class="invest-percent">{{item.pjRate}}%</span>
                                        </div>

                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" :aria-valuenow="item.pjRate" aria-valuemin="0" aria-valuemax="100" :style="{width: item.pjRate + '%'}">
                                                <span class="sr-only">{{item.pjRate}}% 완료</span>
                                            </div>
                                        </div>

                                        <div class="row not-space">
                                            <div class="col-xs-8">
                                                <span class="btn btn-xs btn-default-outline" v-if="item.pjSecurName != null && item.pjSecurName != ''">{{item.pjSecurName}}</span>
                                                <span class="btn btn-xs btn-default-outline" v-if="item.pjSecurName != '채권형' && item.pjStockName != null && item.pjStockName != ''">{{item.pjStockName}}</span>
                                                <span class="btn btn-xs btn-primary-outline" v-if="item.pjSecurName != '채권형' && item.pjDeductionConfirm == 'Y'">소득공제</span>
                                            </div>
                                            <div class="col-xs-4 text-right">
                                                <span class="invest-day">{{endDay(item.pjStartDate, item.pjEndDate)}}</span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </a>
                        </template>
                    </div>
                    
                    <div v-show="moreShow">
                        <a href="javascript:void(0)" v-on:click="more()" class="btn btn-primary-outline col-sm-8 col-md-6 col-xs-12 col-sm-offset-2 col-md-offset-3 btn-more text-center mt50 mb20 md-mt65 md-mb0 m-mt50 m-mb20 xs-mt20 xs-mb10">프로젝트 더보기</a>
                    </div>

				</div>
			`,
            props : {
                paramOrderType : {
                    default : "1"
                },
                paramEndType : {
                    default : "0"
                },
                paramSearchCount : {
                    default : "3"
                }
            },
			data : function() {
				return  {
					invest : [],
                    search : {
                        orderType : this.paramOrderType,
                        endType: this.paramEndType,
                        paging : {
                            page : "1",
                            count : this.paramSearchCount
                        }
                    },
                    moreShow : true
				}
			},
			created : function() {
                this.load();
            },
            methods : {
                load : function() {
                    var self = this;
                    axios.post('/data/view/invest/list', this.search)
                        .then(function(response){
                            self.$emit('invest-list-cnt', response.data.rData.length);
                            self.invest = _.concat(self.invest, response.data.rData);
                            if(response.data.rData.length != parseInt(self.search.paging.count)) {
                                self.moreShow = false;
                            }
                        })
                },
                more : function() {
                    this.search.paging.page  = _.toString(_.add(_.toNumber(this.search.paging.page), 1));
                    this.load();
                },
                endDay : function(startDate, endDate) {
                    var now = moment().format("YYYY-MM-DD");
                    var start = moment(startDate)
                    var end = moment(endDate)
                    var duration = end.diff(now, 'days');

                    return duration > 0 ? "D - " + duration : this.endTime()
                },
                endTime : function() {
                    moment.updateLocale('en', {
                        relativeTime : {
                            hh : "%d 시간 남음",
                            mm : "%d 분 남음",
                            d : "오늘 까지",
                            h : "1 시간 남음"
                        }
                    })
                    return moment().endOf('day').add(-1, 'hours').fromNow(true)
                }
            },
            filters : {
                assign : function(val) {
                    if(val == 'ASS001') {
                        return '선착순배정'
                    } else if(val == 'ASS002') {
                        return '금액순배정'
                    } else {
                        return '기타배정'
                    }
                }
            }
		}
	}
}


export default new InvestMainList()