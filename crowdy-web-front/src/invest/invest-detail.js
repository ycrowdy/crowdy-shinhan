class InvestDetail {
    component() {
        return {
            template : `
            <div>
                <div>
                    <div class="detail_vi_wrap common_vi_wrap8">
                        <div class="inv_detail_vi_frame webfont2" v-if="dataConfirm">
                            <div class="detail_vi_zindex">
                                <div class="detail_vi_type">
                                    <span class="webfont2">{{pjTypeName}}</span>
                                </div>
                                <h2 class="webfont2">{{ investInfoData.pjTitle }}</h2>
                                <h4>{{ investInfoData.pjCompanyName }}</h4>
                            </div>
                        </div>
                    </div>
            
                    <div class="common_sub_top_menu common_stm_detail hidden-sm hidden-md hidden-lg" style="border-top: 1px solid #ddd;">
                        <div class="container common_stm_detail_mobile">
                            <a href="#" v-on:click="stepChange(1)" class="hash-link" :class="{'active' : step == 1}"><span class="webfont2">사업내용</span></a>
                            <!-- <a v-on:click="stepChange(2)" class="hash-link"  :class="{'active' : step == 2}"><span class="webfont2">투자보고서</span></a> -->
                            <a href="#" v-on:click="stepChange(3)" class="hash-link new_icon"  :class="{'active' : step == 3}"><span class="webfont2">의견 게시판 <em v-if="dataConfirm">{{investInfoData.pjCommentCount}}<i>New</i></em></span></a>
                        </div>
                    </div>

                    <div id="list_category" class="hidden-xs" style="border-top: 1px solid #ddd;">
                        <div class="common_sub_top_menu common_stm_detail">
                            <div class="container common_stm_detail_hide">
                                <a href="#" v-on:click="stepChange(1)" class="hash-link" :class="{'active' : step == 1}"><span class="webfont2">사업내용</span></a>
                                <!-- <a v-on:click="stepChange(2)" class="hash-link" :class="{'active' : step == 2}"><span class="webfont2">투자보고서</span></a> --> 
                                <a href="#" v-on:click="stepChange(3)" class="hash-link new_icon" :class="{'active' : step == 3}"><span class="webfont2">의견 게시판 <em v-if="dataConfirm">{{investInfoData.pjCommentCount}}<i>New</i></em></span></a>
                            </div>
                            <div class="container common_stm_detail_show">
                                <div class="row row-mobile-n">
                                    <div class="col-sm-8">
                                        <a href="#" v-on:click="stepChange(1)" class="hash-link first-child" :class="{'active' : step == 1}"><span class="webfont2">사업내용</span></a>
                                        <!-- <a v-on:click="stepChange(2)" class="hash-link" :class="{'active' : step == 2}"><span class="webfont2">투자보고서</span></a> -->
                                        <a href="#" v-on:click="stepChange(3)" class="hash-link new_icon" :class="{'active' : step == 3}"><span class="webfont2">의견 게시판 <em v-if="dataConfirm">{{investInfoData.pjCommentCount}}<i>New</i></em></span></a>
                                        <div class="dropdown">
                                            <a href="javascript:void(0)" id="dLabel" class="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="webfont2">공유하기</span> <span class="caret"></span></a>
                                            <ul class="dropdown-menu" aria-labelledby="dLabel">
                                                <li><a v-on:click="facebookShare">페이스북 공유</a></li>
                                                <!-- <li><a href="javascript:void(0)" data-target="#linkModal" data-toggle="modal" data-dismiss="modal">URL 공유</a></li> -->
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="row not-space">
                                            <div class="col-md-1"></div>
                                            <div class="col-md-11">
                                                <div class="detail_order_btn">
                                                    <button type="button" class="btn btn-block btn-lg btn-primary" v-on:click="funding" :disabled="investInfoData.pjEndStatus != 1">{{pjButtonText}}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="dinvestNavWrapXs hidden-md hidden-lg">
                        <a href="javascript:void(0)" class="dinvestNav_m_close">바로가기 창닫기</a>
                        <div class="dinvestNav">
                            <detailMenu :invest-detail-data="investDetailData" :check="check" :news-confirm="newsConfirm" :faq-confirm="faqConfirm" :file-confirm="fileConfirm"></detailMenu>
                        </div>
                    </div>

                    <a href="javascript:void(0)" class="dinvestNav_m_open hidden-md hidden-lg">바로가기 창열기</a>

                    <div class="sideOpenMask"></div>

                    <basic :invest-info-data="investInfoData" :invest-detail-data="investDetailData" :investor-status="investorStatus" :check="check" v-on:funding="funding" v-on:change-good="changeGood" v-on:facebook-share="facebookShare" v-on:file-confirm-change="fileConfirmChange" v-on:faq-confirm-change="faqConfirmChange" v-on:news-confirm-change="newsConfirmChange" :pj-code="pjCode" v-if="step == 1 && dataConfirm"></basic>

                    <div v-if="step == 3  && dataConfirm">
                        <!-- 댓글 -->
                        <div class="detail_wrap">
                            <div class="container">
                                <div class="row row-mobile-n">
                                    <div class="col-md-8">
                                        <div class="list_title list_title_none webfont2 xs-mt25">
                                            여러분의 한마디가<br />
                                            기업에게 큰 힘이 됩니다
                                        </div>
                                        <div class="list_summury list_summury_big webfont2">
                                            <span class="blue-800">{{ investInfoData.pjCommentCount }}개</span>의 댓글이 달려있습니다
                                        </div>
                                        <!-- 댓글작성 & 댓글목록 -->
                                            <comment :pj-code="pjCode" :param-search-count="4"></comment>
                                        <!-- //댓글작성 & 댓글목록 -->
                                    </div>
                                    <div class="col-md-4 hidden-xs hidden-sm">
                                        <div class="row not-space">
                                            <div class="col-md-1"></div>
                                            <div class="col-md-11">
                                                <info :invest-info-data="investInfoData" :investor-status="investorStatus" v-on:change-good="changeGood" v-on:funding="funding" v-on:facebook-share="facebookShare" v-if="buttonConfirm"></info>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- //댓글 -->
                    </div>
                    <!-- //댓글 -->

                    <div class="clearfix"></div>

                    <div class="detail_order_btn_mobile hidden-sm hidden-md hidden-lg">
                        <div class="detail_order_btn_mobile_frame">
                            <div class="detail_order_btn_mobile_left">
                                <div class="mdsns_btn_wrap">
                                    <a class="mdsns_btn" v-on:click="share">공유하기</a>
                                    <ul class="mdsns_btn_navi" :class="{'open' : shareCheck}">
                                        <li class="mdsns_facebook"><a v-on:click="changeGood">좋아요</a></li>
                                        <li class="mdsns_heart"><a v-on:click="facebookShare">페이스북 공유</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="detail_order_btn_mobile_right">
                                <div class="detail_order_btn" v-if="buttonConfirm">
                                    <button type="button" class="btn btn-lg btn-block btn-primary" v-on:click="funding" :disabled="investInfoData.pjEndStatus != 1">{{pjButtonText}}</button>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>  
                 <!-- 귀하는 현재 소득요건/전문투자자 구비서류 기간 만료 대상자입니다. -->
                <div id="basicMemberModal" class="modal fade" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body modal-order">
                                <div class="pt20 pb20">
                                    귀하는 현재 소득요건/전문투자자<br />
                                    <span class="red-800">구비서류 기간 만료 대상자</span>입니다.
                                </div>

                                <div class="modal-footer text-center">
                                    <div class="row row-mobile-n">
                                        <div class="col-xs-8 col-xs-offset-2">
                                            <div class="row row-mobile-n">
                                                <div class="col-xs-6">
                                                    <button type="button" class="btn btn-block btn-default-outline" data-dismiss="modal">투자계속진행하기</button>
                                                </div>
                                                <div class="col-xs-6">
                                                    <button type="submit" class="btn btn-block btn-primary" data-dismiss="modal">서류 업데이트 하러가기</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //귀하는 현재 소득요건/전문투자자 구비서류 기간 만료 대상자입니다. -->

                <!-- 청약외시간 안내 -->
                <div id="OutsideTime" class="modal fade" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <form class="modal-content form-horizontal" action="javascript:void(0)">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body text-center">
                                <div class="rewards-modal-head rewards-modal-head-big">
                                    <h3 class="sm">실시간 청약 & 청약취소 가능 시간</h3>
                                    <h5 class="webfont2">영업일 AM 5:00 - PM 11:00</h5>
                                </div>

                                <!-- <div class="mt20 mb30">
                                    <button type="button" class="btn btn-lg btn-primary-outline" data-dismiss="modal">청약알림신청</button>
                                </div>

                                <div class="mb30">
                                    알림 신청을 하시면 청약 가능한 시간에<br />
                                    알림톡으로 알려드립니다.
                                </div> -->
                            </div>
                        </form>
                    </div>
                </div>
                <!-- //청약외시간 안내 -->
            </div>
            `,
            props: ['pjAliasUrl'],
            data: function() {
                return {
                    step : 1,
                    dataConfirm : false,
                    investInfoData: {},
                    investDetailData : {},
                    param: {
                    	pjAliasUrl: this.pjAliasUrl,
                        memCode: userInfo.memCode
                    },
                    pjCode : '',
                    check : {
                        content : false,
                        problem : false,
                        riExplan : false,
                        service : false,
                        trend : false,
                        validate : false,
                        biz : false,
                        compare : false,
                        atPlan : false,
                        nowState : false,
                        pinState : false,
                        value : false,
                        plan : false,
                        staff : false,
                        patent : false,
                        pointSummary : false,
                        pointPoint : false,
                        pointProblem : false,
                        pointBiz : false,
                        pointMarket : false,
                        pointCompetition : false
                    },
                    newsConfirm : false,
                    faqConfirm : false,
                    fileConfirm : false,
                    buttonConfirm : false,
                    investorStatus : '',
                    investorIdx : '',
                    cancelRequest : {
                        pjInvestorIdx : '',
                        memCode : userInfo.memCode,
                        pjCode : '',
                        vsCancelType : '1'
                    },
                    shareCheck : false,
                    goodRequest : {
                        pjCode : '', 
                        memCode : userInfo.memCode,
                        goodType : '',
                        pjGoodConfirm : ''
                    }
                }
            },
            components : {
                basic : require('./invest-detail-basic.js').default.component(),
                info : require('./invest-detail-info.js').default.component(),
                detailMenu : require('./invest-detail-menu.js').default.component(),
                comment : require('./invest-detail-comment.js').default.component()
            },
            created: function() {
                // 스크롤 메뉴 체크 
                this.scrollEvent();
                // 정보 가져오기 
                this.load();  
            },
            computed : {
                pjTypeName : function() {
                    return this.investInfoData.pjStockType == 1 ? '주식형' : '채권형'
                },
                pjButtonText : function() {
                       // pjEndStatus : 펀딩 상태 확인 (승인대기:0 진행중:1 성공:2 실패:3)
                       // investorStatus : IVSS00 : 청약완료, IVSS01 : 주식배정중, IVSS02 : 배정완료, IVSS03 : 청약취소, IVSS04 : 배정탈락, IVSS05 : 투자모금실패, IVSS06 : 청약부분완료
                    if (this.investInfoData.pjEndStatus == 0 ) {
                        return '투자 대기 중';
                    } else if (this.investInfoData.pjEndStatus == 1) {
                        if(this.investorStatus == 'IVSS00') {
                            return '청약취소';
                        } else {
                            return '투자하기';    
                        }
                    } else {
                        return '투자 종료';
                    }
                },
                content : function() {
                    return this.investDetailData.onlineIr.companyContent != '' ? true : false;
                },
                problem : function() {
                    return this.investDetailData.onlineIr.companyProblem != '' ? true : false;
                },
                riExplan : function() {
                    return this.investDetailData.onlineIr.companyRiExplan != '' ? true : false;
                },
                service : function() {
                    return this.investDetailData.onlineIr.companyService != '' ? true : false;
                },
                trend : function() {
                    return this.investDetailData.onlineIr.companyTrend != '' ? true : false;
                },
                validate : function() {
                    return this.investDetailData.onlineIr.companyValidate != '' ? true : false;
                },
                biz : function() {
                    return this.investDetailData.onlineIr.companyBiz != '' ? true : false;
                },
                compare : function() {
                    return this.investDetailData.onlineIr.companyCompare != '' ? true : false;
                },
                atPlan : function() {
                    return this.investDetailData.onlineIr.companyAtPlan != '' ? true : false;
                },
                nowState : function() {
                    return this.investDetailData.onlineIr.companyNowState != '' ? true : false;
                },
                pinState : function() {
                    return this.investDetailData.onlineIr.companyPinState != '' ? true : false;
                },
                value : function() {
                    return this.investDetailData.onlineIr.companyValue != '' ? true : false;
                },
                plan : function() {
                    return this.investDetailData.onlineIr.companyPlan != '' ? true : false;
                },
                staff : function() {
                    return this.investDetailData.onlineIr.companyStaff != '' ? true : false;
                },
                patent : function() {
                    return this.investDetailData.onlineIr.companyPatent != '' ? true : false;
                },
                pointSummary : function() {
                    return this.investDetailData.point.pjSummary != '' ? true : false;
                },
                pointPoint : function() {
                    return this.investDetailData.point.pjPoint != '' ? true : false;
                },
                pointProblem : function() {
                    return this.investDetailData.point.pjProblem != '' ? true : false;
                },
                pointBiz : function() {
                    return this.investDetailData.point.pjBizModel != '' ? true : false;
                },
                pointMarket : function() {
                    return this.investDetailData.point.pjMarketAz != '' ? true : false;
                },
                pointCompetition : function() {
                    return this.investDetailData.point.pjCometition != '' ? true : false;
                }
            },
            methods: {
                load : function() {
                    var self = this;
                     axios.post('/data/view/invest/info', this.param)
                        .then(function (response) {
                            var result = response.data.rData;
                            
                            if(result == null) {
                                noti.open('잘못된 주소입니다.', function() {window.open("/", '_self');});
                                return;
                            }

                            if (result.pjStatus == '0') {
                                noti.open('비공개 처리된 프로젝트입니다.', function() {window.open("/", '_self');});
                                return;   
                            }

                            self.investInfoData = response.data.rData;

                            self.pjCode = result.pjCode;

                            // 사모 프로젝트일 경우
                            if (self.investInfoData.pjType == 2) {
                                // 로그인했는지 확인 
                                if(!userInfo.loginConfirm()) return;
                            }

                            self.getDetailData();
                            // 펀딩 가능한지 확인
                            self.getInvestorStatus();
                        })
                },
                scrollEvent : function() {

                    // 모바일 화면 : 리워드/투자 상세페이지 공유하기 버튼 열고 닫기
                    $( ".mdsns_btn" ).on( "click", function() {
                        $( ".mdsns_btn_navi" ).toggleClass( "open" );
                    });
                
                    // 모바일 화면에서 스크롤 다운 시 리워드/투자 상세페이지에서 하단에 [펀딩/청약하기] 버튼 노출
                    $(document).scroll(function () {
                        var scroll = $(this).scrollTop();
                        var gnb = $(".navbar-venture");
                        var category = $(".list_category");
                        var cstopMenu = $("#list_category .common_sub_top_menu");
                        var dobm = $(".detail_order_btn_mobile");
                        var cpl = $("#ch-plugin-launcher");


                        var listWrap = $("#list_wrap").position();
                        var listCategory = $("#list_category").position();

                        if (scroll > listWrap.top) {
                            gnb.addClass('navbar-fixed-up');
                            cpl.addClass('affix');
                        } else {
                            gnb.removeClass('navbar-fixed-up');
                            cpl.removeClass('affix');
                        }

                        if (scroll > listCategory.top) {
                            $("body").addClass('fixed');
                            category.addClass('category_fixed');
                            cstopMenu.addClass('stm_fixed');
                            dobm.addClass('fixed');
                        } else {
                            $("body").removeClass('fixed');
                            category.removeClass('category_fixed');
                            cstopMenu.removeClass('stm_fixed');
                            dobm.removeClass('fixed');
                        }
                              
                    });

                    if ( $('.navbar').length ){
                        var affixTop =  $('.navbar').offset().top;
                        $('.navbar').affix({
                            offset: {
                                top: affixTop
                            }
                        })
                    }
                },
                stepChange : function(step) {
                    this.step = step;
                },
                getDetailData : function() {
                     var self = this;
                     axios.post('/data/view/invest/detail', {pjCode : this.pjCode})
                        .then(function (response) {
                            var result = response.data.rData;
                            
                            self.investDetailData = response.data.rData;
                            self.investInfoData.pjInvestMinStock = self.investDetailData.securitiesIssue.pjInvestMinStock;
                            self.investInfoData.companyCeo = self.investDetailData.companyCeo;
                            
                            self.check.content = self.content
                            self.check.problem = self.problem
                            self.check.riExplan = self.riExplan
                            self.check.service = self.service
                            self.check.trend = self.trend
                            self.check.validate = self.validate
                            self.check.biz = self.biz
                            self.check.compare = self.compare
                            self.check.atPlan = self.atPlan
                            self.check.nowState = self.nowState
                            self.check.pinState = self.pinState
                            self.check.value = self.value
                            self.check.plan = self.plan
                            self.check.staff = self.staff
                            self.check.patent = self.patent

                            self.check.pointSummary = self.pointSummary;
                            self.check.pointPoint = self.pointPoint;
                            self.check.pointProblem = self.pointProblem;
                            self.check.pointBiz = self.pointBiz;
                            self.check.pointMarket = self.pointMarket;
                            self.check.pointCompetition = self.pointCompetition;

                            self.dataConfirm = !self.dataConfirm;
                        })
                },
                getInvestorStatus : function() {
                    var self = this;

                    // 로그인했는지 확인 
                    if(userInfo.memCode != '') {
                        axios.post('/data/invest/funding/investor-status', {pjCode : this.pjCode, memCode : userInfo.memCode})
                            .then(function (response) {
                                var result = response.data.rData;
                                if (result != null) {
                                    self.investorStatus = result.investorStatus;
                                    self.investorIdx = result.investorIdx;

                                    // 사모 프로젝트에 등록된 회원인지 확인
                                    if (result.investorPrivateConfirm == 'N') {
                                        noti.open('프로젝트에 투자할 수 없는 회원입니다.', function() {window.open('/', '_self')}, true);    
                                    }

                                }
                                self.buttonConfirm = true;
                            })
                    } else {
                        self.buttonConfirm = true;
                    }
                     
                },
                changeGood : function() {
                    // 로그인했는지 확인 
                    if(!userInfo.loginConfirm()) return;
                    this.goodRequest.goodType = "good";

                    if (this.investInfoData.pjGoodConfirm == 'Y') {
                        this.goodRequest.pjGoodConfirm =  'N'
                    } else {
                        this.goodRequest.pjGoodConfirm = 'Y'
                    }
                    this.setGoodStatus();
                },
                setGoodStatus : function() {
                    var self = this;

                    this.goodRequest.pjCode = this.pjCode;
                    $('.page-loader-more').fadeIn('')
                      axios.post('/set/save/invest/set-good', this.goodRequest)
                        .then(function(response){
                            var result = response.data;
                            if(result.rCode == "0000") {
                            $('.page-loader-more').fadeOut('');
                                if (self.goodRequest.goodType == 'good') {
                                    if (self.investInfoData.pjGoodConfirm == 'Y') {
                                        noti.open('관심 프로젝트 등록이 취소되었습니다.');
                                        self.investInfoData.pjGoodConfirm = 'N'
                                        self.investInfoData.pjGoodCount--;
                                    } else {
                                        noti.open('관심 프로젝트로 등록되었습니다. <br /> 프로젝트의 새 소식을 이메일로 전달드립니다.');
                                        self.investInfoData.pjGoodConfirm = 'Y'
                                        self.investInfoData.pjGoodCount++;
                                    }
                                } else {
                                    noti.open('공유되었습니다.');
                                }

                            } else {
                                noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
                            }   
                        })
                },
                facebookShare : function() {
                    var self = this;
                    if(!userInfo.loginConfirm()) return;
                    
                    FB.ui({
                        method: 'share',
                        display: 'popup',
                        href: encodeURI(document.location.href) + '?utm_source=facebook&utm_medium=share&utm_campaign=' + this.pjAliasUrl,
                    }, function(response){
                        // 공유를 다 안하고 팝업을 끄면 undefined 반환,
                        // 공유 성공 시 post_id를 반환함
                        // if (typeof(response) != "undefined" || response.post_id != '') {
                        //     self.goodRequest.goodType = "facebook";
                        //     self.goodRequest.pjGoodConfirm = 'Y'
                        //     self.setGoodStatus();
                        // }
                    });
                },
                fileConfirmChange : function(fileConfirm) {
                    this.fileConfirm = fileConfirm;
                },
                faqConfirmChange : function(faqConfirm) {
                    this.faqConfirm = faqConfirm;
                },
                newsConfirmChange : function(newsConfirm) {
                    this.newsConfirm = newsConfirm;
                },
                funding : function() {
                    var self = this;

                    // if (self.pjAliasUrl == 'ecomine' || self.pjAliasUrl == 'amuz' || self.pjAliasUrl == 'parkingbrother') {
                    //     noti.open("잠시동안 청약이 어렵습니다. 조금만 기다려주세요.");
                    //     return;   
                    // }
                    
                    // 로그인했는지 확인 
                    if(!userInfo.loginConfirm()) return;

                    if(this.investInfoData.memCode == userInfo.memCode) {
                        noti.open('프로젝트 진행자는 담당 프로젝트에 투자할 수 없습니다.')
                        return;
                    }

                    // if (userInfo.memCode != '1000002087' &&
                    //     userInfo.memCode != '1000004713' &&
                    //     userInfo.memCode != '1000000442' &&
                    //     userInfo.memCode != '1000000431' &&
                    //     userInfo.memCode != '1000017022' &&
                    //     userInfo.memCode != '1000006861' &&
                    //     userInfo.memCode != '1000000967' &&
                    //     userInfo.memCode != '1000053598' &&
                    //     userInfo.memCode != '1000009537' ) {
                    //     noti.open("본 프로젝트는 크라우디의 가상계좌 도입로 인해, <br /> 11월 1일부터 청약이 가능합니다.");
                    //     return;   
                    // }


                    // 청약 시간 확인 (공휴일 가능 / 일요일 불가능)
                    // var hour = moment().hour();
                    // Sunday as 0 and Saturday as 6
                    // var day = moment().day();

                    // if(hour < 5 || hour > 22 || day == 0) {
                    //     $('#OutsideTime').modal('show');
                    //     return;
                    // }

                    if(this.investorStatus == 'IVSS00') {
                        self.cancel();
                        return;
                    }

                    $('.page-loader-more').fadeIn('')
                    axios.post('/data/member/investor/state', {memCode : userInfo.memCode})
                        .then(function (response) {
                            $('.page-loader-more').fadeOut('')
                            var result = response.data.rData;

                            if(result == null) {
                                noti.open('잘못된 주소입니다.', function() {window.open("/", '_self');});
                                return;
                            }
                            
                            //사용자 투자회원 유무, Y - 투자회원, N - 투자회원 아님
                            if(result.investor == 'N') {
                                noti.open('투자회원으로 등록해야 투자할 수 있습니다. <br /> 설정 페이지로 이동하시겠습니까?', function() {window.open('/mypage/main?menu=5&sub-menu=2', '_self')}, true);
                                return;
                            }

                            //가상계좌 발급 여부, Y - 있음, N - 없음
                            if(result.virtualAccountConfirm == 'N') {
                                noti.open('투자예치금계좌를 발급받아야 투자할 수 있습니다. <br /> 설정 페이지로 이동하시겠습니까?', function() {window.open('/mypage/main?menu=5&sub-menu=2', '_self')}, true);
                                return;
                            }

                            //투자자 승인상태, MIC001 : 미승인 / MIC002 : 승인 / MIC003 : 반려
                            if(result.memIvsState != 'MIC002') {
                                //투자자 유형, MIT001 : 일반투자자 / MIT002 :  소득요건 등 적격투자자 / MIT003 : 전문투자자
                                var typeText = '';
                                if(result.memIvsType == 'MIT002') {
                                    typeText = '소득요건 등 적격투자자';
                                } else if(result.memIvsType == 'MIT003') {
                                    typeText = '전문투자자';
                                }

                                if(result.memIvsState == 'MIC001') {
                                    noti.open('[' + typeText + '] 아직 승인되지 않았습니다.');
                                    return;
                                } else if(result.memIvsState == 'MIC003') {
                                    noti.open('[' + typeText + '] 투자자승인이 반려되었습니다. 투자자유형 재승인요청을 바랍니다. 설정 페이지로 이동하시겠습니까?', function() {window.open('/mypage/main?menu=5&sub-menu=2', '_self')}, true);
                                    return;
                                }

                                // 소득요건구비자, 전문투자자 근거자료기간 체크
                                // TODO : 1. 근거자료기간 확인 
                                // TODO : 2. '투자계속진행하기' 버튼 클릭 -> 일반투자자로 전환
                                // TODO : 3. '서류 업데이트하고 투자하겠습니다.' 버튼 클릭 -> 설정 페이지로 이동
                                // if(userInfo.investorType != 'Y') {
                                //     $('#basicMemberModal').modal('show');
                                //     return;
                                // }
                            }

                            if (self.investorStatus == 'IVSS06') {
                                // 부분 청약 
                                noti.open('부분완료된 청약신청이 있습니다. <br /> 부분완료 청약신청을 하시겠습니까?', function() {window.open('/invest/funding/'+self.pjAliasUrl+'?moveStep=3&investorIdx=' + self.investorIdx, '_self')}, true);
                                return;
                            }
                            window.open("/invest/funding/" + self.pjAliasUrl + '?moveStep=1', '_self') 
                        })
                },
                cancel : function() {
                    var self = this;

                    // 청약 시간 확인 (공휴일 가능 / 일요일 불가능)
                    // var hour = moment().hour();
                    // Sunday as 0 and Saturday as 6
                    // var day = moment().day();

                    // if(hour < 5 || hour > 22 || day == 0) {
                    //     $('#OutsideTime').modal('show');
                    //     return;
                    // }

                    noti.open('청약을 취소하시겠습니까?', self.cancelModalConfirm, true);
                },
                 //청약취소 - 확인
                cancelModalConfirm : function() {
                    var self = this;

                    this.cancelRequest.pjInvestorIdx = this.investorIdx;
                    this.cancelRequest.pjCode = this.investInfoData.pjCode;

                    $('.page-loader-more').fadeIn('');
                     axios.post('/data/invest/funding/pay-cancel', this.cancelRequest)
                        .then(function (response) {
                            $('.page-loader-more').fadeOut('');
                            var result = response.data;
                            
                            if (result.rCode == '0000') {
                                noti.open('청약이 성공적으로 취소되었습니다.', function() {window.open("/i/" +  self.pjAliasUrl, '_self');});    
                            } else {
                                noti.open(result.rMsg);
                                return;
                            }
                            
                        })
                },
                //모바일에서 공유하기 버튼 클릭
                share : function() {
                    this.shareCheck = !this.shareCheck;
                },
            },
        }
    }
}

export default new InvestDetail()