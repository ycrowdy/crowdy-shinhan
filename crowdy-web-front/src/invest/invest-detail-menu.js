class InvestDetailMenu {
    component() {
        return {
            template : `
            <ul class="nav m-mb10">
                <li>
                    <a href="#dinvest1_1">기본정보</a>
                </li>
                <li>
                    <a href="#dinvest2_1">증권 발행 조건</a>
                </li>
                <li v-if="checkPoint">
                    <a href="javascript:void(0)" data-toggle="collapse" data-target="#dinvest3" aria-controls="dinvest3" aria-expanded="false" aria-label="Toggle navigation">핵심투자 노트 <span class="caret"></span></a>
                    <div class="collapse dinvestNav-collapse" id="dinvest3">
                        <ul class="nav">
                            <li v-if="check.pointSummary"><a href="#dinvest3_1">회사 3줄 요약</a></li>
                            <li v-if="check.pointPoint"><a href="#dinvest3_2">투자포인트</a></li>
                            <li v-if="check.pointProblem"><a href="#dinvest3_3">문제점 / 해결방안</a></li>
                            <li v-if="check.pointBiz"><a href="#dinvest3_4">사업모델</a></li>
                            <li v-if="check.pointMarket"><a href="#dinvest3_5">시장분석</a></li>
                            <li v-if="check.pointCompetition"><a href="#dinvest3_6">경쟁</a></li>
                        </ul>
                    </div>
                </li>
                <li class="dinvestNav_line">-</li>
                <li v-if="newsConfirm">
                    <a href="#dinvest4_1">새소식 업데이트</a>
                </li>
                <li>
                    <a href="javascript:void(0)" data-toggle="collapse" data-target="#dinvest6" aria-controls="dinvest6" aria-expanded="false" aria-label="Toggle navigation">사업내용 <span class="caret"></span></a>
                    <div class="collapse dinvestNav-collapse" id="dinvest6">
                        <ul class="nav">
                            <li v-if="check.content"><a href="#dinvest6_1">프로젝트 소개</a></li>
                            <li v-if="check.problem"><a href="#dinvest6_2" v-on:click="loginCheck">문제점 / 해결방안</a></li>
                            <li v-if="check.service"><a href="#dinvest6_3" v-on:click="loginCheck">제품/서비스 소개</a></li>
                            <li v-if="check.trend"><a href="#dinvest6_4" v-on:click="loginCheck">시장분석</a></li>
                            <li v-if="check.validate"><a href="#dinvest6_5" v-on:click="loginCheck">시장성 검증</a></li>
                            <li v-if="check.biz"><a href="#dinvest6_6" v-on:click="loginCheck">비즈니스모델</a></li>
                            <li v-if="check.compare"><a href="#dinvest6_7" v-on:click="loginCheck">경쟁자비교 / 경쟁우위</a></li>
                            <li v-if="check.atPlan"><a href="#dinvest6_8" v-on:click="loginCheck">고객획득 전략 / 실행전략</a></li>
                            <li v-if="check.riExplan"><a href="#dinvest6_9" v-on:click="loginCheck">리스크요인 및 대응전략</a></li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="javascript:void(0)" data-toggle="collapse" data-target="#dinvest7" aria-controls="dinvest7" aria-expanded="false" aria-label="Toggle navigation">재무현황 및 계획 <span class="caret"></span></a>
                    <div class="collapse dinvestNav-collapse" id="dinvest7">
                        <ul class="nav">
                            <li v-if="check.nowState"><a href="#dinvest7_1" v-on:click="loginCheck">기존재무현황</a></li>
                            <li v-if="check.pinState"><a href="#dinvest7_2" v-on:click="loginCheck">추정재무제표</a></li>
                            <li v-if="check.value"><a href="#dinvest7_3" v-on:click="loginCheck">기업가치 및 펀딩제안</a></li>
                            <li v-if="check.plan"><a href="#dinvest7_4" v-on:click="loginCheck">자금사용계획</a></li>
                        </ul>
                    </div>
                </li>
                <li v-if="check.staff">
                    <a href="#dinvest8_1" v-on:click="loginCheck">팀 소개 / 주주구성</a>
                </li>
                <li v-if="check.patent">
                    <a href="#dinvest9_1" v-on:click="loginCheck">특허 / 언론보도 / 수상내용</a>
                </li>
                <li v-if="faqConfirm">
                    <a href="#dinvest10_1">자주하는 질문</a>
                </li>
                <li v-if="fileConfirm">
                    <a href="#dinvest11_1">첨부파일 다운로드</a>
                </li>
                <li>
                    <a href="#dinvest12_1">의견 게시판</a>
                </li>
            </ul>
            `,
             props : {
                investDetailData : {
                    type : Object
                },
                check : {
                    type : Object
                },
                newsConfirm : {
                    type : Boolean
                },
                faqConfirm : {
                    type : Boolean
                },
                fileConfirm : {
                    type : Boolean
                }
            },
            data: function() {
                return {
                   
                }
            },
            components : {
            },
            created: function() {
                this.$nextTick(function() {
                    this.main();
                    this.dinvestNav();
                    this.smoothScrolldinvestNav();
                    this.scrollSpydinvestNav();
                });
            },
            computed : {
                checkPoint : function() {
                    return (this.check.pointSummary || this.check.pointPoint || this.check.pointProblem || this.check.pointBiz || this.check.pointMarket || this.check.pointCompetition) ? true : false
                }
            },
            methods: {
                loginCheck : function() {
                    if(!userInfo.loginConfirm()) return;
                },
                main : function() {
                     $('.dinvestNav_m_open').click(function() {
                        $('.dinvestNavWrapXs').animate({
                            right: "0px"
                        }, 200);

                        $('.page-wrapper').animate({
                            right: "285px"
                        }, 200);

                        $('.page-wrapper').addClass('sideOpen');
                        $('body').addClass('sideOpen');
                     });

                    if ( $('.dinvestNav_m_open').length ){
                        $('.dinvestNav_m_open').affix({
                            offset: {
                                top: 92
                            }
                        })
                    }

                    $('.dinvestNav_m_close').click(function() {
                        $('.dinvestNavWrapXs').animate({
                            right: "-285px"
                        }, 200);

                        $('.page-wrapper').animate({
                            right: "0px"
                        }, 200);

                        $('.page-wrapper').removeClass('sideOpen');
                        $('body').removeClass('sideOpen');
                    });

                    $('.sideOpenMask').click(function() {
                        $('.dinvestNavWrapXs').animate({
                            right: "-285px"
                        }, 200);

                        $('.page-wrapper').animate({
                            right: "0px"
                        }, 200);

                        $('.page-wrapper').removeClass('sideOpen');
                        $('body').removeClass('sideOpen');
                    });
                },
                dinvestNav : function() {
                    if ( $('.dinvestNavWrap').length ){
                        $('.dinvestNavWrap').affix({
                            offset: {
                                top: 1200,
                                bottom: function () {
                                    return (this.bottom = $('footer').outerHeight(!0) + 440)
                                }
                            }
                        })
                    }
                },
                smoothScrolldinvestNav : function() {

                    if ($('.dinvestNavWrap').length ) {
                        $('.dinvestNavWrap ul li').removeClass('active');

                        $('.dinvestNavWrap ul li a[data-toggle*="collapse"]').on( "click", function() {
                            $('.dinvestNavWrap ul li').removeClass('active');
                            $(this).parent('li').addClass('active');
                            $(".dinvest_wrap_contents").removeClass('active');
                            $('.dinvestNav-collapse').removeClass('in').next();
                            $(this.hash).addClass('active');
                        });

                        $('.dinvestNavWrap ul li a[href*="#"]').on('click', function () {
                            $('.jcorgFilterTextParent').removeClass('active');
                            $(this.hash).addClass('active');

                            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                                var target = $(this.hash);
                                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                                if (target.length) {
                                    $('html, body').animate({
                                      scrollTop: (target.offset().top - 70)
                                    }, 1000, "easeInOutExpo");
                                    return false
                                }
                            }
                            return false
                        })
                    }

                    if ($('.dinvestNavWrapXs').length ) {
                        $('.dinvestNavWrapXs ul li').removeClass('active');

                        $('.dinvestNavWrapXs ul li a[data-toggle*="collapse"]').on( "click", function() {
                            $('.dinvestNavWrapXs ul li').removeClass('active');
                            $(this).parent('li').addClass('active');
                            $(".dinvest_wrap_contents").removeClass('active');
                            $('.dinvestNav-collapse').removeClass('in').next();
                            $(this.hash).addClass('active');
                        });

                        $('.dinvestNavWrapXs ul li a[href*="#"]').on('click', function () {
                            $(this.hash).addClass('active');

                             $('.dinvestNavWrapXs').animate({
                                right: "-285px"
                            }, 200);

                            $('.page-wrapper').animate({
                                right: "0px"
                            }, 200);

                            $('.page-wrapper').removeClass('sideOpen');
                            $('body').removeClass('sideOpen');

                            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                                var target = $(this.hash);
                                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                                if (target.length) {
                                    $('html, body').animate({
                                      scrollTop: (target.offset().top - 70)
                                    }, 1000, "easeInOutExpo");
                                    return false
                                }
                            }
                            return false
                        })
                    }
                },
                scrollSpydinvestNav : function() {
                     if ($('.dinvestNavWrap').length ) {
                        $('body').scrollspy({
                            target: '.dinvestNavWrap',
                            offset: 90
                        })
                    }

                    if ($('.dinvestNavWrapXs').length ) {
                        $('body').scrollspy({
                            target: '.dinvestNavWrapXs',
                            offset: 90
                        })
                    }
                }
            },
        }
    }
}

export default new InvestDetailMenu()