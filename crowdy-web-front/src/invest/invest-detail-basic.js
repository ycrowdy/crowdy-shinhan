class InvestDetailBasic {
    component() {
        return {
            template : `
            <div>
                <!-- 사업내용 -->
                <div class="detail_wrap">
                    <div class="container">
                        <!-- 동영상 & 투자하기 등 -->
                        <div class="row row-mobile-n">
                            <div class="col-sm-8">
                                <!-- 동영상, 이미지 미리보기 -->
                                 <ul id="lightSlider" class="xs-mt10" v-if="mediaConfirm">
                                    <template v-for="(item, index) in mediaData">
                                        <li v-if="item.mediaType == '1'" :data-thumb="item.src">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <iframe class="embed-responsive-item" :src="item.msrc" height="538" allowfullscreen></iframe>
                                            </div>
                                        </li>
                                        <li v-else="item.mediaType == '2'" :data-thumb="item.src">
                                            <img :src="item.msrc" class="img-responsive" />
                                        </li>
                                    </template>
                                </ul>
                                <!-- //동영상, 이미지 미리보기 -->

                                <!-- 프로필 PC -->
                                <div class="detail_order_info_mobile hidden-sm hidden-md hidden-lg">
                                    <div class="media">
                                        <div class="media-left media-middle">
                                            <div class="imgava"><img :src="'//' + investInfoData.pjCardCiImg + '/ycrowdy/resize/!50x!50'" class="media-object" /></div>
                                        </div>
                                        <div class="media-body media-middle">
                                            <strong>{{investInfoData.pjCompanyName}}</strong>
                                            <div class="detail_order_info_invest">
                                                <template v-for="(item, index) in pjTags">
                                                    <a href="javascript:void(0)" target="_blank" class="mr5">{{item}}</a>
                                                </template>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- //프로필 PC -->

                                <!-- 투자 위험 고지 PC -->
                                <div class="project_per hidden-xs">
                                    <strong class="red-800">투자 위험 고지</strong>
                                    <ul>
                                        <li>비상장 기업에 대한 투자는 높은 기대수익만큼 손실 가능성을 가지고 있습니다.</li>
                                        <li>투자 전에 투자위험에 대한 내용을 꼭 확인해주세요. <br class="hidden-sm hidden-md hidden-lg" /><a href="javascript:void(0)" data-target="#irnModal" data-toggle="modal" data-dismiss="modal" class="red-800">자세히보기 <i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>
                                <!-- //투자 위험 고지 PC -->
                            </div>
                            <div class="col-sm-4">
                                <div class="row not-space">
                                    <div class="col-md-1"></div>
                                    <div class="col-md-11">
                                        <info :invest-info-data="investInfoData" :investor-status="investorStatus" v-on:change-good="changeGood" v-on:facebook-share="facebookShare" v-on:funding="funding"></info>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <hr class="hidden-xs hidden-sm" />
                        <!-- //동영상 & 투자하기 등 -->

                        <!-- 투자 상세 -->
                        <div class="row row-mobile-n mt30 m-mt20">
                            <div class="col-md-3">
                                <div class="dinvestNavWrap hidden-xs hidden-sm">
                                    <div class="dinvestNav">
                                        <detailMenu :invest-detail-data="investDetailData" :check="check" :news-confirm="newsConfirm" :faq-confirm="faqConfirm" :file-confirm="fileConfirm"></detailMenu>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="dinvest_line">
                                    <!-- 기본정보 -->
                                    <section id="dinvest1_1" class="dinvest_wrap_contents">
                                        <hr class="big-detail xs-mt0 hidden-sm hidden-md hidden-lg" />
                                        <h3 class="webfont2 xs-mt30">기본정보</h3>

                                        <div class="row not-space con-table">
                                            <div class="col-xs-3 label-style">
                                                <span>업종</span>
                                            </div>
                                            <div class="col-xs-9 col-md-6 con-style">
                                                {{investDetailData.companyBasic.companyType}}
                                            </div>
                                        </div>

                                        <div class="row not-space con-table">
                                            <div class="col-xs-3 label-style">
                                                <span>대표자명</span>
                                            </div>
                                            <div class="col-xs-9 col-md-6 con-style">
                                                {{investDetailData.companyBasic.companyCeo}}
                                            </div>
                                        </div>

                                        <div class="row not-space con-table">
                                            <div class="col-xs-3 label-style">
                                                <span>설립일자</span>
                                            </div>
                                            <div class="col-xs-9 col-md-6 con-style">
                                                {{investDetailData.companyBasic.companyIncoDate}}
                                            </div>
                                        </div>

                                        <div class="row not-space con-table">
                                            <div class="col-xs-3 label-style">
                                                <span>임직원 수</span>
                                            </div>
                                            <div class="col-xs-9 col-md-6 con-style">
                                                {{parseInt(investDetailData.companyBasic.companyStaffCount).toLocaleString()}}명
                                            </div>
                                        </div>

                                        <div class="row not-space con-table">
                                            <div class="col-xs-3 label-style">
                                                <span>회사주소</span>
                                            </div>
                                            <div class="col-xs-9 col-md-6 con-style">
                                                {{investDetailData.companyBasic.companyAddress1 + investDetailData.companyBasic.companyAddress2}}
                                            </div>
                                        </div>

                                        <div class="row not-space con-table">
                                            <div class="col-xs-3 label-style">
                                                <span>웹사이트</span>
                                            </div>
                                            <div class="col-xs-9 col-md-6 con-style">
                                                <a :href="linkUrl(investDetailData.companyBasic.companyWebSite)" target="_blank" class="grey-800">{{investDetailData.companyBasic.companyWebSite}}</a>
                                            </div>
                                        </div>

                                        <div class="row not-space con-table">
                                            <div class="col-xs-3 label-style">
                                                <span>이메일</span>
                                            </div>
                                            <div class="col-xs-9 col-md-6 con-style">
                                                <a :href="'mailto:'+investDetailData.companyBasic.companyEmail" class="grey-700">{{investDetailData.companyBasic.companyEmail}}</a>
                                            </div>
                                        </div>

                                        <div class="row not-space con-table mb30 md-mb0 m-mb0 xs-mb0">
                                            <div class="col-xs-3 label-style">
                                                <span>범죄이력</span>
                                            </div>
                                            <div class="col-xs-9 col-md-6 con-style">
                                                {{companyLawText}}
                                            </div>
                                        </div>

                                        <hr class="mt20" style="margin:0"/>
                                    </section>
                                    <!-- //기본정보 -->

                                    <!-- 증권 발행 조건 -->
                                    <section id="dinvest2_1" class="dinvest_wrap_contents">
                                        <h3 class="webfont2">증권 발행 조건</h3>
                                        
                                        <!-- 주식 --> 
                                        <template v-if="investDetailData.securitiesIssue.pjStockType == 1">

                                            <div class="sded_wrap notbond">
                                                <div class="row not-space">
                                                    <div class="col-sm-10 col-sm-offset-1">
                                                        <div class="row not-space">
                                                            <div class="col-xs-3 loop-dot child1">
                                                                <div class="loop-dot-group">
                                                                    <em><i></i></em>
                                                                    <strong>시작일</strong>
                                                                    <p>{{investDetailData.securitiesIssue.pjStartDate}}</p>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-3 loop-dot child2">
                                                                <div class="loop-dot-group">
                                                                    <em><i></i></em>
                                                                    <strong>종료일</strong>
                                                                    <p>{{investDetailData.securitiesIssue.pjEndDate}}</p>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-3 loop-dot child3">
                                                                <div class="loop-dot-group">
                                                                    <em><i></i></em>
                                                                    <strong><span class="hidden-xs">배정</span>확정/환불일</strong>
                                                                    <p>{{investDetailData.securitiesIssue.pjFixedDate}}</p>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-3 loop-dot child4">
                                                                <div class="loop-dot-group">
                                                                    <em><i></i></em>
                                                                    <strong>증권 입고일</strong>
                                                                    <p>{{investDetailData.securitiesIssue.pjWearingDate}}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row row-mobile-n">
                                            <div class="col-sm-4">
                                                <div class="sded_wrap_frame">
                                                    <table class="table table-condensed table-not-bordered table-bbt table-fixed">
                                                            <colgroup>
                                                                <col style="width:46%;">
                                                                <col style="width:54%;">
                                                            </colgroup>
                                                            <tbody>
                                                                <tr>
                                                                    <td class="pl10">청약배정순위</td>
                                                                    <td class="pl10"><strong>{{investDetailData.securitiesIssue.pjAssignPlType | assign }}</strong></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="pl10">증권구분</td>
                                                                    <td class="pl10"><strong>{{investDetailData.securitiesIssue.pjSecurName}}</strong></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="pl10">주식종류</td>
                                                                    <td class="pl10"><strong>{{investDetailData.securitiesIssue.pjStockName}}</strong></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="pl10">납입일</td>
                                                                    <td class="pl10"><strong>{{investDetailData.securitiesIssue.pjPmtDate}}</strong></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="pl10">발행예정일</td>
                                                                    <td class="pl10"><strong>{{investDetailData.securitiesIssue.pjPublishDate}}</strong></td>
                                                                </tr>
                                                            </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                           <div class="col-sm-4">
                                                <div class="sded_wrap_frame">
                                                    <table class="table table-condensed table-not-bordered table-bbt table-fixed">
                                                        <colgroup>
                                                            <col style="width:46%;">
                                                            <col style="width:54%;">
                                                        </colgroup>
                                                        <tbody>
                                                            <tr>
                                                                <td class="pl10">현재 주식수</td>
                                                                <td class="pl10"><strong>{{parseInt(investDetailData.securitiesIssue.pjCurrStock).toLocaleString()}}주</strong></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="pl10">발행예정<br />주식수</td>
                                                                <td class="pl10"><strong>{{parseInt(investDetailData.securitiesIssue.pjPublishCnt).toLocaleString()}}주</strong></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="pl10">발행 후 총<br />배정지분율</td>
                                                                <td class="pl10"><strong>{{computedPjAtAssign}}%</strong></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="sded_wrap_frame">
                                                    <table class="table table-condensed table-not-bordered table-bbt table-fixed">
                                                        <colgroup>
                                                            <col style="width:46%;">
                                                            <col style="width:54%;">
                                                        </colgroup>
                                                        <tbody>
                                                            <tr>
                                                                <td class="pl10">현재 기업 가치</td>
                                                                <td class="pl10"><strong>{{parseInt(investDetailData.securitiesIssue.pjCurrValue).toLocaleString()}}원</strong></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="pl10">주당가격</td>
                                                                <td class="pl10"><strong>{{parseInt(investDetailData.securitiesIssue.pjContStock).toLocaleString()}}원</strong></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="pl10">투자가능<br />최소주식수</td>
                                                                <td class="pl10"><strong>{{parseInt(investDetailData.securitiesIssue.pjInvestMinStock).toLocaleString()}}주</strong></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="pl10">액면가</td>
                                                                <td class="pl10"><strong>{{parseInt(investDetailData.securitiesIssue.pjFaceValue).toLocaleString()}}원</strong></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                        </template>  

                                        <!-- 채권 --> 
                                        <template v-if="investDetailData.securitiesIssue.pjStockType == 2">

                                            <div class="sded_wrap bond">
                                                <div class="row not-space">
                                                    <div class="col-sm-1"></div>
                                                    <div class="col-xs-4 col-sm-2 loop-dot child1">
                                                        <div class="loop-dot-group">
                                                            <em><i></i></em>
                                                            <strong>시작일</strong>
                                                            <p>{{investDetailData.securitiesIssue.pjStartDate}}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-4 col-sm-2 loop-dot child2">
                                                        <div class="loop-dot-group">
                                                            <em><i></i></em>
                                                            <strong>종료일</strong>
                                                            <p>{{investDetailData.securitiesIssue.pjEndDate}}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-4 col-sm-2 loop-dot child3">
                                                        <div class="loop-dot-group">
                                                            <em><i></i></em>
                                                            <strong><span class="hidden-xs">배정</span>확정/환불일</strong>
                                                            <p>{{investDetailData.securitiesIssue.pjFixedDate}}</p>
                                                        </div>
                                                    </div>
                                                    <div class="xs-mt15 col-xs-2 hidden-sm hidden-md hidden-lg"></div>
                                                    <div class="xs-mt15 col-xs-4 col-sm-2 loop-dot child4">
                                                        <div class="loop-dot-group">
                                                            <em><i></i></em>
                                                            <strong>채권발행일</strong>
                                                            <p>{{investDetailData.securitiesIssue.pjWearingDate}}</p>
                                                        </div>
                                                    </div>
                                                    <div class="xs-mt15 col-xs-4 col-sm-2 loop-dot child5">
                                                        <div class="loop-dot-group">
                                                            <em><i></i></em>
                                                            <strong>채권만기일</strong>
                                                            <p>{{investDetailData.securitiesIssue.pjBondDueDate}}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-2 col-sm-1"></div>
                                                </div>
                                            </div>

                                            <div class="row row-mobile-n">
                                                <div class="col-sm-4">
                                                    <div class="sded_wrap_frame sded_wrap_frame_bond">
                                                        <table class="table table-condensed table-not-bordered table-bbt table-fixed">
                                                            <colgroup>
                                                                <col style="width:48%;">
                                                                <col style="width:52%;">
                                                            </colgroup>
                                                            <tbody>
                                                                <tr>
                                                                    <td class="pl10">청약배정순위</td>
                                                                    <td class="pl10"><strong>{{investDetailData.securitiesIssue.pjAssignPlType | assign }}</strong></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="pl10">채권구분</td>
                                                                    <td class="pl10"><strong>{{investDetailData.securitiesIssue.pjSecurName}}</strong></td>
                                                                </tr>
                                                                 <tr>
                                                                    <td class="pl10">채권종류</td>
                                                                    <td class="pl10"><strong>{{investDetailData.securitiesIssue.pjBondType}}</strong></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="pl10">만기일</td>
                                                                    <td class="pl10"><strong>{{investDetailData.securitiesIssue.pjBondDueDate}}</strong></td>
                                                                </tr>
                                                            </tbody>    
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="sded_wrap_frame sded_wrap_frame_bond">
                                                        <table class="table table-condensed table-not-bordered table-bbt table-fixed">
                                                            <colgroup>
                                                                <col style="width:48%;">
                                                                <col style="width:52%;">
                                                            </colgroup>
                                                            <tbody>
                                                                <tr>
                                                                    <td class="pl10">연이자율</td>
                                                                    <td class="pl10"><strong>{{investDetailData.securitiesIssue.pjPaymentRate}}%</strong></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="pl10">발행예정<br />구좌수</td>
                                                                    <td class="pl10"><strong>{{parseInt(investDetailData.securitiesIssue.pjPublishCnt).toLocaleString()}}구좌</strong></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="pl10">이자지급주기</td>
                                                                    <td class="pl10"><strong>{{investDetailData.securitiesIssue.pjPaymentCycle}}일</strong></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="sded_wrap_frame sded_wrap_frame_bond">
                                                        <table class="table table-condensed table-not-bordered table-bbt table-fixed">
                                                            <colgroup>
                                                                <col style="width:48%;">
                                                                <col style="width:52%;">
                                                            </colgroup>
                                                            <tbody>
                                                                <tr>
                                                                    <td class="pl10">구좌당가격</td>
                                                                    <td class="pl10"><strong>{{parseInt(investDetailData.securitiesIssue.pjContStock).toLocaleString()}}원</strong></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="pl10">투자가능<br />최소구좌수</td>
                                                                    <td class="pl10"><strong>{{parseInt(investDetailData.securitiesIssue.pjInvestMinStock).toLocaleString()}}구좌</strong></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </template>  

                                        <ul class="con-icon con-icon-small blue-800 mb30 md-mb0 m-mb0 xs-mb0">
                                            <li>발행증권의 특이사항 : {{investDetailData.securitiesIssue.pjSignReport}}</li>
                                        </ul>
                                        
                                    </section>
                                    <!-- //증권 발행 조건 -->

                                    <template v-if="checkPoint">
                                        <hr class="big-detail mb0" />
                                        <div class="dinvest_wrap_contents">
                                            <h3 class="webfont2">핵심투자노트</h3>
                                        </div>
                                    </template>

                                    <!-- 핵심투자노트 : 회사 3줄 요약 -->
                                    <template v-if="check.pointSummary">
                                        <section id="dinvest3_1" class="dinvest_wrap_contents">
                                            <h4>회사 3줄 요약</h4>
                                            <ul class="dwc-icon mce-content-body" v-html="investDetailData.point.pjSummary"></ul>
                                        </section>
                                    </template>
                                    <!-- //핵심투자노트 : 회사 3줄 요약 -->

                                    <!-- 핵심투자노트 : 투자 포인트 -->
                                    <template v-if="check.pointPoint">
                                        <section id="dinvest3_2" class="dinvest_wrap_contents">
                                            <h4>투자 포인트</h4>
                                            <ul class="dwc-icon mce-content-body" v-html="investDetailData.point.pjPoint"></ul>
                                        </section>
                                    </template>
                                    <!-- //핵심투자노트 : 투자 포인트 -->

                                    <!-- 핵심투자노트 : 문제점 / 해결방안 -->
                                    <template v-if="check.pointProblem">
                                        <section id="dinvest3_3" class="dinvest_wrap_contents">
                                            <h4>문제점 / 해결방안</h4>
                                            <ul class="dwc-icon mce-content-body" v-html="investDetailData.point.pjProblem"></ul>
                                        </section>
                                    </template>
                                    <!-- //핵심투자노트 : 문제점 / 해결방안 -->

                                    <!-- 핵심투자노트 : 사업모델 -->
                                    <template v-if="check.pointBiz">
                                        <section id="dinvest3_4" class="dinvest_wrap_contents">
                                            <h4>사업모델</h4>
                                            <ul class="dwc-icon mce-content-body" v-html="investDetailData.point.pjBizModel"></ul>
                                        </section>
                                    </template>
                                    <!-- //핵심투자노트 : 사업모델 -->

                                    <!-- 핵심투자노트 : 시장분석 -->
                                    <template v-if="check.pointMarket">
                                        <section id="dinvest3_5" class="dinvest_wrap_contents">
                                            <h4>시장분석</h4>
                                            <ul class="dwc-icon mce-content-body" v-html="investDetailData.point.pjMarketAz"></ul>
                                        </section>
                                    </template>
                                    <!-- //핵심투자노트 : 시장분석 -->

                                    <!-- 핵심투자노트 : 경쟁 -->
                                    <template v-if="check.pointCompetition">
                                        <section id="dinvest3_6" class="dinvest_wrap_contents">
                                            <h4>경쟁</h4>
                                            <ul class="dwc-icon mce-content-body" v-html="investDetailData.point.pjCometition"></ul>
                                        </section>
                                    </template>
                                    <!-- //핵심투자노트 : 경쟁 -->

                                    <!-- 새소식 업데이트 -->
                                    <hr class="big-detail hidden-md hidden-lg" />

                                     <section id="dinvest4_1" class="dinvest_wrap_contents" v-if="!newsConfirm && (investInfoData.memCode == memCode)">
                                        <hr class="big-detail hidden-xs hidden-sm" />
                                        <h3>새소식 업데이트</h3>

                                        <!-- 새소식 추가 & 스토리 & 댓글 등 -->
                                        <div class="news_plus_btn xs-text-center mb20">
                                            <a class="hidden-xs" data-target="#newsWriteModal" data-toggle="modal" data-dismiss="modal" v-on:click="showNewsAddModal">새소식 추가하기 <i></i></a>
                                        </div>
                                        <!-- //새소식 추가 & 스토리 & 댓글 등 -->
                                    </section>

                                    <section id="dinvest4_1" class="dinvest_wrap_contents" v-if="newsConfirm">
                                        <hr class="big-detail hidden-xs hidden-sm" />
                                        <h3>새소식 업데이트</h3>

                                        <!-- 새소식 추가 & 스토리 & 댓글 등 -->
                                        <div class="news_plus_btn xs-text-center mb20" v-if="investInfoData.memCode == memCode">
                                            <a class="hidden-xs" data-target="#newsWriteModal" data-toggle="modal" data-dismiss="modal" v-on:click="showNewsAddModal">새소식 추가하기 <i></i></a>
                                        </div>
                                        <!-- //새소식 추가 & 스토리 & 댓글 등 -->

                                        <table class="table table-fixed" >
                                            <colgroup>
                                                <col style="width:100%;">
                                            </colgroup>
                                            <tbody>
                                                <tr v-for="(item, index) in newsData">
                                                    <td class="text-left pl15">
                                                        <a href="javascript:void(0)" data-toggle="modal" data-dismiss="modal" v-on:click="showNewsInfoModal(item)">
                                                            <div class="row row-mobile-n ">
                                                                <div class="col-sm-2 small grey-600">
                                                                    {{item.wdate}}
                                                                </div>
                                                                <div class="col-sm-10 grey-800">
                                                                    {{item.newsTitle}}
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                         <!-- 페이징 -->
                                       <nav class="text-center mt30 mb20">
                                            <paginate
                                                :page-count="pageCount"
                                                :class="'pagination'"
                                                :click-handler="nextPage"
                                                :force-page="forcePage"
                                                >
                                            </paginate>
                                        </nav>
                                        <!-- //페이징 -->
                                    </section>
                                    <!-- //새소식 업데이트 -->

                                    <!-- 기업개요 : 프로젝트 소개 -->
                                    <template v-if="check.content">
                                        <hr class="big-detail hidden-md hidden-lg" />
                                        <section id="dinvest6_1" class="dinvest_wrap_contents">
                                            <hr class="big-detail hidden-xs hidden-sm" />
                                            <h3>프로젝트 소개</h3>

                                            <div class="dinvest_wrap_box mce-content-body" v-html="investDetailData.onlineIr.companyContent">
                                            </div>
                                        </section>
                                    </template>
                                    <!-- //기업개요 : 프로젝트 소개 -->

                                    <template v-if="memCode == ''">
                                        <div class="text-center mb40">
                                            <a v-on:click="login" id="comment_list_more" class="btn btn-primary-outline">로그인을 하시면 투자 정보를 볼 수 있습니다.</a>
                                        </div>
                                    </template>
                                    <template v-if="memCode != ''">
                                        <!-- 사업내용 : 문제점 / 해결방안 -->
                                        <template v-if="check.problem">
                                            <hr class="big-detail hidden-md hidden-lg" />
                                            <section id="dinvest6_2" class="dinvest_wrap_contents">
                                                <hr class="big-detail hidden-xs hidden-sm" />
                                                <h3>문제점 / 해결방안</h3>

                                                <div class="dinvest_wrap_box mce-content-body" v-html="investDetailData.onlineIr.companyProblem">
                                                </div>
                                            </section>
                                        </template>
                                        <!-- //사업내용 : 문제점 / 해결방안 -->

                                        <!-- 사업내용 : 제품 / 서비스소개 -->
                                        <template v-if="check.service">
                                            <hr class="big-detail hidden-md hidden-lg" />
                                            <section id="dinvest6_3" class="dinvest_wrap_contents">
                                                <hr class="big-detail hidden-xs hidden-sm" />
                                                <h3>제품 / 서비스소개</h3>

                                                <div class="dinvest_wrap_box mce-content-body" v-html="investDetailData.onlineIr.companyService">
                                                </div>
                                            </section>
                                        </template>
                                        <!-- //사업내용 : 제품 / 서비스소개 -->

                                        <!-- 사업내용 : 시장분석 -->
                                        <template v-if="check.trend">
                                            <hr class="big-detail hidden-md hidden-lg" />
                                            <section id="dinvest6_4" class="dinvest_wrap_contents">
                                                <hr class="big-detail hidden-xs hidden-sm" />
                                                <h3>시장분석</h3>

                                                <div class="dinvest_wrap_box mce-content-body" v-html="investDetailData.onlineIr.companyTrend">
                                                </div>
                                            </section>
                                        </template>
                                        <!-- //사업내용 : 시장분석 -->

                                        <!-- 사업내용 : 시장성 검증 -->
                                        <template v-if="check.validate">
                                            <hr class="big-detail hidden-md hidden-lg" />
                                            <section id="dinvest6_5" class="dinvest_wrap_contents">
                                                <hr class="big-detail hidden-xs hidden-sm" />
                                                <h3>시장성 검증</h3>

                                                <div class="dinvest_wrap_box mce-content-body" v-html="investDetailData.onlineIr.companyValidate">
                                                </div>
                                            </section>
                                        </template>
                                        <!-- //사업내용 : 시장성 검증 -->

                                        <!-- 사업내용 : 비즈니스모델 -->
                                        <template v-if="check.biz">
                                            <hr class="big-detail hidden-md hidden-lg" />
                                            <section id="dinvest6_6" class="dinvest_wrap_contents">
                                                <hr class="big-detail hidden-xs hidden-sm" />
                                                <h3>비즈니스모델</h3>

                                                <div class="dinvest_wrap_box mce-content-body" v-html="investDetailData.onlineIr.companyBiz">
                                                </div>
                                            </section>
                                        </template>
                                        <!-- //사업내용 : 비즈니스모델 -->

                                        <!-- 사업내용 : 경쟁자비교 / 경쟁우위 -->
                                        <template v-if="check.compare">
                                            <hr class="big-detail hidden-md hidden-lg" />
                                            <section id="dinvest6_7" class="dinvest_wrap_contents">
                                                <hr class="big-detail hidden-xs hidden-sm" />
                                                <h3>경쟁자비교 / 경쟁우위</h3>

                                                <div class="dinvest_wrap_box mce-content-body" v-html="investDetailData.onlineIr.companyCompare">
                                                </div>
                                            </section>
                                        </template>
                                        <!-- //사업내용 : 경쟁자비교 / 경쟁우위 -->

                                        <!-- 사업내용 : 고객획득 전략 / 실행전략 -->
                                        <template v-if="check.atPlan">
                                            <hr class="big-detail hidden-md hidden-lg" />
                                            <section id="dinvest6_8" class="dinvest_wrap_contents">
                                                <hr class="big-detail hidden-xs hidden-sm" />
                                                <h3>고객획득 전략 / 실행전략</h3>

                                                <div class="dinvest_wrap_box mce-content-body" v-html="investDetailData.onlineIr.companyAtPlan">
                                                </div>
                                            </section>
                                        </template>
                                        <!-- //사업내용 : 고객획득 전략 / 실행전략 -->

                                        <!-- 사업내용 : 리스크요인 및 대응전략 -->
                                        <template v-if="check.riExplan">
                                            <hr class="big-detail hidden-md hidden-lg" />
                                            <section id="dinvest6_9" class="dinvest_wrap_contents">
                                                <hr class="big-detail hidden-xs hidden-sm" />
                                                <h3>리스크요인 및 대응전략</h3>

                                                <div class="dinvest_wrap_box mce-content-body" v-html="investDetailData.onlineIr.companyRiExplan">
                                                </div>
                                            </section>
                                        </template>
                                        <!-- //사업내용 : 리스크요인 및 대응전략 -->


                                        <!-- 재무현황 및 계획 : 기존재무현황 -->
                                        <template v-if="check.nowState">
                                            <hr class="big-detail hidden-md hidden-lg" />
                                            <section id="dinvest7_1" class="dinvest_wrap_contents">
                                                <hr class="big-detail hidden-xs hidden-sm" />
                                                <h3>기존재무현황</h3>

                                                <div class="dinvest_wrap_box mce-content-body" v-html="investDetailData.onlineIr.companyNowState">
                                                </div>
                                            </section>
                                        </template>
                                        <!-- //재무현황 및 계획 : 기존재무현황 -->

                                        <!-- 재무현황 및 계획 : 추정재무제표 -->
                                        <template v-if="check.pinState">
                                            <hr class="big-detail hidden-md hidden-lg" />
                                            <section id="dinvest7_2" class="dinvest_wrap_contents">
                                                <hr class="big-detail hidden-xs hidden-sm" />
                                                <h3>추정재무제표</h3>

                                                <div class="dinvest_wrap_box mce-content-body" v-html="investDetailData.onlineIr.companyPinState">
                                                </div>
                                            </section>
                                        </template>
                                        <!-- //재무현황 및 계획 : 추정재무제표 -->

                                        <!-- 재무현황 및 계획 : 기업가치 및 펀딩제안 -->
                                        <template v-if="check.value">
                                            <hr class="big-detail hidden-md hidden-lg" />
                                            <section id="dinvest7_3" class="dinvest_wrap_contents">
                                                <hr class="big-detail hidden-xs hidden-sm" />
                                                <h3>기업가치 및 펀딩제안</h3>

                                                <div class="dinvest_wrap_box mce-content-body" v-html="investDetailData.onlineIr.companyValue">
                                                </div>
                                            </section>
                                        </template>
                                        <!-- //재무현황 및 계획 : 기업가치 및 펀딩제안 -->

                                        <!-- 재무현황 및 계획 : 자금사용계획 -->
                                        <template v-if="check.plan">
                                            <hr class="big-detail hidden-md hidden-lg" />
                                            <section id="dinvest7_4" class="dinvest_wrap_contents">
                                                <hr class="big-detail hidden-xs hidden-sm" />
                                                <h3>자금사용계획</h3>

                                                <div class="dinvest_wrap_box mce-content-body" v-html="investDetailData.onlineIr.companyPlan">
                                                </div>
                                            </section>
                                        </template>
                                        <!-- //재무현황 및 계획 : 자금사용계획 -->


                                        <!-- 팀소개 / 주주구성 -->
                                        <template v-if="check.staff">
                                            <hr class="big-detail hidden-md hidden-lg" />
                                            <section id="dinvest8_1" class="dinvest_wrap_contents">
                                                <hr class="big-detail hidden-xs hidden-sm" />
                                                <h3>팀소개 / 주주구성</h3>

                                                <div class="dinvest_wrap_box mce-content-body" v-html="investDetailData.onlineIr.companyStaff">
                                                </div>
                                            </section>
                                        </template>
                                        <!-- //팀소개 / 주주구성 -->


                                        <!-- 특허 / 언론보도 / 수상내용 -->
                                        <template v-if="check.patent">
                                            <hr class="big-detail hidden-md hidden-lg" />
                                            <section id="dinvest9_1" class="dinvest_wrap_contents">
                                                <hr class="big-detail hidden-xs hidden-sm" />
                                                <h3>특허 / 언론보도 / 수상내용</h3>

                                                <div class="dinvest_wrap_box mce-content-body" v-html="investDetailData.onlineIr.companyPatent">
                                                </div>
                                            </section>
                                        </template>
                                        <!-- //특허 / 언론보도 / 수상내용 -->


                                        <!-- 자주하는 질문 -->
                                        <template v-if="faqConfirm">
                                            <hr class="big-detail hidden-md hidden-lg" />

                                            <section id="dinvest10_1" class="dinvest_wrap_contents">
                                                <hr class="big-detail hidden-xs hidden-sm" />
                                                <h3>자주하는 질문</h3>

                                                <!-- 게시물 -->
                                                <div class="panel-group faq xs-mb0" id="faq">
                                                    <!-- Loop -->
                                                    <template v-for="(item, index) in faqData">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <a class="panel-title accordion-toggle" data-toggle="collapse" data-parent="#faq" :href="'#collapse10'+index">
                                                                    {{item.faqTitle}}
                                                                </a>
                                                            </div>
                                                            <div :id="'collapse10'+index" class="panel-collapse collapse" :class="{'in' : index == 0}">
                                                                <div class="panel-body">
                                                                    <div class="faq_a" v-html="$options.filters.commentFilter(item.faqContent)"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </template>
                                                    <!-- //Loop -->
                                                </div>
                                                <!-- //게시물 -->
                                            </section>
                                        </template>
                                        <!-- //자주하는 질문 -->
                                    </template>

                                
                                    <!-- 첨부자료 다운로드 -->
                                    <template v-if="fileConfirm">
                                        <hr class="big-detail hidden-md hidden-lg" />
                                        <section id="dinvest11_1" class="dinvest_wrap_contents">
                                            <hr class="big-detail hidden-xs hidden-sm" />
                                            <h3>첨부자료 다운로드</h3>

                                            <table class="table table-stext table-fixed xs-mb0">
                                                <colgroup>
                                                    <col style="width:22%;">
                                                    <col style="width:44%;" class="hidden-xs">
                                                    <col style="width:14%;" class="hidden-md">
                                                    <col style="width:20%;">
                                                </colgroup>
                                                <thead>
                                                    <tr>
                                                        <th><span>문서제목</span></th>
                                                        <th class="hidden-xs"><span>첨부파일명</span></th>
                                                        <th class="hidden-md"><span>용량</span></th>
                                                        <th>다운로드</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <template v-for="(item, index) in fileData">
                                                        <!-- Loop -->
                                                        <tr class="text-center">
                                                            <td v-text="item.fileInfo"></td>
                                                            <td class="hidden-xs" v-text="item.fileName"></td>
                                                            <td class="hidden-md" v-text="size(item.fileSize)"></td>
                                                            <td><a :href="'//image-cr.ycrowdy.com/' + item.fileCode" target><button type="button" class="btn btn-sm btn-link"><i class="fa fa-download mr5" aria-hidden="true" style="position: relative;top:1px;"></i> 다운로드</button></a></td>
                                                        </tr>
                                                        <!-- //Loop -->
                                                    </template>
                                                </tbody>
                                            </table>
                                        </section>
                                    </template>
                                    <!-- //첨부자료 다운로드 -->

                                    <!-- 피드백 및 댓글 -->
                                    <hr class="big-detail hidden-xs hidden-md hidden-lg" />
                                    <section id="dinvest12_1" class="dinvest_wrap_contents">
                                        <hr class="big-detail hidden-xs hidden-sm" />
                                        <!-- 댓글작성 & 댓글목록 -->
                                            <comment :pj-code="pjCode" :param-search-count="4"></comment>
                                        <!-- //댓글작성 & 댓글목록 -->
                                    </section>
                                    <!-- //피드백 및 댓글 -->


                                    <!-- 주목 받는 프로젝트 -->
                                    <div class="mp-collapse">
                                        <div class="mp-body">
                                            <div class="mt50 mb80 xs-mt0 xs-mb50">
                                                <div class="notable-project-title hidden-xs pb10">
                                                    지금 주목받는 리워드 프로젝트
                                                </div>
                                                <best :param-order-type="1"></best>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- //주목 받는 프로젝트 -->
                                </div>
                            </div>
                        </div>
                        <!-- //투자 상세 -->
                    </div>
                </div>
                <!-- //사업내용 -->

                <!-- 새소식 추가는 PC에서 가능합니다. -->
                <div id="mnewsWriteModal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body modal-order">
                                새소식 추가는 PC에서 가능합니다.

                                <div class="modal-footer text-center">
                                    <div class="row row-mobile-n">
                                        <div class="col-xs-4 col-xs-offset-4">
                                            <button type="button" class="btn btn-block btn-primary webfont2" data-dismiss="modal">확인</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //새소식 추가는 PC에서 가능합니다. -->

                <!-- 새소식 추가 -->
                <div id="newsWriteModal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
                    <div class="modal-dialog modal-dialog-lg" role="document">
                        <form class="form-horizontal modal-content" id="newsWriteForm" method="get" action="javascript:void(0)">
                            <div class="modal-header modal-header-black">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <div class="modal-title">새소식 등록하기</div>
                            </div>

                            <div class="modal-body">
                                <div class="form-group row-mobile-n mt30 xs-mt10">
                                    <label for="newsSubject" class="col-xs-12 control-label">
                                        <div class="text-left">새소식 제목</div>
                                    </label>
                                    <div class="col-xs-12">
                                        <input type="text" class="form-control {validate:{required:true}}" id="newsSubject" maxlength="20" v-model="save.newsTitle" placeholder="" />
                                    </div>
                                </div>

                                <div class="form-group row-mobile-n">
                                    <label for="newsMemo" class="col-xs-12 control-label">
                                        <div class="text-left">새소식 상세내용</div>
                                    </label>
                                    <div class="col-xs-12">
                                        <div class="mb15">
                                            Enter(↵) : 문단 나눔, Shift + Enter : 줄바꿈입니다!  <br />
                                            적절한 문단/줄 바꿈만으로 멋진 프로젝트를 완성 할 수 있습니다!
                                        </div> 
                                        <news-area rows="8" cols="50" :id="'editor1'" class="form-control" :value="save.newsContent" v-model="save.newsContent"></news-area>
                                    </div>
                                </div>

                                <div class="modal-footer text-center">
                                    <div class="row row-mobile-n">
                                        <div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3">
                                            <div class="row row-mobile-n">
                                                <div class="col-xs-6">
                                                    <button type="button" class="btn btn-block btn-default-outline" data-dismiss="modal">취소</button>
                                                </div>
                                                <div class="col-xs-6">
                                                    <button type="button" class="btn btn-block btn-primary" data-dismiss="modal" v-on:click="setNews">등록</button>
                                                </div>
                                            </div>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- //새소식 추가 -->

                <!-- 새소식 보기 -->
                <div id="newsViewModal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body">
                                <div class="panel panel-bordered panel-news">
                                    <div class="panel-heading">
                                        <h4 class="panel-title webfont2">{{newsPopupData.newsTitle}}</h4>
                                    </div>
                                    <div class="list-group list-group-bordered list-group-m">
                                        <div class="list-group-item">
                                            <div class="row row-mobile-n">
                                                <div class="col-xs-7">
                                                    {{newsPopupData.wdate}}
                                                </div>
                                                <div class="col-xs-5 text-right">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-body mce-content-body" v-html="newsPopupData.newsContent"></div>
                                </div>

                                <div class="modal-footer text-center">
                                    <div class="row row-mobile-n">
                                        <div class="col-xs-4 col-xs-offset-4">
                                            <button type="button" class="btn btn-block btn-primary" data-dismiss="modal">확인</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //새소식 보기 -->
            </div>
            `,
             props : {
                investorStatus : {
                    type : String  
                },
                investInfoData : {
                    type : Object
                },
                investDetailData : {
                    type : Object
                },
                check : {
                    type : Object
                },
                pjCode : {
                    type : String
                }
            },
            data: function() {
                return {
                    memCode : userInfo.memCode,

                    //NEWS
                    param: {
                        pjCode: this.pjCode,
                        paging : {
                            page : "1",
                            count : "12"
                        }
                    },
                    save : {
                        pjCode: this.pjCode,
                        memCode : userInfo.memCode,
                        newsTitle : '',
                        newsContent : ''
                    },
                    saveBackup : {},
                    newsChange : 1,
                    newsConfirm : false,
                    newsData : [],
                    newsPopupData : {},

                    //PAGING
                    pageCount : 0,
                    forcePage : 0,

                    //FILE
                    fileConfirm : false,
                    fileData : [],
                    
                    //FAQ
                    faqConfirm : false,
                    faqData : [],
                    
                    //MEDIA 
                    mediaConfirm : false,
                    mediaData : [],
                    youtubeUrl : {},
                    vimeoConfirm : false,
                    lightSlideObj : {}
                }
            },
            components : {
                comment : require('./invest-detail-comment.js').default.component(),
                info : require('./invest-detail-info.js').default.component(),
                detailMenu : require('./invest-detail-menu.js').default.component(),
                newsArea : require('../common/text-editor.js').default.component(),
                best : require('../reward/reward-best-list.js').default.component(),
                paginate : VuejsPaginate
            },
            created: function() {
                var self = this;
                self.saveBackup = _.clone(self.save);

                // 동영상 & 이미지
                this.getMedia();

                // 새소식 목록 
                this.getProjectNews();
                
                // 자주하는 질문
                this.getProjectFaq();
                
                // 첨부파일
                this.getProjectFile();
            },
            computed : {               
                pjTags : function() {
                    if (this.investInfoData.pjTag == "") {
                        return [];
                    } else {
                        return this.investInfoData.pjTag.split(",");    
                    }
                },
                companyLawText : function() {
                    return this.investDetailData.companyBasic.companyLaw == 'Y' ? '있음' : '없음'
                },
                computedPjAtAssign : function() {
                    return parseFloat(this.investDetailData.securitiesIssue.pjAtAssign).toFixed(4);
                },
                checkPoint : function() {
                    return this.check.pointSummary || this.check.pointPoint || this.check.pointProblem || this.check.pointBiz || this.check.pointMarket || this.check.pointCompetition ? true : false; 
                }
            },
            methods: {
                getMedia : function() {
                    var self = this;
                    axios.post('/data/view/invest/media', {pjCode : this.pjCode})
                        .then(function (response) {
                            var result = response.data.rData;
                            if(result == null) {
                                noti.open('잘못된 주소입니다.', function() {window.open("/", '_self');});
                                return;
                            }
                            var result =  response.data.rData;
                            self.mediaConfirm = true;
                            var regExp = /(?:youtube(?:-nocookie)?\.com\/(?:[^\/\n\s]+\/\S+\/|(?:v|e(?:mbed)?)\/|\S*?[?&]v=)|youtu\.be\/)([a-zA-Z0-9_-]{11})/;

                            for(var i=0; i < result.length; i++) {
                                var data = result[i];
                                var mediaObject = {
                                    mediaType : '',
                                    msrc : '',
                                    src : '',                                
                                };
                                
                                mediaObject.src = data.mediaUrl;
                                //동영상
                                if(data.mediaType == '1') {
                                    mediaObject.mediaType = data.mediaType;
                                    //youtube
                                    var url = data.mediaUrl.match(regExp);
                                    if(url != null) {
                                        mediaObject.msrc = 'https://www.youtube.com/embed/' + url[1];
                                        mediaObject.src = '//img.youtube.com/vi/' + url[1] + '/default.jpg';
                                       
                                    } else { //vimeo
                                        var vimeoRegExp = /(https?:\/\/)?(www.)?(player.)?vimeo.com\/([a-z]*\/)*([0-9]{6,11})[?]?.*/;
                                        mediaObject.msrc = 'https://player.vimeo.com/video/' + data.cmscUrl.match(vimeoRegExp)[5];
                                        mediaObject.src = '//image-se.ycrowdy.com/logo/project-default.png/ycrowdy/resize/!200x!150'
                                        self.getVimeoUrl(data.mediaUrl, i);
                                    }
                                } else {
                                    mediaObject.mediaType = data.mediaType;
                                    mediaObject.src = '//' + data.mediaUrl + '/ycrowdy/resize/!200x!150';
                                    mediaObject.msrc = '//' + data.mediaUrl + '/ycrowdy/resize/!740x!417';
                                } 
                                self.mediaData.push(mediaObject);
                            }

                            self.$nextTick(function() {
                                self.lightSlide();
                            });
                        })


                },
                getVimeoUrl : function(url, index) {
                    var self = this;
                    var regExp = /(https?:\/\/)?(www.)?(player.)?vimeo.com\/([a-z]*\/)*([0-9]{6,11})[?]?.*/;
                    var vUrl = url.match(regExp);
                    axios.get('/vimeo/get/' + vUrl[5])
                       .then(function (response) {
                           var vUrl = response.data.rData.data.medium;
                           self.mediaData[index].src = vUrl
                           self.mediaData[index].confirm = true;
                           self.lightSlideObj.destroy();
                           self.lightSlide();
                    })
                },
                lightSlide : function() {
                    this.lightSlideObj = $('#lightSlider').lightSlider({
                        item:1,
                        loop:true,
                        speed:500,
                        keyPress:true,
                        gallery:true,
                        thumbItem:6,
                        thumbMargin: 10,
                        slideMargin:0,
                        responsive : [
                            {
                                breakpoint:800,
                                settings: {
                                    thumbItem:6,
                                    thumbMargin: 5
                                  }
                            },
                            {
                                breakpoint:480,
                                settings: {
                                    thumbItem:6,
                                    thumbMargin: 5
                                  }
                            }
                        ],
                        enableTouch:true,
                        enableDrag:false,
                        adaptiveHeight: true,
                        currentPagerPosition: 'middle',
                        slideEndAnimation: true,
                        onSliderLoad: function() {
                            $('#lightSlider').removeClass('cS-hidden');
                        }
                    });


                },
                // 새소식 
                getProjectNews : function() {
                    var self = this;
                    this.newsData = [];
                    this.pageCount = 0;
                    this.forcePage = 0;

                     axios.post('/data/view/invest/news-list', this.param)
                        .then(function (response) {
                            self.save = _.clone(self.saveBackup);

                            var remainder = parseInt(response.data.rData.count % self.param.paging.count);
                            var remainderCount = 0;
                            if(remainder > 0) {
                                remainderCount++;
                            }
                            self.pageCount = (parseInt(response.data.rData.count / self.param.paging.count)) + remainderCount;
                            self.newsData = response.data.rData.list;

                            if (response.data.rData.count > 0) {
                                self.newsConfirm = true; 
                                self.$emit('news-confirm-change', self.newsConfirm);        
                            }
                            
                        })
                },
                //새소식 다음 페이지
                nextPage : function(page) {
                    this.forcePage = page - 1;
                    this.param.paging.page = page;
                    this.getProjectNews();
                },
                // 자주하는 질문
                getProjectFaq : function() {
                     var self = this;
                     axios.post('/data/view/invest/faq-list', {pjCode : this.pjCode})
                        .then(function (response) {
                            self.faqData = response.data.rData;
                            if (self.faqData.length > 0) {
                                self.faqConfirm = true; 
                                self.$emit('faq-confirm-change', self.faqConfirm);     
                            }
                        })
                }, 
                // 첨부파일
                getProjectFile : function() {
                     var self = this;
                     axios.post('/data/save/invest/info/file', {pjCode : this.pjCode})
                        .then(function(response){
                            self.fileData = response.data.rData.files;
                            if (self.fileData.length > 0) {
                                self.fileConfirm = true;
                                self.$emit('file-confirm-change', self.fileConfirm);    
                            }
                        })
                },
                // 새소식 등록 모달
                showNewsAddModal : function() {
                   // $('#newsWriteModal').modal('show');
                },
                // 새소식 상세보기 모달
                showNewsInfoModal : function(data) {
                    // 로그인했는지 확인 
                    if(!userInfo.loginConfirm()) return;

                    this.newsPopupData = data;
                    $('#newsViewModal').modal('show');
                },
                // 새소식 등록
                setNews : function() {
                    var self = this;
                      axios.post('/set/save/invest/set-news', this.save)
                        .then(function(response){
                            var result = response.data;
                            if(result.rCode == "0000") {
                                noti.open('등록되었습니다.');
                                $('#newsWriteModal').modal('hide');
                                self.newsConfirm = false;
                                self.save = _.clone(self.saveBackup);
                                self.getProjectNews();
                            } else {
                                noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
                            }   
                        })
                },
                changeGood : function() {
                    this.$emit('change-good');
                },
                facebookShare : function() {
                    this.$emit('facebook-share');
                },
                login : function() {
                    userInfo.login();
                },
                funding : function() {
                    this.$emit('funding');
                },
                size : function formatFileSize(size) {
                    var units = ['Byte', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
                    var threshold = 1024;
                    const i = size === 0 ? 0 : Math.floor(Math.log(size) / Math.log(threshold));
                    return ((size / Math.pow(threshold, i)).toFixed(2) * 1) + " " + units[i];
                },
                linkUrl : function(value) {
                    var urls = value;
                    var returnUrl = '';
                    if(urls.indexOf("http://") >= 0 || urls.indexOf("https://") >= 0) {
                        returnUrl = urls;
                    } else {
                        returnUrl = 'http://' + urls;
                    }
                    return returnUrl;
                }
            },
            filters : {
                assign : function(val) {
                    if(val == 'ASS001') {
                        return '선착순배정'
                    } else if(val == 'ASS001') {
                        return '금액순배정'
                    } else {
                        return '기타배정'
                    }
                },
                 commentFilter : function(val) {
                    return val.replace( /[\n]/g, "<br/>")
                }
            }
        }
    }
}

export default new InvestDetailBasic()