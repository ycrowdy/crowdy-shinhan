class InvestSucessList {
	component() {
		return {
			template : `
				<div class="container">
					<div class="list_title webfont2">
						투자 성공 업체
					</div>
					<div class="list_summury">
						크라우디에서 투자 성공한 업체들을 보여드립니다.
					</div>

					<div class="row row-mobile">
						<div class="col-xs-6 col-sm-3 list_card_success_height_parent" v-for="(item, index) in invest">
							<a :href="'/i/' + item.pjAliasUrl" class="bbs-caption over-box list_card_success_height">
								<div class="bbs-img">
									<img :src="'//' + item.pjCardImg + '/ycrowdy/resize/!370x!246'" class="img-responsive" alt="" />
								</div>
								<div class="bbs-point">{{ parseInt(item.pjCurrentAmount).toLocaleString() }}<span class="webfont2">원</span> 펀딩 성공!</div>
								<div class="bbs-subject mt0">{{item.companyName}}</div>
								<div class="bbs-summury">{{item.pjTitle}}</div>
							</a>
						</div>
					</div>

					<div class="text-center mt40 mb20 md-mt40 md-mb0 m-mt40 m-mb20 xs-mt5 xs-mb0">
						<a href="javascript:void(0)" v-show="moreShow" v-on:click="more" class="btn btn-primary-outline">더보기</a>
					</div>
				</div>
			`,
			props : {
				paramOrderType : {
                    default : "1"
                },
                paramEndType : {
                    default : "2"
                },
                paramSearchCount : {
                    default : "4"
                }
			},
			data : function() {
				return {
					invest : [],
					search : {
                        orderType : this.paramOrderType,
                        endType: this.paramEndType,
                        paging : {
                            page : "1",
                            count : this.paramSearchCount
                        }
                    },
                    moreShow : true
				}
			},
			created : function() {
				this.load()
			},
			methods : {
				load : function() {
					var self = this;
					axios.post('/data/view/invest/list', this.search)
                        .then(function(response){
                            self.invest = _.concat(self.invest, response.data.rData);
                            if(response.data.rData.length != parseInt(self.search.paging.count)) {
                                self.moreShow = false;
                            }
                        })
				},
                more : function() {
                    this.search.paging.page  = _.toString(_.add(_.toNumber(this.search.paging.page), 1));
                    this.load();
                }
			}
		}
	}
}

export default new InvestSucessList() 