class InvestDetailInfo {
    component() {
        return {
            template : `
            <div>
                <!-- 투자하기 외 -->
                <div class="clearfix"></div>
                <div class="detail_order_wrap">
                    <!-- 펀딩시작 전 -->
                    <template v-if="investInfoData.pjEndStatus == 0">
                        <div class="funding_open">
                            <div class="funding_open_frame webfont2">
                                <div class="fo_title">펀딩시작 {{startDday}}일전</div>
                                <div class="fo_goal">목표금액 {{parseInt(investInfoData.pjTgAmount).toLocaleString()}}원</div>
                                <div class="fo_time">{{investInfoData.pjStartDate}} 오전 9시 OPEN</div>
                            </div>
                        </div>
                    </template>
                    <!-- 펀딩시작 후 --> 
                    <template v-if="investInfoData.pjEndStatus != 0">    
                        <div class="detail_order_price webfont2"><strong>₩{{parseInt(investInfoData.pjCurrentAmount).toLocaleString()}}</strong></div>

                        <div class="detail_order_goal">목표금액 {{parseInt(investInfoData.pjTgAmount).toLocaleString()}}원</div>

                        <div class="detail_order_progress">
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" :style="{'width' : investInfoData.pjRate + '%'}"><span>{{investInfoData.pjRate}}%</span></div>
                            </div>
                        </div>

                        <div class="detail_order_rp">
                            <span v-if="investInfoData.pjEndStatus == 1">남은기간 <em><strong>{{dDay(investInfoData)}}</strong></em></span>
                            <span>투자자수 <em><strong>{{investInfoData.pjInvestorCount}}</strong>명</em></span>
                        </div>

                        <div class="detail_order_btn">
                            <button type="button" class="btn btn-block btn-lg btn-primary" v-on:click="funding" :disabled="investInfoData.pjEndStatus != 1">{{pjButtonText}}</button>
                        </div>

                        <div class="detail_invest_s">
                            <span class="btn btn-block btn-default-outline" v-if="investInfoData.pjStockType == 1" >1주 : {{parseInt(investInfoData.pjContStock).toLocaleString()}}원 <br class="hidden-xs hidden-md hidden-lg" />({{parseInt(investInfoData.pjInvestMinStock).toLocaleString()}}주 이상 투자가능)</span>
                            <span class="btn btn-block btn-default-outline" v-else>1구좌 : {{parseInt(investInfoData.pjContStock).toLocaleString()}}원 <br class="hidden-xs hidden-md hidden-lg" />({{parseInt(investInfoData.pjInvestMinStock).toLocaleString()}}구좌 이상 투자가능)</span>
                            <span class="btn btn-block btn-default-outline">청약시간 <br class="hidden-xs hidden-md hidden-lg" />(AM 05:00 - PM 23:00) *일요일 제외</span>
                            <span class="btn btn-block btn-primary-outline" v-if="investInfoData.pjDeductionConfirm == 'Y'">소득공제 가능</span>
                        </div>
                    </template>

                    <!-- 투자 위험 고지 Mobile -->
                    <div class="project_per hidden-sm hidden-md hidden-lg">
                        <strong class="red-800">투자 위험 고지</strong>
                        <ul>
                            <li>비상장 기업에 대한 투자는 높은 기대수익만큼 손실 가능성을 가지고 있습니다.</li>
                            <li>투자 전에 투자위험에 대한 내용을 꼭 확인해주세요. <br class="hidden-sm hidden-md hidden-lg" /><a href="javascript:void(0)" data-target="#irnModal" data-toggle="modal" data-dismiss="modal" class="red-800">자세히보기 <i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                    <!-- //투자 위험 고지 Mobile -->

                    <!-- 프로필 PC -->
                    <div class="detail_order_info invest hidden-xs">
                        <div class="detail_order_info_head">
                            <div class="media">
                                <div class="media-left media-middle">
                                    <div class="imgava"><img :src="'//' + investInfoData.pjCardCiImg" class="media-object" /></div>
                                </div>
                                <div class="media-body media-middle">
                                    <strong>{{investInfoData.pjCompanyName}}</strong>
                                    <p>담당자 : {{investInfoData.pjResName}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="detail_order_info_btn detail_order_info_btn_link" >
                            <template v-for="(item, index) in pjTags">
                                <a href="javascript:void(0)" target="_blank">{{item}}</a>
                            </template>
                        </div>
                    </div>
                    <!-- //프로필 PC -->

                    <!-- <div class="detail_order_sns">
                        <strong>247명</strong>의 공유로, <strong>6573명</strong>에게<br />
                        본 프로젝트가 소개되었습니다.
                    </div> -->

                    <div class="detail_order_sns_btn hidden-xs pb30">
                        <ul>
                            <li><a v-on:click="facebookShare" href="javascript:void(0)"><small class="hidden-sm"></small> 페이스북 공유</a></li>
                            <!-- <li><a href="javascript:void(0)" data-target="#linkModal" data-toggle="modal" data-dismiss="modal"><small class="hidden-sm"></small> URL 공유</a></li> -->
                            <li class="heart_btn" ><a v-on:click="changeGood" href="javascript:void(0)" :class="{'active' : investInfoData.pjGoodConfirm == 'Y'}" ><small class="hidden-sm"></small> {{parseInt(investInfoData.pjGoodCount).toLocaleString()}}명의 관심투자자</a></li>
                        </ul>
                    </div>
                </div>
                <!-- //투자하기 외 -->
            </div>
            `,
            props : {
                investInfoData : {
                    type : Object
                },
                investorStatus : {
                    type : String  
                }
            },
            data: function() {
                return {

                }
            },
            computed : {
                pjTags : function() {
                    if (this.investInfoData.pjTag == "") {
                        return [];
                    } else {
                        return this.investInfoData.pjTag.split(",");    
                    }
                },
                startDday : function() {
                    return moment(moment().format("YYYY-MM-DD")).diff(this.investInfoData.pjStartDate, 'days');
                },

                pjButtonText : function() {
                    // pjEndStatus : 펀딩 상태 확인 (승인대기:0 진행중:1 성공:2 실패:3)
                    // investorStatus : IVSS00 : 청약완료, IVSS01 : 주식배정중, IVSS02 : 배정완료, IVSS03 : 청약취소, IVSS04 : 배정탈락, IVSS05 : 투자모금실패, IVSS06 : 청약부분완료
                    if (this.investInfoData.pjEndStatus == 0 ) {
                        return '투자 대기 중';
                    } else if (this.investInfoData.pjEndStatus == 1) {
                        if(this.investorStatus == 'IVSS00') {
                            return '청약취소';
                        } else {
                            return '투자하기';    
                        }
                    } else {
                        return '투자 종료';
                    }
                },
            },
            created: function() {
                
            },
            methods: {
                changeGood : function() {
                    this.$emit('change-good');
                },
                funding : function() {
                    this.$emit('funding');
                },
                facebookShare : function() {
                    this.$emit('facebook-share');
                },
                dDay : function(data) {
                    var start = moment(data.pjStartDate)
                    var end = moment(data.pjEndDate)
                    var now = moment().format("YYYY-MM-DD");
                    
                    var duration = end.diff(now, 'days');
                    var nowDuraion = start.diff(now, 'days');
                    
                    if(data.pjstatus == 0) {
                    	return "펀딩시작 " + nowDuraion + "일전"
                    }

                    return duration > 0 ? "D - " + duration : this.endTime()
                },
                endTime : function() {
                    moment.updateLocale('en', {
                        relativeTime : {
                            hh : "%d 시간 남음",
                            mm : "%d 분 남음",
                            d : "오늘 까지",
                            h : "1 시간 남음"
                        }
                    })

                    return moment().endOf('day').add(-1, 'hours').fromNow(true)
                },
            },
        }
    }
}

export default new InvestDetailInfo()