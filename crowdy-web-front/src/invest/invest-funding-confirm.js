class InvestFundingConfirm {
    component() {
        return {
            template : `
                <div>
                    <div class="ivs-confirm-layout mt30">
                        <div class="font22 mr50" style="width:400px">
                            축하드립니다 !<br/>
                            <span class="blue-800">{{memName}}</span>님의 
                            <span class="blue-800">{{investInfoData.pjCompanyName}}</span>에 대한<br/>
                            청약신청이 완료되었습니다
                        </div>

                        <div class="xs-mt40">
                            <div class="font13 colorGray1">투자프로젝트명</div>
                            <div class="font15">{{investInfoData.pjTitle}}</div>

                            <div class="font13 colorGray1 mt20">기업 명</div>
                            <div class="font15">{{investInfoData.pjCompanyName}}</div>

                            <div class="font13 colorGray1 mt20">청약 신청 일시</div>
                            <div class="font15">{{result.requestDate}}</div>

                            <div class="font13 colorGray1 mt20">청약증거금</div>
                            <div class="font15">{{pjAmount}}원</div>

                            <template v-if="investInfoData.pjStockType == '1'">
                                <div class="font13 colorGray1 mt20">
                                    청약신청 주식수
                                </div>
                                <div class="font15">
                                    {{result.investorAssignQty}}주
                                </div>
                            </template>
                            <template v-if="investInfoData.pjStockType == '2'">
                                <div class="font13 colorGray1 mt20">
                                    청약신청 구좌수
                                </div>
                                <div class="font15">
                                    {{result.investorAssignQty}}구좌
                                </div>
                            </template>

                            <div class="font13 colorGray1 mt20">증권계좌</div>
                            <div class="font15">"{{result.pjSecurName}}" - {{result.pjSecurNo}}</div>

                            <div class="font13 colorGray1 mt20">배정확정/환불일</div>
                            <div class="font15">
                               <span class="blue-800">{{investInfoData.pjFixedDate}}</span> | 프로젝트 종료 후 영업일 기준 8일 이내
                            </div>

                            <div class="font13 colorGray1 mt20">증권 입고일</div>
                            <div class="font15">
                                <span class="blue-800">{{investInfoData.pjWearingDate}}</span> | 프로젝트 종료 후 영업일 기준 21일 이내
                            </div>

                            <div class="font13 colorGray1 mt20">발행증권의 권리 및 특이사항</div>
                            <div class="font15 blue-800">배정시 금액순 배정</div>
                            <ul>
                                <li>- 큰 금액의 투자자 순으로 배정</li>
                                <li>- 동일 금액인 경우 선착순 배정</li>
                                <li>- 청약 기간 내에는 모집 금액의 100%가 초과 되어도 추후 금액순으로 배정 가능</li>
                            </ul>
                        </div>
                    </div>
                    <hr class="common_hr_custom1 mt30 hidden-xs" >
                    <a class="mypage-setting-btn mt30 mb30 xs-mt40" v-on:click="moveProjectDetail" style="max-width:500px;">확인</a>
                </div>           
            `,
            props : [
                'investInfoData',
                'investorIdx'
            ],
            data : function() {
                return {
                    memName : userInfo.name,
                    memCode : userInfo.memCode,
                    request : {
                        investorIdx : this.investorIdx
                    },
                    result : {
                        investorStatus : '',
                        requestDate : '',
                        reqResultMsg : ''
                    },
                    dataConfirm : false
                }
            },
            created : function() {
                $(window).scrollTop(0);
                this.getResult();
            },
            computed : {
                pjAmount : function() {
                    return (parseInt(this.result.investorAssignQty) * parseInt(this.investInfoData.pjContStock)).toLocaleString();
                },
                successCheck : function() {
                    return this.result.investorStatus == 'IVSS00' ? true : false;
                }
            },
            methods : {
                getResult : function() {
                    var self = this;

                    // 로그인했는지 확인 
                    //if(!userInfo.loginConfirm()) return;

                     axios.post('/data/invest/funding/result', this.request)
                        .then(function (response) {
                            var result = response.data;

                            if (result.rCode == '0000') {

                                // gtm 청약 완료 이벤트 발생
                                window.dataLayer.push({
                                    'event': 'InvestComplete'
                                });

                                self.result = result.rData;
                                self.result.requestDate = self.result.reqDt + ' ' + self.result.reqTm;
                                self.dataConfirm = true;
                            } else {
                                noti.open(result.rMsg, function() {window.open("/i/" + self.investInfoData.pjAliasUrl, '_self');});
                            }
                        })
                },
                moveProjectDetail : function() {
                    this.$emit('move-project-detail');  
                }
            }
        }
    }
}
export default new InvestFundingConfirm