class InvestReply {
    component() {
        return {
            template : `
            <div>
                <div class="comments" v-for="(item, index) in replyFilter">
                    <div class="comment media">
                        <div class="media-left pr20 m-pr10">
                            <div class="reply-object" href="javascript:void(0)"><img v-if="item.memShotImg != ''" :src="'//' + item.memShotImg + '/ycrowdy/resize/!160x!160'" /></div>
                        </div>
                        <div class="comment-body media-body">
                            <a href="javascript:void(0)" class="comment-author">{{ item.memName }}</a>
                            <div class="comment-content" v-html="$options.filters.commnetFilter(item.feedbackContent)">
                            </div>
                            <div class="comment-meta">
                                <span class="date">{{ item.wdate }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            `,
            props: [
                "paramReply", "paramCommentIdx", "replyChange"
            ],
            data : function() {
                return {
                    comment : this.paramCommentIdx,
                    reply : this.paramReply
                }
            },
            watch : {
                replyChange : function() {
                    var self = this;
                    return this.reply.filter(function(reply) {
                        return reply.feedbackIdx == self.comment;
                    })
                }
            },
            computed : {
                replyFilter : function() {
                    var self = this;
                    return this.reply.filter(function(reply) {
                        return reply.feedbackIdx == self.comment;
                    })
                }
            },
            filters : {
                commnetFilter : function(val) {
                    return val.replace( /[\n]/g, "<br/>")
                }
            }
        }
    }
}

export default new InvestReply()