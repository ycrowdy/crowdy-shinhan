class InvestFundingPay {
    component() {
        return {
            template : `
                <div>
                    <div style="display:none">

                        <!-- BankPay PC -->     
                        <form name="postForm" method="post" action="" accept-charset="euc-kr">
                            <!-- BANKPAY WEB (ADDED) -->
                            <input type="hidden" name="hd_msg_code" value="0200">
                            <input type="hidden" name="hd_msg_type" value="EFT">
                            <input type="hidden" name="service_order" value="E">
                            <input type="hidden" name="termURL" value="">
                            
                            <!-- 고정파라미터 수정하지 마세요!!!! -->
                            <!-- 아래 필드는 금융결제원 내부 사용값으로 아래의 값 그대로 유지 (임의변경 불가) -->
                            <input type="hidden" name="hd_ep_type" value="">
                            <input type="hidden" name="hd_pi" value="">

                            <!-- BANKPAY WEB (옵션 / 없어도 됨) -->
                            <input type="hidden" name="hd_ep_option" value="324"/> -->

                            <input type="hidden" name="hd_approve_no" value="15201418"><!-- 2015.12.18 변경 -->
                            <input type="hidden" name="hd_serial_no" value="0000000">
                            <input type="hidden" name="hd_firm_name" value="한국증권금융">
                            <input type="hidden" name="hd_timeout" value="300000">

                            <input type="hidden" name="sbp_service_use" value="N">
                            <input type="hidden" name="sbp_tab_first" value="N">
                            <input type="hidden" name="moid" value="">
                            
                            <!-- 중개사 셋팅파라미터 -->
                            <!-- 사용자임의식별번호(거래번호) 유니크한 키값을 셋팅 -->
                            <input type="hidden" name="tx_user_key" value="">
                            <!-- 전자지갑에 표시될 청약종목명(청약종목번호)을 세팅한다.(최대 10자리)-->
                            <input type="hidden" name="hd_item_name" value="">
                            <!-- 실제로 결제할 금액을 기입한다. 수수료 포함-->
                            <input type="hidden" name="tx_amount" value="">
                            <input type="hidden" id="pj_code" name="pj_code" value="">
                            <input type="hidden" id="pj_url" name="pj_url" value="">
                            <input type="hidden" id="previewYn" name="previewYn" value="N">
                            <input type="hidden" id="ivs_status" name="ivs_status" value="">
                            <input type="hidden" id="ivs_sub_subs_amt" name="ivs_sub_subs_amt" value="">
                            <input type="hidden" id="ivs_sub_req_seq" name="ivs_sub_req_seq" value="">
                            <input type="hidden" id="ivs_sub_idx" name="ivs_sub_idx" value="">
                            <input type="hidden" id="ivs_sub_subs_qty" name="ivs_sub_subs_qty" value="">
                            <input type="hidden" id="ivs_idx" name="ivs_idx" value="">
                        </form>
                    </div>

                   <div class="col-md-10 col-line">
                        <div class="row not-space">
                            <div class="col-md-1"></div>
                            <div class="col-md-11">
                                <div class="ifm-wrap text-center" v-if="dataConfirm">
                                    <strong><span class="blue-800">{{memName}}</span>님, 아래와 같이 <span class="blue-800">"{{info.pjCompanyName}}"</span>의<br />
                                    주식발행에 대한 청약증거금을 이체 하시겠습니까?</strong>
                                </div>

                                <hr class="big" />

                                <div class="ifm-wrap-box" v-if="dataConfirm">
                                        주당가격 {{ parseInt(info.pjContStock).toLocaleString() }}원<span class="ml10 mr10 xs-ml5 xs-mr5"> x </span><span class="blue-800">{{info.investorAssignQty}}</span>주 <i class="fa fa-long-arrow-right grey-400 ml15 mr15 xs-ml5 xs-mr5" aria-hidden="true"></i> 청약금액 <span class="blue-800">{{ parseInt(info.investorAssignQty * info.pjContStock).toLocaleString() }}원</span>
                                    <div class="ifm-wrap-info">(청약 최소 단위는 {{info.pjMinStock}}주 입니다)</div>
                                </div>

                                <hr class="big" />

                                <form class="form-horizontal pay_info_wrap_not">
                                    <div class="pay_info_wrap">
                                        <!-- Loop -->
                                        <div class="step-q step-q-non">
                                            <div class="step-q-num">
                                                1
                                            </div>
                                            <div class="step-q-memo">
                                                본 회사의 서비스거래약관을 읽고 이해하였으며 거래약관에 따라 청약 주문하는 것에 동의 합니다. <br class="hidden-md hidden-lg" /><a href="/crowdy/term?menu=1" class="blue-800" target="_blank">서비스거래약관 확인하기</a>
                                                <div class="radio-group mb0">
                                                    <label for="question1_1" class="radio-inline">
                                                        <input type="radio" name="question1" id="question1_1" value="true" v-model="question[0]"/> <span class="label"></span> <span class="label-text">예</span>
                                                    </label>
                                                    <label for="question1_2" class="radio-inline">
                                                        <input type="radio" name="question1" id="question1_2" value="false" v-model="question[0]"/> <span class="label"></span> <span class="label-text">아니오</span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <!-- //Loop -->

                                        <!-- Loop -->
                                        <div class="step-q step-q-non">
                                            <div class="step-q-num">
                                                2
                                            </div>
                                            <div class="step-q-memo">
                                                증권의 일괄예탁 등에 대하여 동의합니다. <br class="hidden-sm hidden-md hidden-lg" /><a href="/crowdy/term?menu=3" class="blue-800" target="_blank">예탁관련 필수 동의사항 확인하기</a>
                                                <div class="radio-group mb0">
                                                    <label for="question2_1" class="radio-inline">
                                                        <input type="radio" name="question2" id="question2_1" value="true" v-model="question[1]"/> <span class="label"></span> <span class="label-text">예</span>
                                                    </label>
                                                    <label for="question2_2" class="radio-inline">
                                                        <input type="radio" name="question2" id="question2_2" value="false" v-model="question[1]"/> <span class="label"></span> <span class="label-text">아니오</span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <!-- //Loop -->

                                        <!-- Loop -->
                                        <div class="step-q step-q-non">
                                            <div class="step-q-num">
                                                3
                                            </div>
                                            <div class="step-q-memo">
                                                개인정보 제 3자에 대한 정보제공에 동의합니다. <br class="hidden-sm hidden-md hidden-lg" /><a href="/crowdy/term?menu=4" class="blue-800" target="_blank">개인정보 취급방침 확인하기</a>
                                                <div class="radio-group mb0">
                                                    <label for="question3_1" class="radio-inline">
                                                        <input type="radio" name="question3" id="question3_1" value="true" v-model="question[2]"/> <span class="label"></span> <span class="label-text">예</span>
                                                    </label>
                                                    <label for="question3_2" class="radio-inline">
                                                        <input type="radio" name="question3" id="question3_2" value="false" v-model="question[2]"/> <span class="label"></span> <span class="label-text">아니오</span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <!-- //Loop -->
                                    </div>

                                    <div class="fp-items-method-info fp-items-method-info2 mt15 xs-mt20">
                                        <div class="fp-items-method-info-i fp-items-method-info-i-red"></div>
                                        <div class="fp-items-method-subject">투자를 진행하기 전 꼭 확인해주세요.</div>
                                        <p>
                                            PC<span class="ml10 mr10 xs-ml5 xs-mr5">|</span>팝업차단을 해제하셔야 원활한 투자가 가능합니다.<br />
                                            mobile<span class="ml10 mr10 xs-ml5 xs-mr5">|</span>'뱅크페이'(은행공동 계좌이체 PG서비스) <br class="hidden-sm hidden-md hidden-lg" />어플이 설치되어 있어야 합니다.<br />
                                            다음 이체 단계에서는 반드시 <strong class="red-800">'은행계좌'</strong>를 입력해주세요. <br class="hidden-sm hidden-md hidden-lg" /><span class="text-s">(이체시 증권계좌를 입력하시면 안됩니다)</span>
                                        </p>
                                    </div>

                                    <div class="row st-submit" v-if="buttonView">
                                        <template v-if="info.investorStatus != 'IVSS06'">
                                            <div class="col-sm-4 col-sm-offset-4">
                                                <button type="button" class="btn btn-block btn-primary-outline" v-on:click="goback">프로젝트 페이지로 돌아가기</button>
                                            </div>
                                        </template>
                                        <template v-if="info.investorStatus == 'IVSS06'">
                                            <div class="col-sm-4 col-sm-offset-2">
                                                <button type="button" class="btn btn-block btn-primary-outline" v-on:click="pay" id="complete_pay">청약증거금이체하기</button>
                                            </div>
                                            <div class="col-sm-4 xs-mt20">
                                                <button type="button" class="btn btn-block btn-primary-outline" v-on:click="cancel">청약취소</button>
                                            </div>
                                        </template>
                                    </div>
                                    <div class="row st-submit" v-if="!buttonView">
                                        <div class="col-sm-4 col-sm-offset-4">
                                            <button type="button" class="btn btn-block btn-primary-outline" v-on:click="goback">프로젝트 페이지로 돌아가기</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <!-- BANKPAY WEB (ADDED) -->
                    <div style="display:none; float:left;">
                        <iframe id=INVISIBLE name=INVISIBLE height="200" width="1000"></iframe> 
                    </div>

                </div>           
            `,
             props : {
                investorIdx : {
                    type : String
                },
                pjUrl : {
                    type : String
                }
            },
            data : function() {
                return {
                    memName : userInfo.name,
                    memCode : userInfo.memCode,
                    question : [false, false, false],
                    buttonView : true,
                    request : {
                        pjInvestorIdx : this.investorIdx,
                        memCode : userInfo.memCode
                    },
                    cancelRequest : {
                        pjInvestorIdx : this.investorIdx,
                        memCode : userInfo.memCode,
                        pjCode : ''
                    },
                    ksfRequest : {
                        memCode : userInfo.memCode,
                        memEmail : userInfo.email,
                        pjCode : '',
                        pjAmount : 0,
                        hdPi : '',
                        hdEpType : '',
                        moid : '',
                        pjInvestorIdx : this.investorIdx
                    },
                    info : {
                        pjCode : ''
                    },
                    dataConfirm : false,
                    mobileData : {
                        hd_ep_type : '',
                        approve_no : '15201418',
                        serial_no : '0000000',
                        firm_name : '한국증권금융',
                        sbp_service_use : 'N',
                        callbackfunc : '',
                        user_key : '',
                        hd_item_name : '',
                        amount : '',
                        //redis key
                        callbackparam1 : '',
                        //os
                        callbackparam2 : ''
                    },
                    os : '',

                    cnt : 0,
                    timeout : 600,
                    k_timeout : 1,
                    processed : false,
                    goon : false,
                    childwin : null,

                    payInfoData : {
                        pjAliasUrl : '',
                        id : '',
                        memCode : userInfo.memCode,
                        memEmail : userInfo.email,
                        pjAmount : 0,
                        pjInvestorIdx : this.investorIdx,
                        pjCode : ''
                    }
                }
            },
            created : function() {
                $(window).scrollTop(0);
                window.investFunding = this;

                // 주식/채권 구분에 따라 텍스트가 다름
                // SEC001  지분증권-주식형
                // SEC002  채무증권-채권형
                // SEC003  채무증권-CB(전환사채)형
                // SEC004  채무증권-BW(신주인수권부사채)형
                // SEC005  투자계약증권
                // SEC006  채무증권-등록발행
                // SEC007  채무증권-비등록발행
                // if (this.investInfoData.pjSecurCode == 'SEC001') {

                // } else if (this.investInfoData.pjSecurCode == '') {

                // } else {

                // }


                // PC/Mobile 구분 
                this.deviceCheck();
            },
            computed : {
                pjAmount : function() {
                    return this.info.pjSubsQty * this.info.pjContStock;
                },
                pjTotalAmount : function() {
                    return ( this.info.pjSubsQty * this.info.investInfoData.pjContStock ) + this.info.investorAssignCharge;
                },
            },
            methods : {
                deviceCheck : function() {
                    if(isMobile.apple.device) {
                        this.os = 'IOS'
                    } else if(isMobile.android.device) {
                        this.os = 'ANDROID'
                    } else {
                        this.os = 'PC'
                    }
                    
                    this.getPayData();
                },
                getPayData : function() {
                    var self = this;
                     axios.post('/data/invest/funding/info', this.request)
                        .then(function (response) {
                            var result = response.data.rData;
                            
                            if(response.data.rCode == "0000") {
                                self.info = result;
                                
                                if (self.os == 'PC') {
                                    $('form[name="postForm"]').attr('action', self.info.bankpayUrl);
                                    $('input[name="termURL"]').val(self.info.callbackUrl);
                                    $('input[name="hd_ep_type"]').val(self.info.hdEpType);
                                    $('input[name="moid"]').val(self.info.moid);
                                    $('input[name="tx_user_key"]').val(self.info.moid);
                                    $('input[name="hd_item_name"]').val(self.info.pjCompanyName);
                                    $('input[name="tx_amount"]').val((parseInt(self.info.investorAssignQty) * parseInt(self.info.pjContStock)) + parseInt(self.info.investorAssignCharge));
                                    $('input[name="pj_code"]').val(self.info.pjCode);
                                    $('input[name="pj_url"]').val(self.pjUrl);
                                    $('input[name="ivs_status"]').val(self.info.investorStatus);
                                    $('input[name="ivs_sub_subs_amt"]').val(self.info.investorAssignQty);
                                    $('input[name="ivs_sub_req_seq"]').val(self.info.investorSubReqSeq);
                                    $('input[name="ivs_sub_idx"]').val(self.info.investorSubReqSeq);
                                    $('input[name="ivs_sub_subs_qty"]').val(self.info.pjContStock);
                                    $('input[name="ivs_idx"]').val(self.investorIdx);
                                } else {
                                    self.mobileData.hd_ep_type = self.info.hdEpType;
                                    self.mobileData.hd_item_name = self.info.pjCompanyName;
                                    self.mobileData.amount = (parseInt(self.info.investorAssignQty) * parseInt(self.info.pjContStock)) + parseInt(self.info.investorAssignCharge);
                                    self.mobileData.callbackfunc = self.info.termURL;
                                    self.mobileData.user_key = self.info.moid;
                                }

                                self.dataConfirm = !self.dataConfirm;

                                self.payInfoData.pjAmount = self.mobileData.amount;
                                self.payInfoData.pjCode = self.info.pjCode;
                                self.payInfoData.id = 'i' + self.memCode + self.info.pjCode + Math.random().toString(36).substr(2, 5);
                                self.payInfoData.pjAliasUrl = self.info.pjAliasUrl;
                                self.payInfoData.moid = self.info.moid;
                            } else {
                                 noti.open(response.data.rMsg, function() {window.open("/i/" + self.pjUrl, '_self');});
                            } 

                        })

                },
                pay : function() {
                    var self = this;

                    // 청약 시간 확인 (공휴일 가능 / 일요일 불가능)
                    // var hour = moment().hour();
                    // Sunday as 0 and Saturday as 6
                    // var day = moment().day();

                    // if(hour < 5 || hour > 22 || day == 0) {
                    //     noti.open('실시간 청약 가능 시간(영업일 AM 5:00 - PM 11:00) <br/> 에만 청약이 가능합니다.', function() {window.open('/', '_self')}, true);    
                    //     return;
                    // }
                    //

                    // 1. 항목 확인
                    for (var i = 0; i < this.question.length; i++) {
                        if (!this.question[i]) {
                            noti.open((i + 1) + "번 항목에 대한 내용을 <br/> 다시 확인해 주시기 바랍니다. <br/> 모든 항목을 확인 및 동의 하셔야만 <br/> 투자가 가능합니다.");
                            return false;
                        }
                    }

                    self.buttonView = false;

                    var url = '';
                    var tempStr = "";
                    
                    var d = new Date();                   

                        // 로그인했는지 확인 
                        if(userInfo.memCode != '') {
                            axios.post('/data/invest/funding/investor-status', {pjCode : this.info.pjCode, memCode : userInfo.memCode})
                                .then(function (response) {
                                    var result = response.data;

                                    if (result.rCode == '0000') {
                                        // 청약 완료
                                        if (result.rData.investorStatus == 'IVSS06') {

                                            // 2-3. PC    
                                            if (self.os == 'PC') {

                                                if (self.getPCBrowserCheck()) {
                                                    noti.open("고객님의 브라우저에서는 청약 신청을 <br/>할 수 없습니다.<br/><br/>Internet Explorer 9이상 버전의 <br/>브라우저와 Chrome, Firefox에서만 가능합니다.");
                                                    return;
                                                }

                                                self.ksfRequest.pjCode = self.info.pjCode;
                                                self.ksfRequest.pjAmount = (parseInt(self.info.investorAssignQty) * parseInt(self.info.pjContStock)) + parseInt(self.info.investorAssignCharge);

                                                var leftPosition = (screen.width - 720) / 2 - 10;
                                                var topPosition = (screen.height - 600) / 2 - 50;

                                                self.childwin = window.open('about:blank','BANKPAYPOPUP', 'top='+topPosition+',left='+leftPosition+',height=600,width=720,status=no,dependent=no,scrollbars=no,resizable=no');
                                                document.postForm.target = 'BANKPAYPOPUP';
                                                
                                                if(document.postForm.canHaveHTML) { // detect IE
                                                    document.charset = document.postForm.acceptCharset;
                                                }
                                                document.postForm.submit();
                                                self.popupIsClosed();

                                            // 2-1. IOS
                                            } else  if(self.os == 'IOS') {

                                                if (!self.getMobileBrowserCheck()) {
                                                    noti.open("고객님의 브라우저에서는 청약 신청을 <br/>할 수 없습니다.<br/><br/> Safari 브라우저로 접속하여 서비스를 이용하여 주시길 바랍니다.  ");
                                                    return;
                                                }

                                                self.setBankpayMemberInfo();

                                                 url = "kftc-bankpay://eftpay?"
                                                 // for (data in this.mobileData) {
                                                 //    tempStr += data + "=" + this.mobileData[data] + "&";
                                                 // }
                                                 
                                                  tempStr = "hd_ep_type"  + "=" +  self.mobileData.hd_ep_type + "&" +
                                                  "approve_no"  + "=" +  self.mobileData.approve_no + "&" +
                                                  "serial_no"  + "=" +  self.mobileData.serial_no + "&" +
                                                  "firm_name"  + "=" +  self.mobileData.firm_name + "&" +
                                                  "sbp_service_use"  + "=" +  self.mobileData.sbp_service_use + "&" +
                                                  "callbackfunc"  + "=" +  self.mobileData.callbackfunc + "&" +
                                                  "user_key"  + "=" +  self.mobileData.user_key + "&" +
                                                  "hd_item_name"  + "=" +  self.mobileData.hd_item_name + "&" +
                                                  "amount"  + "=" +  self.mobileData.amount + "&" +
                                                  "callbackparam1"  + "=" +  self.payInfoData.id + "&" +
                                                  "callbackparam2"  + "=" +  "IOS" + "&" +
                                                  "callbackparam3"  + "=" +  self.pjUrl + "&" ;

                                                 var iframe = document.createElement('IFRAME'),
                                                 start;
                                                 iframe.style.display = 'none';
                                                 iframe.src = url + encodeURI(tempStr);
                                                 
                                                 window.location = url + encodeURI(tempStr);

                                                 start =+ new Date();
                                                 setTimeout(function() {
                                                        var now = +new Date();
                                                        if (now - start < 2000) {
                                                            document.location = "https://itunes.apple.com/kr/app/id398456030?mt=8";
                                                        }
                                                }, 500);
                                            
                                            // 2-2. ANDROID     
                                            } else if(self.os == 'ANDROID'){

                                                self.setBankpayMemberInfo();

                                                url = "intent://eftpay/#Intent;scheme=kftc-bankpay;package=com.kftc.bankpay.android;";

                                                // for (data in this.mobileData) {
                                                //     tempStr += "S." + data + "=" + this.mobileData[data] + ";";
                                                //  }

                                                 tempStr = "S." + "hd_ep_type"  + "=" +  self.mobileData.hd_ep_type + ";" +
                                                  "S." + "approve_no"  + "=" +  self.mobileData.approve_no + ";" +
                                                  "S." + "serial_no"  + "=" +  self.mobileData.serial_no + ";" +
                                                  "S." + "firm_name"  + "=" +  self.mobileData.firm_name + ";" +
                                                  "S." + "sbp_service_use"  + "=" +  self.mobileData.sbp_service_use + ";" +
                                                  "S." + "callbackfunc"  + "=" +  self.mobileData.callbackfunc + ";" +
                                                  "S." + "user_key"  + "=" +  self.mobileData.user_key + ";" +
                                                  "S." + "hd_item_name"  + "=" +  self.mobileData.hd_item_name + ";" +
                                                  "S." + "amount"  + "=" +  self.mobileData.amount + ";" +
                                                  "S." + "callbackparam1"  + "=" +  self.payInfoData.id + ";" +
                                                  "S." + "callbackparam2"  + "=" +  "ANDROID" + ";" +
                                                  "S." + "callbackparam3"  + "=" +  self.pjUrl + ";" ;

                                               // tempStr += "S.method=post;";
                                                location.href = url + encodeURI(tempStr) + "end";
                                            }

                                        } else if (result.rData.investorStatus == 'IVSS00') {
                                            noti.open('이미 청약을 완료하셨습니다. <br/>[마이페이지]메뉴에서 청약 내역을 상세히 확인하실 수 있습니다.', function() {window.open("/i/" +  self.info.pjAliasUrl, '_self');});
                                            return;
                                        } else {
                                            noti.open('청약을 할 수 없는 상태입니다. <br/> 다시 시도해주세요.', function() {window.open("/i/" +  self.info.pjAliasUrl, '_self');});
                                            return;
                                        }
                                    } else {
                                        noti.open(result.rMsg, function() {window.open("/i/" + self.info.pjAliasUrl, '_self');});
                                    }
                                })
                        }
                },
                getInvestorStatus : function() {
                    var self = this;

                    // 로그인했는지 확인 
                    if(userInfo.memCode != '') {
                        axios.post('/data/invest/funding/investor-status', {pjCode : this.info.pjCode, memCode : userInfo.memCode})
                            .then(function (response) {
                                var result = response.data;
                                if (result.rCode == '0000') {
                                    // 청약 완료
                                    if (result.rData.investorStatus == 'IVSS00') {
                                        return false;
                                    } else {
                                        return true;
                                    }

                                } else {
                                    return false;
                                }
                            })
                    }

                },

                setBankpayMemberInfo : function() {
                    axios.post('/invest/funding/set/pay-info', this.payInfoData);                  
                },
                getMobileBrowserCheck : function() {
                    var parser = new UAParser();
                    var result = parser.getResult();
                    
                   if(isMobile.apple.device) {
                        // if(result.browser.name == 'Mobile Safari') {
                        //     return true;
                        // } else {
                        //     return true;
                        // }
                        return true;
                    } else {
                        return false;   
                    }
                },
                // TODO : 그대로 써도 되는지 모르겠음
                getPCBrowserCheck : function() {
                    var word;
                    var version = "N/A";
                    var agent = navigator.userAgent.toLowerCase();
                    var name = navigator.appName;

                    // OS check
                    if (agent.indexOf("mac") > 0) {
                        noti.open('청약은 Window PC와 모바일에서만 가능합니다.');
                        return;
                    }

                    // IE old version ( IE 10 or Lower ) 
                    if (name == "Microsoft Internet Explorer")
                        word = "msie ";
                    else {
                        // IE 11 
                        if (agent.search("trident") > -1)
                            word = "trident/.*rv:";
                        // Microsoft Edge  
                        else if (agent.search("edge/") > -1)
                            word = "edge/";
                    }
                    var reg = new RegExp(word + "([0-9]{1,})(\\.{0,}[0-9]{0,1})");
                    if (reg.exec(agent) != null)
                        version = RegExp.$1 + RegExp.$2;

                    if (version != "N/A") {
                        if (9 > parseInt(version, 10)) {
                            noti.open('고객님은 Internet Explorer 8 버전 이하의 브라우저를 사용하고 계십니다.\n크라우디는 Internet Explorer 9 버전 이상 및 Chrome, Firefox 브라우저에 최적화되어 있습니다.\n이에 고객님의 브라우저로는 서비스 이용이 원활하지 않을 수 있습니다.\nInternet Explorer 버전을 업그레이드 하시거나, Chrome, Firefox 브라우저로 접속하여 서비스를 이용하여 주시길 바랍니다.');
                            return;
                        }
                    }
                },
                popupIsClosed : function() {
                    if(this.childwin) {
                        // if(this.childwin.closed) {
                        //     if( !this.goon ) {
                        //         if( !this.processed ) {
                        //             this.processed = true;
                        //             self.setTimeout(this.popupIsClosed(), 2000);
                        //         }// else popError("인증처리 중 문제가 발생하였습니다.(1)");
                        //     }
                        // } else {
                        //     this.cnt++;
                        //     if(this.cnt > this.timeout) {
                        //         this.popError("작업시간이 초과되었습니다.");
                        //     } else {
                        //         self.setTimeout(this.popupIsClosed(), 1000);
                        //     }
                        // }
                    } else if ( this.childwin == null ) {
                        this.cnt++;
                        if ( this.cnt > this.k_timeout ) {
                            this.popError("팝업창이 차단되었습니다. 팝업 차단을 해제해 주십시오.");
                        } else {
                            self.setTimeout(this.popupIsClosed(), 1000);
                        }
                    } else {
                        this.popError("인증처리 중 문제가 발생하였습니다.");
                    }
                },
                popError : function(text){
                    if( this.childwin ) {
                        this.childwin.close();
                    }
                    //alert(text);
                    noti.open(text);
                },    
                bankpayResult : function(hd_pi, hd_ep_type) {
                    var self = this;
                   
                    this.childwin.close();
                    this.ksfRequest.hdPi = hd_pi;
                    this.ksfRequest.hdEpType = hd_ep_type;
                    this.ksfRequest.moid = this.info.moid;

                    if (this.ksfRequest.hdPi == '' || this.ksfRequest.hdPi == null) {
                        noti.open("인증실패. 인증결과를 넘겨주지 않았습니다.", function() {window.open("/i/" +  self.pjUrl, '_self');});
                    }

                    $('.page-loader-more').fadeIn('');
                    axios.post('/data/invest/funding/pay', this.ksfRequest)
                        .then(function (response) {
                            $('.page-loader-more').fadeOut('');
                            var result = response.data;

                            if(result.rCode == '0000') {
                                self.$emit('set-pay-info', self.investorIdx); 
                                self.$emit('step-change', 4); 
                            } else {
                                noti.open(result.rMsg + ' <br/> 상세페이지로 돌아갑니다. ', function() {window.open("/i/" +  self.pjUrl, '_self');});
                            }
                        })

                },
                cancel : function() {
                    var self = this;

                    this.cancelRequest.pjCode = this.info.pjCode;
                    $('.page-loader-more').fadeIn('');

                    axios.post('/data/invest/funding/investor-status', {pjCode : this.info.pjCode, memCode : userInfo.memCode})
                        .then(function (response) {
                            $('.page-loader-more').fadeOut('');
                            var result = response.data;

                            if (result.rCode == '0000') {
                                // 청약 완료
                                if (result.rData.investorStatus == 'IVSS00') {
                                     noti.open('이미 청약을 완료하셨습니다. <br/>[마이페이지]메뉴에서 청약 내역을 상세히 확인하실 수 있습니다.', function() {window.open("/i/" +  self.info.pjAliasUrl, '_self');});
                                     return;
                                } else {
                                    $('.page-loader-more').fadeIn('');
                                     axios.post('/data/invest/funding/cancel', self.cancelRequest)
                                        .then(function (response) {
                                            $('.page-loader-more').fadeOut('');
                                            var cancelResult = response.data;

                                            if (cancelResult.rCode == '0000') {
                                                noti.open('청약이 성공적으로 취소되었습니다.', function() {window.open("/i/" +  self.pjUrl, '_self');});
                                            } else {
                                                noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.", function() {window.open("/i/" +  self.pjUrl, '_self');});
                                            }
                                            
                                        })
                                }
                            }
                        })
                    
                },
                goback : function() {
                    var self = this;
                    window.open("/i/" +  self.pjUrl, '_self');
                }
            }
        }
    }
}
export default new InvestFundingPay