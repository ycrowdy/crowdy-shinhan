class InvestFundingDanger {
    component() {
        return {
            template : `
                <div>
                    <div class="ivs-danger-title mt40 xs-mt20">
                        <span class="blue-800">{{memName}}</span>님은 <span class="blue-800">{{investInfoData.pjCompanyName}}</span>의 증권발행에 대하여<br/>
                        청약함에 있어 다음과 같은 사항을 확인합니다.
                    </div>

                    <div class="ivs-danger-quest mt40 xs-mt20">
                        1. 귀하는 본 금융투자상품이 자본시장법에 따른 “증권”에 해당되므로 원본손실 위험성이 있다는 것을 확인합니다. 따라서 투자한 자금의 원본손실의 위험이 있으며, 발행인이 제시한 예상 수익과, 귀하가 예상하는 수익이나 기대하는 수익의 일부 또는 전부를 얻지 못할 수 있음을 확인합니다.
                    </div>
                    <div class="radio-group mt15">
                        <label for="question1_1" class="radio-inline pl0">
                            <input type="radio" name="question1" id="question1_1" value="true" v-model="question[0]"/> <span class="label-custom"></span> <span class="font15">예</span>
                        </label>
                        <label for="question1_2" class="radio-inline">
                            <input type="radio" name="question1" id="question1_2" value="false" v-model="question[0]"/> <span class="label-custom"></span> <span class="font15">아니오</span>
                        </label>
                    </div>

                    <div class="mt25 ivs-danger-quest">
                        2. 귀하는 (주)크라우디가 자본시장법에 따른 “온라인소액투자중개업자”의 지위에서 온라인소액증권 발행인과 온라인소액투자 중개계약을 체결하여 위 발행인이 발행하는 증권에 대한 청약 거래를 중개 역할만 하므로, 직접 증권을 발행하거나 주금을 납입 받지 않는다는 것을 알고 있습니다.
                    </div>
                    <div class="radio-group mt15">
                        <label for="question2_1" class="radio-inline pl0">
                            <input type="radio" name="question2" id="question2_1" value="true" v-model="question[1]"/> <span class="label-custom"></span> <span class="font15">예</span>
                        </label>
                        <label for="question2_2" class="radio-inline">
                            <input type="radio" name="question2" id="question2_2" value="false" v-model="question[1]"/> <span class="label-custom"></span> <span class="font15">아니오</span>
                        </label>
                    </div>

                    <div class="mt25 ivs-danger-quest">
                        3. 귀하는 (주)크라우디의 홈페이지에 게재되어 모집되는 증권의 취득에 따른 투자위험요소, 증권의 발행조건, 발행인의 재무상태가 기재된 서류 및 사업계획서의 내용을 충분히 확인하였으며, 또한 게재된 사항은 청약기간 종료 전에 정정될 수 있다는 점을 인지하고 있습니다.
                    </div>
                    <div class="radio-group mt15">
                        <label for="question3_1" class="radio-inline pl0">
                            <input type="radio" name="question3" id="question3_1" value="true" v-model="question[2]"/> <span class="label-custom"></span> <span class="font15">예</span>
                        </label>
                        <label for="question3_2" class="radio-inline">
                            <input type="radio" name="question3" id="question3_2" value="false" v-model="question[2]"/> <span class="label-custom"></span> <span class="font15">아니오</span>
                        </label>
                    </div>

                    <div class="mt25 ivs-danger-quest">
                        4. 귀하는 (주)크라우디는 온라인소액중개 플랫폼으로써 크라우드펀딩으로 자금을 모집하는 단순 중개자로서의 역할만 수행하므로 투자손실의 위험을 보전하는 당사자가 아님을 확인합니다. 투자에 대한 모든 위험은 투자자 본인에게 있음을 확인합니다.
                    </div>
                    <div class="radio-group mt15">
                        <label for="question4_1" class="radio-inline pl0">
                            <input type="radio" name="question4" id="question4_1" value="true" v-model="question[3]"/> <span class="label-custom"></span> <span class="font15">예</span>
                        </label>
                        <label for="question4_2" class="radio-inline">
                            <input type="radio" name="question4" id="question4_2" value="false" v-model="question[3]"/> <span class="label-custom"></span> <span class="font15">아니오</span>
                        </label>
                    </div>

                    <div class="mt25 ivs-danger-quest">
                        5. 귀하는 금번에 발행되는 비상장 증권의 발행이 한국거래소의 상장을 목적으로 하는 것이 아니며, 따라서 증권의 환금성에 큰 제약이 있다는 점과 예상 회수금액에 대한 일부 혹은 전부를 회수할 수 없는 위험이 있음을 이해하며, 귀하가 이를 감당할 수 있음을 확인합니다.
                    </div>
                    <div class="radio-group mt15">
                        <label for="question5_1" class="radio-inline pl0">
                            <input type="radio" name="question5" id="question5_1" value="true" v-model="question[4]"/> <span class="label-custom"></span> <span class="font15">예</span>
                        </label>
                        <label for="question5_2" class="radio-inline">
                            <input type="radio" name="question5" id="question5_2" value="false" v-model="question[4]"/> <span class="label-custom"></span> <span class="font15">아니오</span>
                        </label>
                    </div>

                    <div class="mt25 ivs-danger-quest">
                        6. 발행인이 증권의 발행조건과 관련하여 상환조건, 전환조건을 설정하거나, 이해관계자에 대해 특정한 조건을 설정한 경우 이에 대한 구체적인 내용을 홈페이지 혹은 IR보고서에서 확인해야 함을 인지하고 있습니다.
                    </div>
                    <div class="radio-group mt15">
                        <label for="question6_1" class="radio-inline pl0">
                            <input type="radio" name="question6" id="question6_1" value="true" v-model="question[5]"/> <span class="label-custom"></span> <span class="font15">예</span>
                        </label>
                        <label for="question6_2" class="radio-inline">
                            <input type="radio" name="question6" id="question6_2" value="false" v-model="question[5]"/> <span class="label-custom"></span> <span class="font15">아니오</span>
                        </label>
                    </div>

                    <template v-if="investInfoData.pjStockType == '2'">
                        <div class="mt25 ivs-danger-quest">
                            7. 귀하는 금번에 발행되는 사채증권의 내용에서 미리 정한 손익정산이 완료될 경우, 원금손실이 있더라도 발행인의 청산을 위하여 잔존채무에 대하여 전부 면책함에 동의합니다.
                        </div>
                        <div class="radio-group mt15">
                            <label for="question7_1" class="radio-inline pl0">
                                <input type="radio" name="question7" id="question7_1" value="true" v-model="question[6]"/> <span class="label-custom"></span> <span class="font15">예</span>
                            </label>
                            <label for="question7_2" class="radio-inline">
                                <input type="radio" name="question7" id="question7_2" value="false" v-model="question[6]"/> <span class="label-custom"></span> <span class="font15">아니오</span>
                            </label>
                        </div>
                    </template>

                    <div class="mt25 ivs-danger-quest">
                        <template v-if="investInfoData.pjStockType == '1'">7.</template>
                        <template v-if="investInfoData.pjStockType == '2'">8.</template>
                        귀하는 “자본시장과 금융투자업에 관한 법률” 제117조의 10 제7항에 따라 발행된 증권이 예외없이 예탁결제원에 예탁 혹은 보호예수 되며 전문투자자에 대한 매도 등 예외적인 경우를 제외하고는 원칙적으로 6개월간 전매가 제한된다는 점을 이해합니다.
                    </div>

                    <div class="radio-group mt15">
                        <label for="question8_1" class="radio-inline pl0">
                            <input type="radio" name="question8" id="question8_1" value="true" v-model="question[7]"/> <span class="label-custom"></span> <span class="font15">예</span>
                        </label>
                        <label for="question8_2" class="radio-inline">
                            <input type="radio" name="question8" id="question8_2" value="false" v-model="question[7]"/> <span class="label-custom"></span> <span class="font15">아니오</span>
                        </label>
                    </div>

                    <div class="mt25 ivs-danger-quest">
                        <template v-if="investInfoData.pjStockType == '1'">8.</template>
                        <template v-if="investInfoData.pjStockType == '2'">9.</template>
                        귀하는 청약기간 중에는 청약의 철회를 할 수 있으나, 청약기간 종료일 이후에는 청약을 철회할 수 없으며, 모집예정금액의 80% 미달 시 증권발행이 취소되며, 귀하의 청약증거금은 투자예치금 계좌에 복원됩니다.
                    </div>

                    <div class="radio-group mt15">
                        <label for="question9_1" class="radio-inline pl0">
                            <input type="radio" name="question9" id="question9_1" value="true" v-model="question[8]"/> <span class="label-custom"></span> <span class="font15">예</span>
                        </label>
                        <label for="question9_2" class="radio-inline">
                            <input type="radio" name="question9" id="question9_2" value="false" v-model="question[8]"/> <span class="label-custom"></span> <span class="font15">아니오</span>
                        </label>
                    </div>
                    

                    <div class="mt25 ivs-danger-quest">
                        <template v-if="investInfoData.pjStockType == '1'">9.</template>
                        <template v-if="investInfoData.pjStockType == '2'">10.</template>
                        귀하는 개인정보보호정책(투자)을 확인하였으며, 귀하에게 서비스 제공과 원활한 계약사항의 이행을 위해 본 약관에 허용된 범위에 한하여 제3자에게 개인정보가 제공될 수 있음에 동의합니다. <a href="/crowdy/term?menu=5" class="blue-800" target="_blank">개인정보보호정책(투자) 확인하기</a>
                    </div>

                    <div class="radio-group mt15">
                        <label for="question10_1" class="radio-inline pl0">
                            <input type="radio" name="question10" id="question10_1" value="true" v-model="question[9]"/> <span class="label-custom"></span> <span class="font15">예</span>
                        </label>
                        <label for="question10_2" class="radio-inline">
                            <input type="radio" name="question10" id="question10_2" value="false" v-model="question[9]"/> <span class="label-custom"></span> <span class="font15">아니오</span>
                        </label>
                    </div>


                    <div class="mt25 ivs-danger-quest">
                        <template v-if="investInfoData.pjStockType == '1'">10.</template>
                        <template v-if="investInfoData.pjStockType == '2'">11.</template>
                        귀하는 CROWDY 이용약관(투자)을 확인하였으며, 투자정보의 게재, 청약의 방법, 청약의 주문 및 철회, 모집결과의 게시 및 통보에 관한 사항 등 온라인소액투자 중개 서비스 이용에 대한 약관 내용에 동의합니다. <a href="/crowdy/term?menu=3" class="blue-800" target="_blank">CROWDY 이용약관(투자) 확인하기</a>
                    </div>

                    <div class="radio-group mt15">
                        <label for="question11_1" class="radio-inline pl0">
                            <input type="radio" name="question11" id="question11_1" value="true" v-model="question[10]"/> <span class="label-custom"></span> <span class="font15">예</span>
                        </label>
                        <label for="question11_2" class="radio-inline">
                            <input type="radio" name="question11" id="question11_2" value="false" v-model="question[10]"/> <span class="label-custom"></span> <span class="font15">아니오</span>
                        </label>
                    </div>
                    
                    <div class="mt25 ivs-danger-quest">
                        <template v-if="investInfoData.pjStockType == '1'">11.</template>
                        <template v-if="investInfoData.pjStockType == '2'">12.</template>
                        (주)크라우디는 온라인소액증권 청약과 관련하여 투자자들에게 별도의 수수료 (CROWDY 서비스 이용료 등)를 징수하지 않습니다. 다만 청약증거금 용도의 자금을 투자예치금 계좌에 이체할 때, 이용하는 은행의 정책에 따라 타행이체의 경우 이체 수수료가 발생할 수 있습니다.
                    </div>

                    <div class="radio-group mt15">
                        <label for="question12_1" class="radio-inline pl0">
                            <input type="radio" name="question12" id="question12_1" value="true" v-model="question[11]"/> <span class="label-custom"></span> <span class="font15">예</span>
                        </label>
                        <label for="question12_2" class="radio-inline">
                            <input type="radio" name="question12" id="question12_2" value="false" v-model="question[11]"/> <span class="label-custom"></span> <span class="font15">아니오</span>
                        </label>
                    </div>

                    <div class="mt25 ivs-danger-quest">
                        <template v-if="investInfoData.pjStockType == '1'">12.</template>
                        <template v-if="investInfoData.pjStockType == '2'">13.</template>
                        (주)크라우디는 발행인의 요청에 따라(법적으로 설정 가능한) 청약 시 합리적으로 명확한 기준(선착순, 금액순, 전문투자가순 등)에 따라 투자자의 자격 등을 제한할 수 있으므로 해당 기준과 조건에 따라 청약의 우대 및 차별을 받게 될 수 있음을 인지합니다.
                    </div>

                    <div class="radio-group mt15">
                        <label for="question13_1" class="radio-inline pl0">
                            <input type="radio" name="question13" id="question13_1" value="true" v-model="question[12]"/> <span class="label-custom"></span> <span class="font15">예</span>
                        </label>
                        <label for="question13_2" class="radio-inline">
                            <input type="radio" name="question13" id="question13_2" value="false" v-model="question[12]"/> <span class="label-custom"></span> <span class="font15">아니오</span>
                        </label>
                    </div>


                    <div class="mt25 ivs-danger-quest">
                        <template v-if="investInfoData.pjStockType == '1'">13.</template>
                        <template v-if="investInfoData.pjStockType == '2'">14.</template>
                        위 사항들은 청약 주문 거래에 수반되는 위험과 제도와 관련하여 귀하가 알아야할 내용 및 사안을 간략하게 서술한 것으로 본 거래와 관련하여 발생될 수 있는 모든 위험과 중요사항이 전부 기술된 것은 아닙니다. 따라서 상세내용은 관계법규 및 (주)크라우디의 CROWDY 이용약관(투자)와 개인정보보호정책(투자)을 통해 확인하여야 합니다. 또한 본 고지는 청약 주문 거래와 관련된 법규 등에 우선하지 못한다는 점을 양지하여 주시기 바랍니다.
                    </div>

                    <div class="radio-group mt15">
                        <label for="question14_1" class="radio-inline pl0">
                            <input type="radio" name="question14" id="question14_1" value="true" v-model="question[13]"/> <span class="label-custom"></span> <span class="font15">예</span>
                        </label>
                        <label for="question14_2" class="radio-inline">
                            <input type="radio" name="question14" id="question14_2" value="false" v-model="question[13]"/> <span class="label-custom"></span> <span class="font15">아니오</span>
                        </label>
                    </div>


                    <hr class="common_hr_blue mt50">

                    <div class="font22 mb80 ivs-danger-bottom-text">
                        <div class="mt30 textCenter">{{now}} 투자자</div>
                        <div class="mt15 textCenter">
                            <template v-if="type=='1'">
                                <!-- TODO : REALTIME 이름 확인 -->
                                <!-- <input type="text" class="form-control" title="이름작성" v-on:input="nameCheck" v-model="reName" placeholder="이름작성" style="width:120px;display:inline-block;"/> -->
                                <input type="text" class="form-control" title="이름작성" v-model="reName" placeholder="이름작성" style="width:120px;display:inline-block;"/>
                            </template>
                            <template v-if="type!='1'">
                                {{memName}}
                            </template>
                            &nbsp;은/는 <br class="visible-xs-block"/> 투자위험고지를 모두 확인하였습니다.
                        </div>
                        <!-- TODO : REALTIME 이름 확인 -->
                        <!-- <div class="mypage-setting-btn mt40" v-on:click="next" style="max-width:500px;" :class="{'not-allowed-btn' : notAllowed }">다음 단계</div>  -->
                        <div class="mypage-setting-btn mt40" v-on:click="next" style="max-width:500px;">다음 단계</div> 
                    </div>
                </div>           
            `,
            props : {
                investInfoData : {
                    type : Object
                },
                code : {
                    type : String
                }
            },
            data : function() {
                return {
                    memName : userInfo.name,
                    type : userInfo.type,
                    reName : "",    //이름 작성
                    now : moment().format("YYYY년 MM월 DD일"),
                    notAllowed : true
                }
            },
            created : function() {
                // 주식/채권 구분에 따라 텍스트가 다름
                //this.investInfoData.pjSecurCode == 'SEC001'
                // SEC001  지분증권-주식형
                // SEC002  채무증권-채권형
                // SEC003  채무증권-CB(전환사채)형
                // SEC004  채무증권-BW(신주인수권부사채)형
                // SEC005  투자계약증권
                // SEC006  채무증권-등록발행
                // SEC007  채무증권-비등록발행
            },
            computed : {
                question : function(){
                    // return this.investInfoData.pjStockType == '1' ? [false, false, false, false, false, false, false, true, false] : [false, false, false, false, false, false, false, false, false];
                    return this.investInfoData.pjStockType == '1' ? [false, false, false, false, false, false, true, false, false, false, false, false, false, false] : [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false];
                    // return this.investInfoData.pjStockType == '1' ? [true, true, true, true, true, true, true, true, true] : [true, true, true, true, true, true, true, true, true];
                }
            },
            methods : {
                nameCheck : function() {
                    //개인일 경우 이름 재 확인 
                    if(this.type == 1){
                        if(this.reName != this.memName){
                            this.notAllowed = true;
                        } else {
                            this.notAllowed = false;
                        }
                    } else {
                        this.notAllowed = false;
                    }
                },
                next : function() {
                    // 청약 시간 확인 (공휴일 가능 / 일요일 불가능)
                    var hour = moment().hour();
                    // Sunday as 0 and Saturday as 6
                    var day = moment().day();

                    if(hour < 5 || hour > 22 || day == 0) {
                        noti.open('실시간 청약 가능 시간(영업일 AM 5:00 - PM 11:00) <br/> 에만 청약이 가능합니다.', function() {window.open('/', '_self')}, true);    
                        return;
                    }

                    for (var i = 0; i < this.question.length; i++) {
                        if (!this.question[i]) {
                            if (this.investInfoData.pjStockType == '1' && i == 8) {
                                noti.open(i + "번 항목에 대한 투자위험 고지내용을 <br/> 다시 확인해 주시기 바랍니다. <br/> 모든 항목을 확인 및 동의 하셔야만 <br/> 투자가 가능합니다.");
                            } else {
                                noti.open((i + 1) + "번 항목에 대한 투자위험 고지내용을 <br/> 다시 확인해 주시기 바랍니다. <br/> 모든 항목을 확인 및 동의 하셔야만 <br/> 투자가 가능합니다.");    
                            }
                            return false;
                        }
                    }
                
                    // TODO : REALTIME 이름 확인
                    // if (this.notAllowed) { 
                    //     noti.open("이름을 다시 작성해주세요.");
                    //     return;
                    // }

                    if(this.type == 1){
                        if(this.reName != this.memName){
                            noti.open("이름을 다시 작성해주세요.");
                            return;
                        }
                    }

                    this.$emit('step-change', 2);   
                },
            }
        }
    }
}
export default new InvestFundingDanger