class InvestFundingLimit {
    component() {
        return {
            template : `
                <div>
                    <div class="common-default-flex mt30">
                        <div class="ivs-funding-limit-layout">
                            <!--투자한도-->
                            <div class="ivs-funding-limit-title"><span class="blue-800">{{memName}}</span>님의 투자한도</div>
                            <div v-if="dataConfirm" class="ivs-funding-limit-amount">{{limitAmount}}</div>
                            <div class="font13 mt15">
                                <span class="blue-800">{{memName}}님의 투자한도({{investorTypeText}})와 투자가능 예치금 내에서 청약신청이 가능합니다. </span>
                                투자한도를 높이려면, [마이페이지>투자필수정보설정]에서 적격, 전문투자자 신청을 해주세요.
                            </div>
                            <!--//투자한도-->

                            <!-- 투자 가능 예치금 mobile tablet-->
                            <div class="ivs-funding-limit-layout1">
                                <div class="common-account-info-box"></div>
                                <div class="common-account-info-text">
                                    <div class="font16 mt30 mr20">
                                        <span class="blue-800">{{memName}}</span>님의 투자 가능 예치금
                                    </div>
                                    <div style="display: flex; align-items: baseline;" class="mt5">
                                        <div class="ivs-funding-limit-amount1 blue-800 mr20">{{ parseInt(accountInfo.restAmount).toLocaleString() }}원</div>
                                        <div class="common-default-flex pointerCursor z-index-1000" v-on:click="reload">
                                            <div class="reload-ico"></div>
                                            <div class="font12 blue-800" style="margin:-4px 20px 0px 4px!important;">새로고침</div>
                                        </div>
                                    </div>
                                    <div class="ml70 xs-ml50">
                                        <div class="font16 mt15">크라우디 {{memName}}</div>
                                        <div>
                                            <span class="font16 mr50">신한은행</span>
                                            <span class="font18">{{accountInfo.virtualAccount}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- //투자 가능 예치금 mobile-->


                            <!--청약신청하기-->
                            <div class="ivs-funding-limit-title mt50 xs-mt70">청약 신청하기</div>

                            <div class="font13 colorRed1">
                                <template v-if="investInfoData.pjMaxStock == 0">
                                청약 최소 단위는 {{investInfoData.pjMinStock}}주 입니다.
                                </template>
                                <template v-if="investInfoData.pjMaxStock > 0">
                                    청약 가능 단위는 {{investInfoData.pjMinStock}} ~ {{investInfoData.pjMaxStock}}주 입니다.
                                </template>
                            </div>

                            <div class="common-default-flex mt10">
                                <span style="width:33%;">{{stockText}}당 가격</span>
                                <span style="width:33%;">{{stockText=='주'?'주식':'구좌'}} 수</span>
                                <span style="width:33%;">청약 금액</span>
                            </div>
                            <div class="ivs-funding-limit-layout2">
                                <div class="font18 mr10">{{ parseInt(investInfoData.pjContStock).toLocaleString() }}원</div>
                                <div class="plus-ico mr10"> + </div>
                                <number-input class="form-control blue-800 text-right xs-text-center mr10" :num="request.pjSubsQty" v-model="request.pjSubsQty" style="max-width:70px"></number-input>
                                <div class="font18 mr10">{{stockText}}</div>
                                <div class="equl-ico"> = </div>
                                <div class="blue-800 font22 ivs-funding-limit-amout">{{pjAmount}}원</div>
                            </div>

                             <!--증권계좌확인-->
                            <template v-if="pjSecurCode != 'SEC007'">
                                <div class="ivs-funding-limit-title mt50">증권 계좌 확인</div>
                                <div class="mt15 common-default-flex">
                                    <bank-select v-if="dataConfirm" :options="bankCodeOptions" v-model="memIvsSecurCode"></bank-select>
                                    <input type="tel" class="form-control ml20" id="delivery_mobile2 ivs-funding-input-custom" v-model="memIvsSecurNo">
                                </div>
                                <div class="mt10 font13">   
                                    투자자 본인 명의의 증권계좌로 실제 주식증권이 한국 예탁결제원을 통해 배정됩니다. 거래 가능한 증권계좌번호를 등록해주세요.
                                </div>
                                <div class="mt10 font13">   
                                    투자자 본인 명의의 증권 게좌로 교부됩니다. 직접 주식거래가 가능한 증권계좌번호를 입력해주세요. 
                                    <span class="blue-800">CMA계좌는 사용이 불가합니다.</span>
                                </div>
                                <div class="mt10 font13">   
                                    <span class="blue-800">한국 예탁결제원과 계약이 된 증권사의 계좌만 이용이 가능합니다.</span> 목록에 없는 증권사는 이용할 수 없습니다. 
                                    증권계좌는 실시간으로 유효성 확인이 되지 않으며, 유효하지 않는 증권계좌 입력시 배정탈락의 사유가 될 수 있습니다.
                                </div>
                            </template>
                            <!--//증권계좌확인-->
                        </div>

                        <!-- 투자 가능 예치금 pc-->
                        <div class="ivs-funding-limit-layout3">
                            <div class="common-account-info-box"></div>
                            <div class="common-account-info-text">
                                <div class="font16 mt30 mr20">
                                    <span class="blue-800">{{memName}}</span>님의 투자 가능 예치금
                                </div>
                                <div style="display: flex; align-items: baseline;" class="mt5">
                                    <div class="ivs-funding-limit-amount1 blue-800 mr20">{{ parseInt(accountInfo.restAmount).toLocaleString() }}원</div>
                                    <div class="common-default-flex pointerCursor z-index-1000" v-on:click="reload">
                                        <div class="reload-ico"></div>
                                        <div class="font12 blue-800" style="margin:-4px 20px 0px 4px!important;">새로고침</div>
                                    </div>
                                </div>
                                <div class="ml100">
                                    <div class="font24 mt40">크라우디 {{memName}}</div>
                                    <div>
                                        <span class="font22 mr50">신한은행</span>
                                        <span class="font24">{{accountInfo.virtualAccount}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr class="common_hr_custom1 mt70 hidden-xs" >
                    <div class="mypage-setting-btn mt30 mb30" v-on:click="subscribe" v-if="dataConfirm" style="max-width:500px;">청약 신청하기</div>
                    
                </div>           
            `,
            props : {
                investInfoData : {
                    type : Object
                },
                code : {
                    type : String
                }
            },
            data : function() {
                return {
                    // SEC001  지분증권-주식형
                    // SEC002  채무증권-채권형
                    // SEC003  채무증권-CB(전환사채)형
                    // SEC004  채무증권-BW(신주인수권부사채)형
                    // SEC005  투자계약증권
                    // SEC006  채무증권-등록발행
                    // SEC007  채무증권-비등록발행
                    pjSecurCode : this.investInfoData.pjSecurCode,
                    memName : userInfo.name,
                    memCode : userInfo.memCode,
                    request : {
                        pjCode : this.code,
                        memCode : userInfo.memCode,
                        // 청약 주식 수
                        pjSubsQty : this.investInfoData.pjMinStock
                    },
                    limit : {
                        /**
                         * KSD 투자자 유형 
                         * 01 - 일반투자자
                         * 02 - 소득요건 등 적격투자자
                         * 03 - 전문투자자
                         * 04 - 연고자
                         */                        
                        ksdInvestorType : '01',
                        /**
                         * 크라우디 투자자 유형 
                         * MIT001 - 일반투자자
                         * MIT002 - 소득요건 등 적격투자자
                         * MIT003 - 전문투자자
                         * MIT004 - 연고자
                         */   
                        investorType : '',
                        /**
                         * 투자 한도 금액
                         */
                        investorFundingLimitAmount : 0
                    },
                    confirm : {
                        data : false,
                        bank : false,
                        account : false
                    },
                    bankCodeOptions : [],
                    // 증권계좌 : 증권사 코드 
                    memIvsSecurCode : '',
                    // 증권계좌 : 증권계좌
                    memIvsSecurNo : '',
                    // 증권계좌 : 증권사 이름
                    //memIvsSecurName : '',
                    // 은행계좌
                    memIvsBankCode : '',
                    memIvsBankNo : '',
                    //memIvsBankName : ''
                    accountInfo : {
                        //가상계좌번호
                        virtualAccount : 0,
                        //지금까지 총 예치되었던 금액들 
                        addTotalAmount : 0,
                        // 남은 예치금
                        restAmount : 0, 
                        bankCode : 0,
                        bankNo : 0,
                        //출금계좌 상태 (0: 1원이체진행중, 1: 성공, 2: 실패)
                        bankStatus : 0
                    }
                }
            },
            created : function() {
                $(window).scrollTop(0);
                // 투자자 정보 조회
                this.information();

                // 은행 목록
                this.getBankCodeList();

                // 계좌 정보 조회
                this.getVirtualAccountInfo();

                // 한도 조회

                 //pjType 1:공모, 2:사모
                if (this.investInfoData.pjType == 1) {
                    this.checkLimit(); 
                } else {
                    self.confirm.data = true;
                    $(".if_loader").hide();
                    $(".if_limit_wrap").show();
                }
            },
            computed : {
                investorTypeText : function() {
                    if (this.limit.investorType == 'MIT001') {
                        return '일반투자자';
                    } else if (this.limit.investorType == 'MIT002') {
                        return '소득요건 등 적격투자자';
                    } else if (this.limit.investorType == 'MIT003') {
                        return '전문투자자';
                    } else if (this.limit.investorType == 'MIT004') {
                        return '연고자';
                    }     
                },
                dataConfirm : function() {
                    return this.confirm.data && this.confirm.bank && this.confirm.account;
                },
                // 청약 금액
                pjAmount : function() {
                    if (parseInt(this.request.pjSubsQty) > 0) {
                        return (parseInt(this.request.pjSubsQty) * parseInt(this.investInfoData.pjContStock)).toLocaleString();    
                    } else {
                        return 0;
                    }
                    
                },
                // 청약 금액
                limitAmount : function() {

                    // 전문투자자, 연고자
                    if (this.investInfoData.memIvsType == 'MIT003' || this.investInfoData.memIvsType == 'MIT004' || this.investInfoData.pjType == 2) {
                        return '무제한';
                    } else {
                        return parseInt(this.limit.investorFundingLimitAmount).toLocaleString() + '원';
                    }
                },
                stockText : function() {
                    return this.investInfoData.pjStockType == '1' ? '주' : '구좌';
                }
            },
            components : {
                bankSelect : require('../common/select.js').default.component(),
                numberInput : require('../common/number-input.js').default.component()
            },
            methods : {
                checkLimit : function() {
                    var self = this;

                    axios.post('/data/invest/funding/limit', this.request)
                        .then(function (response) {
                            var result = response.data;

                            if (result.rCode == '0000') {
                                self.limit = response.data.rData;

                                // 전문투자자, 연고자
                                if (self.investInfoData.memIvsType == 'MIT003' || self.investInfoData.memIvsType == 'MIT004') {
                                    self.limit.investorFundingLimitAmount = '무제한';
                                }

                                self.confirm.data = true;
                                $(".if_loader").hide();
                                $(".if_limit_wrap").show();
                            } else {
                                noti.open(result.rMsg, function() {window.open("/i/" + self.investInfoData.pjAliasUrl, '_self');});
                            }
                        })
                   
                },
                getVirtualAccountInfo : function() {
                    var self = this;
                     $('.page-loader-more').fadeIn('');
                    axios.post('/data/invest/virtual/account-refresh', { memCode : userInfo.memCode, seq : 0})
                        .then(function (response) {
                            $('.page-loader-more').fadeOut('');
                             var result = response.data;

                            if (result.rCode == '0000') {
                                self.accountInfo = result.rData;
                                self.confirm.account = true;
                            } else {
                                noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.");
                            }
                        })
                },
                getBankCodeList : function() {
                    var self = this;

                    var gcode = "";

                    if (this.pjSecurCode == 'SEC007') {
                        gcode = "IRMI_STOCK_BANK";
                    } else {
                        gcode = "MEM_IVS_SECUR_CODE";
                    }
                    
                    axios.post('/data/crowdy/code/invest', {gcode : gcode})
                        .then(function(response){
                            var result = response.data;
                            if(result.rCode == "0000") {
                                for (var i = 0; i < result.rData.length; i++) {
                                    var option = {
                                            id : result.rData[i].commonCode,
                                            text : result.rData[i].commonInfo
                                        };
                                    self.bankCodeOptions.push(option);
                                }
                                self.confirm.bank = true;
                            } else {
                                noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.");
                            }
                    })
                },
                subscribe : function() {
                    var self = this;

                    if(this.memIvsSecurNo==''|| this.memIvsSecurNo.length < 1){
                        var notiText1 = "<div class='common-modal-title'>증권계좌를 입력해주세요</div><div class='font15 mt15 xs-mt10'>청약하신 증권이 입력하신 증권계좌로 입고 됩니다.</div>";
                        info.open(notiText1);
                        return;
                    }

                    if (parseInt(this.pjAmount) > parseInt(this.accountInfo.restAmount)) {
                        var notiText1 = "<div class='common-modal-title'>예치금을 확인해주세요</div><div class='font15 mt15 xs-mt10'>예치금보다 큰 금액은 투자하실 수 없습니다.</div>";
                        info.open(notiText1);
                        return;
                    }

                    // if (this.memIvsSecurNo == '' || this.memIvsSecurNo.length < 1) {
                    //     noti.open("증권계좌가 잘못되거나 등록되지 않았습니다. <br /> '투자필수정보 설정'에서 증권계좌를 등록하시겠습니까?", function() {window.open("/mypage/main?menu=5&sub-menu=2", '_self');});
                    //     return;
                    // }

                    // 청약 시간 확인 (공휴일 가능 / 일요일 불가능)
                    var hour = moment().hour();
                    // Sunday as 0 and Saturday as 6
                    var day = moment().day();

                    if(hour < 5 || hour > 22 || day == 0) {
                        noti.open('실시간 청약 가능 시간(영업일 AM 5:00 - PM 11:00) <br/> 에만 청약이 가능합니다.', function() {window.open('/', '_self')}, true);    
                        return;
                    }

                    var requestSecur = {
                        memCode : userInfo.memCode,
                        memIvsBankCode : this.memIvsBankCode,
                        memIvsBankNo : this.memIvsBankNo,
                        memIvsSecurCode : this.memIvsSecurCode,
                        memIvsSecurNo : this.memIvsSecurNo
                    };

                    // if(self.investInfoData.pjStockType == 1) {
                        if(parseInt(self.investInfoData.pjMinStock) > parseInt(self.request.pjSubsQty)) {
                            noti.open("청약 가능 최소 단위는 " + self.investInfoData.pjMinStock + "주 입니다.");
                            return;
                        }

                        if(parseInt(self.investInfoData.pjMaxAmount) > 0) {
                            if(parseInt(self.investInfoData.pjMaxAmount) < parseInt(self.request.pjSubsQty)) {
                                noti.open("청약 가능 최대 단위는 " + self.investInfoData.pjMaxAmount + "주 입니다.");
                                return;
                            }
                        }
                    // }


                    $('.page-loader-more').fadeIn('');
                    axios.post('/data/member/investor/update/secur', requestSecur)
                        .then(function (response) {
                            $('.page-loader-more').fadeOut('');
                             var result = response.data;

                            if (result.rCode == '0000') {
                                
                                    if (self.pjSecurCode == 'SEC007') {
                                        if (self.memIvsBankNo == '' || self.memIvsBankNo.length < 1) {
                                            noti.open("일반계좌가 잘못되거나 등록되지 않았습니다. <br /> 일반계좌를 등록해야 청약이 가능합니다.");
                                            return;
                                        }
                                    } else {
                                        if (self.memIvsSecurNo == '' || self.memIvsSecurNo.length < 1) {
                                            noti.open("증권계좌가 잘못되거나 등록되지 않았습니다. <br /> 증권계좌를 등록해야 청약이 가능합니다.");
                                            return;
                                        }
                                    }

                                    $('.page-loader-more').fadeIn('');
                                    axios.post('/data/invest/funding/subscribe-pay', self.request)
                                        .then(function (response) {
                                            $('.page-loader-more').fadeOut('');
                                            var result = response.data;

                                            if (result.rCode == '0000') {
                                                // window.open('/invest/funding/'+self.investInfoData.pjAliasUrl+'?moveStep=3&investorIdx=' + result.rData.pjInvestorIdx, '_self')
                                                self.$emit('set-idx', result.rData.pjInvestorIdx);   
                                                self.$emit('step-change', 3);   
                                            } else {
                                                noti.open(result.rMsg, function() {window.open("/i/" + self.investInfoData.pjAliasUrl, '_self');});
                                            }
                                        })  

                            } else {
                                noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.");
                            }
                        })
                },
                // 투자자 정보
                information : function() {
                    var self = this;

                    axios.post('/data/member/investor/info', {memCode : userInfo.memCode})
                        .then(function (response) {
                            var result = response.data.rData;

                            self.memIvsBankCode = result.memIvsBankCode;
                            self.memIvsBankNo = result.memIvsBankNo;    
                            // self.memIvsBankName = _.find(self.bankCodeOptions, {'id' : result.memIvsBankCode})
                            self.memIvsSecurCode = result.memIvsSecurCode;
                            self.memIvsSecurNo = result.memIvsSecurNo;    
                            // self.memIvsSecurName = _.find(self.bankCodeOptions, {'id' : result.memIvsSecurCode})

                            //pjType 1:공모, 2:사모
                            if (self.investInfoData.pjType == 2) {
                                self.limit.ksdInvestorType = self.investInfoData.memIvsType;
                                self.limit.investorType = self.investInfoData.memIvsType;
                                self.limit.investorFundingLimitAmount = '무제한';
                            }

                            self.confirm.file = true;
                        })
                },
                // 증권/일반계좌 업데이트
                updateSecurNo : function() {
                    var self = this;

                    var requestSecur = {
                        memCode : userInfo.memCode,
                        memIvsBankCode : this.memIvsBankCode,
                        memIvsBankNo : this.memIvsBankNo,
                        memIvsSecurCode : this.memIvsSecurCode,
                        memIvsSecurNo : this.memIvsSecurNo
                    };

                    $('.page-loader-more').fadeIn('');
                    axios.post('/data/member/investor/update/secur', requestSecur)
                        .then(function (response) {
                            $('.page-loader-more').fadeOut('');
                             var result = response.data;

                            if (result.rCode == '0000') {
                                noti.open("입력하신 계좌정보가 등록되었습니다.");
                            } else {
                                noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.");
                            }
                        })
                },
                reload : function(){
                    this.getVirtualAccountInfo();
                }
            }
        }
    }
}
export default new InvestFundingLimit