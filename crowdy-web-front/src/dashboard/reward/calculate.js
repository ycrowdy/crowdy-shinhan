class DashboardRewardCalculate {
    component() {
        return {
            template : `
            <div>
                <div class="my_dash_stitle">
                    <strong v-if="rewardData.cpEndStatus == 0">현재 프로젝트가 시작 전입니다.</strong>
                    <strong v-if="rewardData.cpEndStatus == 1">현재 프로젝트가 진행 중입니다.</strong>
                    <strong v-if="rewardData.cpEndStatus == 2 && dataConfirm">정산 상세 내역</strong>
                    <strong v-if="rewardData.cpEndStatus == 3">프로젝트가 목표 금액을 달성하지 못했습니다.</strong>
                </div>

                <template v-if="dataConfirm">
                    <div class="my_dash_table mt10">
                        <table class="table table-fixed">
                            <colgroup>
                                <col style="width:24%;">
                                <col style="width:28%;">
                                <col style="width:48%;">
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td class="first-child td_t_none">총 후원금액</td>
                                    <td class="td_t_none"><strong>{{ parseInt(calculateData.fhisTotSpAmount).toLocaleString() }}원</strong></td>
                                    <td class="last-child td_t_none"></td>
                                </tr>
                                <tr>
                                    <td class="first-child">총 후원건수</td>
                                    <td><strong>{{ parseInt(calculateData.sponCount).toLocaleString() }}건</strong></td>
                                    <td class="last-child"></td>
                                </tr>
                                <tr>
                                    <td class="first-child">결제 승인금액</td>
                                    <td><strong>{{ parseInt(calculateData.fhisFinalSpAmount).toLocaleString() }}원</strong></td>
                                    <td class="last-child">결제 승인금액 = 총 후원금액 - 결제 실패 금액</td>
                                </tr>
                                <tr>
                                    <td class="first-child">결제 실패금액</td> 
                                    <td><strong>{{ parseInt(calculateData.failAmount).toLocaleString() }}원</strong></td>
                                    <td class="last-child"></td>
                                </tr>
                                <tr>
                                    <td class="first-child">카드 결제 및 계좌 출금 실패건</td>
                                    <td><strong>{{ parseInt(calculateData.failCount).toLocaleString() }}건</strong></td>
                                    <td class="last-child"></td>
                                </tr>
                                <tr>
                                    <td class="first-child">CROWDY 수수료</td>
                                    <td><strong>{{ parseInt(parseInt(calculateData.crowdyRate) + parseInt(calculateData.crowdyVat)).toLocaleString() }}원</strong> ({{ parseInt(calculateData.crowdyRate).toLocaleString() }}원 + {{ parseInt(calculateData.crowdyVat).toLocaleString() }}원)</td>
                                    <template v-if="rewardData.cpFundType == '1'">
                                        <td class="last-child">CROWDY 수수료 = 결제 승인 금액의 5% + 부가가치세 10%</td>
                                    </template>
                                    <template v-if="rewardData.cpFundType == '2'">
                                        <td class="last-child">CROWDY 수수료 = 결제 승인 금액의 10% + 부가가치세 10%</td>
                                    </template>                                        
                                </tr>
                                <tr>
                                    <td class="first-child">PG 수수료</td>
                                    <td><strong>{{ parseInt(parseInt(calculateData.pgRate) + parseInt(calculateData.pgVat)).toLocaleString() }} 원</strong> ({{ parseInt(calculateData.pgRate).toLocaleString() }}원 + {{ parseInt(calculateData.pgVat).toLocaleString() }}원)</td>
                                    <td class="last-child">PG 수수료 = 카드 결제 승인 금액의 3% + 부가가치세 10%</td>
                                </tr>
                            </tbody>
                        </table>

                        <!-- 1, 2차 정산 -->
                        <table class="table table-fixed">
                            <colgroup>
                                <col style="width:24%;">
                                <col style="width:28%;">
                                <col style="width:48%;">
                            </colgroup>
                            <tbody>

                                <tr>
                                    <td class="first-child td_t_none">1차 정산금액</td>
                                    <td class="td_t_none"><strong>{{ parseInt(calculateData.firstCalculateAmount).toLocaleString() }}원</strong></td>
                                    <td class="last-child td_t_none">1차 정산금액 = 1일차 결제 금액</td>
                                </tr>
                                <tr>
                                    <td class="first-child">2차 정산금액</td>
                                    <td><strong>{{ parseInt(calculateData.secondCalculateAmount).toLocaleString() }}원</strong></td>
                                    <td class="last-child">2차 정산금액 = 2 ~ 7일차 결제 금액</td>
                                </tr>
                                <tr>
                                    <td class="first-child">정산상태</td>
                                    <td><strong>{{ getFratText(calculateData.fratStatus) }}</strong></td>
                                    <td class="last-child"></td>
                                </tr>
                            </tbody>
                        </table>
                        

                        <div class="my_dash_total">
                            <div class="row">
                                <div class="col-xs-10 text-right">
                                    <strong>최종정산금액</strong>
                                </div>
                                <div class="col-xs-2 text-right blue-800">
                                    <strong>{{ parseInt(calculateData.fhisFinalSpAmount - calculateData.crowdyRate - calculateData.crowdyVat - calculateData.pgRate - calculateData.pgVat).toLocaleString() }}원</strong>
                                </div>
                            </div>
                            <p>최종정산금액 = 결제 승인금액 - CROWDY 수수료 - PG 수수료</p> <!--  - 계좌이체수수료(기본 + 성공) -->
                        </div>
                    </div>

                    <div class="row mt40">
                        <div class="col-xs-6">
                            <div class="row">
                                <div class="col-xs-11">
                                    <div class="my_dash_stitle text-left">
                                        <strong>지급계좌 안내</strong>
                                    </div>
                                    <div class="my_dash_table mt10">
                                        <table class="table table-fixed table_none">
                                            <colgroup>
                                                <col style="width:30%;">
                                                <col style="width:70%;">
                                            </colgroup>
                                            <tbody>
                                                <tr>
                                                    <td class="first-child td_t_none">예금주</td>
                                                    <td class="td_t_none"><strong>{{ calculateData.cpFundBankAccountName  }}</strong></td>
                                                </tr>
                                                <tr>
                                                    <td class="first-child">은행</td>
                                                    <td><strong>{{ calculateData.cpFundBankCode  }}</strong></td>
                                                </tr>
                                                <tr>
                                                    <td class="first-child">계좌 번호</td>
                                                    <td><strong>{{ calculateData.cpFundBankAccountNo  }}</strong></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="text-right mt45 mb30">
                                        <a id="tax_print" class="btn btn-primary-outline" v-if="rewardData.cpEndStatus == '2'" :disabled="calculateData.taxStatus == '1' || calculateData.taxStatus == '2'" v-on:click="taxRequest">{{taxStatusText}}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                                    

                    <!-- 세금 계산서 발행 -->
                    <div id="taxModal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog modal-sm" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <a class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                                </div>
                                <div class="modal-body modal-order">
                                    세금 계산서를 발행 하시겠습니까?
                                    <div class="modal-footer text-center">
                                        <div class="row not-space">
                                            <div class="col-xs-4 col-xs-offset-4">
                                                <a class="btn btn-block btn-primary-outline tax-close-btn" v-on:click="taxRequestConfirm">발행요청</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- //세금 계산서 발행 -->
                </template>
            </div>
            `,
            props : ['rewardData'],
            data : function() {
                return {
                    calculateData : {},
                    search : {
                        cpCode : this.rewardData.cpCode,
                        memCode : userInfo.memCode,
                    },
                    dataConfirm : false,
                    taxStatusText : ""
                }
            },
            components : {
            },
            created : function() {
                if (this.rewardData.cpEndStatus == '2') {
                    this.load();
                }
            },
            methods : {
                load : function() {
                    var self = this;
                    axios.post('/data/view/reward/dashboard/calculate', this.search)
                        .then(function (response) {
                            var result = response.data.rData;
                            self.calculateData = result;

                            if (result.taxStatus == '0' || result.taxStatus == '3') {
                                self.taxStatusText = "세금계산서 발행요청";
                            } else if (result.taxStatus == '1') {
                                self.taxStatusText = "세금계산서 발행중";
                            } else {
                                self.taxStatusText = "세금계산서 발행완료";
                            }

                            self.dataConfirm = true;  
                        })
                },
                taxRequest : function() {
                    if (this.calculateData.taxStatus == '0') {
                        $('#taxModal').modal('show');    
                    } else {
                        noti.open("세금계산서 발행이 필요한 경우 담당 매니저에게 연락 부탁드립니다.")
                    }
                },
                //세금계산서 발행 요청
                taxRequestConfirm : function() {
                    var self = this;

                    $('.page-loader-more').fadeIn('');
                    axios.post('/set/save/reward/tax-request', this.search)
                    .then(function (response) {
                        $('.page-loader-more').fadeOut('');

                        var result = response.data;
                        if(result.rCode == "0000") {
                            noti.open("세금계산서 발행이 신청되었습니다.");
                            self.load();
                        } else {
                            noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.");
                        }                                  
                    })

                    $('#taxModal').modal('hide');
                },
                getFratText : function(val) {
                    if(val == '0') {
                        return '1차 정산 전'
                    } else if(val == '1') {
                        return '1차 정산 완료'
                    } else if(val == '2') {
                        return '2차 정산 완료'
                    }
                }
            }
        }
    }
}
export default new DashboardRewardCalculate