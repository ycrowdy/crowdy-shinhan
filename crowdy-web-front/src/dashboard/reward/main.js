class DashboardRewardMain {
    component() {
        return {
            template : `
            <div>
                <div id="page" class="page-wrapper">
                    <!-- 리워드(2) - 대쉬보드 - 프로젝트 통계 -->
                    <div id="list_wrap" class="my_dash_wrap" v-bind:class="{'hidden-xs hidden-sm' : step != 1}">
                        <div class="container">
                            <div class="my_dash_header">
                                <div class="my_dash_header_condition">
                                    <span class="btn btn-default-outline btn_cursor">{{projectType(rewardData.cpType)}}</span>
                                    <span class="btn btn-primary-outline btn_cursor">{{projectStatus(rewardData.cpEndStatus)}}</span>
                                </div>
                                <h4 class="webfont2">{{ rewardData.cpTitle }}</h4>
                                {{ startDay }} ~ {{ endDay }}
                            </div>

                            <div class="mobile_info_tip hidden-md hidden-lg">
                                회원님의 프로젝트 간략 현황입니다.<br />
                                상세현황은 PC에서 확인 바랍니다.
                                <p>프로젝트 종류 후 정산내역은 PC에서만 확인가능합니다.</p>
                            </div>

                            <div class="my_dash_nav hidden-xs hidden-sm">
                                <ul class="nav nav-tabs nav-justified">
                                    <li :class="{'active' : step == 1}"><a v-on:click="stepChange(1)" style="cursor: pointer;">프로젝트 통계</a></li>
                                    <li :class="{'active' : step == 2}"><a v-on:click="stepChange(2)" style="cursor: pointer;">모든 펀딩 상세 현황</a></li>
                                    <li :class="{'active' : step == 3}"><a v-on:click="stepChange(3)" style="cursor: pointer;">리워드 주문 현황</a></li>
                                    <li :class="{'active' : step == 4}"><a v-on:click="stepChange(4)" style="cursor: pointer;">정산 내역</a></li>
                                </ul>
                            </div>
                            <statics v-if="step == 1 && dataConfirm" :reward-data="rewardData" :sdate="startDay" :edate="endDay"></statics>
                            <sponsor v-if="step == 2 && dataConfirm" :reward-data="rewardData" :sdate="startDay" :edate="endDay"></sponsor>
                            <orderstatus v-if="step == 3 && dataConfirm" :reward-data="rewardData"></orderstatus>
                            <calculate v-if="step == 4 && dataConfirm" :reward-data="rewardData"></calculate>
                        </div>
                    </div>
                </div>
            </div>
            `,
            props : ['cpAliasUrl'],
            data : function() {
                return {
                    step : 1,
                    search : {
                        paging : {
                            page : "1",
                            count : "10"
                        },
                        cpCode : '',
                    },
                    param : {
                        cpAliasUrl : this.cpAliasUrl,
                        cpTraining : 'N'
                    },
                    rewardData : [],
                    startDay : '',
                    endDay : '',
                    dataConfirm : false,
                }
            },
            components : {
                statics : require('./statics.js').default.component(),
                sponsor : require('./sponsor.js').default.component(),
                orderstatus : require('./orderstatus.js').default.component(),
                calculate : require('./calculate.js').default.component(),
            },
            created : function() {
                this.load();
            },
            methods : {
                load : function() {
                    if(userInfo.memCode == null || userInfo.memCode == '') {
                        //noti.open('로그인을 해주세요.', function() {window.open("/", '_self');});
                        return;
                    }
                    var self = this;
                    axios.post('/data/view/reward/detail', this.param)
                        .then(function (response) {
                            var result = response.data.rData;
                            //여기
                            self.rewardData = result;
                            self.search.cpCode = self.rewardData.cpCode;
                            self.startDay = moment(self.rewardData.cpStartDate, 'YYYY-MM-DD').format('YYYY.MM.DD');
                            self.endDay = moment(self.rewardData.cpEndDate, 'YY.MM.DD').format('YYYY.MM.DD');
                            self.dataConfirm = true;
                        })
                },
                stepChange : function(step) {
                    this.step = step;
                    this.$emit('mobile-change', step);
                },
                projectStatus : function(val) {
                    var result = "";
                    if(val == "0") {
                        result = "승인대기";
                    } else if(val == "1") {
                        result = "진행중";
                    } else if(val == "2") {
                        result = "성공";
                    } else if(val == "3") {
                        result = "실패";
                    }
                    return result;
                },
                projectType : function(val) {
                    var result = "";
                    if(val == "0") {
                        result = "기부";
                    } else if(val == "1") {
                        result = "후원";
                    } else if(val == "2") {
                        result = "제품마케팅";
                    } else if(val == "3") {
                        result = "지분투자";
                    }
                    return result;
                }
            }
        }
    }
}
export default new DashboardRewardMain