class DashboardRewardMain {
    component() {
        return {
            template : `
            <div>
                <!-- 바뀌는 부분 -->
                <div class="my_dash_stitle text-left">
                    <strong>프로젝트 펀딩 현황</strong>
                </div>

                <div class="row row-12 mdashs_icon_wrap">
                    <div class="col-md-12 col-xs-6 col-md-3">
                        <div class="mdashs_icon nth-child1">
                            <i></i>
                            <em>목표금액</em>
                            <span class="webfont2">{{ parseInt(rewardData.tgAmount).toLocaleString() }}원</span>
                        </div>
                    </div>
                    <div class="col-md-12 col-xs-6 col-md-3">
                        <div class="mdashs_icon nth-child2">
                            <i></i>
                            <em>총 후원액</em>
                            <span class="webfont2">{{ parseInt(rewardData.cpSponsorAmount).toLocaleString() }}원</span>
                        </div>
                    </div>
                    <div class="col-md-12 col-xs-6 col-md-3">
                        <div class="mdashs_icon nth-child3">
                            <i></i>
                            <em>달성률</em>
                            <span class="webfont2">{{ rewardData.cpAchievement }}%</span>
                        </div>
                    </div>
                    <div class="col-md-12 col-xs-6 col-md-3">
                        <div class="mdashs_icon nth-child4">
                            <i></i>
                            <em>총 후원자수</em>
                            <span class="webfont2">{{ parseInt(rewardData.sponCnt).toLocaleString() }}명</span>
                        </div>
                    </div>
                </div>

                <hr class="big big_bg hidden-md hidden-lg" />

                <div class="my_dash_stitle text-left hidden-md hidden-lg">
                    <strong>모든 펀딩 상세 현황</strong>
                </div>

                <div class="my_dash_table my_dash_table_center mt15 hidden-md hidden-lg" v-if="dataConfirm">
                    <table class="table table-condensed-big table-fixed">
                        <colgroup>
                            <col style="width:20%;">
                            <col style="width:37%;">
                            <col style="width:22%;">
                            <col style="width:21%;">
                        </colgroup>
                        <thead>
                            <tr>
                                <th>이름</th>
                                <th>리워드명</th>
                                <th>펀딩상태</th>
                                <th>상세</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- Loop -->
                            <tr v-for="item in sponsorData">
                                <!-- <td>{{ item.spsRsName }}</td> -->
                                <td>{{ hide(item.memName, item.spsStatus)}}</td>
                                <td>{{ item.cbfTitle }}</td>
                                <td>{{ status(item.spsStatus) }}</td>
                                <td><a class="btn btn-xs btn-primary-outline" v-on:click="more(item)" :disabled="rewardData.cpEndStatus == '3'">더보기</a></td>
                            </tr>
                            <!-- //Loop -->
                        </tbody>
                    </table>
                </div>

                <!-- 페이징 -->
                <nav class="text-center hidden-md hidden-lg">
                    <paginate
                        :page-count="pageCount"
                        :class="'pagination'"
                        :click-handler="nextPage"
                        :force-page="forcePage"
                        >
                    </paginate>
                </nav>
                <!-- //페이징 -->

                <hr class="big big_bg" />

                <div class="graph_wrap">
                    <div class="row not-space">
                        <div class="col-sm-3">
                            <div class="my_dash_stitle text-left mt10 xs-mb15">
                                <strong>프로젝트 방문 통계</strong>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-2"></div>
                
                        <div class="col-sm-2 col-md-2">
                            <date-picker v-model="chartSearch.startDay" :date="chartSearch.startDay" v-if="chartDataConfirm" :min="chartSearch.startDay"></date-picker>
                        </div>
                        <div class="col-sm-2 col-md-2">
                            <date-picker v-model="chartSearch.endDay" :date="chartSearch.endDay" v-if="chartDataConfirm" :max="chartSearch.endDay"></date-picker>
                        </div>
                        <div class="col-sm-2 col-md-2">
                            <a class="btn btn-block btn-md btn-primary" v-on:click="getChartData">조회하기</a>
                        </div>   
                    </div>

                    <div class="tab-content mt20">
                        <lineChart v-if="gaDataConfirm"
                            :data="visitChartData"
                            :options="chartOptions">
                        </lineChart>

                        <lineChart v-if="chartDataConfirm"
                            :data="sponsorChartData"
                            :options="chartOptions">
                        </lineChart>

                        <lineChart v-if="chartDataConfirm"
                            :data="amountChartData"
                            :options="chartOptions">
                        </lineChart>

                         <div class="mt15 text-right">
                           <!--  <a class="btn btn-primary-outline" v-on:click="excel">EXCEL 다운로드</a> -->
                        </div>
                    </div>
                </div>

                <!-- 상세정보 -->
                <div id="mrDetail" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body">
                                <div class="rewards-modal-head">
                                    <h3 class="webfont2">리워드주문 상세정보</h3>
                                    <p><span>주문일시</span> {{ moreData.wdate }}</p>
                                </div>
                                <div class="rewards-modal-table">
                                    <!-- 주문자 정보 -->
                                    <table class="table table-fixed">
                                        <colgroup>
                                            <col style="width:38%;">
                                            <col style="width:62%;">
                                        </colgroup>
                                        <thead>
                                            <tr>
                                                <th colspan="2" class="first-child">주문자 정보</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>이름</td>
                                                <td>{{ hide(moreData.memName, moreData.spsStatus)}}</td>
                                            </tr>
                                            <tr>
                                                <td>E-MAIL(ID)</td>
                                                <td>{{ hide(moreData.memEmail, moreData.spsStatus)}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- //주문자 정보 -->

                                    <!-- 배송 정보 -->
                                    <table class="table table-fixed">
                                        <colgroup>
                                            <col style="width:38%;">
                                            <col style="width:62%;">
                                        </colgroup>
                                        <thead>
                                            <tr>
                                                <th colspan="2" class="first-child">배송 정보</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>수령자</td>
                                                <td>{{ hide(moreData.spsRsName, moreData.spsStatus)}}</td>
                                            </tr>
                                            <tr>
                                                <td>수령자 전화번호</td>
                                                <template>
                                                    <td v-if="moreData.spsStatus != '1'"> ... </td>
                                                    <td v-else>{{ moreData.spsRSMobileno}}</td>
                                                </template>
                                            </tr>
                                            <tr>
                                                <td class="td_hi">배송지</td>
                                                <template>
                                                    <td v-if="moreData.spsStatus != '1'"> ... </td>
                                                    <td v-else>{{ moreData.spsAddr1 }}, {{ moreData.spsAddr2 }}</td>
                                                </template>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- //배송 정보 -->

                                    <!-- 리워드 정보 -->
                                    <table class="table table-fixed">
                                        <colgroup>
                                            <col style="width:38%;">
                                            <col style="width:62%;">
                                        </colgroup>
                                        <thead>
                                            <tr>
                                                <th colspan="2" class="first-child">리워드 정보</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>리워드명</td>
                                                <td>{{ moreData.cbfTitle }}</td>
                                            </tr>
                                            <tr>
                                                <td>리워드 금액</td>
                                                <td>{{ parseInt(moreData.sbfUnitPrice).toLocaleString() }}원</td>
                                            </tr>
                                            <tr>
                                                <td>선택한 옵션</td>
                                                <td>{{ moreData.options }}</td>
                                            </tr>
                                            <tr>
                                                <td>추가펀딩 금액</td>
                                                <td>{{ parseInt(moreData.spsAmount).toLocaleString() }}원</td>
                                            </tr>
                                            <tr>
                                                <td>총 결제 예약금액</td>
                                                <td class="blue-800">{{ parseInt(moreData.spsTotAmount).toLocaleString() }}원</td>
                                            </tr>
                                            <tr>
                                                <td>결제상태</td>
                                                <td>
                                                    <template>
                                                        <span class="blue-800" v-if="moreData.spsStatus != '2'">{{status(moreData.spsStatus)}}</span>
                                                        <span class="red-800" v-else>{{status(moreData.spsStatus)}}</span>
                                                    </template>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_hi">제작자에 <br class="hidden-sm hidden-md hidden-lg" />요청사항</td>
                                                <td>{{ moreData.spsMiscInfo }}
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- //리워드 정보 -->
                                </div>

                                <div class="modal-footer text-center">
                                    <div class="row not-space">
                                        <div class="col-xs-4 col-xs-offset-4">
                                            <a class="btn btn-block btn-primary-outline" data-dismiss="modal">확인</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //상세정보 -->
            </div>
            `,
            props : ["rewardData", "sdate", "edate"],
            data : function() {
                return {
                    sponsorData : [],
                    pageCount : 0,
                    forcePage : 0,
                    duration : 1,
                    search : {
                        paging : {
			                page : "1",
			                count : "10"
                        },
                        cpCode : this.rewardData.cpCode,
                        name : '',
                        cbfIdx : '0',
                        status : '10',
                        startDay : moment(this.sdate, 'YYYY.MM.DD').format('YYYY-MM-DD'),
                        endDay : moment(this.edate, 'YYYY.MM.DD').format('YYYY-MM-DD'),
                    },
                    chartSearch : {
                        cpCode : this.rewardData.cpCode,
                        startDay : moment(this.rewardData.cpStartDate, 'YYYY.MM.DD').format('YYYY-MM-DD'),
                        endDay : moment(this.rewardData.cpEndDate, 'YYYY.MM.DD').format('YYYY-MM-DD'),
                        path : '/r/' + this.rewardData.cpAliasUrl
                    },
                    chartDataConfirm : false,
                    gaDataConfirm : false,
                    moreData : [],
                    dataConfirm : false,
                    visitChartData :  {
                        labels:  [],
                        datasets: [{
                            label: '방문자 수',
                            backgroundColor: 'red',
                            data: []
                          }]
                      },
                      sponsorChartData :  {
                        labels:  [],
                        datasets: [{
                            label: '일별 후원자수',
                            backgroundColor: 'orange',
                            data: []
                          }]
                      },
                      amountChartData :  {
                        labels:  [],
                        datasets: [{
                            label: '일별 펀딩금액',
                            backgroundColor: 'green',
                            data: []
                          }]
                      },
                      chartOptions : {
                          responsive: true, 
                          maintainAspectRatio: false
                      },
                }
            },
            components : {
                paginate : VuejsPaginate,
                lineChart : require('../../common/line-chart.js').default.component(),
                datePicker : require('../../common/date-picker.js').default.component(),
            },
            created : function() {
                this.load();
                this.checkDate();
            },
            methods : {
                load : function() {
                    var self = this;
                    axios.post('/data/view/reward/dashboard/sponsor', this.search)
                    .then(function (response) {
                        var result = response.data.rData.data;
                        var remainder = parseInt(response.data.rData.count % self.search.paging.count);
                        var remainderCount = 0;
                        if(remainder > 0) {
                            remainderCount++;
                        }
                        self.sponsorData = result;
                        self.dataConfirm = true;
                    })
                },
                nextPage : function(page) {
                    this.forcePage = page - 1;
                    this.search.paging.page = page;
                    this.load();
                },
                status : function(val) {
                    var result = "";
                    if(val == "0") {
                        result = "결제대기";
                    } else if (val == "1") {
                        result = "결제완료";
                    } else if (val == "2") {
                        result = "결제실패";
                    } else if (val == "3") {
                        result = "결제취소";
                    } else if (val == "9") {
                        result = "후원취소";
                    }
                    return result;
                },
                more : function(item) {
                    $('#mrDetail').modal('show');
                    this.moreData = item;
                },
                durationChange : function(val) {
                    this.duration = val;
                },
                checkDate : function() {
                    var now = moment().format("YYYY-MM-DD");
                    if (!moment(now).isAfter(this.rewardData.cpEndDate)) {
                        this.chartSearch.endDay = now;
                    }
                    this.getChartData();
                },
                getChartData : function() {
                    var self = this;
                    self.chartDataConfirm = false;
                    this.getGaChartData();
                    axios.post('/data/view/reward/dashboard/chart', this.chartSearch)
                        .then(function (response) {
                            var result = response.data;
                            if(result.rCode == "0000") {
                                var day = [];
                                for (var i = 0; i < result.rData.chartDate.length; i++) {
                                    day.push(result.rData.chartDate[i].day);
                                    self.sponsorChartData.datasets[0].data.push(result.rData.sponsorChartData[i].value);
                                    self.amountChartData.datasets[0].data.push(result.rData.amountChartData[i].value);
                                }
                                self.sponsorChartData.labels = day;
                                self.amountChartData.labels = day;

                                self.chartDataConfirm = true;
                            }  
                        })
                },
                getGaChartData : function() {
                    var self = this;
                    axios.post('/ga/info', this.chartSearch)
                        .then(function (response) {
                            var result = response.data;
                            if(result.rCode == "0000") {
                                for (var i = 0; i < result.rData.chartDate.length; i++) {
                                    self.visitChartData.labels.push(result.rData[i].day);
                                    self.visitChartData.datasets[0].data.push(result.rData[i].value);
                                }
                                self.gaDataConfirm = true;
                            }
                        })
                },
                excel : function() {
                    var excelSearch = {
                        cpCode : this.chartSearch.cpCode,
                        startDate : this.chartSearch.startDay,
                        endDate : this.chartSearch.endDay,
                        cpAliasUrl : this.rewardData.cpAliasUrl
                    };
                    
                     $.fileDownload('/dashboard/reward/static/excel', {
                        httpMethod : 'GET',
                        data: excelSearch
                    })
                },
                 hide : function(name, status) {
                    name = name + "";
                    if (((this.rewardData.cpEndStatus != '2') || (this.rewardData.cpEndStatus == '2' && status != '1')) && name != "") {
                        var returnText = name.slice(0, 1);
                        for (var i = 0; i < name.length - 1; i++) {
                            returnText += '*';
                        }
                        return returnText;
                    } else {
                        return  name;
                    }
                }
            }
        }
    }
}
export default new DashboardRewardMain