class DashboardRewardMain {
    component() {
        return {
            template : `
            <div>
                <div class="my_dash_stitle">
                    <strong v-if="rewardData.cpEndStatus == 0">현재 프로젝트가 시작 전입니다.</strong>
                </div>
                <template v-if="dataConfirm">
                    <form class="my_dash_panel" name="searchForm">
                        <fieldset>
                            <div class="my_dash_panel_body" style="font-size:18px;">
                                <div class="row row-mobile-n">
                                    <div class="my_dash_total">
                                        <div class="row">
                                            <div class="col-xs-9 text-right">
                                                <strong>총 펀딩 금액</strong>
                                            </div>
                                            <div class="col-xs-2 text-right blue-800">
                                                <strong>{{ parseInt(fundingSum).toLocaleString() }}원</strong>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-9 text-right">
                                                <strong>리워드 주문 금액</strong>
                                            </div>
                                            <div class="col-xs-2 text-right blue-800">
                                                <strong>{{ parseInt(rewardSum).toLocaleString() }}원</strong>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-9 text-right">
                                                <strong>추가 펀딩 금액</strong>
                                            </div>
                                            <div class="col-xs-2 text-right blue-800">
                                                <strong>{{ parseInt(addSum).toLocaleString() }}원</strong>
                                            </div>
                                        </div>
                                    </div>                               
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </template>
                <div class="form-group row-mobile-n">
                    <div class="col-xs-12 control-label control-label-big">
                        <div class="text-left">
                            리워드 보기
                        </div>
                    </div>
                    <!-- Loop -->
                        <div class="col-sm-4" v-for="item in benefitData">
                            <div class="st-items st-items-loop">
                                <a href="javascript:void(0)" class="st-link">바로가기</a>
                                <div class="row not-space">
                                    <div class="col-xs-6 col-sm-12">
                                        <div class="st-items-su">
                                            <strong>{{ parseInt(item.cbfAmount).toLocaleString() }}원 펀딩</strong>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-12 xs-text-right">
                                        <div class="st-items-btn">
                                            <span class="btn btn-sm btn-danger" v-if="item.cbfQty > 99998">무제한</span>
                                            <span class="btn btn-sm btn-danger" v-if="item.cbfQty < 99998">{{ item.cbfRemQty }}개 남음</span>
                                            <span class="btn btn-sm btn-danger-outline">{{ parseInt(item.sumQty).toLocaleString() }}개 펀딩</span>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <dl>
                                            <dt>리워드명</dt>
                                            <dd><strong>{{ item.cbfTitle }}</strong></dd>
                                            <dd class="summury">{{ item.cbfInfo }}</dd>
                                        </dl>
                                    </div>
                                </div>

                                <div class="st-items-option-control">
                                    <div class="row">
                                        <div class="col-xs-5 text-left">
                                            총 펀딩금액
                                        </div>
                                        <div class="col-xs-7 text-right">
                                            <strong class="red-800">{{ parseInt(item.spsTotAmount).toLocaleString() }}</strong>원
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!-- //Loop -->
                    </div>
                </div>
            </div>
            `,
            props : ['rewardData'],
            data : function() {
                return {
                    sponsorData : [],
                    benefitData : [],
                    fundingSum : 0,
                    rewardSum : 0,
                    addSum : 0,
                    dataConfirm : false,
                }
            },
            components : {
                
            },
            created : function() {
                if(this.rewardData.cpEndStatus != 0) {
                    this.load();
                }
            },
            methods : {
                load : function() {
					var self = this;
                    axios.post('/data/view/reward/dashboard/orderstatus', {cpCode : this.rewardData.cpCode})
                    .then(function (response) {
                        var result = response.data.rData;
                        self.benefitData = response.data.rData;
                        self.calculate();
                        self.dataConfirm = true;
                    })
                },
                calculate : function() {
                    for(var i=0; i < this.benefitData.length; i++) {
                        this.fundingSum += parseInt(this.benefitData[i].spsTotAmount);
                        this.addSum += parseInt(this.benefitData[i].spsAmount);    

                    }
                    this.fundingSum = this.fundingSum + this.addSum;
                    this.rewardSum = (parseInt(this.fundingSum) - parseInt(this.addSum));
                },
            }
        }
    }
}
export default new DashboardRewardMain