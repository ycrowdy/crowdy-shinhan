class DashboardRewardSponsor {
    component() {
        return {
            template : `
            <div class="my_dash_wrap hidden-xs hidden-sm">
                <form class="my_dash_panel" name="searchForm" action="#">
                    <fieldset>
                        <div class="my_dash_panel_body">
                            <div class="row row-mobile-n">
                                <div class="col-xs-1"></div>
                                <div class="col-xs-2">
                                    <div class="control-label text-left mb5 pt0">
                                        이름조회
                                    </div>
                                </div>
                                <div class="col-xs-2">
                                    <div class="control-label text-left mb5 pt0">
                                        리워드명 조회
                                    </div>
                                </div>
                                <div class="col-xs-2">
                                    <div class="control-label text-left mb5 pt0">
                                        결제상태 조회
                                    </div>
                                </div>
                                <div class="col-xs-5">
                                    <div class="control-label text-left mb5 pt0">
                                        기간 조회
                                    </div>
                                </div>
                            </div>
                            <div class="row row-mobile-n">
                                <div class="col-xs-1"></div>
                                <div class="col-xs-2" >
                                    <input type="text" class="form-control" :disabled="rewardData.cpEndStatus == '3'" v-on:keyup.enter="load" v-model="search.name"/>
                                </div>
                                <div class="col-xs-2">
                                    <name-select :options="rewardOptions" v-on:change="nameChange" v-model="search.cbfIdx" v-if="dataConfirm"></name-select>
                                </div>
                                <div class="col-xs-2">
                                    <status-select :options="paymentStatusOption" v-on:change="change" v-model="search.status"></status-select>
                                </div>
                                <div class="col-xs-2">
                                    <date-picker v-model="search.startDay" :date="search.startDay" v-if="dataConfirm" :min="search.startDay"></date-picker>
                                </div>
                                <div class="col-xs-2">
                                    <date-picker v-model="search.endDay" :date="search.endDay" v-if="dataConfirm" :max="search.endDay"></date-picker>
                                </div>
                                <div class="col-xs-1"></div>
                            </div>
                            <div class="row row-mobile-n mt20">
                                <div class="col-xs-2 col-xs-offset-5">
                                    <a class="btn btn-block btn-primary" v-on:click="sponsorSearch">조회하기</a>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>

                <div class="my_dash_stitle text-left">
                    <template>
                        <strong v-if="rewardData.cpEndStatus != '2'">프로젝트 종료 후 결제에 성공한 참여자에 한해 모든 정보가 공개됩니다</strong>
                        <strong v-else>모든 펀딩 상세 현황을 확인해보세요</strong>
                    </template>

                    
                    <div class="mt15 mb40">
                        <a href="javascript:void(0)" class="btn btn-primary-outline" v-on:click="excel" :disabled="rewardData.cpEndStatus != '2'">EXCEL 다운로드</a>
                    </div>
                </div>

                <div class="my_dash_table my_dash_table_center">
                    <table class="table table-condensed-big table-fixed">
                        <colgroup>
                            <col style="width:16%;">
                            <col style="width:8%;">
                            <col style="width:23%;">
                            <col style="width:11%;">
                            <col style="width:12%;">
                            <col style="width:11%;">
                            <col style="width:9%;">
                            <col style="width:10%;">
                        </colgroup>
                        <thead>
                            <tr>
                                <th>주문일시</th>
                                <th>이름</th>
                                <th>리워드명</th>
                                <th>리워드금액</th>
                                <th>추가펀딩금액</th>
                                <th>총 결제금액</th>
                                <th>결제상태</th>
                                <th>상세</th>
                            </tr>
                        </thead>
                        <tbody v-if="dataConfirm">
                            <tr v-for="item in sponsorData">
                                <td>{{ item.wdate }}</td>
                                <td>{{ hide(item.memName, item.spsStatus)}}</td>
                                <td>{{ item.cbfTitle }}</td>
                                <td>{{ item.sbfUnitPrice }}</td>
                                <td>{{ parseInt(item.spsAmount).toLocaleString() }}원</td>
                                <td>{{ parseInt(item.spsTotAmount).toLocaleString() }}원</td>
                                <template>
                                    <td v-if="rewardData.cpEndStatus == '3'"> - </td>
                                    <td v-else>{{ status(item.spsStatus) }}</td>
                                </template>
                                <td><a class="btn btn-sm btn-primary-outline" v-on:click="more(item)" :disabled="rewardData.cpEndStatus == '3'">더보기</a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <!-- 페이징 -->
                <nav class="text-center">
                    <paginate
                        :page-count="pageCount"
                        :class="'pagination'"
                        :click-handler="nextPage"
                        :force-page="forcePage"
                        >
                    </paginate>
                </nav>
                <!-- //페이징 -->

                <!-- 상세정보 -->
                <div id="mrDetail" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body">
                                <div class="rewards-modal-head">
                                    <h3 class="webfont2">리워드주문 상세정보</h3>
                                    <p><span>주문일시</span> {{ moreData.wdate }}</p>
                                </div>
                                <div class="rewards-modal-table">
                                    <!-- 주문자 정보 -->
                                    <table class="table table-fixed">
                                        <colgroup>
                                            <col style="width:38%;">
                                            <col style="width:62%;">
                                        </colgroup>
                                        <thead>
                                            <tr>
                                                <th colspan="2" class="first-child">주문자 정보</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>이름</td>
                                                <td>{{ hide(moreData.memName, moreData.spsStatus)}}</td>
                                            </tr>
                                            <tr>
                                                <td>E-MAIL(ID)</td>
                                                <td>{{ hide(moreData.memEmail, moreData.spsStatus) }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- //주문자 정보 -->

                                    <!-- 배송 정보 -->
                                    <table class="table table-fixed">
                                        <colgroup>
                                            <col style="width:38%;">
                                            <col style="width:62%;">
                                        </colgroup>
                                        <thead>
                                            <tr>
                                                <th colspan="2" class="first-child">배송 정보</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>수령자</td>
                                                <td>{{ hide(moreData.spsRsName, moreData.spsStatus)}}</td>
                                            </tr>
                                            <tr>
                                                <td>수령자 전화번호</td>
                                                <template>
                                                    <td v-if="moreData.spsStatus != '1'"> ... </td>
                                                    <td v-else>{{ moreData.spsRSMobileno}}</td>
                                                </template>
                                            </tr>
                                            <tr>
                                                <td class="td_hi">배송지</td>
                                                 <template>
                                                    <td v-if="moreData.spsStatus != '1'"> ... </td>
                                                    <td v-else>{{ moreData.spsAddr1 }}, {{ moreData.spsAddr2 }}</td>
                                                </template>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- //배송 정보 -->

                                    <!-- 리워드 정보 -->
                                    <table class="table table-fixed">
                                        <colgroup>
                                            <col style="width:38%;">
                                            <col style="width:62%;">
                                        </colgroup>
                                        <thead>
                                            <tr>
                                                <th colspan="2" class="first-child">리워드 정보</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>리워드명</td>
                                                <td>{{ moreData.cbfTitle }}</td>
                                            </tr>
                                            <tr>
                                                <td>리워드 금액</td>
                                                <td>{{ moreData.sbfUnitPrice }}</td>
                                            </tr>
                                            <tr>
                                                <td>구매 수</td>
                                                <td>{{ moreData.sbfQty }}</td>
                                            </tr>
                                            <tr>
                                                <td>선택한 옵션</td>
                                                <td>{{ moreData.sbfBenefit }}</td>
                                            </tr>
                                            <tr>
                                                <td>추가펀딩 금액</td>
                                                <td>{{ parseInt(moreData.spsAmount).toLocaleString() }}원</td>
                                            </tr>
                                            <tr>
                                                <td>총 결제 예약금액</td>
                                                <td class="blue-800">{{ parseInt(moreData.spsTotAmount).toLocaleString() }}원</td>
                                            </tr>
                                            <tr>
                                                <td>결제상태</td>
                                                <td>
                                                    <template>
                                                        <span class="blue-800" v-if="moreData.spsStatus != '2'">{{status(moreData.spsStatus)}}</span>
                                                        <span class="red-800" v-else>{{status(moreData.spsStatus)}}</span>
                                                    </template>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_hi">제작자에 <br class="hidden-sm hidden-md hidden-lg" />요청사항</td>
                                                <td>{{ moreData.spsMiscInfo }}
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- //리워드 정보 -->
                                </div>

                                <div class="modal-footer text-center">
                                    <div class="row not-space">
                                        <div class="col-xs-4 col-xs-offset-4">
                                            <a class="btn btn-block btn-primary-outline" data-dismiss="modal">확인</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //상세정보 -->

                <div class="enot_wrap hidden-md hidden-lg">
                    <div class="row">
                        <div class="col-sm-2">
                        </div>
                        <div class="col-sm-4">
                            <div class="enot_logo"></div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="enot_oops mt50 xs-mt25"></div>
                            <div class="enot_text">
                                앗! 죄송해요..
                                <p>모바일은 지원하지 않습니다. ㅠ_ㅠ</p>
                            </div>
                            <div class="btn_wrap">
                                <a href="/" class="btn btn-primary-outline">메인화면으로</a>
                            </div>
                        </div>
                        <div class="col-md-2">
                        </div>
                    </div>
                </div>
            </div>
            `,
            props : ["rewardData", "sdate", "edate"],
            data : function() {
                return {
                    sponsorData : [],
                    pageCount : 0,
                    forcePage : 0,
                    search : {
                        paging : {
			                page : "1",
			                count : "10"
                        },
                        cpCode : this.rewardData.cpCode,
                        name : '',
                        cbfIdx : '0',
                        status : '10',
                        startDay : moment(this.sdate, 'YYYY.MM.DD').format('YYYY-MM-DD'),
                        endDay : moment(this.edate, 'YYYY.MM.DD').format('YYYY-MM-DD'),
                    },
                    rewardOptions : [{
                        id:"0", text:"전체"
                    }],
                    paymentStatusOption : [
                        {id:"10", text:"전체보기"},
                        {id:"0", text:"결제대기"},
                        {id:"1", text:"결제완료"},
                        {id:"2", text:"결제실패"},
                        {id:"9", text:"후원취소"},
                    ],
                    dataConfirm : false,
                    //더보기 데이터
                    moreData : []
                }
            },
            components : {
                paginate : VuejsPaginate,
                nameSelect : require('../../common/select.js').default.component(),
                statusSelect : require('../../common/select.js').default.component(),
                datePicker : require('../../common/date-picker.js').default.component(),
            },
            created : function() {
                this.load();
            },
            methods : {
                load : function() {
                    var self = this;
                    axios.post('/data/view/reward/dashboard/sponsor', this.search)
                        .then(function (response) {
                            var result = response.data.rData.data;
                            var remainder = parseInt(response.data.rData.count % self.search.paging.count);
                            var remainderCount = 0;
                            if(remainder > 0) {
                                remainderCount++;
                            }
                            self.pageCount = (parseInt(response.data.rData.count / self.search.paging.count)) + remainderCount;
                            self.sponsorData = result;

                            self.getBenefitList();
                    })
                },
                //리워드 목록
                getBenefitList : function() {
					var self = this;	
					axios.post('/data/save/reward/info/benefit', {cpCode : this.search.cpCode})
	                    .then(function(response){
                            var result = response.data;
	                     	if(result.rCode == "0000") {                              
                                for (var i = 0; i < result.rData.length; i++) {
		                    		var option = {
		                    				id : result.rData[i].cpBenefitCode,
		                    				text : result.rData[i].cpBenefitInfo
		                    			};
                                    self.rewardOptions.push(option);
                                }
                                self.dataConfirm = true;
	                    	} else {
	                    		noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
	                    	}
	                })
				},
                sponsorSearch : function() {
                    this.load();
                },
                nextPage : function(page) {
                    this.forcePage = page - 1;
                    this.search.paging.page = page;
                    this.load();
                },
                status : function(val) {
                    var result = "";
                    if(val == "0") {
                        result = "결제대기";
                    } else if (val == "1") {
                        result = "결제완료";
                    } else if (val == "2") {
                        result = "결제실패";
                    } else if (val == "3") {
                        result = "결제취소";
                    } else if (val == "9") {
                        result = "후원취소";
                    }
                    return result;
                },
                change : function(val) {
                    this.search.status = val;
                },
                nameChange : function(val) {
                    this.search.cbfIdx = val;
                },
                excel : function() {
                    var excelSearch = {
                        cpCode : this.rewardData.cpCode,
                        startDate : moment(this.sdate, 'YYYY.MM.DD').format('YYYY-MM-DD'),
                        endDate : moment(this.edate, 'YYYY.MM.DD').format('YYYY-MM-DD'),
                        cpAliasUrl : this.rewardData.cpAliasUrl,
                        name : this.search.name,
                        cbfIdx : this.search.cbfIdx,
                        status : '1',
                    };
                    
                    $.fileDownload('/dashboard/reward/sponsor/excel', {
                        httpMethod : 'POST',
                        data: excelSearch
                    })
                },
                more : function(item) {
                    $('#mrDetail').modal('show');
                    this.moreData = item;
                },
                hide : function(name, status) {
                    name = name + "";
                    if (((this.rewardData.cpEndStatus != '2') || (this.rewardData.cpEndStatus == '2' && status != '1')) && name != "") {
                        var returnText = name.slice(0, 1);
                        for (var i = 0; i < name.length - 1; i++) {
                            returnText += '*';
                        }
                        return returnText;
                    } else {
                        return  name;
                    }
                }
            }
        }
    }
}
export default new DashboardRewardSponsor