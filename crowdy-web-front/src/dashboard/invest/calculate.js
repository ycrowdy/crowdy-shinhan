class DashboardInvestCalculate {
    component() {
        return {
            template : `

            <div>
                <div class="my_dash_stitle">
                    <strong v-if="investData.pjEndStatus == 1">현재 프로젝트가 진행 중입니다.</strong>
                    <strong v-if="investData.pjEndStatus == 2 && investData.pjRateStatus == 1">프로젝트 내역</strong>
                    <strong v-if="investData.pjEndStatus == 2 && investData.pjRateStatus == 0">배정 작업을 진행 중입니다. <br />배정 완료 후 정산 내역을 보실 수 있습니다.</strong>
                    <strong v-if="investData.pjEndStatus == 3">프로젝트가 투자 목표 금액을 달성하지 못했습니다.</strong>
                </div>

                <template v-if="dataConfirm">
                    <div class="my_dash_table mt10">
                        <table class="table table-fixed">
                            <colgroup>
                                <col style="width:24%;">
                                <col style="width:26%;">
                                <col style="width:24%;">
                                <col style="width:26%;">
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td class="first-child">프로젝트 명</td>
                                    <td colspan="3"><strong>{{calculateData.pjTitle}}</strong></td>
                                </tr>
                                <tr>
                                    <td class="first-child">프로젝트 기간</td>
                                    <td colspan="3"><strong>{{calculateData.pjStartDate}} - {{calculateData.pjEndDate}}</strong></td>
                                </tr>
                                <tr>
                                    <td class="first-child">투자 목표금액</td>
                                    <td colspan="3"><strong>{{parseInt(calculateData.targetAmount).toLocaleString()}}원</strong></td>
                                </tr>
                                <tr>
                                    <td class="first-child">총 청약금액</td>
                                    <td><strong>{{parseInt(calculateData.pjCurrentAmount).toLocaleString()}}원</strong></td>
                                    <td class="first-child">총 투자자수</td>
                                    <td><strong>{{calculateData.pjInvestorCount}}명</strong></td>
                                </tr>
                                <tr>
                                    <td class="first-child">총 배정금액</td>
                                    <td><strong>{{parseInt(calculateData.assConTotalAmount).toLocaleString()}}원</strong></td>
                                    <td class="first-child">총 배정자수</td>
                                    <td><strong>{{calculateData.assCount}}명</strong></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="my_dash_stitle mt40">
                        <strong>정산 내역</strong>
                    </div>

                    <div class="my_dash_table mt10">
                        <table class="table table-fixed">
                            <colgroup>
                                <col style="width:24%;">
                                <col style="width:26%;">
                                <col style="width:50%;">
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td class="first-child">총 배정금액</td>
                                    <td colspan="2"><strong>{{parseInt(calculateData.assConTotalAmount).toLocaleString()}}원</strong></td>
                                </tr>
                                <tr>
                                    <td class="first-child">기본 착수금</td>
                                    <td colspan="2"><strong>2,475,000원</strong></td>
                                </tr>
                                <tr>
                                    <td class="first-child">성공 수수료</td>
                                    <td><strong>{{parseInt(calculateData.assConTotalAmount * 0.055).toLocaleString()}}원</strong></td>
                                    <td class="last-child">(총 배정 금액의 5.5%, VAT 포함)</td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="my_dash_total">
                            <div class="row not-space">
                                <div class="col-xs-10 text-right">
                                    <strong>총 수수료</strong>
                                </div>
                                <div class="col-xs-2 text-right blue-800">
                                    <strong>{{parseInt((calculateData.assConTotalAmount * 0.055) + 2475000).toLocaleString()}}원</strong>
                                </div>
                            </div>
                            <p>(총 수수료 = 기본 착수금 + 성공 수수료, VAT 포함)</p>
                        </div>
                    </div>

                    <div class="mt15 text-right">
                        <button type="button" class="btn btn-primary-outline" v-on:click="excel">EXCEL 다운로드</button>
                    </div>
                </template>
            </div>
            `,
            props : ['investData'],
            data : function() {
                return {
                    calculateData : [],
                    search : {
                        pjCode : this.investData.pjCode
                    },
                    dataConfirm : false,
                }
            },
            created : function() {
                // 성공으로 끝는 프로젝트여야 함
                if (this.investData.pjEndStatus == '2') {
                    this.load();    
                }
            },
            methods : {
                load : function() {
                    var self = this;

                     // 프로젝트가 종료되고, 배정이 확정되어야 조회 가능
                    if (this.investData.pjEndStatus == '2' && this.investData.pjRateStatus == '1') {
                        $('.page-loader-more').fadeIn('')
                        axios.post('/data/view/invest/dashboard/calculate', this.search)
                        .then(function (response) {
                            $('.page-loader-more').fadeOut('');
                            var result = response.data.rData;
                            self.calculateData = result;
                            self.dataConfirm = !self.dataConfirm;   
                        })
                    }
                },
                excel : function() {
                    $.fileDownload('/dashboard/invest/calculate/excel', {
                        httpMethod : 'POST',
                        data: { pjCode : this.investData.pjCode }
                    })
                }
            }
        }
    }
}
export default new DashboardInvestCalculate