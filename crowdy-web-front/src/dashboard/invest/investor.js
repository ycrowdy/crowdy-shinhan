class DashboardInvestInvestor {
    component() {
        return {
            template : `
            <div>
                <form class="my_dash_panel" name="searchForm">
                    <fieldset>
                        <div class="my_dash_panel_body">
                            <div class="row row-mobile-n">
                                <div class="col-xs-1"></div>
                                <div class="col-xs-3">
                                    <div class="control-label text-left mb5 pt0">
                                        청약상태별 조회
                                    </div>
                                </div>
                                <div class="col-xs-5">
                                    <div class="control-label text-left mb5 pt0">
                                        기간 조회
                                    </div>
                                </div>
                            </div>
                            <div class="row row-mobile-n">
                                <div class="col-xs-1"></div>
                                <div class="col-xs-3">
                                    <status-select :options="investorStatusOption" v-on:change="change" v-model="search.investorStatus"></status-select>
                                </div>
                                <div class="col-xs-2">
                                    <date-picker v-model="search.pjStartDate" :date="search.pjStartDate" v-if="dataConfirm" :min="startDate"></date-picker>
                                </div>
                                <div class="col-xs-2">
                                    <date-picker v-model="search.pjEndDate" :date="search.pjEndDate" v-if="dataConfirm" :max="endDate"></date-picker>
                                </div>
                                <div class="col-xs-3">
                                    <div class="btn btn-block btn-md btn-primary" v-on:click="searchInvestor">조회하기</div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>

                <div class="my_dash_stitle text-left">
                    <strong>투자자 현황을 확인해보세요</strong>
                </div>

                <div class="my_dash_table my_dash_table_center">
                    <table class="table table-condensed-big table-fixed">
                        <colgroup>
                            <col style="width:7%;" class="hidden-md">
                            <col style="width:15%;">
                            <col style="width:13%;">
                            <col style="width:12%;">
                            <col style="width:6%;">
                            <col style="width:8%;">
                            <col style="width:18%;">
                        </colgroup>
                        <thead>
                            <tr>
                                <th class="hidden-md">청약번호</th>
                                <th>투자(청약)일시</th>
                                <th>청약자명</th>
                                <th>투자금액</th>
                                <th>청약수량</th>
                                <th>청약상태</th>
                                <th>청약금상태</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- Loop -->
                            <tr v-for="item in investorData">
                                <td class="hidden-md">{{item.investorIdx}}</td>
                                <td>{{item.wdate}}</td>
                                <td>{{ hide(item.memName, item.investorStatus) }}</td>
                                <td>{{parseInt(item.amount).toLocaleString()}}원</td>
                                <td>{{item.investorAssignQty}}</td>
                                <td>{{item.investorStatus | status }}</td>
                                <td>{{ setRefundText(item.investorStatus, item.investorRefund) }}</td>
                            </tr>
                            <!-- //Loop -->

                        </tbody>
                    </table>
                </div>

                <!-- 페이징 -->
                <nav class="text-center">
                    <paginate
                        :page-count="pageCount"
                        :class="'pagination'"
                        :click-handler="nextPage"
                        :force-page="forcePage"
                        >
                    </paginate>
                </nav>
                <!-- //페이징 -->
            </div>
            `,
            props : ['investData'],
            data : function() {
                return {
                    investorData : [],

                    pageCount : 0,
                    forcePage : 0,

                    search : {
                        paging : {
                            page : "1",
                            count : "10"
                        },
                        pjCode : this.investData.pjCode,
                        investorStatus : '전체보기',
                        pjStartDate : moment(this.investData.pjStartDate, 'YYYY.MM.DD').format('YYYY-MM-DD'),
                        pjEndDate : moment(this.investData.pjEndDate, 'YYYY.MM.DD').format('YYYY-MM-DD')
                    },
                    dataConfirm : false,
                    //IVSS00 : 청약완료, IVSS01 : 주식배정중, IVSS02 : 배정완료, IVSS03 : 청약취소, IVSS04 : 배정탈락, IVSS05 : 투자모금실패, IVSS06 : 청약부분완료
                    investorStatusOption : [
                        {id:"전체보기", text:"전체보기"},
                        {id:"IVSS00", text:"청약완료"},
                        {id:"IVSS06", text:"청약부분완료"},
                        {id:"IVSS03", text:"청약취소"},
                        {id:"IVSS01", text:"주식배정중"},
                        {id:"IVSS02", text:"배정완료"},
                        {id:"IVSS04", text:"배정탈락"},
                        {id:"IVSS05", text:"투자모금실패"},
                    ],
                }
            },
            components : {
                paginate : VuejsPaginate,
                statusSelect : require('../../common/select.js').default.component(),
                datePicker : require('../../common/date-picker.js').default.component(),
            },
            created : function() {
                this.load();
            },
            computed : {
                startDate : function() {
                    return moment(this.investData.pjStartDate, 'YYYY.MM.DD').format('YYYY-MM-DD');
                },
                endDate : function() {
                    return moment(this.investData.pjEndDate, 'YYYY.MM.DD').format('YYYY-MM-DD');
                },
            },
            methods : {
                load : function() {
                    var self = this;
                    this.investorData = [];

                    var request = _.cloneDeep(this.search);
                    if (request.investorStatus == '전체보기') {
                        request.investorStatus = '';
                    }
                    $('.page-loader-more').fadeIn('')
                    axios.post('/data/view/invest/dashboard/investor', request)
                        .then(function (response) {
                            $('.page-loader-more').fadeOut('');
                            var result = response.data.rData.investorData;
                            var remainder = parseInt(response.data.rData.count % self.search.paging.count);
                            var remainderCount = 0;
                            if(remainder > 0) {
                                remainderCount++;
                            }
                            self.pageCount = (parseInt(response.data.rData.count / self.search.paging.count)) + remainderCount;
                            self.investorData = result;

                            self.dataConfirm = true;
                        })
                },
                searchInvestor : function() {
                    this.pageCount = 0;
                    this.forcePage = 0;

                    this.search.paiging = {
                            page : "1",
                            count : "10"
                    };

                    this.load();
                },
                nextPage : function(page) {
                    this.forcePage = page - 1;
                    this.search.paging.page = page;
                    this.load();
                },
                change : function(val) {
                    this.search.investorStatus = val;
                },
                hide : function(name, status) {
                    if (((this.investData.pjEndStatus != '2') || (this.investData.pjEndStatus == '2' && status != 'IVSS02')) && name != "") {
                        var returnText = name.slice(0, 1);
                        for (var i = 0; i < name.length - 1; i++) {
                            returnText += '*';
                        }
                        return returnText;
                    } else {
                        return  name;
                    }
                },
                setRefundText : function(status, refund) {
                    //IVSS91 : 청약금 환불예정, IVSS99 : 청약취소 완료, IVSS92 : 청약금 환불이체예정
                    var result = "";
                    if(refund == "IVSS91") {
                        result = "청약금 환불예정";
                    } else if (refund == "IVSS99") {
                        result = "청약취소 완료";
                    } else if (refund == "IVSS92") {
                        result = "청약금 환불이체예정";
                    } else {
                        if (status == "IVSS06") {
                            result = "청약진행중";
                        } else {
                            result = "이체완료";    
                        }
                    }
                    return result;
                }
            },
            filters : {
                status : function(val) {
                    //IVSS00 : 청약완료, IVSS01 : 주식배정중, IVSS02 : 배정완료, IVSS03 : 청약취소, IVSS04 : 배정탈락, IVSS05 : 투자모금실패, IVSS06 : 청약부분완료
                    var result = "";
                    if(val == "IVSS00") {
                        result = "청약완료";
                    } else if (val == "IVSS01") {
                        result = "주식배정중";
                    } else if (val == "IVSS02") {
                        result = "배정완료";
                    } else if (val == "IVSS03") {
                        result = "청약취소";
                    } else if (val == "IVSS04") {
                        result = "배정탈락";
                    } else if (val == "IVSS05") {
                        result = "투자모금실패";
                    } else if (val == "IVSS06") {
                        result = "청약부분완료";
                    }
                    return result;
                },
                refund : function(val) {
                    //IVSS91 : 청약금 환불예정, IVSS99 : 청약취소 완료, IVSS92 : 청약금 환불이체예정
                    var result = "";
                    if(val == "IVSS91") {
                        result = "청약금 환불예정";
                    } else if (val == "IVSS99") {
                        result = "청약취소 완료";
                    } else if (val == "IVSS92") {
                        result = "청약금 환불이체예정";
                    } else {
                        result = "이체완료";
                    }
                    return result;
                }
            }
        }
    }
}
export default new DashboardInvestInvestor