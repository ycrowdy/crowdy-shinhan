class DashboardInvestAssign {
    component() {
        return {
            template : `
            <div>
                <div class="my_dash_stitle text-center" v-if="dataConfirm && (assignData == null || assignData.length == 0)">
                    <strong>배정 내역이 없습니다.</strong>
                </div>

                <template v-if="dataConfirm && assignData.length > 0">
                    <div class="my_dash_stitle text-left">
                        <strong>배정내역 현황을 확인해보세요</strong>
                    </div>

                    <div class="my_dash_table my_dash_table_center">
                        <table class="table table-condensed-big table-fixed">
                            <!-- <colgroup>
                                <col style="width:15%;">
                                <col style="width:11%;">
                                <col style="width:10%;">
                                <col style="width:22%;">
                                <col style="width:8%;">
                                <col style="width:9%;">
                                <col style="width:11%;">
                                <col style="width:8%;">
                            </colgroup> -->
                            <thead>
                                <tr>
                                    <th>배정일</th>
                                    <th>투자자명</th>
                                    <th>이메일</th>
                                    <th>휴대폰번호</th>
                                    <th>주소</th>
                                    <th>생년월일</th>
                                    <th>배정수량</th>
                                    <th>배정금액</th>
                                    <th>투자자 유형</th>
                                    <th>배정상태</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- Loop -->
                                <tr v-for="(item, index) in assignData">
                                    <td>{{item.assignDate}}</td>
                                    <td>{{item.memName}}</td>
                                    <td>{{item.memEmail}}</td>
                                    <td></td>
                                    <td></td>
                                    <td>{{item.memInvestorBirth}}</td>
                                    <td>{{item.investAssignQty}} 중 {{item.investAssQty}} 성공</td>
                                    <td>{{parseInt(item.amount).toLocaleString()}}원</td>
                                    <td>{{item.memInvestorType}}</td>
                                    <td>{{item.investorStatus | status }}</td>
                                </tr>
                                <!-- //Loop -->
                            </tbody>
                        </table>
                    </div>

                    <!-- 페이징 -->
                    <nav class="text-center">
                        <paginate
                            :page-count="pageCount"
                            :class="'pagination'"
                            :click-handler="nextPage"
                            :force-page="forcePage"
                            >
                        </paginate>
                    </nav>
                    <!-- //페이징 -->

                     <div class="mt15 text-right">
                        <button type="button" class="btn btn-primary-outline" v-on:click="excel">EXCEL 다운로드</button>
                    </div>
                </template>
            </div>
            `,
             props : ['investData'],
            data : function() {
                return {
                    assignData : [],

                    pageCount : 0,
                    forcePage : 0,

                    search : {
                        paging : {
                            page : "1",
                            count : "10"
                        },
                        pjCode : this.investData.pjCode
                    },
                    dataConfirm : false,
                    
                }
            },
            components : {
                paginate : VuejsPaginate
            },
            created : function() {
                this.load();
            },
            methods : {
                load : function() {
                    var self = this;

                    // 프로젝트가 종료되고, 배정이 확정되어야 조회 가능
                    if ((this.investData.pjEndStatus == '2' || this.investData.pjEndStatus == '3') && this.investData.pjRateStatus == '1') {
                        $('.page-loader-more').fadeIn('')
                        axios.post('/data/view/invest/dashboard/assign', this.search)
                            .then(function (response) {
                                $('.page-loader-more').fadeOut('');
                                var result = response.data.rData.assignData;
                                var remainder = parseInt(response.data.rData.count % self.search.paging.count);
                                var remainderCount = 0;
                                if(remainder > 0) {
                                    remainderCount++;
                                }
                                self.pageCount = (parseInt(response.data.rData.count / self.search.paging.count)) + remainderCount;
                                self.assignData = result;

                                if (response.data.rData.count > 0) {
                                    self.dataConfirm = true;    
                                }
                            })
                    } else {
                       self.dataConfirm = true;     
                    }
                },
                nextPage : function(page) {
                    this.forcePage = page - 1;
                    this.search.paging.page = page;
                    this.load();
                },
                excel : function() {
                    $.fileDownload('/dashboard/invest/assign/excel', {
                        httpMethod : 'POST',
                        data: { pjCode : this.investData.pjCode }
                    })
                }
            },
            filters : {
                status : function(val) {
                    //IVSS00 : 청약완료, IVSS01 : 주식배정중, IVSS02 : 배정완료, IVSS03 : 청약취소, IVSS04 : 배정탈락, IVSS05 : 투자모금실패, IVSS06 : 청약부분완료
                    var result = "";
                    if(val == "IVSS00") {
                        result = "청약완료";
                    } else if (val == "IVSS01") {
                        result = "주식배정중";
                    } else if (val == "IVSS02") {
                        result = "배정완료";
                    } else if (val == "IVSS03") {
                        result = "청약취소";
                    } else if (val == "IVSS04") {
                        result = "배정탈락";
                    } else if (val == "IVSS05") {
                        result = "투자모금실패";
                    } else if (val == "IVSS06") {
                        result = "청약부분완료";
                    }
                    return result;
                },
            }
        }
    }
}
export default new DashboardInvestAssign