class DashboardInvestStatics {
    component() {
        return {
            template : `
            <div>
                <div class="my_dash_stitle text-left">
                    <strong>투자자 현황</strong>
                </div>

                <div class="row row-15 mdashs_icon_wrap">
                    <div class="col-md-15 col-xs-4 col-md-3">
                        <div class="mdashs_icon nth-child1">
                            <i></i>
                            <em>목표금액</em>
                            <span class="webfont2 small_amount">{{parseInt(investData.pjTgAmount).toLocaleString()}}원</span>
                        </div>
                    </div>
                    <div class="col-md-15 col-xs-4 col-md-3">
                        <div class="mdashs_icon nth-child2">
                            <i></i>
                            <em>총 투자금액</em>
                            <span class="webfont2 small_amount">{{parseInt(investData.pjCurrentAmount).toLocaleString()}}원</span>
                        </div>
                    </div>
                    <div class="col-md-15 col-xs-4 col-md-3">
                        <div class="mdashs_icon nth-child3">
                            <i></i>
                            <em>달성률</em>
                            <span class="webfont2">{{investData.pjRate}}%</span>
                        </div>
                    </div>
                    <div class="col-md-15 col-xs-4 col-md-3">
                        <div class="mdashs_icon nth-child4">
                            <i></i>
                            <em>총 투자자수</em>
                            <span class="webfont2">{{investData.pjInvestorCount}}명</span>
                        </div>
                    </div>
                    <div class="col-md-15 col-xs-4 col-md-3">
                        <div class="mdashs_icon nth-child5">
                            <i></i>
                            <em>총 관심투자자수</em>
                            <span class="webfont2">{{investData.pjGoodCount}}명</span>
                        </div>
                    </div>
                </div>

                <hr class="big big_bg hidden-md hidden-lg" />

                <div class="my_dash_stitle text-left hidden-md hidden-lg">
                    <strong>투자자 현황</strong>
                </div>

                <div class="my_dash_table my_dash_table_center mt15 hidden-md hidden-lg">
                    <table class="table table-condensed-big table-fixed">
                        <colgroup>
                            <col style="width:20%;">
                            <col style="width:37%;">
                            <col style="width:22%;">
                            <col style="width:21%;">
                        </colgroup>
                        <thead>
                            <tr>
                                <th>청약자명</th>
                                <th>투자금액</th>
                                <th>청약상태</th>
                                <th>상세</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- Loop -->
                            <tr v-for="item in investorData">
                                <td>{{ hide(item.memName, item.investorStatus)}}</td>
                                <td>{{ parseInt(item.amount).toLocaleString() }}원</td>
                                <td>{{ status(item.investorStatus) }}</td>
                                <td><a class="btn btn-xs btn-primary-outline" v-on:click="more(item)" :disabled="investData.pjEndStatus == '3'">더보기</a></td>
                            </tr>
                            <!-- //Loop -->
                        </tbody>
                    </table>
                </div>

                <!-- 페이징 -->
                <nav class="text-center hidden-md hidden-lg">
                    <paginate
                        :page-count="pageCount"
                        :class="'pagination'"
                        :click-handler="nextPage"
                        :force-page="forcePage"
                        >
                    </paginate>
                </nav>
                <!-- //페이징 -->

                <hr class="big big_bg" />

                <div class="graph_wrap">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="my_dash_stitle text-left mt10 xs-mb15">
                                <strong>프로젝트 방문 통계</strong>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-2"></div>
                        
                        <div class="col-sm-2 col-md-2 col-xs-6">
                            <date-picker v-model="chartSearch.startDay" :date="chartSearch.startDay" v-if="chartDataConfirm" :min="chartSearch.startDay"></date-picker>
                        </div>
                        <div class="col-sm-2 col-md-2 col-xs-6">
                            <date-picker v-model="chartSearch.endDay" :date="chartSearch.endDay" v-if="chartDataConfirm" :max="chartSearch.endDay"></date-picker>
                        </div>
                        <div class="col-sm-2 col-md-2 col-xs-12 xs-mt20">
                            <button type="submit" class="btn btn-block btn-md btn-primary" v-on:click="getChartData">조회하기</button>
                        </div>   
                    </div>

                    <div class="tab-content mt20">
        
                        <lineChart v-if="gaDataConfirm"
                            :data="visitChartData"
                            :options="chartOptions">
                        </lineChart>

                        <lineChart v-if="chartDataConfirm"
                            :data="goodChartData"
                            :options="chartOptions">
                        </lineChart>

                        <lineChart v-if="chartDataConfirm"
                            :data="investorChartData"
                            :options="chartOptions">
                        </lineChart>

                        <lineChart v-if="chartDataConfirm"
                            :data="amountChartData"
                            :options="chartOptions">
                        </lineChart>

                         <div class="mt15 text-right">
                            <button type="button" class="btn btn-primary-outline" v-on:click="excel">EXCEL 다운로드</button>
                        </div>
                    </div>
                </div>

                <!-- 상세정보 -->
                <div id="miDetail" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body">
                                <div class="rewards-modal-head">
                                    <h3 class="webfont2 mt20">투자자현황 상세정보</h3>
                                    <p><span>주문일시</span> {{moreData.wdate}}</p>
                                </div>
                                <div class="rewards-modal-table">
                                    <!-- 투자자 정보 -->
                                    <table class="table table-fixed">
                                        <colgroup>
                                            <col style="width:38%;">
                                            <col style="width:62%;">
                                        </colgroup>
                                        <thead>
                                            <tr>
                                                <th colspan="2" class="first-child"><strong>투자자 정보</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>이름</td>
                                                <td><strong>{{hide(moreData.memName, moreData.investorStatus)}}</strong></td>
                                            </tr>
                                           <!-- <tr>
                                                <td>E-MAIL(ID)</td>
                                                <td><strong>{{moreData.memEmail}}</strong></td>
                                            </tr> -->
                                        </tbody>
                                    </table>
                                    <!-- //투자자 정보 -->

                                    <!-- 투자 정보 -->
                                    <table class="table table-fixed">
                                        <colgroup>
                                            <col style="width:38%;">
                                            <col style="width:62%;">
                                        </colgroup>
                                        <thead>
                                            <tr>
                                                <th colspan="2" class="first-child"><strong>투자 정보</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>청약번호</td>
                                                <td><strong>{{moreData.investorIdx}}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>투자(청약)일시</td>
                                                <td><strong>{{moreData.wdate}}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>투자금액</td>
                                                <td><strong>{{parseInt(moreData.amount).toLocaleString()}}원</strong></td>
                                            </tr>
                                            <tr>
                                                <td>구좌수</td>
                                                <td><strong>{{moreData.investorAssignQty}}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>배정예정일</td>
                                                <td><strong>{{moreData.pjFixedDate}}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>청약상태</td>
                                                <td><strong>{{status(moreData.investorStatus)}}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>청약금상태</td>
                                                <td>
                                                    <span class="blue-800"><strong>{{refund(moreData.investorRefund)}}</strong></span>
                                                    <!--span class="red-800"><strong>이체중</strong></span-->
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- //투자 정보 -->
                                </div>

                                <div class="modal-footer text-center">
                                    <div class="row not-space">
                                        <div class="col-xs-4 col-xs-offset-4">
                                            <button type="button" class="btn btn-block btn-primary-outline" data-dismiss="modal">확인</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //상세정보 -->

            </div>
            `,
            props : ["investData", "pjAliasUrl"],
            data : function() {
                return {
                    investorData : [],

                    pageCount : 0,
                    forcePage : 0,
                    duration : 1,

                    search : {
                        paging : {
                            page : "1",
                            count : "10"
                        },
                        pjCode : this.investData.pjCode,
                        investorStatus : '',
                        pjStartDate : moment(this.investData.pjStartDate, 'YYYY.MM.DD'),
                        pjEndDate : moment(this.investData.pjEndDate, 'YYYY.MM.DD')
                    },

                    moreData : {},
                    dataConfirm : false,

                    visitChartData :  {
                      labels:  [],
                      datasets: [{
                          label: '방문자 수',
                          backgroundColor: 'red',
                           data: []
                        }]
                    },
                    goodChartData :  {
                      labels:  [],
                      datasets: [{
                          label: '좋아요 수',
                          backgroundColor: 'blue',
                           data: []
                        }]
                    },
                    investorChartData :  {
                      labels:  [],
                      datasets: [{
                          label: '일별 투자자수',
                          backgroundColor: 'green',
                           data: []
                        }]
                    },
                    amountChartData :  {
                      labels:  [],
                      datasets: [{
                          label: '일별 투자금액',
                          backgroundColor: 'orange',
                           data: []
                        }]
                    },
                    chartOptions : {
                        responsive: true, 
                        maintainAspectRatio: false
                    },
                    chartSearch : {
                        pjCode : this.investData.pjCode,
                        startDay : moment(this.investData.pjStartDate, 'YYYY.MM.DD').format('YYYY-MM-DD'),
                        endDay : moment(this.investData.pjEndDate, 'YYYY.MM.DD').format('YYYY-MM-DD'),
                        path : '/i/' + this.pjAliasUrl
                    },

                    chartDataConfirm : false,
                    gaDataConfirm : false
                }
            },
            components : {
                paginate : VuejsPaginate,
                lineChart : require('../../common/line-chart.js').default.component(),
                datePicker : require('../../common/date-picker.js').default.component(),
            },
            created : function() {
                this.load();

                this.checkDate();

            },
            methods : {
                load : function() {
                    var self = this;
                    axios.post('/data/view/invest/dashboard/investor', this.search)
                        .then(function (response) {
                            var result = response.data.rData.investorData;
                            var remainder = parseInt(response.data.rData.count % self.search.paging.count);
                            var remainderCount = 0;
                            if(remainder > 0) {
                                remainderCount++;
                            }
                            self.pageCount = (parseInt(response.data.rData.count / self.search.paging.count)) + remainderCount;
                            self.investorData = result;

                            self.dataConfirm = true;
                    })
                },
                nextPage : function(page) {
                    this.forcePage = page - 1;
                    this.search.paging.page = page;
                    this.load();
                },
                status : function(val) {
                    //IVSS00 : 청약완료, IVSS01 : 주식배정중, IVSS02 : 배정완료, IVSS03 : 청약취소, IVSS04 : 배정탈락, IVSS05 : 투자모금실패, IVSS06 : 청약부분완료
                    var result = "";
                    if(val == "IVSS00") {
                        result = "청약완료";
                    } else if (val == "IVSS01") {
                        result = "주식배정중";
                    } else if (val == "IVSS02") {
                        result = "배정완료";
                    } else if (val == "IVSS03") {
                        result = "청약취소";
                    } else if (val == "IVSS04") {
                        result = "배정탈락";
                    } else if (val == "IVSS05") {
                        result = "투자모금실패";
                    } else if (val == "IVSS06") {
                        result = "청약부분완료";
                    }
                    return result;
                },
                refund : function(val) {
                    //IVSS91 : 청약금 환불예정, IVSS99 : 청약취소 완료, IVSS92 : 청약금 환불이체예정
                    var result = "";
                    if(val == "IVSS91") {
                        result = "청약금 환불예정";
                    } else if (val == "IVSS99") {
                        result = "청약취소 완료";
                    } else if (val == "IVSS92") {
                        result = "청약금 환불이체예정";
                    } else {
                        result = "이체완료";
                    }
                    return result;
                },
                // 더보기 버튼 클릭
                more : function(item) {
                    this.moreData = item;
                    $('#miDetail').modal('show');
                },
                durationChange : function(val) {
                    this.duration = val;
                },
                checkDate : function() {
                    var now = moment().format("YYYY-MM-DD");
                    if (!moment(now).isAfter(this.investData.pjEndDate)) {
                        this.chartSearch.pjEndDate = now;
                    }
                    this.getChartData();
                },
                getChartData : function() {
                    var self = this;
                    self.chartDataConfirm = false;
                    this.getGaChartData();

                    axios.post('/data/view/invest/dashboard/chart', this.chartSearch)
                        .then(function (response) {
                        var result = response.data;
                        if(result.rCode == "0000") {

                            var day = [];
                            for (var i = 0; i < result.rData.chartDate.length; i++) {
                                day.push(result.rData.chartDate[i].day);
                                self.goodChartData.datasets[0].data.push(result.rData.goodChartData[i].value);
                                self.investorChartData.datasets[0].data.push(result.rData.investorChartData[i].value);
                                self.amountChartData.datasets[0].data.push(result.rData.amountChartData[i].value);
                            }

                            self.goodChartData.labels = day;
                            self.investorChartData.labels = day;
                            self.amountChartData.labels = day;

                            self.chartDataConfirm = true;
                        }  
                        
                    })

                },
                getGaChartData : function() {
                    var self = this;
                    self.gaDataConfirm = false;
                    axios.post('/ga/info', this.chartSearch)
                        .then(function (response) {
                        var result = response.data;

                        if(result.rCode == "0000") {

                            for (var i = 0; i < result.rData.chartDate.length; i++) {
                                self.visitChartData.labels.push(result.rData[i].day);
                                self.visitChartData.datasets[0].data.push(result.rData[i].value);
                            }

                            self.gaDataConfirm = true;
                        }

                    })
                },
                excel : function() {
                    var excelSearch = {
                        pjCode : this.chartSearch.pjCode,
                        startDay : this.chartSearch.startDay,
                        endDay : this.chartSearch.endDay,
                        pjAliasUrl : this.pjAliasUrl
                    };
                    
                     $.fileDownload('/dashboard/invest/static/excel', {
                        httpMethod : 'POST',
                        data: excelSearch
                    })
                },
                hide : function(name, status) {

                    //  var result = "";
                    // if(val == "IVSS00") {
                    //     result = "청약완료";
                    // } else if (val == "IVSS01") {
                    //     result = "주식배정중";
                    // } else if (val == "IVSS02") {
                    //     result = "배정완료";
                    // } else if (val == "IVSS03") {
                    //     result = "청약취소";
                    // } else if (val == "IVSS04") {
                    //     result = "배정탈락";
                    // } else if (val == "IVSS05") {
                    //     result = "투자모금실패";
                    // } else if (val == "IVSS06") {
                    //     result = "청약부분완료";
                    // }
                    // return result;
                    
                    name = name + "";
                    if (((this.investData.pjEndStatus != '2') || (this.investData.pjEndStatus == '2' && status != 'IVSS02')) && name != "") {
                        var returnText = name.slice(0, 1);
                        for (var i = 0; i < name.length - 1; i++) {
                            returnText += '*';
                        }
                        return returnText;
                    } else {
                        return  name;
                    }
                }
                
            }
        }
    }
}
export default new DashboardInvestStatics