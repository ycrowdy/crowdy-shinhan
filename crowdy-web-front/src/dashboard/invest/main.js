class DashboardInvestMain {
    component() {
        return {
            template : `
           <div>
                <div id="page" class="page-wrapper">
                    <!-- 투자(2) - 대쉬보드 - 프로젝트 통계 -->
                    <div id="list_wrap" class="my_dash_wrap" v-bind:class="{'hidden-xs hidden-sm' : step != 1}">
                        <div class="container">
                            <div class="my_dash_header">
                                <div class="my_dash_header_condition">
                                    <span class="btn btn-primary-outline btn_cursor">{{pjEndStatus}}</span>
                                </div>
                                <h4 class="webfont2">{{ investData.pjTitle }}</h4>
                                {{ startDate }} ~ {{ endDate }}
                            </div>

                            <div class="mobile_info_tip hidden-md hidden-lg">
                                회원님의 프로젝트 간략 현황입니다.<br />
                                상세현황은 PC에서 확인 바랍니다.
                                <p>프로젝트 종료 후 정산내역은 PC에서만 확인가능합니다.</p>
                            </div>

                            <div class="my_dash_nav hidden-xs hidden-sm">
                                <ul class="nav nav-tabs nav-justified">
                                    <li :class="{'active' : step == 1}"><a v-on:click="stepChange(1)" style="cursor: pointer;">프로젝트 통계</a></li>
                                    <li :class="{'active' : step == 2}"><a v-on:click="stepChange(2)" style="cursor: pointer;">투자자 현황</a></li>
                                    <li :class="{'active' : step == 3}"><a v-on:click="stepChange(3)" style="cursor: pointer;">배정 내역 현황</a></li>
                                    <li :class="{'active' : step == 4}"><a v-on:click="stepChange(4)" style="cursor: pointer;">정산 내역</a></li>
                                </ul>
                            </div>
                            <statics v-if="step == 1 && dataConfirm" :invest-data="investData" :pj-alias-url="pjAliasUrl"></statics>
                            <investor v-if="step == 2 && dataConfirm" :invest-data="investData"></investor>
                            <assign v-if="step == 3 && dataConfirm" :invest-data="investData"></assign>
                            <calculate v-if="step == 4 && dataConfirm" :invest-data="investData"></calculate>
                        </div>
                    </div>
                </div>
            </div>
            `,
            props : ['pjAliasUrl'],
            data : function() {
                return {
                    step : 1,
                    search : {
                        paging : {
                            page : "1",
                            count : "10"
                        },
                        pjCode : '',
                    },                    
                    investData : {},
                    startDate : '',
                    endDate : '',
                    dataConfirm : false,
                }
            },
            components : {
                statics : require('./statics.js').default.component(),
                investor : require('./investor.js').default.component(),
                assign : require('./assign.js').default.component(),
                calculate : require('./calculate.js').default.component(),
            },
            computed : {
                //프로젝트 완료상태 (승인대기:0 진행중:1 성공:2 실패:3)
                pjEndStatus : function(){
                    if (this.investData.pjEndStatus == 0) {
                        return '승인대기';
                    } else if (this.investData.pjEndStatus == 1) {
                        return '진행중';
                    } else {
                        return '종료';
                    }
                }
            },
            created : function() {
                this.load();
            },
            methods : {
                load : function() {
                    if(userInfo.memCode == null || userInfo.memCode == '') {
                        noti.open('로그인을 해주세요.', function() {window.open("/", '_self');});
                        return;
                    }

                    var self = this;
                    axios.post('/data/view/invest/info', { pjAliasUrl : this.pjAliasUrl})
                        .then(function (response) {
                            var result = response.data.rData;
                            self.investData = result;

                            if(userInfo.memCode != self.investData.memCode) {
                                noti.open('제작자 계정이 아닙니다.', function() {window.open("/", '_self');});
                                return;
                            }

                            self.search.pjCode = self.investData.pjCode;
                            self.startDate = moment(self.investData.pjStartDate, 'YYYY-MM-DD').format('YYYY.MM.DD');
                            self.endDate = moment(self.investData.pjEndDate, 'YYYY-MM-DD').format('YYYY.MM.DD');
                            self.dataConfirm = true;                           
                        })
                },
                stepChange : function(step) {
                    this.step = step;
                    this.$emit('mobile-change', step);
                }
            }
        }
    }
}
export default new DashboardInvestMain