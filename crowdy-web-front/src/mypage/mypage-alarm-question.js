class MypageAlarmQuestion {
    component() {
        return {
            template : `
            <div>
                <div class="container">
                    <div class="row not-space">
                        <div class="col-md-2">
                            <div class="st-menu webfont2">
                                <ul>
                                    <li class="w30" :class="{'active' : type == 1}"><a href="javascript:void(0)" v-on:click="subMenuChange(1)">알림</a></li>
                                    <li class="w30" :class="{'active' : type == 2}"><a href="javascript:void(0)" v-on:click="subMenuChange(2)">문의 내역</a></li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-10 col-line">
                            <div class="row not-space">
                                <div class="col-md-1"></div>
                                <div class="col-md-11">
                                    <div class="common_sub_title webfont2">
                                        새로운 알림을 <br />
                                        확인해보세요
                                    </div>

                                    <alarm v-if="type == 1"></alarm>
                                    <question v-if="type == 2"></question>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            `,
            props : ['subMenu'],
            data : function() {
                return {
                    makedList : [],
                    pageCount : 0,
                    forcePage : 0,
                    search : {
                        memCode : this.code,
                        paging : {
			                page : "1",
			                count : "12"
			            }
                    },
                    type : this.subMenu,
                }
            },
            created : function() {
                this.load();
            },
            components : {
                paginate : VuejsPaginate,
                alarm : require('./mypage-alarm.js').default.component(),
                question : require('./mypage-question.js').default.component(),
            },
            computed : {
                
            },
            methods : {
                load : function() {
                    var self = this;
                },
                subMenuChange : function(menu) {
                    this.type = menu;
                }
            }
        }
    }
}

export default new MypageAlarmQuestion()