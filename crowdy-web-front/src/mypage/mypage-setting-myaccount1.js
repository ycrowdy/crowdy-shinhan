class MypageSettingMyAccount1 {
    component() {
        return {
            template : `
            <div class="mt60 xs-mt30 mb80 mypage-myaccount-layout">
                <!-- 사진영역-->
                <div class="dropify-wrapper-80 mypage-myaccount-photo">
                    <dropify-input accept="image/*" v-model="request.memShotImgFile" default-message="" :default-img="userImage"></dropify-input>
                </div>

                <!-- 기본정보-->
                <div class="mypage-form xs-mt30">
                
                    <div class="mb20 mypage-title">기본정보 설정</div>

                    <!-- 이름 -->
                    <label for="member_name" class="mypage-rabel">이름</label>
                    <input type="text" class="form-control" title="이름을 입력하세요." v-model="request.memName" placeholder="이름을 입력"/>
                    <!-- //이름 -->

                    <!-- 이메일 -->
                    <label for="member_email" class="mypage-rabel">이메일</label>
                    <input type="email" class="form-control" v-model="userEmail" placeholder="" disabled/>
                    <!-- //이메일 -->

                    <!-- 전화번호 -->
                    <label for="member_mobile" class="mypage-rabel">전화번호</label>
                    <div class="common-flex-end">
                        <input type="tel" class="form-control mypage-tel-layout1 mypage-tel-width" v-model="request.mobileNo" placeholder="" disabled/>
                        <a class="mypage-btn mypage-tel-layout2" v-on:click="auth()">{{authBtnText}}</a>
                    </div>
                    <div class="blue-800 mypage-margin-layout" v-if="isMobileAuth">본인인증이 완료되었습니다.</div>
                    <!-- //전화번호 -->

                    <!-- 우편번호 -->
                    <label for="zip_code" class="mypage-rabel">우편번호</label>
                    <div class="common-flex-end">
                        <input type="tel" class="form-control mypage-zip-layout1" v-on:click="postOpen" v-model="postNum" name="zip_code" id="zip_code" placeholder="" readonly="readonly"/>
                        <div class="mypage-btn-search" v-on:click="postOpen"></div>
                    </div>
                    <!-- //우편번호 -->

                    <!-- 주소 -->
                    <label for="address1" class="mypage-rabel">주소</label>
                    <input type="text" class="form-control" v-on:click="postOpen" v-model="address1" name="address1" id="address1" placeholder="주소를 검색해주세요." readonly="readonly" />
                    <input type="text" class="form-control" v-model="request.memSpsAddr2" name="address2" id="address2" placeholder="상세 주소를 입력해주세요." />

                    <label class="chk_container">크라우디의 소식과 다양한 안내를 받겠습니다.
                        <input type="checkbox" v-model="checkMarketingAgree" v-on:click="checkTerms">
                        <span class="checkmark"></span>
                    </label>
                    <!-- //주소 -->

                    <div class="mypage-setting-btn mt20" v-on:click="changeAccountInfo(0)">설정 저장</div>

                    <!-- 비밀번호 변경 -->
                    <div class="mt60 mb20 mypage-title">비밀번호 변경</div>
                    
                    <label for="current_password" class="mypage-rabel">현재 비밀번호</label>
                    <input type="password" class="form-control" v-model="request.memPwd" placeholder="" />

                    <label for="new_password" class="mypage-rabel">새 비밀번호</label>
                    <input type="password" class="form-control" v-model="request.newMemPwd" title="8자 이상의 영문 대소문자,  숫자 특수문자를 사용하세요." placeholder="" />
                
                    <label for="new_password2" class="mypage-rabel">비밀번호 확인</label>
                    <input type="password" class="form-control" v-model="request.newMemPwdConfirm" placeholder="" />

                    <div class="mypage-setting-btn mt20" v-on:click="changePassword()">비밀번호 변경</div>
                    <!-- //비밀번호 변경 -->

                    <!-- 회원탈퇴 -->
                    <div class="mt60 mypage-title">회원탈퇴</div>
                    <div class="mypage-desc mt15">
                        회원탈퇴시 관련정보는 모두 5년간 분리보관되며, 5년후 영구적으로 삭제됩니다.
                    </div>
                    <div class="font15 colorRed1 mt20">유의사항</div>  
                            
                    <ul class="con-icon con-icon-small">
                        <li>현재 프로젝트를 진행 중이거나, 프로젝트를 등록하여 진행한 이력이 있는 회원님은 리워드 프로젝트 종료 후 1년 그리고 투자 프로젝트 종료 후 3년 이내에는 회원 탈퇴가 불가합니다.</li>
                        <li>현재 프로젝트를 펀딩 중인 회원의 경우도 펀딩 종료 후 1개월 이내에는 회원을 탈퇴할 수 없습니다.</li>
                    </ul>
                     
                     <div class="mypage-setting-btn mt20 gray-btn" v-on:click="confirmLeaveCrowdy">회원탈퇴 하기</div>   
                    <!-- //회원탈퇴 -->

                    <form name="form_chk" method="post">
                        <input type="hidden" name="m" value="checkplusSerivce">                     <!-- 필수 데이타로, 누락하시면 안됩니다. -->
                        <input type="hidden" name="EncodeData" id="encodeData" value="">     <!-- 위에서 업체정보를 암호화 한 데이타입니다. -->

                        <input type="hidden" name="param_r1" value="accountSetting.authResult">
                        <input type="hidden" name="param_r2" value="">
                        <input type="hidden" name="param_r3" value="">
                    </form>
                </div>
                <!-- //기본정보-->

                <div id="leave-confirm-modal" class="modal fade" tabindex="-1" role="dialog" style="z-index: 9999;" data-backdrop="static" data-keyboard="false">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" aria-label="Close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body modal-order">
                                <p>크라우디를 탈퇴하시겠습니까?</p>
                                <div class="modal-footer text-center">
                                    <div class="row not-space">
                                        <div class="col-xs-4 col-xs-offset-1">
                                            <button type="button" class="btn btn-block btn-primary-outline" v-on:click="leaveCrowdy">확인</button>
                                        </div>
                                        <div class="col-xs-4 col-xs-offset-2">
                                            <button type="button" class="btn btn-block btn-primary-outline" data-dismiss="modal">취소</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            `,
            props : ['naverOauth'],
            data : function() {
                return {
                    dataConfirm : false,
                    oauthConfirm : false,
                    request : {
                        bizNum : "",
                        memCode : userInfo.memCode,
                        memType : userInfo.type,
                        memName : userInfo.name,
                        memEmail : userInfo.email,
                        memPwd :"",
                        newMemPwd : "",
                        newMemPwdConfirm : "",
                        memSpsAddr1 : "",
                        memSpsAddr2 : "",
                        memSpsPostNum : "",
                        resName : "",
                        memShotImg : userInfo.image,
                        memShotImgFile: {},
                        mobileNo : "",
                        authChange : false,
                        marketingAgree : 'N',
                        memNameConfirm : 'N'
                    },
                    address : {
                        postNum : '',
                        address1 : ''
                    },
                    authBtnText : '인증번호 받기',
                    isMobileAuth : false,
                    checkMarketingAgree : false,
                    snsOauth : {
                        // true/false 반대로 되어있음
                        naver : false,
                        facebook : false,
                        naverIdx : '',
                        facebookIdx : '',
                        naverSnsId : '',
                        facebookSnsId : ''
                    },
                    snsOauthRequest : {
                        memCode : userInfo.memCode,
                        memSnsIdx : '',
                        snsId : '',
                        snsType : ''
                    },
                    snsInfo : {}, 
                    os : ''
                }
            },
            created : function() {
                
                $(document).ready(function(){
                    $(".switch-hb").bootstrapSwitch();
                });

                this.load();
                // this.getSnsInfo();
                // this.getSnsList();
                // if(this.naverOauth){
                //     this.checkNaverOauth();
                // }
                window.accountSetting = this;

                // PC/Mobile 구분 
                this.deviceCheck();
            },
            computed : {
                postNum : function() {
                    this.request.memSpsPostNum = this.address.postNum
                    return this.address.postNum
                },
                address1 : function() {
                    this.request.memSpsAddr1 = this.address.address1
                    return this.address.address1
                },
                userName : function() {
                    return userInfo.name
                },
                userEmail : function() {
                    return userInfo.email
                },
                userImage : function() {
                    return userInfo.image
                }
            },
            methods : {
                load : function() {
                    var self = this;
                    axios.post('/data/member/info', this.request)
                        .then(function(response){
                            self.request.bizNum = response.data.rData.bizNum;
                            self.request.mobileNo = response.data.rData.mobileNo;
                            self.request.memSpsAddr1 = response.data.rData.memSpsAddr1;
                            self.request.memSpsAddr2 = response.data.rData.memSpsAddr2;
                            self.request.memSpsPostNum = response.data.rData.memSpsPostNum;
                            self.request.marketingAgree = response.data.rData.marketingAgree;
                            self.request.memNameConfirm = response.data.rData.memNameConfirm;
                            self.request.resName = response.data.rData.resName;
                            self.address.postNum =  response.data.rData.memSpsPostNum;
                            self.address.address1 = response.data.rData.memSpsAddr1;

                            if(self.request.marketingAgree == 'Y') {
                                self.checkMarketingAgree = true;
                            } else {
                                self.checkMarketingAgree = false;
                            }

                            if(self.request.memNameConfirm == 'Y') {
                                self.isMobileAuth = true;
                            } else {
                                self.isMobileAuth = false;
                            }

                            self.dataConfirm = !self.dataConfirm;
                        })
                },
                getSnsInfo : function() {
                    var self = this;
                    axios.post('/member/getSnsInfo', this.request)
                    .then(function(response){
                        if(response.data.rCode == "0000") {
                            self.snsInfo = response.data.rData;
                        } else {
                            self.error = response.data.rMsg
                        }
                    })
                },
                resetSnsList : function() {
                    this.snsOauth = {
                        naver : false,
                        facebook : false,
                        naverIdx : '',
                        facebookIdx : '',
                        naverSnsId : '',
                        facebookSnsId : ''
                    };
                },
                getSnsList : function() {
                    var self = this;

                    this.resetSnsList();
                    axios.post('/data/member/sns-info', this.request)
                        .then(function(response){
                            var result = response.data;
                            if(result.rCode == "0000") {
                                if(result.rData.count > 0) {
                                    var list = result.rData.list;
                                    for (var i = 0; i < result.rData.count; i++) {
                                        if (list[i].snsType == '1') {
                                            self.snsOauth.facebook = true;
                                            self.snsOauth.facebookIdx = list[i].memSnsIdx;
                                            self.snsOauth.facebookSnsId = list[i].snsId;
                                        } else if (list[i].snsType == '5') {
                                            self.snsOauth.naver = true;
                                            self.snsOauth.naverIdx = list[i].memSnsIdx;
                                            self.snsOauth.naverSnsId = list[i].snsId;
                                        }
                                    }
                                }
                                self.oauthConfirm = true;
                            } else {
                                noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
                            }
                        })
                },
                changeSnsOauth : function(snsType) {
                    var self = this;
                    if (snsType == 'facebook') {
                        if (this.snsOauth.facebookIdx == '') {
                            var FB_SCOPE = 'email, publish_actions, user_friends';
                            // FACEBOOK-1 : FACEBOOK LOGIN
                            FB.login(function(response) {
                                var status = response && response.status;
                                if (status === 'connected') {
                                    // FACEBOOK-2 : GET USER INFO
                                    self.getFacebookUserInfo(response);
                                } else if (status === 'not_authorized') {
                                    noti.open('페이스북 연동을 취소하셨거나 실패하였습니다.');
                                } else {
                                    noti.open('페이스북 연동을 실패하였습니다.');
                                }
                            }, { scope: FB_SCOPE });

                        } else {
                            this.snsOauthRequest.snsId = this.snsOauth.facebookSnsId;
                            this.snsOauthRequest.snsType = '5';
                            this.snsOauthRequest.memSnsIdx = this.snsOauth.facebookIdx;
                            this.updateSnsOauth();
                        }
                    } else {
                        if (this.snsOauth.naverIdx == '') {
                             var state = naverLogin.getUniqState();
                            naverLogin.setDomain(self.snsInfo.naverAppDomain);
                            naverLogin.setState(state);
                            // naverLogin.redirect_uri = "https://crowdy-dev.ycrowdy.com/mypage/main";
                            naverLogin.redirect_uri = "https://www.ycrowdy.com/mypage/main";
                            // NAVER-1 : NAVER LOGIN
                            window.open(naverLogin.getNaverIdLoginLink(), '_self');
                        } else {
                            this.snsOauthRequest.snsId = this.snsOauth.naverSnsId;
                            this.snsOauthRequest.snsType = '1';
                            this.snsOauthRequest.memSnsIdx = this.snsOauth.naverIdx;
                            this.updateSnsOauth();
                        }
                    }
                },
                // FACEBOOK-2 : GET USER INFO
                getFacebookUserInfo : function(status) {
                    var self = this;
                    FB.api('/me', { fields: 'name, email, picture' }, function(response) {
                        self.snsOauthRequest.snsId = response.id;
                        self.snsOauthRequest.snsType = '1';
                        self.snsOauthRequest.memSnsIdx = '';
                        self.checkSnsExist();
                    }); 
                },
                checkSnsExist : function() {
                    var self = this;
                        axios.post('/data/member/sns-check', this.snsOauthRequest)
                            .then(function(response){
                                 var result = response.data;
                                 if(result.rCode == "0000") {
                                    var memCode = response.data.rData.memCode;
                                    
                                    if(memCode == '' || memCode == null) {
                                        self.updateSnsOauth();
                                    } else if(memCode == self.memCode) {
                                        self.updateSnsOauth();
                                    } else if(memCode != self.memCode) {
                                        noti.open("이미 다른 계정에 연동되어 있습니다. 이 계정으로 연동을 바꾸시겠어요?", self.updateSnsOauth, true);
                                        
                                        if (self.snsOauthRequest.snsType == '1') {
                                            self.snsOauth.naver = !self.snsOauth.naver;
                                        } else {
                                            self.snsOauth.facebook = !self.snsOauth.facebook;
                                        }
                                        return;
                                    }

                                 } else {
                                    noti.open("시스템 문제가 발생하였습니다. <br/> 다시 시도해주세요.");
                                    self.getSnsList();
                                    return;
                                 }
                               
                            })
                },
                updateSnsOauth : function() {
                    var self = this;
                    $('.page-loader-more').fadeIn('')
                    axios.post('/data/member/sns-update', this.snsOauthRequest)
                        .then(function(response){
                            $('.page-loader-more').fadeOut('');
                            var result = response.data;
                            if(result.rCode == "0000") {
                                noti.open("변경되었습니다.");
                                self.getSnsList();
                            } else {
                                noti.open("시스템 문제가 발생하였습니다. <br/> 다시 시도해주세요.");
                                self.getSnsList();
                            }
                        })
                },
                // NAVER-2 : NAVER OAUTH 확인
                checkNaverOauth : function() {
                    var self = this;
                    window.loginInfo = self;
                    naverLogin.get_naver_userprofile("loginInfo.naverSignInCallback()");
                },
                // NAVER-3 : GET USER INFO
                naverSignInCallback : function() {
                    var self = this;
                    self.snsOauthRequest.snsId =  naverLogin.getProfileData('id');
                    self.snsOauthRequest.memSnsIdx = '';
                    self.snsOauthRequest.snsType = '5';
                    self.checkSnsExist();
                },
                postOpen : function() {
                    post.open(this);
                },
                changeAccountInfo : function(mode) {
                    var self = this;
                    $('.page-loader-more').fadeIn('')
                    axios.post('/member/update', this.request)
                        .then(function(response){
                            $('.page-loader-more').fadeOut('');
                            var result = response.data;
                            if(result.rCode == "0000") {
                                userInfo.updateInfo(response.data.rData);
                                self.request.memName = userInfo.name;
                                self.request.memShotImg  = userInfo.image;
                                if(mode == 0) {
                                    noti.open("저장되었습니다.")
                                } else if(mode == 1) {
                                    noti.open("본인인증이 완료되었습니다.")
                                }
                            } else {
                                noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
                            }
                        })
                },
                changePassword : function() {
                    var self = this;

                    if(!/^.*(?=.{8,20})(?=.*[0-9])(?=.*[a-zA-Z]).*$/.test(self.request.newMemPwd)){
                        noti.open('숫자와 영문자 조합으로 8자리 이상을 사용해야 합니다.');
                        return false;
                    }

                    if(self.request.newMemPwd == self.request.newMemPwdConfirm) {
                        $('.page-loader-more').fadeIn('')
                        axios.post('/data/member/password-update', this.request)
                            .then(function(response){
                                $('.page-loader-more').fadeOut('');
                                var result = response.data;
                                if(result.rCode == "0000") {
                                    noti.open("변경되었습니다.")
                                } else if(result.rCode == "2001") {
                                    noti.open("현재 비밀번호를 잘못 입력하셨습니다.")
                                } else {
                                    noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
                                }
                            })
                    } else {
                        noti.open("새 비밀번호와 비밀번호 확인이 일치하지 않습니다.")
                    }
                }, 
                deviceCheck : function() {
                    if(isMobile.apple.device) {
                        this.os = 'M';
                    } else if(isMobile.android.device) {
                        this.os = 'M';
                    } else {
                        this.os = 'W';
                    }
                },
                auth : function() {
                    this.request.authChange = false;
                    this.isMobileAuth = false;
                    this.authBtnText = "재인증요청";

                    if (this.os == '') {
                        this.os = 'M';
                    }

                    axios.post('/auth/nice/' + this.os)
                        .then(function(response){
                            // $('#encodeData').val(response.data.rData.data)
                            document.getElementById('encodeData').value = response.data.rData.data;
                            // window.open('', 'popupChk', 'width=500, height=550, top=100, left=100, fullscreen=no, menubar=no, status=no, toolbar=no, titlebar=yes, location=no, scrollbar=no');
                            window.open('', 'popupChk', 'width=500, height=800, top=100, left=100, fullscreen=no, menubar=no, status=no, toolbar=no, titlebar=yes, location=no, scrollbar=no');
                            document.form_chk.action = "https://nice.checkplus.co.kr/CheckPlusSafeModel/checkplus.cb";
                            document.form_chk.target = "popupChk";
                            document.form_chk.submit();
                        });
                },
                authResult : function(result) {
                    if(result) {
                        var self = this;
                        axios.post('/auth/info')
                            .then(function(response) {
                                self.request.mobileNo = response.data.mobileNo;
                                self.request.authChange = true;
                                self.request.memNameConfirm = 'Y';
                                self.isMobileAuth = true;
                                self.changeAccountInfo(1);
                            });
                    } else {
                        noti.open('본인인증에 실패했습니다 <br/> 다시시도해주세요.')
                    }
                },
                checkTerms : function() {
                    if(this.request.marketingAgree == 'Y') {
                        this.request.marketingAgree = 'N';
                    } else {
                        this.request.marketingAgree = 'Y';
                    } 
                },
                confirmLeaveCrowdy: function() {
                    $('#leave-confirm-modal').modal('show');
                },
                leaveCrowdy : function() {
                    $('#leave-confirm-modal').modal('hide');
                    axios.post('/data/member/leave', this.request)
                        .then(function(response){
                            if(response.data.rCode == "0000") {
                                noti.open('회원탈퇴가 정상적으로 처리되었습니다.', function() {window.open("/logout", '_self');});
                            } else {
                                noti.open(response.data.rMsg)
                            }
                        });
                }
            },
            components :{
                dropifyInput : require('../common/dropify-input.js').default.component()
            }
        }
    }
}

export default new MypageSettingMyAccount1()