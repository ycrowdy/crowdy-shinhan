class MypageSettingMyMember4 {
    component() {
        return {
            template : `
            <div class="mt60 xs-mt30 mb80 mypage-myaccount-layout">
                <!-- 사진영역-->
                <div class="dropify-wrapper-80 mypage-myaccount-photo">
                    <dropify-input accept="image/*" v-model="request.memShotImgFile" default-message="" :default-img="userImage"></dropify-input>
                </div>

                <!-- 컨텐츠 영역 -->
                <div class="mypage-form xs-mt30">
                    
                    <div class="mb20 mypage-title">투자 회원 {{registerTitle}}하기 (조합회원)</div>

                    <!-- 조합명 -->
                    <div class="mypage-rabel mb5">조합명</div>
                    <input type="text" class="form-control" maxlength="20" v-model="request.memName" placeholder="조합명 입력" readonly="readonly"/>
                    <!-- //조합명 -->

                    <!-- 조합고유번호 -->
                    <div class="mypage-rabel mb5">조합고유번호</div>
                    <input type="text" v-model="request.memIvsComNo" class="form-control" maxlength="10" placeholder="" readonly="readonly" />
                    <!-- //조합고유번호 -->

                    <!-- 전화번호 -->
                    <div class="mypage-rabel mb5">전화번호</div>
                    <number-input type="tel" class="form-control" placeholder="" :num="request.memIvsTellNo" v-model="request.memIvsTellNo"/></number-input>
                    <!-- //전화번호 -->

                    <!-- 우편번호 -->
                    <div for="zip_code" class="mypage-rabel mb5">우편번호</div>
                    <div class="common-flex-end">
                        <input type="tel" class="form-control mypage-zip-layout1" v-on:click="postOpen" v-model="postNum" name="zip_code" id="zip_code" placeholder="" readonly="readonly"/>
                        <div class="mypage-btn-search" v-on:click="postOpen"></div>
                    </div>
                    <!-- //우편번호 -->

                    <!-- 주소 -->
                    <div class="mypage-rabel mb5">주소</div>
                    <input type="text" class="form-control" v-on:click="postOpen" v-model="address1" name="address1" placeholder="주소를 검색해주세요." readonly="readonly" />
                    <input type="text" class="form-control" v-model="request.memIvsAddr2" placeholder="상세 주소를 입력해주세요." />
                    <!-- //주소 -->

                    <!-- 기본정보 수정 -->
                    <div class="mypage-setting-btn mt20" v-on:click="updateBasicInfo()" v-if="investor == 'Y'">설정 저장</div>
                    <!-- //기본정보 수정 -->


                    <div class="mypage-title mt50 mb20">조합 담당자 정보</div>

                    <!-- 본인인증 -->
                    <div class="mypage-rabel mb5">본인인증</div>
                    <div class="mypage-setting-btn" v-on:click="auth()">본인인증 하기</div>
                    <!-- //본인인증 -->

                    <!-- 담당자명 -->
                    <div class="mypage-rabel mb5 mt10">담당자명</div>
                    <input type="text" v-model="request.memIvsResName" class="form-control" maxlength="20" placeholder="" disabled/>
                    <!-- //담당자명 -->

                    <!-- 휴대폰번호 -->
                    <div class="mypage-rabel mb5 mt10">휴대폰번호</div>
                    <input type="tel" class="form-control" v-model="request.memIvsRegiHpNo" placeholder="" disabled/>
                    <!-- //휴대폰번호 -->

                    <!-- 이메일 -->
                    <div class="mypage-rabel mb5 mt10">이메일</div>
                    <input type="email" class="form-control" maxlength="100" v-model="request.memIvsResEmail" placeholder=""/>
                    <!-- //이메일 -->

                    <!-- 기본정보 수정 -->
                    <div class="mypage-setting-btn mt20" v-on:click="updateResInfo()" v-if="investor == 'Y'">설정 저장</div>
                    <!-- //기본정보 수정 -->
                    <!-- //조합 담당자 정보 -->

                    <!-- 회원유형 -->
                    <div class="mypage-title mt50">회원유형</div>
                    
                    <div class="mypage-rabel mb5 mt20">회원유형 선택</div>
                    <div class="option_group option_group_none">
                        <div class="option1_radio">
                            <label for="option1_radio1" class="radio-inline">
                                <input type="radio" name="option1_radio" id="option1_radio1" value="option1_radio1" checked="checked" /><span class="webfont">전문투자자</span>
                            </label>
                        </div>
                    </div>

                    <!-- 실명확인용 서류 -->
                    <div class="mb20 mt50 mypage-title">실명확인용 서류</div>

                    <!-- 조합고유번호증 -->
                    <div class="mypage-rabel mb5">조합고유번호증*</div>
                    <div class="input-group input-group-file">
                        <input type="text" class="form-control" v-model="request.memIvsCorFile.fileName" placeholder="" readonly="readonly" />
                        <span class="input-group-btn pb10">
                            <span class="btn btn-outline btn-file">
                                <i class="fa fa-upload" aria-hidden="true"></i>
                                <file-input v-model="request.memIvsCorFile" data-vv-name="memIvsCorFile" v-validate="'required'" :disabled="memIvsRnmDocDisabled" v-on:error="error"></file-input>
                            </span>
                        </span>
                    </div>
                    <!-- //조합고유번호증 -->

                    <!-- 통장사본 -->
                    <div class="mypage-rabel mb5">통장사본*</div>

                    <div class="input-group input-group-file">
                        <input type="text" class="form-control" v-model="request.memIvsBankFile.fileName" placeholder="청약금 출금 통장" readonly="readonly" />
                        <span class="input-group-btn pb10">
                            <span class="btn btn-outline btn-file">
                                <i class="fa fa-upload" aria-hidden="true"></i>
                                <file-input v-model="request.memIvsBankFile" data-vv-name="memIvsBankFile" v-validate="'required'" :disabled="memIvsRnmDocDisabled" v-on:error="error"></file-input>
                            </span>
                        </span>
                    </div>

                    <!-- 실명확인증표 -->
                    <div class="mypage-rabel mb5">실명확인증표*</div>
                    <div class="input-group input-group-file">
                        <input type="text" class="form-control" v-model="request.memIvsRnmFile.fileName" placeholder="대표자 혹은 대리인의 신분증" readonly="readonly" />
                        <span class="input-group-btn pb10">
                            <span class="btn btn-outline btn-file">
                                <i class="fa fa-upload" aria-hidden="true"></i>
                                <file-input v-model="request.memIvsRnmFile" data-vv-name="memIvsRnmFile" v-validate="'required'" :disabled="memIvsRnmDocDisabled" v-on:error="error"></file-input>
                            </span>
                        </span>
                    </div>
                    <!-- //실명확인증표 -->

                    <!-- 실명확인증표(위임장) -->
                    <div class="input-group input-group-file">
                        <input type="text" class="form-control" v-model="request.memIvsRnmAttorneyFile.fileName" placeholder="대리인 신분증 첨부시 위임장 첨부" readonly="readonly" />
                        <span class="input-group-btn pb10">
                            <span class="btn btn-outline btn-file">
                                <i class="fa fa-upload" aria-hidden="true"></i>
                                <file-input v-model="request.memIvsRnmAttorneyFile" data-vv-name="memIvsRnmAttorneyFile" v-validate="'required'" :disabled="memIvsRnmDocDisabled" v-on:error="error"></file-input>
                            </span>
                        </span>
                    </div>
                    <a href="https://image-se.ycrowdy.com/file/%E1%84%8F%E1%85%B3%E1%84%85%E1%85%A1%E1%84%8B%E1%85%AE%E1%84%83%E1%85%B5%E1%84%8B%E1%85%B1%E1%84%8B%E1%85%B5%E1%86%B7%E1%84%8C%E1%85%A1%E1%86%BC%E1%84%8B%E1%85%A3%E1%86%BC%E1%84%89%E1%85%B5%E1%86%A8.hwp" class="btn-link grey-600 mypage-setting-link-custom">(위임장 샘플)</a>
                    <!-- //실명확인증표(위임장) -->
                     
                    <!-- 사용인감계(선택) -->
                    <div class="mypage-rabel mb5">사용인감계 <span class="red-800">(선택사항)</span></div>
                    <div class="input-group input-group-file">
                        <input type="text" class="form-control" v-model="request.memIvsCorAttorneyFile.fileName" placeholder="" readonly="readonly" />
                        <span class="input-group-btn pb10">
                            <span class="btn btn-outline btn-file">
                                <i class="fa fa-upload" aria-hidden="true"></i>
                                <file-input v-model="request.memIvsCorAttorneyFile" data-vv-name="memIvsCorAttorneyFile" v-validate="'required'" :disabled="memIvsRnmDocDisabled" v-on:error="error"></file-input>
                            </span>
                        </span>
                    </div>
                    <a href="https://image-se.ycrowdy.com/file/%E1%84%8F%E1%85%B3%E1%84%85%E1%85%A1%E1%84%8B%E1%85%AE%E1%84%83%E1%85%B5%E1%84%89%E1%85%A1%E1%84%8B%E1%85%AD%E1%86%BC%E1%84%8B%E1%85%B5%E1%86%AB%E1%84%80%E1%85%A1%E1%86%B7%E1%84%80%E1%85%A8(%E1%84%90%E1%85%AE%E1%84%8C%E1%85%A1%E1%84%8C%E1%85%A9%E1%84%92%E1%85%A1%E1%86%B8)_180102.hwp" class="btn-link grey-600 mypage-setting-link-custom">(사용인감계 샘플)</a>
                    <!-- //사용인감계(선택) -->      

                    <div class="mypage-setting-desc mb15 red-800" v-if="request.memIvsRnmDocStatus == 'MIRT04'">
                        인증이 반려 되었습니다. 반려사유를 살펴보고 다시 제출해주세요. <br/>
                        <a href="javascript:void(0)" class="btn-link grey-600 pl0 pr0 ml10" v-on:click="showRejectedModal(1)">반려사유 보기 <i class="fa fa-angle-right ml5" aria-hidden="true"></i></a>
                    </div>

                    <div class="mypage-setting-desc">
                        실명확인 서류가 승인되지 않을 경우, 배정탈락의 사유가 될 수 있습니다.
                    </div>
                    
                    <!-- 실명확인용 서류 수정 -->
                    <div class="mypage-setting-btn mt20" v-on:click="updateRealNameDoc()" v-if="investor == 'Y' && request.memIvsRnmDocStatus == 'MIRT04'">설정 저장</div>
                    <!-- //실명확인용 서류 수정 -->

                    <!-- 투자 회원 이용약관 동의 -->
                    <div v-if="investor == 'N'" class="mt20">
                        <label class="chk_container mypage-setting-desc">투자회원 <a class="blue-800" href="/crowdy/term?menu=3" target="_blank"><strong>이용약관</strong></a>에 동의합니다.
                            <input type="checkbox" v-model="checkTerms1">
                            <span class="checkmark"></span>
                        </label>
                    </div>

                    <div v-if="investor == 'N'" class="mt10">
                        <label class="chk_container mypage-setting-desc">투자회원 <a class="blue-800" href="/crowdy/term?menu=5" target="_blank"><strong>개인정보 취급방침</strong></a>에 동의합니다.
                            <input type="checkbox" v-model="checkTerms2">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <!-- 투자 회원 이용약관 동의 -->

                    <div class="mt20 mypage-myaccount-layout" v-if="investor == 'N'">
                        <button type="button" class="mypage-setting-btn mypage-setting-btn-cancle">취소</button>
                        <button type="button" class="mypage-setting-btn" v-on:click="applyData()">등록</button>
                    </div>

                    <rejected-modal :rejectedReason="rejectedContent"></rejected-modal>

                    <form name="form_chk" method="post">
                        <input type="hidden" name="m" value="checkplusSerivce">                     <!-- 필수 데이타로, 누락하시면 안됩니다. -->
                        <input type="hidden" name="EncodeData" id="encodeData" value="">            <!-- 위에서 업체정보를 암호화 한 데이타입니다. -->

                        <input type="hidden" name="param_r1" value="accountSetting.authResult">
                        <input type="hidden" name="param_r2" value="">
                        <input type="hidden" name="param_r3" value="">
                    </form>
                </div>
            </div>
            `,
            data : function() {
                return {
                    dataConfirm : false,
                    request : {
                        memCode : userInfo.memCode,
                        memName : userInfo.name,
                        memType : userInfo.type,
                        memIvsAddr1 : "",
                        memIvsAddr2 : "",
                        memIvsPostNum : "",
                        memShotImg : userInfo.image,
                        authChange : false,
                        memIvsComNo : "",
                        memIvsCompanyName : userInfo.name,
                        memIvsCorNo : "",
                        memIvsStockType : "",
                        memIvsTellNo : "",
                        memIvsResName : "",
                        memIvsResEmail : "",
                        memIvsRnmDoc : "",
                        memIvsRnmDocStatus : "",
                        memIvsRnmFile : {
                            fileData : "",
                            fileName : "",
                            state: true
                        },
                        memIvsCorDoc : "",
                        memIvsCorFile : {
                            fileData : "",
                            fileName : "",
                            state: true  
                        },
                        memIvsBankImg : "",
                        memIvsBankFile : {
                            fileData : "",
                            fileName : "",
                            state: true    
                        },
                        memIvsRnmAttorney : "",
                        memIvsRnmAttorneyFile : {
                            fileData : "",
                            fileName : "",
                            state: true  
                        },
                        memIvsCorAttorney : "",
                        memIvsCorAttorneyFile : {
                            fileData : "",
                            fileName : "",
                            state: true  
                        },
                        memIvsRegiHpNo : "",
                        memIvsType: "MIT001",
                        memIvsSecurCode : "1",
                        memIvsSecurNo : "",
                        memIvsBankCode : "001",
                        memIvsBankNo : "",
                        memIvsState : "",
                        memIvsRnmReturn : ""
                    },
                    address : {
                        postNum : '',
                        address1 : ''
                    },
                    isCntdownRestart : false,
                    resultText : '',
                    bankCodeOptions : [],
                    securCodeOptions : [],
                    confirm : {
                        bank : false,
                        community : false,
                        fund : false,
                        secur : false
                    },
                    authType : 1,
                    investor : "",
                    rejectedContent: "",
                    memIvsRnmDocDisabled : false,
                    checkAuthTried : false,
                    checkTerms1 : false,
                    checkTerms2 : false,
                    os : '' 
                }
            },
            created : function() {
                this.getBankCodeList();
                this.getStockCodeList();
                this.loadInvestState();
                this.loadBizNum();
                
                window.accountSetting = this;

                // PC/Mobile 구분 
                this.deviceCheck();
            }, 
            mounted: function() {
                $(".switch-hb").bootstrapSwitch();
            },
            computed : {
                registerTitle : function() {
                    if(this.investor == 'Y') {
                        return "수정"
                    } else {
                        return "등록"
                    }
                },
                postNum : function() {
                    this.request.memIvsPostNum = this.address.postNum
                    return this.address.postNum
                },
                address1 : function() {
                    this.request.memIvsAddr1 = this.address.address1
                    return this.address.address1
                },
                userName : function() {
                    return userInfo.name
                },
                userEmail : function() {
                    return userInfo.email
                },
                userImage : function() {
                    return userInfo.image
                }
            },
            methods : {
                loadInvestState : function() {
                    var self = this;
                    
                    axios.post('/data/member/investor/state', this.request)
                        .then(function(response){
                            self.investor = response.data.rData.investor;
                            self.request.memIvsState = response.data.rData.memIvsState;

                            if(self.investor == 'Y') {
                                self.load();
                            } else {
                                self.$nextTick(function() {
                                    $( ".option_group .radio-inline input, .option_group .checkbox-inline input" ).checkboxradio();
                                    $( ".option1_radio, .option1_checkbox" ).controlgroup();
                                });
                            }
                        })
                },
                loadBizNum : function() {
                    var self = this;
                    
                    axios.post('/data/member/info', this.request)
                        .then(function(response){
                            self.request.memIvsComNo = response.data.rData.bizNum;
                        })
                },
                load : function() {
                    var self = this;
                    axios.post('/data/member/investor/info', this.request)
                        .then(function(response){
                            self.request.memIvsAddr1 = response.data.rData.memIvsAddr1;
                            self.request.memIvsAddr2 = response.data.rData.memIvsAddr2;
                            self.request.memIvsPostNum = response.data.rData.memIvsPostNum;
                            self.address.postNum =  response.data.rData.memIvsPostNum;
                            self.address.address1 = response.data.rData.memIvsAddr1;
                            self.request.memIvsResName = response.data.rData.memIvsResName;
                            self.request.memIvsResEmail = response.data.rData.memIvsResEmail;
                            self.request.memIvsRegiHpNo = response.data.rData.memIvsRegiHpNo;
                            self.request.memIvsTellNo = response.data.rData.memIvsTellNo;
                            self.request.memIvsSecurNo = response.data.rData.memIvsSecurNo;

                             if (response.data.rData.memIvsSecurCode.length > 0) {
                                self.request.memIvsSecurCode = response.data.rData.memIvsSecurCode;    
                            }

                            self.request.memIvsCorFile.fileName = response.data.rData.memIvsCorDocName;
                            self.request.memIvsBankFile.fileName = response.data.rData.memIvsBankImgName;
                            self.request.memIvsRnmFile.fileName = response.data.rData.memIvsRnmDocName;
                            self.request.memIvsRnmAttorneyFile.fileName = response.data.rData.memIvsRnmAttorneyName;
                            self.request.memIvsCorAttorneyFile.fileName = response.data.rData.memIvsCorAttorneyName;

                            self.request.memIvsRnmReturn = response.data.rData.memIvsRnmReturn;
                            self.request.memIvsRnmDocStatus = response.data.rData.memIvsRnmDocStatus;

                            if(self.request.memIvsRnmDocStatus == 'MIRT01' || self.request.memIvsRnmDocStatus == 'MIRT04') {
                                self.request.memIvsCorFile.fileName = "";
                                self.request.memIvsBankFile.fileName = "";
                                self.request.memIvsRnmFile.fileName = "";
                                self.request.memIvsRnmAttorneyFile.fileName = "";
                                self.request.memIvsCorAttorneyFile.fileName = "";

                                self.memIvsRnmDocDisabled = false;
                            } else {
                                self.memIvsRnmDocDisabled = true;
                            }

                            self.$nextTick(function() {
                                $( ".option_group .radio-inline input, .option_group .checkbox-inline input" ).checkboxradio();
                                $( ".option1_radio, .option1_checkbox" ).controlgroup();
                            });
                        });
                },
                subMenuChange : function(menu) {
                    this.type = menu;
                },
                postOpen : function() {
                    post.open(this);
                },
                deviceCheck : function() {
                    if(isMobile.apple.device) {
                        this.os = 'M';
                    } else if(isMobile.android.device) {
                        this.os = 'M';
                    } else {
                        this.os = 'W';
                    }
                },
                auth : function() {
                    this.request.authChange = false;

                    if (this.os == '') {
                        this.os = 'M';
                    }
                    
                    axios.post('/auth/nice/' + this.os)
                        .then(function(response){
                            // $('#encodeData').val(response.data.rData.data)
                            document.getElementById('encodeData').value = response.data.rData.data;
                            window.open('', 'popupChk', 'width=500, height=800, top=100, left=100, fullscreen=no, menubar=no, status=no, toolbar=no, titlebar=yes, location=no, scrollbar=no');
                            document.form_chk.action = "https://nice.checkplus.co.kr/CheckPlusSafeModel/checkplus.cb";
                            document.form_chk.target = "popupChk";
                            document.form_chk.submit();
                        });
                },
                authResult : function(result) {
                    if(result) {
                        var self = this;
                        axios.post('/auth/info')
                            .then(function(response) {
                                self.request.memIvsResName = response.data.name;
                                self.request.memIvsRegiHpNo = response.data.mobileNo;
                                self.request.authChange = true;
                                self.checkAuthTried = true;
                                noti.open('본인인증이 완료되었습니다. <br/> 남은 사항들을 입력해주세요.');
                            });
                    } else {
                        noti.open('본인인증에 실패했습니다 <br/> 다시시도해주세요.')
                    }
                },
                error : function(size) {
                    noti.open('업로드되는 파일 사이즈는 10MB보다 작아야합니다.')
                },
                getStockCodeList : function() {
                    var self = this;
                    
                    axios.post('/data/crowdy/code/invest', {gcode : "MEM_IVS_SECUR_CODE"})
                        .then(function(response){
                            var result = response.data;

                            if(result.rCode == "0000") {
                                for (var i = 0; i < result.rData.length; i++) {
                                    var option = {
                                            id : result.rData[i].commonCode,
                                            text : result.rData[i].commonInfo
                                    };
                                    self.securCodeOptions.push(option);
                                }
                                self.confirm.secur = true;
                            } else {
                                noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
                            }
                    })
                },
                getBankCodeList : function() {
                    var self = this;
                    
                    axios.post('/data/crowdy/code/invest', {gcode : "IRMI_STOCK_BANK"})
                        .then(function(response){
                            var result = response.data;

                            if(result.rCode == "0000") {
                                for (var i = 0; i < result.rData.length; i++) {
                                    var option = {
                                            id : result.rData[i].commonCode,
                                            text : result.rData[i].commonInfo
                                    };
                                    self.bankCodeOptions.push(option);
                                }
                                self.confirm.bank = true;
                            } else {
                                noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
                            }
                    })
                },
                applyData: function() {
                    if(this.checkTerms1 == false || this.checkTerms2 == false) {
                        noti.open("투자 회원 등록을 위해 약관 동의가 필요합니다.");
                        return;
                    }

                    if(!this.request.authChange) {
                        noti.open("본인인증을 완료해주십시오.");
                        return;
                    }

                    if(this.request.memIvsComNo.length == 0) {
                        noti.open("조합등록번호를 입력해주세요.");
                        return;
                    }

                    // if(this.request.memIvsSecurNo.length == 0) {
                    //     noti.open("증권계좌의 계좌번호를 입력해주세요.");
                    //     return;
                    // }

                    if(this.request.memIvsAddr2.length == 0) {
                        noti.open("주소를 입력해주세요.");
                        return;
                    }

                    if(this.request.memIvsCorFile.fileData.length == 0) {
                        noti.open("조합고유번호증은 실명확인용 필수서류입니다.");
                        return;
                    }

                    if(this.request.memIvsBankFile.fileData.length == 0) {
                        noti.open("통장사본은 실명확인용 필수서류입니다.");
                        return;
                    }

                    if(this.request.memIvsRnmFile.fileData.length == 0) {
                        noti.open("실명확인증표는 실명확인용 필수서류입니다.");
                        return;
                    }

                    if ((this.request.memIvsType == 'MIT002' || this.request.memIvsType == 'MIT003') && !this.ivsFileExist) {
                        noti.open("회원유형을 적격투자자나 전문투자자로 변경하시려면 <br/> 자격요건에 따른 서류를 제출하셔야합니다.");
                        return;
                    }

                    $('.page-loader-more').fadeIn('');
                    axios.post('/member/investor/insert', this.request)
                        .then(function(response){
                            $('.page-loader-more').fadeOut('');

                            // gtm 투자회원 가입 완료 이벤트 발생
                            window.dataLayer.push({
                                'event': 'InvestorComplete'
                            });
                            
                            noti.open('투자회원 등록 신청이 완료되었습니다.', function() {window.open("/", '_self');});
                        });
                },
                updateBasicInfo : function() {
                    this.request.authChange = false;
                    if(this.request.memIvsAddr2.length == 0) {
                        noti.open("주소를 입력해주세요.");
                        return;
                    }

                    $('.page-loader-more').fadeIn('');
                    axios.post('/member/investor/update/basic', this.request)
                        .then(function(response){
                            $('.page-loader-more').fadeOut('');
                            noti.open('기본정보 수정이 완료되었습니다.');
                        });
                },
                updateResInfo : function() {
                    if(this.checkAuthTried == false) {
                        this.request.authChange = false;
                    }

                    $('.page-loader-more').fadeIn('');
                    axios.post('/member/investor/update/representative', this.request)
                        .then(function(response){
                            $('.page-loader-more').fadeOut('');
                            noti.open('담당자 정보 변경이 완료되었습니다.');
                        });
                },
                updateRealNameDoc : function() {

                    if(this.request.memIvsCorFile.fileData.length == 0) {
                        noti.open("조합고유번호증은 실명확인용 필수서류입니다.");
                        return;
                    }

                    if(this.request.memIvsBankFile.fileData.length == 0) {
                        noti.open("통장사본은 실명확인용 필수서류입니다.");
                        return;
                    }

                    this.request.memIvsRnmDoc = "";

                    $('.page-loader-more').fadeIn('');
                    axios.post('/member/investor/update/real-name-doc', this.request)
                        .then(function(response){
                            $('.page-loader-more').fadeOut('');
                            noti.open('실명확인용 정보 수정 신청이 완료되었습니다.');
                        });
                },
                showRejectedModal : function() {
                    this.rejectedContent = this.request.memIvsRnmReturn;
                    $('#aRejectedModal').modal('show');  
                }
            },
            components :{
                dropifyInput : require('../common/dropify-input.js').default.component(),
                fileInput : require('../common/file-input.js').default.component(),
                bankSelect : require('../common/select.js').default.component(),
                securSelect : require('../common/select.js').default.component(),
                RejectedModal : require('../common/rejected-modal.js').default.component(),
                numberInput : require('../common/number-input.js').default.component()
            }
        }
    }
}

export default new MypageSettingMyMember4()