class MypageFundedReward {
    component() {
        return {
            template : `
            <div>
                <!-- Loop 결제완료 -->
                <div class="mif-list" v-for="item in fundedList">
                    <div class="mif-frame" v-on:click="detail(item.cpAliasUrl)" style="cursor: pointer;">
                        <div class="media">
                            <div class="media-left media-middle">
                                <img :src="'//' + item.cpCardImg + '/ycrowdy/resize/!340x!226'" class="media-object" />
                            </div>
                            <div class="media-body media-middle">
                                <div class="pn_list_frame">
                                    <div class="pn_summury" :class="{'red-800' : item.spsStatus == 2}">{{ statusCheck(item.spsStatus, item.spsDeliveryStatus, item.cpEndStatus) }}</div>
                                    <div class="pn_subject">{{ item.cpTitle }}</div>
                                    <div class="pn_time">
                                        <span>{{ item.projectAchievement }}%</span>
                                        <span>{{ item.paymentDate }} 결제일</span>
                                        <span class="xs-mt5">{{ item.fundWdate }} 펀딩참여일</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- 선택한 리워드 -->
                    <div class="row not-space row-label">
                        <div class="col-xs-3 col-sm-2">
                            <span>선택한 리워드</span>
                        </div>
                        <div class="col-xs-9 col-sm-10 text-label" v-if="item.cbfTitle != null ">
                            <span v-if="item.cbfTitle != ''">{{ item.cbfTitle }}</span>
                            <span v-if="item.cbfTitle == ''">없음</span>
                        </div>
                    </div>
                    <!-- //선택한 리워드 -->

                    <!-- 리워드 옵션 -->
                    <div class="row not-space row-label">
                        <div class="col-xs-3 col-sm-2">
                            <span>리워드 옵션</span>
                        </div>
                        <div class="col-xs-9 col-sm-10 text-label" v-if="item.cbfTitle != null ">
                            <span v-if="item.options != ''">{{ item.options }}</span>
                            <span v-if="item.options == ''">없음</span>
                        </div>
                    </div>
                    <!-- //리워드 옵션 -->

                    <!-- 리워드 구매 수량 -->
                    <div class="row not-space row-label">
                        <div class="col-xs-3 col-sm-2">
                            <span>구매 수량</span>
                        </div>
                        <div class="col-xs-9 col-sm-10 text-label" v-if="item.cbfTitle != null ">
                            <span>{{ item.sbfQty }}</span>
                        </div>
                    </div>
                    <!-- //리워드 구매 수량 -->

                    <!-- 받는분/연락처 -->
                    <div class="row not-space row-label">
                        <div class="col-xs-3 col-sm-2">
                            <span>받는분/연락처</span>
                        </div>
                        <div class="col-xs-9 col-sm-10 text-label">
                            <span> {{ item.spsRsName }}<small class="ml10">/ {{ item.spsMbMobileNo }} </small></span>
                        </div>
                    </div>
                    <!-- //받는분/연락처 -->

                    <!-- 배송지 -->
                    <div class="row not-space row-label" v-if="item.cbfDevrAddrYn == 'Y'">
                        <div class="col-xs-3 col-sm-2">
                            <span>배송지</span>
                        </div>
                        <div class="col-xs-9 col-sm-8 text-label">
                            <span>
                                {{ item.spsAddr1 }} {{ item.spsAddr2 }}
                            </span>
                        </div>
                        <div v-if="item.spsStatus == 0 && item.cpEndStatus != 3" class="hidden-xs col-xs-4 col-xs-offset-4 col-sm-2 col-sm-offset-0 text-right">
                            <span class="pr0"><a class="btn btn-block btn-sm btn-primary-outline" v-on:click="deliveryModalOpen(item)">배송지 변경</a></span>
                        </div>
                    </div>
                    <!-- //배송지 -->

                    <!-- 결제수단 -->
                    <div class="row not-space row-label">
                        <div class="col-xs-3 col-sm-2">
                            <span>결제수단</span>
                        </div>
                        <div class="col-xs-9 col-sm-8 text-label">
                            <span>{{ payType(item.payDiv, item.cardCompany) }}</span>
                        </div>
                        <div v-if="item.spsStatus == 0 && item.cpEndStatus == '1'" class="col-xs-4 col-xs-offset-4 col-sm-2 col-sm-offset-0 text-right">
                            <span class="pr0"><a class="btn btn-block btn-sm btn-primary-outline" v-on:click="payment('c', item)">결제수단 변경</a></span>
                        </div>
                    </div>
                    <!-- //결제수단 -->

                    <!-- 총 결제금액 -->
                    <div class="row not-space row-label">
                        <div class="col-xs-3 col-sm-2">
                            <span>총 결제금액</span>
                        </div>
                        <div class="col-xs-9 col-sm-8 text-label">
                            <span><strong class="blue-800">{{ parseInt(item.spsTotAmount).toLocaleString() }} 원</strong> <br class="hidden-sm hidden-md hidden-lg" />
                            <em>(펀딩금액 {{ parseInt((item.spsTotAmount) - (item.spsAmount)).toLocaleString() }} 원 + 추가펀딩 {{ parseInt(item.spsAmount).toLocaleString() }}원)</em></span>
                        </div>
                        <div v-if="item.spsStatus == 0 && item.cpEndStatus == '1'" class="hidden-xs col-xs-4 col-xs-offset-4 col-sm-2 col-sm-offset-0 text-right">
                            <span class="pr0"><a class="btn btn-block btn-sm btn-default-outline" v-on:click="fundingCancel(item)">펀딩 취소</a></span>
                        </div>
                        <div v-if="item.spsStatus == 2 && item.cpEndStatus == '2' && checkDate(item.cpEndDate) < 3 && (checkTime(item.cpEndDate) == 0)" class="hidden-xs col-xs-4 col-xs-offset-4 col-sm-2 col-sm-offset-0 text-right">
                            <span class="pr0"><a class="btn btn-block btn-sm btn-primary-outline" v-on:click="payment('i', item)">즉시결제</a></span>
                        </div>
                    </div>
                    <!-- //총 결제금액 -->

                    <div class="row row-mobile-n hidden-sm hidden-md hidden-lg mt10 mb10">
                        <div class="col-xs-4">
                            <a class="btn btn-block btn-sm btn-primary-outline" v-if="item.spsStatus == 0" v-on:click="deliveryModalOpen(item)">배송지 변경</a>
                        </div>
                        <div  v-if="item.spsStatus == 0 && item.cpEndStatus == '1'" class="col-xs-4">
                            <a class="btn btn-block btn-sm btn-primary-outline" v-on:click="fundingCancel(item)">펀딩 취소</a>
                        </div>
                        <div v-if="item.spsStatus == 2 && item.cpEndStatus == '2' && checkDate(item.cpEndDate) < 8 && (checkTime(item.cpEndDate) == 0)" class="col-xs-4">
                            <a class="btn btn-block btn-sm btn-primary-outline" v-on:click="payment('i', item)">즉시결제</a>
                        </div>
                    </div>
                </div>
                <!-- //Loop 결제완료 -->

                <!-- 페이징 -->
                <nav class="text-center mt30 mb20" >
                    <paginate
                        :page-count="pageCount"
                        :class="'pagination'"
                        :click-handler="nextPage"
                        :force-page="forcePage"
                        >
                    </paginate>
                </nav>
                <!-- //페이징 -->

                <!-- 펀딩취소 -->
                <div id="cancelModal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body modal-order">
                                "{{title}}" <br />펀딩을 취소합니다.<br />
                                <div class="mt10 red-800">정말 취소하시겠습니까?</div>
        
                                <div class="modal-footer text-center">
                                    <div class="row row-mobile-n">
                                        <div class="col-xs-8 col-xs-offset-2">
                                            <div class="row row-mobile-n">
                                                <div class="col-xs-6">
                                                    <a class="btn btn-block btn-primary-outline" v-on:click="cancelModalConfirm">예</a>
                                                </div>
                                                <div class="col-xs-6">
                                                    <a class="btn btn-block btn-default-outline" v-on:click="cancelModalNo">아니오</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //펀딩취소 -->

                <!-- 결제완료 : 배송지 변경 불가능 -->
                <div id="deliveryNModal" class="modal fade" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body modal-order">
                                프로젝트가 종료되어 배송지 변경은 <br />
                                제작자에게 직접 연락을 통해서만 가능합니다.
                                <div class="mt15 blue-800">(제작자 연락처 : 010.3544.2241)</div>
        
                                <div class="modal-footer text-center">
                                    <div class="row row-mobile-n">
                                        <div class="col-xs-4 col-xs-offset-4">
                                            <a class="btn btn-block btn-primary-outline" data-dismiss="modal">확인</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //결제완료 : 배송지 변경 불가능 -->

                <!-- 배송지 변경 -->
                <div id="deliveryModal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" style="overflow-y:scroll;">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" v-on:click="deliveryModalDismiss" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body">
                                <form class="form-horizontal" action="#">
                                    <!-- 배송정보 입력 -->
                                    <div class="pay_info_wrap">
                                        <div class="step-title">
                                            배송정보 입력
                                        </div>
                                        <div class="row row-mobile-n">
                                            <div class="col-sm-5">
                                                <!-- 받는 사람 휴대폰 번호 -->
                                                <div class="form-group row-mobile-n">
                                                    <label for="delivery_mobile" class="col-xs-12 control-label">
                                                        <div class="text-left">
                                                            받는 사람 휴대폰 번호
                                                        </div>
                                                    </label>
                                                    <div class="col-sm-12">
                                                        <div class="row row-mobile-n">
                                                            <div class="col-xs-12">
                                                                <number-input class="form-control" :num="deliveryData.spsRsMobileNo" v-model="deliveryData.spsRsMobileNo" maxlength="11" :class="{'error' : errors.has('spsRsMobileNo')}" data-vv-name="spsRsMobileNo" v-validate="'required'"></number-input>
                                                                <label style="margin-left:15px;" class="error" v-if="errors.has('spsRsMobileNo')" v-text="errors.first('spsRsMobileNo')"></label>
                                                            </div>
                                                        </div>
                                                        <label for="delivery_person_check" class="checkbox-inline">
                                                            <input type="checkbox" v-model="deliveryData.memberInfo" v-on:click="sameInfoInput"/><span class="label"></span><span class="label-text" v-on:click="sameInfoText">회원 정보와 동일</span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <!-- //결제자 휴대폰 번호 -->
                                            </div>
                                            <div class="col-sm-7">
                                                <!-- 받는 사람 -->
                                                <div class="form-group mb10 row-mobile-n">
                                                    <label for="delivery_person" class="col-xs-12 control-label">
                                                        <div class="text-left">
                                                            받는 사람
                                                        </div>
                                                    </label>
                                                    <div class="col-sm-12">
                                                        <input v-model="deliveryData.spsRsName" class="form-control" :class="{'error' : errors.has('spsRsName')}" data-vv-name="spsRsName" v-validate="'required'" placeholder="반드시 실명을 입력해주세요"/>
                                                        <label style="margin-left:15px;" class="error" v-if="errors.has('spsRsName')" v-text="errors.first('spsRsName')"></label>
                                                    </div>
                                                </div>
                                                <!-- //받는 사람 -->
                                            </div>
                                        </div>
        
                                        <!-- 우편번호 -->
                                        <div class="form-group row-mobile-n">
                                            <label for="zip_code" class="col-xs-12 control-label">
                                                <div class="text-left">
                                                    우편번호
                                                </div>
                                            </label>
                                            <div class="col-sm-6">
                                                <div class="input-group input-group-file">
                                                    <input v-model="postNum" v-on:click="postOpen" class="form-control" :class="{'error' : errors.has('postNum')}" data-vv-name="postNum" v-validate="'required'" readonly="readonly" />
                                                    <label style="margin-left:15px;" class="error" v-if="errors.has('postNum')" v-text="errors.first('postNum')"></label>
                                                    <span class="input-group-btn" v-on:click="postOpen">
                                                        <span class="btn btn-outline btn-file">
                                                            <i class="fa fa-upload" aria-hidden="true"></i>
                                                        </span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- //우편번호 -->
        
                                        <!-- 주소 -->
                                        <div class="form-group row-mobile-n">
                                            <label for="address1" class="col-xs-12 control-label">
                                                <div class="text-left">
                                                    주소
                                                </div>
                                            </label>
                                            <div class="col-xs-12 mb10">
                                                <input type="text" class="form-control" v-model="address1" :class="{'error' : errors.has('address1')}" data-vv-name="address1" v-validate="'required'" v-on:click="postOpen" placeholder="주소를 검색해주세요." readonly="readonly" />
                                                <label style="margin-left:15px;" class="error" v-if="errors.has('address1')" v-text="errors.first('address1')"></label>
                                            </div>
                                            <div class="col-xs-12">
                                                <input type="text" class="form-control" v-model="deliveryData.spsAddr2" :class="{'error' : errors.has('spsAddr2')}" data-vv-name="spsAddr2" v-validate="'required'" placeholder="상세 주소를 입력해주세요." />
                                                <label style="margin-left:15px;" class="error" v-if="errors.has('spsAddr2')" v-text="errors.first('spsAddr2')"></label>
                                            </div>
                                        </div>
                                        <!-- //주소 -->
        
                                        <!-- 제작자에게 요청사항 -->
                                        <div class="form-group mb0 row-mobile-n">
                                            <label for="delivery_person" class="col-xs-12 control-label">
                                                <div class="text-left">
                                                    진행자에게 요청사항
                                                </div>
                                            </label>
                                            <div class="col-sm-12">
                                                <textarea rows="3" cols="50" class="form-control" v-model="deliveryData.spsMiscInfo"></textarea>
                                            </div>
                                        </div>
                                        <!-- //제작자에게 요청사항 -->
                                    </div>
                                    <!-- //배송정보 입력 -->
                                </form>
        
                                <div class="modal-footer text-center">
                                    <div class="row row-mobile-n">
                                        <div class="col-xs-4 col-xs-offset-4">
                                            <a class="btn btn-block btn-primary" v-on:click="changeDelivery">배송지 변경</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //배송지 변경 -->

                <!-- 결제수단 변경 -->
                <div id="paymentModal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" style="overflow-y:scroll;">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" v-on:click="paymentModalDismiss" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body">
                                <form class="form-horizontal" action="#">        
                                    <!-- 결제정보입력 -->
                                    <div class="pay_info_wrap">
                                        <div class="step-title">
                                            결제정보입력
                                        </div>
        
                                        <!-- 신용/체크카드 결제 -->
                                        <div class="ptab_contents">
                                            <!-- 카드정보 -->
                                            <div class="form-group row-mobile-n">
                                                <div class="col-sm-7">
                                                    <div class="row row-mobile-n">
                                                        <label for="pay_card_num" class="col-xs-12 control-label">
                                                            <div class="text-left">
                                                                카드번호
                                                            </div>
                                                        </label>
                                                        <div class="col-xs-3">
                                                            <number-input class="form-control" :num="cardNo[0]" ref="cardText0" v-model="cardNo[0]" v-on:keydown="card(0)" maxlength="4" :class="{'error' : errors.has('cardNo[0]')}" data-vv-name="cardNo[3]" v-validate="'required'"></number-input>
                                                        </div>
                                                        <div class="col-xs-3">
                                                            <number-input class="form-control" :num="cardNo[1]" ref="cardText1" v-model="cardNo[1]" v-on:keydown="card(1)" maxlength="4" :class="{'error' : errors.has('cardNo[1]')}" data-vv-name="cardNo[3]" v-validate="'required'"></number-input>
                                                        </div>
                                                        <div class="col-xs-3">
                                                            <number-input class="form-control" :num="cardNo[2]" ref="cardText2" v-model="cardNo[2]" v-on:keydown="card(2)" maxlength="4" :class="{'error' : errors.has('cardNo[2]')}" data-vv-name="cardNo[3]" v-validate="'required'"></number-input>
                                                        </div>
                                                        <div class="col-xs-3">
                                                            <number-input class="form-control" :num="cardNo[3]" ref="cardText3" v-model="cardNo[3]" v-on:keydown="card(3)" maxlength="4" :class="{'error' : errors.has('cardNo[3]')}" data-vv-name="cardNo[3]" v-validate="'required'"></number-input>
                                                        </div>
                                                        <label style="margin-left:15px;" class="error" v-if="errors.has('cardNo[0]')" v-text="errors.first('cardNo[0]')"></label>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group row-mobile-n">
                                                <div class="col-sm-7">
                                                    <div class="row row-mobile-n">
                                                        <label for="pay_card_pass" class="col-xs-12 control-label">
                                                            <div class="text-left">
                                                                카드 비밀번호
                                                            </div>
                                                        </label>
                                                        <div class="col-xs-3">
                                                            <input type="password" class="form-control" style="font-size: 14px;" v-model="cardData.cardPw" v-on:keypress="isNumber" placeholder="앞 두자리" :class="{'error' : errors.has('cardPw')}" data-vv-name="cardPw" v-validate="'required'" maxlength="2" />
                                                        </div>
                                                        <div class="col-xs-3">
                                                            <p class="form-control-static form-control-static-big"> · ·</p>
                                                        </div>
                                                        <label style="margin-left:15px;" class="error" v-if="errors.has('cardPw')" v-text="errors.first('cardPw')"></label>
                                                    </div>
                                                </div>
                                            </div>
        
                                            <div class="form-group row-mobile-n">
                                                <div class="col-sm-7">
                                                    <div class="row row-mobile-n">
                                                        <label for="pay_card_month" class="col-xs-12 control-label">
                                                            <div class="text-left">
                                                                유효기간
                                                            </div>
                                                        </label>
                                                        <div class="col-xs-3">
                                                            <number-input class="form-control" ref="expMonth" v-on:keydown="validData(1)" :num="cardData.expMonth" v-model="cardData.expMonth" :class="{'error' : errors.has('expMonth')}" data-vv-name="expMonth" v-validate="'required'" placeholder="MM" maxlength="2"></number-input>
                                                        </div>
                                                        <div class="col-xs-3">
                                                            <number-input class="form-control" ref="expYear" v-on:keydown="validData(2)" :num="cardData.expYear" v-model="cardData.expYear" :class="{'error' : errors.has('expYear')}" data-vv-name="expYear" v-validate="'required'" placeholder="YY" maxlength="2"></number-input>
                                                        </div>
                                                        <label style="margin-left:15px;" class="error" v-if="errors.has('expMonth')" v-text="errors.first('expMonth')"></label>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- //카드정보 -->
                                        </div>
                                        <!-- //신용/체크카드 결제 -->
        
                                        <!-- 생년월일(앞 6자리) -->
                                        <div class="form-group row-mobile-n">
                                            <label for="pay_happy" class="col-xs-12 control-label">
                                                <div class="text-left">
                                                    생년월일 <span style="display:inline-block;">(앞 6자리)</span>
                                                </div>
                                            </label>
                                            <div class="col-sm-7">
                                                <number-input class="form-control" :num="cardData.idNo" v-model="cardData.idNo" :class="{'error' : errors.has('idNo')}" data-vv-name="idNo" v-validate="'required'" placeholder="법인카드의 경우 사업자등록번호 10자리 입력" maxlength="10"></number-input>
                                                <label style="margin-left:15px;" class="error" v-if="errors.has('idNo')" v-text="errors.first('idNo')"></label>
                                            </div>
                                        </div>
                                        <!-- //생년월일(앞 6자리) -->
        
                                        <!-- 정보동의 -->
                                        <div class="form-group row-mobile-n">
                                            <label for="pay_consent1" class="col-xs-12 control-label">
                                                <div class="text-left">
                                                    <strong>정보동의</strong>
                                                </div>
                                            </label>
                                            <div class="col-xs-12">
                                                <label for="pay_consent2" class="checkbox-inline">
                                                    <input type="checkbox" name="pay_consent2" id="pay_consent2" v-model="cardData.agree" /><span class="label"></span><span class="label-text grey-800">결제사 정보 제공을 동의합니다.</span>
                                                </label>
                                                <div class="ml25"><a href="javascript:void(0)" class="btn-link grey-500" v-on:click="policy">결제사 정보 제공 약관 보기 <i class="fa fa-angle-right ml5" aria-hidden="true"></i></a></div>
                                            </div>
                                        </div>
                                        <!-- //정보동의 -->
                                    </div>
                                    <!-- //결제정보입력 -->
                                </form>
        
                                <div class="modal-footer text-center">
                                    <div class="row row-mobile-n">
                                        <template>
                                            <div v-if="paymentType == 'c'" class="col-xs-4 col-xs-offset-4">
                                                <a class="btn btn-block btn-primary" v-on:click="paymentConfirm('c')">결제수단 변경</a>
                                            </div>
                                            <div v-else class="col-xs-4 col-xs-offset-4">
                                                <a class="btn btn-block btn-primary" v-on:click="paymentConfirm('i')">즉시결제</a>
                                            </div>
                                        </template>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //결제수단 변경 -->

                <!-- 결제사 정보 제공 약관 -->
                <div class="modal fade modal-primary" id="privacyCardPopup" role="dialog" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body">
                                <h4 class="blue-800 text-center mb20">결제사 정보 제공 약관 보기</h4>
                                <ol>
                                    <li>1. 귀하가 신청하신 신용카드 정기과금 결제는 나이스정보통신(주)에서 제공하는 서비스로, 귀하의 신용카드 결제내역에는 이용가맹점이 NICE로 표기됩니다. 또한, 나이스정보통신㈜는 정기과금 결제대행만 제공하므로, 정기과금 결제신청 및 해지 등 모든 업무는 해당 인터넷 상점을 통해 직접 요청하셔야 합니다.</li>
                                    <li>2. 나이스정보통신㈜는 귀하의 본 신청과 관련한 거래내역을 e-mail로 통보 드리며, 당사 홈페이지 (<a href="https://www.nicepay.co.kr" target="_blank">https://home.nicepay.co.kr</a>)에서도 조회서비스를 제공합니다.</li>
                                    <li>3. 나이스정보통신㈜는 조회 등의 기본 서비스제공을 위해 필요한 최소 정보(성명, 이메일)만을 보관하고 있습니다.</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //결제사 정보 제공 약관 -->

            </div>
            `,
            props : ['code'],
            data : function() {
                return {
                    fundedList : [],
                    pageCount : 0,
                    forcePage : 0,
                    search : {
                        memCode : userInfo.memCode,
                        paging : {
			                page : "1",
			                count : "5"
                        },
                        cpTrainingYn : 'N',
                    },
                    fundCancelData : {
                        cpCode : '',
                        memCode : '',
                        spsIdx : '',
                        spsStatus : '9'
                    },
                    deliveryData : {
                        spsRsMobileNo : '',
                        spsRsName : '',
                        spsPostNum : '',
                        spsAddr1 : '',
                        spsAddr2 : '',
                        spsMiscInfo : '',
                        memberInfo : false,
                    },
                    address : {
						postNum : '',
						address1 : ''
                    },
                    sponsorCardData : {
                        bankCode : '',
                        payNum : '',
                        payInfoNum : '',
                        spsIdx : '',
                        spsPgNo : '',
                        spsStatus : '',
                    },
                    cardData : {
                        cardNo : [],
                        cardPw : '',
                        expMonth : '',
                        expYear : '',
                        idNo : '',
                        agree : false,
                    },
                    //카드승인 데이터
                    cardApprovalData : {
                        billKey : '',
                        buyerName : '',
                        amt : '',
                    },
                    cardNo : [],
                    cardText : [],
                    title : '',
                    phoneNo : '',
                    //즉시결제, 결제수단 변경
                    paymentType : '',
                    sameInfoCheck : false,
                }
            },
            created : function() {
                this.load();
            },
            components : {
                paginate : VuejsPaginate,
                numberInput : require('../common/number-input.js').default.component()
            },
            computed : {
                postNum : function() {
                    this.deliveryData.spsPostNum = this.address.postNum
                    return this.address.postNum
                },
                address1 : function() {
                    this.deliveryData.spsAddr1 = this.address.address1
                    return this.address.address1
                }
            },
            methods : {
                load : function() {
                    var self = this;
                    axios.post('/data/mypage/funded/reward', self.search)
                        .then(function(response){
                            var remainder = parseInt(response.data.rData.count % self.search.paging.count);
                            var remainderCount = 0;
                            if(remainder > 0) {
                                remainderCount++;
                            }
                            self.pageCount = (parseInt(response.data.rData.count / self.search.paging.count)) + remainderCount;
                            self.fundedList = response.data.rData.data;
                        })
                },
                nextPage : function(page) {
                    this.forcePage = page - 1;
                    this.search.paging.page = page;
                    this.load();
                },
                statusCheck : function(cpStatus, dlvStatus, endStatus) {
                    var result = "";
                    //후원상태(공통코드)0:결제대기,1:결제완료,2:결제실패,3:결제취소,9:후원취소
                    //배송상태(배송없음:0 배송대기:1  배송중:2 배송완료:3 취소:4)-공통코드
                    if(cpStatus == "0") {
                        if (endStatus == '3') {
                            result = "프로젝트 종료";
                        } else {
                            result = "결제대기";    
                        }
                    } else if(cpStatus == "1") {
                        result = "결제완료";
                    } else if(cpStatus == "2") {
                        result = "결제실패";
                    } else if(cpStatus == "3") {
                        result = "결제취소";
                    } else if(cpStatus == "9") {
                        result = "후원취소";    
                    } else if(cpStatus == "1" && dlvStatus == "0") {
                        result = "배송없음";
                    } else if(cpStatus == "1" && dlvStatus == "1") {
                        result = "배송대기";
                    } else if(cpStatus == "1" && dlvStatus == "2") {
                        result = "배송중";
                    } else if(cpStatus == "1" && dlvStatus == "3") {
                        result = "배송완료";
                    } else if(cpStatus == "1" && dlvStatus == "4") {
                        result = "취소";
                    }
                    return result;
                },
                payType : function(val, card) {
                    //결제구분(카드:1, 계좌이체:2, 쿠폰:3)
                    var result = "";

                    if(val == "1"){
                        result = card + "카드";
                    } else if(val == "2") {
                        result = "계좌이체";
                    } else if(val == "3") {
                        result = "쿠폰";
                    }
                    return result;
                },
                //펀딩 취소
                fundingCancel : function(data) {
                    var self = this;
                    $('#cancelModal').modal('show');
                    self.title = data.cpTitle;
                    self.fundCancelData.cpCode = data.cpCode;
                    self.fundCancelData.memCode = userInfo.memCode;
                    self.fundCancelData.spsIdx = data.spsIdx;
                },
                //우편번호
                postOpen : function() {
					post.open(this);
                },
                //배송지 변경 Modal
                deliveryModalOpen : function(data) {
                    $('#deliveryModal').modal('show');
                    this.deliveryData.spsIdx = data.spsIdx;
                    this.deliveryData.memCode = userInfo.memCode;
                    this.deliveryData.spsRsMobileNo = data.spsRsMobileNo;
                    this.deliveryData.spsRsName = data.spsRsName;
                    this.address.postNum = data.spsPostNum;
                    this.address.address1 = data.spsAddr1;
                    this.deliveryData.spsPostNum = this.address.postNum;
                    this.deliveryData.spsAddr1 = this.address.address1;
                    this.deliveryData.spsAddr2 = data.spsAddr2;
                    this.deliveryData.spsMiscInfo = data.spsMiscInfo;
                },
                //배송지 변경 Modal Close
                deliveryModalDismiss : function() {
                    $('#deliveryModal').modal('hide');
                    this.deliveryDataInit();
                },
                //배송지 정보 초기화
                deliveryDataInit : function() {
                    this.deliveryData.spsRsMobileNo = '';
                    this.deliveryData.spsRsName = '';
                    this.deliveryData.spsPostNum = '';
                    this.deliveryData.spsAddr1 = '';
                    this.deliveryData.spsAddr2 = '';
                    this.deliveryData.spsMiscInfo = '';
                },
                //펀딩취소 - 확인
                cancelModalConfirm : function() {
                    var self = this; 
                    axios.post('/data/reward/funding/cancel', this.fundCancelData)
                    .then(function (response) {
                        var result = response.data;
                        if(result.rCode == "0000") {
                            noti.open('취소되었습니다.');

                            // GTM - 향상된 전자상거래 환불 데이터 삽입
                            dataLayer.push({
                              'ecommerce': {
                                'refund': {
                                  'actionField': { 'id': self.fundCancelData.spsIdx }
                                }
                              },
                              'event' : 'transactionrefund'
                            });

                            self.load();
                        } else {
                            noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
                        }                                  
                    })
                    $('#cancelModal').modal('hide');
                },
                //펀딩취소 - 취소
                cancelModalNo : function() {
                    $('#cancelModal').modal('hide');
                },
                //결제수단 변경, 즉시결제
                payment : function(val, data) {
                    this.paymentType = val;
                    this.sponsorCardData.spsIdx = data.spsIdx;
                    //즉시결제값 셋팅
                    if(val == 'i') {
                        this.cardApprovalData.buyerName = data.memName;
                        this.cardApprovalData.amt = data.spsTotAmount;
                        this.sponsorCardData.spsStatus = '1';
                    }
                    $('#paymentModal').modal('show');
                },
                //결제수단 변경, 즉시결제 닫기
                paymentModalDismiss : function() {
                    $('#paymentModal').modal('hide');
                    this.paymentDataInit();
                },
                //결제수단 초기화
                paymentDataInit : function() {     
                    this.cardNo = [];
                    this.cardData.cardNo = '';
                    this.cardData.cardPw = '';
                    this.cardData.expMonth = '';
                    this.cardData.expYear = '';
                    this.cardData.idNo = '';
                    this.cardData.agree = false;
                    this.cardApprovalData.billKey = '';
                    this.cardApprovalData.buyerName = '';
                    this.cardApprovalData.amt = '';
                    this.sponsorCardData.bankCode = '';
                    this.sponsorCardData.payNum = '';
                    this.sponsorCardData.payInfoNum = '';
                    this.sponsorCardData.spsIdx = '';
                    this.sponsorCardData.spsPgNo = '';
                    this.sponsorCardData.spsStatus = '';
                },
                //결제사 정보 제공 약관
                policy : function() {
                    $('#privacyCardPopup').modal('show');
                },
                //배송지 변경
                changeDelivery : function() {
                    var self = this;
                    if(this.deliveryData.spsRsMobileNo == "") {
                        noti.open("받는사람 휴대폰 번호를 입력해주세요.")
                        return false;
                    }
                    if(this.deliveryData.spsRsName == "") {
                        noti.open("받는사람을 입력해주세요.")
                        return false;
                    }
                    if(this.address.postNum == "") {
                        noti.open("주소를 선택해 주세요.")
                        return false;
                    }
                    if(this.address.address1 == "") {
                        noti.open("주소를 선택해 주세요.")
                        return false;
                    }
                    if(this.deliveryData.spsAddr2 == "") {
                        noti.open("배송지 상세 주소를 입력해 주세요.")
                        return false;
                    }

                    axios.post('/set/reward/funding/change/delivery', this.deliveryData)
                    .then(function (response) {
                        var result = response.data;
                        if(result.rCode == "0000") {
                            noti.open('변경되었습니다.');
                            self.load();
                            self.deliveryDataInit();
                            $('#deliveryModal').modal('hide');
                        } else {
                            noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
                        }                                  
                    })
                },
                //숫자만 입력
                isNumber : function(evt) {
                    evt = (evt) ? evt : window.event;
                    var charCode = (evt.which) ? evt.which : evt.keyCode;
                    if ((charCode > 31 && (charCode < 48 || charCode > 57)) && charCode !== 46) {
                      evt.preventDefault();
                    } else {
                      return true;
                    }
                },
                //즉시결제 및 결제수단 변경
                paymentConfirm : function(val) {
                    var self = this;            
      
                    this.cardData.cardNo = this.cardNo.join('');
                    //결제수단 변경
                    if(this.cardData.agree) {
                        if(val == 'c') {
                            this.checkCardInfo(val);
                        } else { //즉시결제
                            this.checkCardInfo(val);
                        }
                    } else {
                        $('.page-loader-more').fadeOut('')
                        noti.open("결제사 정보 제공 동의 해야 결제예약이 진행 가능합니다.")
                        return;
                    }
                },
                //카드정보 확인
                checkCardInfo : function(val) {
                    var self = this;
                    $('.page-loader-more').fadeIn('')
                    axios.post('/data/reward/funding/bill/issue', this.cardData)
                    .then(function (response) {
                        var result = response.data.rData;
                        self.sponsorCardData.bankCode = result.cardCode;
                        self.sponsorCardData.payNum = result.bid;
                        self.sponsorCardData.payInfoNum = self.cardData.idNo;
                        if(result.resultCode != "F100") {
                            $('.page-loader-more').fadeOut('')
                            var error = result.resultMsg;
                            noti.open("결제정보를 확인해 주세요. " + error);
                            return;
                        } else if (val == 'c'){ //결제정보 변경일때
                            self.paymentUpdate(val);
                        } else if (val == 'i') { //즉시결제일때
                            self.immediatelyPayment(val);
                        }
                    });
                },
                //결제정보 업데이트
                paymentUpdate : function(val) {
                    var self = this;                    
                    axios.post('/data/reward/funding/change/cardInfoUpdate', self.sponsorCardData)
                    .then(function (response) {
                        var result = response.data;
                        if(result.rCode == "0000") {
                            $('.page-loader-more').fadeOut('')
                            self.load();
                            self.paymentModalDismiss();
                            if(val == 'c') {
                                noti.open('결제 정보가 변경 되었습니다.');
                            } else if(val == 'i') {
                                noti.open('즉시결제 되었습니다.');
                            }
                        } else {
                            $('.page-loader-more').fadeOut('')
                            noti.open("시스템 문제가 발생하였습니다.")
                        }                                  
                    })
                },
                //즉시결제
                immediatelyPayment : function(val) {
                    var self = this;
                    this.cardApprovalData.billKey = this.sponsorCardData.payNum;
                    //$('.page-loader-more').fadeIn('')
                    axios.post('/data/reward/funding/bill/approve', this.cardApprovalData)
                    .then(function (response) {
                        var result = response.data.rData;
                        self.sponsorCardData.spsPgNo = result.spsPgNo;
                        if(result.resultCode != "3001") {
                            $('.page-loader-more').fadeOut('')
                            var error = result.resultMsg;
                            noti.open("결제정보를 확인해 주세요." + error);
                            return;
                        } else {
                            self.paymentUpdate(val);
                        }
                    });
                },
                sameInfoInput : function() {
                    this.sameInfo();
                },
                sameInfoText : function() {
                    this.sameInfo();
                },
                //회원 정보 동일
                sameInfo : function() {
                    var self = this;
                    if(this.deliveryData.memberInfo == false) {
                        this.deliveryData.memberInfo = true;
                    } else {
                        this.deliveryData.memberInfo = false;
                    }

                    if(this.deliveryData.memberInfo == true) {
                        axios.post('/data/member/info', { memCode : userInfo.memCode })
                        .then(function (response) {
                            var result = response.data.rData;
                            self.deliveryData.spsRsMobileNo = result.mobileNo == "NULL" ? "" : result.mobileNo;
                            self.deliveryData.spsRsName = result.resName == "NULL" ? "" : result.resName;
                            self.address.postNum = result.memSpsPostNum == "NULL" ? "" : result.memSpsPostNum;
                            self.address.address1 = result.memSpsAddr1 == "NULL" ? "" : result.memSpsAddr1;
                            self.deliveryData.spsAddr2 = result.memSpsAddr2 == "NULL" ? "" : result.memSpsAddr2;
                        })
                    } else {
                        self.deliveryData.spsRsMobileNo = "";
                        self.deliveryData.spsRsName = "";
                        self.address.postNum = "";
                        self.address.address1 = "";
                        self.deliveryData.spsAddr2 = "";
                    }
                },
                detail : function(url) {
                    window.open("/r/" + url, '_self');
                },
                card : function(val) {
                    var self = this;
                    self.$nextTick(function() {
                        if(_.toString(this.cardNo[val]).length == 4) {
                            if(val == 0) {
                                this.$refs.cardText1.focus();
                            } else if(val == 1) {
                                this.$refs.cardText2.focus();
                            } else if(val == 2) {
                                this.$refs.cardText3.focus();
                            }
                        }
                    });
                },
                validData : function(val) {
                    var self = this;
                    self.$nextTick(function() {
                        if(_.toString(this.cardData.expMonth).length == 2) {
                            if(val == 1) {
                                this.$refs.expYear.focus();
                            }
                        }
                    });
                },
                //즉시결제 날짜 확인
                checkDate : function(date) {
                    var end = moment(date);
                    var now = moment(new Date());
                    var duration = now.diff(end, 'days');
                    return duration;
                },
                //즉시결제 가능시간 확인(종료후 +7일 / 23:40분까지)
                checkTime : function(date) {
                    var timeCheck = 0;
                    var end = moment(date);
                    var now = moment(new Date());
                    var duration = now.diff(end, 'days');
                    //종료후 즉시결제가능 마지막날 23:40분 이후면 즉시결제 x
                    if(duration == 7 && (moment(now, "hmm").format("HH:mm") > "23:40"))  {
                        timeCheck++;
                    }
                    return timeCheck;
                }
            }
        }
    }
}

export default new MypageFundedReward()