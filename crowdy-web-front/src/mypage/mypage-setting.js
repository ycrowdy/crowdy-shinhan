class MypageSetting {
    component() {
        return {
            template : `
            <div class="container">
                <div class="row not-space">
                    
                    <!--sub menu-->
                    <!--pc & tablet--> 
                    <div class="col-md-2 col-sm-3 hidden-xs">
                        <div class="mypage-setting-sub-box webfont2">
                            <div :class="{'mypage-setting-name' : userType == 1, 'mypage-setting-name-1' : userType != 1}">{{userName}} 님</div>
                            <div class="mypage-setting-email">{{userEmail}}</div>
                            <ul>
                                <li class="w50" :class="{'active' : type == 1}"><a href="javascript:void(0)" v-on:click="subMenuChange(1)">계정 설정</a></li>
                                <li class="w50" :class="{'active' : type == 2}"><a href="javascript:void(0)" v-on:click="subMenuChange(2)">투자 회원 설정</a></li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <!-- //pc & tablet--> 

                    <!--mobile--> 
                    <div class="visible-xs-block mt30">
                        <div class="common-default-flex">
                            <div class="mypage-sub-menu" :class="{'mypage-sub-menu-on' : type == 1}" v-on:click="subMenuChange(1)">계정 설정</div>
                            <div class="mypage-sub-menu" :class="{'mypage-sub-menu-on' : type == 2}" v-on:click="subMenuChange(2)">투자회원 설정</div>
                        </div>
                    </div>
                    <!--//mobile--> 
                    <!--//sub menu-->

                    <div class="col-md-8 col-sm-8">

                        <myaccount1 v-if="type == 1 && userType == 1" :naver-oauth="naverOauth"></myaccount1>
                        <myaccount2 v-if="type == 1 && (userType == 2 || userType == 3)"></myaccount2>
                        
                        <mymember1 v-if="type == 2 && investorState == false && virtualState == false" v-on:change-register-ivs-info="changeRegisterIvsInfo" v-on:change-virtual-account="changeVirtualAccount"></mymember1>
                        <mymember2 v-if="type == 2 && investorState == true && userType == 1 && virtualState == false" v-on:change-register-ivs-info="changeRegisterIvsInfo"></mymember2>
                        <mymember3 v-if="type == 2 && investorState == true && userType == 2 && virtualState == false" v-on:change-register-ivs-info="changeRegisterIvsInfo"></mymember3>
                        <mymember4 v-if="type == 2 && investorState == true && userType == 3 && virtualState == false" v-on:change-register-ivs-info="changeRegisterIvsInfo"></mymember4>
                        
                        <virtualAccount v-if="type == 2 && virtualState == true && withdrawAccountFlag == false" :virtualAccountConfirm = "virtualAccountConfirm" :memCiConfirm="memCiConfirm" v-on:change-withdraw-flag="changeWithdrawFlag" v-on:set-ci-confirm-flag="setCiConfirmFlag" v-on:set-virtual-account-flag="setVirtualAccountFlag"></virtualAccount><!--가상계좌-->
                        <withdrawAccount v-if="type == 2 && virtualState == true && withdrawAccountFlag == true" :bankStatus="bankStatus" v-on:change-withdraw-flag="changeWithdrawFlag"></withdrawAccount><!--출금계좌 -->
                        
                    </div>
                </div>
            </div>
            `,
            props : ['subMenu', 'naverOauth'],
            data : function() {
                return {
                    type : this.subMenu,
                    registerIvsInfo : false,
                    virtualFlag : false,
                    withdrawAccountFlag : false,
                    investor : "",
                    memIvsState : "",
                    memIvsType : "",
                    bankStatus : '0',
                    virtualAccountConfirm : 'N',
                    memCiConfirm : 'N',
                    userName : userInfo.name,
                    userEmail: userInfo.email,
                    request : {
                        memCode : userInfo.memCode
                    }
                }
            },
            created : function() {
                // this.load();
                this.loadInvestState();
            },
            components : {
                myaccount1 : require('./mypage-setting-myaccount1.js').default.component(),
                myaccount2 : require('./mypage-setting-myaccount2.js').default.component(),
                mymember1 : require('./mypage-setting-ivs-info1.js').default.component(),
                mymember2 : require('./mypage-setting-ivs-info2.js').default.component(),
                mymember3 : require('./mypage-setting-ivs-info3.js').default.component(),
                mymember4 : require('./mypage-setting-ivs-info4.js').default.component(),
                virtualAccount : require('./mypage-virtual-account.js').default.component(),
                withdrawAccount : require('./mypage-withraw-account.js').default.component()
            },
            computed : {
                userType : function() {
                    return userInfo.type
                },
                investorState : function() {
                    return this.registerIvsInfo ? true : false;
                },
                virtualState : function() {
                    return this.virtualFlag ? true : false;
                }
            },   
            methods : {
                loadInvestState : function() {
                    var self = this;

                    $('.page-loader-more').fadeIn('');
                    axios.post('/data/member/investor/state', this.request)
                        .then(function(response){
                            $('.page-loader-more').fadeOut('');
                            self.investor = response.data.rData.investor;
                            self.memIvsState = response.data.rData.memIvsState;
                            self.memIvsType = response.data.rData.memIvsType;
                            self.bankStatus = response.data.rData.bankStatus;
                            self.virtualAccountConfirm = response.data.rData.virtualAccountConfirm;
                            self.memCiConfirm = response.data.rData.memCiConfirm;
                        })
                },
                subMenuChange : function(menu) {

                    if (menu == '2') {
                        window.open('/mypage/main?menu=5&sub-menu=2', '_self');
                    } else {
                        this.type = menu;    
                    }
                },
                changeRegisterIvsInfo : function(status) {
                    if(status) {
                        this.registerIvsInfo = true;
                    } else {
                        this.registerIvsInfo = false;
                    }
                },
                changeVirtualAccount : function(status){
                    if(status) {
                        this.virtualFlag = true;
                    } else {
                        this.virtualFlag = false;
                    }
                },
                changeWithdrawFlag : function(status){
                    if(status) {
                        this.withdrawAccountFlag = true;
                        this.loadInvestState();
                    } else {
                        this.withdrawAccountFlag = false;
                    }
                },
                setCiConfirmFlag : function(status){
                     if(status) {
                        this.memCiConfirm = "Y";
                        this.loadInvestState();
                    } else {
                        this.memCiConfirm = "N";
                    }
                },
                setVirtualAccountFlag : function(status){
                    if(status) {
                        // this.setAccountFlag = true;
                        this.virtualAccountConfirm = "Y";
                        this.loadInvestState();
                    } else {
                        this.virtualAccountConfirm = "N";
                    }
                }
            }
        }
    }
}

export default new MypageSetting()