class MypageSettingMyMember1 {
    component() {
        return {
            template : `
                <div>
                    <div class="mypage-setting-big-text mypage-setting-layout">
                        <template v-if="investor == 'N'">
                            <span class="mypage-setting-color-text">{{userName}}</span>님은<br/>아직 투자회원이 아닙니다<br />
                        <span class="mypage-setting-small-text">투자회원 등록 한 후 '투자자'가 되어보세요</span>
                        </template>

                        <template v-if="investor == 'Y' && memIvsState == 'MIC001'">
                            <span class="mypage-setting-color-text">{{userName}}</span>님은 투자회원 
                            <span class="mypage-setting-color-text">승인 대기중</span>입니다.<br/>영업일 3일 소요<br/>
                            <span class="mypage-setting-small-text" v-if="virtualAccountConfirm == 'N'">승인을 기다리는 동안 <b>'투자계좌 설정'</b>을 해주세요.</span>
                        </template>

                        <template v-if="investor == 'Y' && memIvsState == 'MIC002'">
                            <span class="mypage-setting-color-text">{{userName}}</span>님은 
                            <span class="mypage-setting-color-text">투자회원</span>이며,<br/>회원유형은 
                            <span class="mypage-setting-color-text">{{userIvsType}}</span> 입니다<br/>
                            <span class="mypage-setting-small-text pl30 pr30 xs-pl20 xs-pr20">
                                <template v-if="virtualAccountConfirm == 'N'">
                                    투자계좌를 설정해야 투자 프로젝트에 참여할 수 있습니다.
                                    <b>‘투자계좌 설정’</b>을 해주세요.
                                </template>
                                <template v-if="virtualAccountConfirm == 'Y'">
                                    회원유형 및 연락처 및 주소 변경은 <b>투자회원 설정</b>에서,
                                    증권계좌 및 투자 예치금 계좌에 대한 확인 및 변경은
                                    <b>투자계좌 설정</b>에서 하실 수 있습니다.
                                </template>
                            </span>
                        </template>

                        <template v-if="investor == 'Y' && memIvsState == 'MIC003'">
                            <span class="mypage-setting-color-text">{{userName}}</span>님은 투자회원 승인이<br/>
                            <span class="mypage-setting-color-text">반려되었습니다</span>
                            <span class="mypage-setting-small-text">
                                <b>'투자회원 설정'</b>에서 반려사유를 확인하세요<br/>
                                문의 : 02.6954.2568  |  contact@ycrowdy.com
                            </span>
                        </template>

                        <div class="mt40" v-if="investor == 'N'">
                            <a class="mypage-btn-1" v-on:click="registerInfo">투자회원 등록하기</a>
                        </div>

                        <div class="mt40" v-if="investor == 'Y'">
                            <a class="mypage-btn-2 mr15 xs-mr10" v-on:click="registerInfo" :class="{'mypage-btn-active' : memIvsState == 'MIC003'}">투자회원 설정</a>
                            <a class="mypage-btn-2" v-on:click="virtualAccount" :class="{'mypage-btn-active' : virtualAccountConfirm == 'N'}">투자계좌 설정</a>
                        </div>
                    </div>
                </div>
            `,
            data : function() {
                return {
                    type : 1,
                    investor : "",
                    virtualAccountConfirm : "",
                    memIvsState : "",
                    memIvsType : "",
                    request : {
                        memCode : userInfo.memCode,
                        memShotImg : userInfo.image
                    }
                }
            },
            created : function() {
                this.loadInvestState();
                
            }, 
            mounted: function() {
                $(".switch-hb").bootstrapSwitch();
            },
            computed : {
                userImage: function() {
                    return userInfo.image
                },
                userName: function() {
                    return userInfo.name
                },
                userIvsType : function() {
                    if(this.memIvsType == 'MIT001') {
                        return "일반투자자"
                    } else if(this.memIvsType == 'MIT002') {
                        return "소득요건 등 적격투자자"
                    } else if(this.memIvsType == 'MIT003') {
                        return "전문투자자"
                    }
                }
            },
            methods : {
                loadInvestState : function() {
                    var self = this;
                    axios.post('/data/member/investor/state', this.request)
                        .then(function(response){
                            self.memIvsType = response.data.rData.memIvsType;
                           
                            self.investor = response.data.rData.investor;
                            self.memIvsState = response.data.rData.memIvsState;
                            self.virtualAccountConfirm = response.data.rData.virtualAccountConfirm;
                        })
                },
                registerInfo : function() {
                    this.$emit('change-register-ivs-info', true);
                },
                virtualAccount : function(){
                    this.$emit('change-virtual-account', true);
                }
            },
            components :{
                dropifyInput : require('../common/dropify-input.js').default.component()
            }
        }
    }
}

export default new MypageSettingMyMember1()