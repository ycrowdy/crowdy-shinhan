class MypageFundedProject {
    component() {
        return {
            template : `
            <div class="container">
                <div class="row not-space">
                    <div class="col-md-2">
                        <div class="st-menu webfont2">
                            <ul>
                                <li class="w30" :class="{'active' : type == 1}"><a href="javascript:void(0)" v-on:click="subMenuChange(1)">리워드</a></li>
                                <li class="w30" :class="{'active' : type == 2}"><a href="javascript:void(0)" v-on:click="subMenuChange(2)">투자</a></li>
                                <li class="w40" :class="{'active' : type == 3}"><a href="javascript:void(0)" v-on:click="subMenuChange(3)">모의펀딩</a></li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <div class="col-md-10 col-line">
                        <div class="row not-space">
                            <div class="col-md-1"></div>
                            <div class="col-md-11">
                                <div class="common_sub_title webfont2">
                                    펀딩한 프로젝트를 <br />
                                    확인해보세요
                                </div>
                                <reward v-if="type == 1"></reward>
                                <invest v-if="type == 2"></invest>
                                <simulation v-if="type == 3"></simulation>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            `,
            props : ['subMenu'],
            data : function() {
                return {
                    fundedList : [],
                    pageCount : 0,
                    forcePage : 0,
                    search : {
                        memCode : this.code,
                        paging : {
			                page : "1",
			                count : "12"
			            }
                    },
                    type : this.subMenu,
                }
            },
            created : function() {
            },
            components : {
                paginate : VuejsPaginate,
                reward : require('./mypage-funded-reward.js').default.component(),
                invest : require('./mypage-funded-invest.js').default.component(),
                simulation : require('./mypage-funded-simulation.js').default.component(),
            },
            computed : {
                
            },
            methods : {
                subMenuChange : function(menu) {
                    this.type = menu;
                }
            }
        }
    }
}

export default new MypageFundedProject()