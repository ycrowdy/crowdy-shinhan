class MypageQuestion {
    component() {
        return {
            template : `
            <div>
                <div class="panel-group faq" id="faq">
                    <!-- Loop -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a class="panel-title accordion-toggle" data-toggle="collapse" data-parent="#faq" href="#collapse1010">
                                <div class="faq_st">2017.08.16 / 답변 완료</div>
                                프로젝트 등록시에 필요한 서류들은 무엇 무엇이 있을까요?
                            </a>
                        </div>
                        <div id="collapse1010" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="faq_a">
                                    <div class="date">2017.08.17</div>
                                    어떤 홈페이지인가?는 유지를 어떻게 하는가에 결정됩니다. 안전한 개인정보, 편한 유지보수와 확장성을 가진 전자전부 표준프레임워크의 기능을 그대로 구현한 표준의 표준!! 홈스토리를 제안합니다.
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- //Loop -->

                    <!-- Loop -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a class="panel-title accordion-toggle collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse1009">
                                <div class="faq_st">2017.08.16 / 답변 대기중</div>
                                프로젝트 등록시에 필요한 서류들은 무엇 무엇이 있을까요?
                            </a>
                        </div>
                        <div id="collapse1009" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="faq_a">
                                    답변 대기중입니다.
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- //Loop -->

                    <!-- Loop -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a class="panel-title accordion-toggle collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse1008">
                                <div class="faq_st">2017.08.16 / 답변 대기중</div>
                                프로젝트 등록시에 필요한 서류들은 무엇 무엇이 있을까요?
                            </a>
                        </div>
                        <div id="collapse1008" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="faq_a">
                                    답변 대기중입니다.
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- //Loop -->

                    <!-- Loop -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a class="panel-title accordion-toggle collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse1007">
                                <div class="faq_st">2017.08.16 / 답변 대기중</div>
                                프로젝트 등록시에 필요한 서류들은 무엇 무엇이 있을까요?
                            </a>
                        </div>
                        <div id="collapse1007" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="faq_a">
                                    답변 대기중입니다.
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- //Loop -->
                </div>
            </div>
            `,
            data : function() {
                return {
                    makedList : [],
                    pageCount : 0,
                    forcePage : 0,
                    search : {
                        memCode : this.code,
                        paging : {
			                page : "1",
			                count : "12"
			            }
                    },
                    type : 1,
                }
            },
            created : function() {
                this.load();
            },
            components : {
                paginate : VuejsPaginate,
            },
            computed : {
                
            },
            methods : {
                load : function() {
                    var self = this;
                },
              
            }
        }
    }
}

export default new MypageQuestion()