class MypageMakedProject {
    component() {
        return {
            template : `
            <div class="container">
                <div class="row not-space">
                    <div class="col-md-2">
                        <div class="st-menu webfont2">
                            <ul>
                                <li v-bind:class="classObject1"><a href="javascript:void(0)" v-on:click="subMenuChange(1)">리워드</a></li>
                                <li v-bind:class="classObject2" v-if="investor"><a href="javascript:void(0)" v-on:click="subMenuChange(2)">투자</a></li>
                                <li v-bind:class="classObject3"><a href="javascript:void(0)" v-on:click="subMenuChange(3)">모의펀딩</a></li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <div class="col-md-10 col-line">
                        <div class="row not-space">
                            <div class="col-md-1"></div>
                            <div class="col-md-11">
                                <div class="common_sub_title webfont2">
                                제작한 프로젝트를 <br />
                                확인해보세요
                                </div>
                                <reward v-if="type == 1"></reward>
                                <invest v-if="type == 2"></invest>
                                <simulation v-if="type == 3"></simulation>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            `,
            props : ['subMenu'],
            data : function() {
                return {
                    makedList : [],
                    pageCount : 0,
                    forcePage : 0,
                    search : {
                        memCode : this.code,
                        paging : {
			                page : "1",
			                count : "12"
			            }
                    },
                    type : this.subMenu,
                    investor : false
                }
            },
            created : function() {
                this.load();
                if(!this.investor) {
                    this.type = 1;
                }
            },
            computed: {
                classObject1 : function () {
                    return {
                        w30 : this.investor,
                        w50 : !this.investor,
                        'active' : this.type == 1,
                    }
                },
                classObject2 : function () {
                    return {
                        w30 : this.investor,
                        'active' : this.type == 2,
                    }
                },
                classObject3 : function () {
                    return {
                        w40 : this.investor,
                        w50 : !this.investor,
                        'active' : this.type == 3,
                    }
                }
            },
            components : {
                paginate : VuejsPaginate,
                reward : require('./mypage-maked-reward.js').default.component(),
                invest : require('./mypage-maked-invest.js').default.component(),
                simulation : require('./mypage-maked-simulation.js').default.component(),
            },
            methods : {
                load : function() {
                    if(userInfo.type != 2) return;
                    var self = this;

                    axios.post('/data/member/investor/state', {memCode : userInfo.memCode})
                        .then(function(response){
                            if(response.data.rData.investor == 'Y') {
                                self.investor = true;
                            }
                        })

                },
                subMenuChange : function(menu) {
                    this.type = menu;
                }
            }
        }
    }
}

export default new MypageMakedProject()