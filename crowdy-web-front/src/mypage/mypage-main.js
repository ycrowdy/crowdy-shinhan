class MypageMain {
    component() {
        return {
            template : `
            <div>
                <div id="list_wrap" class="common_vi_wrap common_vi_wrap6">
                    <div class="common_mypage_title webfont2 hidden-xs">
                        마이페이지
                    </div>

                    <div class="webfont2 visible-xs-block pl20 pr20">
                        <div class="common_mypage_title_xs">마이페이지</div>
                        <div class="common_block_box"></div>
                        <div style="margin: -86px 0px 30px 0px;text-align:center;">
                            <div class="font18"><span class="blue-800">{{userName}}</span>님</div>
                            <div class="font14">{{userEmail}}</div>
                        </div>
                    </div>
                </div>

                <div class="common_sub_layout" >
                    <div id="list_category" class="common_sub_top_menu_1">
                        <div class="common_sub_top_menu webfont2">
                            <div class="container">
                                
                                <a href="javascript:void(0)" :class="{'active' : menuType == 1}" v-on:click="menuChange(1)">
                                    <span class="hidden-xs">펀딩한 프로젝트</span>
                                    <span class="visible-xs-block">펀딩</span>
                                </a>
                                <a href="javascript:void(0)" :class="{'active' : menuType == 2}" v-on:click="menuChange(2)">
                                    <span class="hidden-xs">제작한 프로젝트</span>
                                    <span class="visible-xs-block">제작</span>
                                </a>
                                <a href="javascript:void(0)" :class="{'active' : menuType == 3}" v-on:click="menuChange(3)">
                                    <span class="hidden-xs">관심 프로젝트</span>
                                    <span class="visible-xs-block">관심</span>
                                </a>
                                <!-- <a href="javascript:void(0)" :class="{'active' : menuType == 4}" v-on:click="menuChange(4)"><span>알림 &amp; 문의내역</span></a> -->
                                <a href="javascript:void(0)" :class="{'active' : menuType == 5}" v-on:click="menuChange(5)">
                                    <span>설정</span>
                                </a>
                                
                            </div>
                        </div>
                    </div>
                    
                    <funded v-if="menuType == 1 && dataConfirm" :sub-menu="subMenu"></funded>
                    <maked v-if="menuType == 2 && dataConfirm" :sub-menu="subMenu"></maked>
                    <liked v-if="menuType == 3 && dataConfirm" :sub-menu="subMenu"></liked>
                    <!-- <alarm v-if="menuType == 4 && dataConfirm" :sub-menu="subMenu"></alarm> -->
                    <setting v-if="menuType == 5 && dataConfirm" :naver-oauth="naverOauth" :sub-menu="subMenu"></setting>
                </div>
            </div>
            `,
            props : ['myMenu', 'subMenu'],
            data : function() {
                return {
                    menuType : this.myMenu,
                    userName : userInfo.name,
                    userEmail: userInfo.email,
                    menuOptions : [
                        {
                            id : "1",
                            text : "펀딩한 프로젝트"
                        },
                        {
                            id : "2",
                            text : "제작한 프로젝트"
                        },
                        {
                            id : "3",
                            text : "관심 프로젝트"
                        },
                        // {
                        //     id : "4",
                        //     text : "알림 & 문의내역"
                        // },
                        {
                            id : "5",
                            text : "설정"
                        }
                    ],
                    dataConfirm : false,
                    naverOauth : false,
                }
            },
            created : function() {
                if(!userInfo.loginConfirm()) return;
                this.dataConfirm = true;
               this.checkNaverOauth();
            },
            components : { 
                menuSelect : common.select.component(),
                funded : require('./mypage-funded-project.js').default.component(),
                maked : require('./mypage-maked-project.js').default.component(),
                liked : require('./mypage-liked-project.js').default.component(),
                // alarm : require('./mypage-alarm-question.js').default.component(),
                setting : require('./mypage-setting.js').default.component(),
            },
            methods : {
                menuChange : function(menu) {
                    if (menu == '5') {
                        window.open('/mypage/main?menu=5', '_self');
                    } else {
                    this.menuType = menu;
                    }
                },
                // NAVER-2 : NAVER OAUTH 확인
                checkNaverOauth : function() {
                    if (naverLogin.oauthParams.access_token != undefined) {
                       this.menuType = 5;
                       this.naverOauth = true;
                }
            },
            },
        }
    }
}

export default new MypageMain()