class MypageSettingMyAccount3 {
    component() {
        return {
            template : `
            <div class="row not-space mt60 mb50">
                <div class="col-sm-3 text-center">
                    <div class="setting-radius-wrap xs-mb20">
                        <div class="dropify-wrapper-110">
                            <dropify-input v-model="request.memShotImgFile" default-message="" :default-img="userImage"></dropify-input>
                        </div>
                        {{request.memName}}
                    </div>
                </div>
                <div class="col-sm-9">
                    <form class="form-horizontal" id="memberForm" method="get" action="#">
                        <!-- 조합명 -->
                        <div class="form-group row-mobile-n">
                            <label for="companyName" class="col-sm-3 col-md-2 control-label">
                                <div class="text-left">
                                    조합명
                                </div>
                            </label>
                            <div class="col-sm-8 col-md-7">
                                <div class="row row-mobile-n">
                                    <div class="col-xs-7 col-lg-8">
                                        <input type="text" class="form-control" id="companyName" maxlength="20" title="조합명을 입력하세요." placeholder="조합명 입력" />
                                    </div>
                                    <div class="col-xs-5 col-lg-4">
                                        <button type="button" class="btn btn-block btn-primary-outline">조합명 변경 저장</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- //조합명 -->

                        <!-- 조합고유번호 -->
                        <div class="form-group row-mobile-n">
                            <label for="companyNumber" class="col-sm-3 col-md-2 control-label">
                                <div class="text-left">
                                    조합고유번호
                                </div>
                            </label>
                            <div class="col-sm-6 col-md-5">
                                <input type="text" class="form-control" id="companyNumber" maxlength="10" title="조합 고유번호를 입력해 주세요." placeholder="" readonly="readonly" />
                            </div>
                        </div>
                        <!-- //조합고유번호 -->

                        <!-- 담당자명 -->
                        <div class="form-group row-mobile-n">
                            <label for="managerName" class="col-sm-3 col-md-2 control-label">
                                <div class="text-left">
                                    담당자명
                                </div>
                            </label>
                            <div class="col-sm-6 col-md-5">
                                <div class="row row-mobile-n">
                                    <div class="col-xs-8 col-sm-7 col-lg-8">
                                        <input type="text" class="form-control" id="managerName" maxlength="20" title="담당자명을 입력하세요." placeholder="담당자명을 입력하세요." />
                                    </div>
                                    <div class="col-xs-4 col-sm-5 col-lg-4">
                                        <button type="button" class="btn btn-block btn-primary-outline">본인인증</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- //담당자명 -->

                        <!-- 이메일 -->
                        <div class="form-group row-mobile-n">
                            <label for="member_email" class="col-sm-3 col-md-2 control-label">
                                <div class="text-left">
                                    이메일
                                </div>
                            </label>
                            <div class="col-sm-6 col-md-5">
                                <input type="email" class="form-control" id="member_email" maxlength="100" title="이메일 양식을 확인 바랍니다." placeholder="" />
                            </div>
                        </div>
                        <!-- //이메일 -->

                        <!-- 전화번호 -->
                        <div class="form-group row-mobile-n">
                            <label for="member_mobile" class="col-sm-3 col-md-2 control-label">
                                <div class="text-left">
                                    전화번호
                                </div>
                            </label>
                            <div class="col-sm-6 col-md-5">
                                <input type="tel" class="form-control" v-model="request.mobileNo" placeholder="" disabled/>
                            </div>
                        </div>
                        <!-- //전화번호 -->

                        <!-- 우편번호 -->
                        <div class="form-group row-mobile-n">
                            <label for="zip_code" class="col-sm-3 col-md-2 control-label">
                                <div class="text-left">
                                    우편번호
                                </div>
                            </label>
                            <div class="col-sm-6 col-md-5">
                                <div class="input-group input-group-file">
                                    <input type="tel" class="form-control" v-on:click="postOpen" v-model="postNum" name="zip_code" id="zip_code" placeholder="" readonly="readonly" />
                                    <span class="input-group-btn" v-on:click="postOpen">
                                        <span class="btn btn-outline btn-file">
                                            <i class="fa fa-upload" aria-hidden="true"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <!-- //우편번호 -->

                        <!-- 주소 -->
                        <div class="form-group row-mobile-n">
                            <label for="address1" class="col-sm-3 col-md-2 control-label">
                                <div class="text-left">
                                    주소
                                </div>
                            </label>
                            <div class="col-sm-9 col-md-10 mb10">
                                <input type="text" class="form-control" v-on:click="postOpen" v-model="address1" name="address1" id="address1" placeholder="주소를 검색해주세요." readonly="readonly" />
                            </div>
                            <div class="col-sm-3 col-md-2"></div>
                            <div class="col-sm-9 col-md-10 mb10">
                                <input type="text" class="form-control" v-model="request.memSpsAddr2" name="address2" id="address2" placeholder="상세 주소를 입력해주세요." />
                            </div>
                            <div class="col-sm-9"></div>
                            <div class="col-sm-3 mb15">
                                <a class="btn btn-block btn-primary-outline" v-on:click="changeAccountInfo()">설정 저장</a>
                            </div>
                        </div>
                        <!-- //주소 -->
                    </form>

                    <form class="form-horizontal" id="passwordForm" method="get" action="#">
                        <!-- 비밀번호 변경 -->
                        <hr class="big" />
                        <div class="form-group row-mobile-n">
                            <div class="col-xs-12 control-label control-label-big">
                                <div class="text-left mb20">비밀번호 변경</div>
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group row-mobile-n">
                                    <label for="current_password" class="col-sm-3 col-md-2 control-label">
                                        <div class="text-left">현재 비밀번호</div>
                                    </label>
                                    <div class="col-sm-9 col-md-7">
                                        <input type="password" class="form-control" v-model="request.memPwd" placeholder="" />
                                    </div>
                                </div>

                                <div class="form-group row-mobile-n">
                                    <label for="new_password" class="col-sm-3 col-md-2 control-label">
                                        <div class="text-left">새 비밀번호</div>
                                    </label>
                                    <div class="col-sm-9 col-md-7">
                                        <input type="password" class="form-control" v-model="request.newMemPwd" title="8자 이상의 영문 대소문자,  숫자 특수문자를 사용하세요." placeholder="" />
                                    </div>
                                </div>

                                <div class="form-group row-mobile-n">
                                    <label for="new_password2" class="col-sm-3 col-md-2 control-label">
                                        <div class="text-left">비밀번호 확인</div>
                                    </label>
                                    <div class="col-sm-9 col-md-7">
                                        <input type="password" class="form-control" v-model="request.newMemPwdConfirm" placeholder="" />
                                    </div>
                                    <div class="col-xs-7 col-sm-8 hidden-md col-lg-1"></div>
                                    <div class="col-xs-5 col-sm-4 col-md-3 col-lg-3 text-right">
                                        <a class="btn btn-block btn-primary-outline m-mt15" v-on:click="changePassword()">비밀번호 변경</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- //비밀번호 변경 -->
                    </form>

                    <form class="form-horizontal" id="withdrawalForm" method="get" action="#">
                        <!-- 회원탈퇴 -->
                        <hr class="big" />
                        <div class="form-group row-mobile-n">
                            <div class="col-xs-12 control-label control-label-big">
                                <div class="text-left mb15">회원탈퇴</div>
                            </div>
                            <div class="col-xs-12" style="line-height:22px;">
                                회원탈퇴시 관련정보는 모두 5년간 분리보관되며, 5년후 영구적으로 삭제됩니다.<br />
                                <div class="red-800 mt15">
                                    유의사항
                                    <ul class="con-icon con-icon-small">
                                        <li>현재 프로젝트를 진행 중이거나, 프로젝트를 등록하여 진행한 이력이 있는 회원님은 리워드 프로젝트 종료 후 1년 그리고 투자 프로젝트 종료 후 3년 이내에는 회원 탈퇴가 불가합니다.</li>
                                        <li>현재 프로젝트를 펀딩 중인 회원의 경우도 펀딩 종료 후 1개월 이내에는 회원을 탈퇴할 수 없습니다.</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-xs-7 col-sm-8 col-md-9"></div>
                            <div class="col-xs-5 col-sm-4 col-md-3">
                                <button type="button" class="btn btn-block btn-default-outline mt20 xs-mt10">회원탈퇴 하기</button>
                            </div>
                        </div>
                        <!-- //회원탈퇴 -->
                    </form>
                </div>
            </div>
            `,
            data : function() {
                return {
                    dataConfirm : false,
                    request : {
                        bizNum : "",
                        memCode : userInfo.memCode,
                        memType : userInfo.type,
                        memName : userInfo.name,
                        memEmail : userInfo.email,
                        memPwd :"",
                        newMemPwd : "",
                        newMemPwdConfirm : "",
                        memSpsAddr1 : "",
                        memSpsAddr2 : "",
                        memSpsPostNum : "",
                        resName : "",
                        memShotImg : userInfo.image,
                        memShotImgFile: {},
                        mobileNo : "",
                        authChange : false
                    },
                    address : {
                        postNum : '',
                        address1 : ''
                    }
                }
            },
            created : function() {
                this.load();
                window.accountSetting = this;
                
            }, 
            mounted: function() {
                
                window.accountSetting = this;
                $(".switch-hb").bootstrapSwitch();
            },
            computed : {
                userType : function() {
                    return userInfo.type
                },
                userTypeLabel : function() {
                    if(userInfo.type == 2) {
                        return "법인명"
                    } else if(userInfo.type == 3) {
                        return "조합명"
                    }
                },
                userTypeNumberLabel : function() {
                    if(userInfo.type == 2) {
                        return "사업자등록번호"
                    } else if(userInfo.type == 3) {
                        return "조합고유번호"
                    }
                },
                postNum : function() {
                    this.request.memSpsPostNum = this.address.postNum
                    return this.address.postNum
                },
                address1 : function() {
                    this.request.memSpsAddr1 = this.address.address1
                    return this.address.address1
                },
                userName : function() {
                    return userInfo.name
                },
                userEmail : function() {
                    return userInfo.email
                },
                userImage : function() {
                    return userInfo.image
                }
            },
            methods : {
                load : function() {
                    var self = this;
                    
                    axios.post('/data/member/info', this.request)
                        .then(function(response){
                            self.request.bizNum = response.data.rData.bizNum;
                            self.request.mobileNo = response.data.rData.mobileNo;
                            self.request.memSpsAddr1 = response.data.rData.memSpsAddr1;
                            self.request.memSpsAddr2 = response.data.rData.memSpsAddr2;
                            self.request.memSpsPostNum = response.data.rData.memSpsPostNum;
                            self.request.resName = response.data.rData.resName;
                            self.address.postNum =  response.data.rData.memSpsPostNum;
                            self.address.address1 = response.data.rData.memSpsAddr1;

                            self.dataConfirm = !self.dataConfirm;
                        })
                },
                postOpen : function() {
                    post.open(this);
                },
                changeAccountInfo : function() {
                    var self = this;
                    $('.page-loader-more').fadeIn('')
                    axios.post('/member/update', this.request)
                        .then(function(response){
                            $('.page-loader-more').fadeOut('');
                            var result = response.data;
                            if(result.rCode == "0000") {
                                userInfo.updateInfo(response.data.rData);
                                self.request.memName = userInfo.name;
                                self.request.memShotImg  = userInfo.image;
                                self.request.authChange = false;
                                noti.open("저장되었습니다.")
                            } else {
                                noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
                            }
                        })
                },
                changePassword : function() {
                    var self = this;
                    if(self.request.newMemPwd == self.request.newMemPwdConfirm) {
                        $('.page-loader-more').fadeIn('')
                        axios.post('/data/member/password-update', this.request)
                            .then(function(response){
                                $('.page-loader-more').fadeOut('');
                                var result = response.data;
                                if(result.rCode == "0000") {
                                    noti.open("변경되었습니다.")
                                } else if(result.rCode == "2001") {
                                    noti.open("현재 비밀번호를 잘못 입력하셨습니다.")
                                } else {
                                    noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
                                }
                            })
                    } else {
                        noti.open("새 비밀번호와 비밀번호 확인이 일치하지 않습니다.")
                    }

                },
                auth : function() {
                    this.request.authChange = false;
                    axios.post('/auth/nice/M')
                        .then(function(response){
                            // $('#encodeData').val(response.data.rData.data)
                            document.getElementById('encodeData').value = response.data.rData.data;
                            window.open('', 'popupChk', 'width=500, height=800, top=100, left=100, fullscreen=no, menubar=no, status=no, toolbar=no, titlebar=yes, location=no, scrollbar=no');
                            document.form_chk.action = "https://nice.checkplus.co.kr/CheckPlusSafeModel/checkplus.cb";
                            document.form_chk.target = "popupChk";
                            document.form_chk.submit();
                        });
                },
                authResult : function(result) {
                    if(result) {
                        var self = this;
                        axios.post('/auth/info')
                            .then(function(response) {
                                self.request.resName = response.data.name;
                                self.request.mobileNo = response.data.mobileNo;
                                self.request.authChange = true;
                                noti.open("본인인증이 완료되었습니다.");
                            });
                    } else {
                        noti.open('본인인증에 실패했습니다 <br/> 다시시도해주세요.')
                    }
                }
            },
            components :{
                dropifyInput : require('../common/dropify-input.js').default.component()
            }
        }
    }
}

export default new MypageSettingMyAccount3()