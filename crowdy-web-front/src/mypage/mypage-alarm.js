class MypageAlarm {
    component() {
        return {
            template : `
            <div>
                <div class="pn_list_wrap">
                    <!-- Loop 읽지 않음 클래스 (not_read) -->
                    <a href="javascript:void(0)" class="pn_list_loop not_read">
                        <div class="media">
                            <div class="media-left media-middle">
                                <img src="./data/ex001.jpg" class="media-object" alt="..." />
                            </div>
                            <div class="media-body media-middle">
                                <div class="pn_list_frame">
                                    <div class="pn_summury">리워드</div>
                                    <div class="pn_type">프로젝트 묘미</div>
                                    <div class="pn_subject">요청한 프로젝트가 반려되었습니다. 반려사유는 등록한 메일로 보냈습</div>
                                    <div class="pn_time">42분 전</div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <!-- //Loop -->

                    <!-- Loop -->
                    <a href="javascript:void(0)" class="pn_list_loop">
                        <div class="media">
                            <div class="media-left media-middle">
                                <img src="./data/ex001.jpg" class="media-object" alt="..." />
                            </div>
                            <div class="media-body media-middle">
                                <div class="pn_list_frame">
                                    <div class="pn_summury">리워드</div>
                                    <div class="pn_type">연세대학교 창업 지원단</div>
                                    <div class="pn_subject">연세대학교 창업지원단은 학생들의 창의적이고 우수한 아이디어를 발굴</div>
                                    <div class="pn_time">1일 전</div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <!-- //Loop -->

                    <!-- Loop -->
                    <a href="javascript:void(0)" class="pn_list_loop">
                        <div class="media">
                            <div class="media-left media-middle">
                                <img src="./data/ex002.jpg" class="media-object" alt="..." />
                            </div>
                            <div class="media-body media-middle">
                                <div class="pn_list_frame">
                                    <div class="pn_summury red-800">모의크라우드펀딩</div>
                                    <div class="pn_type">프로젝트 묘미</div>
                                    <div class="pn_subject">펀딩한 프로젝트가 반려되었습니다. 반려사유는 등록한 메일로 보냈습</div>
                                    <div class="pn_time">42분 전</div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <!-- //Loop -->

                    <!-- Loop -->
                    <a href="javascript:void(0)" class="pn_list_loop">
                        <div class="media">
                            <div class="media-left media-middle">
                                <img src="./data/ex003.jpg" class="media-object" alt="..." />
                            </div>
                            <div class="media-body media-middle">
                                <div class="pn_list_frame">
                                    <div class="pn_summury red-800">모의크라우드펀딩</div>
                                    <div class="pn_type">프로젝트 묘미</div>
                                    <div class="pn_subject">펀딩한 프로젝트가 반려되었습니다. 반려사유는 등록한 메일로 보냈습</div>
                                    <div class="pn_time">42분 전</div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <!-- //Loop -->
                </div>
            </div>
            `,
            data : function() {
                return {
                    makedList : [],
                    pageCount : 0,
                    forcePage : 0,
                    search : {
                        memCode : this.code,
                        paging : {
			                page : "1",
			                count : "12"
			            }
                    },
                    type : 1,
                }
            },
            created : function() {
                this.load();
            },
            components : {
                paginate : VuejsPaginate,
                reward : require('./mypage-maked-reward.js').default.component(),
                invest : require('./mypage-maked-invest.js').default.component(),
                simulation : require('./mypage-maked-simulation.js').default.component(),
            },
            computed : {
                
            },
            methods : {
                load : function() {
                    var self = this;
                },
              
            }
        }
    }
}

export default new MypageAlarm()