class MypageFundedSimulation {
    component() {
        return {
            template : `
            <div>
                <!-- Loop 결제완료 -->
                <div class="mif-list" v-for="item in fundedList">
                    <div class="mif-frame" v-on:click="detail(item.cpAliasUrl)" style="cursor: pointer;">
                        <div class="media">
                            <div class="media-left media-middle">
                                <img :src="'//' + item.cpCardImg + '/ycrowdy/resize/!340x!226'" class="media-object" />
                            </div>
                            <div class="media-body media-middle">
                                <div class="pn_list_frame">
                                    <div class="pn_summury" :class="{'red-800' : item.spsStatus == 2}">{{ statusCheck(item.spsStatus, item.spsDeliveryStatus) }}</div>
                                    <div class="pn_subject">{{ item.cpTitle }}</div>
                                    <div class="pn_time">
                                        <span>{{ item.projectAchievement }}%</span>
                                        <span>{{ item.paymentDate }} 결제일</span>
                                        <span class="xs-mt5">{{ item.fundWdate }} 펀딩참여일</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- 선택한 리워드 -->
                    <div class="row not-space row-label">
                        <div class="col-xs-3 col-sm-2">
                            <span>선택한 리워드</span>
                        </div>
                        <div class="col-xs-9 col-sm-10 text-label" v-if="item.cbfTitle != null ">
                            <span>{{ item.cbfTitle }}, {{ item.cbfInfo }} X {{ item.sbfQty }}</span>
                        </div>
                    </div>
                    <!-- //선택한 리워드 -->

                    <!-- 받는분/연락처 -->
                    <!--<div class="row not-space row-label">
                        <div class="col-xs-3 col-sm-2">
                            <span>받는분/연락처</span>
                        </div>
                        <div class="col-xs-9 col-sm-10 text-label">
                            <span> {{ item.spsRsName }} <small class="ml10"> {{ item.spsRsMobileNo }} </small></span>
                        </div>
                    </div>-->
                    <!-- //받는분/연락처 -->

                    <!-- 배송지 -->
                    <!--<div class="row not-space row-label">
                        <div class="col-xs-3 col-sm-2">
                            <span>배송지</span>
                        </div>
                        <div class="col-xs-9 col-sm-8 text-label">
                            <span>
                                {{ item.spsAddr1 }} {{ item.spsAddr2 }}
                            </span>
                        </div>
                    </div>-->
                    <!-- //배송지 -->

                    <!-- 결제수단 -->
                    <!--<div class="row not-space row-label">
                        <div class="col-xs-3 col-sm-2">
                            <span>결제수단</span>
                        </div>
                        <div class="col-xs-9 col-sm-8 text-label">
                            <span>{{ payType(item.payDiv) }}</span>
                        </div>
                    </div>-->
                    <!-- //결제수단 -->

                    <!-- 총 결제금액 -->
                    <div class="row not-space row-label">
                        <div class="col-xs-3 col-sm-2">
                            <span>총 결제금액</span>
                        </div>
                        <div class="col-xs-9 col-sm-8 text-label">
                            <span><strong class="blue-800">{{ parseInt(item.spsTotAmount).toLocaleString() }} 원</strong> <br class="hidden-sm hidden-md hidden-lg" />
                            <em>(펀딩금액 {{ parseInt((item.spsTotAmount) - (item.spsAmount)).toLocaleString() }} 원 + 추가펀딩 {{ parseInt(item.spsAmount).toLocaleString() }}원)</em></span>
                        </div>
                    </div>
                    <!-- //총 결제금액 -->
                </div>
                <!-- //Loop 결제완료 -->

                <!-- 페이징 -->
                <nav class="text-center mt30 mb20" >
                    <paginate
                        :page-count="pageCount"
                        :class="'pagination'"
                        :click-handler="nextPage"
                        :force-page="forcePage"
                        >
                    </paginate>
                </nav>
                <!-- //페이징 -->

                <!-- 결제완료 : 배송지 변경 불가능 -->
                <div id="deliveryNModal" class="modal fade" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body modal-order">
                                프로젝트가 종료되어 배송지 변경은 <br />
                                제작자에게 직접 연락을 통해서만 가능합니다.
                                <div class="mt15 blue-800">(제작자 연락처 : 010.3544.2241)</div>
        
                                <div class="modal-footer text-center">
                                    <div class="row row-mobile-n">
                                        <div class="col-xs-4 col-xs-offset-4">
                                            <button type="button" class="btn btn-block btn-primary-outline" data-dismiss="modal">확인</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //결제완료 : 배송지 변경 불가능 -->
            </div>
            `,
            props : ['code'],
            data : function() {
                return {
                    fundedList : [],
                    pageCount : 0,
                    forcePage : 0,
                    search : {
                        memCode : userInfo.memCode,
                        paging : {
			                page : "1",
			                count : "4"
                        },
                        cpTrainingYn : 'Y',
                    },
                }
            },
            created : function() {
                this.load();
            },
            components : {
                paginate : VuejsPaginate
            },
            methods : {
                load : function() {
                    var self = this;
                    axios.post('/data/mypage/funded/reward', self.search)
                        .then(function(response){
                            var remainder = parseInt(response.data.rData.count % self.search.paging.count);
                            var remainderCount = 0;
                            if(remainder > 0) {
                                remainderCount++;
                            }
                            self.pageCount = (parseInt(response.data.rData.count / self.search.paging.count)) + remainderCount;
                            self.fundedList = response.data.rData.data;
                        })
                },
                nextPage : function(page) {
                    this.forcePage = page - 1;
                    this.search.paging.page = page;
                    this.load();
                },
                statusCheck : function(cpStatus, dlvStatus) {
                    var result = "";
                    //후원상태(공통코드)0:결제대기,1:결제완료,2:결제실패,3:결제취소,9:후원취소
                    //배송상태(배송없음:0 배송대기:1  배송중:2 배송완료:3 취소:4)-공통코드
                    if(cpStatus == "9") {
                        result = "후원취소";
                    } else {
                        result = "결제완료";
                    }
                    return result;
                },
                payType : function(val) {
                    //결제구분(카드:1, 계좌이체:2, 쿠폰:3)
                    var result = "";

                    if(val == "1"){
                        result = "카드";
                    } else if(val == "2") {
                        result = "계좌이체";
                    } else if(val == "3") {
                        result = "쿠폰";
                    }
                    return result;
                },
                detail : function(url) {
                    window.open("/sr/" + url, '_self');
                },
            }
        }
    }
}

export default new MypageFundedSimulation()