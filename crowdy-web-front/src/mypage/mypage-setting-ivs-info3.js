class MypageSettingMyMember3 {
    component() {
        return {
            template : `
            <div class="mt60 xs-mt30 mb80 mypage-myaccount-layout">
                <!-- 사진영역-->
                <div class="dropify-wrapper-80 mypage-myaccount-photo">
                    <dropify-input accept="image/*" v-model="request.memShotImgFile" default-message="" :default-img="userImage"></dropify-input>
                </div>

                <!-- 컨텐츠 영역 -->
                <div class="mypage-form xs-mt30">

                    <div class="mb20 mypage-title">투자 회원 {{registerTitle}}하기 (법인회원)</div>

                    <!-- 법인명 -->
                    <div class="mypage-rabel mb5">법인명</div>
                    <input type="text" v-model="request.memName" class="form-control" maxlength="20" placeholder="" readonly="readonly" />
                    <!-- //법인명 -->

                    <!-- 사업자등록번호 -->
                    <div class="mypage-rabel mb5">사업자등록번호</div>
                    <input type="text" v-model="request.memIvsComNo" class="form-control" maxlength="10" placeholder="" readonly="readonly" />
                    <!-- //사업자등록번호 -->

                    <!-- 법인등록번호 -->
                    <div class="mypage-rabel mb5">법인등록번호</div>
                    <number-input type="tel" class="form-control" placeholder="" maxlength="13" :num="request.memIvsCorNo" v-model="request.memIvsCorNo" :disabled="investor == 'Y'"/></number-input>
                    <!-- //법인등록번호 -->

                    <!-- 대표자명 -->
                    <div class="mypage-rabel mb5">대표자명</div>
                    <input type="text" v-model="request.memIvsCeo" class="form-control" maxlength="20" placeholder="" :disabled="investor == 'Y'"/>
                    <!-- //대표자명 -->

                    <!-- 종목 -->
                    <div class="mypage-rabel mb5">종목</div>
                    <input type="text" v-model="request.memIvsStockType" class="form-control" maxlength="20" placeholder="" />
                    <!-- //종목 -->

                    <!-- 전화번호 -->
                    <div class="mypage-rabel mb5">전화번호</div>
                    <number-input type="tel" class="form-control" placeholder="" :num="request.memIvsTellNo" v-model="request.memIvsTellNo"/></number-input>
                    <!-- //전화번호 -->

                    <!-- 우편번호 -->
                    <div for="zip_code" class="mypage-rabel mb5">우편번호</div>
                    <div class="common-flex-end">
                        <input type="tel" class="form-control mypage-zip-layout1" v-on:click="postOpen" v-model="postNum" name="zip_code" id="zip_code" placeholder="" readonly="readonly"/>
                        <div class="mypage-btn-search" v-on:click="postOpen"></div>
                    </div>
                    <!-- //우편번호 -->

                    <!-- 주소 -->
                    <div class="mypage-rabel mb5">주소</div>
                    <input type="text" class="form-control" v-on:click="postOpen" v-model="address1" name="address1" placeholder="주소를 검색해주세요." readonly="readonly" />
                    <input type="text" class="form-control" v-model="request.memIvsAddr2" placeholder="상세 주소를 입력해주세요." />
                    <!-- //주소 -->

                    <!-- 기본정보 수정 -->
                    <div class="mypage-setting-btn mt20" v-on:click="updateBasicInfo()" v-if="investor == 'Y'">설정 저장</div>
                    <!-- //기본정보 수정 -->

                    <!-- 법인 담당자 정보 -->
                    <div class="mb20 mt50 mypage-title">법인 담당자 정보</div>
                    
                    <!-- 본인인증 -->
                    <div class="mypage-rabel mb5">본인인증</div>
                    <div class="mypage-setting-btn" v-on:click="auth()">본인인증받기</div>
                    <!-- //본인인증 -->

                    <!-- 담당자명 -->
                    <div class="mypage-rabel mb5 mt10">담당자명</div>
                    <input type="text" v-model="request.memIvsResName" class="form-control" maxlength="20" placeholder="" disabled/>
                    <!-- //담당자명 -->

                    <!-- 휴대폰번호 -->
                    <div class="mypage-rabel mb5">휴대폰번호</div>
                    <input type="tel" class="form-control" v-model="request.memIvsRegiHpNo" placeholder="" disabled/>
                    <!-- //휴대폰번호 -->

                    <!-- 이메일 -->
                    <div class="mypage-rabel mb5">이메일</div>
                    <input type="email"  v-model="request.memIvsResEmail" class="form-control" maxlength="100" placeholder="" />
                    <!-- //이메일 -->

                    <!-- 기본정보 수정 -->
                    <div class="mypage-setting-btn mt20" v-on:click="updateResInfo()" v-if="investor == 'Y'">설정 저장</div>
                    <!-- //기본정보 수정 -->
                    
                    <!-- //법인 담당자 정보 -->

                    <!-- 회원유형 -->
                    <div class="mypage-title mt50">회원유형</div>
                    
                    <div class="mypage-rabel mb5 mt20">회원유형 선택</div>
                    <div class="option_group option_group_none">
                        <div class="option1_radio">
                            <label for="option1_radio1" class="radio-inline mypage-radio-custom">
                                <input type="radio" name="option1_radio" id="option1_radio1" value="MIT001" v-model="request.memIvsType" :disabled="micType1Disabled"/><span class="webfont">일반투자자</span>
                            </label>
                            <label for="option1_radio2" class="radio-inline mypage-radio-custom">
                                <input type="radio" name="option1_radio" id="option1_radio2" value="MIT002" v-model="request.memIvsType" v-on:click="changeMemIvsType(1)" :disabled="micType2Disabled"/><span class="webfont">적격투자자</span>
                            </label>
                            <label for="option1_radio3" class="radio-inline mypage-radio-custom">
                                <input type="radio" name="option1_radio" id="option1_radio3" value="MIT003" v-model="request.memIvsType" v-on:click="changeMemIvsType(2)" :disabled="micType3Disabled"/><span class="webfont">전문투자자</span>
                            </label>
                        </div>
                    </div>

                    <!-- 일반투자자 -->
                    <div id="option1_contents_radio1" class="otab1_contents" v-if="request.memIvsType == 'MIT001'"></div>
                    <!-- //일반투자자 -->

                    <!-- 소득요건 등 적격투자자 or 전문투자자-->
                    <div id="option1_contents_radio2" v-if="request.memIvsType == 'MIT002' || request.memIvsType == 'MIT003'">
                        <div class="mypage-rabel mt10">증명서류</div>

                        <div class="vod-holder input-append" v-for="(item, index) in request.memIvsFiles" v-if="request.memIvsFiles[index].state == true">
                            <div class="mypage-file-layout input-group input-group-file">
                                <input type="text" class="form-control mb5 mypage-code-width" :class="{'error' : errors.has('memIvsFiles[files]')}" v-model="request.memIvsFiles[index].fileName" placeholder="파일을 업로드해주시기 바랍니다." readonly="readonly" />
                                <span class="input-group-btn common-default-flex"">
                                    <span class="btn btn-outline btn-file mr5">
                                        <i class="fa fa-upload" aria-hidden="true"></i>
                                        <input type="file" name="" multiple="" />
                                        <file-input v-model="request.memIvsFiles[index]" data-vv-name="memIvsFiles" v-validate="'required'" :disabled="memIvsFilesDisabled" v-on:error="error"></file-input>
                                    </span>
                                    <span class="files-add vod-add mr5" v-on:click="fileAdd" v-if="((index + 1 == request.memIvsFiles.length  || index == viewAddButtonCount) && userTypeDocCount != 5) && !memIvsFilesDisabled">+</span>
                                    <span class="files-add img-delete vod-delete" v-on:click="fileDelete(index)" v-if="userTypeDocCount != 1 && !memIvsFilesDisabled">-</span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <!-- //소득요건 등 적격투자자 or 전문투자자 -->

                    <!-- 회원유형 : 투자 설명 -->
                    <div class="mt20">  
                        <div class="mypage-setting-desc mb15 red-800" v-if="request.memIvsState == 'MIC003'">
                            인증이 반려 되었습니다. 반려사유를 살펴보고 다시 제출해주세요. <br/>
                            <a href="javascript:void(0)" class="btn-link grey-600 pl0 pr0 ml10" v-on:click="showRejectedModal(2)">반려사유 보기 <i class="fa fa-angle-right ml5" aria-hidden="true"></i></a>
                        </div>
                        <div class="mypage-setting-desc">
                            투자자 유형에 따라 투자한도가 달라질 수 있습니다. 투자자 유형을 변경하려면, 소득요건 등 적격투자자 또는 전문투자자의 경우에 따라 자격요건에 따른 서류를 제출하신 후 인증을 받으셔야 합니다.<br />
                            <a href="javascript:void(0)" class="blue-800" data-toggle="modal" data-target="#investModal">투자자별 자격 요건 안내 <i class="fa fa-angle-right ml5" aria-hidden="true"></i></a>
                        </div>
                        <div class="mypage-setting-desc mt20">
                            등록을 완료하시면 <a href="/crowdy/term?menu=3" class="blue-800">서비스 이용약관</a> 및 <a href="/crowdy/term?menu=4" class="blue-800">개인정보취급방침</a>에 동의하시는 것으로 간주합니다.
                        </div>
                    </div>

                    <!-- 회원유형 수정 -->
                    <div class="mypage-setting-btn mt20" v-on:click="updateIvsType()" v-if="(investor == 'Y' && request.memIvsState == 'MIC003') || (memIvsOriginalType == 'MIT002' && request.memIvsType == 'MIT003') || memIvsOriginalType == 'MIT001'">설정 저장</div>
                    <!-- //회원유형 수정 -->

                    <!-- 실명확인용 서류 -->
                    <div class="mb20 mt50 mypage-title">실명확인용 서류</div>

                    <!-- 사업자등록증 -->
                    <div class="mypage-rabel mb5">사업자등록증</div>
                    <div class="input-group input-group-file common-default-flex">
                        <input type="text" class="form-control mypage-search-width" v-model="request.memIvsCorFile.fileName" placeholder="" readonly="readonly" />
                        <span class="input-group-btn egnolerValue">
                            <span class="btn btn-outline btn-file">
                                <i class="fa fa-upload" aria-hidden="true"></i>
                                <file-input v-model="request.memIvsCorFile" data-vv-name="memIvsCorFile" v-validate="'required'" :disabled="memIvsRnmDocDisabled" v-on:error="error"></file-input>
                            </span>
                        </span>
                    </div>
                    <!-- //사업자등록증 -->
                        
                    <!-- 법인등기부등본 -->
                    <div class="mypage-rabel mb5">법인등기부등본*</div>
                    <div class="input-group input-group-file common-default-flex">
                        <input type="text" class="form-control mypage-search-width" v-model="request.memIvsRnmCorFile.fileName" placeholder="" readonly="readonly" />
                        <span class="input-group-btn egnolerValue">
                            <span class="btn btn-outline btn-file">
                                <i class="fa fa-upload" aria-hidden="true"></i>
                                <file-input v-model="request.memIvsRnmCorFile" data-vv-name="memIvsRnmCorFile" v-validate="'required'" :disabled="memIvsRnmDocDisabled" v-on:error="error"></file-input>
                            </span>
                        </span>
                    </div>
                    <!-- //법인등기부등본 -->

                    <!-- 법인감증명서 -->
                    <div class="mypage-rabel mb5">법인감증명서*</div>
                    <div class="input-group input-group-file common-default-flex">
                        <input type="text" class="form-control mypage-search-width" v-model="request.memIvsCorSealFile.fileName" placeholder="" readonly="readonly" />
                        <span class="input-group-btn egnolerValue">
                            <span class="btn btn-outline btn-file">
                                <i class="fa fa-upload" aria-hidden="true"></i>
                                <file-input v-model="request.memIvsCorSealFile" data-vv-name="memIvsCorSealFile" v-validate="'required'" :disabled="memIvsRnmDocDisabled" v-on:error="error"></file-input>
                            </span>
                        </span>
                    </div>
                    <!-- //법인감증명서 -->

                    <!-- 실명확인증표 -->
                    <div class="mypage-rabel mb5">실명확인증표*</div>
                    <div class="input-group input-group-file common-default-flex">
                        <input type="text" class="form-control mypage-search-width" v-model="request.memIvsRnmFile.fileName" placeholder="대표자 혹은 대리인의 신분증" readonly="readonly" />
                        <span class="input-group-btn egnolerValue">
                            <span class="btn btn-outline btn-file">
                                <i class="fa fa-upload" aria-hidden="true"></i>
                                <file-input v-model="request.memIvsRnmFile" data-vv-name="memIvsRnmFile" v-validate="'required'" :disabled="memIvsRnmDocDisabled" v-on:error="error"></file-input>
                            </span>
                        </span>
                    </div>
                    <!-- //실명확인증표 -->

                    <!-- 실명확인증표(위임장) -->
                    <div class="input-group input-group-file common-default-flex">
                        <input type="text" class="form-control mypage-search-width" v-model="request.memIvsRnmAttorneyFile.fileName" placeholder="대리인 신분증 첨부시 위임장 첨부" readonly="readonly" />
                        <span class="input-group-btn egnolerValue">
                            <span class="btn btn-outline btn-file">
                                <i class="fa fa-upload" aria-hidden="true"></i>
                                <file-input v-model="request.memIvsRnmAttorneyFile" data-vv-name="memIvsRnmAttorneyFile" v-validate="'required'" :disabled="memIvsRnmDocDisabled" v-on:error="error"></file-input>
                            </span>
                        </span>
                    </div>
                    <a href="https://image-se.ycrowdy.com/file/%E1%84%8F%E1%85%B3%E1%84%85%E1%85%A1%E1%84%8B%E1%85%AE%E1%84%83%E1%85%B5%E1%84%8B%E1%85%B1%E1%84%8B%E1%85%B5%E1%86%B7%E1%84%8C%E1%85%A1%E1%86%BC%E1%84%8B%E1%85%A3%E1%86%BC%E1%84%89%E1%85%B5%E1%86%A8.hwp" class="btn-link grey-600 mypage-setting-link-custom">(위임장 샘플)</a>
                    <!-- //실명확인증표(위임장) -->

                    <!-- 사용인감계(선택) -->
                    <div class="mypage-rabel mb5">사용인감계 <span class="red-800">(선택사항)</span></div>
                    <div class="input-group input-group-file common-default-flex">
                        <input type="text" class="form-control mypage-search-width" v-model="request.memIvsCorAttorneyFile.fileName" placeholder="" readonly="readonly" />
                        <span class="input-group-btn egnolerValue">
                            <span class="btn btn-outline btn-file">
                                <i class="fa fa-upload" aria-hidden="true"></i>
                                <file-input v-model="request.memIvsCorAttorneyFile" data-vv-name="memIvsCorAttorneyFile" v-validate="'required'" :disabled="memIvsRnmDocDisabled" v-on:error="error"></file-input>
                            </span>
                        </span>
                    </div>
                    <a href="https://image-se.ycrowdy.com/file/%E1%84%8F%E1%85%B3%E1%84%85%E1%85%A1%E1%84%8B%E1%85%AE%E1%84%83%E1%85%B5%E1%84%89%E1%85%A1%E1%84%8B%E1%85%AD%E1%86%BC%E1%84%8B%E1%85%B5%E1%86%AB%E1%84%80%E1%85%A1%E1%86%B7%E1%84%80%E1%85%A8(%E1%84%87%E1%85%A5%E1%86%B8%E1%84%8B%E1%85%B5%E1%86%AB)_180102.hwp" class="btn-link grey-600 mypage-setting-link-custom">(사용인감계 샘플)</a>
                    <!-- //사용인감계(선택) -->

                    <div class="mypage-setting-desc mb15 red-800" v-if="request.memIvsRnmDocStatus == 'MIRT04'">
                        인증이 반려 되었습니다. 반려사유를 살펴보고 다시 제출해주세요. <br/>
                        <a href="javascript:void(0)" class="btn-link grey-600 pl0 pr0 ml10" v-on:click="showRejectedModal(1)">반려사유 보기 <i class="fa fa-angle-right ml5" aria-hidden="true"></i></a>
                    </div>
                    <!-- //실명확인용 서류 -->    

                    <!-- 실명확인용 서류 수정 -->
                    <div class="mypage-setting-btn mt20" v-on:click="updateRealNameDoc()" v-if="investor == 'Y' && request.memIvsRnmDocStatus == 'MIRT04'">설정 저장</div>
                    <!-- //실명확인용 서류 수정 -->


                    <!-- 연고자여부 -->
                    <div class="mypage-rabel mb5">연고자여부</div>
                    <div class="option_group option_group_none">
                        <div class="option1_radio">
                            <label for="option1_radio4" class="radio-inline mypage-radio-custom-1">
                                <input type="radio" name="option1_radio_1" id="option1_radio4" value="2" v-model="relConfirm" v-on:click="changeRelStatus" :disabled="relDisabled"/><span class="webfont">연고자 아닙니다</span>
                            </label>
                            <label for="option1_radio5" class="radio-inline mypage-radio-custom-1">
                                <input type="radio" name="option1_radio_1" id="option1_radio5" value="1" v-model="relConfirm" v-on:click="changeRelStatus" :disabled="relDisabled"/><span class="webfont">연고자 맞습니다</span>
                            </label>
                        </div>
                    </div>
                    <div class="mypage-setting-desc mt10">
                        연고자는 투자하고자하는 기업의 기존 관계자, 주주에 해당 할 때만 선택하실 수 있습니다. 특정 기업의 연고자가 되면, 
                        해당 기업에 한해 무제한으로 투자가 가능합니다. 해당 기업의 주주 명부 혹은 재직증명서를 제출하신 후 인증을 받으셔야 합니다.
                    </div>

                    <template v-if="relConfirm == 1">
                        <!-- 연고자 증빙서류 -->
                        <div class="mypage-rabel mb5 mt20">증빙서류</div>
                        <div class="input-group input-group-file common-flex-center">
                            <input type="text" class="form-control mypage-search-width" :class="{'error' : errors.has('memIvsRelFile')}" v-model="request.memIvsRelFile.fileName" placeholder="파일을 업로드해주시기 바랍니다." readonly="readonly"/>
                            <span class="input-group-btn" style="top: -12px;">
                                <span class="btn btn-outline btn-file">
                                    <i class="fa fa-upload" aria-hidden="true"></i>
                                    <file-input v-model="request.memIvsRelFile" data-vv-name="memIvsRelFile" v-validate="'required'" :disabled="relDisabled" v-on:error="error"></file-input>
                                </span>
                            </span>
                            <!--<label class="error" v-if="errors.has('memIvsRelFile')" v-text="errors.first('memIvsRelFile')"></label>-->
                        </div>

                        <div class="mypage-setting-desc mb15 red-800" v-if="request.memIvsRelStatus == 'MIR004'">
                            인증이 반려 되었습니다. 반려사유를 살펴보고 다시 제출해주세요. <br/>
                            <a href="javascript:void(0)" class="btn-link grey-600 pl0 pr0 ml10" v-on:click="showRejectedModal(3)">반려사유 보기 <i class="fa fa-angle-right ml5" aria-hidden="true"></i></a>
                        </div>
                        <!-- //연고자 증빙서류 -->

                        <!-- 사업자등록번호 -->
                        <div class="mypage-rabel mt10 mb5">사업자등록번호</div>
                        <number-input type="tel" class="form-control" :num="request.memIvsRelComNo" v-model="request.memIvsRelComNo" maxlength="10" placeholder="" :disabled="relDisabled"/></number-input>
                        <!-- //사업자등록번호 -->
                    </template>

                    <!-- 연고자 수정 -->
                    <div class="mypage-setting-btn mt20" v-on:click="updateRel()" v-if="(investor == 'Y' && (request.memIvsRelStatus == 'MIR001' || request.memIvsRelStatus == 'MIR004')) || relRegistered == false">설정 저장</div>
                    <!-- // 연고자 수정 -->


                    <!-- 투자 회원 이용약관 동의 -->
                    <div v-if="investor == 'N'" class="mt20">
                        <label class="chk_container mypage-setting-desc">투자회원 <a class="blue-800" href="/crowdy/term?menu=3" target="_blank"><strong>이용약관</strong></a>에 동의합니다.
                            <input type="checkbox" v-model="checkTerms1">
                            <span class="checkmark"></span>
                        </label>
                    </div>

                    <div v-if="investor == 'N'" class="mt10">
                        <label class="chk_container mypage-setting-desc">투자회원 <a class="blue-800" href="/crowdy/term?menu=5" target="_blank"><strong>개인정보 취급방침</strong></a>에 동의합니다.
                            <input type="checkbox" v-model="checkTerms2">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <!-- 투자 회원 이용약관 동의 -->

                    <div class="mt20 mypage-myaccount-layout" v-if="investor == 'N'">
                        <button type="button" class="mypage-setting-btn mypage-setting-btn-cancle">취소</button>
                        <button type="button" class="mypage-setting-btn" v-on:click="applyData()">등록</button>
                    </div>

                    <invest-modal :mode="2"></invest-modal>
                    <rejected-modal :rejectedReason="rejectedContent"></rejected-modal>

                    <form name="form_chk" method="post">
                        <input type="hidden" name="m" value="checkplusSerivce">                     <!-- 필수 데이타로, 누락하시면 안됩니다. -->
                        <input type="hidden" name="EncodeData" id="encodeData" value="">            <!-- 위에서 업체정보를 암호화 한 데이타입니다. -->

                        <input type="hidden" name="param_r1" value="accountSetting.authResult">
                        <input type="hidden" name="param_r2" value="">
                        <input type="hidden" name="param_r3" value="">
                    </form>
                </div>
            </div>
            `,
            data : function() {
                return {
                    dataConfirm : false,
                    request : {
                        memCode : userInfo.memCode,
                        memType : userInfo.type,
                        memName : userInfo.name,
                        memEmail : userInfo.email,
                        memIvsAddr1 : "",
                        memIvsAddr2 : "",
                        memIvsPostNum : "",
                        resName : "",
                        memShotImg : userInfo.image,
                        authChange : false,
                        memIvsComNo : "",
                        memIvsCompanyName : userInfo.name,
                        memIvsCorNo : "",
                        memIvsCeo : "",
                        memIvsStockType : "",
                        memIvsTellNo : "",
                        memIvsResName : "",
                        memIvsResEmail : "",
                        memIvsDoc : "", 
                        memIvsFiles : [{
                            fileData : "",
                            fileName : "",
                            state: true
                        }],
                        memIvsCorDoc : "",
                        memIvsCorFile : {
                            fileData : "",
                            fileName : "",
                            state: true  
                        },
                        memIvsRnmCorDoc : "",
                        memIvsRnmCorFile : {
                            fileData : "",
                            fileName : "",
                            state: true  
                        },
                        memIvsCorSeal : "",
                        memIvsCorSealFile : {
                            fileData : "",
                            fileName : "",
                            state: true  
                        },
                        memIvsRnmDoc : "",
                        memIvsRnmDocStatus : "",
                        memIvsRnmFile : {
                            fileData : "",
                            fileName : "",
                            state: true
                        },
                        memIvsRelDoc : "",
                        memIvsRelFile: {
                            fileData : "",
                            fileName : "",
                            state: true
                        },
                        memIvsRnmAttorney : "",
                        memIvsRnmAttorneyFile : {
                            fileData : "",
                            fileName : "",
                            state: true  
                        },
                        memIvsCorAttorney : "",
                        memIvsCorAttorneyFile : {
                            fileData : "",
                            fileName : "",
                            state: true  
                        },
                        memIvsRelComNo: "",
                        memIvsRelStatus: "MIR001",
                        memIvsRegiName : "",
                        memIvsRegiHpNo : "",
                        memIvsType: "MIT001",
                        memIvsSecurCode : "1",
                        memIvsSecurNo : "",
                        memIvsBankCode : "001",
                        memIvsBankNo : "",
                        memIvsState : "",
                        memIvsReturn : "",
                        memIvsRnmReturn : "",
                        memIvsRelReturn : ""
                    },
                    address : {
                        postNum : '',
                        address1 : ''
                    },
                    resultText : '',
                    telNumDisabled: true,
                    securCodeOptions : [],
                    bankCodeOptions : [],
                    confirm : {
                        bank : false,
                        community : false,
                        fund : false,
                        secur : false
                    },
                    relConfirm : "2",
                    relDisabled : false,
                    relRegistered : true,
                    authType : 1,
                    investor : "",
                    userTypeDocCount : 0,
                    viewAddButtonCount : -1,
                    rejectedContent: "",
                    micType1Disabled : false,
                    micType2Disabled : false,
                    micType3Disabled : false,
                    memIvsFilesDisabled : false,
                    memIvsRnmDocDisabled : false,
                    memIvsOriginalType : "",
                    memIvsOriginalFiles : [],
                    userTypeDocOriginalCount : 0,
                    viewAddButtonOriginalCount : -1,
                    checkAuthTried : false,
                    checkTerms1 : false,
                    checkTerms2 : false,  
                    os : '',
                    test:true
                }
            },
            created : function() {
                this.getBankCodeList();
                this.getStockCodeList();
                this.loadInvestState();
                this.loadBizNum();
                window.accountSetting = this;

                if(this.request.memIvsFiles.length == 0) {
                    this.request.memIvsFiles.push("");
                }

                // PC/Mobile 구분 
                this.deviceCheck();
            }, 
            mounted: function() {
                $(".switch-hb").bootstrapSwitch();
            },
            computed : {
                registerTitle : function() {
                    if(this.investor == 'Y') {
                        return "수정"
                    } else {
                        return "등록"
                    }
                },
                postNum : function() {
                    this.request.memIvsPostNum = this.address.postNum
                    return this.address.postNum
                },
                address1 : function() {
                    this.request.memIvsAddr1 = this.address.address1
                    return this.address.address1
                },
                userName : function() {
                    return userInfo.name
                },
                userEmail : function() {
                    return userInfo.email
                },
                userImage : function() {
                    return userInfo.image
                },
                ivsFileExist : function() {
                    for (var i = 0; i < this.request.memIvsFiles.length; i++) {
                       if(this.request.memIvsFiles[i].state == true && this.request.memIvsFiles[i].fileName != '') {
                            return true;
                       }
                    }
                    return false;
                }
            },
            methods : {
                loadInvestState : function() {
                    var self = this;
                    
                    axios.post('/data/member/investor/state', this.request)
                        .then(function(response){
                            self.investor = response.data.rData.investor;
                            self.request.memIvsState = response.data.rData.memIvsState;

                            if(self.investor == 'Y') {
                                self.load();
                            } else {
                                self.userTypeDocCount++;
                                self.viewAddButtonCount++;  

                                self.$nextTick(function() {
                                    $( ".option_group .radio-inline input, .option_group .checkbox-inline input" ).checkboxradio();
                                    $( ".option1_radio, .option1_checkbox" ).controlgroup();
                                });
                            }
                        })
                },
                loadBizNum : function() {
                    var self = this;
                    
                    axios.post('/data/member/info', this.request)
                        .then(function(response){
                            self.request.memIvsComNo = response.data.rData.bizNum;
                        })
                },
                load : function() {
                    var self = this;
                    axios.post('/data/member/investor/info', this.request)
                        .then(function(response){
                            self.request.memIvsAddr1 = response.data.rData.memIvsAddr1;
                            self.request.memIvsAddr2 = response.data.rData.memIvsAddr2;
                            self.request.memIvsPostNum = response.data.rData.memIvsPostNum;
                            self.address.postNum =  response.data.rData.memIvsPostNum;
                            self.address.address1 = response.data.rData.memIvsAddr1;
                            self.request.memIvsComNo = response.data.rData.memIvsComNo;
                            self.request.memIvsCorNo = response.data.rData.memIvsCorNo;
                            self.request.memIvsCeo = response.data.rData.memIvsCeo;
                            self.request.memIvsStockType = response.data.rData.memIvsStockType;
                            self.request.memIvsTellNo = response.data.rData.memIvsTellNo;
                            self.request.memIvsSecurNo = response.data.rData.memIvsSecurNo;

                            if (response.data.rData.memIvsSecurCode.length > 0) {
                                self.request.memIvsSecurCode = response.data.rData.memIvsSecurCode;    
                            }

                            self.request.memIvsResName = response.data.rData.memIvsResName;
                            self.request.memIvsResEmail = response.data.rData.memIvsResEmail;
                            self.request.memIvsRegiHpNo = response.data.rData.memIvsRegiHpNo;
                            self.request.memIvsType = response.data.rData.memIvsType;
                            self.request.memIvsState = response.data.rData.memIvsState;

                            self.request.memIvsFiles = response.data.rData.memIvsFiles;

                            self.request.memIvsCorFile.fileName = response.data.rData.memIvsCorDocName;
                            self.request.memIvsRnmCorFile.fileName = response.data.rData.memIvsRnmCorDocName;
                            self.request.memIvsCorSealFile.fileName = response.data.rData.memIvsCorSealName;
                            self.request.memIvsRnmFile.fileName = response.data.rData.memIvsRnmDocName;
                            self.request.memIvsRnmAttorneyFile.fileName = response.data.rData.memIvsRnmAttorneyName;
                            self.request.memIvsCorAttorneyFile.fileName = response.data.rData.memIvsCorAttorneyName;

                            self.request.memIvsRelStatus = response.data.rData.memIvsRelStatus;
                            self.request.memIvsRelComNo = response.data.rData.memIvsRelComNo;
                            self.request.memIvsRelFile.fileName = response.data.rData.memIvsRelDocName;         

                            self.request.memIvsReturn = response.data.rData.memIvsReturn;
                            self.request.memIvsRnmReturn = response.data.rData.memIvsRnmReturn;
                            self.request.memIvsRelReturn = response.data.rData.memIvsRelReturn;
                            self.request.memIvsRnmDocStatus = response.data.rData.memIvsRnmDocStatus;

                            self.memIvsOriginalType = response.data.rData.memIvsType;
                            
                            if(self.request.memIvsType == 'MIT002' && self.request.memIvsState != 'MIC003') {
                                self.micType1Disabled = true;
                                
                                if(self.request.memIvsState == 'MIC002') {
                                    self.micType3Disabled = false;

                                    for(var i=0; i < self.request.memIvsFiles.length; i++) {
                                        self.memIvsOriginalFiles.push(self.request.memIvsFiles[i]);
                                    }
                                } else {
                                    self.micType3Disabled = true;
                                }

                            } else if(self.request.memIvsType == 'MIT003' && self.request.memIvsState != 'MIC003') {
                                self.micType1Disabled = true;
                                self.micType2Disabled = true;
                            }


                            if(self.request.memIvsState == 'MIC003') {
                                self.request.memIvsFiles = [{
                                    fileData : "",
                                    fileName : "",
                                    state: true
                                }];
                            } else {
                                if(self.request.memIvsType != 'MIT001') {
                                    self.memIvsFilesDisabled = true;    
                                }
                            }

                            if(self.request.memIvsRnmDocStatus == 'MIRT01' || self.request.memIvsRnmDocStatus == 'MIRT04') {
                                self.request.memIvsCorFile.fileName = "";
                                self.request.memIvsRnmCorFile.fileName = "";
                                self.request.memIvsCorSealFile.fileName = "";
                                self.request.memIvsRnmFile.fileName = "";
                                self.request.memIvsRnmAttorneyFile.fileName = "";
                                self.request.memIvsCorAttorneyFile.fileName = "";
                                self.memIvsRnmDocDisabled = false;
                            } else {
                                self.memIvsRnmDocDisabled = true;
                            }

                            if(self.request.memIvsRelStatus != 'MIR001') {
                                self.relConfirm = "1";
                                if(self.request.memIvsRelStatus == 'MIR002' || self.request.memIvsRelStatus == 'MIR003') {
                                    self.relDisabled = true;
                                    self.relRegistered = true;
                                } else {
                                    self.relRegistered = false;
                                    self.relDisabled = false;
                                    //self.request.memIvsRelFile.fileName = "";
                                    //self.request.memIvsRelComNo = "";
                                }
                            } else {
                                self.relRegistered = false;
                                self.relDisabled = false;
                            }

                            for(var i=0; i < self.request.memIvsFiles.length; i++) {
                                if(self.request.memIvsFiles[i].state) {
                                    self.userTypeDocCount++;
                                    self.viewAddButtonCount++;
                                }
                            }

                            if(self.request.memIvsFiles.length == 0) {
                                var addValue = {
                                    fileData : "",
                                    fileName : "",
                                    state: true
                                }

                                if(self.userTypeDocCount < 5) {
                                    self.userTypeDocCount++;
                                    self.viewAddButtonCount++;
                                    self.request.memIvsFiles.push(addValue);
                                }
                            }

                            self.userTypeDocOriginalCount = self.userTypeDocCount;
                            self.viewAddButtonOriginalCount = self.viewAddButtonCount;

                            self.$nextTick(function() {
                                $( ".option_group .radio-inline input, .option_group .checkbox-inline input" ).checkboxradio();
                                $( ".option1_radio, .option1_checkbox" ).controlgroup();
                            });
                        });
                },
                subMenuChange : function(menu) {
                    this.type = menu;
                },
                postOpen : function() {
                    post.open(this);
                },
                deviceCheck : function() {
                    if(isMobile.apple.device) {
                        this.os = 'M';
                    } else if(isMobile.android.device) {
                        this.os = 'M';
                    } else {
                        this.os = 'W';
                    }
                },
                auth : function() {

                    if (this.os == '') {
                        this.os = 'M';
                    }

                    this.request.authChange = false;
                    
                    axios.post('/auth/nice/' + this.os)
                        .then(function(response){
                            // $('#encodeData').val(response.data.rData.data)
                            document.getElementById('encodeData').value = response.data.rData.data;
                            window.open('', 'popupChk', 'width=500, height=800, top=100, left=100, fullscreen=no, menubar=no, status=no, toolbar=no, titlebar=yes, location=no, scrollbar=no');
                            document.form_chk.action = "https://nice.checkplus.co.kr/CheckPlusSafeModel/checkplus.cb";
                            document.form_chk.target = "popupChk";
                            document.form_chk.submit();
                        });
                },
                authResult : function(result) {
                    if(result) {
                        var self = this;
                        axios.post('/auth/info')
                            .then(function(response) {
                                self.request.memIvsResName = response.data.name;
                                self.request.memIvsRegiHpNo = response.data.mobileNo;
                                self.request.authChange = true;
                                self.checkAuthTried = true;
                                noti.open('본인인증이 완료되었습니다. <br/> 남은 사항들을 입력해주세요.');
                            });
                    } else {
                        noti.open('본인인증에 실패했습니다 <br/> 다시시도해주세요.')
                    }
                },
                changeRelStatus : function() {
                    if(this.relConfirm==2) {
                        this.request.memIvsRelStatus = "MIR001"
                        this.request.memIvsRelFile = {
                            fileData : "",
                            fileName : "",
                            state: true
                        }
                        this.request.memIvsRelComNo = ""
                        this.request.memIvsRelDocName = ""
                        this.relConfirm = "2";
                    } else {
                        this.request.memIvsRelStatus = "MIR002"
                        this.relConfirm = "1";
                    }
                },
                error : function(size) {
                    noti.open('업로드되는 파일 사이즈는 10MB보다 작아야합니다.')
                },
                fileAdd : function() {

                    var addValue = {
                        fileData : "",
                        fileName : "",
                        state: true
                    }

                    if(this.userTypeDocCount < 5) {
                        this.userTypeDocCount++;
                        this.viewAddButtonCount++;
                        this.request.memIvsFiles.push(addValue);
                    }
                },
                fileDelete : function(index) {
                    this.request.memIvsFiles[index].state = false;
                    if(this.userTypeDocCount > 1) {
                        this.userTypeDocCount--;
                    }

                    for(var i=0; i < this.request.memIvsFiles.length; i++) {
                        if(this.request.memIvsFiles[i].state == true) {
                            this.viewAddButtonCount = i;
                        }
                    }
                },
                getStockCodeList : function() {
                    var self = this;
                    
                    axios.post('/data/crowdy/code/invest', {gcode : "MEM_IVS_SECUR_CODE"})
                        .then(function(response){
                            var result = response.data;

                            if(result.rCode == "0000") {
                                for (var i = 0; i < result.rData.length; i++) {
                                    var option = {
                                            id : result.rData[i].commonCode,
                                            text : result.rData[i].commonInfo
                                    };
                                    self.securCodeOptions.push(option);
                                }
                                self.confirm.secur = true;
                            } else {
                                noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
                            }
                    })
                },
                getBankCodeList : function() {
                    var self = this;
                    
                    axios.post('/data/crowdy/code/invest', {gcode : "IRMI_STOCK_BANK"})
                        .then(function(response){
                            var result = response.data;

                            if(result.rCode == "0000") {
                                for (var i = 0; i < result.rData.length; i++) {
                                    var option = {
                                            id : result.rData[i].commonCode,
                                            text : result.rData[i].commonInfo
                                    };
                                    self.bankCodeOptions.push(option);
                                }
                                self.confirm.bank = true;
                            } else {
                                noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
                            }
                    })
                },
                applyData: function() {
                    if(this.checkTerms1 == false || this.checkTerms2 == false) {
                        noti.open("투자 회원 등록을 위해 약관 동의가 필요합니다.");    
                        return;
                    }
                    
                    if(!this.request.authChange) {
                        noti.open("본인인증을 완료해주십시오.");
                        return;
                    }

                    if(this.request.memIvsAddr2.length == 0) {
                        noti.open("주소를 입력해주세요.");
                        return;
                    }

                    if(this.request.memIvsCorNo.length == 0) {
                        noti.open("법인등록번호를 입력해주세요.");
                        return;
                    }

                    // if(this.request.memIvsSecurNo.length == 0) {
                    //     noti.open("증권계좌의 계좌번호를 입력해주세요.");
                    //     return;
                    // }

                    if(this.request.memIvsCorFile.fileData.length == 0) {
                        noti.open("사업자등록증은 실명확인용 필수서류입니다.");
                        return;
                    }

                    if(this.request.memIvsRnmCorFile.fileData.length == 0) {
                        noti.open("법인등기부등본은 실명확인용 필수서류입니다.");
                        return;
                    }

                    if(this.request.memIvsCorSealFile.fileData.length == 0) {
                        noti.open("법인감증명서는 실명확인용 필수서류입니다.");
                        return;
                    }
                    
                    if(this.request.memIvsRnmFile.fileData.length == 0) {
                        noti.open("실명확인증표는 실명확인용 필수서류입니다.");
                        return;
                    }

                    if ((this.request.memIvsType == 'MIT002' || this.request.memIvsType == 'MIT003') && !this.ivsFileExist) {
                        noti.open("회원유형을 적격투자자나 전문투자자로 변경하시려면 <br/> 자격요건에 따른 서류를 제출하셔야합니다.");
                        return;
                    }

                    for(var i=0; i < this.request.memIvsFiles.length; i++) {
                        if(this.request.memIvsFiles[i].state && this.request.memIvsFiles[i].fileName.length == 0) {
                            this.request.memIvsFiles[i].state = false;
                        }
                    }

                    $('.page-loader-more').fadeIn('');
                    axios.post('/member/investor/insert', this.request)
                        .then(function(response){
                            $('.page-loader-more').fadeOut('');

                            // gtm 투자회원 가입 완료 이벤트 발생
                            window.dataLayer.push({
                                'event': 'InvestorComplete'
                            });
                            
                            noti.open('투자회원 등록 신청이 완료되었습니다.', function() {window.open("/", '_self');});
                        });
                },
                updateBasicInfo : function() {
                    this.request.authChange = false;
                    if(this.request.memIvsAddr2.length == 0) {
                        noti.open("주소를 입력해주세요.");
                        return;
                    }

                    $('.page-loader-more').fadeIn('');
                    axios.post('/member/investor/update/basic', this.request)
                        .then(function(response){
                            $('.page-loader-more').fadeOut('');
                            noti.open('기본정보 수정이 완료되었습니다.');
                        });
                },
                updateResInfo : function() {
                    if(this.checkAuthTried == false) {
                        this.request.authChange = false;
                    }

                    $('.page-loader-more').fadeIn('');
                    axios.post('/member/investor/update/representative', this.request)
                        .then(function(response){
                            $('.page-loader-more').fadeOut('');
                            noti.open('담당자 정보 변경이 완료되었습니다.');
                        });
                },
                updateIvsType : function() {
                    this.request.memIvsDoc = "";

                    if ((this.request.memIvsType == 'MIT002' || this.request.memIvsType == 'MIT003') && !this.ivsFileExist) {
                        noti.open("회원유형을 적격투자자나 전문투자자로 변경하시려면 <br/> 자격요건에 따른 서류를 제출하셔야합니다.");
                        return;
                    }

                    $('.page-loader-more').fadeIn('');
                    axios.post('/member/investor/update/type', this.request)
                        .then(function(response){
                            $('.page-loader-more').fadeOut('');
                            noti.open('회원유형 변경 신청이 완료되었습니다.');
                        });
                },
                updateRealNameDoc : function() {
                    if(this.request.memIvsCorFile.fileData.length == 0) {
                        noti.open("사업자등록증은 실명확인용 필수서류입니다.");
                        return;
                    }
                    if(this.request.memIvsRnmCorFile.fileData.length == 0) {
                        noti.open("법인등기부등본은 실명확인용 필수서류입니다.");
                        return;
                    }
                    if(this.request.memIvsCorSealFile.fileData.length == 0) {
                        noti.open("법인감증명서는 실명확인용 필수서류입니다.");
                        return;
                    }

                    this.request.memIvsRnmDoc = "";

                    $('.page-loader-more').fadeIn('');
                    axios.post('/member/investor/update/real-name-doc', this.request)
                        .then(function(response){
                            $('.page-loader-more').fadeOut('');
                            noti.open('실명확인용 정보 수정 신청이 완료되었습니다.');
                        });
                },
                updateRel : function() {
                    var self = this;

                    if (this.request.memIvsRelComNo == "" || this.request.memIvsRelComNo == null) {
                        noti.open('투자할 기업의 사업자등록번호를 입력해주세요.')
                        return;
                    }

                    if (this.request.memIvsRelFile.fileData.length == 0 && this.request.memIvsRelFile.fileName == '') {
                        noti.open('연고자 증빙서류를 업로드해주세요.')
                        return;
                    }

                    if (this.request.memIvsRelFile.fileData.length > 0) {
                        this.request.memIvsRelDoc = "";
                    }       

                    if((this.request.memIvsRelStatus == 'MIR004' && this.request.memIvsRelFile.fileData.length != 0) || this.request.memIvsRelStatus == 'MIR001') {
                        this.request.memIvsRelStatus = 'MIR002';
                    }

                    $('.page-loader-more').fadeIn('');
                    axios.post('/member/investor/update/rel', this.request)
                        .then(function(response){
                            $('.page-loader-more').fadeOut('');
                            noti.open('연고자 정보 수정이 신청되었습니다.');
                            self.loadInvestState();
                        });
                },
                showRejectedModal : function(mode) {
                    // mode (1:실명확인서류, 2:투자회원유형, 3:연고자)
                    if(mode === 1) {
                        this.rejectedContent = this.request.memIvsRnmReturn;
                    } else if(mode === 2) {
                        this.rejectedContent = this.request.memIvsReturn;
                    } else {
                        this.rejectedContent = this.request.memIvsRelReturn;
                    }
                    $('#aRejectedModal').modal('show');  
                },
                changeMemIvsType : function(mode) {
                    // mode (1: 소득요건 등 적격투자자, 2: 전문투자자)

                    if(this.memIvsOriginalType == 'MIT002' && this.request.memIvsState == 'MIC002') {
                        if(mode === 1) {
                            this.memIvsFilesDisabled = true;
                            this.request.memIvsFiles = [];
                            for(var i=0; i < this.memIvsOriginalFiles.length; i++) {
                                this.request.memIvsFiles.push(this.memIvsOriginalFiles[i]);
                                this.request.memIvsFiles[i].state = true;
                            }
                            
                            this.userTypeDocCount = this.userTypeDocOriginalCount;
                            this.viewAddButtonCount = this.viewAddButtonOriginalCount;
                        } else {
                            var addValue = {
                                fileData : "",
                                fileName : "",
                                state: true
                            }

                            this.request.memIvsFiles = [];
                            this.request.memIvsFiles.push(addValue);
                            this.memIvsFilesDisabled = false;
                        }    
                    }                    
                }
            },
            //
            components :{
                dropifyInput : require('../common/dropify-input.js').default.component(),
                fileInput : require('../common/file-input.js').default.component(),
                securSelect : require('../common/select.js').default.component(),
                bankSelect : require('../common/select.js').default.component(),
                investModal : require('../common/invest-modal.js').default.component(),
                RejectedModal : require('../common/rejected-modal.js').default.component(),
                numberInput : require('../common/number-input.js').default.component()
            }
        }
    }
}

export default new MypageSettingMyMember3()