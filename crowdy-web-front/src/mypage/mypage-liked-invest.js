class MypageLikedInvest {
    component() {
        return {
            template : `
            <div>
                <div class="pn_list_wrap">
                    <!-- Loop -->
                    <a href="javascript:void(0)" class="pn_list_loop" v-for="item in likedList">
                        <div class="media">
                            <div class="media-left media-middle">
                                <img :src="'//' + item.pjCardImg" class="media-object" alt="..." />
                            </div>
                            <div class="media-body media-middle">
                                <div class="row">
                                    <div class="col-sm-9">
                                        <div class="pn_list_frame">
                                            <div class="pn_summury">투자 <span class="hidden-sm hidden-md hidden-lg pn_time grey-500 ml5">2017.09.10</span></div>
                                            <div class="pn_subject">{{ item.pjTItle }}</div>
                                            <div class="pn_time hidden-xs">{{ item.wdate }}</div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 text-right xs-text-left">
                                        <div class="pn_list_frame">
                                            <a class="xs-mt5 btn btn-xs btn-primary" v-on:click="likeCancel(item.pgIdx)">좋아요 취소</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <!-- //Loop -->
                    <!-- 페이징 -->
                    <nav class="text-center mt30 mb20" >
                        <paginate
                            :page-count="pageCount"
                            :class="'pagination'"
                            :click-handler="nextPage"
                            :force-page="forcePage"
                            >
                        </paginate>
                    </nav>
                    <!-- //페이징 -->
                </div>
            </div>
            `,
            props : ['subMenu'],
            data : function() {
                return {
                    likedList : [],
                    pageCount : 0,
                    forcePage : 0,
                    search : {
                        memCode : userInfo.memCode,
                        paging : {
			                page : "1",
			                count : "10"
			            }
                    },
                    pgIdx : '',
                }
            },
            created : function() {
                this.load();
            },
            components : {
                paginate : VuejsPaginate
            },
            methods : {
                load : function() {
                    var self = this;
                    axios.post('/data/mypage/liked/invest', this.search)
                    .then(function(response){
                        var remainder = parseInt(response.data.rData.count % self.search.paging.count);
                        var remainderCount = 0;
                        if(remainder > 0) {
                            remainderCount++;
                        }
                        self.pageCount = (parseInt(response.data.rData.count / self.search.paging.count)) + remainderCount;
                        self.likedList = response.data.rData.data;
                    })
                },
                nextPage : function(page) {
                    this.forcePage = page - 1;
                    this.search.paging.page = page;
                    this.load();
                },
                likeCancel : function(val) {
                    var self = this;
                    this.pgIdx = val;
                    noti.open("좋아요를 취소 하시겠습니까?", self.disliked, true);
                },
                disliked : function() {
                    var self = this;
                    axios.post('/data/mypage/liked/invest/dislike', {pgIdx : this.pgIdx} )
                    .then(function (response) {
                        var result = response.data;
                        if(result.rCode == "0000") {
                            self.load();
                        } else {
                            noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
                        }                                  
                    })
                },
                
            }
        }
    }
}

export default new MypageLikedInvest()