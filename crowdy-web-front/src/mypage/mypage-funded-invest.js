class MypageFundedInvest {
    component() {
        return {
            template : `
            <div>
                <div class="mif-list" v-for="item in fundedList">
                    <div class="mif-frame" v-on:click="detail(item.pjAliasUrl)" style="cursor: pointer;">
                        <div class="media">
                            <div class="media-left media-middle">
                                <img :src="'//' + item.pjCardImg" class="media-object" />
                            </div>
                            <div class="media-body media-middle">
                                <div class="pn_list_frame">
                                    <div class="pn_summury">{{ ivsStatusCode(item) }}</div>
                                    <div class="pn_subject">{{ item.pjTitle }}</div>
                                    <div class="pn_time">
                                        <span>{{ item.achieveMent }}%</span>
                                        <span v-if="(parseInt(item.ivsAssignQty) == parseInt(item.ivsAssQty))">청약일 {{ item.ivsResPayDt + " " + item.ivsResPayTm }}</span>
                                        <template>
                                            <span class="xs-mt5" v-if="item.pjEndStatus == '1'">남은기간 {{duration(item)}}일</span>
                                            <span class="xs-mt5 red-800" v-else>종료</span>
                                        </template>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- 증권구분 -->
                    <div class="row not-space row-label">
                        <div class="col-xs-3 col-sm-2">
                            <span>증권구분</span>
                        </div>
                        <div class="col-xs-9 col-sm-10 text-label">
                            <span>{{ projectSecureCode(item.pjSecurCode) }}</span>
                        </div>
                    </div>
                    <!-- //증권구분 -->

                    <!-- 투자금액 -->
                    <div class="row not-space row-label">
                        <div class="col-xs-3 col-sm-2">
                            <span>투자금액</span>
                        </div>
                        <div class="col-xs-9 col-sm-10 text-label">
                            <span>{{ parseInt((item.ivsAssignQty) * (item.pjContStock)).toLocaleString() }}원</span>
                        </div>
                    </div>
                    <!-- //투자금액 -->

                    <!-- 구좌수 -->
                    <div class="row not-space row-label">
                        <div class="col-xs-3 col-sm-2">
                            <span v-if="item.pjStockType == '1'">주식수</span>
                            <span v-else>구좌수</span>
                        </div>
                        <div class="col-xs-9 col-sm-10 text-label">
                            <span v-if="item.pjStockType == '1'">{{ parseInt(item.ivsAssignQty).toLocaleString() }}주</span>
                            <span v-else>{{ parseInt(item.ivsAssignQty).toLocaleString() }}구좌</span>
                        </div>
                    </div>
                    <!-- //구좌수 -->

                    <!-- 주식배정일  배정 완료된 이후에는 배정 주식수(배정 구좌수) 로 변경 -->
                    <div class="row not-space row-label" v-if="item.ivsStatus != 'IVSS03'">
                        <div class="col-xs-3 col-sm-2">
                            <span v-if="item.ivsStatus != 'IVSS02'">배정일</span>
                            <span v-if="item.pjStockType == '1' && item.ivsStatus == 'IVSS02'">배정 주식수</span>
                            <span v-if="item.pjStockType == '2' && item.ivsStatus == 'IVSS02'">배정 구좌수</span>
                        </div>
                        <div class="col-xs-9 col-sm-10 text-label">
                            <span v-if="item.ivsStatus != 'IVSS02'">{{ item.pjPublishSc }}</span>
                            <span v-if="item.pjStockType == '1' && item.ivsStatus == 'IVSS02'">{{parseInt(item.ivsAssQty).toLocaleString()}}주</span>
                            <span v-if="item.pjStockType == '2' && item.ivsStatus == 'IVSS02'">{{parseInt(item.ivsAssQty).toLocaleString()}}구좌</span>
                        </div>
                    </div>
                    <!-- //주식배정일 -->

                    <!-- 증권입고일 -->
                    <div class="row not-space row-label" v-if="item.ivsStatus != 'IVSS03'">
                        <div class="col-xs-3 col-sm-2">
                            <span>증권입고일</span>
                        </div>
                        <div class="col-xs-9 col-sm-8 text-label">
                            <span>
                                <strong class="blue-800">{{ item.pjWearingData }}</strong>
                                <div class="xs-text-right hidden-sm hidden-md hidden-lg xs-mt5 xs-mb10"></div>
                            </span>
                        </div>
                        <div class="hidden-xs col-xs-4 col-xs-offset-4 col-sm-2 col-sm-offset-0 text-right">
                            <span class="pr0"><a class="btn btn-block btn-sm btn-default-outline" v-if="item.pjEndStatus == '1' && (item.ivsStatus == 'IVSS00' || item.ivsStatus == 'IVSS06')" v-on:click="investCancel(item)">청약 취소</a></span>
                        </div>
                    </div>
                    <!-- //증권입고일 -->

                    <div class="row row-mobile-n hidden-sm hidden-md hidden-lg mt10 mb10">
                        <div class="col-xs-8"></div>
                        <div class="col-xs-4">
                            <a class="btn btn-block btn-sm btn-primary-outline" v-if="item.pjEndStatus == '1' && (item.ivsStatus == 'IVSS00' || item.ivsStatus == 'IVSS06')" v-on:click="investCancel(item)">청약 취소</a>
                        </div>
                    </div>                    
                    </div>
                    <!-- 페이징 -->
                    <nav class="text-center mt30 mb20">
                        <paginate
                            :page-count="pageCount"
                            :class="'pagination'"
                            :click-handler="nextPage"
                            :force-page="forcePage"
                            >
                        </paginate>
                    </nav>
                    <!-- //페이징 -->

                <!-- 정말 취소하시겠습니까? -->
                <div id="cancelModal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body modal-order">
                                "{{title}}" <br />청약을 취소합니다.<br />
                                <div class="mt15 red-800">정말 취소하시겠습니까?</div>

                                <div class="modal-footer text-center">
                                    <div class="row row-mobile-n">
                                        <div class="col-xs-8 col-xs-offset-2">
                                            <div class="row row-mobile-n">
                                                <div class="col-xs-6">
                                                    <a class="btn btn-block btn-primary-outline" v-on:click="cancelModalConfirm()">예</a>
                                                </div>
                                                <div class="col-xs-6">
                                                    <a class="btn btn-block btn-default-outline" v-on:click="cancelModalNo">아니오</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //정말 취소하시겠습니까? -->

                <!-- 청약외시간 안내 -->
                <div id="OutsideTime" class="modal fade" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <form class="modal-content form-horizontal" action="javascript:void(0)">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body text-center">
                                <div class="rewards-modal-head rewards-modal-head-big">
                                    <h3 class="sm">실시간 청약 & 청약취소 가능 시간</h3>
                                    <h5 class="webfont2">영업일 AM 5:00 - PM 11:00</h5>
                                </div>

                                <!-- <div class="mt20 mb30">
                                    <button type="button" class="btn btn-lg btn-primary-outline" data-dismiss="modal">청약알림신청</button>
                                </div>

                                <div class="mb30">
                                    알림 신청을 하시면 청약 가능한 시간에<br />
                                    알림톡으로 알려드립니다.
                                </div> -->
                            </div>
                        </form>
                    </div>
                </div>
                <!-- //청약외시간 안내 -->

            </div>
            `,
            props : ['code'],
            data : function() {
                return {
                    fundedList : [],
                    pageCount : 0,
                    forcePage : 0,
                    title : '',
                    search : {
                        memCode : userInfo.memCode,
                        paging : {
			                page : "1",
			                count : "4"
			            }
                    },
                    fundCancelData : {
                        pjCode : '',
                        memCode : '',
                    },
                    cancelRequest : {
                        pjInvestorIdx : '',
                        memCode : userInfo.memCode,
                        pjCode : '',
                        vsCancelType : '1'
                    },
                }
            },
            created : function() {
                this.load();
            },
            components : {
                paginate : VuejsPaginate
            },
            methods : {
                load : function() {
                    var self = this;
                    axios.post('/data/mypage/funded/invest', self.search)
                        .then(function(response){
                            var remainder = parseInt(response.data.rData.count % self.search.paging.count);
                            var remainderCount = 0;
                            if(remainder > 0) {
                                remainderCount++;
                            }
                            self.pageCount = (parseInt(response.data.rData.count / self.search.paging.count)) + remainderCount;
                            self.fundedList = response.data.rData.data;
                        })
                },
                nextPage : function(page) {
                    this.forcePage = page - 1;
                    this.search.paging.page = page;
                    this.load();
                },
                //증권구분
                projectSecureCode : function(pjSecCode) {
                    var result = "";
                    if(pjSecCode == "SEC001") {
                        result = "주식형";
                    } else if(pjSecCode == "SEC002") {
                        result = "채권형";
                    } else if(pjSecCode == "SEC003") {
                        result = "CB형";
                    } else if(pjSecCode == "SEC004") {
                        result = "BW형";
                    } else if(pjSecCode == "SEC005") {
                        result = "투자계약형";
                    }
                    return result;
                },
                //투자상태
                ivsStatusCode : function(data) {
                    var result = "";      
                    if(data.ivsStatus == "IVSS00") {
                        result = "청약완료";
                    } else if(data.ivsStatus == "IVSS01") {
                        result = "배정대기";
                    } else if(data.ivsStatus == "IVSS02" && (parseInt(data.ivsAssignQty) == parseInt(data.ivsAssQty))) {
                        result = "배정완료";
                    } else if(data.ivsStatus == "IVSS02" && (parseInt(data.ivsAssignQty) != parseInt(data.ivsAssQty))) {
                        result = "부분배정완료";
                    } else if(data.ivsStatus == "IVSS03") {
                        result = "청약취소";
                    } else if(data.ivsStatus == "IVSS04") {
                        result = "배정탈락";
                    } else if(data.ivsStatus == "IVSS05") {
                        result = "목표달성실패";
                    } else if(data.ivsStatus == "IVSS06") {
                        result = "청약부분완료";
                    } 
                    return result;
                },
                //청약취소
                investCancel : function(data) {
                    // noti.open("5월 5일, 6일, 7일 삼일 간 한국증권금융의 서비스 점검으로 인해 청약취소가 불가합니다. <br /> 청약 및 청약취소는 5월 8일 오전 5시부터 다시 정상적으로 가능합니다.");
                    // return;

                    // 청약 시간 확인 (공휴일 가능 / 일요일 불가능)
                    // var hour = moment().hour();
                    // Sunday as 0 and Saturday as 6
                    // var day = moment().day();

                    // if(hour < 5 || hour > 22 || day == 0) {
                    //     $('#OutsideTime').modal('show');
                    //     return;
                    // }

                    this.title = data.pjTitle;
                    this.cancelRequest.pjInvestorIdx = data.ivsIdx;
                    this.cancelRequest.pjCode = data.pjCode;
                    $('#cancelModal').modal('show');
                },
                //청약취소 - 확인
                cancelModalConfirm : function() {
                    var self = this;
                    $('.page-loader-more').fadeIn('')
                    axios.post('/data/invest/funding/pay-cancel', this.cancelRequest)
                    .then(function (response) {
                        var result = response.data;
                        $('.page-loader-more').fadeOut('');
                        $('#cancelModal').modal('hide');
                        if(result.rCode == "0000") {
                            noti.open('청약이 성공적으로 취소되었습니다.');
                            self.load();
                        } else {
                            noti.open(result.rMsg);
                            // noti.open("시스템 문제가 발생하였습니다.<br/>")
                        }  
                    })
                },
                //청약취소 - 취소
                cancelModalNo : function() {
                    $('#cancelModal').modal('hide');
                },
                detail : function(pjAliasUrl){
                    window.open("/i/" +  pjAliasUrl, '_self');
                },
                duration : function(data) {
                    return moment(data.pjEndDate).diff(moment().format("YYYY-MM-DD"), 'days');
                }
            }
        }
    }
}

export default new MypageFundedInvest()