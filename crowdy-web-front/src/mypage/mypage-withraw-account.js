class MypageWithdrawAccount {
    component() {
        return {
            template : `
            <div class="mt60 xs-mt30 mb80 mypage-myaccount-layout">
                <!-- 사진영역-->
                <div class="dropify-wrapper-80 mypage-myaccount-photo">
                    <dropify-input accept="image/*" default-message="" :default-img="userImage"></dropify-input>
                </div>
                <!-- //사진영역-->

                <!-- 컨텐츠 영역 -->
                <div class="mypage-form xs-mt30">
                    
                    <!-- 출금계좌가 있을 경우 -->
                    <template v-if="bankStatus == '1'">
                        
                        <div class="mypage-title">{{userName}}님의 출금계좌</div>

                        <div class="mypage-setting-block-box1 mt30"></div>
                        <div class="mypage-setting-virtual-text">
                            <div class="common-flex-between">
                                <div class="font16 fontBold ml20">크라우디{{userName}}</div>
                                <div class="mypage-setting-btn-small mr20 z-index-1000" v-on:click="accountProcess">변경하기</div>
                            </div>
                            <div class="common-default-flex mt10">
                                <div class="font13 pl70" style="width:100%;">
                                    <div style="line-height: 30px;">{{accountInfo.bankCom}}</div>
                                    <div style="line-height: 30px;">투자가능예치금</div>
                                </div>
                                <div class="font16 text-right pr20" style="width:100%;">
                                    <div>{{accountInfo.bankNo}}</div>
                                    <div class="blue-800 fontBold">{{parseInt(accountInfo.restAmount).toLocaleString()}}원</div>
                                </div>
                            </div>
                        </div>

                        <div class="font18 mt50">출금하기</div>
                        <div class="common-default-flex mt20">
                            <number-input type="tel" :num="amount" class="form-control mypage-amount-width" placeholder="0" v-model="amount"/></number-input>
                            <div class="mypage-default-btn" v-on:click="allAmount">전액</div>
                        </div>
                        <div class="font13 mt5"> 
                            - 출금 가능금액에 청약신청 중인 금액은 제외됩니다.<br/>
                            - 출금은 한 달에 2회 무료이며, 이후 이체수수료가 부과됩니다.
                        </div>

                        <div class="mypage-setting-btn mt20" v-on:click="withdraw">출금하기</div>
                    </template>
                    <!-- //출금계좌가 있을 경우 -->


                    <!-- 출금계좌가 없을 경우 -->
                    <template v-if="bankStatus != '1'">
                        
                        <div class="mypage-title">출금계좌 등록안내</div>
                        <div class="mypage-sub-title mt30">{{userName}}님의 출금계좌</div>
                        <div class="font13 mt20">
                            투자예치금을 출금하기 위해서 본인 명의의 은행계좌를 
                            <span class="blue-800">등록해야 합니다. 등록하려는 계좌의 실명 및 소유확인을 위해서 <b>1원 이체를 통한 본인인증을 진행</b>합니다.</span>
                        </div/>
                        <div class="mypage-setting-btn mt20" v-on:click="accountProcess">출금계좌 등록하기</div>

                    </template>
                    <!-- //출금계좌가 없을 경우 -->

                </div>
                <!-- //컨텐츠 영역 -->
            </div>
            `,
            props : ['bankStatus'],
            data : function() {
                return {
                    addFees : "N",
                    request : {
                        memCode : userInfo.memCode,
                        memIvsSecurNo : "",
                        memIvsSecurCode : "1"
                    },
                    accountInfo : {
                        //가상계좌번호
                        virtualAccount : 0,
                        //지금까지 총 예치되었던 금액들 
                        addTotalAmount : 0,
                        // 남은 예치금
                        restAmount : 0, 
                        bankCode : 0,
                        bankNo : 0,
                        //출금계좌 상태 (0: 1원이체진행중, 1: 성공, 2: 실패)
                        bankStatus : 0,
                        withdrawCount : 0,
                        limitCount : "2",
                        fees : "1000"
                    },
                    amount : 0
                }
            },
            created : function() {

                if (this.bankStatus == '1') {
                    this.getVirtualAccountInfo(false);
                }

            }, 
            computed : {
                userName : function() {
                    return userInfo.name
                },
                userCode : function() {
                    return userInfo.code
                },
                userImage : function() {
                    return userInfo.image
                }
            },
            methods : {
                getVirtualAccountInfo : function(refresh) {
                    var self = this;
                   
                     $('.page-loader-more').fadeIn('');
                    axios.post('/data/invest/virtual/account-refresh', { memCode : userInfo.memCode, seq : 0})
                        .then(function (response) {
                            $('.page-loader-more').fadeOut('');
                             var result = response.data;

                            if (result.rCode == '0000') {
                                self.accountInfo = result.rData;
                                if(refresh) {
                                    self.notiShow();
                                }
                            } else {
                                noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.");
                            }
                        })
                },
                notiShow : function() {
                    var notiText3 = "<div class='common-modal-title'>출금 요청</div>"  +
                                    "<div class='font15 mt15 xs-mt10'>은행으로 출금을 요청하였습니다. 은행 상황에 따라 시간이 다소 소요될 수 있습니다</div>";
                    info.open(notiText3, this.showAccountPage);
                },
                showAccountPage : function() {
                    this.$emit('change-withdraw-flag', false);
                },
                accountProcess : function(){    //출금등록 창 프로세스 
                    localStorage.setItem('return', window.location.pathname + window.location.search);
                    window.open("/account/main", '_self');
                },
                withdraw : function(){  //출금하기

                    var self = this;
                    var text = "";
 
                    if (parseInt(this.amount) > parseInt(this.accountInfo.restAmount)) {
                        noti.open("예치금보다 큰 금액은 출금이 불가능합니다.");
                        return;
                    }

                     if (parseInt(this.amount) < 1000) {
                        noti.open("1000원 이하의 금액은 출금할 수 없습니다.");
                        return;
                    }

                    if (parseInt(this.accountInfo.withdrawCount) >= parseInt(this.accountInfo.limitCount)) {
                        text = "<div class='common-modal-title'>출금 요청</div>"  +
                                    "<div class='font15 mt15 xs-mt10'><span class='blue-800'>"+ this.userName + "</span> 님의 " + this.accountInfo.bankCom + " ("+this.accountInfo.bankNo+") 계좌로 "+
                                    "<span class='blue-800'>출금수수료 " + this.accountInfo.fees + "원을 제외한 " + parseInt(parseInt(this.amount) -  parseInt(this.accountInfo.fees)).toLocaleString() +"원</span>이 출금됩니다</div>"+
                                    "<div class='mt5 font13 blue-800'>이번 달 무료 출금 횟수 초과</div>";
                        this.addFees = "Y";
                    } else {
                        text = "<div class='common-modal-title'>출금 요청</div>"  +
                                "<div class='font15 mt15 xs-mt10'><span class='blue-800'>"+ this.userName + "</span> 님의 " + this.accountInfo.bankCom + " ("+this.accountInfo.bankNo+") 계좌로 "+
                                "<span class='blue-800'>"+ parseInt(this.amount).toLocaleString() + "원</span>이 출금됩니다</div>"+
                                "<div class='mt5 font13'>무료 출금 횟수 이번 달<span class='blue-800'>" + (parseInt(this.accountInfo.limitCount) - parseInt(this.accountInfo.withdrawCount)) + "회 남음</span></div>";
                        this.addFees = "N";
                    }

                    info.open(text, this.withdrawRequest ,true);
                },
                withdrawRequest : function() {
                    var self = this;
                    $('.page-loader-more').fadeIn('');
                    axios.post('/data/invest/virtual/withdraw', { "memCode" : userInfo.memCode, "amount" : this.amount, "memo" : "크라우디" + this.accountInfo.bankName, "addFees" : this.addFees})
                        .then(function (response) {
                            $('.page-loader-more').fadeOut('');
                             var result = response.data;

                            if (result.rCode == '0000') {                                
                                self.getVirtualAccountInfo(true);
                            } else {
                                noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.");
                            }
                        })
                },
                allAmount : function() { //전액 선택
                    this.amount = _.cloneDeep(this.accountInfo.restAmount);
                }
            },
            components :{
                dropifyInput : require('../common/dropify-input.js').default.component(),
                securSelect : require('../common/select.js').default.component(),
                numberInput : require('../common/number-input.js').default.component()
            }
        }
    }
}

export default new MypageWithdrawAccount()