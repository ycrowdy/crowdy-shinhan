class MypageVirtualAccount {
    component() {
        return {
            template : `
            <div class="mt60 xs-mt30 mb80 mypage-myaccount-layout">
                <!-- 사진영역-->
                <div class="dropify-wrapper-80 mypage-myaccount-photo">
                    <dropify-input accept="image/*" default-message="" :default-img="userImage"></dropify-input>
                </div>
                <!-- //사진영역-->

                <!-- 컨텐츠 영역 -->
                <div class="mypage-form xs-mt30">
                    
                    <div class="mypage-title">투자계좌 등록하기</div>

                    <!-- 증권계좌 -->
                    <div class="mypage-sub-title mt30">{{userName}}님의 증권계좌</div>

                    <div class="font13 colorGray1 mt20 mb5">증권계좌</div>

                    <div class="common-default-flex mypage-select-custom">
                        <secur-select v-if="confirm.secur" :options="securCodeOptions" v-model="request.memIvsSecurCode" class="mr10"></secur-select>
                        <number-input type="tel" class="form-control" id="pay_secur_code" :num="request.memIvsSecurNo" v-model="request.memIvsSecurNo" placeholder=""></number-input>
                    </div>

                    <div class="mypage-setting-desc mt5">
                        투자자 본인 명의의 증권 계좌로 주권이 교부 됩니다. <span class="blue-800">직접 주식거래가 가능한 증권계좌번호</span>를 입력해주세요. CMA계좌는 사용이 불가합니다. 
                    </div>
                    <div class="mypage-setting-desc mt20">
                        <span class="blue-800">한국예탁결제원과 계약이 된 증권사의 계좌만 이용이 가능</span>합니다. 목록에 없는 증권사는 사용하실 수 없습니다. 증권계좌는 실시간으로 유효성 확인이 되지 않으며, 유효하지 않은 증권계좌 입력시 배정탈락의 사유가 될 수 있습니다.
                    </div>
                    
                    <div class="mypage-setting-btn mt20" v-on:click="securAccountSave()">증권계좌 저장</div>
                    <!-- //증권계좌 -->

                    <div class="mypage-sub-title mt60">{{userName}}님의 투자예치금 계좌</div>

                    <!-- 가상계좌 발급 후-->
                    <template v-if="virtualAccountConfirm=='Y'">                        
                        <!-- 계좌 정보 -->
                        <div class="mypage-setting-block-box1 mt20"></div>
                        <div class="mypage-setting-virtual-text">
                            <div class="common-flex-between">
                                <div class="mypage-setting-virtual-text1 ml20">크라우디 {{userName}}</div>
                                <div class="common-default-flex pointerCursor z-index-1000" v-on:click="reload">
                                    <div class="reload-ico"></div>
                                    <div class="font12 blue-800" style="margin:-4px 20px 0px 4px!important;" v-on:click="getVirtualAccountInfo">새로고침</div>
                                </div>
                            </div>
                            <div class="common-default-flex">
                                <div class="mypage-setting-desc pl70" style="width:100%;">
                                    <div>신한은행</div>
                                    <div>투자가능예치금</div>
                                </div>
                                <div class="mypage-setting-text2 text-right pr20 z-index-1000" style="width:100%;">
                                    <div>{{accountInfo.virtualAccount}}</div>
                                    <div class="blue-800 fontBold"> {{ parseInt(accountInfo.restAmount).toLocaleString() }}원</div>
                                </div>
                            </div>
                        </div>

                        <div class="mypage-setting-desc mt40">
                            위의 계좌는 크라우디의 투자 프로젝트를 위해 발급된 {{userName}}님의 투자 전용 계좌입니다. 
                            투자금을 입금하시거나 이후 채권 프로젝트의 투자금을 정산 받으시는 용도로 사용됩니다.
                        </div>
                        
                        <div class="mypage-setting-btn mt20" v-on:click="accountWithdraw">출금하기</div>
                        <!-- //계좌 정보 -->

                        <!-- 예치금 거래 내역 조회 부분 -->
                        <div class="mypage-sub-title mt50">예치금 거래 내역</div>

                        <div class="font13 colorGray1 mb5 mt20">조회기간</div>
                        <div class="common-default-flex mypage-form mypage-date-width">
                            <date-picker v-model="accountRequest.startDate" :date="accountRequest.startDate"></date-picker>
                            <div class="mypage-hr-custom"><hr/></div>
                            <date-picker v-model="accountRequest.endDate" :date="accountRequest.endDate"></date-picker>
                            <div class="mypage-default-btn" v-on:click="searchList">조회</div>
                        </div>

                        <div class="option_group option_group_none">
                            <div class="option1_radio">
                                <label for="option1_radio1" class="radio-inline mypage-radio-custom-2">
                                    <input type="radio" name="option1_radio" id="option1_radio1" value="1week" v-model="searchDay"/><span class="webfont">1주일</span>
                                </label>
                                <label for="option1_radio2" class="radio-inline mypage-radio-custom-2">
                                    <input type="radio" name="option1_radio" id="option1_radio2" value="1month" v-model="searchDay"/><span class="webfont">1개월</span>
                                </label>
                                <label for="option1_radio3" class="radio-inline mypage-radio-custom-2">
                                    <input type="radio" name="option1_radio" id="option1_radio3" value="3month" v-model="searchDay"/><span class="webfont">3개월</span>
                                </label>
                                <label for="option1_radio4" class="radio-inline mypage-radio-custom-2">
                                    <input type="radio" name="option1_radio" id="option1_radio4" value="6month" v-model="searchDay"/><span class="webfont">6개월</span>
                                </label>
                            </div>
                        </div>
                        <!-- //예치금 거래 내역 조회 부분 -->

                        <!--거래내역이 있을 때-->
                        <template v-if="accountList.length > 0">
                            <div v-for="(item, index) in accountList" class="mypage-virtual-listbox" :class="{mt20 : index == 0}">
                                <div class="font14 mypage-list-layout1">{{item.wdate}}</div>
                                <div class="mypage-list-layout2">
                                    <div class="font14">{{getAccountText(item)}}</div>
                                    <div class="mypage-virtual-text">{{item.vsBankCom}} {{item.vsPayName}}</div>
                                </div>
                                <template v-if="item.vsStatus == 1 || item.vsStatus == 2 || item.vsStatus == 4">
                                     <div class="font15 mypage-list-layout3 colorRed1 textRight"> -{{ parseInt(item.vsAmount).toLocaleString() }}원</div>
                                </template>

                                <template v-if="item.vsStatus == 0 || item.vsStatus == 3">  
                                     <div class="font15 mypage-list-layout3 colorBlue1 textRight">{{ parseInt(item.vsAmount).toLocaleString() }}원</div>
                                </template>

                            </div>
                        </template>
                        <!--//거래내역이 있을 때-->

                        <!-- 거래내역이 없을 때-->
                        <template v-if="accountList.length == 0">
                            <div class="mypage-setting-desc mt20">
                                거래내역이 없습니다. 조회버튼을 눌러 새로고침 해주세요.
                            </div>
                        </template>
                        <!-- //거래내역이 없을 때-->
                       
                        <div class="mypage-setting-btn mt20" v-if="moreShow" v-on:click="more()">더보기</div>
                        <!-- //예치금 거래 내역 -->
                    </template>
                    <!-- //가상계좌 발급 후-->


                    <!-- 가상계좌 발급 전-->
                    <template v-if="virtualAccountConfirm=='N'">
                        <div class="mypage-setting-desc mt20">
                            <b>투자예치금 계좌 발급 안내>  </b><br/>
                            투자 프로젝트에 참여하기 위해서는 투자예치금 계좌를 발급 받아야 합니다. 
                            <span class="blue-800">투자예치금 계좌는 ‘{{userName}}’님의 전용 가상계좌이며, 입금된 예치금을 통해 투자를 진행</span>하실 수 있습니다.<br/>
                            예치금은 금융투자업 규정 제4-114조에 따라 크라우디와 신한은행과의 별도 계약을 통해 '투자자재산'으로 부기됩니다.
                        </div>

                        <!-- 기존 투자회원 (CI가 없는 회원) 은 추가 본인인증 -->
                        <template v-if="memCiConfirm=='N'">
                            <div class="mypage-rabel mb5">본인인증</div>
                            <div class="mypage-setting-btn" v-on:click="authMobile()">{{authTitle}}</div>
                            <div class="blue-800 mb10" style="font-size:13px;">{{authResultText}}</div>

                            <!-- 카드인증 시 핸드폰 번호 문자 인증 추가 -->
                            <template v-if="isMobileAuthShown">
                                <div for="member_mobile" class="mypage-rabel mb5">전화번호</div>
                                <div class="common-flex-end">
                                    <number-input type="tel" class="form-control" placeholder="" maxlength="11" :num="mobileRequest.mobileNo" v-model="mobileRequest.mobileNo" :disabled="telNumDisabled" :class="{'mypage-tel-layout1' : isMobileAuthShown == true }"/></number-input>
                                    <a class="mypage-btn mypage-tel-layout2" v-on:click="smsCheck">{{mobileAuthRequestText}}</a>
                                </div>

                                <!--  인증번호 확인 버튼 -->
                                <div class="common-flex-end" v-if="checkNumber">
                                    <number-input type="tel" class="form-control mypage-tel-layout1" :num="mobileRequest.authNo" v-model="mobileRequest.authNo"/></number-input>
                                    <a class="mypage-btn mypage-tel-layout2" v-on:click="mobileAuth">인증번호 확인</a>
                                </div>
                                <div class="common-flex-between">
                                    <div>{{ time }}</div>  
                                    <div>{{ resultText }}</div>
                                </div>
                            </template>
                            <!-- //카드인증 시 핸드폰 번호 문자 인증 추가 -->
                        </template>
                        <!-- //기존 투자회원 (CI가 없는 회원) 은 추가 본인인증 -->

                        <div class="mypage-setting-block-box mt20"></div>
                        <div class="mypage-setting-agree">
                            <label class="chk_container">
                                투자예치금 계좌 발급 및 제3자 개인정보제공 <br/>
                                약관 동의 <a href="javascript:void(0)" data-toggle="modal" data-target="#virtualModal"><span class="blue-800 ml20">(전체보기)</span></a>
                                <input type="checkbox" v-model="checkTerms">
                                <span class="checkmark"></span>
                            </label> 
                        </div>

                        <div class="mypage-setting-btn mt30" v-on:click="virtualAccountRegist()">발급 받기</div>

                    </template>
                    <!-- //가상계좌 발급 전-->
                    <!-- //가상계좌 -->
                </div>
                <!-- //컨텐츠 영역 -->

                <!-- modal -->
                <div id="virtualModal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body">
                                <ul>
                                    <li>
                                        <strong class="grey-800"></strong>
                                        <ul>
                                            <li>
                                                회원은 본 약관에 동의함으로써, 크라우디 투자 프로젝트에 참여할 수 있는 투자예치금 계좌를 발급 받게 됩니다. 투자예치금 계좌는 회원의 전용 가상계좌이며, 투자 프로젝트에 참여 하기 전, 청약을 원하는 금액을 해당 계좌에 입금해야 합니다.
                                            </li>
                                            <li>
                                               회원이 투자 프로젝트에 청약을 완료하면, 청약증거금 상당의 금액이 투자예치금 계좌에서 공제되며, 해당 금액의 출금이 불가능합니다. 해당 금액은 청약한 투자프로젝트가 모집금액의 80% 달성에 실패할 경우, 프로젝트는 성공하였으나 배정 순위에서 탈락할 경우, 청약 기간 내에 청약 취소를 한 경우에는 회원의 투자예치금 계좌에 해당 금액이 복원되며, 본인 명의의 계좌로 출금이 가능합니다.
                                            </li>
                                            <li>
                                               투자예치금의 출금을 위해서는 '출금계좌등록'의 프로세스를 통해 본인 명의의 계좌를 등록하여야 합니다. 출금계좌등록 전에는 투자예치금 계좌에 잔액이 남아있다고 하더라도 출금이 불가능합니다. 출금계좌등록 프로세스에는 입력한 계좌의 명의확인, 1원 이체 방식을 통한 소유확인 절차가 포함됩니다.
                                            </li>
                                            <li>
                                               회원이 발급받는 투자예치금은 계좌는 (주)크라우디를 예금주로 하는 신한은행 계좌를 모계좌로 하며, 금융투자업 규정 제4-114조에 따라 투자자재산으로 부기 되어있습니다.
                                            </li>
                                            <li>
                                               투자예치금 계좌 발급 및 관리를 위해 크라우디가 신한은행에 제공하는 정보는 아래와 같습니다. 
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <div class="mt15"><strong class="grey-800">[개인회원]</strong></div>
                                        <ul>
                                            <li>
                                                고객명, 생년월일, 휴대폰번호, 출금통장 계좌번호, 가상계좌번호, CI (본인확인기관에서 부여하는 개인식별정보)
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <div class="mt15"><strong class="grey-800">[법인/조합회원]</strong></div>
                                        <ul>
                                            <li>
                                                법인/조합명, 사업자번호(조합번호), 대표이사명, 담당자 생년월일, 담당자 CI, 담당자 휴대폰번호, 출금통장 계좌번호, 가상계좌번호 
                                            </li>
                                        </ul>
                                    </li>
                                </ul>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- modal -->

                 <form name="form_chk" method="post">
                        <input type="hidden" name="m" value="checkplusSerivce">                     <!-- 필수 데이타로, 누락하시면 안됩니다. -->
                        <input type="hidden" name="EncodeData" id="encodeData" value="">            <!-- 위에서 업체정보를 암호화 한 데이타입니다. -->

                        <input type="hidden" name="param_r1" value="accountSetting.authResult">
                        <input type="hidden" name="param_r2" value="">
                        <input type="hidden" name="param_r3" value="">
                    </form>

            </div>
            `,
            props : ['virtualAccountConfirm', 'memCiConfirm'],
            data : function() {
                return {
                    checkTerms : false,
                    moreShow : false,
                    now : moment().format("YYYY-MM-DD"),
                    securCodeOptions : [],
                    confirm : {
                        secur : false
                    },
                    request : {
                        memCode : userInfo.memCode,
                        memIvsSecurNo : "",
                        memIvsSecurCode : "1"
                    },
                    searchDay : "1week",
                    accountList :[],
                    accountRequest : {
                        memCode : userInfo.memCode,
                        seq : 0,
                        startDate : moment(this.now).add(-7, 'days').format("YYYY-MM-DD"),
                        endDate : moment(this.now).format("YYYY-MM-DD"),
                        accountInfoConfirm : "N",
                        paging : {
                            page : "1",
                            count : "4"
                        }
                    },
                    accountInfo : {
                        //가상계좌번호
                        virtualAccount : 0,
                        //지금까지 총 예치되었던 금액들 
                        addTotalAmount : 0,
                        // 남은 예치금
                        restAmount : 0, 
                        bankCode : 0,
                        bankNo : 0,
                        //출금계좌 상태 (0: 1원이체진행중, 1: 성공, 2: 실패)
                        bankStatus : 0
                    },
                    authRequest : {
                        memCode : userInfo.memCode
                    },

                    // 본인인증 관련
                    os : '',
                    authChange : false,
                    authError : false,
                    authResultText : "",
                    isMobileAuthShown : false,
                    telNumDisabled : true,
                    authType : 1,
                    mobileAuthRequestText : "인증번호 요청",
                    mobileAuthRequestCount : 0,
                    checkNumber : false,

                    time : '3:00',
                    timerStart : false,
                    timeOut : false,
                    isCntdownRestart : false,
                    timer : {},
                    resultText : '',
                    telNumDisabled: true,
                    regiCodeDisabled: false,
                    isMobileAuthShown: false,

                    mobileRequest: {
                        mobileNo : '',
                        reqSeq : '',
                        authNo : ''
                    },

                    updateRequest : {
                        bizNum : "",
                        memCode : userInfo.memCode,
                        memType : userInfo.type,
                        memName : userInfo.name,
                        memEmail : userInfo.email,
                        resName : "",
                        memShotImg : userInfo.image,
                        memShotImgFile: {},
                        mobileNo : "",
                        authChange : false,
                        marketingAgree : 'N',
                        memNameConfirm : 'N'
                    }
                }
            },
            created : function() {
                this.investorInfo();
                this.getSecurCodeList();
                window.accountSetting = this;

                if (this.virtualAccountConfirm == 'Y') {
                    this.getVirtualAccountList("Y");
                }
                // PC/Mobile 구분 
                this.deviceCheck();
            }, 
            watch : {
                searchDay : function(value) {
                    if (value == '1week') {
                        this.accountRequest.endDate = moment(this.now).format("YYYY-MM-DD");
                        this.accountRequest.startDate = moment(this.now).add(-7, 'days').format("YYYY-MM-DD");
                    } else if (value == '1month'){
                        this.accountRequest.endDate = moment(this.now).format("YYYY-MM-DD");
                        this.accountRequest.startDate = moment(this.now).add(-1, 'month').format("YYYY-MM-DD");
                    } else if (value == '3month'){
                        this.accountRequest.endDate = moment(this.now).format("YYYY-MM-DD");
                        this.accountRequest.startDate = moment(this.now).add(-3, 'month').format("YYYY-MM-DD");
                    } else if (value == '6month'){
                        this.accountRequest.endDate = moment(this.now).format("YYYY-MM-DD");
                        this.accountRequest.startDate = moment(this.now).add(-6, 'month').format("YYYY-MM-DD");
                    }

                    this.searchList();
                },
                virtualAccountConfirm : function(){
                    var self = this;
                    //체크박스 설정 
                    self.$nextTick(function() {
                        $( ".option_group .radio-inline input" ).checkboxradio();
                        $( ".option1_radio" ).controlgroup();
                    });
                    self.searchDay = "1week";
                }    
            },
            computed : {
                userName : function() {
                    return userInfo.name
                },
                userImage : function() {
                    return userInfo.image
                },
                userType : function() {
                    return userInfo.type
                },
                authTitle : function(){     //인증버튼 타이틀 변경 처리
                    //등록일 때
                    if(this.authChange){
                        this.authResultText = "본인인증이 완료 되었습니다.";
                        return "본인인증 재요청";
                    }else{
                        if(this.authError){
                            this.authResultText = "본인인증을 다시 해주세요!";
                            return "본인인증 다시하기";
                        }else{
                            this.authResultText = "가상계좌 발급을 위해서 본인인증을 해주세요.";
                            return "본인인증 인증하기";
                        }
                    }
                }
            },
            methods : {
                investorInfo : function() {     //회원 정보
                    var self = this;
                    axios.post('/data/member/investor/info', this.request)
                        .then(function(response){
                            if (response.data.rData.memIvsSecurCode.length > 0) {
                                self.request.memIvsSecurCode = response.data.rData.memIvsSecurCode;    
                            }
                            self.request.memIvsSecurNo = response.data.rData.memIvsSecurNo;

                            //체크박스 설정 
                            self.$nextTick(function() {
                                $( ".option_group .radio-inline input" ).checkboxradio();
                                $( ".option1_radio" ).controlgroup();
                            });
                    })
                    
                },
                getSecurCodeList : function() {     //증권사 목록 리스트 업 
                    var self = this;
                    axios.post('/data/crowdy/code/invest', {gcode : "MEM_IVS_SECUR_CODE"})
                        .then(function(response){
                            var result = response.data;

                            if(result.rCode == "0000") {
                                for (var i = 0; i < result.rData.length; i++) {
                                    var option = {
                                            id : result.rData[i].commonCode,
                                            text : result.rData[i].commonInfo
                                    };
                                    self.securCodeOptions.push(option);
                                }
                                self.confirm.secur = true;
                            } else {
                                noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
                            }
                    })
                },
                registerInfo : function() {
                    //this.$emit('change-register-ivs-info', true);
                },
                securAccountSave : function(){      //증권계좌 저장   
                    var self = this;
                     $('.page-loader-more').fadeIn('');
                    axios.post('/data/member/investor/update/secur', this.request)
                        .then(function (response) {
                            $('.page-loader-more').fadeOut('');
                             var result = response.data;

                            if (result.rCode == '0000') {
                                noti.open("입력하신 계좌정보가 등록되었습니다.");
                            } else {
                                noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.");
                            }
                        })
                },
                deviceCheck : function() {
                     if(isMobile.apple.device) {
                        this.os = 'M';
                    } else if(isMobile.android.device) {
                        this.os = 'M';
                    } else {
                        this.os = 'W';
                    }
                },
                authMobile : function() {
                    var self = this;

                    if (this.os == '') {
                        this.os = 'M';
                    }

                    this.authChange = false;
                    this.isMobileAuthShown = false;
                    this.telNumDisabled = true;
                    this.authType = 1;
                    this.checkNumber = false;

                    axios.post('/auth/nice/' + this.os)
                        .then(function(response){
                            document.getElementById('encodeData').value = response.data.rData.data;
                            window.open('', 'popupChk', 'width=500, height=800, top=100, left=100, fullscreen=no, menubar=no, status=no, toolbar=no, titlebar=yes, location=no, scrollbar=no');
                            document.form_chk.action = "https://nice.checkplus.co.kr/CheckPlusSafeModel/checkplus.cb";
                            document.form_chk.target = "popupChk";
                            document.form_chk.submit();

                        });
                },
                authResult : function(result) {
                    if(result) {
                        var self = this;
                        axios.post('/auth/info')
                            .then(function(response) {
                                
                                if (self.userType == '1' && self.userName != response.data.name) {
                                    self.authError = true;
                                    noti.open('기존 회원이름과 본인인증한 이름이 다릅니다.');
                                    return;
                                }

                                self.updateRequest.memName = response.data.name;
                                self.updateRequest.mobileNo = response.data.mobileNo;
                                self.authChange = true;
                                self.authError = false;

                                if(response.data.mobileNo == "" || response.data.mobileNo == null) {
                                    self.authType = 2;
                                } 

                                if(self.authType == 2) {
                                    self.telNumDisabled = false;
                                    self.isMobileAuthShown = true;
                                    noti.open('본인인증이 완료되었습니다. <br/> 카드 인증을 한 경우 핸드폰 문자인증을 추가로 완료하셔야합니다.');
                                } else {
                                    self.changeAccountInfo();
                                }
                                
                            });
                    } else {
                        self.authError = true;
                        noti.open('본인인증에 실패했습니다. <br/> 다시 시도해주세요.');
                    }
                },
                changeAccountInfo : function() {
                    var self = this;
                    self.updateRequest.authChange = true;
                    $('.page-loader-more').fadeIn('')
                    axios.post('/member/update', this.updateRequest)
                        .then(function(response){
                            $('.page-loader-more').fadeOut('');
                            var result = response.data;
                            if(result.rCode == "0000") {
                                userInfo.updateInfo(response.data.rData);
                                self.chagneCiConfirm();
                                noti.open('본인인증이 완료되었습니다. <br/> [발급 받기] 버튼을 누르면 투자예치금계좌를 발급받을 수 있습니다.');
                            } else {
                                noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
                            }
                        })
                },
                smsCheck : function() {
                    var self = this;
                    this.mobileAuthRequestText = "인증번호 재요청";
                    this.mobileAuthRequestCount++;
                    this.checkNumber = true;
                    if(this.mobileAuthRequestCount > 1) {
                        this.isCntdownRestart = true;
                    }
                    axios.post('/auth/mobile', this.mobileRequest)
                        .then(function(response){
                            self.mobileRequest.reqSeq = response.data;
                            self.timerStart = true;
                            self.countdown(self.countdownTimeOut);
                        });
                },
                countdown : function(callback) {
                    var endTime, hours, mins, msLeft, time;
                    var minutes = 2;
                    var seconds = 59;
                    var self = this;

                    function twoDigits(n) {
                        return (n <= 9 ? "0" + n : n);
                    }

                    function updateTimer() {
                        if(self.isCntdownRestart){
                            self.time = "";
                            self.isCntdownRestart = false;
                            clearTimeout(self.timer);
                            minutes = 2;
                            seconds = 59;
                            endTime = (+new Date) + 1000 * (60 * minutes + seconds) + 500;
                        }

                        msLeft = endTime - (+new Date);
                        
                        if ( msLeft < 1000 ) {
                            callback();
                        } else {
                            time = new Date( msLeft );
                            hours = time.getUTCHours();
                            mins = time.getUTCMinutes();
                            self.time = (hours ? hours + ':' + twoDigits(mins) : mins) + ':' + twoDigits(time.getUTCSeconds())
                            self.timer = setTimeout(updateTimer, time.getUTCMilliseconds() + 500);
                        }
                    }
                    endTime = (+new Date) + 1000 * (60* minutes + seconds) + 500;
                    updateTimer();

                },
                countdownTimeOut : function() {
                    this.timeOut = true;
                    this.timerStart = false;
                },
                mobileAuth : function() {
                    var self = this;
                    axios.post('/auth/mobile/result', this.mobileRequest)
                        .then(function(response){
                            clearTimeout(self.timer);
                            self.timerStart = false;
                            if(response.data) {
                                self.authCardMobileConfirm = true;
                                self.updateRequest.mobileNo = self.mobileRequest.mobileNo;
                                // self.chagneCiConfirm();
                                // self.resultText = "휴대폰 번호 인증이 완료되었습니다."
                                self.changeAccountInfo();
                            } else {
                                self.resultText = "인증번호가 유효하지 않습니다."
                            }
                        });
                },
                authUpdate :function() {
                    var self = this;
                        axios.post('/data/member/update-certi', this.authRequest)
                            .then(function(response) {
                                if (response.data.rCode == '0000') {
                                    self.memCiConfirm = "Y";
                                    self.virtualAccountRegist();
                                } else {
                                     noti.open('본인인증에 실패했습니다 <br/> 다시 시도해주세요.');
                                     return;
                                }
                            });
                },
                chagneCiConfirm : function(){   //출금하기
                    this.memCiConfirm == "Y";
                    this.$emit('set-ci-confirm-flag', true);
                },
                virtualAccountRegist : function(){  //가상계좌 발급
                    var self = this;

                    if(this.memCiConfirm == 'N') {
                        noti.open("본인인증을 완료하셔야 투자예치금 발급이 가능합니다.");
                        return;
                    }

                    if(!this.checkTerms) {
                        noti.open("가상예치계좌 발급 및 예치금 신탁 약관 동의가 필요합니다.")
                        return;
                    }

                    $('.page-loader-more').fadeIn('');
                    axios.post('/data/invest/virtual/account-make', { memCode : userInfo.memCode, virtualType : '1' })
                        .then(function (response) {
                            $('.page-loader-more').fadeOut('');
                             var result = response.data;

                            if (result.rCode == '0000') {
                                self.chagneVirtualAccountConfirm();
                            } else {
                                noti.open(result.rMsg);
                                // noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.");
                            }
                        })
                },
                accountWithdraw : function(){   //출금하기
                    this.$emit('change-withdraw-flag', true);
                },
                chagneVirtualAccountConfirm : function(){   //출금하기
                    this.$emit('set-virtual-account-flag', true);
                },
                reload: function(){ //새로고침  
                    this.getVirtualAccountInfo();
                },
                more:function(){    //더보기
                    this.accountRequest.paging.page  = _.toString(_.add(_.toNumber(this.accountRequest.paging.page), 1));
                    this.getVirtualAccountList("N");
                },
                searchList : function() {
                    this.accountRequest.paging.page = '1';
                    this.accountRequest.paging.count = '4';
                    this.accountList = _.cloneDeep([]);
                    this.getVirtualAccountList("N");
                },
                getVirtualAccountInfo : function() {
                    var self = this;
                     $('.page-loader-more').fadeIn('');
                    axios.post('/data/invest/virtual/account-refresh', { memCode : userInfo.memCode, seq : 0})
                        .then(function (response) {
                            $('.page-loader-more').fadeOut('');
                             var result = response.data;

                            if (result.rCode == '0000') {
                                self.accountInfo = result.rData;
                            } else {
                                noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.");
                            }
                        })
                },
                getVirtualAccountList : function(confirm) {
                    var self = this;
                    this.accountRequest.accountInfoConfirm = confirm;

                     $('.page-loader-more').fadeIn('');
                    axios.post('/data/invest/virtual/withdraw-list', this.accountRequest)
                        .then(function (response) {
                            $('.page-loader-more').fadeOut('');
                             var result = response.data;

                            if (result.rCode == '0000') {

                                if (result.rData.list != [] && result.rData.length != 0) {
                                    self.accountList = _.concat(self.accountList, result.rData.list);    
                                    if(result.rData.length > self.accountList.length) {
                                        self.moreShow = true;
                                    } else {
                                        self.moreShow = false;
                                    }
                                } else {
                                    self.moreShow = false;
                                }

                                if (confirm == 'Y') {
                                    self.accountInfo = result.rData.data;
                                }

                            } else {
                                noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.");
                            }

                            //체크박스 설정 
                            self.$nextTick(function() {
                                $( ".option_group .radio-inline input" ).checkboxradio();
                                $( ".option1_radio" ).controlgroup();
                            });
                        })
                },
                getAccountText : function(item) {
                    // vsStatus : 예치금 내역 유형 (0:예치금 입금, 1:예치금 출금, 2:청약, 3:청약취소, 4:출금 수수료)
                    // vsCancelType : 청약 취소 유형 (0:청약진행중, 1:청약취소, 2:배정탈락, 3:부분배정탈락, 4:프로젝트실패)

                    if (item.vsStatus == '0') {
                        return "예치금 입금";
                    } else if (item.vsStatus == '1') {
                        return "예치금 출금";
                    } else if (item.vsStatus == '2') {
                        return "청약";
                    } else if (item.vsStatus == '3') {
                        if (item.vsCancelType == '1') {
                            return "청약취소";
                        } else if (item.vsCancelType == '2') {
                            return "배정탈락";
                        } else if (item.vsCancelType == '3') {
                            return "부분배정탈락";                            
                        } else if (item.vsCancelType == '4') {
                            return "프로젝트실패";                            
                        }
                    } else if (item.vsStatus == '4') {
                        return "출금 수수료";
                    }
    
                }
            },
            components :{
                investModal : require('../common/invest-modal.js').default.component(),
                dropifyInput : require('../common/dropify-input.js').default.component(),
                securSelect : require('../common/select.js').default.component(),
                numberInput : require('../common/number-input.js').default.component(),
                datePicker : require('../common/date-picker.js').default.component(),
                virtualModal : require('../common/virtual-modal.js').default.component()
            }
        }
    }
}

export default new MypageVirtualAccount()