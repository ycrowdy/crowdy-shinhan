class MypageMakedInvest {
    component() {
        return {
            template : `
            <div>
                <div class="rewards-list mp-list">
                    <div class="row row-mobile">
                        <!-- Loop -->
                        <div class="col-sm-6 col-md-4" v-for="item in makedList" v-on:click="detail(item, $event)">
                            <figure>
                                <div class="items over-box">
                                    <a href="javascript:void(0)">
                                        <div class="items_img">
                                            <img :src="'//' + checkImg(item.pjCardImg) + '/ycrowdy/resize/!340x!226'" class="img-responsive" alt="" />
                                        </div>
                                    </a>
                                    <figcaption class="rewards-caption">
                                        <a href="javascript:void(0)">
                                            <span class="btn btn-xs btn-danger-outline" v-if="item.pjRecStatus == 0">제작중</span>
                                            <span class="btn btn-xs btn-danger-outline" v-if="item.pjRecStatus == 1">승인요청중</span>
                                            <span class="btn btn-xs btn-primary-outline btn_cursor" v-if="item.pjRecStatus == 2 && item.pjEndStatus == 1">진행중</span>
                                            <span class="btn btn-xs btn-default-outline" v-if="item.pjRecStatus == 2 && item.pjEndStatus == 2 || item.pjEndStatus == 3">종료</span>
                                            <span class="btn btn-xs btn-danger-outline" v-if="item.pjRecStatus == 2">승인요청완료</span>
                                            <span class="btn btn-xs btn-danger-outline" v-if="item.pjRecStatus == 3">승인반려</span>
                                            <div class="rewards-subject">
                                                <strong>{{ item.pjTitle }}</strong>
                                                <div class="rewards-summury">{{ item.memName }}</div>
                                            </div>

                                            <div class="row row-mobile-n">
                                                <div class="col-xs-9 col-sm-8">
                                                    <span class="rewards-price"><span class="webfont2">\</span>{{ parseInt(item.pjAmount).toLocaleString() }}</span> <span class="rewards-percent">{{ item.achievement }}%</span>
                                                </div>
                                                <div class="col-xs-3 col-sm-4 text-right">
                                                    <span class="rewards-day">D-34</span>
                                                </div>
                                            </div>

                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" :style="{width: item.achievement + '%'}"><span class="sr-only">{{ item.achievement }}% 완료</span></div>
                                            </div>

                                            <div class="row row-mobile-n">
                                                <div class="col-xs-8">
                                                    <div class="invest-support">목표금액 {{ parseInt(item.tgAmount).toLocaleString() }}</div>
                                                </div>
                                                <div class="col-xs-4 text-right">
                                                    <div class="invest-support last-child">{{ parseInt(item.investorCount).toLocaleString() }}명 후원</div>
                                                </div>
                                            </div>
                                        </a>

                                        <div class="mp-btn">
                                            <div class="row row-mobile-n" v-if="item.pjRecStatus == 0">
                                                <div class="col-xs-6">
                                                    <a class="btn btn-block btn-sm btn-default-outline" v-on:click="projectDelete(item.pjCode, item.pjTitle)">삭제하기</a>
                                                </div>
                                                <div class="col-xs-6 hidden-sm hidden-md hidden-lg">
                                                    <a class="btn btn-block btn-sm btn-primary-outline" v-on:click="modifyModal($event)">수정하기</a>
                                                </div>
                                                <div class="col-xs-6 hidden-xs">
                                                    <a class="btn btn-block btn-sm btn-primary-outline" v-on:click="modify(item.pjCode, $event)">수정하기</a>
                                                </div>
                                            </div>
                                            <div class="row row-mobile-n" v-if="item.pjRecStatus == 1">
                                                <div class="col-xs-6">
                                                    <a class="btn btn-block btn-sm btn-default-outline" v-on:click="projectDelete(item.pjCode, item.pjTitle)">삭제하기</a>
                                                </div>
                                                <div class="col-xs-6">
                                                    <a class="btn btn-block btn-sm btn-primary-outline" v-on:click="approvalRequestCancelModal(item.pjCode, item.pjTitle)">승인요청취소</a>
                                                </div>
                                            </div>                                            
                                            <div class="row row-mobile-n" v-if="item.pjRecStatus == 2 && item.pjEndStatus == 1">
                                                <div class="col-xs-6">
                                                    <a v-on:click="dashboard(item.pjAliasUrl, $event)" class="btn btn-block btn-sm btn-default-outline">펀딩현황</a>
                                                </div>
                                                <div class="col-xs-6 hidden-sm hidden-md hidden-lg">
                                                    <a class="btn btn-block btn-sm btn-primary-outline" v-on:click="modifyModal($event)">수정하기</a>
                                                </div>
                                                <div class="col-xs-6 hidden-xs">
                                                    <a class="btn btn-block btn-sm btn-primary-outline" v-on:click="modify(item.pjCode, $event)">수정하기</a>
                                                </div>
                                            </div>
                                            <div class="row row-mobile-n" v-if="item.pjRecStatus == 2 && item.pjEndStatus == 2 || item.pjEndStatus == 3">
                                                <div class="col-xs-8 col-xs-offset-2">
                                                    <a v-on:click="dashboard(item.pjAliasUrl, $event)" class="btn btn-block btn-sm btn-primary-outline">프로젝트 결과보기</a>
                                                </div>
                                            </div>
                                            <div class="row row-mobile-n" v-if="item.pjRecStatus == 2 && item.pjEndStatus == 0">
                                                <div class="col-xs-8 col-xs-offset-2">
                                                    <a class="btn btn-block btn-sm btn-primary-outline">시작대기</a>
                                                </div>
                                            </div>
                                            <div class="row row-mobile-n" v-if="item.pjRecStatus == 3">
                                                <div class="col-xs-6 hidden-sm hidden-md hidden-lg">
                                                    <a class="btn btn-block btn-sm btn-default-outline" v-on:click="modifyModal($event)">수정하기</a>
                                                </div>
                                                <div class="col-xs-6 hidden-xs">
                                                    <a class="btn btn-block btn-sm btn-default-outline" v-on:click="modify(item.pjCode, $event)">수정하기</a>
                                                </div>
                                                <div class="col-xs-6">
                                                    <a class="btn btn-block btn-sm btn-primary-outline" v-on:click="rejectReasonModal(item.transContent)">반려사유보기</a>
                                                </div>
                                            </div>
                                        </div>
                                    </figcaption>
                                </div>
                            </figure>
                        </div>
                        <!-- //Loop -->
                    </div>
                </div>

                <!-- 페이징 -->
                <nav class="text-center mt30 mb20" >
                    <paginate
                        :page-count="pageCount"
                        :class="'pagination'"
                        :click-handler="nextPage"
                        :force-page="forcePage"
                        >
                    </paginate>
                </nav>
                <!-- //페이징 -->

                <!-- 삭제하기 -->
                <div id="deleteModal" class="modal fade" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <a class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                            </div>
                            <div class="modal-body modal-order">
                                <div v-html="title"></div><br />
                                <div class="mt15 red-800">정말 삭제하시겠습니까?</div>
                                <div class="modal-footer text-center">
                                    <a class="btn btn-block btn-primary-outline" v-on:click="deleteModalConfirm">삭제 완료</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //삭제하기 -->

                <!-- 승인요청취소 -->
                <div id="cancelModal" class="modal fade" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <a class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                            </div>
                            <div class="modal-body modal-order">
                                <div v-html="title"></div><br />
                                <div class="mt15 red-800">정말 취소하시겠습니까?</div>
                                <div class="modal-footer text-center">
                                    <div class="row not-space">
                                        <div class="col-xs-4 col-xs-offset-4">
                                            <a class="btn btn-block btn-primary-outline" v-on:click="approvalRequestCancelModalConfirm">취소 완료</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //승인요청취소 -->

                <!-- 수정하기 -->
                <div id="modifyModal" class="modal fade" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <a class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                            </div>
                            <div class="modal-body modal-order">
                                프로젝트 스토리 수정은 <br />PC에서만 가능한 기능입니다.

                                <div class="modal-footer text-center">
                                    <div class="row not-space">
                                        <div class="col-xs-4 col-xs-offset-4">
                                            <a class="btn btn-block btn-primary-outline" data-dismiss="modal">확인</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //수정하기 -->

                <!-- 반려사유 -->
                <div class="modal fade" id="aRejectedModal" role="dialog" tabindex="-1">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <a class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                            </div>
                            <div class="modal-body modal-order">
                                <p>
                                    승인이 반려되었습니다.
                                    <div class="mt15 red-800" v-html="rejectReason">
                                        반려사유 : 
                                    </div>
                                </p>

                                <div class="modal-footer text-center">
                                    <div class="row row-mobile-n">
                                        <div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3">
                                            <div class="row row-mobile-n">
                                                <div class="col-xs-12">
                                                    <a class="btn btn-block btn-default-outline" data-dismiss="modal">확인</a>
                                                </div>
                                                <!--<div class="col-xs-6">
                                                    <a class="btn btn-block btn-primary" data-dismiss="modal">등록</a>
                                                </div>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //반려사유 -->

                <!-- 즉시시작 -->
                <div id="startModal" class="modal fade" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <a class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                            </div>
                            <div class="modal-body modal-order">
                                <div v-html="title"></div><br />
                                <div class="mt15 red-800">프로젝트를 시작하시겠습니까?</div>

                                <div class="modal-footer text-center">
                                    <div class="row not-space">
                                        <div class="col-xs-4 col-xs-offset-4">
                                            <a class="btn btn-block btn-primary-outline" v-on:click="projectStartConfirm">시작</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //즉시시작 -->
            </div>
            `,
            props : ['code'],
            data : function() {
                return {
                    makedList : [],
                    pageCount : 0,
                    forcePage : 0,
                    search : {
                        memCode : userInfo.memCode,
                        paging : {
			                page : "1",
			                count : "6"
			            }
                    },
                    updateData : {
                        pjCode : '',
                    },
                    //반려사유
                    rejectReason : '',
                    //프로젝트명
                    title : '',
                    check : false,
                }
            },
            created : function() {
                this.load();
            },
            components : {
                paginate : VuejsPaginate
            },
            computed : {
              
            },
            methods : {
                load : function() {
                    var self = this;
                    axios.post('/data/mypage/maked/invest', self.search)
                        .then(function(response){
                            var remainder = parseInt(response.data.rData.count % self.search.paging.count);
                            var remainderCount = 0;
                            if(remainder > 0) {
                                remainderCount++;
                            }
                            self.pageCount = (parseInt(response.data.rData.count / self.search.paging.count)) + remainderCount;
                            self.makedList = response.data.rData.data;
                        })
                },
                nextPage : function(page) {
                    this.forcePage = page - 1;
                    this.search.paging.page = page;
                    this.load();
                },
                 //프로젝트 상세로 이동
                detail : function(data) {
                    this.check = false;
                    //제작중, 승인요청중, 승인반려 일때 상세로 이동 X
                    if(data.pjRecStatus == 0 || data.pjRecStatus == 1 || (data.pjRecStatus == 2 && data.pjEndStatus == 0) || data.pjRecStatus == 3) {
                        return;
                    }
                    window.open('/i/' + data.pjAliasUrl, '_self')
                },
                //삭제하기 Modal
                projectDelete : function(data, dataTitle) {
                    var self = this;
                    $('#deleteModal').modal('show');
                    this.updateData.pjCode = data;
                    self.title = dataTitle;
                },
                //삭제하기
                deleteModalConfirm : function() {
                    var self = this;
                    axios.post('/set/save/maked/invest/delete', self.updateData)
                    .then(function (response) {
                        var result = response.data;
                        if(result.rCode == "0000") {
                            noti.open('삭제되었습니다.');
                            $('#deleteModal').modal('hide');
                            self.load();
                        } else {
                            noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
                        }                                  
                    })
                },
                //승인요청 취소 Modal
                approvalRequestCancelModal : function(data, dataTitle) {
                    var self = this;
                    $('#cancelModal').modal('show');
                    this.updateData.pjCode = data;
                    self.title = dataTitle + '승인요청을 취소합니다';
                },
                //승인요청 취소확인
                approvalRequestCancelModalConfirm : function() {
                    var self = this;
                    axios.post('/set/save/maked/invest/approvalRequestCancel', self.updateData)
                    .then(function (response) {
                        var result = response.data;
                        if(result.rCode == "0000") {
                            noti.open('승인요청 취소 되었습니다.');
                            $('#cancelModal').modal('hide');
                            self.load();
                        } else {
                            noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
                        }                                  
                    })
                },
                //반려사유 Modal
                rejectReasonModal : function(data) {
                    var self = this;
                    $('#aRejectedModal').modal('show');
                    self.rejectReason = '반려사유 : ' + data;
                },
                //즉시시작
                projectStart : function(data, dataTitle) {
                    $('#startModal').modal('show');
                    var self = this;
                    this.updateData.pjCode = data;
                    self.title = dataTitle;
                },
                checkImg : function(img) {
                    return (img != null && img != '') ? img : "image-se.ycrowdy.com/logo/project-default.png";
                },
                //즉시시작 확인
                projectStartConfirm : function() {
                    var self = this;
                    axios.post('/data/mypage/maked/invest/start', self.updateData)
                    .then(function (response) {
                        var result = response.data;
                        if(result.rCode == "0000") {
                            noti.open('프로젝트가 시작되었습니다.');
                            $('#startModal').modal('hide');
                            self.load();
                        } else {
                            noti.open("시스템 문제가 발생하였습니다.<br/> 다시 시도해주세요.")
                        }                                  
                    })
                },
                dashboard : function(url, e) {
                    this.check = !this.check;
                    if(this.check) {
                        e.stopPropagation();
                        window.open("/dashboard/invest/" + url, '_self');
                    } 
                },
                modify : function(url, e) {
                    this.check = !this.check;
                    if(this.check) {
                        this.check = false;
                        e.stopPropagation();
                        window.open("/make/invest/main/" + url, '_self');
                    }
                },
                modifyModal : function(e) {
                    this.check = !this.check;
                    if(this.check) {
                        this.check = false;
                        e.stopPropagation();
                        $('#modifyModal').modal('show');
                    }
                }
            }
        }
    }
}

export default new MypageMakedInvest()