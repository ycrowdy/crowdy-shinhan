class PreOpenList {
	component() {
		return {
			template : `
            <div>
                <template v-if="dataConfirm">
                    <div :class="{'mt15 m-mt20 xs-mt20 mb10 xs-mb0' : main}">
                        <div class="row row-mobile">
                            <div v-for="(item, index) in preOpen" 
                                class="col-xs-6 col-sm-3 list_card_pre_open_height_parent">
                                <a :href="'/open/' + item.preopenIdx" class="bbs-caption over-box list_card_pre_open_height">
                                    <div class="bbs-img">
                                        <template v-if="main">
                                            <div v-if="item.preopenType == '1'" class="badge_icon badge_icon_event">{{typeConvert(item.preopenType)}}</div>
                                            <div v-if="item.preopenType == '2'" class="badge_icon badge_icon_invest_event">{{typeConvert(item.preopenType)}}</div>
                                        </template>
                                        <img :src="'//' + item.preopenImg + '/ycrowdy/resize/!370x!246'" class="img-responsive" alt="" />
                                    </div>
                                    <div class="bbs-subject">{{item.preopenBrand}}</div>
                                    <div class="bbs-summury">{{item.preopenTitle}}</div>
                                    <div class="bbs-date">{{item.preopenOpneDay}}</div>
                                </a>
                            </div>
                        </div>

                        <div v-show="moreShow">
                            <a href="javascript:void(0)" v-on:click="more()" class="btn btn-primary-outline col-sm-8 col-md-6 col-xs-12 col-sm-offset-2 col-md-offset-3 btn-more text-center mt50 mb20 md-mt65 md-mb0 m-mt50 m-mb20 xs-mt20 xs-mb10">사전공개 더보기</a>
                        </div>
                    </div>
                </template>

                <template v-if="!dataConfirm">
                    <div style="text-align: center;" class="pre_open_message">
                        <h4 class="main_pre_open_notice">프로젝트가 곧 <span class="blue-800">공개예정</span>입니다</h4>
                    </div>
                </template>
            </div>
			`,
            props : {
                main : {
                    default : true
                },
                paramPreOpenType : {
                    default : "0"
                },
                paramSearchCount : {
                    default : "4"
                }
            },
			data : function() {
				return {
					preOpen : [],
                	search : {
	                    preopenType : this.paramPreOpenType,
	                    paging : {
	                        page : "1",
	                        count : this.paramSearchCount
	                    }
                	},
                    moreShow : true,
                    dataConfirm : false
				}
				
			},
			created : function() {
                this.load();
            },
            methods : {
                load : function() {
                    var self = this;
                    axios.post('/data/crowdy/pre-project/list', this.search)
                    .then(function(response){
                        self.preOpen = _.concat(self.preOpen, response.data.rData);
                        if (self.preOpen.length > 0) {
                            self.dataConfirm = true;
                        }
                        if(response.data.rData.length != parseInt(self.search.paging.count)) {
                            self.moreShow = false;
                        }
                    })
                },
                typeConvert : function(type) {
                    return type == "1" ? "리워드" : "투자"
                },
                more : function() {
                    this.search.paging.page  = _.toString(_.add(_.toNumber(this.search.paging.page), 1));
                    this.load();
                },
			}
		}
	}
}

export default new PreOpenList()