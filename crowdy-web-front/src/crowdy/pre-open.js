class preOpen {
    component() {
        return {
            template : `
                <!-- 프로젝트 사전공개, 알림신청 -->
                <div id="list_wrap" class="deatil_wrap">
                    <div class="detail_vi_wrap">
                        <div class="detail_vi_frame"  :style="{'background-image' : 'url(//'+ preOpen.preopenBannerImg +')'}">
                            <div class="detail_vi_zindex">
                                <div class="detail_vi_type">
                                    <span class="invest webfont2" v-text="type"></span>
                                    <strong>{{ preOpen.preopenBrand }}</strong>
                                </div>
                                <h2 class="webfont2"> {{ preOpen.preopenTitle }}</h2>
                                <p>
                                    {{ preOpen.preopenOpneDay }}
                                </p>
                            </div>
                        </div>
                    </div>


                    <div class="detail_wrap">
                        <div class="container">
                            <div class="detail_project_noti">
                            프로젝트 시작 알림을 신청해주세요. <br class="hidden-sm hidden-md hidden-lg" /><br />
                            프로젝트가 시작할 때, 등록하신 연락처로 알림을 보내드립니다.    
                                
                                <div class="mt30 mb40">
                                    <a class="btn btn-lg btn-primary-outline" v-on:click="notiRequest">프로젝트 알림신청</a>
                                </div>

                                <div class="detail_not-area mce-content-body" v-html="preOpen.preopenContents">
                                </div>

                                <div class="text-center">
                                    <a class="btn btn-lg btn-primary-outline" v-on:click="notiRequest">프로젝트 알림신청</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- 알림신청 -->
                    <div id="prereserve" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <a class="close" v-on:click="dismiss"><span aria-hidden="true">&times;</span></a>
                                </div>
                                <div class="modal-body">
                                    <div class="rewards-modal-head rewards-modal-head-big">
                                        <hr class="big" />
                                        <h3 class="webfont2">프로젝트 알림신청</h3>
                                        <p class="big">프로젝트 시작시 문자 및 메일을 보내드립니다.</p>
                                    </div>
                                    <hr class="big" />
                                    
                                    
                                    <div class="row" style="margin: 0 auto;">
                                        <div class="form-group row-mobile-n">
                                            <div class="hidden-xs col-sm-2 col-md-2 col-lg-2"></div>
                                            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                                <div class="row row-mobile-n">
                                                    <div class="col-xs-6 col-sm-7 col-md-7 mb10">
                                                        <number-input class="form-control" :num="save.mobileNo" v-model="save.mobileNo" maxlength="11" :disabled="isMobileAuthShown" placeholder="휴대폰 번호를 입력해주세요."></number-input>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-5 col-md-5 mb10">
                                                        <a class="btn btn-block btn-primary" v-on:click="buttonClick">{{mobileAuthRequestText}}</a>
                                                    </div>
                                                    
                                                    <div class="col-xs-4 col-sm-5 col-md-5 mb10" v-if="isMobileAuthShown">
                                                        <input type="text" class="form-control" :disabled="mobileAuthCheck" v-model="mobileRequest.authNo" />
                                                    </div>
                                                    <div class="col-xs-2 col-sm-2 col-md-2 mt5 mb5" v-if="isMobileAuthShown" style="text-align:right;">
                                                        <span>{{ time }}</span>  
                                                    </div>
                                                    
                                                    <div class="col-xs-6 col-sm-5 col-md-5 mb10" v-if="isMobileAuthShown">
                                                        <a class="btn btn-block btn-primary" v-on:click="mobileAuth">인증번호 확인</a>
                                                    </div>
                                                    <div class="col-sm-3 col-md-3"></div>
                                                    <span class="col-xs-12 col-sm-9 col-md-9 mb10">{{ resultText }}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="modal-footer modal-footer-noline text-center xs-pt10">
                                            <div class="rewards-modal-tail-text">
                                                확인 버튼을 누르시면 프로젝트 정보 수신을 위한 <br />
                                                개인정보제공에 동의하신 것으로 간주합니다.<br />
                                            </div>
                                            <button type="button" class="btn btn-lg btn-primary-outline" v-on:click="confirm">확인</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- //알림신청 -->

                    <form name="form_chk" method="post">
                        <input type="hidden" name="m" value="checkplusSerivce">              <!-- 필수 데이타로, 누락하시면 안됩니다. -->
                        <input type="hidden" name="EncodeData" id="encodeData" value="">     <!-- 위에서 업체정보를 암호화 한 데이타입니다. -->
                        <input type="hidden" name="param_r1" value="accountSetting.authResult">
                        <input type="hidden" name="param_r2" value="">
                        <input type="hidden" name="param_r3" value="">
                    </form>
                </div>
                <!-- //프로젝트 사전공개, 알림신청 -->
            `,
            props : ['idx'],
            data : function() {
                return {
                    preOpen : {
                        preopenBannerImg : "",
                    },
                    search : {
                        preopenIdx : this.idx,
                        memCode : userInfo.memCode,
                    },
                    save : {
                        mobileNo : '',
                        preCode : this.idx,
                        memCode : userInfo.memCode,
                    },
                    mobileRequest: {
                        mobileNo : '',
                        reqSeq : '',
                        authNo : ''
                    },
                    time : '3:00',
                    resultText : '',
                    timerStart : false,
                    timeOut : false,
                    isCntdownRestart : false,
                    isMobileAuthShown: false,
                    timer : {},
                    mobileAuthRequestText : "인증번호 요청",
                    mobileAuthRequestCount : 0,
                    authChange : false,
                    authMobileConfirm : false,
                    mobileAuthCheck : false,
                }
            },
            computed : {
                type : function() {
                    return this.preOpen.preopenType == 1 ? '리워드' : '투자'
                }
            },
            created : function() {
                this.initData();
                this.load();
                window.accountSetting = this;

                $(window.document).on("contextmenu", function(){
                 if(event.target.nodeName == "IMG"){
                        return false;
                    }
                });

                $( ".mce-content-body img" ).css( "-webkit-user-select", "none !important" );
                $( ".mce-content-body img" ).css( "-webkit-touch-callout", "none !important" );
                $( ".mce-content-body img" ).css( "-moz-user-select", "none !important" );
                $( ".mce-content-body img" ).css( "-ms-user-select", "none !important" );
                $( ".mce-content-body img" ).css( "-o-user-select", "none !important" );
                $( ".mce-content-body img" ).css( "user-select", "none !important" );
                $( ".mce-content-body img" ).css( "pointer-events", "none !important" );
                $( ".mce-content-body img" ).attr( "draggable", "false" );
            },
            components : {
                vueNumeric : VueNumeric.default,
                numberInput : require('../common/number-input.js').default.component(),
            },
            methods : {
                initData : function() {
                    this.preOpen.preopenBannerImg == "";
                    this.search.preopenIdx = this.idx;
                    this.search.memCode = userInfo.memCode;
                    this.save.mobileNo = '';
                    this.save.preCode = this.idx;
                    this.save.memCode = userInfo.memCode;
                    this.mobileRequest.mobileNo = '';
                    this.mobileRequest.reqSeq = '';
                    this.mobileRequest.authNo = '';
                    this.time = '3:00';
                    this.resultText = '';
                    this.timerStart = false;
                    this.timeOut = false;
                    this.isCntdownRestart = false;
                    this.isMobileAuthShown = false;
                    this.timer = {};
                    this.mobileAuthRequestText = "인증번호 요청";
                    this.mobileAuthRequestCount = 0;
                    this.authChange = false;
                    this.authMobileConfirm = false;
                    this.mobileAuthCheck = false;
                },
                load : function() {
                    var self = this;
                    axios.post('/data/crowdy/pre-project/view', self.search)
                        .then(function(response){
                            if(response.data.rData.data == null) {
                                noti.open('잘못된 주소입니다.', function() {window.open("/", '_self');});
                                return;
                            }
                            var result = response.data.rData.data;
                            if(result.preopenStatus == '2') {
                                if (result.preopenType == 1) {
                                    window.open("/r/" + result.cpAliasUrl, '_self');
                                } else {
                                    window.open("/i/" + result.cpAliasUrl, '_self');
                                }
                                return;
                            }
                            self.preOpen = result;
                            self.save.mobileNo = response.data.rData.phoneno;
                        })
                },
                notiRequest : function() {
                    if(!userInfo.loginConfirm()) return;
                    $('#prereserve').modal('show');
                },
                buttonClick : function() {
                    var self = this;
                    if(this.save.mobileNo == "") {
                        noti.open("휴대폰 번호를 입력해 주세요.");
                        return;
                    }
                    this.mobileRequest.mobileNo = this.save.mobileNo;
                    this.mobileAuthRequestText = "인증번호 재요청";
                    this.mobileAuthRequestCount++;
                    if(this.mobileAuthRequestCount > 1) {
                        this.isCntdownRestart = true;
                        this.mobileAuthCheck = false;
                    }
                    axios.post('/auth/mobile', this.mobileRequest)
                        .then(function(response){
                            self.mobileRequest.reqSeq = response.data;
                            self.isMobileAuthShown = true;
                            self.timerStart = true;
                            self.countdown(self.countdownTimeOut);
                        });
                },
                mobileAuth : function() {
                    var self = this;
                    axios.post('/auth/mobile/result', this.mobileRequest)
                        .then(function(response){
                            clearTimeout(self.timer);
                            self.timerStart = false;
                            if(response.data) {
                                self.authMobileConfirm = true;
                                self.mobileAuthCheck = true;
                                self.resultText = "휴대폰 번호 인증이 완료되었습니다."
                            } else {
                                self.resultText = "인증번호가 유효하지 않습니다."
                            }
                        });
                },
                confirm : function() {
                    var self = this;
                    
                    if(this.save.mobileNo == "") {
                        noti.open("휴대폰 번호를 입력해 주세요.");
                        return;
                    }
                    if(!self.authMobileConfirm) {
                        noti.open("휴대폰 인증이 완료되지 않았습니다.");
                        return;
                    }

                    this.$validator.validateAll()
                        .then(function(sucess) {
                            if(!sucess) return;
                            axios.post('/data/crowdy/pre-project/save', self.save)
                                .then(function(response) {
                                    var result = response.data.rCode;
                                    if(result == "0000") {
                                        $('#prereserve').modal('hide');
                                        // gtm 사전예약 신청 이벤트 발생
                                        window.dataLayer.push({
                                            'event': 'PreOpenComplete'
                                        });
                                        noti.open("신청되었습니다.")
                                        self.initData();
                                    }
                                })
                        })
                    
                },
                update : function() { //맴버테이블 mobileno 업데이트(x)
                    var self = this;
                    this.$validator.validateAll()
                        .then(function(sucess) {
                            if(!sucess) return;

                            axios.post('/data/crowdy/pre-project/update', self.save)
                                .then(function(response) {
                                    noti.open("정보가 변경되었습니다.")
                                    self.initData();
                                })
                        })
                },
                dismiss : function() {
                    $('#prereserve').modal('hide');
                    this.initData();
                    this.load();
                },
                //본인인증 인증
                authMobile : function() {
                    this.authChange = false;
                    axios.post('/auth/nice/M')
                    .then(function(response){
                        $('#encodeData').val(response.data.rData.data)
                        window.open('', 'popupChk', 'width=500, height=800, top=100, left=100, fullscreen=no, menubar=no, status=no, toolbar=no, titlebar=yes, location=no, scrollbar=no');
                        document.form_chk.action = "https://nice.checkplus.co.kr/CheckPlusSafeModel/checkplus.cb";
                        document.form_chk.target = "popupChk";
                        document.form_chk.submit();
                    });
                },
                authResult : function(result) {
                    if(result) {
                        var self = this;
                        axios.post('/auth/info')
                        .then(function(response) {
                            self.save.mobileNo = response.data.mobileNo;
                            self.authChange = true;
                        });
                    } else {
                        noti.open('본인인증에 실패했습니다 <br/> 다시시도해주세요.')
                    }
                },
                countdown : function(callback) {
                    var endTime, hours, mins, msLeft, time;
                    var minutes = 2;
                    var seconds = 59;
                    var self = this;

                    function twoDigits(n) {
                        return (n <= 9 ? "0" + n : n);
                    }

                    function updateTimer() {
                        if(self.isCntdownRestart){
                            self.time = "";
                            self.isCntdownRestart = false;
                            clearTimeout(self.timer);
                            minutes = 2;
                            seconds = 59;
                            endTime = (+new Date) + 1000 * (60 * minutes + seconds) + 500;
                        }

                        msLeft = endTime - (+new Date);
                        
                        if ( msLeft < 1000 ) {
                            callback();
                        } else {
                            time = new Date( msLeft );
                            hours = time.getUTCHours();
                            mins = time.getUTCMinutes();
                            self.time = (hours ? hours + ':' + twoDigits(mins) : mins) + ':' + twoDigits(time.getUTCSeconds())
                            self.timer = setTimeout(updateTimer, time.getUTCMilliseconds() + 500);
                        }
                    }
                    endTime = (+new Date) + 1000 * (60* minutes + seconds) + 500;
                    updateTimer();

                },
                countdownTimeOut : function() {
                    this.timeOut = true;
                    this.timerStart = false;
                }
            }
        }
    }
}
export default new preOpen()