class Banner {
	component() {
		return {
			template : `
				<div id="bannerCarousel" class="carousel slide main_slider_wrap mb20 xs-mb10">
				
					<div class="carousel-indicator">
						<span class="arrow arrow-left ml3 hidden-xs" v-on:click="indicatorClick('prev')"></span>
						<span class="arrow arrow-right ml13 hidden-xs" v-on:click="indicatorClick('next')"></span>
						<span class="banner-count"> <span class="color-white">{{ currentIndex }}</span> / {{ bannerCount }} </span>
					</div>

					<div class="carousel-inner" role="listbox">
						<a v-for="(item, index) in banner" 
							class="item" 
							:class="{active : index == 0}"  
							:style="{'background-image' : 'url(//'+ item.bannerImg +')'}" 
							v-on:click="bannerClick(item.bannerUrl, item.bannerUrlType)"
							>
							<div class="carousel-caption webfont2">
								<div style="display: inline-flex;">
									<p class="font-blue type-text custom-mr">
										{{ type(item.bannerType) }}
									</p>
									<p class="font-blue type-text">
										{{ badge(item.bannerBadge) }}
									</p>
								</div>
								<h2 :style="{'color' : color(item.bannerColor)}" v-html="convert(item.bannerTitle)"></h2>
								<p :style="{'color' : color(item.bannerColor)}" class="caption-description">
									{{ item.bannerText }}
								</p>
							</div>
						</a>
					</div>					
				</div>
			`,
			data : function() {
				return {
					banner : [],
					bannerCount : 1,
					currentIndex : 1,
				}
			},
			created : function() {
        		this.load();
    		},
			methods : {
				load : function() {
    				var self = this;
    				axios.post('/data/crowdy/banner/list', {})
    					.then(function(response){
    						self.banner = response.data.rData;
    						self.bannerCount = response.data.rData.length;

    						self.$nextTick(function() {
    							$('#bannerCarousel').carousel({
								  interval: 5000,
								  cycle: true
								})

								$('#bannerCarousel').hammer({velocity: 0.1, threshold: 10, posThreshold:50, time:1000}).on('swipeleft', function(){
									$('#bannerCarousel').carousel('next');
								})
								$('#bannerCarousel').hammer({velocity: 0.1, threshold: 10, posThreshold:50, time:1000}).on('swiperight', function(){
									$('#bannerCarousel').carousel('prev');
								})
								
								$('.carousel').on('slid.bs.carousel', function () {
									self.currentIndex = $('div.carousel-inner a.active').index() + 1;
								});

								// $(".main_slider_wrap .carousel-inner .item").css("background-size", $( document ).width()+"px");
    						});
    						
    					})
    			},
    			bannerClick : function(url, type) {
    				//1:프로젝트 링크, 2:외부 링크
    				var targetValue = '_self';
    				if(type == 2) {
    					targetValue = '_blank';
    				}
    				if(url.indexOf("http://") < 0 && url.indexOf("https://") < 0) {
                    	url = 'http://' + url;
                    }

    				window.open(url, targetValue);
    			},
    			indicatorClick : function(type) {
    				$('#bannerCarousel').carousel(type);
    			},
    			//1: 검정, 2: 흰색
    			color : function(type) {
    				if (type == '1') {
    					return '#464646'
    				} else {
    					return '#ffffff'
    				}
    			},
                convert : function(data) {
					return data.replace( /[\n]/g, "<br/>")
				},
				//배너 뱃지 (0 : 없음, 1: 선착순 배정, 2 : 금액순 배정, 3: 기타배정)
				badge : function(type) {
					if (type == '1') {
    					return '선착순 배정';
    				} else if (type == '2') {
    					return '금액순 배정';
    				} else if (type == '3') {
    					return '기타배정';
    				} else if (type == '0' || type == '') {
    					return '';
    				}
				},
				//1: 리워드, 2: 투자, 3: 공지, 4: 이벤트'
				type : function(type) {
					if (type == '1') {
    					return '리워드';
    				} else if (type == '2') {
    					return '투자';
    				} else if (type == '3') {
    					return '공지';
    				} else if (type == '4') {
    					return '이벤트';
    				}
				}
			}
		}
	}
}

export default new Banner()