class Term {
	component() {
		return {
			template : `
				<div class="common_sub_layout">
					<div class="common_sub_top_menu webfont2">
							<div class="hidden-xs" style="border-bottom: 1px solid #ddd;">
	                            <a href="javascript:void(0)" :class="{'active' : menu == 1}" v-on:click="menuChange('1')"><span class="terms_sub_menu">회원가입 기본약관</span></a>
	                            <a href="javascript:void(0)" :class="{'active' : menu == 2}" v-on:click="menuChange('2')"><span class="terms_sub_menu">CROWDY 이용약관(리워드)</span></a>
	                            <a href="javascript:void(0)" :class="{'active' : menu == 3}" v-on:click="menuChange('3')"><span class="terms_sub_menu">CROWDY 이용약관(투자)</span></a>
	                            <a href="javascript:void(0)" :class="{'active' : menu == 4}" v-on:click="menuChange('4')"><span class="terms_sub_menu">개인정보보호정책(리워드)</span></a>\
	                            <a href="javascript:void(0)" :class="{'active' : menu == 5}" v-on:click="menuChange('5')"><span class="terms_sub_menu">개인정보보호정책(투자)</span></a>
							</div>
						<div class="container">
							<div class="hidden-sm hidden-md hidden-lg">
								<menu-select :options="menuOptions" v-model="menu"></menu-select>
							</div>

							<div class="tab-content" v-if="dataConfirm">
								<div role="tabpanel" class="tab-pane mce-content-body active" v-html="content" style="text-align: left;"></div>
							</div>
						</div>
					</div>
				</div>
			`,
			props : ['termMenu'],
			data : function() {
				return {
					dataConfirm : false,
					menu : this.termMenu,
					menuOptions : [
						{
                            id : "1",
                            text : "회원가입 기본약관"
                    	},
                    	{
                            id : "2",
                            text : "CROWDY 이용약관(리워드)"
                    	},
                    	{
                            id : "3",
                            text : "CROWDY 이용약관(투자)"
                    	},
                    	{
                            id : "4",
                            text : "개인정보보호정책(리워드)"
                    	},
                    	{
                            id : "5",
                            text : "개인정보보호정책(투자)"
                    	},
					],
					terms : []
				}
			},
			components : {
				menuSelect : require('../common/select.js').default.component()
			},
			computed : {
				content : function() {
					var data = _.find(this.terms, {'type' : this.menu})
					return data ? data.content : ''
				}
			},
			created : function() {
				var self = this;
				axios.post('/data/crowdy/term', {})
					.then(function(response) {
						var result = response.data;
						self.terms = result.rData;
						self.dataConfirm = true;
					})
			},
			methods : {
				menuChange : function(menu) {
                    this.menu = menu;
                }
			}

		}
	}
}

export default new Term()