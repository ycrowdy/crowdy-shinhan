class Help {
	component() {
		return {
			template : `
				<div>
					<div class="dinvestNavWrapXs hidden-md hidden-lg">
						<div class="dinvestNav">
							<ul class="nav m-mb10">
								<li :class="{'active' : index == 0}" v-for="(item, index) in faqName">
									<a href="javascript:void(0)" data-toggle="collapse" :data-target="'#help_' + index" :aria-controls="'help_' + index" aria-expanded="false" aria-label="Toggle navigation">
										{{ item.faqName }}<span class="caret"></span>
									</a>
									<div class="collapse dinvestNav-collapse" :class="{'in' : index == 0}" :id="'help_' + index" aria-expanded="true">
										<ul class="nav">
											<li v-for="(faqItem, faqIndex) in faqList(item.faqTypeCode)">
												<a :href="'#help_' + index + '_' + faqItem.faqIdx">{{ faqItem.faqTitle }}</a>
											</li> 
										</ul>
									</div>
								</li>
							</ul>
						</div>
					</div>

					<div class="sideOpenMask"></div>

					<div class="detail_wrap">
						<div class="container">
							<div class="row row-mobile-n">
								<div class="col-md-9">
									<div class="help_line">
										<template v-for="(item, index) in faqName">
											<section v-for="(faqItem, faqIndex) in faqList(item.faqTypeCode)" class="help_wrap_contents jcorgFilterTextParent" :id="'help_' + index + '_' + faqItem.faqIdx">
												<h3 class="webfont2 jcorgFilterTextParent" v-if="faqIndex == 0">{{ item.faqName }}</h3>
												<h4 class="webfont2 jcorgFilterTextParent">{{ faqItem.faqTitle}}</h4>
												<div class="jcorgFilterTextChild mce-content-body" v-html="faqItem.faqContent"></div>
											</section>
										</template>
									</div>
								</div>
								<div class="col-md-3">
									<div class="dinvestNavWrap hidden-xs hidden-sm">
										<div class="dinvestNav">
											<ul class="nav m-mb10">
												<li :class="{'active' : index == 0}" v-for="(item, index) in faqName">
													<a href="javascript:void(0)" data-toggle="collapse" :data-target="'#help_' + index" :aria-controls="'help_' + index" aria-expanded="false" aria-label="Toggle navigation">
														{{ item.faqName }}<span class="caret"></span>
													</a>
													<div class="collapse dinvestNav-collapse" :id="'help_' + index" aria-expanded="true">
														<ul class="nav">
															<li v-for="(faqItem, faqIndex) in faqList(item.faqTypeCode)">
																<a :href="'#help_' + index + '_' + faqItem.faqIdx">{{ faqItem.faqTitle }}</a>
															</li> 
														</ul>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="help_quick_navi">
						<button class="btn-help-navi" type="button">NAVI</button>
					</div>

				</div>
				
			`,
			data : function() {
				return {
					dataConfirm : false,
					faqName : [],
					faq : []
				}
			},
			created : function() {
				this.load()
			},
			methods : {
				load : function() {
					var self = this;
                	axios.post('/data/crowdy/faq/list', {})
	                    .then(function(response){
	                    	var result = response.data.rData;
	                    	self.faqName = result.faqType;
	                    	self.faq = result.faq;
	                    	self.dataConfirm = true;
	                    	self.$nextTick(function() {
						    	self.animateLoad()
						    });

	                    })
				},
				faqList : function(code) {
					return _.filter(this.faq, function(o) { return o.faqType == code });
				},
				animateLoad : function() {
					$('.btn-help-navi').click(function() {
						$('.dinvestNavWrapXs').animate({
							right: "0px"
						}, 200);

						$('.page-wrapper').animate({
							right: "285px"
						}, 200);

						$('.page-wrapper').addClass('sideOpen');
						$('body').addClass('sideOpen');
					});
					
					$('.sideOpenMask, .dinvestNav_m_close').click(function() {
						$('.dinvestNavWrapXs').animate({
							right: "-285px"
						}, 200);

						$('.page-wrapper').animate({
							right: "0px"
						}, 200);

						$('.page-wrapper').removeClass('sideOpen');
						$('body').removeClass('sideOpen');
					});

					var MOBILE_WIDTH = 992;
					$('.dinvestNavWrap').affix({
						offset: {
							top: 224,
							bottom: function () {
								return (this.bottom = $('footer').outerHeight(!0) + 440)
							}
						}
					})

					if ($('.dinvestNavWrap').length ) {
						if(window.location.hash) {
							$('.dinvestNavWrap ul li a[href=\"' +window.location.hash + '\"]').click();
						}

						$('.dinvestNavWrap ul li').removeClass('active');
						$('.dinvestNavWrap ul li a[data-toggle*="collapse"]').on( "click", function() {
								$('.dinvestNavWrap ul li').removeClass('active');
								$(this).parent('li').addClass('active');
								$(".help_wrap_contents jcorgFilterTextParent").removeClass('active');
								$('.dinvestNav-collapse').removeClass('in').next();
								$(this.hash).addClass('active');
						});

						$('.dinvestNavWrap ul li a[href*="#"]').on('click', function () {
							$('.jcorgFilterTextParent').removeClass('active');
							$(this.hash).addClass('active');
							var target = $(this.hash);
							if ( $(window).width() < MOBILE_WIDTH ){
								offset_topscroll = 40
							}
							target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
							if (target.length) {
								$('html, body').animate({
								  scrollTop: (target.offset().top - 40)
								}, 1000, "easeInOutExpo");
								return false
							}
							return false
						})
					}
					

					if ($('.dinvestNavWrapXs').length ) {
						if(window.location.hash) {
							$('.dinvestNavWrap ul li a[href=\"' +window.location.hash + '\"]').click();
						}
						$('.dinvestNavWrapXs ul li').removeClass('active');
						$('.dinvestNavWrapXs ul li a[data-toggle*="collapse"]').on( "click", function() {
							$('.dinvestNavWrapXs ul li').removeClass('active');
							$(this).parent('li').addClass('active');
							$(".help_wrap_contents jcorgFilterTextParent").removeClass('active');
							$('.dinvestNav-collapse').removeClass('in').next();
							$(this.hash).addClass('active');
						});

						$('.dinvestNavWrapXs ul li a[href*="#"]').on('click', function () {
							$(this.hash).addClass('active');

							 $('.dinvestNavWrapXs').animate({
								right: "-285px"
							}, 200);

							$('.page-wrapper').animate({
								right: "0px"
							}, 200);

							$('.page-wrapper').removeClass('sideOpen');
							$('body').removeClass('sideOpen');

							var target = $(this.hash);
							if ( $(window).width() < MOBILE_WIDTH ){
								offset_topscroll = 90
							}
							target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
							if (target.length) {
								$('html, body').animate({
								  scrollTop: (target.offset().top - 70)
								}, 1000, "easeInOutExpo");
								return false
							}
							return false
						})
					}
					

					if ($('.dinvestNavWrap').length ) {
						$('body').scrollspy({
							target: '.dinvestNavWrap',
							offset: 60
						})
					}

					if ($('.dinvestNavWrapXs').length ) {
						$('body').scrollspy({
							target: '.dinvestNavWrapXs',
							offset: 60
						})
					}

				}
			}
		}
	}
}

export default new Help()