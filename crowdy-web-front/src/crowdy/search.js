class search {
	component() {
		return {
			template : `
                <div>
                    <!-- 검색결과 -->
                    <div id="list_wrap" class="list_wrap">
                        <div class="container">
                            <div class="list_title list_title_big webfont2">
                                {{projectCount}}개의 검색결과
                                <div class="list_title_search">검색어 "{{search.title}}"</div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="rewards-list">
                                <div class="row row-mobile">
                                    <!-- Loop -->
                                    <div class="col-sm-6 col-md-4" v-for="item in searchResult" v-on:click="detail(item)">
                                        <figure>
                                            <a href="javascript:void(0)" class="items over-box">
                                                <div class="items_img">
                                                    <div class="badge_icon badge_icon_event" v-if="item.type == '1' && item.trainingYn == 'N'">리워드</div>
                                                    <div class="badge_icon badge_icon_event" v-if="item.type == '1' && item.trainingYn == 'Y'">모의펀딩</div>
                                                    <div class="badge_icon" v-if="item.type == '2'">투자</div>
                                                    <img :src="'//' + item.img + '/ycrowdy/resize/!740x!492'" class="img-responsive" alt="..." />
                                                </div>
                                                <figcaption class="rewards-caption">
                                                    <div class="rewards-subject">
                                                        <strong>{{item.title}}</strong>
                                                        <div class="rewards-summury">{{item.memName}}</div>
                                                    </div>
                                                    <div class="row not-space">
                                                        <div class="col-xs-9 col-sm-8">
                                                            <span class="rewards-price"><span class="webfont2">&#65510;</span>{{parseInt(item.sponsorAmount).toLocaleString()}}</span> <span class="rewards-percent">{{item.achievement}}%</span>
                                                        </div>
                                                        <div class="col-xs-3 col-sm-4 text-right">
                                                            <template>
                                                                <span v-if="item.endStatus != '0' && item.endStatus != '1' " class="rewards-day rewards-day-end">종료</span>
                                                                <span v-else  class="rewards-day">D-{{getdDay(item.endDate)}}</span>
                                                            </template>
                                                        </div>
                                                    </div>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="max-width: 100%;" :style="{ width: item.achievement + '%'}"><span class="sr-only">{{item.achievement}}% 완료</span></div>
                                                    </div>
                                                </figcaption>
                                            </a>
                                        </figure>
                                    </div>
                                    <!-- //Loop -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- //검색결과 -->
                </div>
			`,
            props : {
                title : {
                    default : ""
                },
            },
			data : function() {
				return {
					searchResult : [],
                	search : {
	                    title : this.title,
                    },
                    projectCount : 0,
                    dataConfirm : false,
                    searchTitle : ''
				}
            },
			created : function() {
                this.searchTitle = this.title;
                this.load();
            },
            methods : {
                load : function() {
                    
                    if(this.title == '') {
                        this.dataConfirm = true;
                        return;   
                    }

                    var self = this;

                    this.searchResult = [];
                    $('.page-loader-more').fadeIn('')
                    axios.post('/data/crowdy/search/project', { 'title' : this.title })
                    .then(function(response){
                        $('.page-loader-more').fadeOut('')
                        self.searchResult = response.data.rData;
                        self.projectCount = self.searchResult.length;
                        self.dataConfirm = true;
                    })
                },
                //D-day구하기
                getdDay : function(endDate) {
                        var end = moment(endDate)
                        var now = moment(new Date());
                        var duration = end.diff(now, 'days');
                        return duration;
                },
                //프로젝트 상세로 이동
                detail : function(data) {
                    if(data.type == '1' && data.trainingYn == 'N') {
                        window.open('/r/' + data.aliasUrl, '_self')
                    } else if(data.type == '2' && data.trainingYn == 'N') {
                        window.open('/i/' + data.aliasUrl, '_self')
                    } else if(data.type == '1' && data.trainingYn == 'Y') {
                        window.open('/sc/' + data.aliasUrl, '_self')
                    }
                },
                searchKeyWord : function() {
                    this.title = this.searchTitle;
                    this.load();
                }
			}
		}
	}
}

export default new search()