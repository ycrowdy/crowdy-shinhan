class Main {
	component() {
		return {
			template : `
			<div>
				<banner></banner>

				<div class="main-box-wrap">
	                <div class="container">
	                    <div class="main-box">
	                        <div class="title">
	                            <div class="title-block">
	                                <span class="main-title">주목받는 리워드</span>
	                                <a href="/reward/list" class="title-line"><span class="main-title-description">크라우디에서 핫한 프로젝트들을 만나보세요</span><i class="fa fa-angle-right hidden-md hidden-lg" aria-hidden="true"></i></a>
	                            </div>
	                        </div>
	                        <reward :param-search-count="4" :param-end-type="0"></reward>
	                    </div>
	                </div>
	            </div>

				<!-- 새로운 리워드 -->
                <new-reward :param-search-count="4"></new-reward>

            	<div class="main-box-wrap">
	                <div class="container">
	                    <div class="main-box">
	                        <div class="title">
	                            <div class="title-block">
	                                <span class="main-title">투자 프로젝트</span>
	                                <a href="/invest/list" class="title-line"><span class="main-title-description">크라우디에서 핫한 프로젝트들을 만나보세요</span><i class="fa fa-angle-right hidden-md hidden-lg" aria-hidden="true"></i></a>
								</div>
							</div>
							<invest v-show="investCnt != 0" param-end-type="0" v-on:invest-list-cnt="icnt"></invest>

							<template v-if="investCnt == 0">
			                    <div style="text-align: center;" class="pre_open_message">
			                        <h4 class="main_pre_open_notice">프로젝트가 곧 <span class="blue-800">오픈예정</span>입니다</h4>
			                    </div>
			                </template>

	                    </div>
	                </div>
	            </div>
				
				<div class="main-box-wrap">
	                <div class="container">
	                    <div class="main-box">
	                        <div class="title">
	                            <div class="title-block">
	                              <span class="main-title">프로젝트 사전공개</span>
	                              <a href="/" class="title-line"><span class="main-title-description">곧 오픈할 프로젝트들을 가장 먼저 만나보세요</span></a>
	                            </div>
	                        </div>
	                        <pre-open></pre-open>
	                    </div>
	                </div>
	            </div>
				
				<div class="main-box-wrap">
	                <div class="container">
	                    <div class="main-box">
	                        <crowdy-news></crowdy-news>
	                    </div>
	                </div>
	            </div>
				
				<div class="main-box-wrap">
	                <div class="container">
	                    <div class="main-box">
	                        <div class="title">
	                            <div class="title-block">
	                                <span class="main-title">크라우디 매거진 </span>
	                                <a href="/" class="title-line"><span class="main-title-description">크라우디가 제공하는 유용한 정보들을 만나보세요</span></a>
	                            </div>
	                        </div>
	                        <magazine></magazine>
	                    </div>
	                </div>
	            </div>
				
				<div class="main-box-wrap main-partner">
	                <div class="container">
	                    <div class="main-box">
	                        <div class="title">
	                            <div class="title-block">
	                                <span class="main-title">커뮤니티 파트너</span>
	                                <a href="/community/main" class="title-line"><span class="main-title-description">크라우디는 여러분과 함께 합니다</span><i class="fa fa-angle-right hidden-md hidden-lg" aria-hidden="true"></i></a>
	                            </div>
	                        </div>
	                        <community-banner></community-banner>
	                    </div>
	                </div>
	            </div>

			</div>
			`,
			data : function() {
				return  {
					investCnt : 0,
					dataConfirm : false
				}
			},
			components : {
				banner : require('./banner.js').default.component(),
                reward : require('../reward/reward-list.js').default.component(),
                newReward : require('../reward/reward-new-list.js').default.component(),
                invest : require('../invest/invest-main-list.js').default.component(),
                preOpen : require('./pre-open-list.js').default.component(),
                communityBanner :  require('../community/community-banner.js').default.component(),
                crowdyNews : require('./main-news.js').default.component(),
                magazine : require('./crowdy-magazine.js').default.component(),
			},
			methods : {
				icnt : function(cnt) {
					this.investCnt = cnt;
				},
			}
		}
	}
}

export default new Main()
