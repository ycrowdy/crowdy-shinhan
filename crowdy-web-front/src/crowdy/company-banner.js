class companyBanner {
	component() {
		return {
			template : `
				<div id="company-list" 
					class="pb30 md-pb0"
					:class="{'xs-pb10' : pc}">
					<figure v-for="(item, index) in company">
						<div class="bbs-img">
							<img :src="'//' + item.companyImg" class="img-responsive" alt="" />
						</div>
					</figure>
				</div>
			`,
			props : {
				pc : {
					default : true
				}
			},
			data : function() {
				return {
					company : []
				}
			},
			created : function() {
				this.load()
			},
			updated : function() {
				$("#company-list").slick({
                        dots: false,
                        infinite: true,
                        autoplay: true,
                        autoplaySpeed: 3000,
                        slidesToShow: 4,
                        slidesToScroll: 4,
                        responsive: [
                            {
                              breakpoint: 1050,
                              settings: {
                                dots: true,
                                slidesToShow: 4,
                                slidesToScroll: 4
                              }
                            },
                            {
                              breakpoint: 767,
                              settings: {
                                dots: true,
                                slidesToShow: 3,
                                slidesToScroll: 3
                              }
                            },
                            {
                              breakpoint: 477,
                              settings: {
                                dots: true,
                                slidesToShow: 2,
                                slidesToScroll: 2
                              }
                            }
                        ]
                    });
			},
			methods : {
				load : function() {
					var self = this;
					axios.post('/data/crowdy/banner/company-list', {})
	                    .then(function(response){
	                    	if(response.data.rData.length > 0) {
	                    		self.company = _.concat(self.company, response.data.rData);
	                    	}
	                    })
				}
			}
		}
	}
}

export default new companyBanner()