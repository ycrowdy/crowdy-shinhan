class CrowdyMagazine {
	component() {
		return {
			template : `
				<div class="mt15 m-mt20 xs-mt20">
					<div class="row row-mobile">
						<div class="col-xs-6 col-sm-3 list_card_magazine_height_parent" v-for="(item, index) in data">
							<a href="#" v-on:click="bannerClick(item.newsUrl, item.newsType)" class="bbs-caption over-box list_card_magazine_height">
								<div class="bbs-img">
									<img :src="'//' + item.newsImage + '/ycrowdy/resize/!370x!246'" class="img-responsive" />
								</div>
								<div class="bbs-subject" v-text="item.newsTitle"></div>
								<div class="bbs-summury" v-text="item.newsText"></div>
							</a>
						</div>
					</div>

					<div v-show="moreShow">
						<a href="javascript:void(0)" v-on:click="more()" class="btn btn-primary-outline col-sm-8 col-md-6 col-xs-12 col-sm-offset-2 col-md-offset-3 btn-more text-center mt50 mb20 md-mt65 md-mb0 m-mt50 m-mb20 xs-mt20 xs-mb10">매거진 더보기</a>
                    </div>
				</div>
			`,
			props : {
	            paramSearchCount : {
	                default : "4"
	            }
	        },
			data : function() {
				return {
					data : [],
					search : {
						paging : {
	                        page : "1",
	                        count : this.paramSearchCount
                    	}
					},
					moreShow : true
				}
			},
			created : function() {
				this.load()
			},
			methods : {
				load : function() {
					var self = this;
	                axios.post('/data/crowdy/news/magazine', self.search)
	                    .then(function(response){
							self.data = _.concat(self.data, response.data.rData);
							if(response.data.rData.length != parseInt(self.search.paging.count)) {
                            	self.moreShow = false;
                        	}
	                    })
				},
				more : function() {
                    this.search.paging.page  = _.toString(_.add(_.toNumber(this.search.paging.page), 1));
                    this.load();
                },
                bannerClick : function(url, type) {
    				var targetValue = '_blank';
    				if(type == 1) {
    					targetValue = '_self';
    				}
    				window.open(url, targetValue);
    			}
			}
		}
	}
}

export default new CrowdyMagazine()