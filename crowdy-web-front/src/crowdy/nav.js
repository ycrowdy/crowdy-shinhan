class Nav {
	component() {
		return {
			template : `
			<div>
				<nav class="navbar navbar-venture navbar-fixed-top affix">
				<!-- css : affix -->
					<!-- 상단 팝업 -->
					<!--<div class="top-popup">
						<div class="container">
							<div class="top-popup-contents-text text-center">
								변경된 약관을 확인하세요! <a href="/crowdy/term?menu=1">약관 확인하기 -> </a>
								<a href="javascript:void(0)" title="창닫기" class="top-popup-close">닫기</a>
							</div>
						</div>
					</div> -->
					<!-- //상단 팝업 -->

					<div class="container">
						
						<template v-if="!searching">
							<div class="navbar-header">
								<a href="/" class="navbar-brand"><div>CROWDY</div></a>
							</div>
							<div id="navbar" class="navbar-collapse collapse">
								<ul class="nav navbar-nav">
									<li><a href="/reward/list" class="dropdown-toggle">리워드</a></li>
									<li><a href="/invest/list" class="dropdown-toggle">투자</a></li>
									<li class="dropdown">
										<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
											더보기 <span class="caret"></span>
										</a>
										<div class="dropdown-menu" role="menu">
											<ul>
												<li><a href="/community/main">커뮤니티 파트너</a></li>
												<li><a href="/simulation/main">모의펀딩</a></li>
												<li><a href="/crowdy/about">리워드/투자란?</a></li>
												<li><a href="/crowdy/help" target="_blank">도움말</a></li>
											</ul>
										</div>
									</li>
									<li><a href="/make/start" class="dropdown-toggle dropdown-pro">프로젝트 만들기</a></li>
								</ul>
								<ul class="nav navbar-nav navbar-right">
									<li class="nav-search-form">
										<form action="javascript:void(0);">
											<div class="input-group">
												<!-- <search-input style="cursor: pointer;" placeholder="키워드를 입력하세요."></search-input> -->
												<span class="input-group-addon" v-on:click="searchConfirm">검색</span>
											</div>
										</form>
									</li>
									<template v-if="check">
									<li>
										<a href="#" class="dropdown-toggle open_my_card" v-on:click="gnbCardOpen">
											마이페이지
										</a>
									</li>
									</template>
									<template v-else>
									<li>
										<a href="javascript:void(0)" class="dropdown-toggle" v-on:click="login">
											로그인
										</a>
									</li>
									<li>
										<a href="javascript:void(0)" class="dropdown-toggle" v-on:click="join">
											회원가입
										</a>
									</li>	
									</template>
									
									<li>
										<a href="/crowdy/help" class="dropdown-toggle" target="_blank">
											도움말
										</a>
									</li> 
								</ul>
							</div>
						</template>
						<template v-if="searching">
							 <div class="input-group input-group-file margin-top-10">
							 	<span class="input-group-btn">
                                    <span class="btn btn-outline btn-file btn-search">
                                    </span>
                                </span> 
                                <input type="text" placeholder="찾으시는 프로젝트가 있으신가요?" class="form-control border-none search-box" v-model="keyWord" v-on:keyup.enter="searchKeyWord" autofocus> 
                                <span class="input-group-btn">
                                    <span class="btn btn-outline btn-file btn-exit" v-on:click="searchConfirm">
                                    </span>
                                </span> 
                            </div>
						</template>
					</div>
				</nav>

				<nav class="mobile-header-wrap">

					<!-- 상단 팝업 -->
					<!--<div class="top-popup">
						<div class="container">
							<div class="top-popup-contents-text text-center">
								변경된 약관을 확인하세요! <br/> <a href="/crowdy/term?menu=1">약관 확인하기 -> </a>
								<a href="javascript:void(0)" title="창닫기" class="top-popup-close">닫기</a>
							</div>
						</div>
					</div> -->
					<!-- //상단 팝업 -->

					
					<template v-if="!searching">
						<div class="mobile-header">
							<div class="nav-search-form">
								<form action="javascript:void(0);">
									<div class="input-group">
										<span type="text" class="form-control" v-on:click="searchConfirm"></span>
									</div>
								</form>
							</div>

							<a href="/" class="mobile-brand">CROWDY</a>
							<a href="#" class="mobile-icon open_my_card" v-on:click="gnbCardOpen">
								<span>마이페이지</span>
							</a>	
						</div>
					</template>

					<template v-if="searching">
						<div class="mobile-header">
							 <div class="input-group input-group-file">
							 	<span class="input-group-btn">
                                    <span class="btn btn-outline btn-file btn-search">
                                    </span>
                                </span> 
                                <input type="text" placeholder="찾으시는 프로젝트가 있으신가요?" class="form-control border-none search-box" v-model="keyWord" v-on:keyup.enter="searchKeyWord" autofocus> 
                                <span class="input-group-btn padding-right-10">
                                    <span class="btn btn-outline btn-file btn-exit" v-on:click="searchConfirm">
                                    </span>
                                </span> 
                            </div>	
                        </div>
					</template>

					<div class="mobile-navbar">
						<ul class="mobile-nav">
							<li><a href="/reward/list" class="mobile-anav">리워드</a></li>
							<li><a href="/invest/list" class="mobile-anav">투자</a></li>
							<li class="dropdown">
								<a href="javascript:void(0);" class="mobile-anav" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
									더보기 <span class="caret"></span>
								</a>
								<div class="dropdown-menu" role="menu">
									<ul>
										<li><a href="/community/main">커뮤니티 파트너</a></li>
										<li><a href="/simulation/main">모의펀딩</a></li>
										<li><a href="/crowdy/about">리워드/투자란</a></li>
										<li><a href="/crowdy/help" target="_blank">도움말</a></li>
										<li><a href="/make/start">프로젝트 만들기</a></li>
									</ul>
								</div>
							</li>
						</ul>
					</div>
				</nav>

				<div class="gnb_card">
					<div class="gnb_card_frame">
						<div class="gnb_card_header">
							<div class="gnb_card_header_table">
								<div class="gnb_card_header_table_cell">
									<a href="javascript:void(0)" class="gnb_card_close">창닫기</a>
									<div class="user_photo">
										<img v-if="image != ''" :src="'//' + image" class="img-responsive" />
									</div>
									<div class="user_name" v-text="name">
									</div>
									<div class="user_email" v-text="email">
									</div>
								</div>
							</div>
						</div>
						<div class="gnb_card_body">
							<ul>
								<template v-if="check">
									<li><a href="#" v-on:click="loadInvestState(1)">펀딩한 프로젝트</a></li>
									<li><a href="/mypage/main?menu=2">제작한 프로젝트</a></li>
									<li><a href="/mypage/main?menu=3">관심 프로젝트</a></li>
									<li><a href="/mypage/main?menu=5">설정</a></li>
									<li><a href="javascript:void(0);" v-on:click="logout">로그아웃</a></li>
								</template>
								<template v-else>
									<li><a v-on:click="login">로그인</a></li>
									<li><a v-on:click="join">회원가입</a></li>	
								</template>
							</ul>
						</div>
					</div>
				</div>

			</div>
			`,
			props : ['check'],
			data : function() {
                return {
					keyWord : "",
					searching : false
				}
			},
			components : {
                searchInput : require('../common/search-input.js').default.component()
            },
			created : function() {

				$(document).ready(function(){
					$(".top-popup-close").click(function(){
						$(".top-popup").addClass('top-popup-hidden');
					});
				});

				window.userInfo = this;

				// if (this.check) {
				// 	var self = this;
				// 	window.dataLayer.push({'uid': self.memCode, 'CD1': self.memCode});	
				// }

			},
			computed : {
				name : {
					cache: false,
					get : function() {
						return this.check ? JSON.parse(localStorage.getItem('user')).memName : ''
					}
				},
				email : {
					cache: false,
					get : function() {
						return this.check ? JSON.parse(localStorage.getItem('user')).memEmail : ''
					}
				},
				image : {
					cache: false,
					get : function() {
						return this.check ? JSON.parse(localStorage.getItem('user')).memShotImg : ''
					}
				},
				memCode : {
					cache: false,
					get : function() {
						return this.check ? JSON.parse(localStorage.getItem('user')).memCode : ''
					}
				},
				type : {
					cache: false,
					get : function() {
						return this.check ? JSON.parse(localStorage.getItem('user')).memType : ''
					}
				},
				investorType : {
					cache: false,
					get : function() {
						return this.check ? JSON.parse(localStorage.getItem('user')).memInvestorTypeName : ''
					}
				},
				investorState : {
					cache: false,
					get : function() {
						return this.check ? JSON.parse(localStorage.getItem('user')).memInvestor : ''
					}
				},
				nameConfirm : {
					cache: false,
					get : function() {
						return this.check ? JSON.parse(localStorage.getItem('user')).memNameConfirm : ''
					}
				},
			},
			methods : {
				logout : function() {
					localStorage.clear();
					window.open("/logout", '_self');
				},
				login : function() {
					localStorage.setItem('return', window.location.pathname + window.location.search);
					window.open("/user/login", '_self');
				},
				join : function() {
					localStorage.setItem('return', window.location.pathname + window.location.search);
					window.open("/user/join", '_self');
				},
				loginConfirm : function() {
					if(this.memCode == '') {
						noti.open('로그인이 필요한 서비스입니다.<br/> 로그인 후 이용하세요.', function() {userInfo.login();})
						return false;
					}
					return true;
				},
				updateInfo : function(data) {
					localStorage.clear();
					localStorage.setItem('user', JSON.stringify(data));
				},
				setLoginInfo : function(value) {
					localStorage.setItem('user', JSON.stringify(value));
				},
				loadInvestState : function(menu) {
                    axios.post('/data/member/investor/state', {memCode : userInfo.memCode})
                        .then(function(response){
                            if(response.data.rData.investor == 'Y') {
                            	window.open("/mypage/main?menu=" + menu + "&sub-menu=2", '_self')
                            } else {
                            	window.open("/mypage/main?menu=" + menu, '_self')
                            }
                        })
                },
                searchConfirm : function() {
                	this.searching = !this.searching;
                },
                searchKeyWord : function() {
					this.keyWord = this.keyWord.replace(/^\s+|\s+$/g,"")
					var encodeURL = encodeURI('/crowdy/search?title=' + this.keyWord);
					window.open(encodeURL, '_self')
                },
                gnbCardOpen : function() {
					$( "body" ).toggleClass( "gnb_card_open" );
                }

			}
		}
	}
}

export default new Nav()