class mainNews {
	component() {
		return {
			template : `
				<div>

					<div class="title mb10 m-mb20 xs-mb30">
                        <div class="title-block">

                        	<div class="col-sm-8 col-xs-8 pl0">
                        		<span class="main-title">프로젝트 새소식</span>
                        	</div>
                        	<div class="col-sm-4 col-xs-4 text-right pr0">
                        		<ul class="nav nav-tabs nav-justified main-ul" role="tablist">
									<li role="presentation" :class="{'active' : menuType == 1}" v-on:click="menuChange(1)"><a href="javascript:void(0);">리워드</a></li>
									<li role="presentation" :class="{'active' : menuType == 2}" v-on:click="menuChange(2)"><a class="mb-text-centor" href="javascript:void(0);">투자</a></li>
								</ul>
                        	</div>
                        </div>
                    </div>

                    <hr class="mb30 hidden-xs">
					
					<reward-news v-show="menuType == 1"></reward-news>
					<invest-news v-show="menuType == 2"></invest-news>
				</div>
			`,
			data : function() {
				return {
					menuType : 1
				}
			},
			components : {
				rewardNews : require('../reward/reward-main-news.js').default.component(),
				investNews : require('../invest/invest-main-news.js').default.component()
			},
			methods : {
				menuChange : function(menu) {
                    this.menuType = menu;
                }
			}
		}
	}
}

export default new mainNews()