
(function($){
	$(document).ready(function(){

		// $( ".open_my_card" ).on( "click", function() {
		// 	$( "body" ).toggleClass( "gnb_card_open" );
		// });
		$( ".gnb_card_close, .gnb_card_wrap" ).on( "click", function() {
			$( "body" ).removeClass( "gnb_card_open" );
		});

		$('.webuiPopover').webuiPopover({
            defaults: {
                trigger: "click",
                width: 320,
                multi: true,
                cloaseable: false,
                style: "",
                delay: 300,
                padding: true
            }
        });
		/*
		$(document).scroll(function () {
            var scroll = $(this).scrollTop();

            // 아래로 스크롤하면 [위로 이동] 버튼 보이기
            if (scroll > 305) {
                $("div.scrollToTop").addClass("scroll-visible");
            } else {
                $("div.scrollToTop").removeClass("scroll-visible");
            }

        });*/

        // [위로 이동] 버튼
        $("body").append('<div class="scrollToTop"><i class="fa fa-angle-up"></i></div>');

        // [위로 이동] 버튼 클릭 시 화면 처음으로 이동
        $("div.scrollToTop").on("click", function() {
	        $('html, body').animate({
		            scrollTop: 0
		          },700);
	        return false;
      	});

	}); // End document ready

})(this.jQuery);

// NAVER INIT 
var naverLogin = new naver_id_login("Y7FrHjCDGkCrRMCZEzOe");


// FACEBOOK INIT 
 window.fbAsyncInit = function(a,b) {
  FB.init({
	//appId            : '173081373134550', // localhost 
//     appId            : '941006922713243', // crowdy-dev
//	  appId            : '261760031131184', // crowdy-dev
    appId            : '213171065778840', //real
    //autoLogAppEvents : true,
     cookie     : true,
    xfbml            : true,
	version          : 'v2.8'
  });
  //FB.AppEvents.logPageView();
}; 

(function(d, s, id){
   var js, fjs = d.getElementsByTagName(s)[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement(s); js.id = id;
   js.src = "https://connect.facebook.net/ko_KR/sdk.js?v=20171117";
   fjs.parentNode.insertBefore(js, fjs);
 }(document, 'script', 'facebook-jssdk'));
	  

// Basic Tab
(function ($) {
	$.fn.noticetab = function(options, configs) {
		//tab ON OFF
		var tabtitlist = jQuery(this);
		var list_type = (tabtitlist.find('.tab-contents').length)?'ul':'ol';
		tabtitlist.find(list_type).hide().end().find(list_type+':first').show();
		var tablink = tabtitlist.find('.title a');
		jQuery(tabtitlist).each(function(){
			//mouseover
			jQuery(this).find('.title a').bind('mouseover keyup' , function(){
				jQuery(tabtitlist).find(list_type).hide();
				var index = $(tablink).index(this);
				var temp = $(tabtitlist).find(list_type)
				$(temp[index]).show();
				$(this).parent().addClass('on').next().addClass('on').end().siblings().removeClass('on');
				$(this).parent().next().addClass('on')
			});
		});
	};
})(jQuery);

$(document).on('focusin', function(e) {
    if ($(e.target).closest(".mce-window").length) {
        e.stopImmediatePropagation();
    }
});
/*
function formatFileSize(size) {
	var units = ['Byte', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
	var threshold = 1024;
	size = Number(size) * threshold;
  	const i = size === 0 ? 0 : Math.floor(Math.log(size) / Math.log(threshold));
  	return ((size / Math.pow(threshold, i)).toFixed(2) * 1) + " " + units[i];
}*/
