package com.ycrowdy.web.view.controller;

import com.ycrowdy.web.common.service.MessageService;
import com.ycrowdy.web.common.vo.MetaVo;
import com.ycrowdy.web.view.service.MetaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/open/")
public class PreOpenViewController {

    @Autowired
    private MetaService metaService;

    @Autowired
    private HashMap<String, String> titleMap;

    @Autowired
    private HashMap<String, String> descriptionMap;

    @Autowired
    private HashMap<String, String> ogDescriptionMap;

    @Autowired
    private MessageService message;

    @GetMapping("/{idx:.+}")
    public String controller(@PathVariable(name = "idx") String idx, HttpServletRequest request, Model model) throws Exception {
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("preopenIdx", idx);
        MetaVo metaVo = metaService.getMeta("pre", param);

        model.addAttribute("idx", idx);
        model.addAttribute("image", request.getScheme() + "://" + metaVo.getImage());
        model.addAttribute("title", message.createMessage(titleMap.get("pre-detail"), metaVo.getTitle()));
        model.addAttribute("metaUrl", request.getScheme() + "://" + request.getServerName() + request.getRequestURI());
        model.addAttribute("description", ogDescriptionMap.get("main"));
        model.addAttribute("siteDescription", descriptionMap.get("main"));

        return "/crowdy/pre-open";
    }
}
