package com.ycrowdy.web.view.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class BeforeLinkController {

    @GetMapping("/m/ticketServlet")
    public String beforeLinkController(RedirectAttributes model) {
        return "redirect:/";
    }

}
