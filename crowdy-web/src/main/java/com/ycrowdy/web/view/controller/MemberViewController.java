package com.ycrowdy.web.view.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

@Controller
public class MemberViewController {

    @Value("${crowdy.image}")
    private String crowdyImage;

    @Value("${crowdy.invest.image}")
    private String crowdyInvestImage;

    @Autowired
    private HashMap<String, String> titleMap;

    @Autowired
    private HashMap<String, String> descriptionMap;

    @Autowired
    private HashMap<String, String> ogDescriptionMap;

    @GetMapping("/password/init/{token:.+}")
    public String controller(@PathVariable(name = "token") String token, HttpServletRequest request, Model model) {
        model.addAttribute("metaUrl", request.getScheme() + "://" + request.getServerName() + request.getRequestURI());
        model.addAttribute("title" , StringUtils.defaultString(titleMap.get("main")));
        model.addAttribute("description", StringUtils.defaultString(ogDescriptionMap.get("main")));
        model.addAttribute("siteDescription", StringUtils.defaultString(descriptionMap.get("main")));

        model.addAttribute("token", token);
        return "/user/setting-password";
    }
}
