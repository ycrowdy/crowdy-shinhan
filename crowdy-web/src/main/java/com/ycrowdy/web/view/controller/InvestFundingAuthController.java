package com.ycrowdy.web.view.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/invest")
public class InvestFundingAuthController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private String logInHeader  = "/NiceAuthResultController INPUT:\n";
    private String logOutHeader = "/NiceAuthResultController OUTPUT:\n";

    @RequestMapping("/funding/auth")
    public String controller(HttpServletRequest request,
            HttpSession session,
            @RequestParam Map<String, Object> data,
            Model model) throws Exception {
        model.addAttribute("hd_pi", String.valueOf(data.get("hd_pi")));
        model.addAttribute("hd_ep_type", String.valueOf(data.get("hd_ep_type")));
        return "/invest/auth";
    }
}








