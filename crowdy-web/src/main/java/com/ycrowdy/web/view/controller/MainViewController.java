package com.ycrowdy.web.view.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;

@Controller
public class MainViewController {

    @Value("${crowdy.image}")
    private String crowdyImage;

    @Autowired
    private HashMap<String, String> titleMap;

    @Autowired
    private HashMap<String, String> descriptionMap;

    @Autowired
    private HashMap<String, String> ogDescriptionMap;

    @RequestMapping("/")
    public String home(HttpSession session, HttpServletRequest request, Model model) {

        model.addAttribute("metaUrl", request.getScheme() + "://" + request.getServerName() + request.getRequestURI());
        model.addAttribute("title" , titleMap.get("main"));
        model.addAttribute("description", ogDescriptionMap.get("main"));
        model.addAttribute("siteDescription", descriptionMap.get("main"));
        model.addAttribute("image" , crowdyImage);
        model.addAttribute("keywords" , descriptionMap.get("main"));

        return "/main/main";
    }
}
