package com.ycrowdy.web.view.controller;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;

@Controller
public class ViewController {

    @Value("${crowdy.image}")
    private String crowdyImage;

    @Value("${crowdy.invest.image}")
    private String crowdyInvestImage;

    @Autowired
    private HashMap<String, String> titleMap;

    @Autowired
    private HashMap<String, String> descriptionMap;

    @Autowired
    private HashMap<String, String> ogDescriptionMap;

    @RequestMapping("/{path:^(?!resources).+}/{end}")
    public String view(HttpSession session,
                       @PathVariable(name = "path") String path,
                       @PathVariable(name = "end") String end,
                       HttpServletRequest request,
                       Model model) throws Exception {

        model.addAttribute("metaUrl", request.getScheme() + "://" + request.getServerName() + request.getRequestURI());
        model.addAttribute("title" , StringUtils.defaultString(titleMap.get(path), titleMap.get("main")));
        model.addAttribute("description", StringUtils.defaultString(ogDescriptionMap.get(path), ogDescriptionMap.get("main")));
        model.addAttribute("siteDescription", StringUtils.defaultString(descriptionMap.get(path), descriptionMap.get("main")));
        if(path.compareTo("invest") == 0 && end.compareTo("list") == 0) {
            model.addAttribute("image" , crowdyInvestImage);
        } else {
            model.addAttribute("image" , crowdyImage);
        }
        return "/" + path + "/" + end;
    }

    @RequestMapping("/{path:^(?!resources).+}/{middle}/{end}")
    public String view(HttpSession session,
                       @PathVariable(name = "path") String path,
                       @PathVariable(name = "middle") String middle,
                       @PathVariable(name = "end") String end,
                       HttpServletRequest request,
                       Model model) throws Exception {

        model.addAttribute("metaUrl", request.getScheme() + "://" + request.getServerName() + request.getRequestURI());
        model.addAttribute("title" , StringUtils.defaultString(titleMap.get(path), titleMap.get("main")));
        model.addAttribute("description", StringUtils.defaultString(ogDescriptionMap.get(path), titleMap.get("main")));
        model.addAttribute("siteDescription", StringUtils.defaultString(descriptionMap.get(path), titleMap.get("main")));
        if(path.compareTo("invest") == 0 && end.compareTo("list") == 0) {
            model.addAttribute("image" , crowdyInvestImage);
        } else {
            model.addAttribute("image" , crowdyImage);
        }
        return "/" + path + "/" + middle + "/" + end;
    }
}
