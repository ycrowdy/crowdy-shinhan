package com.ycrowdy.web.view.controller;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/invest")
public class InvestFundingViewController {

    @GetMapping("/funding/{url:.+}")
    public String controller(@PathVariable(name = "url") String url, RedirectAttributes model, @RequestParam Map<String, String> parameters) {
        model.addFlashAttribute("url", url);
        model.addFlashAttribute("moveStep", parameters.get("moveStep"));
        model.addFlashAttribute("investorIdx", parameters.get("investorIdx"));
        return "redirect:/invest/funding";
    }
}