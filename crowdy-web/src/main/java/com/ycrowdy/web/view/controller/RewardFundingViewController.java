package com.ycrowdy.web.view.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class RewardFundingViewController {

    @GetMapping("/reward/funding/{url:.+}")
    public String rewardController(@PathVariable(name = "url") String url, RedirectAttributes model) {
        model.addFlashAttribute("url", url);
        return "redirect:/reward/funding";
    }

    @GetMapping("/simulation/funding/{url:.+}")
    public String simulationController(@PathVariable(name = "url") String url, RedirectAttributes model) {
        model.addFlashAttribute("url", url);
        return "redirect:/simulation/funding";
    }
}
