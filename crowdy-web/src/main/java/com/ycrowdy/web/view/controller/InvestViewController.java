package com.ycrowdy.web.view.controller;

import com.ycrowdy.web.common.service.MessageService;
import com.ycrowdy.web.common.vo.MetaVo;
import com.ycrowdy.web.view.service.MetaService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
public class InvestViewController {

    @Autowired
    private MetaService metaService;

    @Autowired
    private HashMap<String, String> titleMap;

    @Autowired
    private HashMap<String, String> descriptionMap;

    @Autowired
    private HashMap<String, String> ogDescriptionMap;

    @Autowired
    private MessageService message;

    @GetMapping("/i/{url:.+}")
    public String controller(@PathVariable(name = "url") String url, HttpServletRequest request, Model model) throws Exception {
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("pjAliasUrl", url);
        MetaVo metaVo = metaService.getMeta("invest", param);

        model.addAttribute("url", url);
        model.addAttribute("image" , request.getScheme() + "://" + metaVo.getImage());
        model.addAttribute("title", message.createMessage(titleMap.get("detail"), metaVo.getTitle()));
        model.addAttribute("metaUrl", request.getScheme() + "://" + request.getServerName() + request.getRequestURI());
//        model.addAttribute("description", ogDescriptionMap.get("invest-detail"));
//        model.addAttribute("siteDescription", descriptionMap.get("invest-detail"));
        
        if (metaVo.getKeywords().isEmpty()) {
       	 	model.addAttribute("description", metaVo.getTitle());
            model.addAttribute("siteDescription", metaVo.getTitle());
            model.addAttribute("keywords", metaVo.getTitle());
		} else {
			 model.addAttribute("description", metaVo.getTitle() + ", " + metaVo.getKeywords());
		     model.addAttribute("siteDescription", metaVo.getTitle() + ", " + metaVo.getKeywords());
		     model.addAttribute("keywords", metaVo.getKeywords());
		}

        return "/invest/invest-detail";
    }

    @GetMapping("/cfi/project/{url:.+}")
    public String cpController(@PathVariable(name = "url") String url) throws Exception {
        return "redirect:/i/" + url;
    }
}
