package com.ycrowdy.web.view.service;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.ycrowdy.web.common.util.FrontConstants;
import com.ycrowdy.web.common.vo.MetaVo;
import com.ycrowdy.web.data.service.DataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class MetaService {

    @Autowired
    private DataService dataService;

    @Autowired
    private Gson gson;

    public MetaVo getMeta(String type, Map<String, Object> param) throws Exception {
        String url = "";
        if(type.compareTo("reward") == 0) {
            url = FrontConstants.VIEW_PREFIX + FrontConstants.REWARD_PROJECT_META;
        } else if(type.compareTo("invest") == 0) {
            url = FrontConstants.VIEW_PREFIX + FrontConstants.INVEST_PROJECT_META;
        } else if(type.compareTo("news") == 0) {
            url = FrontConstants.CROWDY_PREFIX + FrontConstants.NEWS_META;
        } else if(type.compareTo("pre") == 0) {
            url = FrontConstants.CROWDY_PREFIX + FrontConstants.PRE_OPEN_META;
        } else if(type.compareTo("simulaionContest") == 0) {
            url = FrontConstants.VIEW_PREFIX + FrontConstants.SIMULATION_CONTEST_META;
        }

        Map<String, Object> result = dataService.getData(url, param);
        Map<String, Object> data = (Map<String, Object>) result.get("rdata");

        MetaVo meta = new MetaVo();
        if(data != null) {
            JsonElement jsonElement = gson.toJsonTree(data);
            meta = gson.fromJson(jsonElement, MetaVo.class);
        }

        return meta;
    }
}
