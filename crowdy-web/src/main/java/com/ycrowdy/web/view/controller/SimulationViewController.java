package com.ycrowdy.web.view.controller;

import com.google.gson.Gson;
import com.ycrowdy.web.common.service.MessageService;
import com.ycrowdy.web.common.vo.MetaVo;
import com.ycrowdy.web.view.service.MetaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
public class SimulationViewController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private MetaService metaService;

    @Autowired
    private HashMap<String, String> titleMap;

    @Autowired
    private HashMap<String, String> descriptionMap;

    @Autowired
    private HashMap<String, String> ogDescriptionMap;

    @Autowired
    private MessageService message;

    @Autowired
    private Gson gson;

    @GetMapping("/sc/{url:.+}")
    public String scController(@PathVariable(name = "url") String url, HttpServletRequest request, Model model) throws Exception {
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("simulationUrl", url);
        MetaVo metaVo = metaService.getMeta("simulaionContest", param);

        model.addAttribute("url", url);
        model.addAttribute("image" , request.getScheme() + "://" + metaVo.getImage());
        model.addAttribute("title", message.createMessage(titleMap.get("detail"), metaVo.getTitle()));
        model.addAttribute("metaUrl", request.getScheme() + "://" + request.getServerName() + request.getRequestURI());
        model.addAttribute("description", ogDescriptionMap.get("main"));
        model.addAttribute("siteDescription", descriptionMap.get("main"));

        return "/simulation/simulation-contest-detail";
    }

    @GetMapping("/sr/{url:.+}")
    public String srController(@PathVariable(name = "url") String url, HttpServletRequest request, Model model) throws Exception {
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("cpAliasUrl", url);
        MetaVo metaVo = metaService.getMeta("reward", param);

        model.addAttribute("url", url);
        model.addAttribute("image" , request.getScheme() + "://" + metaVo.getImage());
        model.addAttribute("title", message.createMessage(titleMap.get("detail"), metaVo.getTitle()));
        model.addAttribute("metaUrl", request.getScheme() + "://" + request.getServerName() + request.getRequestURI());
//        model.addAttribute("description", ogDescriptionMap.get("detail"));
//        model.addAttribute("siteDescription", descriptionMap.get("detail"));
        
        if (metaVo.getKeywords().isEmpty()) {
       	 	model.addAttribute("description", metaVo.getTitle());
            model.addAttribute("siteDescription", metaVo.getTitle());
            model.addAttribute("keyword", metaVo.getTitle());
		} else {
			 model.addAttribute("description", metaVo.getTitle() + ", " + metaVo.getKeywords());
		     model.addAttribute("siteDescription", metaVo.getTitle() + ", " + metaVo.getKeywords());
		     model.addAttribute("keyword", metaVo.getKeywords());
		}

        return "/simulation/simulation-detail";
    }
}
