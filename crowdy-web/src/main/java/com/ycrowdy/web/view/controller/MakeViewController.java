package com.ycrowdy.web.view.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

@Controller
@RequestMapping("/make")
public class MakeViewController {

    @Value("${crowdy.image}")
    private String crowdyImage;

    @Value("${crowdy.invest.image}")
    private String crowdyInvestImage;

    @Autowired
    private HashMap<String, String> titleMap;

    @Autowired
    private HashMap<String, String> descriptionMap;

    @Autowired
    private HashMap<String, String> ogDescriptionMap;

    @GetMapping("/invest/main/{pjCode}")
    public String investController(@PathVariable(name="pjCode") String pjCode, HttpServletRequest request, Model model) {
        model.addAttribute("metaUrl", request.getScheme() + "://" + request.getServerName() + request.getRequestURI());
        model.addAttribute("title" , StringUtils.defaultString(titleMap.get("main")));
        model.addAttribute("description", StringUtils.defaultString(ogDescriptionMap.get("main")));
        model.addAttribute("siteDescription", StringUtils.defaultString(descriptionMap.get("main")));

        model.addAttribute("pjCode", pjCode);
        return "/make/invest/main";
    }
    
    @GetMapping("/reward/main/{cpCode}")
    public String rewardController(@PathVariable(name="cpCode") String cpCode, HttpServletRequest request, Model model) {
        model.addAttribute("metaUrl", request.getScheme() + "://" + request.getServerName() + request.getRequestURI());
        model.addAttribute("title" , StringUtils.defaultString(titleMap.get("main")));
        model.addAttribute("description", StringUtils.defaultString(ogDescriptionMap.get("main")));
        model.addAttribute("siteDescription", StringUtils.defaultString(descriptionMap.get("main")));

        model.addAttribute("cpCode", cpCode);
        return "/make/reward/main";
    }
    
    @GetMapping("/simulation/main/{cpCode}")
    public String simulationController(@PathVariable(name="cpCode") String cpCode, HttpServletRequest request, Model model) {
        model.addAttribute("metaUrl", request.getScheme() + "://" + request.getServerName() + request.getRequestURI());
        model.addAttribute("title" , StringUtils.defaultString(titleMap.get("main")));
        model.addAttribute("description", StringUtils.defaultString(ogDescriptionMap.get("main")));
        model.addAttribute("siteDescription", StringUtils.defaultString(descriptionMap.get("main")));

        model.addAttribute("cpCode", cpCode);
        return "/make/simulation/main";
    }
}
