package com.ycrowdy.web.file.service;

import java.io.ByteArrayInputStream;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.ycrowdy.web.common.vo.FileVo;


@Service
public class AWSFileService {
    
    @Autowired
    private AmazonS3Client amazonS3Client;

    @Value("${aws.bucketname}")
    private String bucketName;

    public void fileUpload(String fileName, String imgStr, String imgType) throws Exception {

        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentType(imgType);
        byte[] result = DigestUtils.md5(new ByteArrayInputStream(Base64.decodeBase64(imgStr)));
        metadata.setContentMD5(new String(Base64.encodeBase64(result)));
        metadata.setContentLength((long) Base64.decodeBase64(imgStr).length);

        PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, fileName, new ByteArrayInputStream(Base64.decodeBase64(imgStr)), metadata);
        amazonS3Client.putObject(putObjectRequest);
    }

    public void fileUpload(FileVo data) throws Exception {
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentType(data.getFileExt());
        byte[] result = DigestUtils.md5(new ByteArrayInputStream(data.getFileValue()));
        metadata.setContentMD5(new String(Base64.encodeBase64(result)));
        metadata.setContentLength(Long.parseLong(data.getFileSize()));
        
        PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, data.getFileName(), new ByteArrayInputStream(data.getFileValue()), metadata);
        amazonS3Client.putObject(putObjectRequest);
    }

    public void fileUpload(String fileName, MultipartFile file) throws Exception {
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentType(file.getContentType());

        PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, fileName, file.getInputStream(), metadata);
        amazonS3Client.putObject(putObjectRequest);
    }
}
