package com.ycrowdy.web.file.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.ycrowdy.web.common.vo.FileVo;

@Service
public class FileService {

    @Autowired
    private AWSFileService awsFileService;
    
    @Autowired
    private DataBaseFileService dataBaseFileService;
    
    @Value("${image.cdn1}")
    private String awsCdnUrl;
    
    @Value("${image.cdn2}")
    private String crowdyCdnUrl;
    
    public String saveAwsFile(FileVo data) throws Exception {
        String fileName = getFileName("CROWDY", data.getFileExt());
        data.setFileName(fileName);
        
        awsFileService.fileUpload(data);
        return awsCdnUrl + fileName;
    }
    
    public String saveCrowdyFile(FileVo data, String fileGroup, String fileType) throws Exception {
        return saveCrowdyFile(data, fileGroup, fileType, true);
    }
    
    public String saveCrowdyFile(FileVo data, String fileGroup, String fileType, boolean cdn) throws Exception {
        return saveCrowdyFile(data, "", fileGroup, fileType, cdn);
    }
    
    public String saveCrowdyFile(FileVo data, String fileCodeGroup, String fileGroup, String fileType, boolean cdn) throws Exception {

        if(data == null || StringUtils.isEmpty(data.getFileData())) return "";

        data.setFileGroup(fileGroup);
        data.setFileType(fileType);
        
        String fileName = getDatabaseFileName(fileType, data.getFileIndex());
        data.setFileCode(fileName);
        if(StringUtils.isEmpty(fileCodeGroup)) {
            fileCodeGroup = fileName;
        }
        data.setFileCodeGroup(fileCodeGroup);
        
        dataBaseFileService.fileUpload(data);
        
        StringBuffer buffer = new StringBuffer(fileName);
        if(cdn) {
            buffer.insert(0, crowdyCdnUrl);
        }
        return buffer.toString();
    }
    
    public String saveCrowdyFiles(List<FileVo> data, String fileCodeGroup, String fileGroup, String fileType, boolean cdn) throws Exception {
        if(StringUtils.isEmpty(fileCodeGroup)) {
            fileCodeGroup = "";
        }

        for(int i = 0, size = data.size(); i < size; i++) {
            FileVo vo = data.get(i);
            if(!vo.isState()) {
                //파일 삭제
                dataBaseFileService.fileDelete(vo.getFileCode());
            } else {
                if(StringUtils.isEmpty(vo.getFileData())) continue;

                vo.setFileIndex(String.valueOf(i));
                vo.setFileGroup(fileGroup);
                vo.setFileType(fileType);

                String fileName = getDatabaseFileName(fileType, vo.getFileIndex());
                vo.setFileCode(fileName);

                if(StringUtils.isEmpty(fileCodeGroup)) {
                    fileCodeGroup = fileName;
                }
                vo.setFileCodeGroup(fileCodeGroup);

                dataBaseFileService.fileUpload(vo);
            }

        }

        StringBuffer buffer = new StringBuffer(fileCodeGroup);
        if(cdn) {
            buffer.insert(0, crowdyCdnUrl);
        }
        return buffer.toString();
    }
    
    private String getDatabaseFileName(String prefix, String index) {
        StringBuffer fileName = new StringBuffer();
        fileName.append(prefix);
        fileName.append("_");
        fileName.append(index);
        fileName.append("_");
        fileName.append(getNow("yyyyMMddHHmmssSSSS"));
        fileName.append("_");
        fileName.append(RandomStringUtils.randomAlphanumeric(5));

        return fileName.toString();
    }
    
    private String getFileName(String prefix, String ext) {
        StringBuffer fileName = new StringBuffer();
        fileName.append(getNow("yyyyMMdd"));
        fileName.append("/");
        fileName.append(prefix);
        fileName.append("_");
        fileName.append(getNow("yyyyMMddHHmmssSSSS"));
        fileName.append("_");
        fileName.append(RandomStringUtils.randomAlphanumeric(5));
        fileName.append(".");
        fileName.append(ext);
        
        return fileName.toString();
    }
    
    private String getNow(String pattern) {
        if(StringUtils.isEmpty(pattern)) {
            pattern = "yyyy-MM-dd HH:mm:ss";
        }
        
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        
        return format.format(new Date());
    }
}
