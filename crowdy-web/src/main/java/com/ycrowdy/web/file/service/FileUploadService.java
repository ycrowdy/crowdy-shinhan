package com.ycrowdy.web.file.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ycrowdy.web.common.vo.FileUploadVo;

@Service
public class FileUploadService {

    @Autowired
    private FileService fileService;
    
    @Autowired
    private DataBaseFileService baseFileService;
    
    public String upload(FileUploadVo vo) throws Exception {
        return fileService.saveAwsFile(vo.getFile());
    }
    
    public void delete(String fileCode) throws Exception {
        baseFileService.fileDelete(fileCode);
    }
}
