package com.ycrowdy.web.file.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ycrowdy.web.common.mapper.FileMapper;
import com.ycrowdy.web.common.vo.FileVo;

@Service
public class DataBaseFileService {

    @Autowired
    private FileMapper fileMapper;
    
    public void fileUpload(FileVo file) throws Exception {
        fileMapper.setFile(file);
    }
    
    public void fileDelete(String fileCode) throws Exception {
        fileMapper.deleteFile(fileCode);
    }
}
