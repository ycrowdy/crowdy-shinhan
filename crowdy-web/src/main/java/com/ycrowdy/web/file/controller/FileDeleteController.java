package com.ycrowdy.web.file.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ycrowdy.web.file.service.FileUploadService;

@RestController
@RequestMapping("/file")
public class FileDeleteController {
    
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private String logInHeader  = "/FileDeleteController INPUT:\n";
    private String logOutHeader = "/FileDeleteController OUTPUT:\n";
    
    @Autowired
    private FileUploadService fileUploadService;
    
    @PostMapping("/delete/{fileCode}")
    public void controller(@PathVariable(value="fileCode") String fileCode) throws Exception {
        
        fileUploadService.delete(fileCode);
    }
    
}
