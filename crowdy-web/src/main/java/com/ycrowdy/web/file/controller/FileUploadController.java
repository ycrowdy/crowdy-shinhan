package com.ycrowdy.web.file.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.ycrowdy.web.file.service.FileUploadService;
import com.ycrowdy.web.common.vo.FileUploadVo;

@RestController
@RequestMapping("/file")
public class FileUploadController {
    
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private String logInHeader  = "/FileUploadController INPUT:\n";
    private String logOutHeader = "/FileUploadController OUTPUT:\n";
    
    @Autowired
    private FileUploadService fileUploadService;

    @PostMapping("/upload")
    public String controller(@RequestBody FileUploadVo vo) throws Exception {
        return fileUploadService.upload(vo);
    }
}
