package com.ycrowdy.web.dashboard.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.ycrowdy.web.common.util.ExcelGenerator;
import com.ycrowdy.web.common.util.FrontConstants;
import com.ycrowdy.web.common.vo.ResponseVo;
import com.ycrowdy.web.data.service.DataService;
import com.ycrowdy.web.ga.service.GAInfoService;
import com.ycrowdy.web.ga.vo.GAResultVo;
import com.ycrowdy.web.ga.vo.GASearchVo;
import com.ycrowdy.web.reward.vo.RewardDashBoardChartWrapper;
import com.ycrowdy.web.reward.vo.RewardDashBoardExcelSearchVo;
import com.ycrowdy.web.reward.vo.RewardDashBoardSearchVo;
import com.ycrowdy.web.reward.vo.RewardDashBoardSponsorInfoVo;

@Service
public class RewardDashBoardExcelService {

    @Autowired
    private DataService dataService;
    
    @Autowired
    private GAInfoService gaInfoService;
    
    @Autowired
    protected Gson gson;
    
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    
    public void staticExcel(RewardDashBoardSearchVo data, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Map<String, Object> result = dataService.getData(FrontConstants.VIEW_PREFIX + FrontConstants.REWARD_DASHBOARD_CHART, data);
        List<GAResultVo> visitList = gaInfoService.report(new GASearchVo(data.getStartDate(), data.getEndDate(), "/r/" + data.getCpAliasUrl()));
        ResponseVo responseVo = new ResponseVo(result);
        
        if(responseVo.getrCode().compareTo(FrontConstants.SUCCESS) == 0) {
            RewardDashBoardChartWrapper wrapper = gson.fromJson(gson.toJsonTree(responseVo.getrData()), RewardDashBoardChartWrapper.class);
            
            ExcelGenerator excelGenerator = new ExcelGenerator("방문 통계", request, response);
            
            String[] headers = {
                     "날짜",
                     "방문자 수",
                     "후원자 수",
                     "후원금액"
             };
             
             excelGenerator.generateSheetHeader(headers);
             
             List<String> keys = new ArrayList<String>();
             keys.add("day");
             keys.add("value");
             excelGenerator.generateSheetChartBody(keys, wrapper.getChartDate(), visitList, wrapper.getSponsorChartData(), wrapper.getAmountChartData());
             excelGenerator.generateSheetEnd();
        }
    }
    
    public void sponsorExcel(RewardDashBoardExcelSearchVo data, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Map<String, Object> result = dataService.getData(FrontConstants.VIEW_PREFIX + FrontConstants.REWARD_DASHBOARD_SPONSOR, data);
        ResponseVo responseVo = new ResponseVo(result);
        
        if(responseVo.getrCode().compareTo(FrontConstants.SUCCESS) == 0) {
            RewardDashBoardSponsorInfoVo [] vo = gson.fromJson(gson.toJsonTree(responseVo.getrData()), RewardDashBoardSponsorInfoVo[].class);
            
            ExcelGenerator excelGenerator = new ExcelGenerator("편딩 현황", request, response);
            
            String[] headers = {
                    "주문일시",
                    "이름",
                    "이메일",
                    "주소",
                    "휴대전화",
                    "리워드명",
                    "리워드금액",
                    "추가펀딩금액",
                    "수량",
                    "총 결제금액",
                    "결제상태",
                    "혜택옵션",
                    "주문비고내용"
            };
            excelGenerator.generateSheetHeader(headers);
            
            List<String> keys = new ArrayList<String>();
            keys.add("wdate");
            keys.add("spsRsName");
            keys.add("memEmail");
            keys.add("addr");
            keys.add("spsMBMobileno");
            keys.add("cbfTitle");
            keys.add("sbfUnitPrice");
            keys.add("spsAmount");
            keys.add("sbfQty");
            keys.add("spsTotAmount");
            keys.add("spsStatus");
            keys.add("sbfBenefit");
            keys.add("spsMiscInfo");
            excelGenerator.generateSheetBody(keys, Arrays.asList(vo));
            excelGenerator.generateSheetEnd();
        }
        
    }
}
