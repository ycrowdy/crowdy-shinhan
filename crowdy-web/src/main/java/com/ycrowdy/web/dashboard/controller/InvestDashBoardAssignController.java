package com.ycrowdy.web.dashboard.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ycrowdy.web.dashboard.service.InvestDashBoardExcelService;
import com.ycrowdy.web.invest.vo.InvestDashBoardSearchVo;

@Controller
@RequestMapping("/dashboard/invest")
public class InvestDashBoardAssignController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private InvestDashBoardExcelService investDashBoardExcelService;
    
    @PostMapping("/assign/excel")
    public void controller(HttpServletRequest request, HttpServletResponse response, @ModelAttribute InvestDashBoardSearchVo data) throws Exception {
    	investDashBoardExcelService.assignExcel(data, request, response);
    }
}
