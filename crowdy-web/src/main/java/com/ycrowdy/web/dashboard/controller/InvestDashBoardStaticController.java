package com.ycrowdy.web.dashboard.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.ycrowdy.web.dashboard.service.InvestDashBoardExcelService;
import com.ycrowdy.web.invest.vo.InvestDashBoardChartSearchVo;

@Controller
@RequestMapping(value="/dashboard/invest", method=RequestMethod.GET)
public class InvestDashBoardStaticController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private InvestDashBoardExcelService investDashBoardExcelService;
    
    @PostMapping("/static/excel")
    public void controller(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> data) throws Exception {
        InvestDashBoardChartSearchVo vo = new InvestDashBoardChartSearchVo(String.valueOf(data.get("pjCode")), String.valueOf(data.get("startDay")), String.valueOf(data.get("endDay")), String.valueOf(data.get("pjAliasUrl")));
        investDashBoardExcelService.staticExcel(vo, request, response);
    }
}
