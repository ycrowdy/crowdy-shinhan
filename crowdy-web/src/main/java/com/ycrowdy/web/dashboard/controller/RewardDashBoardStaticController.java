package com.ycrowdy.web.dashboard.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.ycrowdy.web.dashboard.service.RewardDashBoardExcelService;
import com.ycrowdy.web.invest.vo.InvestDashBoardChartSearchVo;
import com.ycrowdy.web.reward.vo.RewardDashBoardSearchVo;

@Controller
@RequestMapping(value="/dashboard/reward", method=RequestMethod.GET)
public class RewardDashBoardStaticController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private RewardDashBoardExcelService rewardDashBoardExcelService;
    
    @PostMapping("/static/excel")
    public void controller(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> data) throws Exception {
        RewardDashBoardSearchVo vo = new RewardDashBoardSearchVo(String.valueOf(data.get("cpCode")), String.valueOf(data.get("startDate")), String.valueOf(data.get("endDate")), String.valueOf(data.get("cpAliasUrl")));
        rewardDashBoardExcelService.staticExcel(vo, request, response);
    }
}
