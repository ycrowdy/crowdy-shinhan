package com.ycrowdy.web.dashboard.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.ycrowdy.web.common.util.ExcelGenerator;
import com.ycrowdy.web.common.util.FrontConstants;
import com.ycrowdy.web.common.vo.ResponseVo;
import com.ycrowdy.web.data.service.DataService;
import com.ycrowdy.web.ga.service.GAInfoService;
import com.ycrowdy.web.ga.vo.GAResultVo;
import com.ycrowdy.web.ga.vo.GASearchVo;
import com.ycrowdy.web.invest.vo.InvestDashBoardAssignVo;
import com.ycrowdy.web.invest.vo.InvestDashBoardCalculateVo;
import com.ycrowdy.web.invest.vo.InvestDashBoardChartSearchVo;
import com.ycrowdy.web.invest.vo.InvestDashBoardInvestorWrapper;
import com.ycrowdy.web.invest.vo.InvestDashBoardSearchVo;

@Service
public class InvestDashBoardExcelService {

    @Autowired
    private DataService dataService;

    @Autowired
    private GAInfoService gaInfoService;
    
    @Autowired
    protected Gson gson;
    
    public void assignExcel(InvestDashBoardSearchVo data, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Map<String, Object> result = dataService.getData(FrontConstants.VIEW_PREFIX + FrontConstants.INVEST_DASHBOARD_ASSIGN, data);
        ResponseVo responseVo = new ResponseVo(result);
        
        if(responseVo.getrCode().compareTo(FrontConstants.SUCCESS) == 0) {
            InvestDashBoardInvestorWrapper wrapper = gson.fromJson(gson.toJsonTree(responseVo.getrData()), InvestDashBoardInvestorWrapper.class);
            
            List<InvestDashBoardAssignVo> list = wrapper.getAssignData();

            for (int i = 0; i < list.size(); i++) {
                String status = list.get(i).getInvestorStatus();
                String text = "";
                if (status.compareTo("IVSS00") == 0) {
                    text = "청약완료";
                } else if (status.compareTo("IVSS01") == 0) {
                    text = "주식배정중";
                } else if (status.compareTo("IVSS02") == 0) {
                    text = "배정완료";
                } else if (status.compareTo("IVSS03") == 0) {
                    text = "청약취소";
                } else if (status.compareTo("IVSS04") == 0) {
                    text = "배정탈락";
                } else if (status.compareTo("IVSS05") == 0) {
                    text = "투자모금실패";
                } else {
                    text = "청약부분완료";
                }
                list.get(i).setInvestorStatusText(text);
            }
             
            ExcelGenerator excelGenerator = new ExcelGenerator("배정 내역 현황", request, response);
             
            String[] headers = {
                     "배정일자",
                     "청약자명",
                     "투자자유형",
                     "이메일 ID",
                     "생년월일",
                     "배정구좌수",
                     "배정금액",
                     "배정상태"
             };
             
             excelGenerator.generateSheetHeader(headers);
             
             List<String> keys = new ArrayList<String>();
             keys.add("assignDate");
             keys.add("memName");
             keys.add("memInvestorType");
             keys.add("memEmail");
             keys.add("memInvestorBirth");
             keys.add("investAssQty");
             keys.add("amount");
             keys.add("investorStatusText");
             excelGenerator.generateSheetBody(keys, list);
             excelGenerator.generateSheetEnd();
        }
    }

    public void calculateExcel(InvestDashBoardSearchVo data, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Map<String, Object> result = dataService.getData(FrontConstants.VIEW_PREFIX + FrontConstants.INVEST_DASHBOARD_CALCULATE, data);
        ResponseVo responseVo = new ResponseVo(result);
        
        if(responseVo.getrCode().compareTo(FrontConstants.SUCCESS) == 0) {
            InvestDashBoardCalculateVo vo = gson.fromJson(gson.toJsonTree(responseVo.getrData()), InvestDashBoardCalculateVo.class);
            
            ExcelGenerator excelGenerator = new ExcelGenerator("정산 내역", request, response);
            
             vo.setDefaultAmount("2475000");
             double successAmount = Integer.parseInt(vo.getAssConTotalAmount()) * 0.055;
             vo.setSuccessAmount(String.valueOf(successAmount));
             vo.setSuccessTotalAmount(String.valueOf(successAmount + 2475000));
            
            String[] headers = {
                     "프로젝트 명",
                     "프로젝트 시작날짜",
                     "프로젝트 종료날짜",
                     "투자 목표금액",
                     "총 청약금액",
                     "총 투자자수",
                     "총 배정금액",
                     "총 배정자수",
                     "기본 착수금",
                     "성공 수수료",
                     "총 수수료"
             };
             
             excelGenerator.generateSheetHeader(headers);
             
             List<String> keys = new ArrayList<String>();
             keys.add("pjTitle");
             keys.add("pjStartDate");
             keys.add("pjEndDate");
             keys.add("targetAmount");
             keys.add("pjCurrentAmount");
             keys.add("pjInvestorCount");
             keys.add("assConTotalAmount");
             keys.add("assCount");
             keys.add("defaultAmount");
             keys.add("successAmount");
             keys.add("successTotalAmount");
             excelGenerator.generateSheetObjectBody(keys, vo);
             excelGenerator.generateSheetEnd();
        }

    }

    public void staticExcel(InvestDashBoardChartSearchVo data, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Map<String, Object> result = dataService.getData(FrontConstants.VIEW_PREFIX + FrontConstants.INVEST_DASHBOARD_CHART, data);
        List<GAResultVo> visitList = gaInfoService.report(new GASearchVo(data.getStartDay(), data.getEndDay(), "/i/" + data.getPjAliasUrl()));
        ResponseVo responseVo = new ResponseVo(result);
        
        if(responseVo.getrCode().compareTo(FrontConstants.SUCCESS) == 0) {
            InvestDashBoardInvestorWrapper wrapper = gson.fromJson(gson.toJsonTree(responseVo.getrData()), InvestDashBoardInvestorWrapper.class);
            
            ExcelGenerator excelGenerator = new ExcelGenerator("프로젝트 통계", request, response);
            
            String[] headers = {
                     "날짜",
                     "방문자 수",
                     "좋아요 수",
                     "투자자 수",
                     "투자금액"
             };
             
             excelGenerator.generateSheetHeader(headers);
             
             List<String> keys = new ArrayList<String>();
             keys.add("day");
             keys.add("value");
             excelGenerator.generateSheetChartBody(keys, wrapper.getChartDate(), visitList, wrapper.getGoodChartData(), wrapper.getInvestorChartData(), wrapper.getAmountChartData());
             excelGenerator.generateSheetEnd();
        }
    }
}
