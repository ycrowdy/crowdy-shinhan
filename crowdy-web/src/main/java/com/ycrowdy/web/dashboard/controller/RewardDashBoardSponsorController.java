package com.ycrowdy.web.dashboard.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ycrowdy.web.dashboard.service.RewardDashBoardExcelService;
import com.ycrowdy.web.reward.vo.RewardDashBoardExcelSearchVo;

@Controller
@RequestMapping(value="/dashboard/reward", method=RequestMethod.GET)
public class RewardDashBoardSponsorController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private RewardDashBoardExcelService rewardDashBoardExcelService;
    
    @PostMapping("/sponsor/excel")
    public void controller(HttpServletRequest request, HttpServletResponse response, @ModelAttribute RewardDashBoardExcelSearchVo data) throws Exception {
        //RewardDashBoardExcelSearchVo vo = new RewardDashBoardExcelSearchVo(String.valueOf(data.get("cpCode")), String.valueOf(data.get("name")), String.valueOf(data.get("status")), String.valueOf(data.get("startDate")), String.valueOf(data.get("endDate")), String.valueOf(data.get("cbfIdx")));
        rewardDashBoardExcelService.sponsorExcel(data, request, response);
    }
}