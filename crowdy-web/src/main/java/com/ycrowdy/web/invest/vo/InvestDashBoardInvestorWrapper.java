package com.ycrowdy.web.invest.vo;

import java.util.List;

import lombok.Data;

@Data
public class InvestDashBoardInvestorWrapper {

    private String count;
    
    private List<InvestDashBoardAssignVo> assignData;
    
    private List<InvestDashBoardChartResultVo> chartDate;
    
    private List<InvestDashBoardChartResultVo> goodChartData;
    
    private List<InvestDashBoardChartResultVo> investorChartData;
    
    private List<InvestDashBoardChartResultVo> amountChartData;
    
}
