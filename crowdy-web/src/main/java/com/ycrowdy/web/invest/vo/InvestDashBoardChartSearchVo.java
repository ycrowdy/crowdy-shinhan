package com.ycrowdy.web.invest.vo;

import lombok.Data;

@Data
public class InvestDashBoardChartSearchVo {
    
    private String pjCode;
    
    private String startDay;

    private String endDay;

    private String path;
    
    private String pjAliasUrl;

	public InvestDashBoardChartSearchVo(String pjCode, String startDay, String endDay, String pjAliasUrl) {
		this.pjCode = pjCode;
		this.startDay = startDay;
		this.endDay = endDay;
		this.pjAliasUrl = pjAliasUrl;
	}
}
