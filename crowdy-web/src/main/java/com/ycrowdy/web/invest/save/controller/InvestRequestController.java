package com.ycrowdy.web.invest.save.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ycrowdy.web.common.vo.ResponseVo;
import com.ycrowdy.web.invest.save.service.InvestInsertService;
import com.ycrowdy.web.invest.vo.InvestProjectRequestVo;

@RestController
@RequestMapping("/set")
public class InvestRequestController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private String logInHeader  = "/InvestRequestController INPUT:\n";
    private String logOutHeader = "/InvestRequestController OUTPUT:\n";
    
    @Autowired
    private InvestInsertService investInsertService;
    
    @PostMapping("/invest/request")
    public ResponseVo controller(HttpServletRequest request, HttpSession session,
            @RequestBody InvestProjectRequestVo data) throws Exception {
        
        Map<String, Object> result = investInsertService.setInvestRequest(data);
        
        return new ResponseVo(result);
    }
}
