package com.ycrowdy.web.invest.save.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ycrowdy.web.common.vo.ResponseVo;
import com.ycrowdy.web.invest.save.service.InvestInsertService;
import com.ycrowdy.web.invest.vo.InvestProjectFileVo;

@RestController
@RequestMapping("/set")
public class InvestFileSaveController {
    
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private String logInHeader  = "/InvestFileSaveController INPUT:\n";
    private String logOutHeader = "/InvestFileSaveController OUTPUT:\n";
    
    @Autowired
    private InvestInsertService investInsertService;
    
    @PostMapping("/invest/file")
    public ResponseVo controller(HttpServletRequest request, HttpSession session,
            @RequestBody InvestProjectFileVo data) throws Exception {
        
        String fileCodeGroup = investInsertService.setInvestFile(data);
        
        return new ResponseVo(fileCodeGroup);
    }
    
}
