package com.ycrowdy.web.invest.save.service;

import java.util.Map;

import com.ycrowdy.web.common.mapper.UrlMapper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.ycrowdy.web.file.service.FileService;
import com.ycrowdy.web.common.util.FrontConstants;
import com.ycrowdy.web.common.vo.FileVo;
import com.ycrowdy.web.data.service.DataService;
import com.ycrowdy.web.invest.vo.InvestProjectBasicVo;
import com.ycrowdy.web.invest.vo.InvestProjectFileVo;
import com.ycrowdy.web.invest.vo.InvestProjectRequestVo;

@Service
public class InvestInsertService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private DataService dataService;
    
    @Autowired
    private FileService fileService;

    @Autowired
    private UrlMapper urlMapper;

    public boolean getInvestUrlCheck(Map<String, Object> data) throws Exception {
        int count = urlMapper.getInvestProjectUrl(data);
        return count > 0 ? false : true;
    }

    public Map<String, Object> setInvestRequest(InvestProjectRequestVo vo) throws Exception {
        
        if(StringUtils.isNotEmpty(vo.getFile().getFileData())) {
            String fileName = fileService.saveCrowdyFile(vo.getFile(), "ivr_com_regi", "F0003");
            vo.setPjCompanyStory(fileName);
            vo.setFile(null);
        }
        
        return dataService.setData(FrontConstants.SAVE_PREFIX + FrontConstants.INVEST_REQUEST, vo);
    }
    
    public Map<String, Object> setInvestBasic(InvestProjectBasicVo vo) throws Exception {
        
        //1. 프로젝트 카드 이미지 처리
        if(StringUtils.isNotEmpty(vo.getPjCardFile().getFileData()) ) {
            String pjCardImg = fileService.saveAwsFile(vo.getPjCardFile());
            vo.setPjCardImg(pjCardImg);
            vo.setPjCardFile(null);
        }
        
        //2. CI 이미지 처리
        if(StringUtils.isNotEmpty(vo.getPjCiFile().getFileData()) ) {
            String companyCiImg = fileService.saveAwsFile(vo.getPjCiFile());
            vo.setCompanyCiImg(companyCiImg);
            vo.setPjCiFile(null);
        }
        
        //3. 프로젝트 상단 이미지 처리
        if(StringUtils.isNotEmpty(vo.getPjBannerfile().getFileData()) ) {
            String pjBannerImg = fileService.saveAwsFile(vo.getPjBannerfile());
            vo.setPjBannerImg(pjBannerImg);
            vo.setPjBannerfile(null);
        }
        
        //4. 프로젝트 이미지 처리
        for(FileVo image : vo.getPjImage()) {
            String fileName = image.getFileName();
            if(StringUtils.isNotEmpty(image.getFileData())) {
                fileName = fileService.saveAwsFile(image);
            }
            
            vo.getPjImages().add(fileName);
        }
        vo.setPjImage(null);
        
        
        return dataService.setData(FrontConstants.SAVE_PREFIX + FrontConstants.INVEST_PROJECT_BASIC, vo);
    }
    
    public String setInvestFile(InvestProjectFileVo vo) throws Exception {
        
        String fileName = "";
        vo.getFile().setFileInfo(vo.getFileTitle());
        if(StringUtils.isEmpty(vo.getFileCodeGroup())) {
            fileName = fileService.saveCrowdyFile(vo.getFile(), "irmi_att_file", "F0002", false);
            vo.setFileName(fileName);
            vo.setFile(null);
            vo.setFileCodeGroup(fileName);
            dataService.setData(FrontConstants.SAVE_PREFIX + FrontConstants.INVEST_FILE, vo);
        } else {
            fileName = fileService.saveCrowdyFile(vo.getFile(), vo.getFileCodeGroup(), "irmi_att_file", "F0002", false);
        }
        
        return vo.getFileCodeGroup();
    }
}
