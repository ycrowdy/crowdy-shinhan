package com.ycrowdy.web.invest.save.controller;

import com.ycrowdy.web.invest.save.service.InvestInsertService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

@RestController
@RequestMapping("/data")
public class InvestUrlCheckController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private String logInHeader  = "/InvestUrlCheckController INPUT:\n";
    private String logOutHeader = "/InvestUrlCheckController OUTPUT:\n";

    @Autowired
    private InvestInsertService investInsertService;

    @PostMapping("/invest/url")
    public boolean controller(HttpServletRequest request, HttpSession session,
                              @RequestBody Map<String, Object> data) throws Exception {

        return investInsertService.getInvestUrlCheck(data);
    }
}
