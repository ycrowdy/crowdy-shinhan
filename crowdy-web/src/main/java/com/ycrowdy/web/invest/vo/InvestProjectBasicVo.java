package com.ycrowdy.web.invest.vo;

import java.util.ArrayList;
import java.util.List;

import com.ycrowdy.web.common.vo.FileVo;

import lombok.Data;

@Data
public class InvestProjectBasicVo {

    private String pjCode;
    
    private String pjTitle;
    
    private String pjAliasUrl;
    
    private String pjSummary;
    
    private String pjCardImg;
    
    private FileVo pjCardFile;
    
    private String companyCiImg;
    
    private FileVo pjCiFile;
    
    private String pjBannerImg;
    
    private FileVo pjBannerfile;
    
    private List<FileVo> pjImage;
    
    private List<String> pjVideos;
    
    private List<String> pjImages;
    
    public InvestProjectBasicVo() {
        this.pjImages = new ArrayList<String>();
    }
}
