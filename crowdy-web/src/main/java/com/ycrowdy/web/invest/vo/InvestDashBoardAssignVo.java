package com.ycrowdy.web.invest.vo;

import lombok.Data;

@Data
public class InvestDashBoardAssignVo {

    private String assignDate;

    private String memName;
    
    private String memInvestorType;

    private String memEmail;

    private String memInvestorBirth;

    private String investAssQty;

    private String investAssignQty;

    private String amount;

    private String investorStatus;

    private String investorStatusText;
}
 