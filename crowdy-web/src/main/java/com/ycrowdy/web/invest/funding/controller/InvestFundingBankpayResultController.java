package com.ycrowdy.web.invest.funding.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ycrowdy.web.invest.funding.service.InvestFundingBankpayService;

@Controller
@RequestMapping("/invest/funding")
public class InvestFundingBankpayResultController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private String logInHeader  = "/InvestFundingBankpayResultController INPUT:\n";
    private String logOutHeader = "/InvestFundingBankpayResultController OUTPUT:\n";

    @Autowired
    private InvestFundingBankpayService investFundingBankpayService;

    @RequestMapping("/bankpayAuth")
    public String controller(HttpServletRequest request,
                             HttpSession session,
                             RedirectAttributes model,
                             @RequestParam Map<String, Object> data) throws Exception {

        Map<String, Object> result = investFundingBankpayService.pay(session, data);
        		
    	model.addFlashAttribute("url", (String) result.get("url"));
    	model.addFlashAttribute("investorIdx", (String) result.get("investorIdx"));
        model.addFlashAttribute("moveStep", "4");
        model.addFlashAttribute("memberData", result.get("memberData"));
        return "redirect:/invest/funding";
    }
}
