package com.ycrowdy.web.invest.vo;

import lombok.Data;

@Data
public class InvestDashBoardChartResultVo {
    
    private String value;

    private String day;
}
