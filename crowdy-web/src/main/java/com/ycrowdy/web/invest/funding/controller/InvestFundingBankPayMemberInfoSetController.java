package com.ycrowdy.web.invest.funding.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ycrowdy.web.invest.funding.service.InvestFundingBankpayMemberInfoService;
import com.ycrowdy.web.invest.vo.InvestFundingBankpayMemberVo;

@RestController
@RequestMapping("/invest")
public class InvestFundingBankPayMemberInfoSetController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private String logInHeader  = "/InvestFundingBankPayMemberInfoSetController INPUT:\n";
    private String logOutHeader = "/InvestFundingBankPayMemberInfoSetController OUTPUT:\n";
    
    @Autowired
    private InvestFundingBankpayMemberInfoService investFundingBankpayMemberInfoService;
    
    @PostMapping("/funding/set/pay-info")
    public boolean controller(HttpServletRequest request, @RequestBody InvestFundingBankpayMemberVo data, HttpSession session) throws Exception {
    	investFundingBankpayMemberInfoService.setBankPayMemberInfo(data);
        return true;
    }
}
