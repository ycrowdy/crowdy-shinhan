package com.ycrowdy.web.invest.vo;

import com.ycrowdy.web.common.vo.FileVo;

import lombok.Data;

@Data
public class InvestProjectRequestVo {

    private String memCode;
    
    private String pjAmount;
    
    private String pjContent;
    
    private String pjCompanyStory;
    
    private FileVo file;
}
