package com.ycrowdy.web.invest.vo;

import lombok.Data;

@Data
public class InvestDashBoardCalculateVo {

    private String pjTitle;

    private String pjStartDate;
    
    private String pjEndDate;

    private String targetAmount;

    private String pjCurrentAmount;

    private String pjRate;

    private String pjInvestorCount;

    private String ivsAssCon;

    private String assCount;

    private String assConTotalAmount;

    private String defaultAmount;
    
    private String successAmount;
    
    private String successTotalAmount;
}
 