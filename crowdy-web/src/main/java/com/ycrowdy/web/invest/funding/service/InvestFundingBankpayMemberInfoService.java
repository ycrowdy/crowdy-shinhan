package com.ycrowdy.web.invest.funding.service;

import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.ycrowdy.web.invest.vo.InvestFundingBankpayMemberVo;

@Service
public class InvestFundingBankpayMemberInfoService {
    
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private Gson gson;

    public void setBankPayMemberInfo(InvestFundingBankpayMemberVo vo) throws Exception {
        ValueOperations<String, String> valueOperations = stringRedisTemplate.opsForValue();
        valueOperations.set(vo.getId(), gson.toJson(vo), 40, TimeUnit.MINUTES);

    }
    
    public InvestFundingBankpayMemberVo getBankPayMemberInfo(String id) throws Exception {
        ValueOperations<String, String> valueOperations = stringRedisTemplate.opsForValue();
        String result = valueOperations.get(id);
        if(StringUtils.isEmpty(result)){
        	return null;
        }
        return gson.fromJson(result, InvestFundingBankpayMemberVo.class);
    }

}
