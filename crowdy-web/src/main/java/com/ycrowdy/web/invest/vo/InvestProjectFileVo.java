package com.ycrowdy.web.invest.vo;

import com.ycrowdy.web.common.vo.FileVo;

import lombok.Data;

@Data
public class InvestProjectFileVo {

    private String pjCode;
    
    private String fileTitle;
    
    private String fileName;
    
    private String fileCodeGroup;
    
    private FileVo file;
}
