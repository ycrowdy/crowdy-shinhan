package com.ycrowdy.web.invest.save.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.ycrowdy.web.common.vo.ResponseVo;
import com.ycrowdy.web.invest.save.service.InvestInsertService;
import com.ycrowdy.web.invest.vo.InvestProjectBasicVo;

@RestController
@RequestMapping("/set")
public class InvestBasicInfoSaveController {
    
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private String logInHeader  = "/InvestBasicInfoSaveController INPUT:\n";
    private String logOutHeader = "/InvestBasicInfoSaveController OUTPUT:\n";
   
    @Autowired
    private InvestInsertService investInsertService;
    
    @PostMapping("/invest/basic")
    public ResponseVo controller(HttpServletRequest request, HttpSession session,
            @RequestBody InvestProjectBasicVo data) throws Exception {
        
        Map<String, Object> result = investInsertService.setInvestBasic(data);
        return new ResponseVo(result);
    }
    
}
