package com.ycrowdy.web.invest.funding.service;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.ycrowdy.web.common.util.FrontConstants;
import com.ycrowdy.web.common.vo.CustomUser;
import com.ycrowdy.web.common.vo.ResponseVo;
import com.ycrowdy.web.data.service.DataService;
import com.ycrowdy.web.invest.vo.InvestFundingBankpayMemberVo;
import com.ycrowdy.web.member.service.MemberSessionService;
import com.ycrowdy.web.member.vo.MemberLoginResultVo;

@Service
public class InvestFundingBankpayService {
    
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private DataService dataService;

    @Autowired
    private InvestFundingBankpayMemberInfoService investFundingBankpayMemberInfoService;
    
    @Autowired
    private MemberSessionService memberSessionService;
    
	@Autowired
    protected Gson gson;

	public Map<String, Object> pay(HttpSession session, Map<String, Object> data) throws Exception {
        
		logger.info("INVEST FUNDING MOBILE BANKPAY DATA");
		logger.info("{}",  gson.toJson(data));
		logger.info("INVEST FUNDING MOBILE BANKPAY DATA");
		
		// redis id
		String id = (String) (data.get("callbackparam1"));
		// os check
		String os = (String) (data.get("callbackparam2"));
		// alias url
		String url = (String) (data.get("callbackparam3"));
		
		String hdPi = "";
		String bankpayCode = "";
		
		Map<String, Object> result = new HashMap<String, Object>();
		
		// redis에서 저장해둔 회원 정보 가져옴
		InvestFundingBankpayMemberVo vo = investFundingBankpayMemberInfoService.getBankPayMemberInfo(id);
		
		if(vo == null) {
			result.put("url", url);
			return result;
		}
		
		logger.info("InvestFundingBankpayMemberVo DATA");
		logger.info("{}",  gson.toJson(vo));
		logger.info("InvestFundingBankpayMemberVo DATA");
		
		if (os.compareTo("IOS") == 0) {
			bankpayCode = StringUtils.defaultString((String) (data.get("bankpaycode")));
			hdPi = StringUtils.defaultString((String) (data.get("bankpayvalue"))).replaceAll("\\s", "+");
		} else if(os.compareTo("ANDROID") == 0) {
			bankpayCode = StringUtils.defaultString((String) (data.get("bankpay_code")));
			hdPi = StringUtils.defaultString((String) (data.get("bankpay_value"))).replaceAll("\\s", "+");
		}
		
		vo.setHdPi(hdPi);
		
		// 000 : 성공
		if (StringUtils.isNotEmpty(bankpayCode) && bankpayCode.compareTo("000") == 0) {
			vo.setHdEpType(String.valueOf(data.get("hd_ep_type")));
			// 뱅크페이 결과 저장
			 result = dataService.getData(FrontConstants.INVEST_FUNDING_BANKPAY_RESULT, vo);
		}
		
		result.put("investorIdx", vo.getPjInvestorIdx());
		
		//세션이 없으면 로그인해준다
   	 	Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		
	   	if(!(authentication.getPrincipal() instanceof CustomUser)) {
	       	Map<String, Object> memberData = dataService.getData(FrontConstants.MEMBER_PREFIX + FrontConstants.LOGIN_INFO, vo);
	       	ResponseVo loginResponseVo = new ResponseVo(memberData);
	   	    result.put("memberData", memberData);
	
	       	MemberLoginResultVo member = gson.fromJson(gson.toJsonTree(loginResponseVo.getrData()), MemberLoginResultVo.class);
	        memberSessionService.setMemberSession(session, member);
		}
		
	    result.put("url", url);
	     
		return result;
	}

}
