package com.ycrowdy.web.invest.vo;

import lombok.Data;

@Data
public class InvestDashBoardSearchVo {
    
    private String pjCode;
    
    private String investorStatus;
    
    private String pjStartDate;
    
    private String pjEndDate;
    
}
