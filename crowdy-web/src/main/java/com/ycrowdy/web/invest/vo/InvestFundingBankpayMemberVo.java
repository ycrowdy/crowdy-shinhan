package com.ycrowdy.web.invest.vo;

import lombok.Data;

@Data
public class InvestFundingBankpayMemberVo {

    private String id;
    
    private String pjCode;
    
    private String memCode;
    
    private String pjInvestorIdx;

    private String memEmail;
    
    private String pjAmount;
    
    private String hdPi;

    private String hdEpType;
    
    private String pjAliasUrl;
    
    private String moid;
}
 