package com.ycrowdy.web.common.util;

import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;


public class CommonUtils {

    /**
     * User Password 암호화 함수이다.
     *
     * @param message
     *            암호화가 필요한 String
     * @return 암호화가 된 String
     * @throws Exception
     */
    public static String messageToCryp(String message) throws Exception {

        if (message == null || message.trim().equals("")) {
            return "";
        }

        MessageDigest md;
        byte[] mb = null;

        try {
            md = MessageDigest.getInstance("SHA-512");

            md.update(message.getBytes());
            mb = md.digest();

        } catch (NoSuchAlgorithmException nsae) {
            throw nsae;
        } catch (Exception e) {
            throw e;
        }

        String out = "";

        try {
            for (int i = 0; i < mb.length; i++) {
                byte temp = mb[i];
                String s = Integer.toHexString(new Byte(temp));
                while (s.length() < 2) {
                    s = "0" + s;
                }
                s = s.substring(s.length() - 2);
                out += s;
            }
        } catch (Exception e) {
            throw e;
        }

        return out;
    }

    /**
     * @param length 비밀번호 생성 자리수
     * @Summary 랜덤비밀번호 생성 모듈
     * @return StringBuffer
     */
    public static String randomPassword(int length){
        return RandomStringUtils.randomAlphabetic(length);
    }

    public static String passwordDecode(String password) throws Exception {
        String [] array = StringUtils.splitByWholeSeparator(password, ".");
        int bit = Integer.parseInt(array[0]) - 48;
        StringBuilder builder = new StringBuilder();

        for(int i = 1, size = array.length; i < size; i++) {
            int ascii = Integer.parseInt(array[i]);
            builder.append((char) (ascii + bit));
        }

        return builder.toString();
    }

    /**
     * 중복된 3자이상의 문자 또는 숫자
     * @param passwd
     * @return
     */
    public static boolean checkDuplicate3Character(String passwd) {
        int p = passwd.length();
        byte[] b = passwd.getBytes();

        for (int i = 0; i < ((p * 2) / 3); i++) {
            int b1 = b[i + 1];
            int b2 = b[i + 2];
 
            if ((b[i] == b1) && (b[i] == b2)) {
                return true;
            } else {
                continue;
            }
        }

        return false;
    }

    /**
     * 영문 + 숫자 + 특문 조합
     * @param passwd
     * @return
     */
    public static int digitCheck(String passwd) {

        int varDigit = 0;
        int varAlpha = 0;
        int varHex = 0;
        int varSum = 0;
        for (int i = 0; i < passwd.length(); i++) {
            char index = passwd.charAt(i);
 
            if (index >= '0' && index <= '9') {
                varDigit = 1;
            } else if ( (index >= 'a' && index <= 'z') || (index >= 'A' && index <= 'Z') ) {
                varAlpha = 1;
            } else if (index == '!' || index == '@' || index == '$'
                    || index == '%' || index == '^' || index == '&'
                    || index == '*' || index == '?') {
                varHex = 1;
            }
        }
 
        varSum = varDigit + varAlpha + varHex;

        return varSum;
    }

    public static <T> T dataConvert(Map<String, Object> map, Class<T> t) {
        return new Gson().fromJson(new Gson().toJsonTree(map), t);
    }


    public static boolean juminCheck(String number) {

        boolean checkResult = false;

        boolean isKorean = true;
        int	check	= 0;
        if( number == null || number.length() != 13 ) checkResult = false;
        if( Character.getNumericValue( number.charAt( 6 ) ) > 4 && Character.getNumericValue( number.charAt( 6 ) ) < 9 ) {
            isKorean = false;
        }
        for( int i = 0 ; i < 12 ; i++ ) {
            if( isKorean ) check += ( ( i % 8 + 2 ) * Character.getNumericValue( number.charAt( i ) ) );
            else check += ( ( 9 - i % 8 ) * Character.getNumericValue( number.charAt( i ) ) );
        }
        if( isKorean ) {
            check = 11 - ( check % 11 );
            check %= 10;
        }else {
            int remainder = check % 11;
            if ( remainder == 0 ) check = 1;
            else if ( remainder==10 ) check = 0;
            else check = remainder;

            int check2 = check + 2;
            if ( check2 > 9 ) check = check2 - 10;
            else check = check2;
        }

        if( check == Character.getNumericValue( number.charAt( 12 ) ) ) checkResult = true;
        else checkResult = false;

        return checkResult;
    }
    
	public static String getContentDispositionString(HttpServletRequest request, String filename) {
		String disposition = "";
		{
			try {
				if (request.getHeader("User-Agent").contains("MSIE")) {
					disposition = "attachment; filename="
							+ URLEncoder.encode(filename, "UTF-8").replaceAll("\\+", "%20") + ";";
				} else {
					disposition = "attachment; filename=\"" + new String(filename.getBytes("UTF-8"), "ISO-8859-1")
							+ "\"";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return disposition;
	}
}
