package com.ycrowdy.web.common.exception.controller;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CustomErrorController implements ErrorController {
    
    private static final String PATH = "/error";
    
    @RequestMapping(value = PATH)
    public String error() {
        return "redirect:/";
    }
    
    @RequestMapping(value = "/error/403")
    public String forbidenError() {
        return "redirect:/";
    }
    
    @Override
    public String getErrorPath() {
        return PATH;
    }
}
