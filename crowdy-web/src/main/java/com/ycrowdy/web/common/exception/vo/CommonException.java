package com.ycrowdy.web.common.exception.vo;

import com.ycrowdy.web.common.vo.ResponseVo;

@SuppressWarnings("serial")
public class CommonException extends Exception {
	
	private ResponseVo responseVo;
	
	public CommonException() {
	}
	public CommonException(ResponseVo responseVo) {
		this.responseVo = responseVo;
	}
	public ResponseVo getResponseVo() {
		return responseVo;
	}
	public void setResponseVo(ResponseVo responseVo) {
		this.responseVo = responseVo;
	}
	
	
}
