package com.ycrowdy.web.common.mapper;

import java.sql.SQLException;
import java.util.Map;

import org.apache.ibatis.annotations.Select;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.stereotype.Repository;

@Repository
@MapperScan
public interface UrlMapper {

    @Select("SELECT count(ptn_url) " +
            "FROM tb_partner " +
            "WHERE " +
            "ptn_url = #{url}")
    public int getCommunityUrl(Map<String, Object> data) throws SQLException;

    @Select("SELECT count(cp_alias_url) " +
            "FROM tb_campaign " +
            "WHERE " +
            "cp_alias_url = #{url}")
    public int getRewardProjectUrl(Map<String, Object> data) throws SQLException;

    @Select("SELECT count(pj_alias_url) " +
            "FROM tb_project " +
            "WHERE " +
            "pj_alias_url = #{url}")
    public int getInvestProjectUrl(Map<String, Object> data) throws SQLException;
}
