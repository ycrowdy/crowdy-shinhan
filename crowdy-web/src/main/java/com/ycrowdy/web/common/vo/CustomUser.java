package com.ycrowdy.web.common.vo;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import com.ycrowdy.web.member.vo.MemberLoginResultVo;

import lombok.Data;

@Data
public class CustomUser extends User{

    private final MemberLoginResultVo user;
    
    public CustomUser(String username, String password, Collection<? extends GrantedAuthority> authorities, MemberLoginResultVo user) {
        super(username, password, authorities);
        
        this.user = user;
    }

}
