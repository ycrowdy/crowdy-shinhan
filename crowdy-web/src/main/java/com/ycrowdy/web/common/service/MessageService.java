package com.ycrowdy.web.common.service;

import java.text.MessageFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.ycrowdy.web.common.vo.StatusVo;

@Component
public class MessageService {

	@Autowired
	private Environment env;
	
	public StatusVo getMessage(String code) {
		return new StatusVo(code, env.getProperty(code));
	}
	
	public StatusVo getMessage(String code, String param) {
		return new StatusVo(code, MessageFormat.format(env.getProperty(code), param));
	}
	
	public StatusVo getMessage(String code, String... param) {
		return new StatusVo(code, MessageFormat.format(env.getProperty(code), param));
	}
	
	public String createMessage(String message, String... param) {
		return MessageFormat.format(message, param);
	}
	
}
