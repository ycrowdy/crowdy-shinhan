package com.ycrowdy.web.common.vo;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;

import lombok.Data;

@Data
public class FileVo {
    
    private String fileData;
    
    private byte[] fileValue;
    
    private String fileName;
    
    private String fileExt;
    
    private String fileGroup;
    
    private String fileCodeGroup;
    
    private String fileCode;
    
    private String fileType;
    
    private String fileSize;
    
    private String fileInfo;
    
    private String fileIndex;

    private boolean state;
    
    public FileVo() {
        this.fileIndex = "0";
    }
    
    public void setFileData(String fileData) {
        this.fileData = fileData;
        if(StringUtils.isNotEmpty(fileData)) {
            this.fileData = fileData.split(",")[1];
            this.fileValue = Base64.decodeBase64(this.fileData);
            this.fileSize = String.valueOf(this.fileValue.length);
        }
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
        if(StringUtils.isNotEmpty(fileName)) {
            int fileExtCount = fileName.lastIndexOf(".");
            this.fileExt = fileName.substring(fileExtCount + 1, fileName.length()).toLowerCase();
        }
    }
}
