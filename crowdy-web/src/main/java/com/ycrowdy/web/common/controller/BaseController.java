package com.ycrowdy.web.common.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.ycrowdy.web.common.exception.vo.CommonException;
import com.ycrowdy.web.common.service.MessageService;
import com.ycrowdy.web.common.vo.PagingVo;
import com.ycrowdy.web.common.vo.ResponseVo;

@Component
public abstract class BaseController<T> {

    protected final String SUCCESS = "0000";
    protected final String PARAM_MISSING = "1001";

    @Autowired
    protected MessageService message;

    @Autowired
    protected Gson gson;

    public abstract ResponseVo controller(HttpServletRequest request, Map<String, Object> map, HttpSession session) throws Exception;

    public abstract T validate(HttpServletRequest request, Map<String, Object> map) throws Exception;

    public T dataConvert(Map<String, Object> map, Class<T> t) throws Exception {
        Map<String, Object> rData = (Map<String, Object>) map.get("rData");
        return gson.fromJson(gson.toJsonTree(rData), t);
    }
    
    public void headerValidate(Map<String, Object> map, boolean dataCheck) throws Exception {

        if (StringUtils.isEmpty((String) map.get("traceNo")))
            throw new CommonException(new ResponseVo(message.getMessage(PARAM_MISSING, "traceNo"), ""));
        
        if (dataCheck) {
            dataValidate(map);
        }
    }

    public void dataValidate(Map<String, Object> map) throws Exception {
        if (map.get("rData") == null)
            throw new CommonException(new ResponseVo(message.getMessage(PARAM_MISSING, "rData"), ""));
    }

    public void objectValidate(Object object, String traceNo, String... params) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = mapper.convertValue(object, Map.class);

        for (String param : params) {
            Object obj = map.get(param);
            if (obj == null)
                throw new CommonException(new ResponseVo(message.getMessage(PARAM_MISSING, param), traceNo));
        }
    }

    public void stringValidate(Object object, String traceNo, String... params) throws Exception {

        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = mapper.convertValue(object, Map.class);

        for (String param : params) {
            String requestData = (String) map.get(param);
            if (StringUtils.isEmpty(requestData))
                throw new CommonException(new ResponseVo(message.getMessage(PARAM_MISSING, param), traceNo));
        }
    }

    public void listValidate(Object object, String traceNo, String... params) throws Exception {

        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = mapper.convertValue(object, Map.class);

        for (String param : params) {
            List<Object> list = (List<Object>) map.get(param);
            if (list.isEmpty())
                throw new CommonException(new ResponseVo(message.getMessage(PARAM_MISSING, param), traceNo));
        }
    }

    public void pagingValidate(Object object, String traceNo) throws Exception {
        objectValidate(object, traceNo, "paging");

        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = mapper.convertValue(object, Map.class);
        Map<String, Object> pagingMap = (Map<String, Object>) map.get("paging");
        
        if((int) pagingMap.get("page") < 1) throw new CommonException(new ResponseVo(message.getMessage(PARAM_MISSING, "paging.page"), traceNo));
        if((int) pagingMap.get("count") < 1) throw new CommonException(new ResponseVo(message.getMessage(PARAM_MISSING, "paging.count"), traceNo));

    }
    
    public void pagingDataConvert(PagingVo vo) throws Exception {
        vo.setStartrow((vo.getPage() - 1) * vo.getCount()); 
    }
}
