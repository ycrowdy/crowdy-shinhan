package com.ycrowdy.web.common.vo;

import lombok.Data;

@Data
public class MetaVo {

    private String title;

    private String image;

    private String keywords;

    public MetaVo() {
        this.title = "";
        this.image = "";
        this.keywords = "";
    }
}
