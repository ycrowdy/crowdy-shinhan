package com.ycrowdy.web.common.interceptor;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@Component
@PropertySource(value="classpath:env/${spring.profiles.active:develop}/crowdy.properties")
public class ModelInterceptor extends HandlerInterceptorAdapter {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private Environment env;

    @Autowired
    private HashMap<String, String> titleMap;

    @Autowired
    private HashMap<String, String> descriptionMap;

    @Autowired
    private HashMap<String, String> ogDescriptionMap;

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
//        if(modelAndView != null) {
//            Map<String, Object> model = modelAndView.getModel();
//            String title = (String) model.get("title");
//            String url = (String) model.get("metaUrl");
//            String image = (String) model.get("image");
//            String description = (String) model.get("description");
//            String siteDescription = (String) model.get("siteDescription");
//
//            if(StringUtils.isEmpty(title)) {
//                model.put("title", titleMap.get("main"));
//            }
//
//            if(StringUtils.isEmpty(url)) {
//                model.put("metaUrl", env.getProperty("crowdy.url"));
//            }
//
//            if(StringUtils.isEmpty(image)) {
//                model.put("image", env.getProperty("crowdy.image"));
//            }
//
//            if(StringUtils.isEmpty(description)) {
//                model.put("description", ogDescriptionMap.get("main"));
//            }
//
//            if(StringUtils.isEmpty(siteDescription)) {
//                model.put("siteDescription", descriptionMap.get("main"));
//            }
//        }
    }
}
