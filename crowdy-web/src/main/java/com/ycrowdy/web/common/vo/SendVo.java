package com.ycrowdy.web.common.vo;

import lombok.Data;

@Data
public class SendVo {

    private String term;
    private String traceNo;
    private Object rData;
    
    public Object getrData() {
        return rData;
    }
    public void setrData(Object rData) {
        this.rData = rData;
    }
    
    
}
