package com.ycrowdy.web.common.exception.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ycrowdy.web.common.exception.vo.CommonException;
import com.ycrowdy.web.common.service.MessageService;
import com.ycrowdy.web.common.vo.ResponseVo;


@RestController
@ControllerAdvice
public class ExceptionController {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private MessageService message;
	
	@ResponseStatus(HttpStatus.OK)
	@ExceptionHandler(CommonException.class)
	public ResponseVo handleCommonException(CommonException e) {
		return e.getResponseVo();
	}
	
	@ResponseStatus(HttpStatus.OK)
	@ExceptionHandler(Exception.class)
	public ResponseVo handleException(Exception e) {
		logger.error("{}", e);
		return new ResponseVo(message.getMessage("9999"), "");
	}
}
