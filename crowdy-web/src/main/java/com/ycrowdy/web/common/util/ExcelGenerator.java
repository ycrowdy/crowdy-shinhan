package com.ycrowdy.web.common.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ExcelGenerator {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private final String XML_ENCODING = "UTF-8";
	
	private HttpServletRequest request;
	private HttpServletResponse response;
	
	private XSSFWorkbook wb;
	private XSSFSheet sheet;
	private Map<String, XSSFCellStyle> styles;
	private File template;
	private FileOutputStream os;

	private File tmp1;
	private Writer fw1;
	private SpreadsheetWriter sw;

	private String fileName;
	private int rownum = 0;
	private String sheetRef1;

	public ExcelGenerator() {
	}

	public ExcelGenerator(String fileName, HttpServletRequest request, HttpServletResponse response) throws Exception {
		this.fileName = fileName;
		this.request = request;
		this.response = response;
		setting(fileName);
	}

	private Map<String, XSSFCellStyle> createStyles(XSSFWorkbook wb) {
		Map<String, XSSFCellStyle> styles = new HashMap<String, XSSFCellStyle>();
		XSSFCellStyle hd = wb.createCellStyle();
		XSSFDataFormat fmt = wb.createDataFormat();
		XSSFFont hdfont = wb.createFont();
		hdfont.setBold(true);

		hd.setFont(hdfont);
		hd.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		hd.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		hd.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		hd.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		hd.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		hd.setBorderTop(XSSFCellStyle.BORDER_THIN);
		hd.setBorderRight(XSSFCellStyle.BORDER_THIN);
		hd.setBorderBottom(XSSFCellStyle.BORDER_THIN);

		XSSFCellStyle bd = wb.createCellStyle();
		bd.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		bd.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		bd.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		bd.setBorderTop(XSSFCellStyle.BORDER_THIN);
		bd.setBorderRight(XSSFCellStyle.BORDER_THIN);
		bd.setBorderBottom(XSSFCellStyle.BORDER_THIN);

		XSSFCellStyle curry = wb.createCellStyle();
		curry.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
		curry.setVerticalAlignment(XSSFCellStyle.ALIGN_RIGHT);
		curry.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		curry.setBorderTop(XSSFCellStyle.BORDER_THIN);
		curry.setBorderRight(XSSFCellStyle.BORDER_THIN);
		curry.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		curry.setDataFormat(fmt.getFormat("$#,##0.00"));

		styles.put("hd", hd);
		styles.put("bd", bd);
		styles.put("curry", curry);
		return styles;
	}

	private void setting(String fileName) throws Exception {
		wb = new XSSFWorkbook();
		sheet = wb.createSheet(fileName);

		styles = createStyles(wb);
		sheetRef1 = sheet.getPackagePart().getPartName().getName();
		
		template = File.createTempFile("template", ".xlsx");
		os = new FileOutputStream(template);
		wb.write(os);
		os.close();

		tmp1 = File.createTempFile(fileName, ".xml");
		fw1 = new OutputStreamWriter(new FileOutputStream(tmp1), XML_ENCODING);
		sw = new SpreadsheetWriter(fw1);
	}

	public void generateSheetHeader(String... headers) throws Exception {
		int i = 0;
		int styleIndex = styles.get("hd").getIndex();

		sw.beginSheet();

		sw.insertRow(rownum++);
		for (String header : headers) {
			sw.createCell(i++, header, styleIndex);
		}
		sw.endRow();
	}

	public void generateSheetBody(List<String> keys, List<?> data) throws Exception {
		int styleIndex = styles.get("bd").getIndex();
		
		for (int i = 0, size = data.size(); i < size; i++) {
			Object object = data.get(i);
			ObjectMapper mapper = new ObjectMapper();
			Map<String, Object> map = mapper.convertValue(object, Map.class);
			sw.insertRow(rownum++);
			for (int j = 0, keySize = keys.size(); j < keySize; j++) {
				String key = keys.get(j);
				sw.createCell(j, StringEscapeUtils.escapeXml10(String.valueOf(map.get(key))), styleIndex);
			}
			sw.endRow();
		}
	}
	
	public void generateSheetObjectBody(List<String> keys, Object data) throws Exception {
		int styleIndex = styles.get("bd").getIndex();
		
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> map = mapper.convertValue(data, Map.class);
		sw.insertRow(rownum++);
		for (int j = 0, keySize = keys.size(); j < keySize; j++) {
			String key = keys.get(j);
			sw.createCell(j, StringEscapeUtils.escapeXml10(String.valueOf(map.get(key))), styleIndex);
		}
		sw.endRow();
	}
		
	public void generateSheetChartBody(List<String> keys, List<?> dayList, List<?>... list) throws Exception {
		int styleIndex = styles.get("bd").getIndex();
		int count = 1;
		for (int i = 0; i < dayList.size(); i++) {
			sw.insertRow(rownum++);
			
			Object object = dayList.get(i);
			ObjectMapper mapper = new ObjectMapper();
			Map<String, Object> map = mapper.convertValue(object, Map.class);
			sw.createCell(0, StringEscapeUtils.escapeXml10(String.valueOf(map.get(keys.get(0)))), styleIndex);
			
			for (List<?> data : list) {
				if (data.size() == 0) {
					sw.createCell(count++, StringEscapeUtils.escapeXml10(String.valueOf(" ")), styleIndex);
				} else {
					Object valueObject = data.get(i);
					ObjectMapper valueMapper = new ObjectMapper();
					Map<String, Object> valueMap = valueMapper.convertValue(valueObject, Map.class);
					sw.createCell(count++, StringEscapeUtils.escapeXml10(String.valueOf(valueMap.get(keys.get(1)))), styleIndex);
				}
			}
			count = 1;
			sw.endRow();
		}			
	}

	public void generateSheetEnd() throws Exception {
		sw.endSheet();
		fw1.close();

		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=UTF-8");
		response.setHeader("Content-Encoding", "UTF-8");
		response.setHeader("Content-Description", "Generated Data");
		response.setHeader("Content-Disposition", CommonUtils.getContentDispositionString(request, fileName + ".xlsx"));
		response.setHeader("Set-Cookie", "fileDownload=true; path=/");
		ServletOutputStream sos = response.getOutputStream();
		substitute(template, tmp1, sheetRef1.substring(1), sos);

		template.delete();
		tmp1.delete();

	}

	private void substitute(File zipfile, File tmpfile1, String entry1, OutputStream out) throws IOException {
		ZipFile zip = new ZipFile(zipfile);
		ZipOutputStream zos = new ZipOutputStream(out);
		
		@SuppressWarnings("unchecked")
		Enumeration<ZipEntry> en = (Enumeration<ZipEntry>) zip.entries();
		while (en.hasMoreElements()) {
			ZipEntry ze = en.nextElement();
			if (!ze.getName().equals(entry1)) {
				zos.putNextEntry(new ZipEntry(ze.getName()));
				InputStream is = zip.getInputStream(ze);
				copyStream(is, zos);
				is.close();
			}
		}
		zos.putNextEntry(new ZipEntry(entry1));
		InputStream is1 = new FileInputStream(tmpfile1);
		copyStream(is1, zos);
		is1.close();

		zos.close();
		zip.close();
	}
	
	private void copyStream(InputStream in, OutputStream out) throws IOException {
		byte[] chunk = new byte[1024];
		int count;
		while ((count = in.read(chunk)) >= 0) {
			out.write(chunk, 0, count);
		}
	}
}
