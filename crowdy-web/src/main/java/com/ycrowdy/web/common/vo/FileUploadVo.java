package com.ycrowdy.web.common.vo;

import lombok.Data;

@Data
public class FileUploadVo {

    private String fileType;
    
    private FileVo file;
}
