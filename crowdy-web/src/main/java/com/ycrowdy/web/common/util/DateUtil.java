package com.ycrowdy.web.common.util;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;

public class DateUtil {

    /**
     * 남은 시간 구하기
     * @return
     * @throws ParseException
     */
    public static String getRemainHour() throws ParseException {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date()); // 오늘 날짜

        Calendar calendar2 = Calendar.getInstance();
        calendar2.add(calendar.DATE, 1); // 내일 날짜
        calendar2.setTime(DateUtils.parseDate(DateFormatUtils.format(calendar2, "yyyyMMdd"), "yyyyMMdd"));

        long todayHour = calendar2.getTimeInMillis() - calendar.getTimeInMillis();
        return String.valueOf((todayHour / 1000) / 3600);
    }
}
