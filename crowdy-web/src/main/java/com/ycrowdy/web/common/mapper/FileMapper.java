package com.ycrowdy.web.common.mapper;

import java.sql.SQLException;
import java.util.List;

import org.mybatis.spring.annotation.MapperScan;

import com.ycrowdy.web.common.vo.FileVo;
import org.springframework.stereotype.Repository;

@Repository
@MapperScan
public interface FileMapper {

    public void setFile(FileVo data) throws SQLException;
    
    public void setFileList(List<FileVo> data) throws SQLException;
    
    public void deleteFile(String fileCode) throws SQLException;
}
