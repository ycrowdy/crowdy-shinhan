package com.ycrowdy.web.common.vo;

import lombok.Data;

@Data
public class PagingVo {

    private Integer page;
    private Integer count;
    private Integer startrow;
}
