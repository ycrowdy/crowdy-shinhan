package com.ycrowdy.web.common.vo;

public class StatusVo {

	private String rCode;
	private String rMsg;
	
	public StatusVo() {
	}
	
	public StatusVo(String rCode, String rMsg) {
		this.rCode = rCode;
		this.rMsg = rMsg;
	}
	
	public String getrCode() {
		return rCode;
	}
	public void setrCode(String rCode) {
		this.rCode = rCode;
	}
	public String getrMsg() {
		return rMsg;
	}
	public void setrMsg(String rMsg) {
		this.rMsg = rMsg;
	}
	
	
}
