package com.ycrowdy.web.common.vo;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

public class ResponseVo {

    private String rMsg;
    private String rCode;
    private Object rData;

    public ResponseVo() {
    }

    public ResponseVo(StatusVo statusVo) {
        this.rCode = statusVo.getrCode();
        this.rMsg = statusVo.getrMsg();
        this.rData = new EmptyResultVo();
    }

    public ResponseVo(StatusVo statusVo, Object rData) {
        this.rCode = statusVo.getrCode();
        this.rMsg = statusVo.getrMsg();
        this.rData = rData;
    }

    public ResponseVo(Map<String, Object> data) {
        this.rCode = String.valueOf(data.get(StringUtils.lowerCase("rcode")));
        this.rMsg = String.valueOf(data.get(StringUtils.lowerCase("rmsg")));
        this.rData = data.get(StringUtils.lowerCase("rdata"));
    }

    public ResponseVo(String data) {
        this.rCode = "0000";
        this.rMsg = "";
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("data", data);
        this.rData = map;
    }

    public ResponseVo(Object obj) {
        this.rCode = "0000";
        this.rMsg = "";
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("data", obj);
        this.rData = map;
    }

    public String getrMsg() {
        return rMsg;
    }

    public void setrMsg(String rMsg) {
        this.rMsg = rMsg;
    }

    public String getrCode() {
        return rCode;
    }

    public void setrCode(String rCode) {
        this.rCode = rCode;
    }

    public Object getrData() {
        return rData;
    }

    public void setrData(Object rData) {
        this.rData = rData;
    }


}
