package com.ycrowdy.web.common.service;

import java.util.Date;
import java.util.Map;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.ycrowdy.web.common.vo.SendVo;

@Service
public class ApiCallService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private RestTemplate restTemplate;
    
    @Value("${crowdy.base.url}")
    private String baseUrl;
    
    public Map<String, Object> call(String targetApi, Object data, boolean whileCheck) throws Exception {
        
        int whileState = 1;
        
        if(whileCheck) {
            whileState = 5;
        }
        ResponseEntity<Map> result = null;
        
        do {
            whileState--;
            result = restTemplate.postForEntity(baseUrl + targetApi, data, Map.class);
            if(result.getStatusCode().compareTo(HttpStatus.OK) == 0) {
                whileState = 0;
            }
        } while (whileState > 0);
        return result.getBody();
    }
    
    private SendVo requestData(Object data) {
        
        SendVo sendVo = new SendVo();
        sendVo.setTerm("01");
        sendVo.setTraceNo(getTraceNo());
        sendVo.setrData(data);
        
        return sendVo;
    }
    
    private String getTraceNo() {
        return DateFormatUtils.format(new Date(), "yyyyMMddHHmmss") + RandomStringUtils.random(3, 0, 999, false, true);
    }
}
