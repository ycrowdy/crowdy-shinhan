package com.ycrowdy.web.common.util;

import java.io.IOException;
import java.io.Writer;
import java.util.Calendar;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;

public class SpreadsheetWriter {

	private Writer _out;
	private int _rownum;
	private List mergedRegionList;
	private final String XML_ENCODING = "UTF-8";

	public SpreadsheetWriter() {
		this.mergedRegionList = new LinkedList<>();
	}

	public SpreadsheetWriter(Writer out) {
		this._out = out;
		this.mergedRegionList = new LinkedList<>();
	}

	public void beginSheet() throws IOException {
		_out.write("<?xml version=\"1.0\" encoding=\"" + XML_ENCODING + "\"?>"
				+ "<worksheet xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\">");
		_out.write("<sheetData>\n");
	}

	public void endSheet() throws IOException {
		_out.write("</sheetData>");
		if (!mergedRegionList.isEmpty()) {
			_out.write("<mergeCells>");
			sortList(mergedRegionList);
			for (int i = 0, l = mergedRegionList.size(); i < l; i++) {
				_out.write("<mergeCell ref=\"" + mergedRegionList.get(i) + "\"/>");
			}
			_out.write("</mergeCells>");
		}
		_out.write("</worksheet>");
	}

	public void sortList(List list) {
		if (list == null)
			return;
		StringAscComparator comparator = new StringAscComparator();
		Collections.sort(list, comparator);
	}

	/**
	 * Insert a new row
	 * 
	 * @param rownum
	 *            0-based row number
	 */
	public void insertRow(int rownum) throws IOException {
		_out.write("<row r=\"" + (rownum + 1) + "\">\n");
		this._rownum = rownum;
	}

	/**
	 * Insert row end marker
	 */
	public void endRow() throws IOException {
		_out.write("</row>\n");
	}

	public void createCell(int columnIndex, String value, int styleIndex) throws IOException {
		String ref = new CellReference(_rownum, columnIndex).formatAsString();
		_out.write("<c r=\"" + ref + "\" t=\"inlineStr\"");
		if (styleIndex != -1)
			_out.write(" s=\"" + styleIndex + "\"");
		_out.write(">");
		_out.write("<is><t>" + value + "</t></is>");
		_out.write("</c>");
	}

	public void createCell(int columnIndex, String value) throws IOException {
		createCell(columnIndex, value, -1);
	}

	public void createCell(int columnIndex, double value, int styleIndex) throws IOException {
		String ref = new CellReference(_rownum, columnIndex).formatAsString();
		_out.write("<c r=\"" + ref + "\" t=\"n\"");
		if (styleIndex != -1)
			_out.write(" s=\"" + styleIndex + "\"");
		_out.write(">");
		_out.write("<v>" + value + "</v>");
		_out.write("</c>");
	}

	public void createCell(int columnIndex, double value) throws IOException {
		createCell(columnIndex, value, -1);
	}

	public void createCell(int columnIndex, Calendar value, int styleIndex) throws IOException {
		createCell(columnIndex, DateUtil.getExcelDate(value, false), styleIndex);
	}

	public void addMergedRegion(CellRangeAddress range) {
		mergedRegionList.add(range.formatAsString());
	}
}
