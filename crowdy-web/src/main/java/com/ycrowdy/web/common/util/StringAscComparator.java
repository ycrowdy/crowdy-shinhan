package com.ycrowdy.web.common.util;

import java.util.Comparator;

public class StringAscComparator implements Comparator {

	@Override
	public int compare(Object o1, Object o2) {
		return o1.toString().compareTo(o2.toString());
	}

}
