package com.ycrowdy.web.common.util;

public class FrontConstants {
    //API SUCESS CODE
    public static final String SUCCESS ="0000";
    
    //member
    public static final String MEMBER_PREFIX = "/member";
    
    public static final String LOGIN = "/login";
    public static final String LOGIN_INFO = "/login/info";
    public static final String UPDATE = "/update";
    public static final String UPDATE_CERTI = "/update-certi";
    public static final String INVESTOR_INSERT = "/investor/insert";
    public static final String INVESTOR_BASIC_UPDATE = "/investor/update/basic";
    public static final String INVESTOR_REAL_NAME_DOC_UPDATE = "/investor/update/real-name-doc";
    public static final String INVESTOR_TYPE_UPDATE = "/investor/update/type";
    public static final String INVESTOR_REPRESENTATIVE_UPDATE = "/investor/update/representative";
    public static final String INVESTOR_REL_UPDATE = "/investor/update/rel";
    

    //save
    public static final String SAVE_PREFIX = "/save";

    //reward
    public static final String REWARD_PROJECT_BASIC = "/reward/basic"; //기본정보 저장
    public static final String REWARD_PROJECT_STORY = "/reward/story"; //스토리 저장
    public static final String REWARD_PROJECT_FUND = "/reward/fund"; //제작자 정보

    //invest
    public static final String INVEST_REQUEST = "/invest/request"; //프로젝트 의뢰
    public static final String INVEST_PROJECT_BASIC = "/invest/basic"; //기본정보 저장
    public static final String INVEST_FILE = "/invest/file"; //첨부파일 저장
    public static final String INVEST_FUNDING_BANKPAY_RESULT = "/invest/funding/pay"; //뱅크페이 결과 저장 및 ksf update

    public static final String COMMUNIYT_APPLY = "/community/apply"; //커뮤니티 신청

    //view
    public static final String VIEW_PREFIX = "/view";
    public static final String REWARD_PROJECT_META = "/reward/meta"; //리워드 프로젝트 메타 정보
    public static final String INVEST_PROJECT_META = "/invest/meta"; //증권 프로젝트 메타 정보
    public static final String SIMULATION_CONTEST_META = "/simulation/contest/meta"; // 모의크라우드펀딩 대회 메타 정보
    public static final String INVEST_DASHBOARD_ASSIGN = "/invest/dashboard/assign"; // 투자 - 대시보드 - 배정내역 
    public static final String INVEST_DASHBOARD_CALCULATE = "/invest/dashboard/calculate"; // 투자 - 대시보드 - 정산내역 
    public static final String INVEST_DASHBOARD_CHART = "/invest/dashboard/chart"; // 투자 - 대시보드 - 그래프 
    public static final String REWARD_DASHBOARD_CHART = "/reward/dashboard/chart"; // 리워드 - 대시보드 - 그래프
    public static final String REWARD_DASHBOARD_SPONSOR = "/reward/dashboard/sponsor-list"; // 리워드 - 대시보드 - 후원내역
    
    //crowdy
    public static final String CROWDY_PREFIX = "/crowdy";
    public static final String NEWS_META = "/news/meta";
    public static final String PRE_OPEN_META = "/pre-project/meta";
    
}
