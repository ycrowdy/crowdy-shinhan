package com.ycrowdy.web.ga.service;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.analyticsreporting.v4.AnalyticsReporting;
import com.google.api.services.analyticsreporting.v4.AnalyticsReportingScopes;
import com.google.api.services.analyticsreporting.v4.model.ColumnHeader;
import com.google.api.services.analyticsreporting.v4.model.DateRange;
import com.google.api.services.analyticsreporting.v4.model.DateRangeValues;
import com.google.api.services.analyticsreporting.v4.model.Dimension;
import com.google.api.services.analyticsreporting.v4.model.DynamicSegment;
import com.google.api.services.analyticsreporting.v4.model.GetReportsRequest;
import com.google.api.services.analyticsreporting.v4.model.GetReportsResponse;
import com.google.api.services.analyticsreporting.v4.model.Metric;
import com.google.api.services.analyticsreporting.v4.model.MetricHeaderEntry;
import com.google.api.services.analyticsreporting.v4.model.OrFiltersForSegment;
import com.google.api.services.analyticsreporting.v4.model.Report;
import com.google.api.services.analyticsreporting.v4.model.ReportRequest;
import com.google.api.services.analyticsreporting.v4.model.ReportRow;
import com.google.api.services.analyticsreporting.v4.model.Segment;
import com.google.api.services.analyticsreporting.v4.model.SegmentDefinition;
import com.google.api.services.analyticsreporting.v4.model.SegmentDimensionFilter;
import com.google.api.services.analyticsreporting.v4.model.SegmentFilter;
import com.google.api.services.analyticsreporting.v4.model.SegmentFilterClause;
import com.google.api.services.analyticsreporting.v4.model.SimpleSegment;
import com.google.gson.Gson;
import com.ycrowdy.web.ga.vo.GAResultVo;
import com.ycrowdy.web.ga.vo.GASearchVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class GAInfoService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ServletContext servletContext;

    @Autowired
    private Gson gson;

    @Value("${ga.file}")
    private String gaCredentialFile;

    @Value("${ga.view}")
    private String gaView;

    private AnalyticsReporting analyticsReporting;

    @PostConstruct
    public void init() throws Exception {
        String path = servletContext.getRealPath(gaCredentialFile);
        JsonFactory jsonFactory = GsonFactory.getDefaultInstance();

        HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
        GoogleCredential credential = GoogleCredential
                .fromStream(new FileInputStream(path))
                .createScoped(AnalyticsReportingScopes.all());

        analyticsReporting = new AnalyticsReporting.Builder(httpTransport, jsonFactory, credential).setApplicationName("크라우디").build();
    }

    public List<GAResultVo> report(GASearchVo vo) throws Exception {
        
        SimpleDateFormat current = new SimpleDateFormat("yyyymmdd");
        SimpleDateFormat change = new SimpleDateFormat("yyyy-mm-dd");

        DateRange dateRange = new DateRange();
        dateRange.setStartDate(vo.getStartDay());
        dateRange.setEndDate(vo.getEndDay());

        Metric page = new Metric().setExpression("ga:pageviews").setAlias("pageViews");

        Dimension dimension = new Dimension().setName("ga:date");

        ReportRequest request = new ReportRequest()
                .setViewId(gaView)
                .setDateRanges(Arrays.asList(dateRange))
                .setDimensions(Arrays.asList(dimension))
                .setMetrics(Arrays.asList(page))
                .setFiltersExpression("ga:pagePath==" + vo.getPath());

        ArrayList<ReportRequest> requests = new ArrayList<ReportRequest>();
        requests.add(request);

        GetReportsRequest getReport = new GetReportsRequest().setReportRequests(requests);
        GetReportsResponse response = analyticsReporting.reports().batchGet(getReport).execute();

        List<GAResultVo> result = new ArrayList<GAResultVo>();

        for(Report report : response.getReports()) {
            ColumnHeader header = report.getColumnHeader();
            List<String> dimensionHeaders = header.getDimensions();
            List<MetricHeaderEntry> metricHeaders = header.getMetricHeader().getMetricHeaderEntries();
            List<ReportRow> rows = report.getData().getRows();
            
            if (rows != null) {
            	 for (ReportRow row : rows) {
                     List<String> dimensions = row.getDimensions();
                     List<DateRangeValues> metrics = row.getMetrics();

                     for (int i = 0; i < dimensionHeaders.size() && i < dimensions.size(); i++) {
                         GAResultVo gaResult = new GAResultVo();
                         gaResult.setDay(change.format(current.parse(dimensions.get(i))));

                         for (int j = 0; j < metrics.size(); j++) {
                             DateRangeValues values = metrics.get(j);
                             for (int k = 0; k < values.getValues().size() && k < metricHeaders.size(); k++) {
                                 gaResult.setValue(values.getValues().get(k));
                             }
                         }
                         result.add(gaResult);
                     }
                 }
			}
        }

        return result;
    }
}
