package com.ycrowdy.web.ga.controller;

import com.ycrowdy.web.ga.service.GAInfoService;
import com.ycrowdy.web.ga.vo.GAResultVo;
import com.ycrowdy.web.ga.vo.GASearchVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/ga")
public class GAController {

    @Autowired
    private GAInfoService gaInfoService;

    @PostMapping("/info")
    public List<GAResultVo> Controller(@RequestBody GASearchVo vo) throws Exception {
        return gaInfoService.report(vo);
    }
}
