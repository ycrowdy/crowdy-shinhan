package com.ycrowdy.web.ga.vo;

import lombok.Data;

@Data
public class GAResultVo {

    private String value;

    private String day;
}
