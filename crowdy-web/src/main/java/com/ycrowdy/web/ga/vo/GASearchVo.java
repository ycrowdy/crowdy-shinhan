package com.ycrowdy.web.ga.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class GASearchVo {

    private String startDay;

    private String endDay;

    private String path;

    public GASearchVo(String startDay, String endDay, String path) {
        this.startDay = startDay;
        this.endDay = endDay;
        this.path = path;
    }
    
}
