package com.ycrowdy.web.data.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ycrowdy.web.common.service.ApiCallService;

@Service
public class DataService {

    @Autowired
    private ApiCallService apiCallService;
    
    public Map<String, Object> getData(String path, Object data) throws Exception {
        return apiCallService.call(path, data, true);
    }
    
    public Map<String, Object> setData(String path, Object data) throws Exception {
        return apiCallService.call(path, data, false);
    }
}
