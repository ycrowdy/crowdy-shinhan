package com.ycrowdy.web.data.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ycrowdy.web.common.vo.ResponseVo;
import com.ycrowdy.web.data.service.DataService;

@RestController
@RequestMapping("/data")
public class DataController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private String logInHeader  = "/DataController INPUT:\n";
    private String logOutHeader = "/DataController OUTPUT:\n";
    
    @Autowired
    private DataService dataService;

    @PostMapping("/{prefix}/{api}")
    public ResponseVo controller(HttpServletRequest request, HttpSession session,
                                 @RequestBody Map<String, Object> data,
                                 @PathVariable(name="prefix") String prefix,
                                 @PathVariable(name="api") String api)
            throws Exception {

        Map<String, Object> result = dataService.getData("/" + prefix + "/" + api, data);
        return new ResponseVo(result);
    }

    @PostMapping("/{prefix}/{api}/{endPoint}")
    public ResponseVo controller(HttpServletRequest request, HttpSession session, 
            @RequestBody Map<String, Object> data, 
            @PathVariable(name="prefix") String prefix,
            @PathVariable(name="api") String api,
            @PathVariable(name="endPoint") String endPoint)
            throws Exception {

        Map<String, Object> result = dataService.getData("/" + prefix + "/" + api + "/" + endPoint, data);
        return new ResponseVo(result);
    }
    
    @PostMapping("/{prefix}/{api}/{middle}/{endPoint}")
    public ResponseVo controller(HttpServletRequest request, HttpSession session, 
            @RequestBody Map<String, Object> data, 
            @PathVariable(name="prefix") String prefix,
            @PathVariable(name="api") String api,
            @PathVariable(name="middle") String middle,
            @PathVariable(name="endPoint") String endPoint)
            throws Exception {

        Map<String, Object> result = dataService.getData("/" + prefix + "/" + api + "/" + middle + "/" + endPoint, data);
        return new ResponseVo(result);
    }

    @PostMapping("/{prefix}/{api}/{middle}/{middle2}/{endPoint}")
    public ResponseVo controller(HttpServletRequest request, HttpSession session,
                                 @RequestBody Map<String, Object> data,
                                 @PathVariable(name="prefix") String prefix,
                                 @PathVariable(name="api") String api,
                                 @PathVariable(name="middle") String middle,
                                 @PathVariable(name="middle2") String middle2,
                                 @PathVariable(name="endPoint") String endPoint)
            throws Exception {

        Map<String, Object> result = dataService.getData("/" + prefix + "/" + api + "/" + middle + "/" + middle2 + "/" + endPoint, data);
        return new ResponseVo(result);
    }
}
