package com.ycrowdy.web.auth.controller;

import com.ycrowdy.web.auth.service.NiceMobileService;
import com.ycrowdy.web.auth.vo.NiceMobileOwnerVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
public class NiceMobileOwnerController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private String logInHeader  = "/NiceMobileOwnerController INPUT:\n";
    private String logOutHeader = "/NiceMobileOwnerController OUTPUT:\n";

    @Autowired
    private NiceMobileService niceMobileService;

    @PostMapping("/mobile")
    private String controller(@RequestBody NiceMobileOwnerVo vo) throws Exception {

        String result = niceMobileService.niceMobileOwner(vo.getMobileNo());

        return result;
    }
}
