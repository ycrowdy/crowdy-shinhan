package com.ycrowdy.web.auth.vo;

import lombok.Data;

@Data
public class NiceMobileOwnerVo {

    private String mobileNo;

    private String authNo;

    private String reqSeq;
}
