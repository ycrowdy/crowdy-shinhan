package com.ycrowdy.web.auth.controller;

import com.ycrowdy.web.auth.service.NiceIdService;
import com.ycrowdy.web.common.vo.ResponseVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/auth")
public class NiceAuthController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private String logInHeader  = "/NiceAuthController INPUT:\n";
    private String logOutHeader = "/NiceAuthController OUTPUT:\n";

    @Autowired
    private NiceIdService niceIdService;

    @PostMapping("/nice/{type}")
    public ResponseVo controller(HttpServletRequest request, HttpSession session, @PathVariable(name = "type") String type) throws Exception {

        String result = niceIdService.niceAuth(request, session, type);

        return new ResponseVo(result);
    }
}
