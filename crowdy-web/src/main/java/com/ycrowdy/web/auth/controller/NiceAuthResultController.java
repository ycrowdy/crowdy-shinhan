package com.ycrowdy.web.auth.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.Gson;
import com.ycrowdy.web.auth.service.NiceIdService;

@Controller
@RequestMapping("/auth")
public class NiceAuthResultController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private String logInHeader  = "/NiceAuthResultController INPUT:\n";
    private String logOutHeader = "/NiceAuthResultController OUTPUT:\n";

    @Autowired
    private NiceIdService niceIdService;

    @Autowired
    private Gson gson;

    @PostMapping("/nice/result/{success}")
    public String controller(HttpServletRequest request,
                             HttpSession session,
                             @PathVariable(name = "success") String success,
                             @RequestParam Map<String, Object> data,
                             Model model) throws Exception {
        boolean result = niceIdService.niceAuthResult(session, data, success);
        model.addAttribute("result", result);
        model.addAttribute("callback", String.valueOf(data.get("param_r1")));
        return "/auth/result";
    }
}
