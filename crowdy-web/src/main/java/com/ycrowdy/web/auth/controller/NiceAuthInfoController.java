package com.ycrowdy.web.auth.controller;

import com.ycrowdy.web.auth.service.NiceIdService;
import com.ycrowdy.web.common.vo.ResponseVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;

@RestController
@RequestMapping("/auth")
public class NiceAuthInfoController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private String logInHeader  = "/NiceAuthInfoController INPUT:\n";
    private String logOutHeader = "/NiceAuthInfoController OUTPUT:\n";

    @Autowired
    private NiceIdService niceIdService;

    @PostMapping("/info")
    public Map<String, Object> controller(HttpSession session) throws Exception {

        Map<String, Object> result = niceIdService.getNiceAuthInfo(session);

        return result;
    }

}
