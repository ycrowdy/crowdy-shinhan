package com.ycrowdy.web.auth.service;

import NiceID.Check.CPClient;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

@Service
public class NiceIdService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${nice.person.site.code}")
    private String siteCode;

    @Value("${nice.person.site.password}")
    private String sitePassword;

    @Value("${nice.callback.success}")
    private String successCallBack;

    @Value("${nice.callback.fail}")
    private String failCallBack;

    @Autowired
    private CPClient cpClient;

    public String niceAuth(HttpServletRequest request, HttpSession session, String type) throws Exception {
//        NiceID.Check.CPClient cpClient = new NiceID.Check.CPClient();
        String sRequestNumber = cpClient.getRequestNO(siteCode);
        session.setAttribute("REQ_SEQ", sRequestNumber); // 해킹등의 방지를 위하여 세션을 쓴다면, 세션에 요청번호를 넣는다.

        String sAuthType = type.toUpperCase();  // 없으면 기본 선택화면, M: 핸드폰, C: 신용카드, X: 공인인증서
        String popgubun = "Y";  //Y : 취소버튼 있음 / N : 취소버튼 없음
        String customize = "Mobile";  //없으면 기본 웹페이지 / Mobile : 모바일페이지
        
        if (sAuthType.compareTo("W") == 0) {
        	sAuthType = "";
        	customize = "";
		}

        String sGender = ""; //없으면 기본 선택 값, 0 : 여자, 1 : 남자

        String sReturnUrl = successCallBack; // 성공시 이동될 URL
        String sErrorUrl = failCallBack; // 실패시 이동될 URL

        // 입력될 plain 데이터를 만든다.
        String sPlainData = "7:REQ_SEQ" + sRequestNumber.getBytes().length + ":" + sRequestNumber +
                "8:SITECODE" + siteCode.getBytes().length + ":" + siteCode +
                "9:AUTH_TYPE" + sAuthType.getBytes().length + ":" + sAuthType +
                "7:RTN_URL" + sReturnUrl.getBytes().length + ":" + sReturnUrl +
                "7:ERR_URL" + sErrorUrl.getBytes().length + ":" + sErrorUrl +
                "11:POPUP_GUBUN" + popgubun.getBytes().length + ":" + popgubun +
                "9:CUSTOMIZE" + customize.getBytes().length + ":" + customize +
                "6:GENDER" + sGender.getBytes().length + ":" + sGender;

        int iReturn = cpClient.fnEncode(siteCode, sitePassword, sPlainData);
        if (iReturn != 0) throw new Exception();

        String sEncData = cpClient.getCipherData();
        return sEncData;
    }

    public boolean niceAuthResult(HttpSession session, Map<String, Object> data, String returnResult) throws Exception {
        boolean result = false;
//        NiceID.Check.CPClient cpClient = new NiceID.Check.CPClient();

        String name = "";
        String mobileNo = "";
        String birthDate = "";
        String ci = "";
        String di = "";
        String sex = "M";

        String sEncodeData = requestReplace(String.valueOf(data.get("EncodeData")), "encodeData");

        int iReturn = cpClient.fnDecode(siteCode, sitePassword, sEncodeData);

        if(returnResult.compareTo("sucess") != 0) return result;
        if(iReturn != 0) return result;

        String sPlainData = cpClient.getPlainData();
        Map<String, Object> resultMap = cpClient.fnParse(sPlainData);

        String sRequestNumber = String.valueOf(resultMap.get("REQ_SEQ"));
        String requestNumberSession = String.valueOf(session.getAttribute("REQ_SEQ"));
        if(sRequestNumber.compareTo(requestNumberSession) != 0) return result;

        String type = String.valueOf(resultMap.get("AUTH_TYPE"));
        name = String.valueOf(resultMap.get("NAME"));
        birthDate = String.valueOf(resultMap.get("BIRTHDATE"));
        di = StringUtils.defaultString((String) resultMap.get("DI"));
        ci = StringUtils.defaultString((String) resultMap.get("CI"));
        String gender = String.valueOf(resultMap.get("GENDER"));

        if(gender.compareTo("2") == 0) {
            sex = "F";
        }
        if(type.compareTo("C") != 0) {
            mobileNo = String.valueOf(resultMap.get("MOBILE_NO"));
        }

        Map<String, Object> memberAuthData = new HashMap<String, Object>();
        memberAuthData.put("name", name);
        memberAuthData.put("mobileNo", mobileNo);
        memberAuthData.put("birthDate", birthDate);
        memberAuthData.put("ci", ci);
        memberAuthData.put("di", di);
        memberAuthData.put("sex", sex);

        if(type.compareTo("C") != 0) {
            memberAuthData.put("type", "1");
        } else {
            memberAuthData.put("type", "5");
        }

        session.setAttribute("memberAuthData", memberAuthData);

        result = true;

        return result;
    }

    public Map<String, Object> getNiceAuthInfo(HttpSession session) throws Exception {
        Map<String, Object> result = new HashMap<String, Object>();

        Map<String, Object> memberAuthData = (Map<String, Object>) session.getAttribute("memberAuthData");

        result.put("name", memberAuthData.get("name"));
        result.put("mobileNo", memberAuthData.get("mobileNo"));

        return result;
    }

    private String requestReplace(String paramValue, String gubun) {

        String result = "";

        if (paramValue != null) {

            paramValue = paramValue.replaceAll("<", "&lt;").replaceAll(">", "&gt;");

            paramValue = paramValue.replaceAll("\\*", "");
            paramValue = paramValue.replaceAll("\\?", "");
            paramValue = paramValue.replaceAll("\\[", "");
            paramValue = paramValue.replaceAll("\\{", "");
            paramValue = paramValue.replaceAll("\\(", "");
            paramValue = paramValue.replaceAll("\\)", "");
            paramValue = paramValue.replaceAll("\\^", "");
            paramValue = paramValue.replaceAll("\\$", "");
            paramValue = paramValue.replaceAll("'", "");
            paramValue = paramValue.replaceAll("@", "");
            paramValue = paramValue.replaceAll("%", "");
            paramValue = paramValue.replaceAll(";", "");
            paramValue = paramValue.replaceAll(":", "");
            paramValue = paramValue.replaceAll("-", "");
            paramValue = paramValue.replaceAll("#", "");
            paramValue = paramValue.replaceAll("--", "");
            paramValue = paramValue.replaceAll("-", "");
            paramValue = paramValue.replaceAll(",", "");

            if (gubun != "encodeData") {
                paramValue = paramValue.replaceAll("\\+", "");
                paramValue = paramValue.replaceAll("/", "");
                paramValue = paramValue.replaceAll("=", "");
            }

            result = paramValue;

        }
        return result;
    }
}
