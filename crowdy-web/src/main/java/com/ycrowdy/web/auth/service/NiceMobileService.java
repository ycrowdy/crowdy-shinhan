package com.ycrowdy.web.auth.service;

import CheckPlus.kisinfo.SCheckPlus;
import com.ycrowdy.web.auth.vo.NiceMobileOwnerVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class NiceMobileService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${nice.check.mobile.site.code}")
    private String siteCode;

    @Value("${nice.check.mobile.site.password}")
    private String sitePassword;

    public String niceMobileOwner(String mobileNo) throws Exception {

        String sJumin = "0000000000000"; // 주민등록번호 체크안함
        String sMobileCo = "1";// 이통사 구분 없음
        String sMobileNo = mobileNo;  // 휴대폰 번호
        String cpRequest = "";

        SCheckPlus sCheckPlus = new SCheckPlus();

        int iRtn = sCheckPlus.fnRequestSMSAuth(siteCode, sitePassword, sJumin, "", sMobileCo, sMobileNo, cpRequest);

        if(iRtn != 0) return "";

        return sCheckPlus.getResponseSEQ();
    }

    public boolean niceMobileOwnerResult(NiceMobileOwnerVo data) throws Exception {

        String sJumin = "0000000000000"; // 주민등록번호 체크안함
        String sResSeq = data.getReqSeq(); // 응답 고유번호 (CPMobileStep1 에서 확인된 cpMobile.getResponseSEQ() 데이타)
        String sAuthNo = data.getAuthNo();  // SMS 인증번호
        String cpRequest = "";

        SCheckPlus sCheckPlus = new SCheckPlus();

        int iRtn = sCheckPlus.fnRequestConfirm(siteCode, sitePassword, sResSeq, sAuthNo, sJumin, cpRequest);

        if(iRtn != 0) return false;
        if(sCheckPlus.getReturnCode().compareTo("0000") != 0) return false;

        return true;
    }
}
