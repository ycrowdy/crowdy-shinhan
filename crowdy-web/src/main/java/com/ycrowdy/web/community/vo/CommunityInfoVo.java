package com.ycrowdy.web.community.vo;

import com.ycrowdy.web.common.vo.FileVo;

import lombok.Data;

@Data
public class CommunityInfoVo {

    private String communityIdx;

    private String communityMemCode;

    private String communityType;

    private String communityUrl;

    private String communityName;

    private String communityInfo;

    private String communityImg;

    private FileVo communityImgFile;

    private String communityBackgroundImg;

    private FileVo communityBackgroundImgFile;

    private String communityCurrentAmount;

    private String communityCpCount;

    private String communityEmail;

    private String communityFaceBook;

    private String commnunityWebSite;

    private String communityInstagram;

    private String communityBlog;

    private String communityStatus;
}
