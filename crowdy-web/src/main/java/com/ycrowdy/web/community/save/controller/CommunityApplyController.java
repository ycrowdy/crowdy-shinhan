package com.ycrowdy.web.community.save.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ycrowdy.web.common.vo.ResponseVo;
import com.ycrowdy.web.community.save.service.CommunityInsertService;
import com.ycrowdy.web.community.vo.CommunityInfoVo;

@RestController
@RequestMapping("/set")
public class CommunityApplyController {
    
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private String logInHeader  = "/CommunityApplyController INPUT:\n";
    private String logOutHeader = "/CommunityApplyController OUTPUT:\n";
    
    @Autowired
    private CommunityInsertService communityInsertService;
    
    @PostMapping("/community/apply")
    public ResponseVo controller(HttpServletRequest request, HttpSession session,
            @RequestBody CommunityInfoVo data) throws Exception {
        
        Map<String, Object> result = communityInsertService.setCommunityApply(data);
        
        return new ResponseVo(result);
    }
}
