package com.ycrowdy.web.community.save.service;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ycrowdy.web.common.mapper.UrlMapper;
import com.ycrowdy.web.file.service.FileService;
import com.ycrowdy.web.common.util.FrontConstants;
import com.ycrowdy.web.community.vo.CommunityInfoVo;
import com.ycrowdy.web.data.service.DataService;

@Service
public class CommunityInsertService {
    
    @Autowired
    private DataService dataService;
    
    @Autowired
    private FileService fileService;
    
    @Autowired
    private UrlMapper urlMapper;
    
    public boolean getCommunityUrlCheck(Map<String, Object> data) throws Exception {
        int count = urlMapper.getCommunityUrl(data);
        return count > 0 ? false : true;
    }
    
    public Map<String, Object> setCommunityApply(CommunityInfoVo vo) throws Exception {
        
        //1. 썸네일 이미지 처리
        if(vo.getCommunityImgFile() != null && StringUtils.isNotEmpty(vo.getCommunityImgFile().getFileData())) {
           String communityImg = fileService.saveAwsFile(vo.getCommunityImgFile());
           vo.setCommunityImg(communityImg);
           vo.setCommunityImgFile(null);
        }
        
        //2. 배너 이미지 처리
        if(vo.getCommunityBackgroundImgFile()!= null && StringUtils.isNotEmpty(vo.getCommunityBackgroundImgFile().getFileData())) {
            String communityBackgroundImg = fileService.saveAwsFile(vo.getCommunityBackgroundImgFile());
            vo.setCommunityBackgroundImg(communityBackgroundImg);
            vo.setCommunityBackgroundImgFile(null);
         }
        
        return dataService.setData(FrontConstants.SAVE_PREFIX + FrontConstants.COMMUNIYT_APPLY, vo);
    }
}
