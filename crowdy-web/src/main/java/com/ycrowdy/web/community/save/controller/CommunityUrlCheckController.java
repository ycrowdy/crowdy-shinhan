package com.ycrowdy.web.community.save.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ycrowdy.web.community.save.service.CommunityInsertService;

@RestController
@RequestMapping("/data")
public class CommunityUrlCheckController {
    
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private String logInHeader  = "/CommunityUrlCheckController INPUT:\n";
    private String logOutHeader = "/CommunityUrlCheckController OUTPUT:\n";
    
    @Autowired
    private CommunityInsertService communityInsertService;
    
    @PostMapping("/community/url")
    public boolean controller(HttpServletRequest request, HttpSession session,
            @RequestBody Map<String, Object> data) throws Exception {
        
        return communityInsertService.getCommunityUrlCheck(data);
    }
    
}
