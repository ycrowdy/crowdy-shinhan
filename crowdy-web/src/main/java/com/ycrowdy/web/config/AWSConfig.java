package com.ycrowdy.web.config;

import javax.annotation.PostConstruct;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;

@Configuration
public class AWSConfig {

    @Autowired
    ApplicationContext ctx;

    @Autowired
    private Environment env;

    @PostConstruct
    public void init() {
        env = ctx.getEnvironment();
    }

    @Bean
    public AmazonS3Client amazonS3Client() {

        AWSCredentials credentials = new BasicAWSCredentials(env.getProperty("aws.access"), env.getProperty("aws.secret"));
        AmazonS3Client amazonS3Client = new AmazonS3Client(credentials);
        amazonS3Client.setRegion(Region.getRegion(Regions.AP_NORTHEAST_2));

        return amazonS3Client;
    }
}
