package com.ycrowdy.web.config;

public class Profiles {
    public final static String REAL = "real";
    public final static String DEVELOP = "develop";
    public final static String LOCAL = "local";
}
