package com.ycrowdy.web.config;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import NiceID.Check.CPClient;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.support.ErrorPageFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.web.client.RestTemplate;

@Configuration
public class CommonConfig {

    @Bean
    public RestTemplate restTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
        return restTemplate;
    }
    
    @Bean
    public TaskScheduler taskScheduler() {
        return new ConcurrentTaskScheduler(); //single threaded by default
    }
    
    @Bean
    public ErrorPageFilter errorPageFilter() {
        return new ErrorPageFilter();
    }

    @Bean
    public FilterRegistrationBean disableSpringBootErrorFilter(ErrorPageFilter filter) {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        filterRegistrationBean.setFilter(filter);
        filterRegistrationBean.setEnabled(false);
        return filterRegistrationBean;
    }

    @Bean
    public Gson gson() {
        return new GsonBuilder().setPrettyPrinting().create();
    }

    @Bean("titleMap")
    public HashMap<String, String> titleMap() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("main", "크라우드펀딩 | 크라우디");
        map.put("reward", "크라우디, 새로운 취미가 되다 | 크라우디");
        map.put("invest", "스타트업 명품관 | 크라우디");
        map.put("community", "커뮤니티 파트너 | 크라우디");
        map.put("detail", "{0} | 크라우디");
        map.put("simulation", "모의 펀딩 | 크라우디");
        map.put("make", "프로젝트 시작하기 | 크라우디");
        map.put("faq", "도움말 | 크라우디");
        map.put("news", "NEWS | 크라우디");
        map.put("news-detail", "{0} - NEWS | 크라우디");
        map.put("pre-detail", "{0} - 사전공개 | 크라우디");
        return map;
    }

    @Bean("descriptionMap")
    public HashMap<String, String> descriptionMap() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("main", "크라우드펀딩, 멋진 아이디어, 명품 스타트업, 스타트업 투자, 영화투자");
        map.put("reward", "크라우드펀딩, 멋진 아이디어, 명품 스타트업, 스타트업 투자, 영화투자");
        map.put("invest", "크라우드펀딩, 멋진 아이디어, 명품 스타트업, 스타트업 투자, 영화투자");
//        map.put("invest-detail", "명품 스타트업에 투자하세요");
//        map.put("detail", "멋진 아이디어에 펀딩하세요");
        return map;
    }

    @Bean("ogDescriptionMap")
    public HashMap<String, String> ogDescriptionMap() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("main", "크라우드펀딩, 멋진 아이디어, 명품 스타트업, 스타트업 투자, 영화투자");
        map.put("reward", "크라우드펀딩, 멋진 아이디어, 명품 스타트업, 스타트업 투자, 영화투자");
        map.put("invest", "크라우드펀딩, 멋진 아이디어, 명품 스타트업, 스타트업 투자, 영화투자");
//        map.put("invest-detail", "명품 스타트업에 투자하세요");
//        map.put("detail", "멋진 아이디어에 펀딩하세요");
        return map;
    }

    @Bean
    public CPClient cpClient() {
        return new CPClient();
    }
}
