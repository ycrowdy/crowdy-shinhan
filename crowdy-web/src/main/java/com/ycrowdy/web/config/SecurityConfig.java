package com.ycrowdy.web.config;

import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.access.AccessDeniedHandler;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/resources/**").antMatchers("/views/**");

    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .csrf().disable()
            .exceptionHandling()
                .accessDeniedHandler(accessDeniedHandler())
                .accessDeniedPage("/")
                .and()
            .authorizeRequests()
                .antMatchers("/make/invest/**").authenticated()
                .antMatchers("/make/reward/**").authenticated()
                .antMatchers("/reward/funding/**").authenticated()
//                .antMatchers("/mypage/**").authenticated()
                .anyRequest().permitAll()
                .and()
            .logout()
                .logoutUrl("/logout")
                .logoutSuccessUrl("/")
                .invalidateHttpSession(true)
                .and()
//            .sessionManagement()
//                .invalidSessionUrl("/")
//                .and()
            .headers()
                .frameOptions().disable()
                .and()
            .httpBasic().disable();
    }

    @Bean
    public AccessDeniedHandler accessDeniedHandler() {
        return new CustomAccessDeniedHandler();
    }
}
