package com.ycrowdy.web.config;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
@MapperScan(value="com.ycrowdy.web.**.mapper")
@EnableTransactionManagement
@PropertySource(value="classpath:env/${spring.profiles.active:develop}/database.properties")
public class DataSourceConfig {

    @Autowired
    ApplicationContext ctx;

    @Autowired
    private Environment env;

    @PostConstruct
    public void init(){
        env = ctx.getEnvironment();
    }

    @Bean(name = "dataSource", destroyMethod = "shutdown")
    public DataSource dataSource() throws Exception {

        HikariConfig config = new HikariConfig();
        config.setDriverClassName(env.getProperty("db.driver"));
        config.setJdbcUrl(env.getProperty("db.url"));
        config.setUsername(env.getProperty("db.user"));
        config.setPassword(env.getProperty("db.passwd"));
        config.addDataSourceProperty("prepStmtCacheSize", Integer.valueOf(env.getProperty("db.prepStmtCacheSize")));
        config.addDataSourceProperty("prepStmtCacheSqlLimit",
                Integer.valueOf(env.getProperty("db.prepStmtCacheSqlLimit")));
        config.addDataSourceProperty("cachePrepStmts", Boolean.valueOf(env.getProperty("db.cachePrepStmts")));
        config.addDataSourceProperty("useServerPrepStmts", Boolean.valueOf(env.getProperty("db.useServerPrepStmts")));

        HikariDataSource dataSource = new HikariDataSource(config);

        System.out.println("\n\ndatasource\n\n");

        return dataSource;
    }

    @Bean
    public DataSourceTransactionManager transactionManager() throws Exception {
        System.out.println("\n\ntransactionManager\n\n");

        DataSourceTransactionManager source = new DataSourceTransactionManager(dataSource());
        source.setRollbackOnCommitFailure(true);
        return source;
    }

    @Bean(destroyMethod = "clearCache")
    public SqlSessionTemplate sqlSession(SqlSessionFactory sqlSessionFactory) throws Exception {
        System.out.println("\n\nsqlSession\n\n");

        SqlSessionTemplate sessionTemplate = new SqlSessionTemplate(sqlSessionFactory);
        return sessionTemplate;
    }

    @Bean
    public SqlSessionFactory sqlSessionFactory() throws Exception {
        System.out.println("\n\nsqlSessionFactory\n\n");

        SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory
                .setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:query/**/*.xml"));
        return sessionFactory.getObject();
    }
}
