package com.ycrowdy.web.member.controller;

import com.ycrowdy.web.common.vo.ResponseVo;
import com.ycrowdy.web.member.service.MemberInsertService;
import com.ycrowdy.web.member.vo.MemberInvestorVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

@RestController
@RequestMapping("/member")
public class MemberInvestorInsertController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private String logInHeader  = "/MemberInvestorInsertController INPUT:\n";
    private String logOutHeader = "/MemberInvestorInsertController OUTPUT:\n";

    @Autowired
    private MemberInsertService memberInsertService;

    @PostMapping("/investor/insert")
    public ResponseVo controller(HttpServletRequest request, @RequestBody MemberInvestorVo data, HttpSession session) throws Exception {

        Map<String, Object> result = memberInsertService.memberInvestorInsert(data, session);

        return new ResponseVo(result);
    }
}
