package com.ycrowdy.web.member.service;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.ycrowdy.web.common.util.FrontConstants;
import com.ycrowdy.web.common.vo.ResponseVo;
import com.ycrowdy.web.data.service.DataService;
import com.ycrowdy.web.member.vo.MemberLoginResultVo;

@Service
public class MemberLoginService {
    
    @Autowired
    private DataService dataService;
    
    @Autowired
    private MemberSessionService memberSessionService;
    
    @Autowired
    protected Gson gson;
    
    public Map<String, Object> memberLogin(HttpSession session, Map<String, Object> data) throws Exception {
        
        Map<String, Object> result = dataService.getData(FrontConstants.MEMBER_PREFIX + FrontConstants.LOGIN, data);
        ResponseVo responseVo = new ResponseVo(result);
        
        if(responseVo.getrCode().compareTo(FrontConstants.SUCCESS) == 0) {
            MemberLoginResultVo member = gson.fromJson(gson.toJsonTree(responseVo.getrData()), MemberLoginResultVo.class);
            memberSessionService.setMemberSession(session, member);
        }
        
        return result;
    }
}
