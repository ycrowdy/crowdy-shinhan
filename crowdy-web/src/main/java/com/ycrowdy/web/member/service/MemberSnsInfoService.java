package com.ycrowdy.web.member.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

@Service
public class MemberSnsInfoService {
    
    @Value("${sns.facebook.app.id}")
    private String facebookAppId;

    @Value("${sns.facebook.app.version}")
    private String facebookAppVersion;
    
    @Value("${sns.naver.app.id}")
    private String naverAppId;

    @Value("${sns.naver.app.domain}")
    private String naverAppDomain;
    
    @Value("${sns.naver.app.join.redirecturi}")
    private String naverAppJoinRedirectUrl;
    
    @Value("${sns.naver.app.login.redirecturi}")
    private String naverAppLoginRedirectUrl;
    
    @Autowired
    protected Gson gson;
    
    public Map<String, Object> memberSnsInfo() throws Exception {
        
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("facebookAppId", facebookAppId);
        result.put("facebookAppVersion", facebookAppVersion);
        result.put("naverAppId", naverAppId);
        result.put("naverAppDomain", naverAppDomain);
        result.put("naverAppJoinRedirectUrl", naverAppJoinRedirectUrl);
        result.put("naverAppLoginRedirectUrl", naverAppLoginRedirectUrl);
        
        return result;
    }
}
