package com.ycrowdy.web.member.vo;

import lombok.Data;

@Data
public class MemberInvestorBasicVo {

    private String memCode;

    private String memType;

    private String memIvsPostNum;

    private String memIvsAddr1;

    private String memIvsAddr2;

    private String memIvsSecurCode;

    private String memIvsSecurNo;
    
    private String memIvsBankCode;
    
    private String memIvsBankNo;

    private String memIvsTellNo;

    private String memIvsRegiHpNo;

    private boolean authChange;

    private MemberCertiVo memberCerti;
}
