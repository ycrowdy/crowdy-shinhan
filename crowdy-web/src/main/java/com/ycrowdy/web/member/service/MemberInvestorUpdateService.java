package com.ycrowdy.web.member.service;

import com.ycrowdy.web.common.util.FrontConstants;
import com.ycrowdy.web.data.service.DataService;
import com.ycrowdy.web.file.service.FileService;
import com.ycrowdy.web.member.vo.MemberCertiVo;
import com.ycrowdy.web.member.vo.MemberInvestorBasicVo;
import com.ycrowdy.web.member.vo.MemberInvestorRealNameVo;
import com.ycrowdy.web.member.vo.MemberInvestorRelVo;
import com.ycrowdy.web.member.vo.MemberInvestorRepresentativeVo;
import com.ycrowdy.web.member.vo.MemberInvestorTypeVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.Map;

@Service
public class MemberInvestorUpdateService {

    @Autowired
    private DataService dataService;

    @Autowired
    private FileService fileService;

    public Map<String, Object> memberInvestorBasicUpdate(MemberInvestorBasicVo vo, HttpSession session) throws Exception {
        if(vo.isAuthChange()) {
            Map<String, Object> memberAuthData = (Map<String, Object>) session.getAttribute("memberAuthData");
            vo.setMemberCerti(new MemberCertiVo(vo.getMemCode(), vo.getMemIvsRegiHpNo(), memberAuthData));
        }

        return dataService.getData(FrontConstants.MEMBER_PREFIX + FrontConstants.INVESTOR_BASIC_UPDATE, vo);
    }

    public Map<String, Object> memberInvestorRealNameDocUpdate(MemberInvestorRealNameVo vo, HttpSession session) throws Exception {
        if(StringUtils.isEmpty(vo.getMemIvsRnmDoc())) {
            String memIvsRnmDoc = fileService.saveCrowdyFile(vo.getMemIvsRnmFile(), "mem_ivs_doc", "F0003", false);
            if(StringUtils.isNotEmpty(memIvsRnmDoc)) {
                vo.setMemIvsRnmDoc(memIvsRnmDoc);
                vo.setMemIvsRnmFile(null);
            }
        }

        if(StringUtils.isEmpty(vo.getMemIvsRnmAttorney())) {
            String memIvsRnmAttorney = fileService.saveCrowdyFile(vo.getMemIvsRnmAttorneyFile(), "mem_ivs_doc", "F0003", false);
            if(StringUtils.isNotEmpty(memIvsRnmAttorney)) {
                vo.setMemIvsRnmAttorney(memIvsRnmAttorney);
                vo.setMemIvsRnmAttorneyFile(null);
            }
        }

        if(StringUtils.isEmpty(vo.getMemIvsCorDoc())) {
            String memIvsCorDoc = fileService.saveCrowdyFile(vo.getMemIvsCorFile(), "mem_ivs_doc", "F0003", false);
            if(StringUtils.isNotEmpty(memIvsCorDoc)) {
                vo.setMemIvsCorDoc(memIvsCorDoc);
                vo.setMemIvsCorFile(null);
            }
        }

        if(StringUtils.isEmpty(vo.getMemIvsRnmCorDoc())) {
            String memIvsRnmCorDoc = fileService.saveCrowdyFile(vo.getMemIvsRnmCorFile(), "mem_ivs_doc", "F0003", false);
            if(StringUtils.isNotEmpty(memIvsRnmCorDoc)) {
                vo.setMemIvsRnmCorDoc(memIvsRnmCorDoc);
                vo.setMemIvsRnmCorFile(null);
            }
        }

        if(StringUtils.isEmpty(vo.getMemIvsCorSeal())) {
            String memIvsCorSeal = fileService.saveCrowdyFile(vo.getMemIvsCorSealFile(), "mem_ivs_doc", "F0003", false);
            if(StringUtils.isNotEmpty(memIvsCorSeal)) {
                vo.setMemIvsCorSeal(memIvsCorSeal);
                vo.setMemIvsCorSealFile(null);
            }
        }

        if(StringUtils.isEmpty(vo.getMemIvsCorAttorney())) {
            String memIvsCorAttorney = fileService.saveCrowdyFile(vo.getMemIvsCorAttorneyFile(), "mem_ivs_doc", "F0003", false);
            if(StringUtils.isNotEmpty(memIvsCorAttorney)) {
                vo.setMemIvsCorAttorney(memIvsCorAttorney);
                vo.setMemIvsCorAttorneyFile(null);
            }
        }

        if(StringUtils.isEmpty(vo.getMemIvsBankImg())) {
            String memIvsBankImg = fileService.saveCrowdyFile(vo.getMemIvsBankFile(), "mem_ivs_doc", "F0003", false);
            if(StringUtils.isNotEmpty(memIvsBankImg)) {
                vo.setMemIvsBankImg(memIvsBankImg);
                vo.setMemIvsBankFile(null);
            }
        }

        return dataService.getData(FrontConstants.MEMBER_PREFIX + FrontConstants.INVESTOR_REAL_NAME_DOC_UPDATE, vo);
    }

    public Map<String, Object> memberInvestorTypeUpdate(MemberInvestorTypeVo vo, HttpSession session) throws Exception {
        if(vo.getMemIvsType().compareTo("MIT001") != 0) { //적격투자자, 전문투자자 증명서류 파일 업로드
            String fileName = fileService.saveCrowdyFiles(vo.getMemIvsFiles(), vo.getMemIvsDoc(),"mem_ivs_doc", "F0003", false);
            vo.setMemIvsDoc(fileName);
            vo.setMemIvsFiles(null);
        } else {
            vo.setMemIvsDoc("");
        }

        return dataService.getData(FrontConstants.MEMBER_PREFIX + FrontConstants.INVESTOR_TYPE_UPDATE, vo);
    }

    public Map<String, Object> memberInvestorRepresentativeUpdate(MemberInvestorRepresentativeVo vo, HttpSession session) throws Exception {
        if(vo.isAuthChange()) {
            Map<String, Object> memberAuthData = (Map<String, Object>) session.getAttribute("memberAuthData");
            vo.setMemberCerti(new MemberCertiVo(vo.getMemCode(), vo.getMemIvsRegiHpNo(), memberAuthData));
        }

        return dataService.getData(FrontConstants.MEMBER_PREFIX + FrontConstants.INVESTOR_REPRESENTATIVE_UPDATE, vo);
    }

    public Map<String, Object> memberInvestorRelUpdate(MemberInvestorRelVo vo, HttpSession session) throws Exception {
        if(StringUtils.isEmpty(vo.getMemIvsRelDoc())) {
            String fileName = fileService.saveCrowdyFile(vo.getMemIvsRelFile(), "mem_ivs_rel_doc", "F0003", false);
            vo.setMemIvsRelDoc(fileName);
            vo.setMemIvsRelFile(null);
        }

        return dataService.getData(FrontConstants.MEMBER_PREFIX + FrontConstants.INVESTOR_REL_UPDATE, vo);
    }
}
