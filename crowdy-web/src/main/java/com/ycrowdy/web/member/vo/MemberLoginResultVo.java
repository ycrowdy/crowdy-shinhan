package com.ycrowdy.web.member.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class MemberLoginResultVo {

    private String memCode;
    
    private String memName;
    
    private String memPwd;
    
    private String memPwdConfirm;
    
    private String memEmail;
    
    private String memNameConfirm;
    
    private String memShotImg;
    
    private String memComImg;
    
    private String mobileNo;
    
    private String partnerIdx;
    
    private String memStatus;
    
    private String memType;
    
    private String memInvestor;
    
    private String memSpsPostNum;
    
    private String memSpsAddr1;
    
    private String memSpsAddr2;
}
