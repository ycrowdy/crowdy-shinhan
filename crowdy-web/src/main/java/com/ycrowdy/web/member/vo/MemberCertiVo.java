package com.ycrowdy.web.member.vo;

import lombok.Data;

import java.util.Map;

@Data
public class MemberCertiVo {

    private String memCode;

    private String memName;

    private String memTelNo;

    private String memSex;

    private String memBirth;

    private String memCi;

    private String memDi;

    private String memNameType;

    public MemberCertiVo(){}

    public MemberCertiVo(String memCode, String memName, String memTelNo, String memSex, String memBirth, String memCi, String memDi, String memNameType) {
        this.memCode = memCode;
        this.memName = memName;
        this.memTelNo = memTelNo;
        this.memSex = memSex;
        this.memBirth = memBirth;
        this.memCi = memCi;
        this.memDi = memDi;
        this.memNameType = memNameType;
    }

    public MemberCertiVo(String memCode, String memTelNo, Map<String, Object> data) {
        this.memCode = memCode;
        this.memTelNo = memTelNo;
        this.memName = (String) data.get("name");
        this.memSex = (String) data.get("sex");
        this.memBirth = (String) data.get("birthDate");
        this.memCi = (String) data.get("ci");
        this.memDi = (String) data.get("di");
        this.memNameType = (String) data.get("type");
    }
}
