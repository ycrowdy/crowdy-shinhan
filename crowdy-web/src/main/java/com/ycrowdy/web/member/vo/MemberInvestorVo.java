package com.ycrowdy.web.member.vo;

import com.ycrowdy.web.common.vo.FileVo;
import lombok.Data;

import java.util.List;

@Data
public class MemberInvestorVo {

    private String memCode;

    private String memType;

    private String memIvsCompanyName;

    private String memIvsTellNo;

    private String memIvsComNo;

    private String memIvsCorNo;

    private String memIvsPostNum;

    private String memIvsAddr1;

    private String memIvsAddr2;

    private String memIvsSecurCode;

    private String memIvsSecurNo;
    
    private String memIvsBankCode;
    
    private String memIvsBankNo;

    private String memIvsCeo;

    private String memIvsStockType;

    private String memIvsRegiName;

    private String memIvsRegiHpNo;

    private String memIvsRegiCi;

    private String memIvsRegiDi;

    private String memIvsBirth;

    private String memIvsRegiCode;

    private String memIvsSex;

    private String memIvsNameType;

    private String memIvsType;

    private String memIvsState;

    private String memIvsDoc;

    private List<FileVo> memIvsFiles;

    private String memIvsRelStatus;

    private String memIvsRelComNo;

    private String memIvsRelDoc;

    private FileVo memIvsRelFile;

    private String memIvsRnmDoc;

    private FileVo memIvsRnmFile;

    private String memIvsRnmAttorney;

    private FileVo memIvsRnmAttorneyFile;

    private String memIvsCorDoc;

    private FileVo memIvsCorFile;

    private String memIvsRnmCorDoc;

    private FileVo memIvsRnmCorFile;

    private String memIvsCorSeal;

    private FileVo memIvsCorSealFile;

    private String memIvsCorAttorney;

    private FileVo memIvsCorAttorneyFile;

    private String memIvsBankImg;

    private FileVo memIvsBankFile;

    private String memIvsResName;

    private String memIvsResEmail;

    private MemberCertiVo memberCerti;

    public MemberInvestorVo() {
        this.memCode = "";
        this.memType = "";
        this.memIvsCompanyName = "";
        this.memIvsTellNo = "";
        this.memIvsComNo = "";
        this.memIvsCorNo = "";
        this.memIvsPostNum = "";
        this.memIvsAddr1 = "";
        this.memIvsAddr2 = "";
        this.memIvsSecurCode = "";
        this.memIvsSecurNo = "";
        this.memIvsCeo = "";
        this.memIvsStockType = "";
        this.memIvsRegiName = "";
        this.memIvsRegiHpNo = "";
        this.memIvsRegiCi = "";
        this.memIvsRegiDi = "";
        this.memIvsBirth = "";
        this.memIvsRegiCode = "";
        this.memIvsSex = "";
        this.memIvsNameType = "";
        this.memIvsType = "";
        this.memIvsState = "";
        this.memIvsDoc = "";
        this.memIvsRelStatus = "";
        this.memIvsRelComNo = "";
        this.memIvsRelDoc = "";
        this.memIvsRnmDoc = "";
        this.memIvsRnmAttorney = "";
        this.memIvsCorDoc = "";
        this.memIvsRnmCorDoc = "";
        this.memIvsCorSeal = "";
        this.memIvsCorAttorney = "";
        this.memIvsBankImg = "";
        this.memIvsResName = "";
        this.memIvsResEmail = "";
    }
}
