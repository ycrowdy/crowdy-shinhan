package com.ycrowdy.web.member.vo;

import com.ycrowdy.web.common.vo.FileVo;
import lombok.Data;

@Data
public class MemberInvestorRealNameVo {

    private String memCode;

    private String memIvsRnmDoc;

    private FileVo memIvsRnmFile;

    private String memIvsRnmDocStatus;

    private String memIvsRnmAttorney;

    private FileVo memIvsRnmAttorneyFile;

    private String memIvsCorDoc;

    private FileVo memIvsCorFile;

    private String memIvsRnmCorDoc;

    private FileVo memIvsRnmCorFile;

    private String memIvsCorSeal;

    private FileVo memIvsCorSealFile;

    private String memIvsCorAttorney;

    private FileVo memIvsCorAttorneyFile;

    private String memIvsBankImg;

    private FileVo memIvsBankFile;

    public MemberInvestorRealNameVo() {
        this.memCode = "";
        this.memIvsRnmDoc = "";
        this.memIvsRnmDocStatus = "";
        this.memIvsRnmAttorney = "";
        this.memIvsCorDoc = "";
        this.memIvsRnmCorDoc = "";
        this.memIvsCorSeal = "";
        this.memIvsCorAttorney = "";
        this.memIvsBankImg = "";
    }
}
