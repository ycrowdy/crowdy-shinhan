package com.ycrowdy.web.member.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ycrowdy.web.common.vo.ResponseVo;
import com.ycrowdy.web.member.service.MemberUpdateService;
import com.ycrowdy.web.member.vo.MemberCertiVo;

@RestController
@RequestMapping("/member")
public class MemberUpdateCertiController {
    
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private String logInHeader  = "/MemberUpdateCertiController INPUT:\n";
    private String logOutHeader = "/MemberUpdateCertiController OUTPUT:\n";
    
    @Autowired
    private MemberUpdateService memberUpdateService;
    
    @PostMapping("/update-certi")
    public ResponseVo controller(HttpServletRequest request, @RequestBody MemberCertiVo data, HttpSession session) throws Exception {
        
        Map<String, Object> result = memberUpdateService.memberCertiUpdate(data, session);
        
        return new ResponseVo(result);
    }
}
