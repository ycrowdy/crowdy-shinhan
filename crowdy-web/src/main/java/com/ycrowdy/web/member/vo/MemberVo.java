package com.ycrowdy.web.member.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.ycrowdy.web.common.vo.FileVo;

import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class MemberVo {

     /*
     * 사용자 코드
     * */
    private String memCode;

     /*
     * SNS로 가입 여부, Y : SNS로가입 / N : 일반회원가입
     * */
    private String sns;

     /*
     * SNS 토큰
     * */
    private String snsTokne;

     /*
     * SNS 가입 타입, 0:일반 / 1:페이스북 / 2:트위터 / 3:카카오톡 / 4:모아 / 5:네이버
     * */
    private String snsType;

     /*
     * SNS ID
     * */
    private String snsId;

     /*
     * 사용자 이름
     * */
    private String memName;

     /*
     * 사용자 이메일
     * */
    private String memEmail;

     /*
     * 사용자 비밀번호
     * */
    private String memPwd;

     /*
     * 사용자 가입 구분, 1:개인 / 2:법인 / 9:챔피언
     * */
    private String memType;

     /*
     * 사용자 상태, 0:대기 / 1:가입완료 / 2:중지 / 3:비회원 / 9:탈퇴
     * */
    private String memStatus;

     /*
     * 사업자번호 / 조합번호
     * */
    private String bizNum;

     /*
     * 담당자 이름
     * */
    private String resName;

     /*
     * 사용자 프로필 이미지
     * */
    private String memShotImg;
    

    /*
    * 사용자 프로필 이미지
    * */
   private FileVo memShotImgFile;

     /*
     * 휴대폰 번호
     * */
    private String mobileNo;

     /*
     * 실명인증여부, N : 미인증 / Y : 인증
     * */
    private String memNameConfirm;

     /*
     * 우편번호
     * */
    private String memSpsPostNum;

     /*
     * 주소
     * */
    private String memSpsAddr1;

     /*
     * 상세주소
     * */
    private String memSpsAddr2;

    /**
     * 본인인증 여부
     */
    private boolean authChange;

    /**
     * 실명인증 정보
     */
    private MemberCertiVo memberCerti;
    
    /**
     * 마케팅 수신 동의
     */
    private String marketingAgree;

    public MemberVo() {
        this.memNameConfirm = "N";
    }
    
    public MemberVo(String memCode, String snsId, String snsType) {
        this.memCode = memCode;
        this.snsId = snsId;
        this.snsType = snsType;
        this.memNameConfirm = "N";
    }
}
