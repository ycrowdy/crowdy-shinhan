package com.ycrowdy.web.member.controller;

import com.ycrowdy.web.common.vo.ResponseVo;
import com.ycrowdy.web.member.service.MemberInvestorUpdateService;
import com.ycrowdy.web.member.vo.MemberInvestorRealNameVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

@RestController
@RequestMapping("/member")
public class MemberInvestorRealNameDocUpdateController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private String logInHeader  = "/MemberInvestorRealNameDocUpdateController INPUT:\n";
    private String logOutHeader = "/MemberInvestorRealNameDocUpdateController OUTPUT:\n";

    @Autowired
    private MemberInvestorUpdateService memberInvestorUpdateService;

    @PostMapping("/investor/update/real-name-doc")
    public ResponseVo controller(HttpServletRequest request, @RequestBody MemberInvestorRealNameVo data, HttpSession session) throws Exception {

        Map<String, Object> result = memberInvestorUpdateService.memberInvestorRealNameDocUpdate(data, session);

        return new ResponseVo();
    }
}
