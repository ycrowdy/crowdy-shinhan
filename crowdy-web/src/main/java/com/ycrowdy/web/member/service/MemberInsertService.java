package com.ycrowdy.web.member.service;

import com.ycrowdy.web.common.exception.vo.CommonException;
import com.ycrowdy.web.common.util.FrontConstants;
import com.ycrowdy.web.common.vo.ResponseVo;
import com.ycrowdy.web.common.vo.StatusVo;
import com.ycrowdy.web.data.service.DataService;
import com.ycrowdy.web.file.service.FileService;
import com.ycrowdy.web.member.vo.MemberCertiVo;
import com.ycrowdy.web.member.vo.MemberInvestorVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.Map;

@Service
public class MemberInsertService {

    @Autowired
    private DataService dataService;

    @Autowired
    private FileService fileService;

    public Map<String, Object> memberInvestorInsert(MemberInvestorVo vo, HttpSession session) throws Exception {
        //1. 실명 인증 정보 설정
        //1.1 주민번호 생성 개인회원인 경우
        Map<String, Object> memberAuthData = (Map<String, Object>) session.getAttribute("memberAuthData");
        if(vo.getMemType().compareTo("1") == 0) {
            String birthDay = String.valueOf(memberAuthData.get("birthDate"));
            String jumin = birthDay.substring(2) + vo.getMemIvsRegiCode();
//            if(!CommonUtils.juminCheck(jumin)) throw new CommonException(new ResponseVo(new StatusVo("9999", "올바른 주민번호가 아닙니다.")));
            vo.setMemIvsRegiCode(jumin);
        }

        vo.setMemberCerti(new MemberCertiVo(vo.getMemCode(), vo.getMemIvsRegiHpNo(), memberAuthData));

        if(vo.getMemIvsType().compareTo("MIT001") != 0) { //적격투자자, 전문투자자 증명서류 파일 업로드
            String fileName = fileService.saveCrowdyFiles(vo.getMemIvsFiles(), vo.getMemIvsDoc(),"mem_ivs_doc", "F0003", false);
            vo.setMemIvsDoc(fileName);
            vo.setMemIvsFiles(null);
        }

        if(vo.getMemIvsRelStatus().compareTo("MIR002") == 0) { //연고자 관련 파일 업로드
            if(vo.getMemIvsRelFile() == null || StringUtils.isEmpty(vo.getMemIvsRelFile().getFileData())) throw new CommonException(new ResponseVo(new StatusVo("9999", "연고자 신청시<br /> 관련파일을 업로드하여야합니다.")));
            String fileName = fileService.saveCrowdyFile(vo.getMemIvsRelFile(), "mem_ivs_rel_doc", "F0003", false);
            vo.setMemIvsRelDoc(fileName);
            vo.setMemIvsRelFile(null);
        }

        String memIvsRnmDoc = fileService.saveCrowdyFile(vo.getMemIvsRnmFile(), "mem_ivs_doc", "F0003", false);
        if(StringUtils.isNotEmpty(memIvsRnmDoc)) {
            vo.setMemIvsRnmDoc(memIvsRnmDoc);
            vo.setMemIvsRnmFile(null);
        }

        //법인 및 조합 관련 파일 업로드
        if(vo.getMemType().compareTo("1") != 0) {
            String memIvsRnmAttorney = fileService.saveCrowdyFile(vo.getMemIvsRnmAttorneyFile(), "mem_ivs_doc", "F0003", false);
            if(StringUtils.isNotEmpty(memIvsRnmAttorney)) {
                vo.setMemIvsRnmAttorney(memIvsRnmAttorney);
                vo.setMemIvsRnmAttorneyFile(null);
            }

            String memIvsCorDoc = fileService.saveCrowdyFile(vo.getMemIvsCorFile(), "mem_ivs_doc", "F0003", false);
            if(StringUtils.isNotEmpty(memIvsCorDoc)) {
                vo.setMemIvsCorDoc(memIvsCorDoc);
                vo.setMemIvsCorFile(null);
            }

            String memIvsRnmCorDoc = fileService.saveCrowdyFile(vo.getMemIvsRnmCorFile(), "mem_ivs_doc", "F0003", false);
            if(StringUtils.isNotEmpty(memIvsRnmCorDoc)) {
                vo.setMemIvsRnmCorDoc(memIvsRnmCorDoc);
                vo.setMemIvsRnmCorFile(null);
            }

            String memIvsCorSeal = fileService.saveCrowdyFile(vo.getMemIvsCorSealFile(), "mem_ivs_doc", "F0003", false);
            if(StringUtils.isNotEmpty(memIvsCorSeal)) {
                vo.setMemIvsCorSeal(memIvsCorSeal);
                vo.setMemIvsCorSealFile(null);
            }

            String memIvsCorAttorney = fileService.saveCrowdyFile(vo.getMemIvsCorAttorneyFile(), "mem_ivs_doc", "F0003", false);
            if(StringUtils.isNotEmpty(memIvsCorAttorney)) {
                vo.setMemIvsCorAttorney(memIvsCorAttorney);
                vo.setMemIvsCorAttorneyFile(null);
            }

            String memIvsBankImg = fileService.saveCrowdyFile(vo.getMemIvsBankFile(), "mem_ivs_doc", "F0003", false);
            if(StringUtils.isNotEmpty(memIvsBankImg)) {
                vo.setMemIvsBankImg(memIvsBankImg);
                vo.setMemIvsBankFile(null);
            }
        }

        return dataService.getData(FrontConstants.MEMBER_PREFIX + FrontConstants.INVESTOR_INSERT, vo);
    }
}
