package com.ycrowdy.web.member.vo;

import com.ycrowdy.web.common.vo.FileVo;
import lombok.Data;

import java.util.List;

@Data
public class MemberInvestorTypeVo {

    private String memCode;
    
    private String memType;
    
    private String memIvsType;

    private String memIvsState;

    private String memIvsDoc;

    private List<FileVo> memIvsFiles;

    public MemberInvestorTypeVo() {
        this.memIvsDoc = "";
    }
}
