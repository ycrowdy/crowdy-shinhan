package com.ycrowdy.web.member.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ycrowdy.web.common.vo.ResponseVo;
import com.ycrowdy.web.common.vo.StatusVo;
import com.ycrowdy.web.member.service.MemberSnsInfoService;

@RestController
@RequestMapping("/member")
public class MemberSnsInfoController {
    
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private String logInHeader  = "/MemberSnsInfoService INPUT:\n";
    private String logOutHeader = "/MemberSnsInfoService OUTPUT:\n";
    
    @Autowired
    private MemberSnsInfoService memberSnsInfoService;
    
    @PostMapping("/getSnsInfo")
    public Map<String, Object> controller(HttpServletRequest request) throws Exception {
        
        Map<String, Object> result = memberSnsInfoService.memberSnsInfo();

        return result;
    }
    
}
