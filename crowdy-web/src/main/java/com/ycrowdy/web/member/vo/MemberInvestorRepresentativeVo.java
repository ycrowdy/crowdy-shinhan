package com.ycrowdy.web.member.vo;

import lombok.Data;

@Data
public class MemberInvestorRepresentativeVo {

    private String memCode;

    private String memIvsCorNo;

    private String memIvsResName;

    private String memIvsResEmail;

    private String memIvsRegiHpNo;

    private boolean authChange;

    private MemberCertiVo memberCerti;
}
