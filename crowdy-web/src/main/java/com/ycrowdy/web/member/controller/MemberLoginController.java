package com.ycrowdy.web.member.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ycrowdy.web.common.vo.ResponseVo;
import com.ycrowdy.web.member.service.MemberLoginService;

@RestController
@RequestMapping("/member")
public class MemberLoginController {
    
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private String logInHeader  = "/MemberLoginController INPUT:\n";
    private String logOutHeader = "/MemberLoginController OUTPUT:\n";
    
    @Autowired
    private MemberLoginService memberLoginService;
    
    @PostMapping("/login")
    public ResponseVo controller(HttpServletRequest request, HttpSession session,
            @RequestBody Map<String, Object> data) throws Exception {
        
        Map<String, Object> result = memberLoginService.memberLogin(session, data);
        
        return new ResponseVo(result);
    }
}
