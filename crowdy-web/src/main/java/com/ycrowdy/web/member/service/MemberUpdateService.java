package com.ycrowdy.web.member.service;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ycrowdy.web.common.util.FrontConstants;
import com.ycrowdy.web.data.service.DataService;
import com.ycrowdy.web.file.service.FileService;
import com.ycrowdy.web.member.vo.MemberCertiVo;
import com.ycrowdy.web.member.vo.MemberVo;

@Service
public class MemberUpdateService {

    @Autowired
    private DataService dataService;

    @Autowired
    private FileService fileService;
    
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    
    public Map<String, Object> memberUpdate(MemberVo vo, HttpSession session) throws Exception {

        // 프로필 사진
        if(StringUtils.isNotEmpty(vo.getMemShotImgFile().getFileData()) ) {
            String memShotImg = fileService.saveAwsFile(vo.getMemShotImgFile());
            vo.setMemShotImg(memShotImg);
            vo.setMemShotImgFile(null);
        }

        if(vo.isAuthChange()) {
            Map<String, Object> memberAuthData = (Map<String, Object>) session.getAttribute("memberAuthData");
            vo.setMemNameConfirm("Y");
            vo.setMemberCerti(new MemberCertiVo(vo.getMemCode(), vo.getMobileNo(), memberAuthData));
        }

        return dataService.getData(FrontConstants.MEMBER_PREFIX + FrontConstants.UPDATE, vo);
    }

	public Map<String, Object> memberCertiUpdate(MemberCertiVo data, HttpSession session) throws Exception {
		Map<String, Object> memberAuthData = (Map<String, Object>) session.getAttribute("memberAuthData");
		return dataService.getData(FrontConstants.MEMBER_PREFIX + FrontConstants.UPDATE_CERTI, new MemberCertiVo(data.getMemCode(), "", memberAuthData));
	}
}
