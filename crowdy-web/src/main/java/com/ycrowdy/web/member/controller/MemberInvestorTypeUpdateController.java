package com.ycrowdy.web.member.controller;

import com.ycrowdy.web.common.vo.ResponseVo;
import com.ycrowdy.web.member.service.MemberInvestorUpdateService;
import com.ycrowdy.web.member.vo.MemberInvestorTypeVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

@RestController
@RequestMapping("/member")
public class MemberInvestorTypeUpdateController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private String logInHeader  = "/MemberInvestorTypeUpdateController INPUT:\n";
    private String logOutHeader = "/MemberInvestorTypeUpdateController OUTPUT:\n";

    @Autowired
    private MemberInvestorUpdateService memberInvestorUpdateService;

    @PostMapping("/investor/update/type")
    public ResponseVo controller(HttpServletRequest request, @RequestBody MemberInvestorTypeVo data, HttpSession session) throws Exception {

        Map<String, Object> result = memberInvestorUpdateService.memberInvestorTypeUpdate(data, session);

        return new ResponseVo(result);
    }

}
