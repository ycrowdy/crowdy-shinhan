package com.ycrowdy.web.member.vo;

import com.ycrowdy.web.common.vo.FileVo;
import lombok.Data;

@Data
public class MemberInvestorRelVo {

    private String memCode;

    private String memIvsRelStatus;

    private String memIvsRelComNo;

    private String memIvsRelDoc;

    private FileVo memIvsRelFile;
}
