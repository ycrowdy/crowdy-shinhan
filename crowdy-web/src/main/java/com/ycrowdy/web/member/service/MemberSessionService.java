package com.ycrowdy.web.member.service;

import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpSession;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.ycrowdy.web.common.vo.CustomUser;
import com.ycrowdy.web.member.vo.MemberLoginResultVo;

@Service
public class MemberSessionService {

    public void setMemberSession(HttpSession session, MemberLoginResultVo data) throws Exception {
        Collection<SimpleGrantedAuthority> role = new ArrayList<SimpleGrantedAuthority>();
        role.add(new SimpleGrantedAuthority("USER"));
        
        CustomUser user = new CustomUser(data.getMemName(), "USER_PASSWORD", role, data);
        Authentication auth = new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword(), user.getAuthorities());
    
        SecurityContext context = SecurityContextHolder.getContext();
        context.setAuthentication(auth);
        
        session.setAttribute("SPRING_SECURITY_CONTEXT", context);
        session.setAttribute("check", true);
        
    }
}
