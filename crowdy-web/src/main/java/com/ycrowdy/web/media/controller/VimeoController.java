package com.ycrowdy.web.media.controller;

import com.ycrowdy.web.common.vo.ResponseVo;
import com.ycrowdy.web.media.service.VimeoService;
import com.ycrowdy.web.media.vo.VimeoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/vimeo")
public class VimeoController {

    @Autowired
    private VimeoService vimeoService;

    @GetMapping("/get/{id:.+}")
    public ResponseVo controller(@PathVariable(name = "id") String id) throws Exception {
        return new ResponseVo(vimeoService.getVimeoThumbnail(id));
    }
}
