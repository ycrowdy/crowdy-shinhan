package com.ycrowdy.web.media.vo;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class VimeoVo {

    @SerializedName("thumbnail_small")
    private String small;

    @SerializedName("thumbnail_medium")
    private String medium;

    @SerializedName("thumbnail_large")
    private String large;
}
