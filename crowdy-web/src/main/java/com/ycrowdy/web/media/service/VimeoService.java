package com.ycrowdy.web.media.service;

import com.google.gson.Gson;
import com.ycrowdy.web.media.vo.VimeoVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@Service
public class VimeoService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private RestTemplate restTemplate;

    @Value("${viemo.api}")
    private String vimeoUrl;

    public VimeoVo getVimeoThumbnail(String id) throws Exception {
        restTemplate.getMessageConverters().add(new ByteArrayHttpMessageConverter());

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM));

        HttpEntity<String> entity = new HttpEntity<String>(httpHeaders);
        ResponseEntity<byte[]> response = restTemplate.exchange(vimeoUrl + id + ".json", HttpMethod.GET, entity, byte[].class);

        String data = new String(response.getBody(), "UTF-8");

        Gson gson = new Gson();
        VimeoVo [] vimeoVos = gson.fromJson(data, VimeoVo[].class);

        return vimeoVos[0];
    }
}
