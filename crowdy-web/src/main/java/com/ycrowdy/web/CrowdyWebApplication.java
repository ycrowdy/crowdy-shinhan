package com.ycrowdy.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrowdyWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrowdyWebApplication.class, args);
	}
}
