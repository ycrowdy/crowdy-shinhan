package com.ycrowdy.web.reward.funding.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ycrowdy.web.common.vo.ResponseVo;
import com.ycrowdy.web.reward.funding.service.BenefitService;
import com.ycrowdy.web.reward.funding.vo.BenefitResultVo;

@RestController
@RequestMapping("/reward")
public class BenefitGetController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private String logInHeader  = "/FundingBenefitController INPUT:\n";
    private String logOutHeader = "/FundingBenefitController OUTPUT:\n";
    
    @Autowired
    private BenefitService benefitService;
    
    @PostMapping("/funding/get/benefit/{id:.+}")
    public ResponseVo controller(HttpServletRequest request, @PathVariable(name = "id") String id, HttpSession session) throws Exception {
        BenefitResultVo result = benefitService.getBenefit(id);
        return new ResponseVo(result);
    }
}
