package com.ycrowdy.web.reward.save.vo;

import java.util.List;

import com.ycrowdy.web.common.vo.FileVo;

import lombok.Data;

@Data
public class RewardProjectFundVo {
	
    /*
     * 프로젝트 code
     * */
    private String cpCode;

    /*
     * 프로젝트 모금 은행 코드
     * */
    private String cpFundBankCode;
    
    /**
     * 개설자 코드
     */
    private String memCode;
    
    /*
     * 프로젝트 모금 은행 계좌번호
     * */
    private String cpFundBankAccountNo;

    /*
     * 프로젝트 모금 은행 예금주명
     * */
    private String cpFundBankAccountName;

    /*
     * 프로젝트 모금 통장 사본
     * */
    private String cpFundBankAccountImg;
    
    /*
     * 프로젝트 모금 통장 사본 파일
     * */
    private FileVo cpFundBankAccountFile;

    /*
     * 사업자등록증 사본
     * */
    private String cpBizImg;
    
    /*
     * 사업자등록증 사본 파일
     * */
    private FileVo cpBizFile;

    /*
     * 세금계산서 발행 이메일
     * */
    private String cpFundTaxEmail;
    
    /**
     * SNS 주소 목록
     */
    private List<RewardProjectSnsListVo> snsList;
}
