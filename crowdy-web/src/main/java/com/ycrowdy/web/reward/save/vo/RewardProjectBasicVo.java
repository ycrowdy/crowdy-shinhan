package com.ycrowdy.web.reward.save.vo;

import com.ycrowdy.web.common.vo.FileVo;

import lombok.Data;

@Data
public class RewardProjectBasicVo {

    private String cpCode;
    
    private String cpTitle;
    
    private String cpAliasUrl;
    
    private String cpFundType;
    
    private String cpTargetAmount;
    
    private String cpEndDate;
    
    private String cpCardImg;
    
    private String cpBannerImg;
    
    private FileVo cpCardFile;
    
    private FileVo cpBannerFile;
    
    private String cpKeyWord;
    
}
