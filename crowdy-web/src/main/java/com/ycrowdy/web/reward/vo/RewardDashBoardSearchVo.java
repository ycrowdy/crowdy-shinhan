package com.ycrowdy.web.reward.vo;

import lombok.Data;

@Data
public class RewardDashBoardSearchVo {

    private String cpCode;
    
    private String startDate;
    
    private String endDate;
    
    private String cpAliasUrl;
    
    public RewardDashBoardSearchVo(String cpCode, String startDate, String endDate, String cpAliasUrl) {
        this.cpCode = cpCode;
        this.startDate = startDate;
        this.endDate = endDate;
        this.cpAliasUrl = cpAliasUrl;
    }
}
