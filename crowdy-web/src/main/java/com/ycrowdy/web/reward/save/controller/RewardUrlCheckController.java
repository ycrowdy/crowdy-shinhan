package com.ycrowdy.web.reward.save.controller;

import com.ycrowdy.web.reward.save.service.RewardInsertService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

@RestController
@RequestMapping("/data")
public class RewardUrlCheckController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private String logInHeader  = "/RewardUrlCheckController INPUT:\n";
    private String logOutHeader = "/RewardUrlCheckController OUTPUT:\n";

    @Autowired
    private RewardInsertService rewardInsertService;

    @PostMapping("/reward/url")
    public boolean controller(HttpServletRequest request, HttpSession session,
                              @RequestBody Map<String, Object> data) throws Exception {

        return rewardInsertService.getRewardUrlCheck(data);
    }

}
