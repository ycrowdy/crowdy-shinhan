package com.ycrowdy.web.reward.save.vo;

import lombok.Data;

@Data
public class RewardProjectCommentVo {

    /*
     * 프로젝트코드
     * */
    private String cpCode;
    
    /*
     * 순번
     * */
    private String crpyIdx;
    
    /*
     * 이름
     * */
    private String memName;
    
    /*
     * 맴버 코드
     * */
    private String memCode;

    /*
     * 작성자 구분
     * */
    private String makerYn;

    /*
     * 프로필 이미지
     * */
    private String memShotImg;

    /*
     * 후원자 구분
     * */
    private String sponConfirm;

    /*
     * 계층
     * */
    private String crpyDeps;

    /*
     * 부모 시퀀스
     * */
    private String crpyPidx;

    /*
     * 정렬 순서 
     * */
    private String crpyOdbyNum;

    /*
     * 댓글 내용
     * */
    private String crpyContent;

    /*
     * 작성일
     * */
    private String wdate;
}
