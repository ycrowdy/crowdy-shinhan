package com.ycrowdy.web.reward.funding.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ycrowdy.web.reward.funding.service.BenefitService;
import com.ycrowdy.web.reward.funding.vo.BenefitResultVo;

@RestController
@RequestMapping("/reward")
public class BenefitSetController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private String logInHeader  = "/FundingBenefitController INPUT:\n";
    private String logOutHeader = "/FundingBenefitController OUTPUT:\n";
    
    @Autowired
    private BenefitService benefitService;
    
    @PostMapping("/funding/set/benefit")
    public boolean controller(HttpServletRequest request, @RequestBody BenefitResultVo data, HttpSession session) throws Exception {
        benefitService.setBenefit(data);
        return true;
    }
}
