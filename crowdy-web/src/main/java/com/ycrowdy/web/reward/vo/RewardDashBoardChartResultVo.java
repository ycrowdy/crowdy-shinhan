package com.ycrowdy.web.reward.vo;

import lombok.Data;

@Data
public class RewardDashBoardChartResultVo {

    private String value;

    private String day;
}
