package com.ycrowdy.web.reward.vo;

import lombok.Data;

@Data
public class RewardDashBoardSponsorInfoVo {

    private String cpCode;

    private String spsIdx;

    private String spsAmount;

    private String spsTotAmount;

    private String spsStatus;

    private String wdate;

    private String spsAddr1;

    private String spsAddr2;

    private String spsRsName;

    private String spsRSMobileno;

    private String spsMiscInfo;

    private String memName;

    private String memCode;

    private String memEmail;

    private String sbfUnitPrice;

    private String sbfQty;

    private String cbfTitle;
    
    private String options;
    
    private String addr;
    
    private String spsMBMobileno;
    
    private String sbfBenefit;
}
