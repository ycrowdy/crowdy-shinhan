package com.ycrowdy.web.reward.funding.service;

import com.google.gson.Gson;
import com.ycrowdy.web.reward.funding.vo.BenefitResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class BenefitService {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private Gson gson;

    public void setBenefit(BenefitResultVo vo) throws Exception {
        ValueOperations<String, String> valueOperations = stringRedisTemplate.opsForValue();
        valueOperations.set(vo.getId(), gson.toJson(vo), 5, TimeUnit.MINUTES);

    }
    
    public BenefitResultVo getBenefit(String id) throws Exception {
        ValueOperations<String, String> valueOperations = stringRedisTemplate.opsForValue();
        String result = valueOperations.get(id);
        return gson.fromJson(result, BenefitResultVo.class);
    }
}
