package com.ycrowdy.web.reward.save.vo;

import lombok.Data;

@Data
public class RewardProjectSnsListVo {

    /**
     * 인덱스
     */
    private String miscIdx;
	   
    /**
     * 프로젝트 code
     */
    private String cpCode;
    
    /**
     * 회원 code
     */
    private String memCode;

    /**
     * SNS 구분
     */
    private String snsType;
    
    /**
     * SNS URL
     */
    private String snsUrl;
}
