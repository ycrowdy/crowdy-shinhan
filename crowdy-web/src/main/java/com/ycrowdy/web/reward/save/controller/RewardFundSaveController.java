package com.ycrowdy.web.reward.save.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.ycrowdy.web.common.vo.ResponseVo;
import com.ycrowdy.web.reward.save.service.RewardInsertService;
import com.ycrowdy.web.reward.save.vo.RewardProjectFundVo;

@RestController
@RequestMapping("/set")
public class RewardFundSaveController {
    
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private String logInHeader  = "/RewardFundSaveController INPUT:\n";
    private String logOutHeader = "/RewardFundSaveController OUTPUT:\n";
   
    @Autowired
    private RewardInsertService rewardInsertService;

    @Autowired
    private Gson gson;
    
    @PostMapping("/reward/fund")
    public ResponseVo controller(HttpServletRequest request,
            @RequestBody RewardProjectFundVo data) throws Exception {
        Map<String, Object> result = rewardInsertService.setRewardFund(data);
        return new ResponseVo(result);
    }
    
}
