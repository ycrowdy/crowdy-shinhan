package com.ycrowdy.web.reward.vo;

import java.util.List;

import lombok.Data;

@Data
public class RewardDashBoardChartWrapper {

    private String count;
    
    private List<RewardDashBoardSponsorInfoVo> sponsorData;

    private List<RewardDashBoardChartResultVo> chartDate;
    
    private List<RewardDashBoardChartResultVo> sponsorChartData;
    
    private List<RewardDashBoardChartResultVo> amountChartData;
}
