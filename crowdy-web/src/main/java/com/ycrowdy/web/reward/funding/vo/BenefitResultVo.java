package com.ycrowdy.web.reward.funding.vo;

import java.util.List;

import lombok.Data;

@Data
public class BenefitResultVo {

    private String id;
    
    private String spsAmount;
    
    private String cpCode;
    
    private String memCode;
    
    List<BenefitVo> list;
    
}
