package com.ycrowdy.web.reward.save.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ycrowdy.web.common.vo.ResponseVo;
import com.ycrowdy.web.reward.save.service.RewardInsertService;
import com.ycrowdy.web.reward.save.vo.RewardProjectStoryVo;

@RestController
@RequestMapping("/set")
public class RewardStorySaveController {
    
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private String logInHeader  = "/RewardStorySaveController INPUT:\n";
    private String logOutHeader = "/RewardStorySaveController OUTPUT:\n";
   
    @Autowired
    private RewardInsertService rewardInsertService;
    
    @PostMapping("/reward/story")
    public ResponseVo controller(HttpServletRequest request, HttpSession session,
            @RequestBody RewardProjectStoryVo data) throws Exception {
        
        Map<String, Object> result = rewardInsertService.setRewardStory(data);
        return new ResponseVo(result);
    }
    
}
