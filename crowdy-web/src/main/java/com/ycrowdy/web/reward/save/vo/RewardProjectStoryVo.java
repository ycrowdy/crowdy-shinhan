package com.ycrowdy.web.reward.save.vo;

import java.util.ArrayList;
import java.util.List;

import com.ycrowdy.web.common.vo.FileVo;

import lombok.Data;

@Data
public class RewardProjectStoryVo {

    /*
     * 프로젝트 코드
     * */
    private String cpCode;

    /*
     * 사용자 코드
     * */
    private String memCode;

    /*
     * 프로젝트 스토리
     * */
    private String cpStory;

    /*
     * 프로젝트 문의전화
     * */
    private String cpRefundTell;

    /*
     * 프로젝트 문의전화 공개여부, 0:비공개 / 1:공개
     * */
    private String cpRefundTellViewStatus;

    /*
     * 프로젝트 문의 메일
     * */
    private String cpInquiryEmail;

    /*
     * 프로젝트 환불정책
     * */
    private String cpRefundPolicy;

    /*
     * 프로젝트 비디오 리스트
     * */
    private List<String> cpVideos;

    /*
     * 프로젝트 이미지 리스트
     * */
    private List<String> cpImages;
    
    /*
     * 프로젝트 이미지 파일 리스트
     * */
    private List<FileVo> cpImage;

    public RewardProjectStoryVo() {
        this.cpImages = new ArrayList<String>();
    }

  
}
