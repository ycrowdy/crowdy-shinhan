package com.ycrowdy.web.reward.vo;

import lombok.Data;

@Data
public class RewardDashBoardExcelSearchVo {

    private String cpCode;
    
    private String name;
    
    private String status;
    
    private String startDate;
    
    private String endDate;
    
    private String cbfIdx;
    
    private String spsIdx;

    /*public RewardDashBoardExcelSearchVo(String cpCode, String name, String status, String startDate, String endDate, String cbfIdx) {
        this.cpCode = cpCode;
        this.name = name;
        this.status = status;
        this.startDate = startDate;
        this.endDate = endDate;
        this.cbfIdx = cbfIdx;
    }*/
}
