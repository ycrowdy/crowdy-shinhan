package com.ycrowdy.web.reward.funding.vo;

import lombok.Data;

@Data
public class BenefitVo {

    private String cbfIdx;
    
    private String cpCode;
    
    private String cbfTitle;
    
    private String sbfUnitPrice;
    
    private String cbfInfo;
    
    private String cbfDevrAddrYn;
    
    private String benefit;
    
    private String qty;

}
