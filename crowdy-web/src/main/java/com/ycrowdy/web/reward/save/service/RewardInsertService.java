package com.ycrowdy.web.reward.save.service;

import java.util.Map;

import com.ycrowdy.web.common.mapper.UrlMapper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.ycrowdy.web.common.util.FrontConstants;
import com.ycrowdy.web.common.vo.FileVo;
import com.ycrowdy.web.data.service.DataService;
import com.ycrowdy.web.file.service.FileService;
import com.ycrowdy.web.reward.save.vo.RewardProjectBasicVo;
import com.ycrowdy.web.reward.save.vo.RewardProjectFundVo;
import com.ycrowdy.web.reward.save.vo.RewardProjectStoryVo;

@Service
public class RewardInsertService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private DataService dataService;
    
    @Autowired
    private FileService fileService;

    @Autowired
    private UrlMapper urlMapper;

    @Autowired
    private Gson gson;

    public boolean getRewardUrlCheck(Map<String, Object> data) throws Exception {
        int count = urlMapper.getRewardProjectUrl(data);
        return count > 0 ? false : true;
    }

    public Map<String, Object> setRewardBasic(RewardProjectBasicVo vo) throws Exception {
        
        //1. 프로젝트 카드 이미지 처리
        if(StringUtils.isNotEmpty(vo.getCpCardFile().getFileData()) ) {
            String cpCardImg = fileService.saveAwsFile(vo.getCpCardFile());
            vo.setCpCardImg(cpCardImg);
            vo.setCpCardFile(null);
        }
        
        //2. 프로젝트 배경 이미지 처리
//        if(StringUtils.isNotEmpty(vo.getCpBannerFile().getFileData()) ) {
//            String pjBannerImg = fileService.saveAwsFile(vo.getCpBannerFile());
//            vo.setCpBannerImg(pjBannerImg);
//            vo.setCpBannerFile(null);
//        }
        
        return dataService.setData(FrontConstants.SAVE_PREFIX + FrontConstants.REWARD_PROJECT_BASIC, vo);
    }
    

    public Map<String, Object> setRewardStory(RewardProjectStoryVo vo) throws Exception {
        
    	 //1. 프로젝트 이미지 처리
        for(FileVo image : vo.getCpImage()) {
            String fileName = image.getFileName();
            if(StringUtils.isNotEmpty(image.getFileData())) {
                fileName = fileService.saveAwsFile(image);
            }
            
            vo.getCpImages().add(fileName);
        }
        vo.setCpImage(null);
        
        return dataService.setData(FrontConstants.SAVE_PREFIX + FrontConstants.REWARD_PROJECT_STORY, vo);
    }


	public Map<String, Object> setRewardFund(RewardProjectFundVo vo) throws Exception {
		
        //1. 프로젝트 모금 통장 사본
        if(StringUtils.isNotEmpty(vo.getCpFundBankAccountFile().getFileData()) ) {
            String cpCardImg = fileService.saveAwsFile(vo.getCpFundBankAccountFile());
            vo.setCpFundBankAccountImg(cpCardImg);
            vo.setCpFundBankAccountFile(null);
        }
        
        //2. 사업자등록증 사본
        if(StringUtils.isNotEmpty(vo.getCpBizFile().getFileData()) ) {
            String pjBannerImg = fileService.saveAwsFile(vo.getCpBizFile());
            vo.setCpBizImg(pjBannerImg);
            vo.setCpBizFile(null);
        }
        
        return dataService.setData(FrontConstants.SAVE_PREFIX + FrontConstants.REWARD_PROJECT_FUND, vo);
	}
}
